import firebase from 'firebase';
import CurrentDateTime from './CurrentDateTime';
const UploadImage = async (type ,uri , imagename, id , sentBy ,childvalue ) =>
{
  const blob = await new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onload = function() {
      resolve(xhr.response);
    };
    xhr.onerror = function(e) {
      reject(new TypeError('Network request failed'));
    };
    xhr.responseType = 'blob';
    xhr.open('GET', uri, true);
    xhr.send(null);
  });
  const ref = firebase
    .storage()
    .ref()
    .child('Images/'+imagename); 
  const snapshot = await ref.put(blob);
  var sendingdate = CurrentDateTime(new Date());
  firebase.database().ref('Massages/').child(childvalue).push({ id: id , message : 'image test' , sentBy: sentBy , date: sendingdate , url: await snapshot.ref.getDownloadURL() , type: type  });
  return  true; 
    
} // 36897 58057  36897 69066
export default UploadImage;