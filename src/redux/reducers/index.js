import { combineReducers } from 'redux';

import auth from './auth';
import init from './init'


export default combineReducers({
  auth,
  init
});
