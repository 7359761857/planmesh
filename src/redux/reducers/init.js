import {GET_PREFERENCES} from './../actions/types'

const initialState = { 
  preferences:{years:[],fuel_types:[],models:[]},
  is_loading_indicator : false,
  credential:{email_address:'',password:''}
};

const init = (state = initialState, action) => {
  if (action.type == GET_PREFERENCES) {
    return { ...state, preferences: action.data };
  }
  return state;
};

export default init;
