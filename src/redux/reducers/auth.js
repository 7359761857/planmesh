import {UPDATE_USER,LOGIN,LOGOUT,SIGNUP_STEP1,FRIENDLIST, NOTIFICATION, NEWPOST, FRIENDREQUEST, NOTIFICATION_COUNT} from './../actions/types'

const initialAuthState = { 
  isLoggedIn: false,
  user:{},
  signup_step1:{},
  friendList:{},
  notification_count:0,
  new_post_count:0,
  friend_request_count:0,
  new_notification_count:0,
};

const auth = (state = initialAuthState, action) => {
  if (action.type === LOGIN) {
    return { ...state, isLoggedIn: true };
  }
  if (action.type === NOTIFICATION) {
    return { ...state, notification_count: action.data};
  }
  if (action.type === NOTIFICATION_COUNT) {
    return { ...state, new_notification_count: action.data};
  }
  if (action.type === FRIENDREQUEST) {
    return { ...state, friend_request_count: action.data};
  }
  if (action.type === NEWPOST) {
    return { ...state, new_post_count: action.data};
  }
  if (action.type === LOGOUT) {
    return { ...state, isLoggedIn: false,user:{},signup_step1:{},friendList:{},notification_count:0,new_post_count:0,friend_request_count:0};
  }
  if (action.type === UPDATE_USER) {
    return { ...state, user: Object.assign(state.user,action.data) };
  }
  if (action.type === SIGNUP_STEP1) {
    return { ...state, signup_step1: action.data };
  }
  if (action.type === FRIENDLIST) {
    return { ...state, friendList: action.data };
  }
  
  return state;
};

export default auth;
