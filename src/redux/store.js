import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' 
import rootReducer from "./reducers";
const persistConfig = {
  key: 'root',
  storage,
  whitelist:['auth','init']
}

const persistedReducer = persistReducer(persistConfig, rootReducer)
const enhancer = compose(
  applyMiddleware(thunk),
  
);
let store = createStore(persistedReducer,{},enhancer)
let persistor = persistStore(store)
 
export  { store, persistor }