import { LOGIN, UPDATE_USER } from '../actions/types'
import { API_ROOT } from './../../config/constant'
import RequestService from './../../services/RequestService'
import { store } from './../store'

export const registerUser = async (data, files) => {
  let params = { url: API_ROOT + 'registration', body: data, files: files }
  console.log('Request', params)
  let response = await new RequestService(params).callCreate()
  if (response.success) {
    store.dispatch({ type: UPDATE_USER, data: response.data })
  } else {
  }
  return response
}

export const loginUser = async (data) => {
  let params = { url: API_ROOT + 'login', body: data }
  let response = await new RequestService(params).callCreate()
  if (response.success) {
    store.dispatch({ type: LOGIN })
    store.dispatch({ type: UPDATE_USER, data: response.data })
  }
  return response
}
// export const  updateUser = (data) =>{
//   return {
//       type: UPDATE_USER,
//       data:data
//   };
export const updateUser = async (data, files) => {
  let params = { url: API_ROOT + 'update/profile', body: data }
  let response = await new RequestService(params).callCreate()
  if (response.success) {
    store.dispatch({ type: LOGIN })
    store.dispatch({ type: UPDATE_USER, data: response.data })
  }
  return response
}

export const logOut = async (data) => {
  let params = { url: API_ROOT + 'logout', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}

export const forgotPassword = async (data) => {
  let params = { url: API_ROOT + 'forgot_password', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}

export const changePassword = async (data) => {
  let params = { url: API_ROOT + 'change_password', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}

export const emailVerify = async (data) => {
  let params = { url: API_ROOT + 'app/email_verification', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}

export const mobileVerify = async (data) => {
  let params = { url: API_ROOT + 'app/mobile_verification', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}

export const emailResendOTP = async (data) => {
  let params = { url: API_ROOT + 'app/resend_email_otp', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}

export const mobileResendOTP = async (data) => {
  let params = { url: API_ROOT + 'app/resend_mobile_otp', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}

export const changeUserType = async (data) => {
  let params = { url: API_ROOT + 'app/salon_type/swap', body: data }
  let response = await new RequestService(params).callCreate()
  if (response.success) {
    let d = response.data
    delete d['user_type']
    d = Object.assign(store.getState().auth.user, d)
    //alert(JSON.stringify(d))
    // if(d['user_type']==0){
    //   d['user_type']='Owner'
    // }
    // else{
    //   d['user_type']='Professional'

    // }

    store.dispatch({ type: UPDATE_USER, data: d })
  } else {
  }
  return response
}

export const editProfile = async (data, files) => {
  let params = { url: API_ROOT + 'app/edit_profile', body: data, files: files }
  let response = await new RequestService(params).callCreate()
  return response
}

export const getFriends = async (data) => {
  let params = { url: API_ROOT + 'get_friends', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}

export const snedFriendRequest = async (data) => {
  let params = { url: API_ROOT + 'friend_request', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}

export const emailCheck = async (data) => {
  let params = { url: API_ROOT + 'email_check', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}

export const getMyFriends = async (data) => {
  let params = { url: API_ROOT + 'get_my_friends', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}
export const getBlockList = async (data) => {
  let params = { url: API_ROOT + 'block/list', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}
export const getCategoryList = async (data) => {
  let params = { url: API_ROOT + 'category/list', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}
export const AddCategoryList = async (data) => {
  let params = { url: API_ROOT + 'category/favourite/add', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}
export const getSubCategoryList = async (data) => {
  let params = { url: API_ROOT + 'sub_category/list', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}
export const AddSubCategoryList = async (data) => {
  let params = { url: API_ROOT + 'sub_category/favourite/add', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}
export const PlaceList = async (data) => {
  let params = { url: API_ROOT + 'place/list', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}
export const AddPlaceList = async (data) => {
  let params = { url: API_ROOT + 'place/favourite/add', body: data }
  let response = await new RequestService(params).callCreate()
  return response
}
