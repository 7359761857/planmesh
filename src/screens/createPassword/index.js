import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Alert, ImageBackground, TouchableOpacity, StatusBar, Platform } from "react-native";
import { Container, View, Text, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import FloatingLabel from 'react-native-floating-labels';
import { changePassword } from "../../redux/actions/auth";
import NavigationService from "../../services/NavigationService";
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts, API_ROOT } from "../../config/constant";

let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height


const backWhite = require("../../../assets/images/backWhite.png");
const bottomRight = require("../../../assets/images/Login/bottomRight.png");
const btnBg = require("../../../assets/images/btnBg.png");
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");

class CreatePassword extends ValidationComponent {

    constructor(props) {
        super(props);
        this.state = {
            labelStyleCode: styles.labelInputUnFocus, styleCode: styles.formInputUnFocus, code: "", codeValidation: true, codeValidationMsg: "",
            labelStyleNewPassword: styles.labelInputUnFocus, styleNewPassword: styles.formInputUnFocus, newpassword: "", newpasswordValidation: true, newpasswordValidationMsg: "",
            labelStyleRepassword: styles.labelInputUnFocus, styleRePassword: styles.formInputUnFocus, reenterpassword: "", reenterValidation: true, reenterValidationMsg: "",
            btnDoneDisable: true, btnDoneImage: btnBgGrey, wsError: false,
            login_id:this.props.navigation.state.params.login_id
        };
    }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;

        return {
            header: (
                <Header style={styles.headerAndroidnav}>
                <StatusBar barStyle="light-content" backgroundColor="#41199B" />
                <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                  <TouchableOpacity onPress={() => { navigation.goBack() }}>
                    <Image source={backWhite} style={{ tintColor: "white" }} />
                  </TouchableOpacity></View>
                <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                  <Text style={styles.headerTitle} >Create new password</Text></View>
        
              </Header>
            )
        }
    }

    componentDidMount() {
        this.setState({
            email: theNavigation.getParam('email'),
        })
    }

    onBlurCode() {
        if (this.state.code.length <= 0) {
            this.setState({
                labelStyleCode: styles.labelInputUnFocus,
                styleCode: styles.formInputUnFocus
            })
        } else {
            this.codevalidtion()
        }
    }
    onFocusCode() {
        this.setState({
            labelStyleCode: styles.labelInput,
            styleCode: styles.formInputFocus
        },()=>{
            this.validtion()
        })
    }
    onChangeTextCode = (code) => {
        this.setState({ code },()=>{
            this.validtion()
        })
        
    }

    codevalidtion = () => {
        this.validate({
            code: { minlength: 5, required: true },
        });
        if (this.state.code.length<5) {
            this.setState({ codeValidation: false, codeValidationMsg: this.getErrorsInField('code'), labelStyleCode: [styles.labelInput, styles.labelInputError] },()=>{
                this.validtion()
            })
            return false
        }
        this.setState({ codeValidation: true, codeValidationMsg: "" },()=>{
            this.validtion()
        })
        return true
    }

    onBlurNewPassword() {
        if (this.state.newpassword.length <= 0) {
            this.setState({
                labelStyleNewPassword: styles.labelInputUnFocus,
                styleNewPassword: styles.formInputUnFocus
            },()=>{
                this.validtion()
            })
        } else {
            this.NewPasswordvalidtion()
        }
    }
    onFocusNewPassword() {
        this.setState({
            labelStyleNewPassword: styles.labelInput,
            styleNewPassword: styles.formInputFocus
        })
    }

    onChangeTextNewPassword = (newpassword) => {
        this.setState({ newpassword },()=>{
            this.validtion()
        })
        
    }

    NewPasswordvalidtion = () => {
        
        if (this.state.newpassword.length < 6) {
            this.setState({ newpasswordValidation: false, newpasswordValidationMsg: "The New password length  must be greater than 5.", labelStyleNewPassword: [styles.labelInput, styles.labelInputError], },()=>{
                this.validtion()
            })
            return false
        }else if (this.state.newpassword != this.state.reenterpassword) {
            this.setState({ reenterValidation: false, reenterValidationMsg: "Re-enter password not match", labelStyleRepassword: [styles.labelInput, styles.labelInputError]},()=>{
                this.validtion()
            })
            return false
        }
        this.setState({ newpasswordValidation: true, newpasswordValidationMsg: "" },()=>{
            this.validtion()
        })
        return true
    }

    onBlurReNewPassword() {
        if (this.state.newpassword.length <= 0) {
            this.setState({
                labelStyleRepassword: styles.labelInputUnFocus,
                styleRePassword: styles.formInputUnFocus
            },()=>{
                this.validtion()
            })
        }else{
            this.ReenterPasswordvalidtion()
        }
    }
    onFocusReNewPassword() {
        this.setState({
            labelStyleRepassword: styles.labelInput,
            styleRePassword: styles.formInputFocus
        })
    }

    onChangeTextReenterPassword = (reenterpassword) => {
        this.setState({ reenterpassword },()=>{
            this.validtion()
        })
       
    }

    ReenterPasswordvalidtion = () => {
        if (this.state.reenterpassword.length < 6) {
            this.setState({ reenterValidation: false, reenterValidationMsg: "The Re-enter password length  must be greater than 5", labelStyleRepassword: [styles.labelInput, styles.labelInputError], },()=>{
                this.validtion()
            })
            return false
        }else if (this.state.newpassword != this.state.reenterpassword) {
            this.setState({ reenterValidation: false, reenterValidationMsg: "Re-enter password not match", labelStyleRepassword: [styles.labelInput, styles.labelInputError], },()=>{
                this.validtion()
            })
            return false
        }
        this.setState({ reenterValidation: true, reenterValidationMsg: "" },()=>{
            this.validtion()
        })
        return true
    }

    validtion = () => {
        this.validate({
            code: { minlength: 5, required: true },
        });
        if (this.isFieldInError('code')) {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
            return false
        } else if (this.state.newpassword.length < 6) {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
            return false
        } else if (this.state.reenterpassword.length < 6) {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
            return false
        } else if (this.state.newpassword != this.state.reenterpassword) {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
            return false
        }
        this.setState({ btnDoneDisable: false, btnDoneImage: btnBg, passwordValidation: true, })
        return true
    }

    onClickResetPassword = () => {

        var data = new FormData()
            data.append('login_id', this.state.login_id)
            data.append('verify_otp', this.state.code)
            data.append('password',this.state.newpassword)

            fetch(API_ROOT + 'reset_password', {
            method: 'post',
            body: data
            }).then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {
                    NavigationService.navigate('Login')
                } else {
                    Alert.alert(
                        '',
                        responseData.text,
                        [
                          { text: 'OK'},
                        ],
                        { cancelable: false },
                      );
                }
            })
            .catch((error) => {
                //alert('Oops your connection seems off, Check your connection and try again')
            })

        //this.wscall(this.state.email, this.state.code, this.state.newpassword)
    //     changePassword({email:this.state.email,verify_pin:this.state.code,new_password:this.state.newpassword}).then(res=>{
    //         if(res.success){
    //             NavigationService.navigate('Login')
    //         }
    //         else{

    //         }
    //         alert(res.text)
    //     }).catch(err=>{
    //         alert(JSON.stringify(err))
    //     })
    // }

    // wscall = async (email, code, password) => {
    //     let httpMethod = 'POST'
    //     let strURL = checkCode
    //     let headers = {
    //         'Content-Type': 'application/json',
    //         'device_token': deviceToken,
    //         'device_type': Platform.OS === 'ios' ? 1 : 2,
    //         'build_version': '1.0',
    //     }
    //     let params = {
    //         'email': email,
    //         'code': code,
    //         'password': password,
    //     }

    //     APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
    //         console.log('Forgot Password -> ');
    //         console.log(resJSON)
    //         if (resJSON.status == 200) {
    //             this.props.navigation.pop(2)
    //             return
    //         } else {
    //             this.refs.toast.show(resJSON.message);
    //         }
    //     })
 }

    render() {
        return (
            <Container flex-direction={"row"} style={{ flex: 1,backgroundColor:'#FFF' }} >
            <Toastt ref="toast"></Toastt>
            <BackgroundImage >
              <SafeAreaView />
    
              <View style={{ position: 'absolute', bottom: 0, right: 0,backgroundColor:'#FFF' }}>
                <ImageBackground source={bottomRight} style={{ width: 165, height: 143 }}>
                </ImageBackground>
              </View>
                    <Content>
                        <Text style={{ marginLeft:Width(4),fontSize:FontSize(18), color:colors.black, fontFamily:fonts.Raleway_Semibold, marginTop: 17,width:Width(80),lineHeight:28 }}>Please enter the code that has been sent to you via email or text</Text>
                        <FloatingLabel
                            labelStyle={[this.state.labelStyleCode,this.state.code.length==0?styles.labelInputUnFocus:'']}
                            inputStyle={styles.input}
                            style={[styles.formInput, this.state.styleCode]}
                            onChangeText={(code) => this.onChangeTextCode(code)} value={this.state.code}
                            onBlur={() => this.onBlurCode()}
                            onFocus={() => this.onFocusCode()}
                            textContentType="oneTimeCode"
                        >Code</FloatingLabel>
                        {
                            this.state.codeValidation == false ?
                                <Text style={[{ marginLeft: 25, marginRight: 30, marginTop: 10, color: "red", fontSize: 10, fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.codeValidationMsg}</Text>
                                : null
                        }
                        <FloatingLabel
                            labelStyle={[this.state.labelStyleNewPassword,this.state.newpassword.length==0?styles.labelInputUnFocus:'']}
                            inputStyle={styles.input}
                            style={[styles.formInput, this.state.styleNewPassword, { marginTop: 10 }]}
                            onChangeText={(newpassword) => this.onChangeTextNewPassword(newpassword)} value={this.state.newpassword}
                            onBlur={() => this.onBlurNewPassword()}
                            onFocus={() => this.onFocusNewPassword()}
                            password={true}
                        >Enter new password</FloatingLabel>
                        {
                            this.state.newpasswordValidation == false ?
                                <Text style={[{ marginLeft: 25, marginRight: 30, marginTop: 10, color: "red", fontSize: 10, fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.newpasswordValidationMsg}</Text>
                                : null
                        }
                        <FloatingLabel
                            labelStyle={[this.state.labelStyleRepassword,this.state.reenterpassword.length==0?styles.labelInputUnFocus:'']}
                            inputStyle={styles.input}
                            style={[styles.formInput, this.state.styleRePassword, { marginTop: 10 }]}
                            onChangeText={(reenterpassword) => this.onChangeTextReenterPassword(reenterpassword)} value={this.state.reenterpassword}
                            onBlur={() => this.onBlurReNewPassword()}
                            onFocus={() => this.onFocusReNewPassword()}
                            password={true}
                        >Re-enter new password</FloatingLabel>
                        {
                            this.state.reenterValidation == false ?
                                <Text style={[{ marginLeft: 25, marginRight: 30, marginTop: 10, color: "red", fontSize: 10, fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.reenterValidationMsg}</Text>
                                : null
                        }
                        <TouchableOpacity style={{ width: '100%', marginTop: 50 }} disabled={this.state.btnDoneDisable} onPress={() => this.onClickResetPassword()} >
                            <ImageBackground source={this.state.btnDoneImage} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Reset Password</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.goBack()}>
                            <Text style={{ color: '#007aff', fontSize: 15, fontFamily: "Roboto-Regular", letterSpacing: 0.005 }}> Cancel</Text>
                        </TouchableOpacity>
                    </Content>

                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default CreatePassword;
