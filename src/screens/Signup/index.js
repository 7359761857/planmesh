import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Alert, ImageBackground, Keyboard, TouchableOpacity, StatusBar, Text, TextInput, Platform } from "react-native";
import { Container, View, Button, Header, Left, Right, Content, Input } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import FloatingLabel from 'react-native-floating-labels';
import DateTimePicker from 'react-native-modal-datetime-picker';
//import DatePicker from 'react-native-datepicker'
import publicIP from 'react-native-public-ip';
import { Height, FontSize, Width } from "../../config/dimensions";
import { registerUser, emailCheck } from "../../redux/actions/auth";
import NavigationService from "../../services/NavigationService";
import { store } from "../../redux/store";
import { LOGIN, SIGNUP_STEP1 } from "../../redux/actions/types";
import { dateformat } from "../../services/dateformat";
//import { Picker, registerCustomDatePickerIOS } from 'react-native-wheel-datepicker';
//import CustomDatePickerIOS from 'react-native-custom-datepicker-ios';
//const DatePicker = registerCustomDatePickerIOS(CustomDatePickerIOS);
import DeviceInfo from 'react-native-device-info';
import moment from 'moment';
import DatePicker from 'react-native-date-picker'
import MixpanelManager from '../../../Analytics'

let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height


const backWhite = require("../../../assets/images/backWhite.png");
const bottomRight = require("../../../assets/images/Login/bottomRight.png");
const btnBgFB = require("../../../assets/images/Login/btnBgFB.png");
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
const btnBg = require("../../../assets/images/btnBg.png");

var subThis;

var dateFormat = require('dateformat');

const FBSDK = require('react-native-fbsdk');
const {
    LoginManager,
    GraphRequest,
    GraphRequestManager,
    AccessToken,
} = FBSDK;

// const infoRequest = new GraphRequest(
//     '/me',
//     null,
//     this._responseInfoCallback,
// );


_responseInfoCallback = (error, result) => {
    console.log('test');
    if (error) {
        alert('Error fetching data: ' + error.toString());
    } else {
        alert('Success fetching data: ' + result.toString());
    }
}


class SignUp extends ValidationComponent {

    constructor(props) {
        super(props);
        subThis = this
        this.defaultdate = new Date();
        this.defaultdate.setFullYear(this.defaultdate.getFullYear() - 18);
        this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
        this.state = {
            emailTouch: false,
            confirmEmailTouch: false,
            passwordTouch: false,
            labelStylePassword: styles.labelInputUnFocus, stylePassword: styles.formInputUnFocus, password: "", passwordValidation: true, passwordValidationMsg: "",
            labelStylebirthday: styles.labelInputUnFocus, stylebirthday: styles.formInputUnFocus, DOB: "", DOBValidation: true, DOBValidation: "",
            labelStyleEmail: styles.labelInputUnFocus, styleEmail: styles.formInputUnFocus, email: this.props.navigation.state.params != null ? this.props.navigation.state.params.email : '', emailValidation: true, emailValidationMsg: "",
            labelStyleconfEmail: styles.labelInputUnFocus, styleconfEmail: styles.formInputUnFocus, confrimemail: "", confemailValidation: true, confemailValidationMsg: "",
            isDateTimePickerVisible: false, selectedDate: '', btnDoneDisable: true, btnDoneImage: btnBgGrey, DOBPass: "",
            device_type: Platform.OS == 'android' ? 0 : 1,
            isEmailFocus: false,
            birthday: '',
            isCofirmEmailFocus: false,
            isPasswordFocus: false,
            typingTimeout: 0,
            existEmail: false,
            uuid: DeviceInfo.getUniqueId(),
            bdayforpicker: new Date(),
            is_from_social: this.props.navigation.state.params != null ? true : false,
            name: this.props.navigation.state.params != null ? this.props.navigation.state.params.name : ''

        };

        publicIP()
            .then(ip => {
                console.log(ip);
                this.setState({ ip: ip })
            })
            .catch(error => {
                console.log(error);
                // 'Unable to get IP address.'
            });

    }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={[styles.headerAndroidnav, { height: 44 }]}>
                    <StatusBar barStyle="light-content" backgroundColor="#41199B" />
                    {/* <Left style={{ alignItems: 'flex-start', justifyContent: 'flex-start',position:'absolute',left:5, bottom:10}} >
                        <Button transparent onPress={() => {
                            navigation.goBack()
                        }} style={{ width: Width(9), height: Height(3),marginLeft:Width(2)}} >
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </Button>
                    </Left>
                    <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row',width:'100%', zIndex:-1}}>
                        <Text style={[styles.headerTitle]} >Sign Up</Text>
                    </View> */}
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack() }}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                        <Text style={styles.headerTitle} >Sign Up</Text>
                    </View>
                    {/* <Right style={{ alignItems: 'flex-end', justifyContent: 'flex-end', flex: 0.25 }}> */}
                    {/* <Button transparent style={{ alignSelf: 'baseline' }} onPress={() => { subThis.onDoneInClick() }}><Text style={{ color: 'white' }}>Done</Text></Button> */}
                    {/* </Right> */}
                </Header>
            )
        }
    }
    finalValidation = () => {
        if (this.state.emailValidation && this.state.confemailValidation && this.state.passwordValidation && this.state.DOB.length != 0 && this.state.emailTouch && this.state.confirmEmailTouch && this.state.passwordTouch) {
            this.setState({ btnDoneDisable: false, btnDoneImage: btnBg })
        }
        else {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })

        }
    }

    onBlurEmail() {

        if (this.state.email.length <= 0) {
            this.setState({
                labelStyleEmail: styles.labelInputUnFocus,
                styleEmail: styles.formInputUnFocus,
            })
        } else {
            this.emailvalidtion()
            if (this.state.confrimemail.length >= 1) {
                this.confemailvalidtion()
            }
        }
        this.setState({
            labelStyleEmail: styles.labelInputUnFocus,
            styleEmail: styles.formInputUnFocus,
            emailTouch: true,
            isEmailFocus: false
        })
        this.finalValidation()
    }
    onFocusEmail() {
        this.setState({
            labelStyleEmail: styles.labelInput,
            styleEmail: styles.formInputFocus,
            isEmailFocus: true,
            isCofirmEmailFocus: false,
            isPasswordFocus: false
        })
    }
    checkEmailExist = (email) => {
        if (email.includes('@') && email.includes('.')) {
            emailCheck({ email }).then(res => {
                if (!res.success) {
                    this.setState({ emailValidation: false, emailTouch: true, isEmailFocus: true, emailValidationMsg: res.text, existEmail: true })
                }
                else {
                    this.setState({ emailValidation: true, emailTouch: true, isEmailFocus: true, emailValidationMsg: "", existEmail: false })
                }
            }).catch(err => {
                // alert('Oops your connection seems off, Check your connection and try again')
            })
        }
        else {

        }

    }
    onChangeTextEmail = (email) => {
        const self = this;

        this.setState({ email: email.toString().trim() }, () => {
            // this.onBlurEmail()
            // this.validtion()
            this.finalValidation()
            this.validate({
                confemail: { email: true, required: true },
            });
            if (this.isFieldInError('confemail')) {
                this.setState({ confemailValidation: false, confemailValidationMsg: this.getErrorsInField('confemail'), labelStyleconfEmail: [styles.labelInput], }, () => {
                    this.finalValidation()
                    this.validtion()
                })
                return false
            } else if (this.state.confrimemail != this.state.email) {
                this.setState({ confemailValidation: false, confemailValidationMsg: "Confirm email is not same as email.", labelStyleconfEmail: [styles.labelInput], }, () => {
                    this.finalValidation()
                    this.validtion()
                })
                return false
            }
            this.setState({ confemailValidation: true, confemailValidationMsg: "", labelStyleconfEmail: [styles.labelInput] }, () => {
                this.finalValidation()
                this.validtion()
            })
        }, () => {
            this.finalValidation()
        })
        if (self.state.typingTimeout) {
            clearTimeout(self.state.typingTimeout);
        }

        self.setState({
            typingTimeout: setTimeout(function () {
                self.checkEmailExist(email)
            }, 1000)
        })
    }
    emailvalidtion = () => {
        this.validate({
            email: { email: true, required: true },
        });
        if (this.isFieldInError('email')) {
            this.setState({ emailValidation: false, emailValidationMsg: 'Please enter your email address in format: yourname@example.com', labelStyleEmail: [styles.labelInput, styles.labelInputError], })
            return false
        }
        this.setState({ emailValidation: true, emailValidationMsg: "" })
        return true
    }

    onBlurconfEmail() {
        if (this.state.confrimemail.length <= 0) {
            this.setState({
                labelStyleconfEmail: styles.labelInputUnFocus,
                styleconfEmail: styles.formInputUnFocus
            })
        } else {
            this.confemailvalidtion()
        }
        this.setState({
            confirmEmailTouch: true,
            labelStyleconfEmail: styles.labelInputUnFocus,
            styleconfEmail: styles.formInputUnFocus,
            isCofirmEmailFocus: false
        })
        this.finalValidation()
    }
    onFocusconfEmail() {
        this.setState({
            labelStyleconfEmail: styles.labelInput,
            styleconfEmail: styles.formInputFocus,
            isCofirmEmailFocus: true
        })
    }
    onChangeTextConfEmail = (confrimemail) => {
        this.setState({ confrimemail: confrimemail.toString().trim() }, () => {
            //this.onBlurconfEmail()
            //this.validtion()
            this.validate({
                confemail: { email: true, required: true },
            });
            if (this.isFieldInError('confemail')) {
                this.setState({ confemailValidation: false, confemailValidationMsg: this.getErrorsInField('confemail'), labelStyleconfEmail: [styles.labelInput], }, () => {
                    this.finalValidation()
                    this.validtion()
                })
                return false
            } else if (confrimemail != this.state.email) {
                this.setState({ confemailValidation: false, confemailValidationMsg: "Confirm email is not same as email.", labelStyleconfEmail: [styles.labelInput] }, () => {
                    this.finalValidation()
                    this.validtion()
                })
                return false
            }

            this.setState({ confemailValidation: true, confemailValidationMsg: "", labelStyleconfEmail: [styles.labelInput], confirmEmailTouch: true }, () => {
                this.finalValidation()
                this.validtion()
            })
        }, () => {


        })
    }
    confemailvalidtion = () => {
        this.validate({
            confemail: { email: true, required: true },
        });
        if (this.isFieldInError('confemail')) {
            this.setState({ confemailValidation: false, confemailValidationMsg: this.getErrorsInField('confemail'), labelStyleconfEmail: [styles.labelInput, styles.labelInputError], })
            return false
        } else if (this.state.confrimemail != this.state.email) {
            this.setState({ confemailValidation: false, confemailValidationMsg: "Confirm email is not same as email.", labelStyleconfEmail: [styles.labelInput, styles.labelInputError], })
            return false
        }
        this.setState({ confemailValidation: true, confemailValidationMsg: "", labelStyleconfEmail: [styles.labelInput] })
        return true
    }

    onBlurPassword() {
        if (this.state.password.length <= 0) {
            this.setState({

                labelStylePassword: styles.labelInputUnFocus,
                stylePassword: styles.formInputUnFocus
            })
        } else {
            // this.setState({
            //     isDateTimePickerVisible : true,})
            //add some text
            this.passwordvalidtion()
        }
        this.setState({
            passwordTouch: true,
            labelStylePassword: styles.labelInputUnFocus,
            stylePassword: styles.formInputUnFocus,
            isPasswordFocus: false
        })
        this.finalValidation()

    }
    onFocusPassword() {
        this.setState({
            labelStylePassword: styles.labelInput,
            stylePassword: styles.formInputFocus,
            isPasswordFocus: true
        })
    }
    onChangeTextPassword = (password) => {
        this.setState({ password }, () => {
            //this.onBlurPassword()
            //this.passwordvalidtion()
            //this.validtion()
            this.finalValidation()
            this.validate({
                password: { minlength: 5, required: true },
            });
            if (this.state.password.length < 6) {
                this.setState({ passwordValidation: false, passwordValidationMsg: this.getErrorsInField('password'), labelStylePassword: [styles.labelInput], }, () => {
                    this.finalValidation()
                    this.validtion()
                })
                return false
            }
            this.setState({
                passwordValidation: true, passwordValidationMsg: "", labelStylePassword: styles.labelInput,
                stylePassword: styles.formInputUnFocus
            }, () => {
                //this.validtion()
                this.finalValidation()
                this.validtion()
            })
        })

    }

    fbLogin = (email, name, profile_pic, birthday) => {
        let data = { social_type: 1, dob: moment(birthday).format("YYYYMMDD"), email: email, android_token: global.android_token, ios_token: global.ios_token, device_type: this.state.device_type, uuid: this.state.uuid }
        registerUser(data).then(res => {
            if (res.success) {
                if (res.data.is_profile_complete == 1) {
                    store.dispatch({ type: LOGIN })
                    NavigationService.reset('dashboard')
                    //subThis.props.navigation.navigate('App')
                }
                else {
                    //store.dispatch({type:LOGIN})
                    NavigationService.navigate('SignupStep1', { name: name, profile_pic: profile_pic, birthday: birthday != '' ? moment(birthday).format("YYYYMMDD") : birthday, email: email })
                    //subThis.props.navigation.navigate('SignupStep1',{name:name,profile_pic:profile_pic,birthday:birthday != '' ? moment(birthday).format("YYYYMMDD") : birthday,email:email})
                }
            }
            else {
                alert(res.text)
                //NavigationService.reset('SignupStep1',{name:name,profile_pic:profile_pic,birthday:false})

            }
        }).catch(err => {
            // alert('Oops your connection seems off, Check your connection and try again')
        })
    }

    passwordvalidtion = () => {
        this.validate({
            password: { minlength: 5, required: true },
        });
        if (this.state.password.length < 6) {
            this.setState({ passwordValidation: false, passwordValidationMsg: this.getErrorsInField('password'), labelStylePassword: [styles.labelInput, styles.labelInputError], })
            return false
        }
        this.setState({
            passwordValidation: true, passwordValidationMsg: "", labelStylePassword: styles.labelInput,
            stylePassword: styles.formInputUnFocus
        }, () => {
            //this.validtion()
            this.finalValidation()
        })

        return true
    }
    onBlurbirthday() {
        if (this.state.password.length <= 0) {
            this.setState({
                labelStylebirthday: styles.labelInputUnFocus,
                stylebirthday: styles.formInputUnFocus
            }, () => {
                //this.validtion()
                this.finalValidation()

            })
            return false
        } else {
            //this.validtion()
            this.finalValidation()

            return true
        }
    }
    onFocusbirthday() {
        this.setState({
            isDateTimePickerVisible: true,
            labelStylebirthday: styles.labelInput,
            stylebirthday: styles.formInputFocus
        }, () => {
            //this.validtion()
            this.finalValidation()

        })
    }

    validtion = () => {
        this.validate({
            password: { minlength: 5, required: true },
            email: { email: true },
            confrimemail: { email: true },
        });
        if (this.isFieldInError('email')) {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
            return false
        } else if (this.isFieldInError('confrimemail')) {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
            return false
        }
        else if (this.state.password.length < 6) {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
            return false
        }
        else if (this.isFieldInError('password')) {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
            return false
        } else if (this.state.email != this.state.confrimemail) {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
            return false
        } else if (this.state.DOB.length < 1) {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
            return false
        }
        this.setState({ btnDoneDisable: false, btnDoneImage: btnBg, passwordValidation: true, })
        return true
    }
    onLoginClick = () => {
        this.props.navigation.push('SocialLogin')
    }

    onDoneClick = () => {
        let data = { social_type: 0, email: this.state.email, password: this.state.password, dob: this.state.DOBPass, android_token: global.android_token, ios_token: global.ios_token, device_type: this.state.device_type, uuid: this.state.uuid }

        store.dispatch({ type: SIGNUP_STEP1, data: data })

        NavigationService.navigate('SignupStep1', { name: this.state.name, profile_pic: false, birthday: this.state.DOBPass, is_from_social: this.state.is_from_social, email: this.state.email })
        //subThis.props.navigation.navigate('SignupStep1',{name:this.state.name,profile_pic:false,birthday:this.state.DOBPass,is_from_social:this.state.is_from_social,email:this.state.email})
    }

    onDoneInClick = () => {
        if (this.emailvalidtion() && this.confemailvalidtion() && this.passwordvalidtion() && this.onBlurbirthday()) {
            this.wscall(this.state.email, this.state.password);
        }
    }

    _showDateTimePicker = () => {
        if (Platform.OS == 'android') {
            if (this.state.isDateTimePickerVisible == true) {
                this.setState({ isDateTimePickerVisible: false })
            }
            else {
                this.setState({ isDateTimePickerVisible: true })
            }
        }
        else {
            this.setState({ isDateTimePickerVisible: true })
        }

        Keyboard.dismiss()
    };

    _hideDateTimePicker = () => {

        this.setState({ isDateTimePickerVisible: false }, () => {
            Keyboard.dismiss()
        });

    }
    _handleDatePicked = (date) => {

        console.log(date)
        if (date != undefined) {
            console.log('A date has been picked: ', dateFormat(date, "dd/mm/yyyy"));
            this.setState({ bdayforpicker: date, DOB: dateformat(dateFormat(date, "yyyy-mm-dd")), DOBPass: dateFormat(date, "yyyy-mm-dd") }, () => {
                //this.validtion()
                //this.finalValidation()

            })
            setTimeout(function () {
                subThis.Datevalidtion()

            }, 100);
        }
        this._hideDateTimePicker();
    };

    Datevalidtion = () => {
        //var g1 = moment().format('ll')
        //var g2 = moment(new Date(this.state.DOB)).format('ll')
        var d1 = new Date();
        var d2 = new Date(this.state.DOB);

        var g1 = d1.getTime()
        var g2 = d2.getTime()


        //console.log('Selected BDay Validation: '+ g2.getTime() >= g1.getTime() ? 'true' : 'false')

        // if (this.state.DOB.length < 1) {

        //     this.setState({ DOBValidation: false, DOBValidationMSG: "Select BirthDay.", labelStyleDOB: [styles.labelInput, styles.labelInputError] },()=>{
        //         //this.validtion()
        //         this.finalValidation()

        //     })
        //     return false
        // }
        // else 

        if (this.compare_dates(g1, g2) == 2 || this.compare_dates(g1, g2) == 0) {
            this.setState({ DOBValidation: false, DOBValidationMSG: "Invalid birthday", labelStyleDOB: [styles.labelInput, styles.labelInputError] }, () => {
                //this.validtion()
                this.finalValidation()
            })
            return false
        }
        else {
            this.setState({ DOBValidation: true }, () => { this.finalValidation() })

        }

        this.defaultdate = new Date(this.state.DOBPass)
        return true
    }

    compare_dates = function (date1, date2) {
        //alert(date1 +' '+date2)
        if (date2 - date1 > 0) {
            return 1
        }
        else if (date2 - date1 < 0) {
            return 2
        }
        else {
            return 0
        }

        //     if (date1>date2) return 1;
        //   else if (date1<date2) return 2;
        //   else return 0; 
    }

    FbLoginButton = () => {
        let self = this
        //LoginManager.logOut()
        LoginManager
            .logInWithPermissions(['public_profile', 'email'])
            .then(function (result) {
                if (result.isCancelled) {
                    console.log('Login cancelled');
                } else {
                    AccessToken
                        .getCurrentAccessToken()
                        .then((data) => {
                            let accessToken = data.accessToken
                            console.log(accessToken.toString())

                            const responseInfoCallback = (error, result) => {
                                if (error) {
                                    console.log(error)
                                    alert('Facebook request failed. Try Again.')
                                } else {
                                    console.log(result)
                                    self.fbLogin(result.email, result.name, result.picture.data.url, result.birthday)
                                    //checkfbFunc(result.id, result.email, result.name, result.picture.data.url)
                                }
                            }

                            const infoRequest = new GraphRequest(
                                '/me?fields=email,name,picture.height(480),birthday',
                                null,
                                responseInfoCallback,
                            );
                            new GraphRequestManager().addRequest(infoRequest).start();


                        }).catch(err => {
                            console.log(err)
                            alert('Facebook request failed. Try Again.')
                        })
                }
            }, function (error) {
                console.log('Login fail with error: ' + error);
            });

        checkfbFunc = (facebook_id, email, name, photo) => {
            if (email.length >= 1) {
                this.verifyFbIdws(facebook_id, email, name, photo)
            } else {
                this.props.navigation.push("subRegistration", {
                    'facebook_id': facebook_id,
                    'email': email,
                    'name': name,
                    'photo': photo,
                    'code': this.state.validCode
                })
            }
        }
    }


    verifyFbIdws = async (facebook_id, email, name, photo) => {
        let httpMethod = 'POST'
        let strURL = checkfb
        let headers = {
            'Content-Type': 'application/json',
            'device_token': deviceToken,
            'device_type': Platform.OS === 'ios' ? 1 : 2,
            'build_version': '1.0'
        }
        let params = {
            'fbid': facebook_id,
            'name': name,
            'email': email,
            'photo': photo,
            'ip': this.state.ip
        }

        registerUser({ social_type: 1, email: email, android_token: global.android_token, ios_token: global.ios_token, device_type: this.state.device_type }).then(res => {
            alert(JSON.stringify(res))
        }).catch(err => {
            //alert('Oops your connection seems off, Check your connection and try again')
        })
        // APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
        //     console.log('Login Screen -> ');
        //     console.log(resJSON)
        //     if (resJSON.status == 200) {
        //         let data = resJSON.data
        //         console.log(data);
        //         Pref.setData(Pref.kUserInfo, data)
        //         Pref.setData(Pref.kUserId, data.id)
        //         Pref.setData(Pref.kaccessToken, data.token)
        //         userid = data.id;
        //         token = data.token;

        //         this.props.navigation.push('dashboard')
        //         return
        //     } else {
        //         this.refs.toast.show(resJSON.message);
        //     }
        // })
    }



    wscall = async () => {
        let httpMethod = 'POST'
        let strURL = userRegister
        let headers = {
            'Content-Type': 'application/json',
            'device_token': deviceToken,
            'device_type': Platform.OS === 'ios' ? 1 : 2,
            'build_version': '1.0',
        }
        let params = {
            'email': this.state.email,
            'password': this.state.password,
            'name': this.state.name,
            'fbid': '',
            'code': this.state.validCode,
        }
        console.log('call api');
        if (this.state.selectedImageURI.length != 0) {
            console.log('uploadPhoto');
            headers = {
                'Content-Type': 'multipart/form-data',
                'device_token': deviceToken,
                'device_type': Platform.OS === 'ios' ? 1 : 2,
                'build_version': '1.0',
            }
            APICall.uploadPhoto('photo', this.state.selectedImageURI, 'image/jpg', 'userPhoto', strURL, headers, params, (resJSON) => {
                console.log('uploadPhoto -> ');
                console.log(resJSON)
                if (resJSON.status == 200) {
                    let data = resJSON.data
                    console.log(data);
                    // Pref.setData(Pref.kUserInfo, data)
                    // Pref.setData(Pref.kUserId, data.id)
                    // Pref.setData(Pref.kaccessToken, data.token)
                    this.props.navigation.push('OTP', {
                        data: data,
                    })
                    return
                } else {
                    this.refs.toast.show(resJSON.message);
                }
            })
        } else {
            console.log('callWebService');
            APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
                console.log(resJSON)
                if (resJSON.status == 200) {
                    let data = resJSON.data
                    console.log(data);
                    // Pref.setData(Pref.kUserInfo, data)
                    // Pref.setData(Pref.kUserId, data.id)
                    // Pref.setData(Pref.kaccessToken, data.token)
                    this.props.navigation.push('OTP', {
                        data: data,
                    })
                    return
                } else {
                    this.refs.toast.show(resJSON.message);
                }

            })
        }

    }
    componentDidMount() {
        //alert(store.getState().auth.isLoggedIn)
        // this.mixpanel.track('signup_start');    
        if (this.state.email != '') {
            this.onBlurEmail()
        }
    }
    render() {
        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
                {/* <BackgroundImage > */}
                <SafeAreaView />
                {/* <View style={{ position: 'absolute', bottom: 0, right: 0,backgroundColor:'#FFF' }}>
                        <ImageBackground source={bottomRight} style={{ width: 165, height: 143 }}>
                        </ImageBackground>
                    </View> */}
                {/* <Content> */}
                <View style={{ backgroundColor: '#fff', height: this.state.isDateTimePickerVisible ? Height(85) : Height(60) }}>
                    {
                        this.state.wsError == true ?
                            <View style={{ backgroundColor: "rgba(255, 0, 0, 0.1)", height: Height(5), alignItems: 'center', justifyContent: 'center', marginLeft: 25, marginRight: 30, marginTop: 20 }}>
                                <Text style={[{ color: "black", alignSelf: 'center', fontFamily: "Roboto-Bold", fontSize: FontSize(13) }, checkFontWeight("700")]}>{this.state.wsmessage}</Text>
                                {/* <Button transparent style={[{ alignSelf: 'center', marginTop: -10 }]}
                                onPress={() => this.onForgotPassword()}
                            >
                                <View style={{ flexDirection: 'row' }}>
                                <Text style={[{ color: "black", fontFamily: "Roboto-Regular", fontSize: FontSize(13) }]}>Did you </Text>
                                <Text style={{
                                    color: 'black', fontSize: FontSize(13), fontFamily: "Roboto-Regular", textDecorationStyle: 'solid', textDecorationLine: 'underline', textDecorationColor: 'black'
                                }}>forget your password?</Text>
                                </View>
                            </Button> */}
                            </View>
                            : null
                    }
                    <FloatingLabel
                        autoCapitalize="none"
                        textContentType={"emailAddress"}
                        labelStyle={[this.state.labelStyleEmail, this.state.email.length == 0 ? styles.labelInputUnFocus : '', this.state.emailValidation == false && this.state.isEmailFocus && this.state.emailTouch && this.state.email.length ? styles.labelInputError : {}]}
                        inputStyle={styles.input}
                        style={[styles.formInput, this.state.styleEmail]}
                        onChangeText={(email) => this.onChangeTextEmail(email)} value={this.state.email}
                        onBlur={() => this.onBlurEmail()}
                        onFocus={() => this.onFocusEmail()}
                        keyboardType={"email-address"}
                        touch={this.state.emailTouch && this.state.email.length}
                        valid={this.state.email.length == 0 ? false : this.state.emailValidation && !this.state.existEmail}

                    >Email</FloatingLabel>
                    {
                        this.state.emailValidation == false && this.state.isEmailFocus && this.state.emailTouch && this.state.email.length ?
                            <Text style={[{ marginLeft: Width(6), marginRight: Width(6), marginTop: Height(1), color: "red", fontSize: FontSize(10), fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.email.includes('@') && this.state.email.includes('.') ? this.state.emailValidationMsg == 'Please enter valid parameter' ? 'Please enter your email address in format: yourname@example.com' : 'Email has already been taken' : 'Please enter your email address in format: yourname@example.com'}</Text>
                            : null
                    }
                    <FloatingLabel
                        autoCapitalize="none"
                        labelStyle={[this.state.labelStyleconfEmail, this.state.confrimemail.length == 0 ? styles.labelInputUnFocus : '', this.state.confemailValidation == false && this.state.isCofirmEmailFocus && this.state.confirmEmailTouch && this.state.confrimemail.length ? styles.labelInputError : {}]}
                        inputStyle={styles.input}
                        style={[styles.formInput, this.state.styleconfEmail]}
                        onChangeText={(confrimemail) => this.onChangeTextConfEmail(confrimemail)} value={this.state.confrimemail}
                        onBlur={() => this.onBlurconfEmail()}
                        onFocus={() => this.onFocusconfEmail()}
                        keyboardType={"email-address"}
                        touch={this.state.confirmEmailTouch && this.state.confrimemail.length}
                        valid={this.state.confrimemail.length == 0 ? false : this.state.confemailValidation}
                    >Confirm email</FloatingLabel>
                    {
                        this.state.confemailValidation == false && this.state.isCofirmEmailFocus && this.state.confirmEmailTouch && this.state.confrimemail.length ?
                            <Text style={[{ marginLeft: Width(6), marginRight: Width(6), marginTop: Height(1), color: "red", fontSize: FontSize(10), fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.confemailValidationMsg}</Text>
                            : null
                    }
                    <FloatingLabel
                        autoCapitalize="none"
                        labelStyle={[this.state.labelStylePassword, this.state.password.length == 0 ? styles.labelInputUnFocus : '', this.state.passwordValidation == false && this.state.isPasswordFocus && this.state.passwordTouch && this.state.password.length ? styles.labelInputError : {}]}
                        inputStyle={styles.input}
                        style={[styles.formInput, this.state.stylePassword, { marginTop: 10 }]}
                        onChangeText={(password) => this.onChangeTextPassword(password)} value={this.state.password}
                        onBlur={() => this.onBlurPassword()}
                        onFocus={() => this.onFocusPassword()}
                        password={true}
                        touch={this.state.password.length}
                        valid={this.state.password.length == 0 ? false : this.state.passwordValidation}
                    >Password</FloatingLabel>
                    {/* {
                            this.state.passwordValidation == false ? */}
                    <Text style={[{ marginLeft: Width(6), marginRight: Width(6), marginTop: Height(1), color: "#B3B3B3", fontSize: FontSize(10), fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>Password should be at least 6 characters</Text>
                    {/* : null
                        } */}
                    <View>
                        <FloatingLabel
                            editable={false}
                            labelStyle={[this.state.labelStylebirthday]}
                            inputStyle={styles.input}
                            style={[styles.formInput, this.state.stylebirthday, { marginTop: 10 }, this.state.DOB.length == 0 ? styles.labelInputUnFocus : '']}
                            onChangeText={(DOB) => this.setState({ DOB }, () => this.finalValidation())} value={this.state.DOB}
                            onBlur={() => this.onBlurbirthday()}
                            onFocus={() => this.onFocusbirthday()}
                            // valid={this.state.DOB.length<1?false:true}
                            // touch={this.state.DOB.length<1?false:true}
                            valid={this.state.DOBValidation == false ? false : true}
                            touch={this.state.DOBValidation == false ? false : true}
                        >
                            {/* {Platform.OS == 'ios' ? 'Birthday' : ''} */}
                            Birthday
                            </FloatingLabel>

                        <TouchableOpacity style={{ position: 'absolute', top: 10, bottom: 0, left: Width(6), right: Width(6) }} onPress={() => this._showDateTimePicker()}>
                        </TouchableOpacity>
                        {
                            Platform.OS == 'android' ?
                                // <DatePicker
                                // style={{position: 'absolute', top: 10, bottom: 0, left: Width(6), right: Width(6), fontSize:FontSize(16)}}
                                // isVisible = {this.state.isDateTimePickerVisible}
                                // mode="date"
                                // bdayforpicker={this.state.bdayforpicker}
                                // maxDate={new Date()}
                                // androidMode='spinner'
                                // placeholder="Birthday"
                                // confirmBtnText="Confirm"
                                // cancelBtnText="Cancel"
                                // showIcon={false}
                                // customStyles={{
                                //     dateText: {
                                //     fontSize: FontSize(16)
                                //     },
                                //     dateInput: {
                                //     position: 'absolute', left: Width(0),
                                //     borderColor: 'transparent',
                                //     fontSize:FontSize(16)
                                //     }
                                // }}
                                // onDateChange={(date) => {
                                //     console.log("datedate",dateformat(dateFormat(date, "yyyy-mm-dd")))
                                //     this.setState({ bdayforpicker:date,DOB: moment(date).format('DD MMM YYYY'), DOBPass: dateFormat(date, "yyyy-mm-dd") },()=>{
                                //         //this.validtion()
                                //         //this.finalValidation()
                                //     })
                                //     setTimeout(function () {
                                //         subThis.Datevalidtion()
                                //     }, 100);

                                // }} />
                                this.state.isDateTimePickerVisible == true ?
                                    <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff' }}>
                                        <DatePicker
                                            date={this.state.bdayforpicker}
                                            minimumDate={new Date()}
                                            mode='date'
                                            onDateChange={(date) => {
                                                this.setState({ bdayforpicker: date, DOB: moment(date).format('DD MMM YYYY'), DOBPass: dateFormat(date, "yyyy-mm-dd") }, () => {
                                                    //this.validtion()
                                                    //this.finalValidation()
                                                })
                                                setTimeout(function () {
                                                    subThis.Datevalidtion()
                                                }, 100);
                                            }}
                                        />
                                    </View>
                                    : null
                                :
                                null
                        }
                    </View>
                    {
                        this.state.DOBValidation == false ?
                            <Text style={[{ marginLeft: Width(6), marginRight: Width(6), marginTop: Height(1), color: "red", fontSize: FontSize(10), fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.DOBValidationMSG}</Text>
                            : null
                    }
                    {/* <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: Height(2), flexDirection: 'row' }}>
                            <Text style={{ backgroundColor: '#363169', width: 25, height: 1, marginRight: 8 }} ></Text>
                            <Text style={{ color: '#363169', textAlign: 'center', fontSize: FontSize(16), fontFamily: "Helvetica", letterSpacing: 0.005 }}>or</Text>
                            <Text style={{ backgroundColor: '#363169', width: 25, height: 1, marginLeft: 8 }} ></Text>
                        </View> */}
                    {/* <View style={{height:Height(4)}}/> */}

                    {/* <TouchableOpacity style={{alignSelf:'center',width: '88%',height:Height(7),backgroundColor:'#3E5D94',borderRadius:Width(6),justifyContent:'center',alignItems:'center' }} onPress={() => this.FbLoginButton()}>
                            <Text style={[{  fontSize: FontSize(19), fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Sign Up with Facebook</Text>
                        </TouchableOpacity> */}
                    {/* <View style={{height:Height(10)}}/> */}

                    <View style={{ width: '100%', bottom: 25, alignItems: 'center', position: 'absolute' }}>
                        <TouchableOpacity style={{ alignSelf: 'center', width: '88%', height: Height(6.5), backgroundColor: this.state.btnDoneDisable ? '#8F8F90' : '#EC4AB9', borderRadius: Width(6), justifyContent: 'center', alignItems: 'center' }} disabled={this.state.btnDoneDisable} onPress={() => this.onDoneClick()} >
                            {/* <ImageBackground source={this.state.btnDoneImage} style={{ width: '100%', height: Height(9), alignItems: 'center', justifyContent: 'center' }}> */}
                            <Text style={[{ fontSize: FontSize(19), fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Next</Text>
                            {/* </ImageBackground> */}
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{ backgroundColor: '#F2F4F7', width: '100%', height: 220, position: 'absolute', bottom: 0, right: 0, zIndex: -1 }}>
                    <ImageBackground source={bottomRight} style={{ width: 165, height: 143, position: 'absolute', bottom: 0, right: 0 }}>
                    </ImageBackground>
                    <View style={{ position: 'absolute', width: '100%', bottom: Width(40), alignItems: 'center' }}>
                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} onPress={() => this.onLoginClick()}>
                            <Text style={{ color: '#333333', fontSize: FontSize(15), fontFamily: "Roboto-Regular", letterSpacing: 0.005 }}>Already have an account? </Text><Text style={{ color: '#007aff', fontSize: FontSize(15), fontFamily: "Roboto-Regular", letterSpacing: 0.005 }}>Log in</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                {/* <View style = {{height:Height(26), backgroundColor:'#F2F4F7'}}>
                        <View style = {{width:'100%',height:Height(5), alignItems:'center'}}>
                            <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: Height(3) }} onPress={() => this.onLoginClick()}>
                                <Text style={{ color: '#333333', fontSize: FontSize(15), fontFamily: "Roboto-Regular", letterSpacing: 0.005 }}>Already have an account? </Text><Text style={{ color: '#007aff', fontSize: FontSize(15), fontFamily: "Roboto-Regular", letterSpacing: 0.005 }}>Log in</Text>
                            </TouchableOpacity>
                        </View>
                        <ImageBackground source={bottomRight} style={{ width: 165, height: 143, position: 'absolute', bottom: 0, right: 0,zIndex:-1}}>
                        </ImageBackground>
                    </View> */}

                {/* </Content> */}

                {/* <SafeAreaView /> */}
                {/* </BackgroundImage> */}
                {
                    Platform.OS == 'ios' ?
                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDateTimePicker}
                            mode='date'
                            date={this.state.bdayforpicker}

                        />
                        :
                        null
                }


                {/* <DatePicker
              date={this.state.firstDate}
              mode="date"
              textSize={36}
              textColor='red'
              onDateChange={date => this.setState({ firstDate })}
              style={{ width: '100%' }}
            /> */}
            </Container>
        );
    }
}

export default SignUp;
