import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Alert, ImageBackground, Keyboard,TouchableOpacity, StatusBar, Text, TextInput, Platform } from "react-native";
import { Container, View, Button, Header, Left, Right, Content, Input } from "native-base";
import AsyncStorage from '@react-native-community/async-storage';
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import FloatingLabel from 'react-native-floating-labels';
import DateTimePicker from 'react-native-modal-datetime-picker';
import DatePicker from 'react-native-datepicker'
import publicIP from 'react-native-public-ip';
import { Height, FontSize, Width } from "../../config/dimensions";
import { registerUser, emailCheck } from "../../redux/actions/auth";
import NavigationService from "../../services/NavigationService";
import { store } from "../../redux/store";
import { LOGIN, SIGNUP_STEP1,UPDATE_USER } from "../../redux/actions/types";
import { dateformat } from "../../services/dateformat";
//import { Picker, registerCustomDatePickerIOS } from 'react-native-wheel-datepicker';
//import CustomDatePickerIOS from 'react-native-custom-datepicker-ios';
//const DatePicker = registerCustomDatePickerIOS(CustomDatePickerIOS);
import DeviceInfo from 'react-native-device-info';
import moment from 'moment';

let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height


const backWhite = require("../../../assets/images/backWhite.png");
const bottomRight = require("../../../assets/images/Login/bottomRight.png");
const btnBgFB = require("../../../assets/images/Login/btnBgFB.png");
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
const btnBg = require("../../../assets/images/btnBg.png");

var subThis;

var dateFormat = require('dateformat');

// const FBSDK = require('react-native-fbsdk');
// const {
//     LoginManager,
//     GraphRequest,
//     GraphRequestManager,
//     AccessToken,
// } = FBSDK;

// const infoRequest = new GraphRequest(
//     '/me',
//     null,
//     this._responseInfoCallback,
// );


_responseInfoCallback = (error, result) => {
    console.log('test');
    if (error) {
        alert('Error fetching data: ' + error.toString());
    } else {
        alert('Success fetching data: ' + result.toString());
    }
}


class Birthday extends ValidationComponent {

    constructor(props) {
        super(props);
        subThis = this
        this.defaultdate = new Date();
        this.defaultdate.setFullYear(this.defaultdate.getFullYear() - 18);
        this.state = {
            emailTouch:false,
            confirmEmailTouch : false,
            passwordTouch:false,
            labelStylebirthday: styles.labelInputUnFocus, stylebirthday: styles.formInputUnFocus, DOB: "", DOBValidation: true, DOBValidation: "",
            isDateTimePickerVisible: false, selectedDate: '', btnDoneDisable: true, btnDoneImage: btnBgGrey, DOBPass: "",
            device_type : Platform.OS=='android'?0:1,
            isEmailFocus :false,
            birthday:'',
            isCofirmEmailFocus :false,
            isPasswordFocus : false,
            typingTimeout: 0,
            existEmail:false,
            uuid:DeviceInfo.getUniqueId(),
            bdayforpicker: new Date(),
            is_from_social: this.props.navigation.state.paramsis_from_social,
            name: this.props.navigation.state.params != null ? this.props.navigation.state.params.name : '',
            email: this.props.navigation.state.params != null ? this.props.navigation.state.params.email : '',
            is_for_apple:this.props.navigation.state.params != null ? this.props.navigation.state.params.is_for_apple : false,
            profile_pic:this.props.navigation.state.params.profile_pic != undefined ? this.props.navigation.state.params.profile_pic : '',
            type: this.props.navigation.state.params.type != undefined ? this.props.navigation.state.params.type : ''
        };

        publicIP()
            .then(ip => {
                console.log(ip);
                this.setState({ ip: ip })
            })
            .catch(error => {
                console.log(error);
                // 'Unable to get IP address.'
            });

    }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={[styles.headerAndroidnav,{height:44}]}>
                    <StatusBar barStyle="light-content" backgroundColor="#41199B" />

                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack() }}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                        <Text style={styles.headerTitle} >Sign Up</Text>
                    </View>

                    {/* <Left style={{ alignItems: 'flex-start', justifyContent: 'flex-start', flex: 0.2, }} >
                        <Button transparent onPress={() => {
                            navigation.goBack()
                        }} style={{ width: Width(9), height: Height(3),marginLeft:Width(2)}} >
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </Button>
                    </Left>
                    <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', flex: 0.6 }}>
                        <Text style={[styles.headerTitle]} >Sign Up</Text>
                    </View>
                    <Right style={{ alignItems: 'flex-end', justifyContent: 'flex-end', flex: 0.25 }}>
                        <Button transparent style={{ alignSelf: 'baseline' }} onPress={() => { subThis.onDoneInClick() }}><Text style={{ color: 'white' }}>Done</Text></Button>
                    </Right> */}
                </Header>
            )
        }
    }
    finalValidation = () =>{
        if(this.state.DOB.length!=0){
            this.setState({ btnDoneDisable: false, btnDoneImage: btnBg})
        }
        else{
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey})
        }
    }

    onBlurbirthday() {
        if (this.state.password.length <= 0) {
            this.setState({
                labelStylebirthday: styles.labelInputUnFocus,
                stylebirthday: styles.formInputUnFocus
            },()=>{
                //this.validtion()
                this.finalValidation()

            })
            return false
        } else {
            //this.validtion()
            this.finalValidation()

            return true
        }
    }
    onFocusbirthday() {
        this.setState({
            isDateTimePickerVisible: true,
            labelStylebirthday: styles.labelInput,
            stylebirthday: styles.formInputFocus
        },()=>{
            //this.validtion()
            this.finalValidation()

        })
    }

    validtion = () => {
        if (this.state.DOB.length < 1) {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
            return false
        }
        this.setState({ btnDoneDisable: false, btnDoneImage: btnBg, passwordValidation: true, })
        return true
    }
   
    titleCase(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
        }
        return splitStr.join(' '); 
     }

    onDoneClick = () => {
        
       // NavigationService.navigate('signup',{email:this.state.email,name:this.state.name,DOBPass:this.state.DOBPass,is_from_social:true})

       var username = this.titleCase(this.state.name)

       if(this.state.is_for_apple == true)
       {
            let country_code = '+1'

            
            let data = {email:this.state.email,name:username.trim(),bio:'',city:'',country_code:country_code,mobile_number:'',profile_pic:'',dob:this.state.DOBPass}
            // const mixpanel_data_with_social = {
            //     loginType: 'Social',
            //     brithday: this.state.DOBPass,
            //     phoneNumber: this.state.phone,
            //     fullName: fullName,
            //     bio: bio,
            // }

            console.log('SignUpRequest1: '+JSON.stringify(data))

            registerUser(Object.assign(data,{device_type:this.state.device_type,uuid:this.state.uuid,android_token:global.android_token,ios_token:global.ios_token,social_type:4})).then(res=>{
                if(res.success){
                    store.dispatch({type:LOGIN})
                    store.dispatch({type:UPDATE_USER,data:res.data})
                    AsyncStorage.setItem('overlay','true')

                    NavigationService.reset('Categories')
                    //NavigationService.navigate('Categories')
                }
                else{
                    alert(res.text)
                }
            }).catch(err=>{
                //alert('Oops your connection seems off, Check your connection and try again')
            })
       }
       else
       {
           let profilepic = this.state.profile_pic != undefined && this.state.profile_pic != null ? this.state.profile_pic : false
            NavigationService.navigate('SignupStep1',{name:this.state.name,profile_pic:profilepic,birthday:this.state.DOBPass,is_from_social:true,email:this.state.email, type: this.state.type})
       }

       
       //subThis.props.navigation.navigate('SignupStep1',{name:this.state.name,profile_pic:false,birthday:this.state.DOBPass,is_from_social:true,email:this.state.email})
        // let data = {social_type:0,email:this.state.email,password:this.state.password,dob:this.state.DOBPass,android_token:global.android_token,ios_token:global.ios_token,device_type:this.state.device_type,uuid:this.state.uuid}
        
        // store.dispatch({type:SIGNUP_STEP1,data:data})
        
        // NavigationService.navigate('SignupStep1',{name:this.state.name,profile_pic:false,birthday:false,is_from_social:this.state.is_from_social,email:this.state.email})
        
           
    }

    _showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true })
        Keyboard.dismiss()
};

    _hideDateTimePicker = () => {
        
        this.setState({ isDateTimePickerVisible: false },()=>{
            Keyboard.dismiss()
        });

    }
    _handleDatePicked = (date) => {
        
        console.log(date)
        if (date != undefined) {
            console.log('A date has been picked: ', dateFormat(date, "dd/mm/yyyy"));
            this.setState({ bdayforpicker:date,DOB: dateformat(dateFormat(date, "yyyy-mm-dd")), DOBPass: dateFormat(date, "yyyy-mm-dd") },()=>{
                //this.validtion()
                //this.finalValidation()

            })
            setTimeout(function () {
                subThis.Datevalidtion()
                
            }, 100);
        }
        this._hideDateTimePicker();
    };

    Datevalidtion = () => {
        
        var g1 = new Date();
        var g2 = new Date(this.state.DOB);

        if(this.compare_dates(g1,g2) == 2 || this.compare_dates(g1,g2) == 0)
        {
            this.setState({ DOBValidation: false, DOBValidationMSG: "Invalid birthday", labelStyleDOB: [styles.labelInput, styles.labelInputError] },()=>{
                //this.validtion()
                this.finalValidation()
            })
            return false
        }
        else
        {
            this.setState({DOBValidation:true}, () => {this.finalValidation()})

        }

        this.defaultdate = new Date(this.state.DOBPass)
        return true
    }

     compare_dates = function(date1,date2){
        if (date1>date2) return 1;
      else if (date1<date2) return 2;
      else return 0; 
     }

    componentDidMount(){
        //alert(store.getState().auth.isLoggedIn)
    }

    render() {
        return (
            <Container flex-direction={"row"} style={{ flex: 1,backgroundColor:'#FFFFFF' }} >
                <BackgroundImage >
                    <SafeAreaView />
                    <Content>
                        <Text style={[{ fontSize: FontSize(17), fontFamily: "Roboto-Bold", color: "#363169",marginLeft:Width(6), marginTop:40}, checkFontWeight("400")]}>When's your Birthday?</Text>
                        <Text style = {{width:Width(90), fontSize:FontSize(13), fontFamily:'Roboto-Regular', marginTop:10, color:'#8F8F90', textAlign:'center'}}>This helps us provide the best experience for your age.</Text>
                        <View>
                            <FloatingLabel
                                editable={false}
                                labelStyle={[this.state.labelStylebirthday]}
                                inputStyle={styles.input}
                                style={[styles.formInput, this.state.stylebirthday, { marginTop: 10 },this.state.DOB.length==0?styles.labelInputUnFocus:'']}
                                onChangeText={(DOB) => this.setState({ DOB },()=>this.finalValidation())} value={this.state.DOB}
                                onBlur={() => this.onBlurbirthday()}
                                onFocus={() => this.onFocusbirthday()}
                                // valid={this.state.DOB.length<1?false:true}
                                // touch={this.state.DOB.length<1?false:true}
                                valid={this.state.DOBValidation == false ?false:true}
                                touch={this.state.DOBValidation == false ?false:true}
                                >
                            {Platform.OS == 'ios' ? 'Birthday' : ''}
                            </FloatingLabel>
                            
                        <TouchableOpacity style={{ position: 'absolute', top: 10, bottom: 0, left: Width(6), right: Width(6) }} onPress={() => this._showDateTimePicker()}>
                        </TouchableOpacity>
                        {
                            Platform.OS == 'android' ?
                            <DatePicker
                            style={{position: 'absolute', top: 10, bottom: 0, left: Width(6), right: Width(6), fontSize:FontSize(16)}}
                            isVisible = {this.state.isDateTimePickerVisible}
                            mode="date"
                            maxDate={new Date()}
                            androidMode='spinner'
                            placeholder="Birthday"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            showIcon={false}
                            customStyles={{
                                dateText: {
                                fontSize: FontSize(16)
                                },
                                dateInput: {
                                position: 'absolute', left: Width(0),
                                borderColor: 'transparent',
                                fontSize:FontSize(16)
                                }
                            }}
                            onDateChange={(date) => {
                                this.setState({ bdayforpicker:date,DOB: moment(date).format('DD MMM YYYY'), DOBPass: dateFormat(date, "yyyy-mm-dd") },()=>{
                                    //this.validtion()
                                    //this.finalValidation()
                                })
                                setTimeout(function () {
                                    subThis.Datevalidtion()
                                }, 100);
                                
                            }} />
                            :
                            null
                        }
                        </View>
                        {
                            this.state.DOBValidation == false ?
                                <Text style={[{ marginLeft: Width(6), marginRight: Width(6), marginTop: Height(1), color: "red", fontSize: FontSize(10), fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.DOBValidationMSG}</Text>
                                : null
                        }
                    </Content>
                    <View style = {{position:'absolute', width:'100%',bottom:60, alignItems:'center'}}>
                        <TouchableOpacity style={{ alignSelf:'center',width: '88%',height:Height(5),backgroundColor:this.state.btnDoneDisable?'#8F8F90':'#EC4AB9',borderRadius:Width(6),justifyContent:'center',alignItems:'center' }} disabled={this.state.btnDoneDisable} onPress={() => this.onDoneClick()} >
                            {/* <ImageBackground source={this.state.btnDoneImage} style={{ width: '100%', height: Height(9), alignItems: 'center', justifyContent: 'center' }}> */}
                                <Text style={[{  fontSize: FontSize(19), fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Next</Text>
                            {/* </ImageBackground> */}
                        </TouchableOpacity>
                    </View>
                    <SafeAreaView />
                </BackgroundImage>
                {
                    Platform.OS == 'ios' ?
                    <DateTimePicker
                        isVisible={this.state.isDateTimePickerVisible}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._hideDateTimePicker}
                        mode='date'
                        date={this.state.bdayforpicker}
                        
                    />
                    :
                    null
                }
                
                
            {/* <DatePicker
              date={this.state.firstDate}
              mode="date"
              textSize={36}
              textColor='red'
              onDateChange={date => this.setState({ firstDate })}
              style={{ width: '100%' }}
            /> */}
            </Container>
        );
    }
}

export default Birthday;
