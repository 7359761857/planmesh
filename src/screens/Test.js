import React from 'react';
import { View , ScrollView , Text , Animated } from 'react-native';
Header_Max_Height = 120;
Header_Min_Height = 70;
export default class Test extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = { ScrollY : new  Animated.Value(0)}
    }
    render()
    {
        const headerHeight = this.state.ScrollY.interpolate({
            inputRange: [0,Header_Max_Height - Header_Min_Height],
            outputRange: [Header_Max_Height,Header_Min_Height],
            extrapolate: 'clamp',  
        })
        return(<View style={{flex: 1}}>
                  <Animated.View style={{ position: 'absolute',
                                  top: 0,
                                  left: 0,
                                  right: 0,
                                  bottom: 0,
                                  backgroundColor: 'red',
                                  height: headerHeight ,}}> 

                  </Animated.View>
                  <ScrollView style={{flex: 1}}
                  scrollEventThrottle={16}
                  onScroll={Animated.event([{nativeEvent: {contentOffset: {y: this.state.ScrollY}}}])}
                  >
                       <View>
                           <Text> Test Sticky </Text>
                       </View>
                       <Text> Test Sticky </Text>
                       <Text> Test Sticky </Text>
                       <Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text>
                       <Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text>
                       <Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text>
                       <Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text>
                       <Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text>
                       <Text> Test Sticky </Text>
                       <Text> Test Sticky </Text>
                       <Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text>
                       <Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text>
                       <Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text>
                       <Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text>
                       <Text> Test Sticky </Text><Text> Test Sticky </Text><Text> Test Sticky </Text>
                  </ScrollView>
        </View>); 
    } 
}