import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, Alert, ImageBackground, TouchableOpacity, StatusBar, Text, Platform, Animated, TouchableHighlight } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import { Container, View, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import { TabView } from 'react-native-tab-view';
import { FontSize, Height, Width, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import RNProgressHUB from 'react-native-progresshub';


import { store } from "../../redux/store";
import { API_ROOT } from "../../config/constant";
const message = require("../../../assets/images/message.png");

const backWhite = require("../../../assets/images/backWhite.png");
const friend_null = require("../../../assets/images/FriendList/friend_null.png");
const btnBgGrey = require("../../../assets/images/btnBg.png");
const chai_cup = require("../../../assets/images/Dashboard/chai_cup.png");
const location = require("../../../assets/images/Dashboard/location.png");
const calendar = require("../../../assets/images/Dashboard/calendar.png");
const flowers = require("../../../assets/images/Dashboard/avtar.png");
import { isIphoneX } from '../Default/is-iphone-x'
const HEADER_SIZE = isIphoneX() ? 30 : 17;
var subThis;
class Friend_Status extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {
      btnDoneDisable: true, btnDoneImage: btnBgGrey,
      index: this.props.navigation.state.params.tabIndex,
      goingList: [],
      inviteList: [],
      offset: 0,
      limit: 10,
      goingStatus: false, interestStatus: false, inviteStatus: false,
      interstedList: [],
      token: store.getState().auth.user.token,
      login_id: store.getState().auth.user.login_id,
      plan_id: this.props.navigation.state.params.plan_id,
      Title_New: this.props.navigation.state.params.Title_New,
      plan_type: this.props.navigation.state.params.plan_type,
      Title_New1: "",
      item:{},
      notificaitonid:this.props.navigation.state.params.notificaitonid,
      routes: [{ key: 'first', title: 'Going' }, { key: 'second', title: 'Interested' }, { key: 'third', title: 'Invited' }]
    };
    this.focusListener = this.props.navigation.addListener('didFocus', () => {
			this.didFocusComp()
		});
    subThis = this
  }
  componentDidMount() {

    if(this.state.plan_type == 1)
    {
      this.setState({ Title_New1: "Let's meet up!" }) 
    }
    else if(this.state.plan_type  == 2)
    {
      this.setState({ Title_New1: this.state.Title_New })
    }
    else if(this.state.plan_type  == 3)
    {
      this.setState({ Title_New1: "Wanna go to " +this.state.Title_New+"?" })
    }
    else if(this.state.plan_type  == 4)
    {
      this.setState({ Title_New1: "I'm free, wanna hangout?" })
    }
  }

  didFocusComp()
  {
    this.getPlanDetails()

    if(this.state.index == 0)
    {
        this.getGoingList()
    }
    else if(this.state.index == 1)
    {
        this.getInterestList()
    }
    else
    {
        this.getInviteList()
    }
    this.readNotification()
  }

  getGoingList = () => {
    RNProgressHUB.showSpinIndeterminate();
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('plan_id', this.state.plan_id)
    data.append('limit', this.state.limit)
    data.append('offset', this.state.offset)
    data.append('type', 'going')

    //alert(JSON.stringify(data))

    fetch(API_ROOT + 'plan/guest/list', {
      method: 'post',
      body: data
    }).then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          //alert(JSON.stringify(responseData))
          RNProgressHUB.dismiss();
          this.setState({ goingList: responseData.data })
        } else {
          //alert(JSON.stringify(responseData))
          RNProgressHUB.dismiss();
          this.setState({ goingStatus: false })
        }
      })
      .catch((error) => {
        RNProgressHUB.dismiss();
        //alert('Oops your connection seems off, Check your connection and try again')
      })
  }
  getInterestList = () => {
    RNProgressHUB.showSpinIndeterminate();
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('plan_id', this.state.plan_id)
    data.append('limit', this.state.limit)
    data.append('offset', this.state.offset)
    data.append('type', 'interested')
    fetch(API_ROOT + 'plan/guest/list', {
      method: 'post',
      body: data
    }).then((response) => response.json())
      .then((responseData) => {
        
        if (responseData.success) {
          RNProgressHUB.dismiss();
          this.setState({ interstedList: responseData.data })
        } else {
          RNProgressHUB.dismiss();
          this.setState({ interestStatus: false })
        }
      })
      .catch((error) => {
        RNProgressHUB.dismiss();

        //alert('Oops your connection seems off, Check your connection and try again')
      })
  }
  getInviteList = () => {
    RNProgressHUB.showSpinIndeterminate();
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('plan_id', this.state.plan_id)
    data.append('limit', this.state.limit)
    data.append('offset', this.state.offset)
    data.append('type', 'invited')

    //console.log('InvitedList Request: '+JSON.stringify(data))

    fetch(API_ROOT + 'plan/guest/list', {
      method: 'post',
      body: data
    }).then((response) => response.json())
      .then((responseData) => {

        //alert('GuestList: '+JSON.stringify(responseData))

        if (responseData.success) {
          RNProgressHUB.dismiss();
          this.setState({ inviteList: responseData.data })
        } else {
          RNProgressHUB.dismiss();
          this.setState({ inviteStatus: false })
        }
      })
      .catch((error) => {
        RNProgressHUB.dismiss();

        //alert('Oops your connection seems off, Check your connection and try again')
      })
  }

  readNotification = () => {

    RNProgressHUB.showSpinIndeterminate();
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('notification_id', this.state.notificaitonid)
    
    fetch(API_ROOT + 'notification/read', {
      method: 'post',
      body: data
    }).then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          RNProgressHUB.dismiss();
          
        } else {
          RNProgressHUB.dismiss();
          
        }
      })
      .catch((error) => {
        RNProgressHUB.dismiss();

        //alert('Oops your connection seems off, Check your connection and try again')
      })
  }

  getPlanDetails() {
		//alert(plan_id)

		//this.setState({ plan_id }) 
		const form = new FormData();
		form.append('token', this.state.token);
		form.append('login_id', this.state.login_id);
		form.append('plan_id', this.state.plan_id);

		fetch(API_ROOT + 'plan/details', {
			method: 'POST',
			body: form
		})
			.then(Resp => Resp.json())
			.then(resp => {
				// 523 

				//alert('Plan Details: '+JSON.stringify(resp))
				this.setState({item: resp.data})
			})
	}

  // static navigationOptions = ({ navigation }) => {
  //   theNavigation = navigation;
  //   return {
  //     header: (
  //       <Header style={styles.headerAndroidnav}>
  //         <StatusBar barStyle="light-content" backgroundColor="#41199B" />
  //         <Left style={{ alignItems: 'flex-start', justifyContent: 'flex-start', flex: 0.2}} >
  //           <Button transparent onPress={() => {
  //             theNavigation.goBack()
  //           }} style={{ width: 44, height: 44, marginLeft: 5 }} >
  //             <Image source={backWhite} style={{ tintColor: "white" }} />
  //           </Button>
  //         </Left>
  //         <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', width:'100%', zIndex:-1}}>
  //           <Text style={[styles.headerTitle]} >Guest List</Text>
  //         </View>
  //         <Right style={{ alignItems: 'flex-end', justifyContent: 'flex-end', flex: 0.25 }}>
  //           {/* <Button transparent style={{ alignSelf: 'baseline' }}  onPress={() => subThis.onDoneInClick()} ><Text style={{ color: 'white' }} >Done</Text></Button> */}
  //         </Right>
  //       </Header>
  //     )
  //   }
  // }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
        header: (
            <View style={styles.headerAndroidnav}>
                <StatusBar barStyle="light-content" backgroundColor="#41199B" />
                <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
                    <TouchableOpacity onPress={() => { navigation.goBack() }}>
                        <Image source={backWhite} style={{ tintColor: "white" }} />
                    </TouchableOpacity></View>
                <View style={{ bottom: Height(2), position: 'absolute', alignSelf: 'center' }}>
                    <Text style={styles.headerTitle} >Guest List</Text>
                </View>
                <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', right: Width(4) }}>
                    <TouchableOpacity>
                        <Text style={{ color: 'white' }} ></Text>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}


renderTopBar() {
  return (
    <View style={styles.headerAndroidnav}>
      <StatusBar barStyle="light-content" backgroundColor="#41199B" />
      <View style={{ flexDirection: 'row', position: 'absolute', left: Width(4), bottom: 0, top: HEADER_SIZE, alignItems: 'center', alignSelf: "center" }}>
        <TouchableOpacity style={{ alignSelf: "center" }} onPress={() => { this.props.navigation.goBack() }}>
          <Image source={backWhite} style={{ tintColor: "white" }} />
        </TouchableOpacity></View>
      <View style={{ bottom: Height(2), position: 'absolute', alignSelf: 'center', alignItems: "center" }}>
        <Text style={styles.headerTitle} >Guest List</Text>
        <View style={{ height: 2 }} />
        <Text style={styles.headerSubTitle} >{this.state.Title_New1}</Text>
      </View>
      <View style={{ flexDirection: 'row', position: 'absolute', right: Width(4), bottom: 0, top: HEADER_SIZE, alignItems: 'center', alignSelf: "center" }}>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('EventDetail', { item: this.state.item , plan_id: this.state.plan_id, other_id: '', plan_type: this.state.plan_type , title_header:this.state.Title_New1, Final_date : '', eventby:'',invitedList:[]}) } style={{ alignSelf: "center" }} >
          <Image source={message} style={{ tintColor: "white", height: 20, width: 20 }} />
        </TouchableOpacity>
      </View>

    </View>
  )
}


  tabViewIndexChange = (index: any) => {
    this.setState({ index });
  };

  goToFriendProfile(item)
  {
    //alert(JSON.stringify(item))
    AsyncStorage.setItem('other_id', item.login_id)
    this.props.navigation.navigate('ohter_user_profile')
  }

  renderScene = ({ route }) => {
    switch (route.key) {
      case 'first':
        return <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }}>
          <Toastt ref="toast"></Toastt>
          <BackgroundImage >
            <SafeAreaView />
            <Content>
            <View style={{ height: Height(1.5) }} />

              {this.state.goingList == '' ?
                <View style={{ alignSelf: 'center' }}>
                  {/* <Image style={{marginTop:Height(5),alignSelf:'center'}} source={friend_null}/>
                <View style={{height:Height(3)}}/>
                <Text style={{fontSize:26,fontFamily:fonts.Raleway_Bold,color:colors.appColor,textAlign:'center'}}>Nothing Here!</Text>
                <View style={{height:Height(1.5)}}/> */}
                </View> :
                <View>
                  {this.state.goingList.map(item => {
                    return (
                      <View style={{}}>
                        <TouchableOpacity
                          onPress = {() => this.goToFriendProfile(item)}

                          style={{ height: Height(5), flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white, marginBottom: '5%' }}>
                          <View style={{ width: Width(15), alignItems: 'center',marginLeft:Width(1) }}>
                            <TouchableHighlight onPress={() => { }} underlayColor="transparent"
                              style={{ height: Height(4.5), width: Height(4.5), borderRadius: Height(4.5), overflow: 'hidden' }}>
                              <Image style={{ height: '100%', width: '100%' }} source={item.image == '' ? flowers : { uri: item.image }} />
                            </TouchableHighlight>
                          </View>
                          <View style={{ width: Width(20), justifyContent: 'center', flex: 5 }}>
                            <Text style={{ fontSize: FontSize(16), color: colors.dargrey, fontFamily: fonts.Raleway_Bold }}> {item.name}</Text>
                          </View>

                        </TouchableOpacity>

                      </View>
                    );
                  })}
                  
                </View>}


            </Content>
            <SafeAreaView />
          </BackgroundImage>
        </Container>
      case 'second':
        return (
          <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }}>
            <Toastt ref="toast"></Toastt>
            <BackgroundImage >
              <SafeAreaView />
              <Content>
              <View style={{ height: Height(1.5) }} />

                {this.state.interstedList == '' ?
                  <View style={{ alignSelf: 'center' }}>
                    {/* <Image style={{marginTop:Height(5),alignSelf:'center'}} source={friend_null}/>
                  <View style={{height:Height(3)}}/>
                  <Text style={{fontSize:26,fontFamily:fonts.Raleway_Bold,color:colors.appColor,textAlign:'center'}}>Nothing Here!</Text>
                  <View style={{height:Height(1.5)}}/> */}
                  </View> :
                  <View>
                    {this.state.interstedList.map(item => {
                    return (
                      <View style={{}}>
                        <TouchableOpacity
                          onPress = {() => this.goToFriendProfile(item)}

                          style={{ height: Height(5), flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white, marginBottom: '5%' }}>
                          <View style={{ width: Width(15), alignItems: 'center',marginLeft:Width(1) }}>
                            <TouchableHighlight onPress={() => { }} underlayColor="transparent"
                              style={{ height: Height(4.5), width: Height(4.5), borderRadius: Height(4.5), overflow: 'hidden' }}>
                              <Image style={{ height: '100%', width: '100%' }} source={item.image == '' ? flowers : { uri: item.image }} />
                            </TouchableHighlight>
                          </View>
                          <View style={{ width: Width(20), justifyContent: 'center', flex: 5 }}>
                            <Text style={{ fontSize: FontSize(16), color: colors.dargrey, fontFamily: fonts.Raleway_Bold }}> {item.name}</Text>
                          </View>

                        </TouchableOpacity>

                      </View>
                    );
                  })}
                  </View>}


              </Content>
              <SafeAreaView />
            </BackgroundImage>
          </Container>
        )
      case 'third':
        return <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }}>
          <Toastt ref="toast"></Toastt>
          <BackgroundImage >
            <SafeAreaView />
            <Content>
            <View style={{ height: Height(1.5) }} />

              {this.state.inviteList == '' ?
                <View style={{ alignSelf: 'center' }}>
                  {/* <Image style={{marginTop:Height(5),alignSelf:'center'}} source={friend_null}/>
                <View style={{height:Height(3)}}/>
                <Text style={{fontSize:26,fontFamily:fonts.Raleway_Bold,color:colors.appColor,textAlign:'center'}}>Nothing Here!</Text>
                <View style={{height:Height(1.5)}}/> */}
                </View> :
                <View>
                 {this.state.inviteList.map(item => {
                    return (
                      <View style={{}}>
                        <TouchableOpacity
                          onPress = {() => this.goToFriendProfile(item)}
                          
                          style={{ height: Height(5), flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white, marginBottom: '5%' }}>
                          <View style={{ width: Width(15), alignItems: 'center',marginLeft:Width(1) }}>
                            <TouchableHighlight onPress={() => { }} underlayColor="transparent"
                              style={{ height: Height(4.5), width: Height(4.5), borderRadius: Height(4.5), overflow: 'hidden' }}>
                              <Image style={{ height: '100%', width: '100%' }} source={item.image == '' ? flowers : { uri: item.image }} />
                            </TouchableHighlight>
                          </View>
                          <View style={{ width: Width(20), justifyContent: 'center', flex: 5 }}>
                            <Text style={{ fontSize: FontSize(16), color: colors.dargrey, fontFamily: fonts.Raleway_Bold }}> {item.name}</Text>
                          </View>

                        </TouchableOpacity>

                      </View>
                    );
                  })}
                </View>}


            </Content>
            <SafeAreaView />
          </BackgroundImage>
        </Container>
      default:
        return null;
    }
  };

  onTabChange(idx)
  {

      this.setState({index:idx})

      if(idx == 0)
      {
          this.getGoingList()
      }
      else if(idx == 1)
      {
          this.getInterestList()
      }
      else
      {
          this.getInviteList()
      }
  }

  renderTabBar = (props) => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    return (
      <View style={styles.tabBar}>
        {props.navigationState.routes.map((route, i) => {
          const color = colors.appColor
          return (
            <TouchableOpacity
              key={i}
              style={[styles.tabItem, this.state.index === i ? styles.selectedTab : {}]}
              //onPress={() => this.setState({ index: i })}
              onPress={() => this.onTabChange(i)}
            >
              <View>
                <Animated.Text style={{ color, fontSize: FontSize(15), fontFamily: fonts.Raleway_Bold }}>{route.title}</Animated.Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };
  showActionSheet = () => {
    this.ActionSheet.show();
  };
  cancelEvent = (index) => {

  }

  render() {
    return (
      <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: colors.white }} >
        <Toastt ref="toast"></Toastt>
        {this.renderTopBar()}
        {/* <BackgroundImage > */}
          <SafeAreaView />
          <View style={{ position: 'absolute', bottom: 0, right: 0 }}>
          </View>
          <TabView
            swipeEnabled={false}
            navigationState={this.state}
            renderScene={this.renderScene}
            renderTabBar={this.renderTabBar}

            onIndexChange={(index: any) => this.tabViewIndexChange(index)}
          />
          <SafeAreaView />
        {/* </BackgroundImage> */}
      </Container>
    );
  }
}

export default Friend_Status;
