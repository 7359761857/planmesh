import React, { Component } from "react";
import { SafeAreaView, Keyboard, Image, ScrollView, ImageBackground, TouchableOpacity, StatusBar, Text,TouchableHighlight, TextInput } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Button, Header, Left, Item, Icon, Right, Content, Input } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import { getMyFriends } from "../../redux/actions/auth";
const backWhite = require("../../../assets/images/backWhite.png");
const SelectRadio = require("../../../assets/images/selectRadio.png");
const btnBg = require("../../../assets/images/btnBg.png");
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
import { store } from "../../redux/store";
const unSelectRadio = require("../../../assets/images/Dashboard/unSelectRadio.png");

import Toast from 'react-native-simple-toast';
import { tupleTypeAnnotation } from "@babel/types";
const flowers = require("../../../assets/images/Dashboard/avtar.png");

const search_img = require("../../../assets/images/Dashboard/search.png");
import RNProgressHUB from 'react-native-progresshub';

//var array = [],name=[]
class where5 extends ValidationComponent {
    constructor(props) {
        super(props);
        this.arrayholder = [];
        this.array = [];
        this.name = [];
        this.search = null;
        this.state = {
            value: null,
            friendName: '',
            selected_array:[],
            isActive:false,
            Friends: [],
            friend_id: this.props.navigation.state.params.friend_id,
            text: '',
            value:'',
            receiver_id:'',
            empty:true,
            token: store.getState().auth.user.token,
            login_id: store.getState().auth.user.login_id,     
           
        };
    }

        componentWillMount()
        {
            if(this.state.friend_id !=null){
                this.setState({selected_array:this.state.friend_id})
                this.array = this.state.friend_id
                }
        }

        componentDidMount(){
            //this.search.focus()
            
            RNProgressHUB.showSpinIndeterminate();
            let data = {token:this.state.token,login_id:this.state.login_id}
            getMyFriends(data).then(res=>{
                if(res.success){   
                    this.setState({Friends:res.data})
                    this.arrayholder = res.data;

                    this.arrayholder.map((item) => {
                        if(this.array.includes(item.receiver_id))
                        {
                            this.name.push(item.receiver_name)
                        }
                })

                    RNProgressHUB.dismiss();

                }
                    
                else{
                    this.setState({empty:false})   
                    RNProgressHUB.dismiss();
                 
                }
              }).catch(err=>{  
                RNProgressHUB.dismiss();
                
              })
              
        }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={styles.headerAndroidnav}>
                    <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack() }}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity></View>
                    <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                        <Text style={styles.headerTitle} >Invite friends</Text></View>
                </Header>
            )
        }
    }
    checkForFriends=(item)=>{
        if(this.state.selected_array.includes(item.receiver_id)){
            return true
        }else{
            return false
        }
    }
    goBack=()=>{

        //alert(this.name)

        var count = this.name.length-1

        AsyncStorage.setItem('where_friends',JSON.stringify(this.state.selected_array))

        if(this.name.length == 1)
        {
            AsyncStorage.setItem('where_friends_name', JSON.stringify(this.name))

        }else{
            AsyncStorage.setItem('where_friends_name', JSON.stringify(this.name[0]+' and '+count+' more'))

        }         
        this.props.navigation.goBack()
    }
    
    SearchFilterFunction(text) {
        //passing the inserted text in textinput
        const newData = this.arrayholder.filter((item) => {
          //applying filter for the inserted text in search bar
          const itemData = item.receiver_name ? item.receiver_name.toUpperCase() : ''.toUpperCase();
          const textData = text.toUpperCase();
          return itemData.indexOf(textData) > -1;
        });
        this.setState({
          //setting the filtered newData on datasource
          //After setting the data it will automatically re-render the view
          Friends: newData,
          text: text,
        });
      }

      _onMomentumScrollBegin = () => {
        Keyboard.dismiss()
    };

       render() {
        let value = ''
        str =''
        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }} >
                <Toastt ref="toast"></Toastt>
                <BackgroundImage >
                    <SafeAreaView />
                    <View style={{ height: Height(1), backgroundColor: colors.white }}></View>
                    <View style={{backgroundColor:'#fff'}}>
                    <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', margin: '1%' }}>
                                <View style={{ backgroundColor: colors.lightgrey, borderColor: colors.white, borderRadius: 22, height: Height(5.5), alignItems: 'center', width: Width(92.5), flexDirection:'row'}}>
                                    <Image source={search_img} style={{tintColor:'#B4B7BA', marginLeft: Width(4.5) }} />
                                    <TextInput
                                     onChangeText={text => this.SearchFilterFunction(text)}
                                     value={this.state.text}
                                        placeholderTextColor='#8F8F90'
                                        placeholder="Search Friends"
                                        ref={search => this.search = search}
                                        style={{ flex: 1, fontFamily: fonts.Raleway_Medium, fontSize: 16,paddingHorizontal:5, marginLeft: Width(2),marginRight:40 }} />
                                </View>
                            </View>
                            </View>
                    <SafeAreaView>
                        <ScrollView style={{ backgroundColor: '#fff', height:Height(67)}} keyboardDismissMode = 'on-drag'>
                            <View style={{ height: 1.3, backgroundColor: colors.lightgrey, marginTop: Height(1) }}></View>
                            <View style={{ height: Height(1.5) }} />
                            {this.state.empty == false ?
                            <Text style={{textAlign:'center',marginTop:Height(15),marginBottom:Height(15),alignItems:'center',justifyContent:'center',fontFamily:fonts.Raleway_Medium,FontSize:20,color:colors.fontDarkGrey}}> No Friends!</Text>:
                            <View style={{  }}>                               
                                {this.state.Friends.map((item,index) => {
                                    return (
                                        <View style={{}}>
                                            <TouchableOpacity                                              
                                            onPress={()=>{
                                                if (this.array.includes(item.receiver_id) == true) {
                                                    let item_index = this.array.indexOf(item.receiver_id)
                                                    let name_index = this.name.indexOf(item.receiver_name)
                                                    this.array.splice(item_index, 1)
                                                    this.name.splice(name_index, 1)
                                                } else {
                                                    this.array.push(item.receiver_id)
                                                    this.name.push(item.receiver_name)    
                                                }
                                                this.setState({selected_array: this.array })
                                                //alert(this.array)
                                            }}
                                            style={{ height: Height(6), flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white }}>
                                               <View style={{ width: Width(18), alignItems: 'center'}}>
                                                <TouchableHighlight onPress={() => { }} underlayColor="transparent" style={{ height: Height(5), width: Height(5), borderRadius: Height(5), overflow: 'hidden' }}>
                                                <Image style={{ height: '100%', width: '100%' }} source={item.receiver_photo == '' ? flowers:{ uri:item.receiver_photo }} />
                                                    </TouchableHighlight>
                                                </View>
                                                <View style={{ width: Width(20),justifyContent: 'center', flex: 4.5 }}>
                                                <Text style={{ fontSize: FontSize(15), color: colors.dargrey, fontFamily: fonts.Raleway_Bold }}> {item.receiver_name}</Text>
                                                </View>
                                                <View style={{ alignItems: 'flex-end', right: Width(4), flex: 2 }}>
                                                    <View                                                   
                                                        style={{
                                                            height: 28, 
                                                            width: 28,
                                                            alignItems: 'center',
                                                            justifyContent: 'center',
                                                        }} >
                                                            { this.checkForFriends(item) == true ?  <Image source={SelectRadio} style={{ height: 25, width: 25 }} />:
                                                             <Image source={unSelectRadio} style={{ height: 25, width: 25 }} />}
                                                    </View>
                                                </View>
                                            </TouchableOpacity>
                                            <View style={{ height: Height(1.5) }} />

                                        </View>
                                    );
                                })}
                            </View>}

                        </ScrollView>

                    </SafeAreaView>
                    {this.state.selected_array == '' ?
                        <View style={{ width: '103%', alignSelf: 'center' ,height:Height(15),bottom:0,position:'absolute'}}  >
                            <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Select</Text>
                            </ImageBackground>
                        </View>
                        : <TouchableOpacity style={{ width: '103%',  alignSelf: 'center',bottom:0,position:'absolute',height:Height(15)}} onPress={this.goBack} >
                            <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Select</Text>
                            </ImageBackground>
                        </TouchableOpacity>}

                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default where5;