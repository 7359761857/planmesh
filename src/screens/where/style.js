const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import { isIphoneX } from '../Default/is-iphone-x'
const HEADER_SIZE = isIphoneX() ? 84 : 64;
const HEADER_PADDING_SIZE = isIphoneX() ? 40 : 16;
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
export default {

    headerAndroidnav: {
        backgroundColor: '#41199B',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        ...Platform.select({
            ios: {
                height: HEADER_SIZE,
                paddingTop: HEADER_PADDING_SIZE,
            },
            android: {
            },
        }),
    },
    headerTitle: {
        letterSpacing: 0.02,
        fontSize: 20,
        fontFamily: "Raleway-Medium",
        color: 'white',
        ...Platform.select({
            ios: {
              fontWeight: '500',
            },
            android: {
            },
          })
    },
    labelInput: {
        color: '#363169',
        fontSize: 15,
        fontFamily: "Roboto-Bold",
        paddingLeft:0
    },
    formInput: {
        color: '#3a4759',
        fontSize: 16,
        fontFamily: "Roboto-Regular",
        marginLeft:Width(6),
       
        
    },
    formInputFocus:{
        borderBottomWidth: 2,
        borderColor: '#3A4759',
        paddingLeft:0,
    },
    formInputUnFocus:{
        borderBottomWidth: 2,
        borderColor: '#BBC1C8',
    },
    labelInputUnFocus:{
        color: '#8f8f90',
        fontSize: 16,
        fontFamily: "Roboto-Regular",
        paddingLeft:0
    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        fontFamily: "Roboto-Regular",

    },
    labelInputError: {
        color:'red'
    },
    subAcitvityColor:{
        marginVertical: Height(2), 
        marginLeft: Width(4),
        color: colors.whatFontColor, 
        fontSize: FontSize(16),
        fontFamily: fonts.Raleway_Medium
    },
    AcitvityColor:{
        marginVertical: Height(1),
        color: '#5A3AA9',
        fontSize: FontSize(18),
        fontFamily: fonts.Roboto_Bold
    },
    imgsearch:{
        marginLeft:Width(5),
        marginTop:Height(2)
    },
    imgsearch1:{
        marginLeft:Width(5),
        marginTop:Height(3)
    },
    textColor:{
        position: 'absolute', right: Width(4),color:colors.pink,fontSize: FontSize(16)},
    textUnColor:{
        position: 'absolute', right: Width(4),color:colors.black,fontSize: FontSize(16)

    },   
};
