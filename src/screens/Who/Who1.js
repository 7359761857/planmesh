import React, { Component } from "react";
import { SafeAreaView, TextInput, Image, Platform, ImageBackground, TouchableOpacity, StatusBar, Text, ScrollView, Keyboard,PermissionsAndroid } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Button, Header, Left, Item, Icon, Right, Content, Input } from "native-base";
import styles from "./style";
import { getMyFriends } from "../../redux/actions/auth";
import ValidationComponent from 'react-native-form-validator';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import Contacts from 'react-native-contacts';
import RNProgressHUB from 'react-native-progresshub';

import { TouchableHighlight } from "react-native-gesture-handler";
const backWhite = require("../../../assets/images/backWhite.png");
const SelectRadio = require("../../../assets/images/selectRadio.png");
const btnBg = require("../../../assets/images/btnBg.png");
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
import { API_ROOT } from "../../config/constant";
import { FRIENDLIST } from "../../redux/actions/types";
import { store } from "../../redux/store";
const flowers = require("../../../assets/images/Dashboard/avtar.png");

const search_img = require("../../../assets/images/Dashboard/search.png");
const btn_invite_friends = require("../../../assets/images/Dashboard/btn_invite_friends.png");


class Who1 extends ValidationComponent {

    constructor(props) {
        super(props);
            this.arrayholder = [];
        this.search = null;
        this.state = {
            value: null,
            text: '',
            friendName:'',   
            token: store.getState().auth.user.token,
            login_id: store.getState().auth.user.login_id,     
            Friends: [],
            empty:true,
            is_edit_plan: this.props.navigation.state.params.is_edit_plan,
            selectedfriends:[],
            selectedfriendNames:[],
            invitedList: this.props.navigation.state.params.is_edit_plan == true ? this.props.navigation.state.params.invitedList : [],
            inviteList:[],
            plan_id:this.props.navigation.state.params.plan_id,
        };
        subThis = this
    }
    componentDidMount(){  

        //this.search.focus()

        if(this.state.is_edit_plan == true)
        {
            //this.setState({value:this.props.navigation.state.params.receiver_id})
            //alert(this.props.navigation.state.params.invitedList)
            this.setState({invitedList:this.props.navigation.state.params.invitedList})
            var arrIds = []
            var arrNames = []
            this.state.invitedList.map( (item) => {
                arrIds.push(item.receiver_id)
                arrNames.push(item.receiver_name)
            })

            this.setState({selectedfriendNames:arrNames, selectedfriends:arrIds})
            //alert(this.state.selectedfriends + ' and '+this.state.selectedfriendNames)
        }

        //RNProgressHUB.showSpinIndeterminate();     
        let data = {token:this.state.token,login_id:this.state.login_id}

        getMyFriends(data).then(res=>{
            if(res.success){
                this.setState({Friends:res.data})
                this.arrayholder = res.data;
                //RNProgressHUB.dismiss();

            }
            else{
                this.setState({empty:false})
                //RNProgressHUB.dismiss();

            }
          }).catch(err=>{
            //RNProgressHUB.dismiss();
            //alert('Oops your connection seems off, Check your connection and try again')
          })
          this._navListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBarStyle('light-content');
            Platform.OS == 'android' && StatusBar.setBackgroundColor('#41199B');
      
          });       

          setTimeout(function () {
            if (Platform.OS === 'android') {
                // PermissionsAndroid.request(
                //     PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                //     {
                //         'title': 'Contacts',
                //         'message': 'This app would like to view your contacts.'
                //     }
                // ).then(() => {
                //     subThis.loadContacts();
                // })

                PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                    {
                        title: 'Planmesh',
                        message: 'Planmesh syncs only phone numbers from your address book to Planmesh servers to help you connect with other Planmesh users.',
                        buttonPositive: "OK"
                    }
                ).then((results) => {
          
                    if (results === PermissionsAndroid.RESULTS.GRANTED)
                    {
                        subThis.loadContacts();
                    } 
                })

            } else {
                subThis.loadContacts();
            }
        }, 200);

      }
      componentWillUnmount() {
        this._navListener.remove();
    }
    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={[styles.headerAndroidnav,{height:44}]}>
                    <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack()}}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity></View>
                    <View style={{bottom: Height(2), position: 'absolute',alignItems:'center'}}>
                        <Text style={styles.headerTitle} >Who do you wanna see?</Text></View>
                </Header>
            )
        }
    }

    loadContacts() {
        

        Contacts.getAll().then(contacts => {
            // contacts returned
            //RNProgressHUB.showSpinIndeterminate();
            let newData = []

                let form = new FormData()
                let str = "";
                contacts.map((contactData) => {

                    if (contactData.givenName != null) {
                        const words = contactData.givenName.split(' ')
                        var initial = ""
                        var seactionStr = ""
                        if (words.length >= 1) {
                            initial = words[0].charAt(0)
                            seactionStr = words[0].charAt(0)
                        }
                        if (words.length >= 2) {
                            initial = initial + words[1].charAt(0)
                        }
                        let dict = { key: contactData.rawContactId, select: false, title: ((contactData.givenName) + ' ' + (contactData.familyName != null ? contactData.familyName : '')), initial: initial.toUpperCase(), sectionStr: seactionStr.toUpperCase(), phoneNumbers: contactData.phoneNumbers }
                        contactData.phoneNumbers ? contactData.phoneNumbers.map((mob1) => {
                            str = str + mob1.number + ',';
                            return mob1.number
                        }) : ''
                        newData.push(dict)
                        //console.log('Call Log', str)

                    }
                });
                form.append('login_id', this.state.login_id)
                form.append('mobile_number', str)

                if(this.state.plan_id != null && this.state.plan_id != '')
                {
                    form.append('plan_id', this.state.plan_id)
                }

                fetch(API_ROOT + 'get_friends',
                    {
                        body: form,
                        method: "post"
                    }).then(res => res.json()).then(resJson => {
                        console.log('Call 2222', resJson)
                        if (resJson.success) {
                            let invi = resJson.invite_friend_list;

                            let send = resJson.friend_request_list;
                            let invi_list = this.invi_friends(newData, invi)
                            //console.log("inv",invi_list)
                            setTimeout(() => {this.setState({inviteList: invi_list })}, 5000)
                            //this.setState({inviteList: invi_list });
                            //RNProgressHUB.dismiss();
                            //alert(JSON.stringify(this.state.inviteList))
                            if (send_list.length == 0) {
                                // this.inviteFriends()
                            }
                        }
                        else {

                            //RNProgressHUB.dismiss();
                        }
                    }).catch(err => {
                        //RNProgressHUB.dismiss();
                       // alert('Oops your connection seems off, Check your connection and try again')
                    });

          })

      /*  Contacts.getAll((err, contacts) => {
            console.log(contacts)
            console.log(err)
            if (err === 'denied') {
                console.warn('Permission to access contacts was denied');
                //RNProgressHUB.dismiss();
            } else {
                let newData = []

                let form = new FormData()
                let str = "";
                contacts.map((contactData) => {

                    if (contactData.givenName != null) {
                        const words = contactData.givenName.split(' ')
                        var initial = ""
                        var seactionStr = ""
                        if (words.length >= 1) {
                            initial = words[0].charAt(0)
                            seactionStr = words[0].charAt(0)
                        }
                        if (words.length >= 2) {
                            initial = initial + words[1].charAt(0)
                        }
                        let dict = { key: contactData.rawContactId, select: false, title: ((contactData.givenName) + ' ' + (contactData.familyName != null ? contactData.familyName : '')), initial: initial.toUpperCase(), sectionStr: seactionStr.toUpperCase(), phoneNumbers: contactData.phoneNumbers }
                        contactData.phoneNumbers ? contactData.phoneNumbers.map((mob1) => {
                            str = str + mob1.number + ',';
                            return mob1.number
                        }) : ''
                        newData.push(dict)
                        //console.log('Call Log', str)

                    }
                });
                form.append('login_id', this.state.login_id)
                form.append('mobile_number', str)
                fetch(API_ROOT + 'get_friends',
                    {
                        body: form,
                        method: "post"
                    }).then(res => res.json()).then(resJson => {
                        console.log('Call 2222', resJson)
                        if (resJson.success) {
                            let invi = resJson.invite_friend_list;

                            let send = resJson.friend_request_list;
                            let invi_list = this.invi_friends(newData, invi)
                            //console.log("inv",invi_list)
                            
                            this.setState({inviteList: invi_list });
                            //RNProgressHUB.dismiss();
                            //alert(JSON.stringify(this.state.inviteList))
                            if (send_list.length == 0) {
                                // this.inviteFriends()
                            }
                        }
                        else {

                            //RNProgressHUB.dismiss();
                        }
                    }).catch(err => {
                        //RNProgressHUB.dismiss();
                    });
            }
        })
        */
    }

goNext=()=>{

    //AsyncStorage.setItem('receiver_id',this.state.value)

    AsyncStorage.setItem('receiver_id',String(this.state.selectedfriends))

    if(this.state.is_edit_plan == true)
    {
        //this.props.navigation.navigate('Who2',{friendName:this.state.friendName, is_edit_plan:true})
        //alert(this.state.invitedList)
        //return
        var invitees = []
        this.state.selectedfriends.map ( (item, index) => {
            let inviteInfo = {'receiver_id':item,'receiver_name':this.state.selectedfriendNames[index]}
            invitees.push(inviteInfo)
        })
        
        if(this.state.selectedfriendNames.length > 1)
        {
            this.props.navigation.navigate('Who2',{friendName:this.state.selectedfriendNames[0]+' and '+ (this.state.selectedfriendNames.length - 1)+' More', is_edit_plan:true, invitedList:invitees})
        }
        else{
            this.props.navigation.navigate('Who2',{friendName:String(this.state.selectedfriendNames), is_edit_plan:true,invitedList:invitees})
        }
        
    }
    else
    {
        //this.props.navigation.navigate('Who2',{friendName:this.state.friendName, is_edit_plan:this.state.is_edit_plan})
        if(this.state.selectedfriendNames.length > 1)
        {
            //alert(this.state.selectedfriendNames[0]+' and '+ (this.state.selectedfriendNames.length - 1)+' More')
            //return
            this.props.navigation.navigate('Who2',{friendName:this.state.selectedfriendNames[0]+' and '+ (this.state.selectedfriendNames.length - 1)+' More', is_edit_plan:this.state.is_edit_plan})
        }
        else
        {
            this.props.navigation.navigate('Who2',{friendName:String(this.state.selectedfriendNames), is_edit_plan:this.state.is_edit_plan})
        }
        
    }
}

SearchFilterFunction(text) {
    //passing the inserted text in textinput
    const newData = this.arrayholder.filter((item) => {
      //applying filter for the inserted text in search bar
      const itemData = item.receiver_name ? item.receiver_name.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      //setting the filtered newData on datasource
      //After setting the data it will automatically re-render the view
      Friends: newData,
      text: text,
    });
  }

  _onMomentumScrollBegin = () => {
    Keyboard.dismiss()
};

    onFriendSelected(item)
    {
        //alert(item.receiver_name)

        //console.log('Before: '+this.state.selectedfriends)

        var list = this.state.selectedfriends
        var listName = this.state.selectedfriendNames

        if(this.state.selectedfriends.includes(item.receiver_id))
        {
            let indx = list.indexOf(item.receiver_id)
            list.splice(indx,1)
            listName.splice(indx,1)
            this.setState({selectedfriends:list, selectedfriendNames:listName})
        }
        else
        {
            list.push(item.receiver_id)
            listName.push(item.receiver_name)
            this.setState({selectedfriends:list, selectedfriendNames:listName})
        }

        //alert('After: '+this.state.selectedfriends)
    }

    invi_friends = (newData, invi) => {

        var data = []
        for(item of newData)
        {
            var number = ''
            if(item.phoneNumbers != undefined && item.phoneNumbers != null && item.phoneNumbers.length > 0 && item.phoneNumbers && item.phoneNumbers[0].number)
            {
                number = item.phoneNumbers[0].number
            }
            
            for(invtcnct of invi){
                if(invtcnct.number == number)
                {
                    item.is_invited = invtcnct.is_invited
                    if(this.state.selectedcontacts != null && this.state.selectedcontacts.length > 0)
                    {
                        if(this.state.selectedcontacts.includes(number))
                        {
                            item.select = true
                        }
                        else
                        {
                            item.select = false
                        }
                    }

                    data.push(item)
                }
            }
        }
        
        return data

        // let data = newData.filter(fruit => fruit.phoneNumbers.some(m => invi.includes(m.number)))
        // return data
    }

    inviteFriends = () => {
        let selectedArray = this.state.inviteList
        this.props.navigation.push('sendFriendRequest', {
            selectedArray: selectedArray.sort((a, b) => a.sectionStr > b.sectionStr)
        })
    }

    render() {
        const { value } = this.state;

        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }} >
                <Toastt ref="toast"></Toastt>
                <BackgroundImage >
                    <SafeAreaView />
                    <View style={{ height: Height(1), backgroundColor: colors.white }}></View>
                    <View style={{backgroundColor:'#fff'}}>
                    <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', margin: '1%' }}>
                                <View style={{ backgroundColor: colors.lightgrey, borderColor: colors.white, borderRadius: 22, height: Height(5.5), alignItems: 'center', width: Width(92.5), flexDirection:'row'}}>
                                    <Image source={search_img} style={{tintColor:'#B4B7BA', marginLeft: Width(4.5) }} />
                                    <TextInput
                                     onChangeText={text => this.SearchFilterFunction(text)}
                                        value={this.state.text}
                                        placeholderTextColor='#8F8F90'
                                        placeholder="Search Friends"
                                        returnKeyType = "done"
                                        ref={search => this.search = search}
                                        style={{ flex: 1, fontFamily: fonts.Raleway_Medium,paddingHorizontal:5, fontSize: 16,paddingHorizontal:5, marginLeft: Width(2),marginRight:40}} />
                                </View>
                    </View>

                    <View style={{ height: 1.3, backgroundColor: colors.lightgrey, marginTop: Height(1) }}></View>
                    <View style={{ height: Height(1.5) }} />

                    </View>
                    <SafeAreaView>

                        {
                            this.state.Friends != null && this.state.Friends.length > 0 ?

                            <ScrollView style={{ backgroundColor: '#fff', height:Height(65) }} keyboardDismissMode = 'on-drag'>
                            {this.state.empty == false ?
                            <Text style={{textAlign:'center',marginTop:Height(15),marginBottom:Height(15),alignItems:'center',justifyContent:'center',fontFamily:fonts.Raleway_Medium,FontSize:20,color:colors.fontDarkGrey}}> No Friends!</Text>:
                            <View style={{}}>
                                {this.state.Friends.map(item => {
                                    return (
                                        <View style={{}}>
                                            <TouchableOpacity  onPress={() => {
                                                            this.onFriendSelected(item)
                                                            // this.setState({
                                                            //     value: item.receiver_id,
                                                            //     friendName:item.receiver_name
                                                            // });
                                                         
                                                        }} style={{ height: Height(5), flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white,marginBottom:'3%' }}>
                                                <View style={{ width: Width(15), alignItems: 'center', marginLeft:Width(1)}}>
                                                <TouchableHighlight onPress={() => { }} underlayColor="transparent" 
                                                style={{ height: Height(4.5), width: Height(4.5), borderRadius: Height(4.5), overflow: 'hidden' }}>
                                                    <Image style={{ height: '100%', width: '100%' }} source={item.receiver_photo == '' ? flowers:{ uri:item.receiver_photo }} />
                                                    </TouchableHighlight>
                                                </View>
                                                <View style={{ width: Width(20),justifyContent:'center',flex:4.5, marginLeft:-3}}>
                                                    <Text style={{ fontSize: FontSize(16), color: colors.dargrey, fontFamily: fonts.Roboto_Medium }}> {item.receiver_name}</Text>
                                                </View>
                                                <View style={{ alignItems: 'flex-end', right: Width(4), flex: 2 }}>
                                                    <View
                                                        style={{
                                                            height: 25,
                                                            width: 25,
                                                            borderRadius: 100,
                                                            borderWidth: 2,
                                                            borderColor: colors.dargrey,
                                                            alignItems: 'center',
                                                            justifyContent: 'center',
                                                        }} >
                                                        {/* {value === item.receiver_id && <View> */}
                                                        {this.state.selectedfriends.includes(item.receiver_id) && <View>
                                                            <Image source={SelectRadio} style={{ height: 30, width: 30 }} />
                                                        </View>}
                                                    </View>
                                                </View>
                                            </TouchableOpacity>                                           

                                        </View>
                                    );
                                })}
                            </View>}

                        </ScrollView>


                            :
                            <View style = {{alignItems:'center', justifyContent:'center', marginTop:80}}>

                                <Text style={{width:Width(45),fontFamily:fonts.Raleway_Bold,fontSize:FontSize(18), color:'#41199B', textAlign:'center'}}>Planmesh is better with friends</Text>
                                <Text style={{width:Width(45),fontFamily:fonts.Roboto_Regular,fontSize:FontSize(15), color:'#3A4759', marginTop:10, textAlign:'center', marginBottom:10}}>Invite your contacts and start making plans.</Text>
                                <TouchableOpacity onPress = { this.inviteFriends}>
                                    <Image source = {btn_invite_friends} resizeMode = 'contain'></Image>
                                </TouchableOpacity>

                            </View>
                        }

                        

                    </SafeAreaView>
                    


                    {
                    
                    this.state.Friends != null && this.state.Friends.length > 0 ?

                        this.state.selectedfriends.length == 0 ?
                        <View style={{width: '103%',bottom:0,position:'absolute', alignSelf: 'center',height:Height(15)}}  >
                            <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Select</Text>
                            </ImageBackground>
                        </View>
                        :<TouchableOpacity style={{ width: '103%',bottom:0,position:'absolute',  alignSelf: 'center',height:Height(15) }} onPress={this.goNext} >
                            <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Select</Text>
                            </ImageBackground>
                        </TouchableOpacity>

                    : 
                    null

                    }
                    
                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default Who1;