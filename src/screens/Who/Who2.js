import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Alert, ImageBackground, BackHandler, TouchableOpacity, StatusBar, TouchableHighlight, Text, Switch, TextInput } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import { Container, View, Button, Header, Left, Right, Content, Item, Body } from "native-base";
import styles from "./style";
import HandleBack from "../Default/HandleBack";//../../screens/Default/HandleBack
// import EmojiPicker from 'react-native-simple-emoji-picker';
import ValidationComponent from 'react-native-form-validator';
import AntDesign from 'react-native-vector-icons/AntDesign';
import ImagePicker from 'react-native-image-crop-picker';
import RNProgressHUB from 'react-native-progresshub';

import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
const backWhite = require("../../../assets/images/backWhite.png");
const btnBgGrey = require("../../../assets/images/btnBg.png");
const cup = require("../../../assets/images/Dashboard/full_cup.png");
const Location_new = require("../../../assets/images/Dashboard/full_Location.png");
const time = require("../../../assets/images/Dashboard/full_time.png");
const unSelectRadio = require("../../../assets/images/Dashboard/unSelectRadio.png");
const SelectRadio = require("../../../assets/images/selectRadio.png");
const bg = require("../../../assets/images/Dashboard/bg.png");
const ic_add_cover = require("../../../assets/images/Dashboard/ic_add_cover_photo.png");
const ic_edit_cover = require("../../../assets/images/Dashboard/ic_edit_cover.png");
const ic_edit_event = require("../../../assets/images/Dashboard/ic_edit_event.png");
const default_placeholder = require("../../../assets/images/Dashboard/default_placeholder.png");
import MixpanelManager from '../../../Analytics'

import { store } from "../../redux/store";
import { API_ROOT } from "../../config/constant";
import moment from 'moment';
var activity = ''
class Who2 extends ValidationComponent {
    _didFocusSubscription
    constructor(props) {
        super(props);
        this.state = {
            value: true,
            activity: 'Add an activity',
            location: '',
            start_date: '',
            end_date: '',
            current_time: '',
            description: '',
            is_guest: 0,
            token: store.getState().auth.user.token,
            login_id: store.getState().auth.user.login_id,
            receiver_id: '',
            full_address: '',
            is_now: '0',
            is_allday:'0',
            is_edit_plan: this.props.navigation.state.params.is_edit_plan,
            item: '',
            plan_id:'',
            invitedList:this.props.navigation.state.params.is_edit_plan == true ? this.props.navigation.state.params.invitedList : [],
            photo_path:''
        }
        this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
        this._didFocusSubscription = props.navigation.addListener('didFocus', payload => {
            this.retriveData()
        }
        )
    }


    async retriveData() {


        //alert(1)

        if(this.state.is_edit_plan == true)
        {
            this.setState(
                {   item:this.props.navigation.state.params.item,
                    value:this.props.navigation.state.params.item.is_guest == 1 ? true : false,
                    full_address:this.props.navigation.state.params.item.line_2,
                    location:this.props.navigation.state.params.item.line_1,
                    description:this.props.navigation.state.params.item.description,
                    plan_id:this.props.navigation.state.params.item.plan_id,
                    is_now:this.props.navigation.state.params.item.is_now,
                    is_allday:this.props.navigation.state.params.item.is_allday,
                    invitedList:this.props.navigation.state.params.invitedList,
                    photo_path:this.props.navigation.state.params.item.plan_image
                }
                )
        }

        await AsyncStorage.getItem('receiver_id').then((value) => {
            this.setState({ receiver_id: value })
        });
        await AsyncStorage.getItem('Activity').then((value) => {
            this.setState({ activity: value })
        });
        await AsyncStorage.getItem('Location').then((value) => {
            this.setState({ location: value })
        });
        await AsyncStorage.getItem('FullAddress').then((value) => {
            this.setState({ full_address: value })
        });
        await AsyncStorage.getItem('CurrentDate').then((value) => {

            if(value == null || value == '')
            {
                this.setState({ current_time: null })
            }
            else
            {
                this.setState({ current_time: value })
            }
            
        });
        await AsyncStorage.getItem('start').then((value) => {
            
            if(value == null || value == '')
            {
                this.setState({ start_date: null })
            }
            else
            {
                this.setState({ start_date: value })
            }
        });
        await AsyncStorage.getItem('end').then((value) => {
            
            if(value == null || value == '')
            {
                this.setState({ end_date: null })
            }
            else
            {
                this.setState({ end_date: value })
            }
        });
        await AsyncStorage.getItem('is_now').then((value) => {
            this.setState({ is_now: value })
        });
        await AsyncStorage.getItem('is_allday').then((value) => {
            this.setState({ is_allday: value })
        });

        await AsyncStorage.getItem('who_desc').then((value) => {
            if(value == null)
            {
                this.setState({ description: '' })
            }
            else
            {
                this.setState({ description: value })
            }
            
            
        });

        //alert(this.state.description)

    }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={[styles.headerAndroidnav,{height:44}]}>
                    <StatusBar barStyle="light-content" backgroundColor="#41199B" />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                        <TouchableOpacity onPress={() => {
                            AsyncStorage.removeItem('Activity')
                            AsyncStorage.removeItem('Location')
                            AsyncStorage.removeItem('CurrentDate')
                            AsyncStorage.removeItem('start')
                            AsyncStorage.removeItem('end')
                            AsyncStorage.removeItem('who_desc')

                            navigation.goBack()
                        }}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity></View>
                    <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                        {theNavigation.state.params.is_edit_plan == true ? <Text style={styles.headerTitle} >Edit Event</Text> : <Text style={styles.headerTitle} >Review Event</Text>}
                    </View>

                </Header>
            )
        }
    }
    addPlan = () => {

        var is_guest = 0
        if (this.state.value == true) {
            is_guest = 1
        } else {
            is_guest = 0
        }

        //alert(this.state.is_guest+' '+this.state.value)
        //return

        RNProgressHUB.showSpinIndeterminate()

        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('friend_id', this.state.receiver_id)
        data.append('plan_type', 1)
        data.append('description', this.state.description == null ? '' : this.state.description)

        if (this.state.is_now == '1') {
            data.append('is_now', 1)
        } else {
            data.append('is_now', 0)
        }
        data.append('share_with', 3)
        data.append('is_guest', is_guest)
        data.append('activity', this.state.activity)
        data.append('add_line_1', this.state.location)
        data.append('add_line_2', this.state.full_address)
        data.append('pin_code', '')
        data.append('city', '')
        data.append('state', '')
        data.append('country', '')
        data.append('start_date', this.state.start_date != null ? moment.utc(new Date(this.state.start_date)).format('LLL') : '')
        data.append('end_date', this.state.end_date != null ? moment.utc(new Date(this.state.end_date)).format('LLL') : '')
        data.append('is_publish', 1)
        data.append('is_allday', this.state.is_allday)

        if(this.state.photo_path == '' || this.state.photo_path == null || this.state.photo_path == undefined)
        {
            data.append("plan_image",'');
        }
        else
        {
            data.append("plan_image", {
                name: 'event.jpg',
                type: 'image/jpg',
                uri:
                  Platform.OS === "android" ? this.state.photo_path : this.state.photo_path.replace("file://", "")
              });
        }

        
        // alert(JSON.stringify(data))
        // return
        const MixpanelData = {
            'Friends Selected' : this.state.friends != null && this.state.friends.length > 0 ? this.state.friends.toString() : '',
            '$email' : store.getState().auth.user.email_address,
            'Activity': this.state.activity,
            // ActivitySelected: this.state.activity,
            'Location': this.state.full_address,
            'Plan Type': 'Who',
            'Description': this.state.description == null ? '' : this.state.description,
            'Cover Photo' : this.state.photo_path == '' || this.state.photo_path == null || this.state.photo_path == undefined ? 'False' : 'True',
            'Date & Time' : this.state.current_time,
            'Plan Name' : `I wanna see ${this.props.navigation.state.params.friendName}`,
            'Guest Can Invite' : is_guest == 1 ? 'True' : 'False',
            'Friends Invited': '1'
        }
        if((this.state.friends == null || this.state.friends == '') && (this.state.contacts == null || this.state.contacts == '')) {
            MixpanelData.Privacy = this.state.share_with == 0 ? 'Only Me' : this.state.share_with == 2 ? 'Friends of Freinds'  : this.state.share_with ==  1 ? 'Friends' : ''
        }else {
            MixpanelData.Privacy = ''
        }

        fetch(API_ROOT + 'plan/create', {
            method: 'post',
            body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                RNProgressHUB.dismiss()
                if (responseData.success) {
                    this.clearData()
                    this.props.navigation.navigate('dashboard', { refreshing: true })
                    this.mixpanel.identify(store.getState().auth.user.email_address);
                    this.mixpanel.getPeople().increment("Lifetime Plan Post", 1);
                    this.mixpanel.getPeople().set("Last Plan Post", moment().format('MMMM Do YYYY, h:mm:ss a'));
                    this.mixpanel.track('Plan Post', MixpanelData);
                    this.mixpanel.getPeople().setOnce({ 'First Plan Post': moment().format('MMMM Do YYYY, h:mm:ss a') });
                    // this.mixpanel.registerSuperPropertiesOnce({ 'First Plan Post': moment().format('MMMM Do YYYY, h:mm:ss a') });
                    //alert(responseData.text)
                } else {
                    alert(responseData.text)
                }
            })
            .catch((error) => {
                RNProgressHUB.dismiss()
                //alert('Oops your connection seems off, Check your connection and try again')
            })

        // setTimeout( () => {
        // this.props.navigation.navigate('dashboard', { refreshing: true })
        // },3000);

    }

    editPlan = () => {
        var is_guest = 0
        if (this.state.value == true) {
            is_guest = 1
        } else {
            is_guest = 0
        }

        RNProgressHUB.showSpinIndeterminate()

        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('friend_id', this.state.receiver_id)
        data.append('plan_type', 1)
        data.append('description', this.state.description)
        data.append('plan_id', this.state.plan_id)

        if (this.state.is_now == '1') {
            data.append('is_now', 1)
        } else {
            data.append('is_now', 0)
        }
        data.append('share_with', 3)
        data.append('is_guest', is_guest)
        data.append('activity', this.state.activity)
        data.append('add_line_1', this.state.location)
        data.append('add_line_2', this.state.full_address)
        data.append('pin_code', '')
        data.append('city', '')
        data.append('state', '')
        data.append('country', '')
        data.append('start_date', this.state.start_date != null ? moment.utc(new Date(this.state.start_date)).format('LLL') : '')
        data.append('end_date', this.state.end_date != null ? moment.utc(new Date(this.state.end_date)).format('LLL') : '')
        data.append('is_publish', 1)
        data.append('is_allday', this.state.is_allday)

        if(this.state.photo_path == '' || this.state.photo_path == null || this.state.photo_path == undefined)
        {
            data.append("plan_image",'');
        }
        else
        {
            data.append("plan_image", {
                name: 'event.jpg',
                type: 'image/jpg',
                uri:
                  Platform.OS === "android" ? this.state.photo_path : this.state.photo_path.replace("file://", "")
              });
        }
        //console.log('Edit Plan Request: '+JSON.stringify(data))


        fetch(API_ROOT + 'plan/edit', {
            method: 'post',
            body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                RNProgressHUB.dismiss()
                if (responseData.success) {
                    this.clearData()
                    this.props.navigation.goBack()

                    const MixpanelData = {
                        '$email' : store.getState().auth.user.email_address,
                        'Post ID' : this.state.plan_id
                        }
                        this.mixpanel.identify(store.getState().auth.user.email_address);
                        this.mixpanel.track('Post Edit', MixpanelData);
                    //alert(responseData.text)
                } else {
                    alert(responseData.text)
                }
            })
            .catch((error) => {
                RNProgressHUB.dismiss()
                //alert('Oops your connection seems off, Check your connection and try again')
            })

            // setTimeout( () => {
            //     this.props.navigation.navigate('dashboard', { refreshing: true })
            //     },3000);
    }

    clearData = () => {
        AsyncStorage.removeItem('Activity')
        AsyncStorage.removeItem('Location')
        AsyncStorage.removeItem('CurrentDate')
        AsyncStorage.removeItem('start')
        AsyncStorage.removeItem('end')
        AsyncStorage.removeItem('who_desc')
    }
    goBack = () => {

        if(this.state.is_edit_plan == true)
        {
            //alert(this.state.invitedList)
            this.props.navigation.navigate('Who1',{receiver_id:this.state.receiver_id, is_edit_plan:true, invitedList:this.state.invitedList,plan_id:this.state.plan_id})
        }
        else
        {
            this.clearData()
            this.props.navigation.goBack()
        }
    }

    onPressCoverPhoto()
    {
        ImagePicker.openPicker({
            width:Width(100),
            height:265,
            cropping: true
          }).then(image => {
            //console.log(image);
            //alert(image.path)
            this.setState({photo_path:image.path})
          });
    }

    render() {
        var str = String(this.state.activity);
        if (str.includes('Wanna')) {
            activity = str.substr(6).slice(0);
        } else {
            activity = str
        }
        if (activity.length -1 == '?') {
            activity = activity.slice(0, -1)
        }
        return (
            <HandleBack onBack={this.clearData}>

                <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#EFEFF4' }} >
                    <Toastt ref="toast"></Toastt>
                    <BackgroundImage >
                        <SafeAreaView />
                        <Content>
                        <ImageBackground source={this.state.photo_path == '' || this.state.photo_path == null || this.state.photo_path == undefined ? default_placeholder : {uri:this.state.photo_path}} style={{ height: 265}} resizeMode = 'cover'>
                            <View style={{ height: 265, flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent' }}>
                                {/* <Text style={{ fontSize: 19, fontFamily: fonts.Raleway_Bold, color: colors.dargrey }}>{this.props.navigation.state.params.friendName}</Text>
                                <View style={{ height: Height(1) }} />
                                <Text onPress={this.goBack} style={{ fontSize: 13, fontFamily: "Raleway-Medium", color: colors.blue }}>Select someone else</Text> */}
                                {
                                    this.state.photo_path =='' || this.state.photo_path == null ?
                                    <TouchableOpacity onPress = { () => this.onPressCoverPhoto()}>
                                        <Image source={ic_add_cover} />
                                    </TouchableOpacity>
                                    : 
                                    null
                                }
                                
                            </View>
                            {
                                this.state.photo_path != '' && this.state.photo_path != null ?

                                <TouchableOpacity onPress = { () => this.onPressCoverPhoto()} style = {{position:'absolute',bottom:10,left:10}}>
                                    <Image source={ic_edit_cover} />
                                </TouchableOpacity>

                                : null
                            }
                        </ImageBackground>
                            <View style = {{height:Height(6), backgroundColor:'#fff', width:'100%'}}>
                                <View style = {{flexDirection:'row', marginHorizontal:'4%', marginTop:15,alignItems:'center'}}>
                                    <Text style={{ fontSize: 19, fontFamily: fonts.Raleway_Bold, color: colors.dargrey, marginRight:10}} numberOfLines = {2}>I wanna see {this.props.navigation.state.params.friendName}</Text>
                                    <TouchableOpacity onPress = {this.goBack}>
                                        <Image source={ic_edit_event} style = {{marginRight:16}}/>
                                    </TouchableOpacity>
                                </View>
                                {/* <View style = {{height:Height(3), width:Width(40),marginLeft:'4%',borderColor:'#C1C1C1',borderWidth:0.5,alignItems:'center',justifyContent:'center', marginTop:10, borderRadius:5, flexDirection:'row'}}>
                                    <Text style={{ fontSize: 12, fontFamily: fonts.Raleway_Regular, color: colors.black, marginLeft:10, marginRight:5}}>Share with</Text>
                                    <Text style={{ fontSize: 12, fontFamily: fonts.Raleway_Regular, color: colors.blue, marginRight:10}}>{this.props.navigation.state.params.friendName}</Text>
                                </View> */}
                            </View>
                            <View style={{ height: Height(5), width: '100%', backgroundColor: colors.lightPink, justifyContent: 'center' }}>
                                <Text style={{ fontSize: 18, fontFamily: "Raleway-Medium", color: colors.appColor, marginLeft: '4%' }}>Optional</Text>
                            </View>
                            <View style={{}}>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate('EventDescription',{plan_type:1,description:this.state.description,is_from_eventdetails:false}) }} style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white, height:Height(6.6)}}>
                                    <View style={{ marginLeft: '4%', marginTop: '3%', marginBottom: '3%', justifyContent: 'center', flex: 6 }}>
                                        {
                                            this.state.description == '' || this.state.description == null ?
                                            <Text style={{ width: Width(90), fontSize: FontSize(18), color: '#8F8F90', fontFamily: "Raleway-Regular" }}>Description</Text>
                                            :
                                            <Text style={{ width: Width(85), fontSize: FontSize(18), color: '#000', fontFamily: "Raleway-Regular" }} numberOfLines = {1}>{this.state.description}</Text>
                                        }
                                    </View>
                                    <View style={{ alignItems: 'flex-end', right: Width(3), flex: 4 }}>
                                        <TouchableHighlight underlayColor="transparent" style={{}}>
                                            <AntDesign size={Width(5)} name="right" color={colors.dargrey} />
                                        </TouchableHighlight>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{ height: Height(1), marginTop: 1 }} />
                            {this.state.activity == null ?
                            <View style={{}}>
                             <TouchableOpacity onPress={() => {  this.props.navigation.navigate('Acitivity1') }}
                                    style={{ height: Height(6.6), flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white }}>
                                    <View style={{ alignItems: 'center', flex: 1.6 ,marginLeft:Width(1)}}>
                                        <Image source={cup} />
                                    </View>
                                    <View style={{ justifyContent: 'center', flex: 6 }}>
                                        <Text style={{ width: Width(60), fontSize: FontSize(18), color: colors.dargrey, fontFamily: "Raleway-Medium" }}>Add an activity</Text>
                                    </View>
                                    <View style={{ alignItems: 'flex-end', right: Width(3), flex: 4 }}>
                                        <TouchableHighlight underlayColor="transparent" style={{}}>
                                            <AntDesign size={Width(5)} name="right" color={colors.dargrey} />
                                        </TouchableHighlight>
                                    </View>
                            </TouchableOpacity>
                        </View>:
                            <View style={{}}>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate('Acitivity1') }} style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white, height:Height(6.6)}}>
                                    <View style={{ width: Width(9), alignItems: 'center', flex: 1.6 ,marginLeft:Width(1)}}>
                                        <Image source={cup} />
                                    </View>
                                    <View style={{ marginTop: '3%', marginBottom: '3%', justifyContent: 'center', flex: 6 }}>
                                        <Text style={{ width: Width(60), fontSize: FontSize(18), color: colors.dargrey, fontFamily: "Raleway-Medium" }}>{this.state.activity}</Text>
                                    </View>
                                    <View style={{ alignItems: 'flex-end', right: Width(3), flex: 4 }}>
                                        <TouchableHighlight underlayColor="transparent" style={{}}>
                                            <AntDesign size={Width(5)} name="right" color={colors.dargrey} />
                                        </TouchableHighlight>
                                    </View>
                                </TouchableOpacity>
                            </View>}

                          
                            {/* <View style={{ height: Height(0.2) }} /> */}
                            {this.state.location == null ?
                                <View style={{}}>
                                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('Location1') }}
                                        style={{ height: Height(6.6), flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white }}>
                                        <View style={{ alignItems: 'center', flex: 1.6, marginLeft: Width(1) }}>
                                            <Image source={Location_new} />
                                        </View>
                                        <View style={{ justifyContent: 'center', flex: 6 }}>
                                            <Text style={{ width: Width(60), fontSize: FontSize(18), color: colors.dargrey, fontFamily: "Raleway-Medium" }}>Add a location</Text>
                                        </View>
                                        <View style={{ alignItems: 'flex-end', right: Width(3), flex: 4 }}>
                                            <TouchableHighlight underlayColor="transparent" style={{}}>
                                                <AntDesign size={Width(5)} name="right" color={colors.dargrey} />
                                            </TouchableHighlight>
                                        </View>
                                    </TouchableOpacity>
                                </View> :
                                <View style={{}}>
                                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('Location1') }} style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white, height:Height(6.6) }}>
                                        <View style={{ width: Width(9), alignItems: 'center', flex: 1.6, marginLeft: Width(1) }}>
                                            <Image source={Location_new} />
                                        </View>
                                        <View style={{ marginTop: '3%', marginBottom: '3%', justifyContent: 'center', flex: 6 }}>
                                            <Text style={{ fontSize: FontSize(18), color: colors.dargrey, fontFamily: "Raleway-Medium" }}>{this.state.location}</Text>
                                        </View>
                                        <View style={{ alignItems: 'flex-end', right: Width(3), flex: 4 }}>
                                            <TouchableHighlight underlayColor="transparent" style={{}}>
                                                <AntDesign size={Width(5)} name="right" color={colors.dargrey} />
                                            </TouchableHighlight>
                                        </View>
                                    </TouchableOpacity>
                                </View>}

                            {/* <View style={{ height: Height(0.2) }} /> */}
                            {this.state.current_time == null ?
                                <View style={{}}>
                                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('Time1',{item:this.state.item,is_edit_plan:this.state.is_edit_plan}) }}
                                        style={{ height: Height(6.6), flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white }}>
                                        <View style={{ alignItems: 'center', flex: 1.6, marginLeft: Width(1) }}>
                                            <Image source={time} />
                                        </View>
                                        <View style={{ justifyContent: 'center', flex: 6 }}>
                                            <Text style={{ width: Width(60), fontSize: FontSize(18), color: colors.dargrey, fontFamily: "Raleway-Medium" }}>Add a date & time</Text>
                                        </View>
                                        <View style={{ alignItems: 'flex-end', right: Width(3), flex: 4 }}>
                                            <TouchableHighlight underlayColor="transparent" style={{}}>
                                                <AntDesign size={Width(5)} name="right" color={colors.dargrey} />
                                            </TouchableHighlight>
                                        </View>
                                    </TouchableOpacity>
                                </View> :
                                <View style={{}}>
                                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('Time1',{item:this.state.item,is_edit_plan:this.state.is_edit_plan}) }} style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white}}>
                                        <View style={{ width: Width(9), alignItems: 'center', flex: 1.6, marginLeft: Width(1) }}>
                                            <Image source={time} />
                                        </View>
                                        <View style={{ marginTop: '3%', marginBottom: '3%', justifyContent: 'center', flex: 6 }}>
                                            <Text style={{ width: Width(60), fontSize: FontSize(18), color: colors.dargrey, fontFamily: "Raleway-Medium" }}>{this.state.current_time}</Text>
                                        </View>
                                        <View style={{ alignItems: 'flex-end', right: Width(3), flex: 4 }}>
                                            <TouchableHighlight underlayColor="transparent" style={{}}>
                                                <AntDesign size={Width(5)} name="right" color={colors.dargrey} />
                                            </TouchableHighlight>
                                        </View>
                                    </TouchableOpacity>
                                </View>}
                            <View style={{ height: Height(1), marginTop: 1 }} />
                            {/* <TextInput
                                style={{ width: '100%', backgroundColor: colors.white, height: Height(9), paddingVertical: 0, marginTop: 1, marginBottom: 1, paddingHorizontal: Width(3.2), fontSize: 16 }}
                                placeholder='More info'
                                placeholderTextColor='#8F8F90'
                                maxLength={120}
                                multiline={true}
                                numberOfLines={2}
                                onChangeText={(description) => { this.setState({ description: description }) }}
                                value={this.state.description} />
                            <View style={{ height: Height(1) }} /> */}

                            <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row', backgroundColor: colors.white, borderColor: colors.white, height: Height(6.5), justifyContent: 'center', width: '100%' }}>
                                <Text style={{ left: Width(3.2), position: 'absolute', fontSize: 18, fontFamily: "Raleway-Medium", color: colors.dargrey }}>Guests can invite friends</Text>
                                <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964', false: '#ccc' }}
                                            value={this.state.value}
                                            style = {{position:'absolute', right:Width(3)}}
                                            onValueChange={(value) => this.setState({ value })} />
                            </View>
                            <View style={{ height: Height(5) }}></View>
                        </Content>
                        <TouchableOpacity style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }} onPress={this.state.is_edit_plan ? this.editPlan : this.addPlan} >
                            <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                                {this.state.is_edit_plan == true ? <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Save</Text>
                                : <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Post</Text>}
                            </ImageBackground>
                        </TouchableOpacity>
                        <SafeAreaView />
                    </BackgroundImage>
                </Container>
            </HandleBack>
        );
    }
}

export default Who2;