import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Switch, Alert, DatePickerIOS, TimePickerAndroid, ImageBackground, TouchableOpacity, StatusBar, Text, Platform, TextInput } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FloatingLabel from 'react-native-floating-labels';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import Modal from "react-native-modal";
//import DatePicker from 'react-native-datepicker'
import DatePicker from 'react-native-date-picker'
import moment from 'moment';
var dateFormat = require('dateformat');

const backWhite = require("../../../assets/images/backWhite.png");
const bottomRight = require("../../../assets/images/Login/bottomRight.png");
const btnBgFB = require("../../../assets/images/Login/btnBgFB.png");
const btnBgGrey = require("../../../assets/images/btnBg.png");
const menu1 = require("../../../assets/images/Dashboard/menu1.png");
const menu2 = require("../../../assets/images/Dashboard/menu2.png");
const menu3 = require("../../../assets/images/Dashboard/menu3.png");
const uncheck = require("../../../assets/images/unSelectRadio.png");
const current_location = require("../../../assets/images/current_location.png");

var subThis;
class Time1 extends ValidationComponent {

  constructor(props) {
    super(props);

    this.state = {
      labelStylePassword: styles.labelInputUnFocus, stylePassword: styles.formInputUnFocus, password: "", passwordValidation: true, passwordValidationMsg: "",
      labelStyleEmail: styles.labelInputUnFocus, styleEmail: styles.formInputUnFocus, email: "", emailValidation: true, emailValidationMsg: "",
      btnDoneDisable: true, btnDoneImage: btnBgGrey, wsError: false,
      switch1Value: false,
      switchnow: false,
      date: new Date(),
      date1: new Date(),
      dateNow: new Date(),
      showDatePicker: false,
      showDatePicker1: false,
      showDatePicker3: false,
      is_now: '0',
      is_edit_plan: this.props.navigation.state.params.is_edit_plan,
      item:this.props.navigation.state.params.item
    };

    subThis = this
  }

  componentDidMount() {

    var d = new Date()
    d.setHours(d.getHours()+1,d.getMinutes());
    this.setState({dateNow:d})

    if(this.state.is_edit_plan == true)
    {

        if(this.state.item.start_date != '')
        {
          var objstartdate =  moment.utc(this.state.item.start_date)
          var objenddate = moment.utc(this.state.item.end_date)
  
          //var startdatelocal = objstartdate.local()
          //var enddatelocal = objenddate.local()

          this.setState({date:new Date(objstartdate),date1:new Date(objenddate)})
          
          //alert(startdatelocal)

          if(this.state.item.is_allday == 1)
          {
              this.setState({switch1Value:true, switchnow:false})
          }
          else
          {
            this.setState({switch1Value:false})
          }
  
          if(this.state.item.is_now == 1)
          {
            this.setState({switch1Value:false, switchnow:true,dateNow:new Date(objenddate)})
          }
          else
          {
            this.setState({switchnow:false})
          }
        }
        else
        {
          var d = new Date()
          d.setHours(d.getHours() + 1, d.getMinutes());
          this.setState({ date1: d })
        }
    }
    else
    {
        var d = new Date()
        d.setHours(d.getHours() + 1, d.getMinutes());
        this.setState({ date1: d })
    }

    
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
      header: (
        <Header style={[styles.headerAndroidnav,{height:44}]}>
          <StatusBar barStyle="light-content" backgroundColor="#41199B" />
          <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
            <TouchableOpacity onPress={() => { navigation.goBack() }}>
              <Image source={backWhite} style={{ tintColor: "white" }} />
            </TouchableOpacity></View>
          <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
            <Text style={styles.headerTitle} >Add a date & time</Text></View>
        </Header>
      )
    }
  }


  toggleSwitch1 = (value) => {

    if(Platform.OS == 'android')
    {
        if(this.state.date != null)
        {
            var strdate = this.state.date.toString()
            var objdate = new Date(strdate)
            this.setState({date:objdate})
        }

        if(this.state.date1 != null)
        {
            var strdate = this.state.date1.toString()
            var objdate = new Date(strdate)
            this.setState({date1:objdate})
        }
    }

    this.setState({ switch1Value: value, switchnow: false })

  }
  toggleSwitchnow = (value) => {
    this.setState({ switchnow: value, switch1Value: false })

  }
  goBack = () => {

    var currentDate = moment().format("ll");
    let tomorrow = moment().add(1, 'days').format("ll")
    var date = moment(this.state.dateNow).format('ll')

    if (this.state.switchnow) {
      if (date == currentDate) {
        AsyncStorage.setItem('CurrentDate', 'Now - ' + moment(this.state.dateNow).format('LT'))
      } else {
        if (date == tomorrow) {
          AsyncStorage.setItem('CurrentDate', 'Now - Tomorrow · ' + moment(this.state.dateNow).format('LT'))

        } else {
          AsyncStorage.setItem('CurrentDate', 'Now - ' + moment(date).format('ddd, MMM D') + ' · ' + moment(this.state.dateNow).format('LT'))

        }

      }

      AsyncStorage.setItem('start', currentDate)
      AsyncStorage.setItem('end', moment(this.state.dateNow).format('lll'))
      AsyncStorage.setItem('is_now', '1')
      AsyncStorage.setItem('is_allday', '0')

      this.props.navigation.goBack()
    }
    else if (this.state.switch1Value == true) {
      if (currentDate == moment(this.state.date).format('ll') && currentDate == moment(this.state.date1).format('ll')) {
        AsyncStorage.setItem('CurrentDate', 'Today · All Day')
      }
      else {
        if (currentDate == moment(this.state.date).format('ll') && tomorrow == moment(this.state.date1).format('ll')) {
          AsyncStorage.setItem('CurrentDate', 'Today - Tomorrow · All Day')

        } else {
          if (tomorrow == moment(this.state.date).format('ll') && tomorrow == moment(this.state.date1).format('ll')) {
            AsyncStorage.setItem('CurrentDate', 'Tomorrow · All Day')

          } else {
            if(tomorrow == moment(this.state.date).format('ll')){
              AsyncStorage.setItem('CurrentDate', 'Tomorrow - ' + moment(this.state.date1).format('ddd, MMM D') + ' · All Day')

            }else{
              if(currentDate == moment(this.state.date).format('ll')){
                AsyncStorage.setItem('CurrentDate', 'Today - ' + moment(this.state.date1).format('ddd, MMM D') + ' · All Day')

              }else{
              if(moment(this.state.date).format('ll') == moment(this.state.date1).format('ll')){
                AsyncStorage.setItem('CurrentDate', 'All Day - ' + moment(this.state.date1).format('ddd, MMM D'))

              }else{
                AsyncStorage.setItem('CurrentDate', moment(this.state.date).format('ddd, MMM D') + ' - ' + moment(this.state.date1).format('ddd, MMM D') + ' · ALL DAY')

              }
            }

            }

          }

        }

      }
      AsyncStorage.setItem('start', moment(this.state.date).format('ll'))
      AsyncStorage.setItem('end', moment(this.state.date1).format('ll'))
      AsyncStorage.setItem('is_allday', '1')

      if (moment(this.state.date).format('ll') > moment(this.state.date1).format('ll')) {
        if (moment(this.state.date1).format('ll') == moment(this.state.date1).format('ll')) {
          AsyncStorage.setItem('is_now', '0')
          this.props.navigation.goBack()
        } else {
          this.refs.toast.show('Select valid end date!');
        }
      }
      else {
        AsyncStorage.setItem('is_now', '0')
        this.props.navigation.goBack()
            }
    }
    else {
      if (currentDate == moment(this.state.date).format('ll') && currentDate == moment(this.state.date1).format('ll')) {
        AsyncStorage.setItem('CurrentDate', 'Today \u00B7 ' + moment(this.state.date).format('LT') + ' - ' + moment(this.state.date1).format('LT'))

      } else {
        if (tomorrow == moment(this.state.date).format('ll') && tomorrow == moment(this.state.date1).format('ll')) {
          AsyncStorage.setItem('CurrentDate', 'Tomorrow \u00B7 ' + moment(this.state.date).format('LT') + ' - ' + moment(this.state.date1).format('LT'))
        } else {
          if (currentDate == moment(this.state.date).format('ll') && tomorrow == moment(this.state.date1).format('ll')) {
            AsyncStorage.setItem('CurrentDate', 'Today \u00B7 ' + moment(this.state.date).format('LT') + ' - Tomorrow \u00B7 ' + moment(this.state.date1).format('LT'))
          } else {
            if(tomorrow == moment(this.state.date).format('ll')){
              AsyncStorage.setItem('CurrentDate', ' Tomorrow - ' + moment(this.state.date1).format('ddd, MMM D \u00B7 LT'))
            }else{
              if(currentDate == moment(this.state.date).format('ll')){
                AsyncStorage.setItem('CurrentDate', 'Today - ' + moment(this.state.date1).format('ddd, MMM D \u00B7 LT') + ' · All Day')

              }else{
              AsyncStorage.setItem('CurrentDate', moment(this.state.date).format('ddd, MMM D \u00B7 LT') + ' to ' + moment(this.state.date1).format('ddd, MMM D \u00B7 LT'))
              }
            }
          }
        }
      }
      AsyncStorage.setItem('start', moment(this.state.date).format('lll'))
      AsyncStorage.setItem('end', moment(this.state.date1).format('lll'))
      AsyncStorage.setItem('is_allday', '0')

      if (this.state.date > this.state.date1) {
        this.refs.toast.show('Select valid end date!???');
      }
      else {
        AsyncStorage.setItem('is_now', '0')
        this.props.navigation.goBack()  
          }
    }

  }
  render() {
    let date = this.state.date
    var currentDate = moment();

    return (
      <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#EFEFF4' }} >
        <Toastt ref="toast"></Toastt>
        <BackgroundImage >
          <SafeAreaView />


          <Content>
            <View style={{ height: Height(2) }} />
            <View style={{ backgroundColor: colors.white }}>
              <TouchableOpacity style={{ backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ margin: Width(4), color: colors.whatFontColor, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>All day</Text>
                {
                  Platform.OS == 'ios' ?
                  <Switch
                  style={{ position: 'absolute', right: Width(4) }}
                  trackColor={{ true: "#4BD964" }}
                  ios_backgroundColor={'#E4E4E5'}
                  onValueChange={this.toggleSwitch1}
                  value={this.state.switch1Value} />
                  :
                  <Switch
                  style={{ position: 'absolute', right: Width(4) }}
                  trackColor={{ true: "#4BD964" }}
                  thumbTintColor='#ffffff'
                  onValueChange={this.toggleSwitch1}
                  value={this.state.switch1Value} />
                }
                
              </TouchableOpacity>
              <View style={{ marginLeft: Width(4), borderBottomColor: '#CCC', borderBottomWidth: Width(0.3) }} />
            </View>
            {this.state.switch1Value == false ?
              <View style={{ backgroundColor: colors.white }}>

                <View style={{ backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={{ margin: Width(4), color: colors.whatFontColor, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Starts</Text>
                  {Platform.OS == 'android' ?
                    // <DatePicker
                    //   style={{ width: 200, position: 'absolute', right: Width(1), }}
                    //   date={this.state.date}
                    //   mode="datetime"
                    //   minDate={new Date()}
                    //   androidMode='spinner'
                    //   placeholder="select date"
                    //   format="ll LT"
                    //   confirmBtnText="Confirm"
                    //   cancelBtnText="Cancel"
                    //   showIcon={false}
                    //   customStyles={{
                    //     dateText: {
                    //       fontSize: FontSize(16)
                    //     },
                    //     dateInput: {
                    //       position: 'absolute', right: Width(4),
                    //       borderColor: '#fff',
                    //     }
                    //   }}
                    //   onDateChange={(date) => {  
                    //     var Newdate = new Date(date)
                    //     Newdate.setHours(Newdate.getHours() + 1, Newdate.getMinutes());
                    //      this.setState({ date: date, date1:Newdate })
                    //   }} /> 
                    <Text
                      onPress={() => this.setState({ showDatePicker: !this.state.showDatePicker, showDatePicke1: false, showDatePicker3: false, date: this.state.date})}
                      style={[this.state.showDatePicker ? styles.textColor : styles.textUnColor]}>{dateFormat(this.state.date, "mmm dd, yyyy   hh:MM TT")}</Text>
                      :
                    <Text
                      onPress={() => this.setState({ showDatePicker: !this.state.showDatePicker, showDatePicke1: false, showDatePicker3: false, date: this.state.date})}
                      style={[this.state.showDatePicker ? styles.textColor : styles.textUnColor]}>{dateFormat(this.state.date, "mmm dd, yyyy   hh:MM TT")}</Text>}

                </View>
                {
                  Platform.OS == 'android' ?
                   this.state.showDatePicker ?
                   <View style = {{alignItems:'center',justifyContent:'center',backgroundColor:'#fff'}}>
                     <DatePicker
                       date={this.state.date}
                       minimumDate ={new Date()}
                       mode = 'datetime'
                         onDateChange={(date) => {  
                         var Newdate = new Date(date)
                         Newdate.setHours(Newdate.getHours() + 1, Newdate.getMinutes());
                         this.setState({ date: date, date1:Newdate })
                       }}
                     />
                   </View>
                     : 
                   <View />
                   :
                this.state.showDatePicker ?
                  <DatePickerIOS
                    style={{ flex: 1, height: 200, marginBottom: 20, backgroundColor: colors.white }}
                    date={this.state.date}
                    minimumDate={new Date()}
                    onDateChange={(date) => {  
                      var Newdate = new Date(date)
                      Newdate.setHours(Newdate.getHours() + 1, Newdate.getMinutes());
                       this.setState({ date: date, date1:Newdate })
                    }} 
                    mode="datetime" /> 
                  : 
                  <View />
                }

                <View style={{ marginLeft: Width(4), borderBottomColor: '#CCC', borderBottomWidth: Width(0.3) }} />
                <View style={{ backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={{ margin: Width(4), color: colors.whatFontColor, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Ends</Text>
                  {Platform.OS == 'android' ?
                    // <DatePicker
                    //   style={{ width: 200, position: 'absolute', right: Width(1), }}
                    //   date={this.state.date1}
                    //   mode="datetime"
                    //   androidMode='spinner'
                    //   minDate={this.state.date}
                    //   placeholder="select date"
                    //   format="ll LT"
                    //   confirmBtnText="Confirm"
                    //   cancelBtnText="Cancel"
                    //   showIcon={false}
                    //   customStyles={{
                    //     dateText: {
                    //       fontSize: FontSize(16)
                    //     },
                    //     dateInput: {
                    //       position: 'absolute', right: Width(4),
                    //       borderColor: '#fff',
                    //     }
                    //   }}
                    //   onDateChange={(date1) => { this.setState({ date1: date1 }) }} /> 
                      <Text
                      onPress={() => this.setState({ showDatePicke1: !this.state.showDatePicke1, showDatePicker: false, showDatePicker3: false, date1: this.state.date1 })}
                      style={[this.state.showDatePicke1 ? styles.textColor : styles.textUnColor]}>{dateFormat(this.state.date1, "mmm dd, yyyy   hh:MM TT")}</Text>
                    :
                    <Text
                      onPress={() => this.setState({ showDatePicke1: !this.state.showDatePicke1, showDatePicker: false, showDatePicker3: false, date1: this.state.date1 })}
                      style={[this.state.showDatePicke1 ? styles.textColor : styles.textUnColor]}>{dateFormat(this.state.date1, "mmm dd, yyyy   hh:MM TT")}</Text>}

                </View>
                {
                this.state.showDatePicke1 ? 
                Platform.OS == 'android' ?
                <View style = {{alignItems:'center',justifyContent:'center',backgroundColor:'#fff'}}>
                  <DatePicker
                      date={this.state.date1}
                      minimumDate = {this.state.date}
                      mode = 'datetime'
                      onDateChange={(date1) => { this.setState({ date1:date1 }) }}
                    />
                </View>
                :
                <DatePickerIOS
                  style={{ flex: 1, height: 200, marginBottom: 20, backgroundColor: colors.white }}
                  minimumDate={this.state.date}
                  date={this.state.date1}
                  onDateChange={(date1) => { this.setState({ date1: date1 }) }}
                  mode="datetime" /> 
                  : 
                <View />
                }
              </View> 
              :
              <View>
                <View style={{ backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={{ margin: Width(4), color: colors.whatFontColor, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Starts</Text>
                  {Platform.OS == 'android' ?
                    // <DatePicker
                    //   style={{ width: 200, position: 'absolute', right: Width(1), }}
                    //   date={this.state.date}
                    //   mode="date"
                    //   format="ddd, ll"
                    //   minDate={new Date()}
                    //   androidMode='spinner'
                    //   placeholder="select date"
                    //   confirmBtnText='Cinform'
                    //   cancelBtnText="Cancel"
                    //   showIcon={false}
                    //   customStyles={{
                    //     dateText: {
                    //       fontSize: FontSize(16)
                    //     },
                    //     dateInput: {
                    //       position: 'absolute', right: Width(4),
                    //       borderColor: '#fff',
                    //     }
                    //   }}
                    //   onDateChange={(date) => { this.setState({ date: date,date1:date }) }} /> 
                    <Text
                      onPress={() => this.setState({ showDatePicker: !this.state.showDatePicker, showDatePicke1: false, showDatePicker3: false, date: this.state.date,date1:this.state.date })}
                      style={[this.state.showDatePicker ? styles.textColor : styles.textUnColor]}>{dateFormat(this.state.date, "ddd, mmm d, yyyy")}</Text>
                    :
                    <Text
                      onPress={() => this.setState({ showDatePicker: !this.state.showDatePicker, showDatePicke1: false, showDatePicker3: false, date: this.state.date,date1:this.state.date })}
                      style={[this.state.showDatePicker ? styles.textColor : styles.textUnColor]}>{dateFormat(this.state.date, "ddd, mmm d, yyyy")}</Text>}

                </View>
                {
                this.state.showDatePicker ? 
                Platform.OS == 'android' ?
                  <View style = {{alignItems:'center',justifyContent:'center',backgroundColor:'#fff'}}>
                    <DatePicker
                      date={this.state.date}
                      minimumDate = {new Date()}
                      mode = 'date'
                      onDateChange={(date) => { this.setState({ date: date,date1:date }) }}
                    />
                  </View>
                  :
                <DatePickerIOS
                  style={{ flex: 1, height: 200, marginBottom: 20, backgroundColor: colors.white }}
                  minimumDate={new Date()}
                  date={this.state.date} onDateChange={(date) => this.setState({ date })}
                  mode="date" /> 
                  : 
                <View />
                }

                <View style={{ borderBottomColor: '#CCC', borderBottomWidth: Width(0.3) }} />
                <View style={{ backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={{ margin: Width(4), color: colors.whatFontColor, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Ends</Text>
                  {Platform.OS == 'android' ?
                    // <DatePicker
                    //   style={{ width: 200, position: 'absolute', right: Width(1), }}
                    //   date={this.state.date1}
                    //   mode="date"
                    //   format="ddd, ll"
                    //   minDate={this.state.date}
                    //   androidMode='spinner'
                    //   placeholder="select date"
                    //   confirmBtnText="Confirm"
                    //   cancelBtnText="Cancel"
                    //   showIcon={false}
                    //   customStyles={{
                    //     dateText: {
                    //       fontSize: FontSize(16)
                    //     },
                    //     dateInput: {
                    //       position: 'absolute', right: Width(4),
                    //       borderColor: '#fff',
                    //     }
                    //   }}
                    //   onDateChange={(date1) => { this.setState({ date1: date1 }) }} /> 
                      <Text
                      onPress={() => this.setState({ showDatePicke1: !this.state.showDatePicke1, showDatePicker: false, showDatePicker3: false, date1: this.state.date1 })}
                      style={[this.state.showDatePicke1 ? styles.textColor : styles.textUnColor]}>{dateFormat(this.state.date1, "ddd, mmm d, yyyy")}</Text>
                      :
                    <Text
                      onPress={() => this.setState({ showDatePicke1: !this.state.showDatePicke1, showDatePicker: false, showDatePicker3: false, date1: this.state.date1 })}
                      style={[this.state.showDatePicke1 ? styles.textColor : styles.textUnColor]}>{dateFormat(this.state.date1, "ddd, mmm d, yyyy")}</Text>}

                </View>
                {
                this.state.showDatePicke1 ? 
                Platform.OS == 'android' ?
                <View style = {{alignItems:'center',justifyContent:'center',backgroundColor:'#fff'}}>
                  <DatePicker
                      date={this.state.date1}
                      minimumDate = {this.state.date}
                      mode = 'date'
                      onDateChange={(date1) => { this.setState({ date1:date1 }) }}
                    />
                </View>
                :
                <DatePickerIOS
                  style={{ flex: 1, height: 200, marginBottom: 20, backgroundColor: colors.white }}
                  minimumDate={this.state.date}
                  date={this.state.date1} onDateChange={(date1) => this.setState({ date1 })}
                  mode="date" /> 
                : 
                <View />
                }
              </View>
              }

            <View style={{ height: Height(3) }} />
            <View style={{ backgroundColor: colors.white }}>
              <TouchableOpacity style={{ backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ margin: Width(4), color: colors.whatFontColor, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Now</Text>
                {
                  Platform.OS == 'ios' ?
                  <Switch
                  style={{ position: 'absolute', right: Width(4) }}
                  trackColor={{ true: "#4BD964" }}
                  ios_backgroundColor={'#E4E4E5'}
                  onValueChange={this.toggleSwitchnow}
                  value={this.state.switchnow} />
                  :
                  <Switch
                  style={{ position: 'absolute', right: Width(4) }}
                  trackColor={{ true: "#4BD964" }}
                  thumbTintColor='#ffffff'
                  onValueChange={this.toggleSwitchnow}
                  value={this.state.switchnow} />
                }
                
              </TouchableOpacity>

            </View>
            <View style={{ backgroundColor: colors.white }}>
              {this.state.switchnow ? <View style={{ marginLeft: Width(4), borderBottomColor: '#CCC', borderBottomWidth: Width(0.3) }} /> : null}</View>

            {/* {Platform.OS == 'android' && this.state.switchnow ?
              <View style={{ backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ margin: Width(4), color: colors.whatFontColor, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Until</Text>
                <DatePicker
                  style={{ width: 200, position: 'absolute', right: Width(1), }}
                  date={this.state.dateNow}
                  mode="datetime"
                  minDate={new Date()}
                  androidMode='spinner'
                  placeholder="select date"
                  format="llll"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  showIcon={false}
                  customStyles={{
                    dateText: {
                      fontSize: FontSize(16)
                    },
                    dateInput: {
                      position: 'absolute', right: Width(4),
                      borderColor: '#fff',
                    }
                  }}
                  onDateChange={(dateNow) => { this.setState({ dateNow: dateNow }) }}
                />
              </View>
              : null} */}
            {/* {Platform.OS == 'ios' && this.state.switchnow ? */}
            {this.state.switchnow ?
              <View style={{ backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ margin: Width(4), color: colors.whatFontColor, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Until</Text>
                <Text
                  onPress={() => this.setState({
                    showDatePicker3: !this.state.showDatePicker3,
                    showDatePicke1: false,
                    showDatePicker: false,
                    dateNow: this.state.dateNow
                  })}
                  style={[this.state.showDatePicker3 ? styles.textColor : styles.textUnColor]}>{moment(this.state.dateNow).format('llll')}</Text>
              </View>
              : null}

            {this.state.showDatePicker3 && this.state.switchnow == true ? 
            Platform.OS == 'android' ?
            <View style = {{alignItems:'center',justifyContent:'center', backgroundColor:'#fff'}}>
            <DatePicker
              date={this.state.dateNow}
              minimumDate = {new Date()}
              mode = 'datetime'
              onDateChange={(dateNow) => { this.setState({ dateNow: dateNow}) }}
            />
            </View>
            :
            <DatePickerIOS
              minimumDate={new Date()}
              style={{ flex: 1, height: 200, marginBottom: Height(10), marginTop: -10, backgroundColor: colors.white }}
              date={this.state.dateNow} onDateChange={(dateNow) => this.setState({ dateNow })}
              mode="datetime" /> 
              : 
              <View />
            }

          </Content>
          <TouchableOpacity style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }} onPress={this.goBack} >
            <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Select</Text>
            </ImageBackground>
          </TouchableOpacity>
          <SafeAreaView />
        </BackgroundImage>
      </Container>
    );
  }
}

export default Time1;
