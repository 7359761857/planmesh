import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, ImageBackground, TouchableOpacity, StatusBar, Text, Platform } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import { Container, View, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import FloatingLabel from 'react-native-floating-labels';
import { Width, Height, FontSize } from "../../config/dimensions";
import { loginUser, registerUser } from "../../redux/actions/auth";
import NavigationService from "../../services/NavigationService";
import { store } from "../../redux/store";
import { LOGIN, NOTIFICATION } from "../../redux/actions/types";
import moment from 'moment';
import { NavigationActions } from 'react-navigation';
import NetInfo from "@react-native-community/netinfo";


import MixpanelManager from '../../../Analytics'
const unsubscribe = NetInfo.addEventListener(state => {
  console.log("Connection type", state.type);
  console.log("Is connected?", state.isConnected);
  global.isConnected = state.isConnected
  if (state.isConnected == false) {

    AsyncStorage.getItem('internetmsg').then((value) => {
      console.log('internetmsg: ' + value)
      if (value == 'false') {
        AsyncStorage.setItem('internetmsg', 'true')
        alert('Oops your connection seems off, Check your connection and try again')
      }

    });
  }
  else {
    AsyncStorage.setItem('internetmsg', 'false')
  }

});

let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height


const backWhite = require("../../../assets/images/backWhite.png");
const bottomRight = require("../../../assets/images/Login/bottomRight.png");
const btnBgFB = require("../../../assets/images/Login/btnBgFB.png");
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
const btnBg = require("../../../assets/images/btnBg.png");

const FBSDK = require('react-native-fbsdk');
const {
  LoginManager,
  GraphRequest,
  GraphRequestManager,
  AccessToken,
} = FBSDK;

// const infoRequest = new GraphRequest(
//   '/me',
//   null,
//   this._responseInfoCallback,
// );


_responseInfoCallback = (error, result) => {
  console.log('test');
  if (error) {
    alert('Error fetching data: ' + error.toString());
  } else {
    alert('Success fetching data: ' + result.toString());
  }
}

var subThis;
class Login extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {
      labelStylePassword: styles.labelInputUnFocus, stylePassword: styles.formInputUnFocus, password: "", passwordValidation: true, passwordValidationMsg: "",
      labelStyleEmail: styles.labelInputUnFocus, styleEmail: styles.formInputUnFocus, email: "", emailValidation: true, emailValidationMsg: "",
      btnDoneDisable: true, btnDoneImage: btnBgGrey, wsError: false,
      device_type: Platform.OS == 'android' ? 0 : 1,
      emailTouch: false,
      birthday: '',
      isEmailFocus: false,
      passwordTouch: false,
      isPasswordFocus: false
    };
    this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
    subThis = this
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
      header: (
        <Header style={[styles.headerAndroidnav, { height: 44 }]}>
          <StatusBar barStyle="light-content" backgroundColor="#41199B" />
          <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
            <TouchableOpacity onPress={() => { navigation.goBack() }}>
              <Image source={backWhite} style={{ tintColor: "white" }} />
            </TouchableOpacity>
          </View>
          <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
            <Text style={styles.headerTitle} >Login</Text>
          </View>

        </Header>
      )
    }
  }

  onBlurEmail() {
    if (this.state.email.length <= 0) {
      this.setState({
        labelStyleEmail: styles.labelInputUnFocus,
        styleEmail: styles.formInputUnFocus,


      })
    } else {
      this.emailvalidtion()

    }
    this.setState({
      labelStyleEmail: styles.labelInputUnFocus,
      styleEmail: styles.formInputUnFocus,
      isEmailFocus: false

    })
    this.emailvalidtion()
    this.validtion()
  }
  onFocusEmail() {
    this.setState({
      labelStyleEmail: styles.labelInput,
      styleEmail: styles.formInputFocus,
      isEmailFocus: true
    })
  }
  onChangeTextEmail = (email) => {
    this.setState({ email }, () => {
      this.validtion()
      //this.emailvalidtion()
    })
  }
  emailvalidtion = () => {
    this.validate({
      email: { email: true, required: true },
    });
    if (this.isFieldInError('email')) {
      this.setState({ emailValidation: false, emailValidationMsg: 'Please enter your email address in format: yourname@example.com', labelStyleEmail: [styles.labelInput], })
      return false
    }
    this.setState({ emailValidation: true, emailValidationMsg: "" }, () => {
      this.validtion()
    })
    return true
  }
  onBlurPassword() {
    if (this.state.password.length <= 0) {
      this.setState({
        labelStylePassword: styles.labelInputUnFocus,
        stylePassword: styles.formInputUnFocus
      }, () => {
        this.validtion()
      })
    } else {
      //add some text
      this.passwordvalidtion()
    }
    this.setState({
      labelStylePassword: styles.labelInputUnFocus, isPasswordFocus: false, passwordTouch: true
    })
  }
  onFocusPassword() {
    this.setState({
      labelStylePassword: styles.labelInput,
      stylePassword: styles.formInputFocus,
      isPasswordFocus: true
    }, () => {
      this.validtion()
    })
  }
  onForgotPassword = () => {
    this.props.navigation.push('forgotPassword')
  }

  onChangeTextPassword = (password) => {
    this.setState({ password }, () => {
      this.validtion()
      // this.passwordvalidtion()

    })
  }



  passwordvalidtion = () => {
    this.validate({
      password: { minlength: 5, required: true },
    });
    if (this.isFieldInError('password')) {
      this.setState({ passwordValidation: false, passwordValidationMsg: this.getErrorsInField('password'), labelStylePassword: [styles.labelInput], })
      return false
    }
    this.setState({ passwordValidation: true, passwordValidationMsg: "" }, () => {
      this.validtion()
    })
    return true
  }

  validtion = () => {
    this.validate({
      password: { minlength: 5, required: true },
      email: { email: true },
    });
    if (this.isFieldInError('email')) {
      this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
      return false
    } else if (this.isFieldInError('password')) {
      this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
      return false
    }
    this.setState({ btnDoneDisable: false, btnDoneImage: btnBg, passwordValidation: true, })
    return true
  }
  customFacebookLogout() {
    let self = this
    if (Platform.OS == 'ios') {
      LoginManager.logOut();
      setTimeout(() => {
        self.facebookLogin()
      }, 4);
    } else {
      self.facebookLogin()
    }

    // let self = this

    // var current_access_token = '';
    // AccessToken.getCurrentAccessToken().then((data) => {
    //   current_access_token = data.accessToken.toString();
    // }).then(() => {
    //   let logout =
    //   new GraphRequest(
    //     "me/permissions/",
    //     {
    //         accessToken: current_access_token,
    //         httpMethod: 'DELETE'
    //     },
    //     (error, result) => {

    //         if (error) {
    //             console.log('Error fetching data: ' + error.toString());
    //             self.facebookLogin()
    //         } else {
    //           console.log('fetching data: ' + result);

    //           //alert('Logout...')
    //             LoginManager.logOut();
    //             self.facebookLogin()
    //         }
    //     });
    //   new GraphRequestManager().addRequest(logout).start();
    // })
    // .catch(error => {
    //   console.log(error)
    //   self.facebookLogin()
    // });    


  }
  facebookLogin() {
    let self = this
    LoginManager
      .logInWithPermissions(['public_profile', 'email', "user_birthday"])
      .then(function (result) {
        if (result.isCancelled) {
          //console.log('Login cancelled');
          alert('Login cancelled')
        } else {
          AccessToken
            .getCurrentAccessToken()
            .then((data) => {
              let accessToken = data.accessToken
              console.log(accessToken.toString())
              const responseInfoCallback = (error, result) => {
                if (error) {
                  console.log(error)
                  alert('Facebook request failed. Try Again.')
                } else {
                  console.log('FBlogin', result)
                  //alert('FbLogin')
                  self.fbLogin(result.email, result.name, result.picture.data.url, result.birthday)
                  //checkfbFunc(result.id, result.email, result.name, result.picture.data.url,result.birthday)
                }
              }

              const infoRequest = new GraphRequest(
                '/me?fields=email,name,picture.height(480),birthday',
                null,
                responseInfoCallback,
              );
              new GraphRequestManager().addRequest(infoRequest).start();
              console.log('responseInfoCallback', responseInfoCallback)

            }).catch(err => {
              alert('Facebook request failed. Try Again.')
            })
        }
      }, function (error) {
        //console.log('Login fail with error: ' + error);
        //alert('Facebook request failed. Try Again.' + error)
        self.customFacebookLogout()
      });
    checkfbFunc = (facebook_id, email, name, photo, birthday) => {
      if (email.length >= 1) {
        this.verifyFbIdws(facebook_id, email, name, photo, birthday)
      } else {
        this.props.navigation.push("SignupStep1", {
          'facebook_id': facebook_id,
          'email': email,
          'name': name,
          'photo': photo,
          'birthday': birthday,
          'code': this.state.validCode
        })
      }
    }
  }

  FbLoginButton = () => {
    let self = this
    self.customFacebookLogout()
  }



  verifyFbIdws = async (facebook_id, email, name, photo, birthday) => {
    let httpMethod = 'POST'
    let strURL = checkfb
    let headers = {
      'Content-Type': 'application/json',
      'device_token': deviceToken,
      'device_type': Platform.OS === 'ios' ? 1 : 2,
      'build_version': '1.0'
    }
    let params = {
      'fbid': facebook_id,
      'name': name,
      'email': email,
      'photo': photo,
      'birthday': birthday
    }

    // APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
    //   console.log('Login Screen -> ');
    //   console.log(resJSON)
    //   if (resJSON.status == 200) {
    //     let data = resJSON.data
    //     console.log(data);
    //     Pref.setData(Pref.kUserInfo, data)
    //     Pref.setData(Pref.kUserId, data.id)
    //     Pref.setData(Pref.kaccessToken, data.token)
    //     userid = data.id;
    //     token = data.token;

    //     this.props.navigation.push('dashboard')
    //     return
    //   } else {
    //     this.refs.toast.show(resJSON.message);
    //   }
    // })
  }


  onDoneInClick = () => {
    if (this.emailvalidtion() && this.passwordvalidtion()) {
      this.wscall(this.state.email, this.state.password);
    }
  }
  fbLogin = (email, name, profile_pic, birthday) => {
    let data = { social_type: 1, dob: moment(birthday).format("YYYYMMDD"), email: email, android_token: global.android_token, ios_token: global.ios_token, device_type: this.state.device_type }
    registerUser(data).then(res => {
      if (res.success) {
        if (res.data.is_profile_complete == 1) {
          store.dispatch({ type: LOGIN })
          AsyncStorage.setItem('overlay', 'false')
          this.mixpanel.identify(email);
          this.mixpanel.track('Login', {
            'Login Type': 'Social',
            '$email': email
          });
          // var date = moment.utc().format('YYYY-MM-DD HH:mm:ss');
          // AsyncStorage.setItem('notidate',date)
          global.isfromlogin = true
          NavigationService.reset('TabNavigator')

          //subThis.props.navigation.navigate('App')
        }
        else {
          //store.dispatch({type:LOGIN})
          NavigationService.navigate('SignupStep1', { name: name, profile_pic: profile_pic, birthday: false, email: email })
          //subThis.props.navigation.navigate('SignupStep1',{name:name,profile_pic:profile_pic,birthday:false,email:email})
        }
      }
      else {
        alert(res.text)
        //NavigationService.reset('SignupStep1',{name:name,profile_pic:profile_pic,birthday:false})

      }
    }).catch(err => {
      //alert('Oops your connection seems off, Check your connection and try again')
    })
  }
  onDoneClick = () => {
    this.onSignInClick()
  }

  onSignInClick = () => {
    //this.wscall(this.state.email, this.state.password); 
    //alert('Hi')

    loginUser({ email: this.state.email, password: this.state.password, social_type: 0, android_token: global.android_token, ios_token: global.ios_token, device_type: this.state.device_type }).then(res => {
      if (res.success) {
        //alert('Hiiii')
        store.dispatch({ type: LOGIN })
        global.isfromlogin = true
        // var date = moment.utc().format('YYYY-MM-DD HH:mm:ss');
        // AsyncStorage.setItem('notidate',date)
        AsyncStorage.setItem('overlay', 'false')
        NavigationService.reset('TabNavigator')

        this.mixpanel.identify(this.state.email);
        this.mixpanel.track('Login', {
          'Login Type': 'Email',
          '$email': this.state.email
        });
        //NavigationActions.reset('TabNavigator')
        //subThis.props.navigation.navigate('App')

      }
      else {
        this.setState({ wsError: true })
      }
    }).catch(err => {
      //alert('Oops your connection seems off, Check your connection and try again')
    })
  }
  onSignUpClick = () => {
    //this.props.navigation.push('signup')
    this.props.navigation.push('SignupOptions')
    //NavigationService.reset('Categories')
  }
  wscall = async (email, password) => {
    let httpMethod = 'POST'
    let strURL = userlogin
    let headers = {
      'Content-Type': 'application/json',
      'device_token': deviceToken,
      'device_type': Platform.OS === 'ios' ? 1 : 2,
      'build_version': '1.0',
    }
    let params = {
      'email': email,
      'password': password,
    }

    APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('Login Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        let data = resJSON.data
        Pref.setData(Pref.kUserInfo, data)
        Pref.setData(Pref.kUserId, data.id)
        Pref.setData(Pref.kaccessToken, data.token)
        userid = data.id
        token = data.token
        is_premium = data.is_premium
        AsyncStorage.setItem('overlay', 'false')

        this.props.navigation.push('TabNavigator')
        this.setState({ wsError: false })
        return
      } else {
        //this.refs.toast.show(resJSON.message);
        this.setState({ wsError: true })
      }

    })
  }


  render() {
    return (
      <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#FFF' }} >
        <Toastt ref="toast"></Toastt>
        <BackgroundImage >
          <SafeAreaView />

          <View style={{ position: 'absolute', bottom: 0, right: 0, backgroundColor: '#FFF' }}>
            <ImageBackground source={bottomRight} style={{ width: 165, height: 143 }}>
            </ImageBackground>
          </View>
          <Content>
            {
              this.state.wsError == true ?
                <View style={{ backgroundColor: "rgba(255, 0, 0, 0.1)", alignItems: 'center', justifyContent: 'center', marginLeft: 25, marginRight: 30, marginTop: 20, paddingTop: 15 }}>
                  <Text style={[{ color: "black", fontFamily: "Roboto-Bold", fontSize: FontSize(13) }, checkFontWeight("700")]}>The email or password is not correct.</Text>
                  <Button transparent style={[{ alignSelf: 'center', marginTop: -10 }]}
                    onPress={() => this.onForgotPassword()}
                  >
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={[{ color: "black", fontFamily: "Roboto-Regular", fontSize: FontSize(13) }]}>Did you </Text>
                      <Text style={{
                        color: 'black', fontSize: FontSize(13), fontFamily: "Roboto-Regular", textDecorationStyle: 'solid', textDecorationLine: 'underline', textDecorationColor: 'black'
                      }}>forget your password?</Text>
                    </View>
                  </Button>
                </View>
                : null
            }
            <FloatingLabel
              autoCapitalize="none"
              textContentType={"emailAddress"}
              labelStyle={[this.state.labelStyleEmail, this.state.email.length == 0 || (!this.state.isEmailFocus) ? styles.labelInputUnFocus : '', this.state.emailValidation == false && this.state.isEmailFocus &&
                this.state.email.length ? styles.labelInputError : {}]} inputStyle={styles.input}
              style={[styles.formInput, this.state.styleEmail]}
              onChangeText={(email) => this.onChangeTextEmail(email)} value={this.state.email}
              onBlur={() => this.onBlurEmail()}
              // onFocus={() => this.onFocusEmail()}
              keyboardType={"email-address"}
              // touch ={this.state.emailTouch && this.state.email.length}
              valid={this.state.emailValidation}
            >Email Address</FloatingLabel>

            {
              this.state.emailValidation == false && this.state.email.length
                ?
                <Text style={[{ marginLeft: Width(6), marginRight: Width(6), marginTop: Height(1), color: "red", fontSize: FontSize(10), fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.emailValidationMsg}</Text>
                : null
            }

            <FloatingLabel
              autoCapitalize="none"
              labelStyle={[this.state.labelStylePassword, this.state.password.length == 0 ? styles.labelInputUnFocus : '', this.state.isPasswordFocus && !this.state.passwordValidation ? styles.labelInputError : {}]}
              inputStyle={styles.input}
              style={[styles.formInput, this.state.stylePassword, { marginTop: 10 }]}
              onChangeText={(password) => this.onChangeTextPassword(password)} value={this.state.password}
              onBlur={() => this.onBlurPassword()}
              // onFocus={() => this.onFocusPassword()}
              password={true}
              // touch ={this.state.password.length}
              valid={this.state.passwordValidation}
            >Password</FloatingLabel>
            {
              this.state.passwordValidation == false && this.state.password.length ?
                <Text style={[{ marginLeft: Width(6), marginRight: Width(6), marginTop: Height(1), color: "red", fontSize: FontSize(10), fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.passwordValidationMsg}</Text>
                : null
            }
            <View style={{ alignItems: 'flex-end', justifyContent: 'flex-end', marginRight: 30, }}>
              <Button transparent style={[{ alignSelf: 'flex-end', marginTop: 0, borderRadius: 0, shadowOffset: { height: 0, width: 0 }, shadowOpacity: 0, elevation: 0 }]}
                onPress={() => this.onForgotPassword()}
              >
                <View style={{}}>
                  <Text style={{
                    color: '#8f8f90', textAlign: 'right',
                    fontSize: FontSize(15), fontFamily: "Roboto-Regular", textDecorationStyle: 'solid', textDecorationLine: 'underline', textDecorationColor: '#8f8f90'
                  }}>Forgot Password?</Text>
                </View>
              </Button>
            </View>

            <View style={{ height: Height(8) }} />

            {/* <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: Height(3), flexDirection: 'row' }}>
              <Text style={{ backgroundColor: '#363169', width: 25, height: 1, marginRight: 8 }} ></Text>
              <Text style={{ color: '#363169', textAlign: 'center', fontSize: FontSize(16), fontFamily: "Helvetica", letterSpacing: 0.005 }}>or</Text>
              <Text style={{ backgroundColor: '#363169', width: 25, height: 1, marginLeft: 8 }} ></Text>
            </View>
            <View style={{height:Height(4)}}/> */}

            {/* <TouchableOpacity style={{alignSelf:'center',width: '88%',height:Height(7),backgroundColor:'#3E5D94',borderRadius:Width(6),justifyContent:'center',alignItems:'center' }} onPress={() => this.FbLoginButton()}>
                <Text style={[{  fontSize: FontSize(19), fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Login with Facebook</Text>
            </TouchableOpacity>
            <View style={{height:Height(4)}}/> */}
            <TouchableOpacity style={{ alignSelf: 'center', width: '88%', height: Height(6.5), backgroundColor: this.state.btnDoneDisable ? '#8F8F90' : '#EC4AB9', borderRadius: Width(6), justifyContent: 'center', alignItems: 'center' }} disabled={this.state.btnDoneDisable} onPress={() => this.onSignInClick()}>
              <Text style={[{ alignSelf: 'center', fontSize: FontSize(19), fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Done</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: Height(3) }} onPress={() => this.onSignUpClick()}>
              <Text style={{ color: '#8f8f90', fontSize: FontSize(15), fontFamily: "Roboto-Regular", letterSpacing: 0.005 }}>Don’t have an account?</Text><Text style={{ color: '#007aff', fontSize: FontSize(15), fontFamily: "Roboto-Regular", letterSpacing: 0.005 }}> Sign up now</Text>
            </TouchableOpacity>
          </Content>

          <SafeAreaView />
        </BackgroundImage>
      </Container>
    );
  }
}

export default Login;
