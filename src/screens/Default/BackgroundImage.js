import React, { Component } from "react";
import { ImageBackground } from "react-native";
import styles from "./style";
class BackgroundImage extends Component {

    render() {
        return (
            //source={BG}
            <ImageBackground  
                  style={styles.backgroundImage}>
                  {this.props.children}
            </ImageBackground>
        )
    }
}

export default BackgroundImage;