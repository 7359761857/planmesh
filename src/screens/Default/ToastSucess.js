import React, { Component } from "react";
import { ImageBackground } from "react-native";
import styles from "./style";
import Toast, { DURATION } from 'react-native-easy-toast';
import {  Header } from "native-base";

class ToastSucess extends Component {
    show = (message) => {
        this.refs.toast.show(message, 500, () => {
        });
    }
    render() {
        return (
            <Toast ref="toast"
            style={{ backgroundColor: 'green', width: '100%',borderRadius: 0,minHeight:Header.HEIGHT,justifyContent: 'center',paddingTop:20}}
            position='top'
            positionValue={0}
            fadeInDuration={750}
            fadeOutDuration={1000}
            opacity={1}
            textStyle={{ color: 'white' }}
        />
        )
    }
}

export default ToastSucess;