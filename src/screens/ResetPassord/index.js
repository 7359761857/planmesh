import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Alert, ImageBackground, TouchableOpacity, StatusBar, Platform } from "react-native";
import { Container, View, Text, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import { forgotPassword } from "../../redux/actions/auth";
import ValidationComponent from 'react-native-form-validator';
import FloatingLabel from 'react-native-floating-labels';
import { changePassword } from "../../redux/actions/auth";
import { Height, FontSize, Width, colors } from "../../config/dimensions";
import { fonts, API_ROOT } from "../../config/constant";
const unSelectRadio = require("../../../assets/images/Dashboard/unSelectRadio.png");
const btnBg = require("../../../assets/images/btnBg.png");
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
const backWhite = require("../../../assets/images/backWhite.png");
const bottomRight = require("../../../assets/images/Login/bottomRight.png");
const SelectRadio = require("../../../assets/images/selectRadio.png");


class ResetPassord extends ValidationComponent {

    constructor(props) {
        super(props);
        this.state = {
            phn:false,
            email:false,
            emailVeri:'',
            userdetails:this.props.navigation.state.params.userdetails

        };
    }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;

        return {
            header: (
                <View style={styles.headerAndroidnav}>
                    <StatusBar barStyle="light-content" backgroundColor="#41199B" />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack() }}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity></View>
                    <View style={{ bottom: Height(2), position: 'absolute', alignSelf: 'center' }}>
                        <Text style={styles.headerTitle} >Reset password</Text>
                    </View>
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', right: Width(4) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack() }} >
                            <Text style={{ color: 'white' }} >Cancel</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            )
        }
    }

    componentDidMount() {
        this.setState({
            emailVeri: theNavigation.getParam('email'),
        })
    }
    onForgotPasswrodClick() {
            var data = new FormData()
            data.append('login_id', this.state.userdetails.login_id)
            data.append('type', this.state.phn == true ? 'mobile' : 'email')

            fetch(API_ROOT + 'sent/otp', {
            method: 'post',
            body: data
            }).then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {
                    this.props.navigation.navigate('createPassword',{email:this.state.emailVeri, login_id:this.state.userdetails.login_id})
                } else {
                    Alert.alert(
                        '',
                        responseData.text,
                        [
                          { text: 'OK'},
                        ],
                        { cancelable: false },
                      );
                }
            })
            .catch((error) => {
                //alert(error)
            })
    }

     emailMask(email) {
        var maskedEmail = email.replace(/([^@\.])/g, "*").split('');
        var previous	= "";
        for(i=0;i<maskedEmail.length;i++){
        if (i<=1 || previous == "." || previous == "@"){
        maskedEmail[i] = email[i];
        }
        previous = email[i];
        }
        return maskedEmail.join('');
    }

    render() {

        var phonenumber = this.state.userdetails.mobile_number != '' ? this.state.userdetails.mobile_number.slice(this.state.userdetails.mobile_number.length - 2 , this.state.userdetails.mobile_number.length) : ''
        return (
            <Container flex-direction={"row"} style={{ flex: 1,backgroundColor:'#FFF' }} >
            <Toastt ref="toast"></Toastt>
            <BackgroundImage >
              <SafeAreaView />
    
              <View style={{ position: 'absolute', bottom: 0, right: 0,backgroundColor:'#FFF' }}>
                <ImageBackground source={bottomRight} style={{ width: 165, height: 143 }}>
                </ImageBackground>
              </View>
                    <Content>
                        <View style={{ marginHorizontal: Width(4) }}>
                            <Text style={{ width: Width(60), lineHeight: 28, fontSize: FontSize(18), color: colors.black, fontFamily: fonts.Raleway_Semibold, marginTop: 17 }}>
                                How do you want to reset your password?</Text>


                            <TouchableOpacity
                            onPress={()=>{this.setState({
                                phn:true,
                                email:false
                            })}}
                             style={{ flexDirection: 'row', alignItems: 'center', marginTop: Height(3) }}>
                                <Image style={{height:15,width:15}} source={this.state.phn == true ? SelectRadio: unSelectRadio} />
                                <Text style={{ marginLeft: Width(3), fontSize: FontSize(14), color: colors.black, fontFamily: fonts.Roboto_Regular }}>
                        Text a code to my phone ending in {phonenumber}</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                             onPress={()=>{this.setState({
                                phn:false,
                                email:true
                            })}}
                             style={{ flexDirection: 'row', alignItems: 'center', marginTop: Height(1) }}>
                                <Image style={{height:15,width:15}} source={this.state.email == true ? SelectRadio: unSelectRadio} />
                                <Text style={{ marginLeft: Width(3), fontSize: FontSize(14), color: colors.black, fontFamily: fonts.Roboto_Regular }}>
                        Email a code to {this.emailMask(this.state.userdetails.email_address)}</Text>
                            </TouchableOpacity>
                        </View>
                        {this.state.email === true || this.state.phn === true ?
                        <TouchableOpacity style={{ width: '100%', marginTop: 50 }}  onPress={() => this.onForgotPasswrodClick()} >
                            <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Continue</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                        :
                        <View style={{ width: '100%', marginTop: 50 }}  >
                        <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Continue</Text>
                        </ImageBackground>
                    </View>
                        }
                    </Content>

                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default ResetPassord;
