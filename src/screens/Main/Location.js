import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Alert, ImageBackground, TouchableOpacity, StatusBar, Text, Platform, TextInput } from "react-native";
import { Container, View, Button, Header, Left, Right, Content, Item, Input } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FloatingLabel from 'react-native-floating-labels';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import Modal from "react-native-modal";
//import Geocoder from 'react-native-geocoder';
import { API_ROOT } from "../../config/constant";
import RNProgressHUB from 'react-native-progresshub';


import { store } from "../../redux/store"


const backWhite = require("../../../assets/images/backWhite.png");

const btnBg = require("../../../assets/images/btnBg.png");

const search_img = require("../../../assets/images/Dashboard/search.png");
const current_location = require("../../../assets/images/Dashboard/location_home.png");
import MixpanelManager from '../../../Analytics'
var subThis;
class Location extends ValidationComponent {

  constructor(props) {
    super(props);
    this.search = null;
    this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
    this.state = {

      labelStylePassword: styles.labelInputUnFocus, stylePassword: styles.formInputUnFocus, password: "", passwordValidation: true, passwordValidationMsg: "",
      labelStyleEmail: styles.labelInputUnFocus, styleEmail: styles.formInputUnFocus, email: "", emailValidation: true, emailValidationMsg: "",
      btnDoneDisable: true, wsError: false, destination: '', searchKey: '',
      latitude: '',
      longitude: '',
      predictions: [],
      plan_id: this.props.navigation.state.params.plan_id,
      is_direct: this.props.navigation.state.params.is_direct,
      other_id: this.props.navigation.state.params.other_id,
      token: store.getState().auth.user.token,
      login_id: store.getState().auth.user.login_id,
      is_from_event: this.props.navigation.state.params.is_from_event,
      item: this.props.navigation.state.params.item,
    };
    subThis = this
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
      header: (
        <Header style={[styles.headerAndroidnav, { height: 44 }]}>
          <StatusBar barStyle="light-content" backgroundColor="#41199B" />
          <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
            <TouchableOpacity onPress={() => { navigation.goBack() }}>
              <Image source={backWhite} style={{ tintColor: "white" }} />
            </TouchableOpacity></View>
          <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
            <Text style={styles.headerTitle} >Suggest a Location</Text></View>

        </Header>
      )
    }
  }
  async requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'Example App',
          'message': 'Example App access to your location '
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the location")
        alert("You can use the location");
      } else {
        console.log("location permission denied")
        alert("Location permission denied");
      }
    } catch (err) {
      console.warn(err)
    }
  }
  async componentWillUnmount() {
    this._navListener.remove();
  }

  async componentDidMount() {

    //this.search.focus()
    this.workaroundFocus()

    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('light-content');
    });
    await requestLocationPermission()
    this.watchID = navigator.geolocation.watchPosition((position) => {
      // Create the object to update this.state.mapRegion through the onRegionChange function
      this.setState({
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      })

    }, (error) => console.log(error));

  }

  workaroundFocus() {
    this.search.blur();
    setTimeout(() => {
      this.search.focus();
    }, 100);
  }


  async onChangeDestination(destination) {
    try {
      const result = await fetch(`https://maps.googleapis.com/maps/api/place/textsearch/json?location=${this.state.latitude},${this.state.longitude}&radius=3000&sensor=true&query=${destination}&key=AIzaSyA6JsG7CPXtIfYJJF3UFdz8z2eBDqohKA8`);
      const json = await result.json();
      this.setState({ predictions: json.results })
    } catch (err) {
      alert(err)
    }


  }
  goBack = (predictions) => {
    RNProgressHUB.showSpinIndeterminate();

    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('plan_id', this.state.plan_id)
    data.append('type', 'location')
    data.append('activity', '')
    data.append('line_1', predictions.name)
    data.append('line_2', predictions.formatted_address)
    data.append('pin_code', '')
    data.append('city', '')
    data.append('state', '')
    data.append('country', '')
    data.append('start_date', '')
    data.append('end_date', '')

    fetch(API_ROOT + 'add/suggestion', {
      method: 'post',
      body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          //alert(responseData.text)
          const MixpanelData = {
            '$email': store.getState().auth.user.email_address,
            'POST ID': this.state.plan_id,
            // locationSuggested: predictions.formatted_address
          }
          this.mixpanel.identify(store.getState().auth.user.email_address);
          this.mixpanel.track('Suggest Location', MixpanelData);

          if (this.state.is_direct == true) {
            this.props.navigation.navigate('SuggestLocation', { refreshing: true, other_id: this.state.other_id, plan_id: this.state.plan_id })
          }
          if (this.state.is_from_event == true) {
            this.props.navigation.navigate('SuggestLocation', { refreshing: true, other_id: this.state.other_id, plan_id: this.state.plan_id, is_from_event: this.state.is_from_event, item: this.state.item })
          }
          else {
            this.props.navigation.navigate('SuggestLocation', { refreshing: true })
          }

          RNProgressHUB.dismiss();

        } else {
          if (responseData.text === 'Invalid token key') {
            alert(responseData.text)
            this.props.navigation.navigate('SuggestLocation', { refreshing: true })
          }
          RNProgressHUB.dismiss();
        }
      })

      .catch((error) => {
        RNProgressHUB.dismiss();

      })

    //this.props.navigation.goBack()
  }
  render() {

    return (
      <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }} >
        <Toastt ref="toast"></Toastt>
        <BackgroundImage >
          <SafeAreaView />

          <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', backgroundColor: colors.white }}>
            <View style={{ height: Height(7), justifyContent: 'center', width: Width(100), flexDirection: 'row', alignItems: 'center', borderBottomColor: '#eae7eb', borderBottomWidth: 1 }}>
              <Image source={search_img} style={{ marginLeft: Width(5) }} />
              <TextInput
                placeholderTextColor={colors.fontDarkGrey}
                placeholder="Enter a place or city "
                ref={search => this.search = search}
                autoFocus={true}
                onChangeText={destination => this.onChangeDestination(destination)}
                style={{ flex: 1, fontFamily: fonts.Raleway_Medium, fontSize: 16, marginLeft: Width(4.5), width: Width(80) }} />
            </View>
          </View>


          <SafeAreaView style={{ backgroundColor: colors.white }}>
            <ScrollView>
              {this.state.predictions.map(predictions => {
                return (
                  <TouchableOpacity onPress={() => this.goBack(predictions)} style={{ minHeight: Height(10), flexDirection: 'row', alignItems: 'center', borderBottomColor: '#ccc', borderBottomWidth: 1, padding: 5 }}>
                    <View style={{ marginLeft: Width(5), justifyContent: 'center', alignItems: 'center' }}>
                      <Image source={current_location} />
                    </View>
                    <View style={{ marginLeft: Width(5.5), marginRight: Width(5) }}>
                      <Text numberOfLines={1} ellipsizeMode="tail" style={{ fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16), color: '#3A4759', width: '60%' }}>
                        {predictions.name}
                      </Text>
                      <Text numberOfLines={2} ellipsizeMode="tail" style={{ fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: '#8F8F90', marginRight: Width(5.5) }}>
                        {predictions.formatted_address}
                      </Text>


                    </View>
                  </TouchableOpacity>
                );
              })}


            </ScrollView>

          </SafeAreaView>

          <SafeAreaView />
        </BackgroundImage>
      </Container>

    );
  }
}

export default Location;