import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Alert, ImageBackground, TouchableOpacity, StatusBar, Text, Platform, TextInput } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import { Container, View, Button, Header, Left, Right, Content, Input, Item } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FloatingLabel from 'react-native-floating-labels';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import Modal from "react-native-modal";
const backWhite = require("../../../assets/images/backWhite.png");
const btnBg = require("../../../assets/images/btnBg.png");
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
import { API_ROOT } from "../../config/constant";
import { store } from "../../redux/store";
const cup = require("../../../assets/images/cup_border.png");
import RNProgressHUB from 'react-native-progresshub';
import MixpanelManager from '../../../Analytics'

class Activity extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {

      bio: "", labelStylePassword: styles.labelInputUnFocus, stylePassword: styles.formInputUnFocus, password: "", passwordValidation: true, passwordValidationMsg: "",
      btnDoneDisable: true, btnDoneImage: btnBgGrey, selectActivity: '',
      plan_id: this.props.navigation.state.params.plan_id,
      is_direct: this.props.navigation.state.params.is_direct,
      other_id: this.props.navigation.state.params.other_id,
      token: store.getState().auth.user.token,
      login_id: store.getState().auth.user.login_id,
      is_from_event: this.props.navigation.state.params.is_from_event,
      item: this.props.navigation.state.params.item,
      title_header:this.props.navigation.state.params.title_header != null ? this.props.navigation.state.params.title_header : '',
    };
    this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
    subThis = this
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
      header: (
        <Header style={[styles.headerAndroidnav,{height:44}]}>
          <StatusBar barStyle="light-content" backgroundColor="#41199B" />
          <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
            <TouchableOpacity onPress={() => { navigation.goBack() }}>
              <Image source={backWhite} style={{ tintColor: "white" }} />
            </TouchableOpacity></View>
          <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
            <Text style={styles.headerTitle} >Suggest an Activity</Text></View>

        </Header>
      )
    }
  }
 
  componentDidMount(){
    this._navListener = this.props.navigation.addListener('didFocus', () => {
        StatusBar.setBarStyle('light-content');
      });
}

componentWillUnmount() {
    this._navListener.remove();
  }

  allDayChange = (value) => {

    this.setState({ allDay: value })

  }
  nowChange = (value) => {
    this.setState({ now: value })

  }
  onBlurPassword() {
    if (this.state.password.length <= 0) {
      this.setState({

        labelStylePassword: styles.labelInputUnFocus,
        stylePassword: styles.formInputUnFocus
      })
    }
    this.setState({
      passwordTouch: true,
      labelStylePassword: styles.labelInputUnFocus,
      stylePassword: styles.formInputUnFocus,
      isPasswordFocus: false
    })

  }
  onFocusPassword() {
    this.setState({
      labelStylePassword: styles.labelInput,
      stylePassword: styles.formInputFocus,
      isPasswordFocus: true
    })
  }
  onChangeTextPassword = (selectActivity) => {
    this.setState({ selectActivity }, () => {

    })

  }
gotoNext=()=>{
  RNProgressHUB.showSpinIndeterminate();     

  var data = new FormData()
  data.append('token', this.state.token)
  data.append('login_id', this.state.login_id)
  data.append('plan_id', this.state.plan_id)
  data.append('type', 'activity')
  data.append('activity', this.state.selectActivity)
  data.append('line_1', '')
  data.append('line_2', '')
  data.append('pin_code', '')
  data.append('city', '')
  data.append('state', '')
  data.append('country', '')
  data.append('start_date', '')
  data.append('end_date', '')

  fetch(API_ROOT + 'add/suggestion', {
      method: 'post',
      body: data
  })
      .then((response) => response.json())
      .then((responseData) => {
          if (responseData.success) {

            RNProgressHUB.dismiss();

            //alert('Response Data: '+ JSON.stringify(responseData))
            const MixpanelData = {
              '$email': store.getState().auth.user.email_address,
              'Post ID': this.state.plan_id,
              }
              this.mixpanel.identify(store.getState().auth.user.email_address);
              this.mixpanel.track('Suggest Activity', MixpanelData);
            if (this.state.is_direct == true)
            {
              this.props.navigation.navigate('SuggestActivity', { refreshing: true,other_id: this.state.other_id, plan_id: this.state.plan_id,title_header:this.state.title_header})
            }
            else if (this.state.is_from_event == true)
            {
              this.props.navigation.navigate('SuggestActivity', { refreshing: true,other_id: this.state.other_id, plan_id: this.state.plan_id, is_from_event:this.state.is_from_event, item:this.state.item, title_header:this.state.title_header })
            }
            else
            {
                this.props.navigation.goBack()
            }

          } else {
              if (responseData.text === 'Invalid token key') {
                  alert(responseData.text)
                  this.props.navigation.navigate('SuggestActivity', { refreshing: true })
                }
              RNProgressHUB.dismiss();
          }
      })

      .catch((error) => {
          RNProgressHUB.dismiss();

      })
  // AsyncStorage.setItem('What_Activity',this.state.selectActivity)
 
}




  render() {

    return (
      <Container flex-direction={"row"} style={{ backgroundColor: '#fff' }} >
        <Toastt ref="toast"></Toastt>
        <BackgroundImage >
          <SafeAreaView />

          <View style={{ height: Height(14), flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white }}>
            <View style={{ marginLeft: Width(4), alignItems: 'center', justifyContent: 'center' }}>
              <Image source={cup}  /></View>
            <View style={{ justifyContent: 'center', width: '70%', marginBottom: Height(3) }}>
              <FloatingLabel
                maxLength={100}
                labelStyle={[this.state.labelStylePassword, this.state.password.length == 0 ? styles.labelInputUnFocus : '', this.state.passwordValidation == false && this.state.isPasswordFocus && this.state.passwordTouch && this.state.password.length ? styles.labelInputError : {}]}
                inputStyle={styles.input}          
                style={[styles.formInput, this.state.stylePassword]}
                onChangeText={(selectActivity) => this.onChangeTextPassword(selectActivity)}
                onBlur={() => this.onBlurPassword()}
                onFocus={() => this.onFocusPassword()}
                value={this.state.selectActivity}
                valid={this.state.password.length == 0 ? false : this.state.passwordValidation}
              >Enter an activity..</FloatingLabel>
            </View>
          </View>
          <Content>
            <View style={{ backgroundColor: '#DCCFFA', justifyContent: 'center', alignItems: 'center' }}>
              <Text style={styles.AcitvityColor}>Online</Text>
            </View>
            
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Video Chat?' })} style={{ backgroundColor: '#fff', flexDirection: 'row',borderBottomColor: '#CCC',borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Video chat</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Have a Game Night?' })} style={{ backgroundColor: '#fff', flexDirection: 'row',borderBottomColor: '#CCC',borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Game night</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Have a Virtual Happy Hour?' })} style={{ backgroundColor: '#fff', flexDirection: 'row',borderBottomColor: '#CCC',borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Virtual happy hour</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Have a Fitness Session?' })} style={{ backgroundColor: '#fff', flexDirection: 'row',borderBottomColor: '#CCC',borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Fitness session</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Have a Movie Night?' })} style={{ backgroundColor: '#fff', flexDirection: 'row',borderBottomColor: '#CCC',borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Movie night</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Have a Virtual Date Night?' })} style={{ backgroundColor: '#fff', flexDirection: 'row',borderBottomColor: '#CCC',borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Virtual date night</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Have a Virtual Study Session?' })} style={{ backgroundColor: '#fff', flexDirection: 'row',borderBottomColor: '#CCC',borderBottomWidth: 1  }}>
              <Text style={styles.subAcitvityColor}>Virtual study session</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Have a Zoom Call?' })} style={{ backgroundColor: '#fff', flexDirection: 'row',borderBottomColor: '#CCC',borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Zoom Call</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Have a Houseparty?' })} style={{ backgroundColor: '#fff', flexDirection: 'row'}}>
              <Text style={styles.subAcitvityColor}>Houseparty</Text>
            </TouchableOpacity>
            
            <View style={{ backgroundColor: '#DCCFFA', justifyContent: 'center', alignItems: 'center' }}>
              <Text style={styles.AcitvityColor}>Food</Text>
            </View>

            <TouchableOpacity
             onPress={() => this.setState({ selectActivity: 'Wanna Grab Breakfast?' })}
              style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Breakfast</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Grab Lunch?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Lunch</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Grab Brunch?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Brunch</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Grab Dinner?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Dinner</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Get Coffee?' })} style={{ backgroundColor: '#fff', flexDirection: 'row' }}>
              <Text style={styles.subAcitvityColor}>Coffee</Text>
            </TouchableOpacity>
            <View style={{ backgroundColor: '#DCCFFA', justifyContent: 'center', alignItems: 'center' }}>
              <Text style={styles.AcitvityColor}>Party</Text>
            </View>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go to a Bar?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Bar</Text>

            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go to a Lounge?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Lounge</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go Clubbing?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Club</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go Dancing?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Dance</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna do Happy Hour?' })} style={{ backgroundColor: '#fff', flexDirection: 'row' }}>
              <Text style={styles.subAcitvityColor}>Happy Hour</Text>
            </TouchableOpacity>
            <View style={{ backgroundColor: '#DCCFFA', justifyContent: 'center', alignItems: 'center' }}>
              <Text style={styles.AcitvityColor}>Travel</Text>
            </View>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go on a Road Trip?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Road Trip</Text>

            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go for a Drive?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Drive</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go on a Cruise?' })} style={{ backgroundColor: '#fff', flexDirection: 'row' }}>
              <Text style={styles.subAcitvityColor}>Cruise</Text>
            </TouchableOpacity>
            <View style={{ backgroundColor: '#DCCFFA', justifyContent: 'center', alignItems: 'center' }}>
              <Text style={styles.AcitvityColor}>Fun</Text>
            </View>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Watch a Movie?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Movies</Text>

            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go to the Museum?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Museum</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go to the Theater?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Theater</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go to a Festival?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Festival</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go see a Play?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Play</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go to the Opera?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Opera</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go to the Zoo?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Zoo</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Watch TV?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>TV</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go Shopping?' })} style={{ backgroundColor: '#fff', flexDirection: 'row' , borderBottomColor: '#CCC', borderBottomWidth: 1}}>
              <Text style={styles.subAcitvityColor}>Shop</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go to the Pool?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Pool</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Play Video Games?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Video Games</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Play Board Games?' })} style={{ backgroundColor: '#fff', flexDirection: 'row' , borderBottomColor: '#CCC', borderBottomWidth: 1}}>
              <Text style={styles.subAcitvityColor}>Board Games</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Play Card Games?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Card Games</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go to the Beach?' })} style={{ backgroundColor: '#fff', flexDirection: 'row' }}>
              <Text style={styles.subAcitvityColor}>Beach</Text>
            </TouchableOpacity>
            <View style={{ backgroundColor: '#DCCFFA', justifyContent: 'center', alignItems: 'center' }}>
              <Text style={styles.AcitvityColor}>Sports</Text>
            </View>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go Workout?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Workout</Text>

            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go for a Run?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Run</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go Bowling?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Bowling</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go to Yoga?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Yoga</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Watch a Game?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Watch Game</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Play Basketball?' })} style={{ backgroundColor: '#fff', flexDirection: 'row' , borderBottomColor: '#CCC', borderBottomWidth: 1}}>
              <Text style={styles.subAcitvityColor}>Basketball</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Play Soccer?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1}}>
              <Text style={styles.subAcitvityColor}>Soccer</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Play Football?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Football</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Play Volleyball?' })} style={{ backgroundColor: '#fff', flexDirection: 'row', borderBottomColor: '#CCC', borderBottomWidth: 1 }}>
              <Text style={styles.subAcitvityColor}>Volleyball</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go for a Bike Ride?' })} style={{ backgroundColor: '#fff', flexDirection: 'row' , borderBottomColor: '#CCC', borderBottomWidth: 1}}>
              <Text style={styles.subAcitvityColor}>Biking</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ selectActivity: 'Wanna Go for a Swim?' })} style={{ backgroundColor: '#fff', flexDirection: 'row' }}>
              <Text style={styles.subAcitvityColor}>Swimming</Text>
            </TouchableOpacity>

            <View style={{ height: Height(5) }}></View>

          </Content>
          {this.state.selectActivity == '' ? 
            <View style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }} >
            <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Select</Text>
            </ImageBackground>
          </View>:
          <TouchableOpacity style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }} onPress={this.gotoNext} >
            <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Select</Text>
            </ImageBackground>
          </TouchableOpacity>}
          <SafeAreaView />
        </BackgroundImage>
      </Container>
    );
  }
}


export default Activity;
