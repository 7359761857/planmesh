import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Switch, Alert, DatePickerIOS, TimePickerAndroid, ImageBackground, TouchableOpacity, StatusBar, Text, Platform, TextInput } from "react-native";
import { Container, View, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import { API_ROOT } from "../../config/constant";

import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
//import DatePicker from 'react-native-datepicker'
import DatePicker from 'react-native-date-picker'

import moment from 'moment';
var dateFormat = require('dateformat');
import RNProgressHUB from 'react-native-progresshub';

import { store } from "../../redux/store";

const backWhite = require("../../../assets/images/backWhite.png");

const btnBgGrey = require("../../../assets/images/btnBg.png");
import MixpanelManager from '../../../Analytics'

var subThis;
class DateTime extends ValidationComponent {

  constructor(props) {
    super(props);

    this.state = {
      labelStylePassword: styles.labelInputUnFocus, stylePassword: styles.formInputUnFocus, password: "", passwordValidation: true, passwordValidationMsg: "",
      labelStyleEmail: styles.labelInputUnFocus, styleEmail: styles.formInputUnFocus, email: "", emailValidation: true, emailValidationMsg: "",
      btnDoneDisable: true, btnDoneImage: btnBgGrey, wsError: false,
      switch1Value: false,
      switchnow: false,
      date: new Date(),
      default: new Date(),
      is_now: '0',
      maxDate: new Date(),
      date1: new Date(),
      dateNow: new Date(),
      showDatePicker: false,
      showDatePicker1: false,
      showDatePicker3: false,
      plan_id: this.props.navigation.state.params.plan_id,
      is_direct: this.props.navigation.state.params.is_direct,
      other_id: this.props.navigation.state.params.other_id,
      token: store.getState().auth.user.token,
      login_id: store.getState().auth.user.login_id,
      is_from_event: this.props.navigation.state.params.is_from_event,
      item: this.props.navigation.state.params.item,
      
    };
    this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
    subThis = this
  }



  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
      header: (
        <Header style={[styles.headerAndroidnav,{height:44}]}>
          <StatusBar barStyle="light-content" backgroundColor="#41199B" />
          <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
            <TouchableOpacity onPress={() => { navigation.goBack() }}>
              <Image source={backWhite} style={{ tintColor: "white" }} />
            </TouchableOpacity></View>
          <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
            <Text style={styles.headerTitle} >Suggest a Date & Time</Text></View>

        </Header>
      )
    }
  }
  
componentWillUnmount() {
    this._navListener.remove();
  }

  componentDidMount() {
    var d = new Date()
    d.setHours(d.getHours() + 1, d.getMinutes());
    this.setState({ date1: d })
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('light-content');
    });
  }

  toggleSwitch1 = (value) => {

    if(Platform.OS == 'android')
    {
        if(this.state.date != null)
        {
            var strdate = this.state.date.toString()
            var objdate = new Date(strdate)
            this.setState({date:objdate})
        }

        if(this.state.date1 != null)
        {
            var strdate = this.state.date1.toString()
            var objdate = new Date(strdate)
            this.setState({date1:objdate})
        }
    }

    this.setState({ switch1Value: value, switchnow: false })

  }
  toggleSwitchnow = (value) => {
    this.setState({ switchnow: value, switch1Value: false })
  }
  goBack = () => {   
    let  is_now =''
    let is_allday = ''
    if (this.state.switchnow) {
     is_now='1'
    }else{
     is_now='0'
    }

    if (this.state.switch1Value) {
      is_allday='1'
     }else{
      is_allday='0'
     }
 

      RNProgressHUB.showSpinIndeterminate();     
  
      var data = new FormData()
      data.append('token', this.state.token)
      data.append('login_id', this.state.login_id)
      data.append('plan_id', this.state.plan_id)
      data.append('type', 'datetime')
      data.append('activity','')
      data.append('line_1', '')
      data.append('line_2', '')
      data.append('pin_code', '')
      data.append('city', '')
      data.append('state', '')
      data.append('country', '')
      data.append('is_now', is_now)
      data.append('is_allday', is_allday)
      data.append('start_date', this.state.switchnow == true ? String(this.state.default) : String(this.state.date))
      data.append('end_date',this.state.switchnow == true ? String(this.state.dateNow) : String(this.state.date1))

       //alert(JSON.stringify(data))
       //return
      
      fetch(API_ROOT + 'add/suggestion', {
          method: 'post',
          body: data
      }).then((response) => response.json())
          .then((responseData) => {

              if (responseData.success) {
                  //alert(JSON.stringify(responseData.data))
                  const MixpanelData = {
                    '$email': store.getState().auth.user.email_address,
                    'POST ID': this.state.plan_id,
                  }
                  this.mixpanel.identify(store.getState().auth.user.email_address);
                  this.mixpanel.track('Suggest Date & Time', MixpanelData);
                  if (this.state.is_direct == true)
                  {
                    this.props.navigation.navigate('SuggestDateTime', { refreshing: true, other_id: this.state.other_id, plan_id: this.state.plan_id })
                  }
                  if (this.state.is_from_event == true)
                  {
                    this.props.navigation.navigate('SuggestDateTime', { refreshing: true, other_id: this.state.other_id, plan_id: this.state.plan_id, is_from_event:this.state.is_from_event,item:this.state.item })
                  }
                  else
                  {
                    this.props.navigation.navigate('SuggestDateTime', { refreshing: true })
                  }
                  RNProgressHUB.dismiss();
    
              } else {
                  if (responseData.text === 'Invalid token key') {
                      alert(responseData.text)
                      this.props.navigation.navigate('SuggestDateTime', { refreshing: true })
                    }
                  RNProgressHUB.dismiss();
              }
          })
          .catch((error) => {
              RNProgressHUB.dismiss();
            
            alert(JSON.stringify(error))

          })
    

  }


  render() {

    return (
      <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#EFEFF4' }} >
        <Toastt ref="toast"></Toastt>
        <BackgroundImage >
          <SafeAreaView />


          <Content>
            <View style={{ height: Height(2) }} />
            <View style={{ backgroundColor: colors.white }}>
              <TouchableOpacity style={{ backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ margin: Width(4), color: colors.whatFontColor, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>All day</Text>
                {
                  Platform.OS == 'ios' ?
                  <Switch
                  style={{ position: 'absolute', right: Width(4) }}
                  trackColor={{ true: "#4BD964" }}
                  ios_backgroundColor = {'#E4E4E5'}
                  onValueChange={this.toggleSwitch1}
                  value={this.state.switch1Value} />
                  :
                  <Switch
                  style={{ position: 'absolute', right: Width(4) }}
                  trackColor={{ true: "#4BD964" }}
                  thumbTintColor='#ffffff'
                  onValueChange={this.toggleSwitch1}
                  value={this.state.switch1Value} />
                }
                
              </TouchableOpacity>
              <View style={{ marginLeft: Width(4), borderBottomColor: '#CCC', borderBottomWidth: Width(0.3) }} />
            </View>
            {this.state.switch1Value == false ?
              <View style={{ backgroundColor: colors.white }}>

                <View style={{ backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={{ margin: Width(4), color: colors.whatFontColor, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Starts</Text>
                  {Platform.OS == 'android' ?
                    // <DatePicker
                    //   style={{ width: 200, position: 'absolute', right: Width(1), }}
                    //   date={this.state.date}
                    //   mode="datetime"
                    //   format="ll  LT"
                    //   minDate={new Date()}
                    //   androidMode='spinner'
                    //   placeholder="select date"
                    //   confirmBtnText="Confirm"
                    //   cancelBtnText="Cancel"
                    //   showIcon={false}
                    //   customStyles={{
                    //     dateText: {
                    //       fontSize: FontSize(16)
                    //     },
                    //     dateInput: {
                    //       position: 'absolute', right: Width(4),
                    //       borderColor: '#fff',
                    //     }
                    //   }}
                    //   onDateChange={(date) => {
                    //     var Newdate = new Date(date)
                    //     Newdate.setHours(Newdate.getHours() + 1, Newdate.getMinutes());
                    //     this.setState({ date: new Date(date), date1: Newdate })
                    //   }} /> 
                      <Text
                      onPress={() => this.setState({ showDatePicker: !this.state.showDatePicker, showDatePicke1: false, showDatePicker3: false, date: this.state.date })}
                       style={[this.state.showDatePicker ? styles.textColor : styles.textUnColor]}>{dateFormat(this.state.date, "mmm dd, yyyy   hh:MM TT")}</Text>
                    :
                    <Text
                      onPress={() => this.setState({ showDatePicker: !this.state.showDatePicker, showDatePicke1: false, showDatePicker3: false, date: this.state.date })}
                       style={[this.state.showDatePicker ? styles.textColor : styles.textUnColor]}>{dateFormat(this.state.date, "mmm dd, yyyy   hh:MM TT")}</Text>}
                </View>
                {
                  Platform.OS == 'android' ?
                  this.state.showDatePicker ?
                  <View style = {{alignItems:'center',justifyContent:'center',backgroundColor:'#fff'}}>
                    <DatePicker
                      date={this.state.date}
                      mode = 'datetime'
                        onDateChange={(date) => {  
                        var Newdate = new Date(date)
                        Newdate.setHours(Newdate.getHours() + 1, Newdate.getMinutes());
                        this.setState({ date: date, date1:Newdate })
                      }}
                    />
                  </View>
                    : 
                  <View />
                  :
                this.state.showDatePicker ?
                  <DatePickerIOS
                    style={{ flex: 1, height: 200, marginBottom: 20, backgroundColor: colors.white }}
                    date={this.state.date}
                    minimumDate={new Date()}
                    onDateChange={(date) => {
                      var Newdate = new Date(date)
                      Newdate.setHours(Newdate.getHours() + 1, Newdate.getMinutes());
                      this.setState({ date: date, date1: Newdate })
                    }}
                    mode="datetime" /> 
                  : <View />
                }

                <View style={{ marginLeft: Width(4), borderBottomColor: '#CCC', borderBottomWidth: Width(0.3) }} />
                <View style={{ backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={{ margin: Width(4), color: colors.whatFontColor, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Ends</Text>
                  {Platform.OS == 'android' ?
                    // <DatePicker
                    //   style={{ width: 200, position: 'absolute', right: Width(1), }}
                    //   date={this.state.date1}
                    //   mode="datetime"
                    //   androidMode='spinner'
                    //   placeholder="select date"
                    //   format="ll  LT"
                    //   confirmBtnText="Confirm"
                    //   cancelBtnText="Cancel"
                    //   showIcon={false}
                    //   customStyles={{
                    //     dateText: {
                    //       fontSize: FontSize(16)
                    //     },
                    //     dateInput: {
                    //       position: 'absolute', right: Width(4),
                    //       borderColor: '#fff',
                    //     }
                    //   }}
                    //   onDateChange={(date1) => { this.setState({ date1: new Date(date1) }) }} /> 
                    <Text
                      onPress={() => this.setState({ showDatePicke1: !this.state.showDatePicke1, showDatePicker: false, showDatePicker3: false, date1: this.state.date1 })}
                      style={[this.state.showDatePicke1 ? styles.textColor : styles.textUnColor]}>{dateFormat(this.state.date1, "mmm dd, yyyy   hh:MM TT")}</Text>
                    :
                    <Text
                      onPress={() => this.setState({ showDatePicke1: !this.state.showDatePicke1, showDatePicker: false, showDatePicker3: false, date1: this.state.date1 })}
                      style={[this.state.showDatePicke1 ? styles.textColor : styles.textUnColor]}>{dateFormat(this.state.date1, "mmm dd, yyyy   hh:MM TT")}</Text>}
                </View>
                {this.state.showDatePicke1 ? 
                Platform.OS == 'android' ?
                <View style = {{alignItems:'center',justifyContent:'center',backgroundColor:'#fff'}}>
                  <DatePicker
                  date={this.state.date1}
                  minimumDate = {this.state.date}
                  mode = 'datetime'
                  onDateChange={(date1) => { this.setState({ date1: date1 }) }}
                  />
                </View>
                :
                <DatePickerIOS
                  style={{ flex: 1, height: 200, marginBottom: 20, backgroundColor: colors.white }}
                  minimumDate={this.state.date}
                  date={this.state.date1}
                  onDateChange={(date1) => { this.setState({ date1: date1 }) }}
                  mode="datetime" /> 
                : 
                <View />
                }
              </View> 
              :
              <View>
                <View style={{ backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={{ margin: Width(4), color: colors.whatFontColor, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Starts</Text>
                  {Platform.OS == 'android' ?
                    // <DatePicker
                    //   style={{ width: 200, position: 'absolute', right: Width(1), }}
                    //   date={this.state.date}
                    //   mode="date"
                    //   format="ddd, ll"
                    //   minDate={new Date()}
                    //   androidMode='spinner'
                    //   placeholder="select date"
                    //   confirmBtnText='Cinform'
                    //   cancelBtnText="Cancel"
                    //   showIcon={false}
                    //   customStyles={{
                    //     dateText: {
                    //       fontSize: FontSize(16)
                    //     },
                    //     dateInput: {
                    //       position: 'absolute', right: Width(4),
                    //       borderColor: '#fff',
                    //     }
                    //   }}
                    //   onDateChange={(date) => { this.setState({ date: date, date1: date }) }} /> 
                    <Text
                      onPress={() => this.setState({ showDatePicker: !this.state.showDatePicker, showDatePicke1: false, showDatePicker3: false, date: this.state.date, date1: this.state.date })}
                       style={[this.state.showDatePicker ? styles.textColor : styles.textUnColor]}>{dateFormat(this.state.date, "ddd, mmm d, yyyy")}</Text>
                    :
                    <Text
                      onPress={() => this.setState({ showDatePicker: !this.state.showDatePicker, showDatePicke1: false, showDatePicker3: false, date: this.state.date, date1: this.state.date })}
                       style={[this.state.showDatePicker ? styles.textColor : styles.textUnColor]}>{dateFormat(this.state.date, "ddd, mmm d, yyyy")}</Text>}
                </View>
                {this.state.showDatePicker ? 
                Platform.OS == 'android' ?
                <View style = {{alignItems:'center',justifyContent:'center',backgroundColor:'#fff'}}>
                  <DatePicker
                    date={this.state.date}
                    minimumDate = {new Date()}
                    mode = 'datetime'
                    onDateChange={(date) => { this.setState({ date: date,date1:date }) }}
                  />
                </View>
                :
                <DatePickerIOS
                  style={{ flex: 1, height: 200, marginBottom: 20, backgroundColor: colors.white }}
                  minimumDate={new Date()}
                  date={this.state.date} onDateChange={(date) => this.setState({ date })}
                  mode="date" /> 
                  : 
                  <View />
                }

                <View style={{ borderBottomColor: '#CCC', borderBottomWidth: Width(0.3) }} />
                <View style={{ backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={{ margin: Width(4), color: colors.whatFontColor, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Ends</Text>
                  {Platform.OS == 'android' ?
                    // <DatePicker
                    //   style={{ width: 200, position: 'absolute', right: Width(1), }}
                    //   date={this.state.date1}
                    //   mode="date"
                    //   format="ddd, ll"
                    //   minDate={this.state.date}
                    //   androidMode='spinner'
                    //   placeholder="select date"
                    //   confirmBtnText="Confirm"
                    //   cancelBtnText="Cancel"
                    //   showIcon={false}
                    //   customStyles={{
                    //     dateText: {
                    //       fontSize: FontSize(16)
                    //     },
                    //     dateInput: {
                    //       position: 'absolute', right: Width(4),
                    //       borderColor: '#fff',
                    //     }
                    //   }}
                    //   onDateChange={(date1) => { this.setState({ date1: date1 }) }} /> 
                    <Text
                      onPress={() => this.setState({ showDatePicke1: !this.state.showDatePicke1, showDatePicker: false, showDatePicker3: false, date1: this.state.date1 })}
                      style={[this.state.showDatePicke1 ? styles.textColor : styles.textUnColor]}>{dateFormat(this.state.date1, "ddd, mmm d, yyyy")}</Text>
                    :
                    <Text
                      onPress={() => this.setState({ showDatePicke1: !this.state.showDatePicke1, showDatePicker: false, showDatePicker3: false, date1: this.state.date1 })}
                      style={[this.state.showDatePicke1 ? styles.textColor : styles.textUnColor]}>{dateFormat(this.state.date1, "ddd, mmm d, yyyy")}</Text>}
                </View>
                {this.state.showDatePicke1 ? 
                Platform.OS == 'android' ?
                <View style = {{alignItems:'center',justifyContent:'center',backgroundColor:'#fff'}}>
                  <DatePicker
                      date={this.state.date1}
                      mode = 'datetime'
                      onDateChange={(date1) => { this.setState({ date1:date1 }) }}
                    />
                </View>
                :
                <DatePickerIOS
                  style={{ flex: 1, height: 200, marginBottom: 20, backgroundColor: colors.white }}
                  minimumDate={this.state.date}
                  date={this.state.date1} onDateChange={(date1) => this.setState({ date1 })}
                  mode="date" /> 
                  : 
                  <View />
                }
              </View>}

            <View style={{ height: Height(3) }} />
            <View style={{ backgroundColor: colors.white }}>
              <TouchableOpacity style={{ backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ margin: Width(4), color: colors.whatFontColor, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Now</Text>
                {
                  Platform.OS == 'ios' ?
                  <Switch
                  style={{ position: 'absolute', right: Width(4) }}
                  trackColor={{ true: "#4BD964" }}
                  ios_backgroundColor = {'#E4E4E5'}
                  onValueChange={this.toggleSwitchnow}
                  value={this.state.switchnow} />
                  :
                  <Switch
                  style={{ position: 'absolute', right: Width(4) }}
                  trackColor={{ true: "#4BD964" }}
                  thumbTintColor='#ffffff'
                  onValueChange={this.toggleSwitchnow}
                  value={this.state.switchnow} />
                }
                
              </TouchableOpacity>

            </View>
            <View style={{ backgroundColor: colors.white }}>
              {this.state.switchnow ? <View style={{ marginLeft: Width(4), borderBottomColor: '#CCC', borderBottomWidth: Width(0.3) }} /> : null}</View>

            {/* {Platform.OS == 'android' && this.state.switchnow ?
              <View style={{ backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ margin: Width(4), color: colors.whatFontColor, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Until</Text>
                <DatePicker
                  style={{ width: 200, position: 'absolute', right: Width(1), }}
                  date={this.state.dateNow}
                  mode="datetime"
                  minDate={new Date()}
                  androidMode='spinner'
                  placeholder="select date"
                  format="llll"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  showIcon={false}
                  customStyles={{
                    dateText: {
                      fontSize: FontSize(16)
                    },
                    dateInput: {
                      position: 'absolute', right: Width(4),
                      borderColor: '#fff',
                    }
                  }}
                  onDateChange={(dateNow) => { this.setState({ dateNow: dateNow }) }}
                />
              </View>
              : null} */}
            {/* {Platform.OS == 'ios' && this.state.switchnow ? */}
            {this.state.switchnow ?
              <View style={{ backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ margin: Width(4), color: colors.whatFontColor, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Until</Text>
                <Text
                  onPress={() => this.setState({
                    showDatePicker3: !this.state.showDatePicker3,
                    showDatePicke1: false,
                    showDatePicker: false,
                    dateNow: this.state.dateNow
                  })}
                  style={[this.state.showDatePicker3 ? styles.textColor : styles.textUnColor]}>{moment(this.state.dateNow).format('llll')}</Text>
              </View>
              : null}

            {this.state.showDatePicker3 && this.state.switchnow == true ? 
            Platform.OS == 'android' ?
            <View style = {{alignItems:'center',justifyContent:'center', backgroundColor:'#fff'}}>
            <DatePicker
              date={this.state.dateNow}
              minimumDate = {new Date()}
              mode = 'datetime'
              onDateChange={(dateNow) => { this.setState({ dateNow: dateNow}) }}
            />
            </View>
            :
            <DatePickerIOS
              minimumDate={new Date()}
              style={{ flex: 1, height: 200, marginBottom: Height(10), marginTop: -10, backgroundColor: colors.white }}
              date={this.state.dateNow} onDateChange={(dateNow) => this.setState({ dateNow })}
              mode="datetime" /> 
              : 
            <View />
            }

          </Content>
          <TouchableOpacity style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }} onPress={this.goBack} >
            <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Select</Text>
            </ImageBackground>
          </TouchableOpacity>
          <SafeAreaView />
        </BackgroundImage>
      </Container>
    );
  }
}

export default DateTime;
