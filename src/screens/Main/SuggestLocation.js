import React, { Component } from "react";
import { SafeAreaView, StyleSheet, Image, ScrollView, FlatList, ImageBackground, TouchableOpacity, StatusBar, TouchableHighlight, Text, Platform, Alert } from "react-native";
import { Container, View, Button, Header, Left, Right, Content, Item, Body } from "native-base";
import ValidationComponent from 'react-native-form-validator';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
import RNProgressHUB from 'react-native-progresshub';

const btnBg = require("../../../assets/images/btnBg.png");
import { API_ROOT } from "../../config/constant";
import { store } from "../../redux/store";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
const add = require("../../../assets/images/Dashboard/add.png");
const blue_left = require("../../../assets/images/Dashboard/blue-left.png");
import { isIphoneX } from '../Default/is-iphone-x'
const HEADER_SIZE = isIphoneX() ? 25 : 39;
const HEADER_PADDING_SIZE = isIphoneX() ? 40 : 16;
class SuggestLocation extends ValidationComponent {
    _didFocusSubscription
    constructor(props) {
        super(props);
        this.state = {
            token: store.getState().auth.user.token,
            login_id: store.getState().auth.user.login_id,
            other_id: this.props.navigation.state.params.other_id,
            plan_id: this.props.navigation.state.params.plan_id,
            suggestLocationList:[],
            refreshing: false,
            common_id:'',
            value: null,
            is_from_event : this.props.navigation.state.params.is_from_event,
            item:this.props.navigation.state.params.item,
            notificaitonid:this.props.navigation.state.params.notificaitonid,
        },
        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            //this.onRefresh()
            this.getSuggestedtLocation()
            this.readNotification()
        });

    }

    getSuggestedtLocation() {
        RNProgressHUB.showSpinIndeterminate();

        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('plan_id', this.state.plan_id)
        data.append('type', 'location')
        data.append('limit', '')
        data.append('offset', '')
        this.setState({ refreshing: true })

        fetch(API_ROOT + 'suggestion/list', {
            method: 'post',
            body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {
                    let data = responseData.data
                    //console.log('SUGGESTED LOCATIONS '+JSON.stringify(data))
                    this.setState({ suggestLocationList: data, refreshing: false })
                    RNProgressHUB.dismiss();

                } else {
                    if (responseData.text === 'Invalid token key') {
                        alert(responseData.text)
                        this.props.navigation.navigate('Login')
                    }
                    this.setState({ refreshing: false })
                    RNProgressHUB.dismiss();
                }
            })

            .catch((error) => {
                this.setState({ refreshing: false })

                RNProgressHUB.dismiss();

            })

    }
    componentWillReceiveProps(props) {
        if (props.navigation.state.params != null) {
            if (props.navigation.state.params.refreshing == true) {
                this.onRefresh()
            }
        }

    }
    componentDidMount() {
        this._navListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBarStyle('dark-content');
            Platform.OS == 'android' && StatusBar.setBackgroundColor('transparent');
        });
        
        //this.getSuggestedtLocation()
    }

    componentWillUnmount() {
       
        this._navListener.remove();
    }

    readNotification = () => {

        if(this.state.notificaitonid != '')
        {
          var data = new FormData()
          data.append('token', this.state.token)
          data.append('notification_id', this.state.notificaitonid)
          
          fetch(API_ROOT + 'notification/read', {
            method: 'post',
            body: data
          }).then((response) => response.json())
            .then((responseData) => {
              if (responseData.success) {
                
              } else {
                
              }
            })
            .catch((error) => {
              //alert(error)
            })
        }
      }

    FinalsuggestPlan  () {
        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('plan_id', this.state.plan_id)
        data.append('type', 'location')
        data.append('common_id',this.state.common_id)
        this.setState({ refreshing: true })

        fetch(API_ROOT + 'suggestion/final', {
            method: 'post',
            body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {
                    if(this.state.is_from_event == true)
                    {
                        this.props.navigation.navigate('EventDetail', { item: this.state.item})
                    }
                    else
                    {
                        this.props.navigation.navigate('dashboard', { refreshing: true })
                    }
                } else {
                    this.setState({ refreshing: false })
                }
            })

            .catch((error) => {
                this.setState({ refreshing: false })


            })

    }
    suggestPlan=(item)=>{
        RNProgressHUB.showSpinIndeterminate();
        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('plan_id', this.state.plan_id)
        data.append('type', 'location')
        data.append('common_id',this.state.common_id)
        this.setState({ refreshing: true })

        fetch(API_ROOT + 'suggestion/vote/add', {
            method: 'post',
            body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {

                    if(this.state.is_from_event == true)
                    {
                        this.props.navigation.navigate('EventDetail', { item: this.state.item})
                    }
                    else
                    {
                        this.props.navigation.navigate('dashboard', { refreshing: true })
                    }
                    RNProgressHUB.dismiss();

                } else {

                    if(this.state.is_from_event == true)
                    {
                        this.props.navigation.navigate('EventDetail', { item: this.state.item})
                    }
                    else
                    {
                        this.props.navigation.navigate('dashboard', { refreshing: true })
                    }

                    if (responseData.text === 'Invalid token key') {
                        this.props.navigation.navigate('Login')
                    }
                    this.setState({ refreshing: false })
                    RNProgressHUB.dismiss();
                }
            })

            .catch((error) => {
                this.setState({ refreshing: false })

                RNProgressHUB.dismiss();

            })

    }
    deleteSuggetion = (item) => {
        RNProgressHUB.showSpinIndeterminate();
        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('plan_id', this.state.plan_id)
        data.append('type', 'location')
        data.append('common_id', item.location_id)
        this.setState({ refreshing: true })

        fetch(API_ROOT + 'suggestion/delete', {
            method: 'post',
            body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {
                    this.onRefresh()
                    RNProgressHUB.dismiss();

                } else {
                    if (responseData.text === 'Invalid token key') {
                        this.props.navigation.navigate('Login')
                    }
                    this.setState({ refreshing: false })
                    RNProgressHUB.dismiss();
                }
            })

            .catch((error) => {
                this.setState({ refreshing: false })

                RNProgressHUB.dismiss();

            })

    }
    _renderItem = ({ item }) => {
        const { value } = this.state;

        return (
            <TouchableOpacity  onPress={() =>this.setState({common_id:item.location_id, value: item.location_id})}
            style={value == item.location_id ? styles.box1:styles.box2}>
                <View style={{ flex: 1 }}>
                    <Text style={styles.activity}>{item.line_1}</Text>
                    <Text style={styles.suggestAct}>Suggested by {item.user_name}</Text>

                </View>
                <View style={{flexDirection:'row'}}>
                {(item.is_vote == 0 && item.created_id != this.state.login_id) ?null :
                        <View>{item.total_vote == 1 ? <Text style={styles.suggestVote}>{item.total_vote} vote</Text>
                            : <Text style={styles.suggestVote}>{item.total_vote} votes</Text>}
                        </View>
                        }
                    { (item.login_id == this.state.login_id && item.total_vote <= 0)?
                        <TouchableOpacity
                        style={{marginLeft:Width(1)}}
                            onPress={() => {
                                Alert.alert(
                                '',
                                'Are you sure you want to delete suggested location?',
                                [
                                    {
                                        text: 'Cancel',
                                        onPress: () => console.log('Cancel Pressed'),
                                        style: 'cancel',
                                    },
                                    { text: 'OK', onPress: () => this.deleteSuggetion(item)},
                                ],
                      { cancelable: false },
                    );
                  }}
                        >
                            <MaterialIcons name='delete' size={20} color='red'/>
                        </TouchableOpacity>
                        : null}
                </View>
            </TouchableOpacity>
        )
    }
    onRefresh = () => {
        this.setState({ refreshing: true });
        this.getSuggestedtLocation()

    }
    goBack=()=>{
        if(this.state.login_id == this.state.other_id)
        {
            this.FinalsuggestPlan()
        }else{
            this.suggestPlan()
        }
    }
    render() {
        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }} >
                <Toastt ref="toast"></Toastt>
                <BackgroundImage >

                    <SafeAreaView />
                    <View style={{ height: Height(1), backgroundColor: colors.white }}></View>

                    <Content>
                        <View style={{ height: Height(5), alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                            <TouchableOpacity
                                style={{ left: Width(3), position: 'absolute' }}
                                onPress={() => { this.props.navigation.goBack() }}>
                                <Image source={blue_left} />
                            </TouchableOpacity>
                            <Text style={{ textAlign: 'center', fontSize: FontSize(20), color: colors.appColor, fontFamily: fonts.Raleway_Medium }}>Suggested Locations</Text>
                            <TouchableOpacity
                                style={{ right: Width(3), position: 'absolute' }}
                                onPress={() => { this.props.navigation.navigate('dashboard') }}>
                            <Text style={{ textAlign: 'center', fontSize: FontSize(15), color: colors.appColor, fontFamily: fonts.Raleway_Medium }}>Done</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ alignSelf: 'center', width: '80%' }}>
                        {this.state.login_id == this.state.other_id ?
                            <Text style={{ textAlign: 'center', fontSize: FontSize(16), fontFamily: fonts.Raleway_Regular, color: colors.black,lineHeight:25}}>
                           Select from the list below to set the location.</Text>:
                            <Text style={{ textAlign: 'center', fontSize: FontSize(16), fontFamily: fonts.Raleway_Regular, color: colors.black,lineHeight:25 }}>
                            Your friends have suggested the following locations. Select one of the options below or make a suggestion.</Text>}
                        </View>

                       
                        <FlatList
                            style={{}}
                            data={this.state.suggestLocationList}
                            //refreshing={this.state.refreshing} onRefresh={this.onRefresh}
                            renderItem={
                                this._renderItem
                            }

                            keyExtractor={(item, index) => index.toString()}
                        />



                        <View style={{ height: Height(2) }} />
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Location', { plan_id: this.state.plan_id })}
                            style={{ alignItems: 'center', flexDirection: 'row' }}>

                            <Image style={{ left: Width(3), position: 'absolute' }} source={add} />

                            <Text style={{ marginLeft: Width(10), fontSize: FontSize(16), color: colors.blue, fontFamily: fonts.Roboto_Regular }}>Suggest another location</Text>
                        </TouchableOpacity>
                    </Content>
                    {this.state.common_id == '' ?
                     <View style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }}>
                        <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                            {this.state.login_id === this.state.other_id ?
                                <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Set Location</Text>
                                :
                                <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Vote</Text>

                            }
                        </ImageBackground>
                    </View>
                    :
                    <TouchableOpacity style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }} onPress={this.goBack} >
                        <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                            {this.state.login_id === this.state.other_id ?
                                <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Set Location</Text>
                                :
                                <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Vote</Text>

                            }
                        </ImageBackground>
                    </TouchableOpacity>}
                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    box1: {
        flexDirection: 'row',
        paddingHorizontal: Width(5),
        marginTop: Height(2),
        paddingVertical:Height(2),
        width: Width(93),
        borderColor:colors.pink,
        borderWidth: 0.5,
        borderRadius: 35,
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor:'#FDF4FD'
    },
    box2: {
        flexDirection: 'row',
        paddingHorizontal: Width(5),
        marginTop: Height(2),
        paddingVertical:Height(2),
        width: Width(93),
        borderColor: '#E8E8E8',
        borderWidth: 0.5,
        borderRadius: 35,
        alignItems: 'center',
        alignSelf: 'center',
        
    },
    activity: {
        fontSize: FontSize(16),
        fontFamily: fonts.Roboto_Bold,
        color: colors.dargrey
    },
    suggestAct: {
        fontSize: FontSize(14),
        marginTop:3,
        letterSpacing:0.7,

        fontFamily: fonts.Roboto_Regular,
        color: colors.fontDarkGrey
    },
    suggestVote: {
        fontSize: FontSize(18),
        fontFamily: fonts.Roboto_Regular,
        color: colors.dargrey
    },
    headerAndroidnav: {
        backgroundColor: '#41199B',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        height:isIphoneX() ? 85 : 64,
        ...Platform.select({
          ios: {
            height: HEADER_SIZE,
            paddingTop: HEADER_PADDING_SIZE,
          },
          android: {
    
            height: HEADER_SIZE,
            paddingTop: HEADER_PADDING_SIZE,
          },
        }),
      },

});
export default SuggestLocation;