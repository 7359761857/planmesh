import React, { Component } from "react";
import { SafeAreaView, StyleSheet, Image, ScrollView, FlatList, ImageBackground, TouchableOpacity, StatusBar, TouchableHighlight, Text, Platform, Alert } from "react-native";
import { Container, View, Button, Header, Left, Right, Content, Item, Body } from "native-base";
import ValidationComponent from 'react-native-form-validator';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
const btnBg = require("../../../assets/images/btnBg.png");
import { API_ROOT } from "../../config/constant";
import { store } from "../../redux/store";
import RNProgressHUB from 'react-native-progresshub';

import AntDesign from 'react-native-vector-icons/AntDesign';
const add = require("../../../assets/images/Dashboard/add.png");
const blue_left = require("../../../assets/images/Dashboard/blue-left.png");
import { isIphoneX } from '../Default/is-iphone-x'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';

const HEADER_SIZE = isIphoneX() ? 25 : 39;
const HEADER_PADDING_SIZE = isIphoneX() ? 40 : 16;
class SuggestDateTime extends ValidationComponent {
    _didFocusSubscription
    constructor(props) {
        super(props);
        this.state = {
            token: store.getState().auth.user.token,
            login_id: store.getState().auth.user.login_id,
            other_id: this.props.navigation.state.params.other_id,
            plan_id: this.props.navigation.state.params.plan_id,
            suggestDateTimeList: [],
            refreshing: false,
            common_id:'',
            value: null,
            is_from_event : this.props.navigation.state.params.is_from_event,
            item:this.props.navigation.state.params.item,
            notificaitonid:this.props.navigation.state.params.notificaitonid,
        },
        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            //this.onRefresh()
            this.getSuggestedtTime()
            this.readNotification()
        });

    }

    getSuggestedtTime() {
        RNProgressHUB.showSpinIndeterminate();

        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('plan_id', this.state.plan_id)
        data.append('type', 'datetime')
        data.append('limit', '')
        data.append('offset', '')
        this.setState({ refreshing: true })

        //console.log('SUGGESTED DATETIME REQUEST: '+JSON.stringify(data))

        fetch(API_ROOT + 'suggestion/list', {
            method: 'post',
            body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {
                    let data = responseData.data
                    //console.log('SUGGESTED DATETIME: '+JSON.stringify(data))
                    this.setState({ suggestDateTimeList: data, refreshing: false })
                    RNProgressHUB.dismiss();

                } else {
                    if (responseData.text === 'Invalid token key') {
                        alert(responseData.text)
                        this.props.navigation.navigate('Login')
                    }
                    this.setState({ refreshing: false, suggestDateTimeList:[]})
                    RNProgressHUB.dismiss();
                }
            })

            .catch((error) => {
                this.setState({ refreshing: false })

                RNProgressHUB.dismiss();

            })

    }
    componentWillReceiveProps(props) {
        if (props.navigation.state.params != null) {
            if (props.navigation.state.params.refreshing == true) {
                this.onRefresh()
            }
        }

    }
    componentDidMount() {
        this._navListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBarStyle('dark-content');
            Platform.OS == 'android' && StatusBar.setBackgroundColor('transparent');
        });
       
       // this.getSuggestedtTime()


    }

    componentWillUnmount() {

        this._navListener.remove();
    }

    readNotification = () => {

        if(this.state.notificaitonid != '')
        {
          RNProgressHUB.showSpinIndeterminate();
          var data = new FormData()
          data.append('token', this.state.token)
          data.append('notification_id', this.state.notificaitonid)
          
          fetch(API_ROOT + 'notification/read', {
            method: 'post',
            body: data
          }).then((response) => response.json())
            .then((responseData) => {
              if (responseData.success) {
                
              } else {
                
              }
            })
            .catch((error) => {
              //alert(error)
            })
        }
      }

    FinalsuggestPlan  () {
        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('plan_id', this.state.plan_id)
        data.append('type', 'datetime')
        data.append('common_id',this.state.common_id)
        this.setState({ refreshing: true })

        fetch(API_ROOT + 'suggestion/final', {
            method: 'post',
            body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {
                    if(this.state.is_from_event == true)
                    {
                        this.props.navigation.navigate('EventDetail', { item: this.state.item})
                    }
                    else
                    {
                        this.props.navigation.navigate('dashboard', { refreshing: true })
                    }
                } else {
                    this.setState({ refreshing: false })

                }
            })

            .catch((error) => {
                this.setState({ refreshing: false })
                

            })

    }
    suggestPlan  ()  {
        RNProgressHUB.showSpinIndeterminate();
        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('plan_id', this.state.plan_id)
        data.append('type', 'datetime')
        data.append('common_id', this.state.common_id)
        this.setState({ refreshing: true })

        fetch(API_ROOT + 'suggestion/vote/add', {
            method: 'post',
            body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {
                    if(this.state.is_from_event == true)
                    {
                        this.props.navigation.navigate('EventDetail', { item: this.state.item})
                    }
                    else
                    {
                        this.props.navigation.navigate('dashboard', { refreshing: true })
                    }
                    RNProgressHUB.dismiss();

                } else {
                    if(this.state.is_from_event == true)
                    {
                        this.props.navigation.navigate('EventDetail', { item: this.state.item})
                    }
                    else
                    {
                        this.props.navigation.navigate('dashboard', { refreshing: true })
                    }

                    RNProgressHUB.dismiss();
                }
            })

            .catch((error) => {
                this.setState({ refreshing: false })

                RNProgressHUB.dismiss();

            })

    }
    deleteSuggetion = (item) => {
        RNProgressHUB.showSpinIndeterminate();
        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('plan_id', this.state.plan_id)
        data.append('type', 'datetime')
        data.append('common_id', item.datetime_id)
        this.setState({ refreshing: true })

        fetch(API_ROOT + 'suggestion/delete', {
            method: 'post',
            body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {
                    //alert(responseData.text)
                    this.onRefresh()
                    RNProgressHUB.dismiss();

                } else {
                    if (responseData.text === 'Invalid token key') {
                        this.props.navigation.navigate('Login')
                    }
                    this.onRefresh()
                    this.setState({ refreshing: false })
                    RNProgressHUB.dismiss();
                }
            })

            .catch((error) => {
                this.setState({ refreshing: false })

                RNProgressHUB.dismiss();

            })

    }
    goBack=()=>{
        if(this.state.login_id == this.state.other_id)
        {
            this.FinalsuggestPlan()
        }else{
            this.suggestPlan()
        }
    }

    _renderItem = ({ item }) => {
        const { value } = this.state;
    let Final_date = ''
    let dateTime = '';
    var currentDate = moment().format("ll");
    var tomorrow = moment().add(1, 'days').format("ll")


    var start = item.start_date.split(" ")
    var end = item.end_date.split(" ")

    var gmtDateTime = moment.utc(item.created_date)
    var date1 = gmtDateTime.local();

     var strstartdate = moment.utc(item.start_date)
     var startdate = strstartdate.local();

     var strenddate = moment.utc(item.end_date)
     var enddate = strenddate.local();

    if (moment(date1).format("ll") == moment().format("ll")) {
      dateTime = date1.fromNow();
    } else {
      dateTime = date1.calendar();

    }
    var activity = ''
    var str = String(item.activity);
    if (str.includes('Wanna')) {
      activity = str.substr(6).slice(0, -1);
    } else {
      activity = str
    }
    if (activity.includes('?')) {
      activity = activity.slice(0, -1)
    }

    if (item.is_now == '1') {
      if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
        Final_date = 'Now - ' + moment(enddate).format('LT')
      } else {
        if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
          Final_date = 'Now - Tomorrow · ' + moment(this.state.date).format('LT')
        } else {
          Final_date = 'Now - ' + moment(enddate).format('ddd, MMM D \u00B7 LT')
        }

      }
    } 
    else {
      if ((currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll'))) {
        if ((start[1] == '00:00:00' && end[1] == '00:00:00') || item.is_allday == '1') {
          Final_date = 'Today · All Day'
        } else {
          Final_date = 'Today · ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT')
        }
      } else {
        if (start[1] == '00:00:00' && end[1] == '00:00:00' || item.is_allday == '1') {
          if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
            Final_date = 'Tomorrow · All Day'

          } else {
            if (tomorrow == moment(startdate).format('ll')) {
              Final_date = 'Tomorrow · All Day'

            } else {
              if (currentDate == moment(enddate).format('ll')) {
                Final_date = 'Today' + ' ·All Day '

              } else {
                Final_date = moment(startdate).format('ddd, MMM D') + ' · All Day '
              }

            }

          }
        } else {
          if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
            Final_date = 'Tomorrow · ' + moment(startdate).format('LT') +' - ' + moment(enddate).format('LT')

          } else {
            if (tomorrow == moment(startdate).format('ll')) {
              Final_date = 'Tomorrow · ' + moment(startdate).format('LT') +' - ' + moment(enddate).format('ddd, MMM D \u00B7 LT')

            } else {
              if (currentDate == moment(startdate).format('ll')) {
                Final_date = 'Today ·' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT')

              } else {
                Final_date = moment(startdate).format('ddd, MMM D \u00B7 LT') + ' - ' + moment(enddate).format('ddd, MMM D \u00B7 LT')
              }

            }

          }

        }

      }

    }

        return (
            <TouchableOpacity onPress={() =>{
                this.setState({common_id:item.datetime_id, value: item.datetime_id})
                //alert(item.start_date +'  '+ item.end_date)
            }
            }
            style={value == item.datetime_id ? styles.box1:styles.box2}>
            <View style={{ flex: 1 }}>
                    <Text style={styles.activity}>{Final_date}</Text>
                    <Text style={styles.suggestAct}>Suggested by {item.user_name}</Text>

                </View>
                <View style={{flexDirection:'row'}}>
                   
                { (item.is_vote == '0' && item.created_id != this.state.login_id)?null :
                        <View>{item.total_vote == 1 ? <Text style={styles.suggestVote}>{item.total_vote} vote</Text>
                            : <Text style={styles.suggestVote}>{item.total_vote} votes</Text>}
                        </View>}
                    
                    {(item.login_id == this.state.login_id && item.total_vote <= 0) ?
                        <TouchableOpacity
                        style={{marginLeft:Width(1)}}
                            onPress={() => {
                                Alert.alert(
                                '',
                                'Are you sure you want to delete suggested date & time?',
                                [
                                    {
                                        text: 'Cancel',
                                        onPress: () => console.log('Cancel Pressed'),
                                        style: 'cancel',
                                    },
                                    { text: 'OK', onPress: () => this.deleteSuggetion(item)},
                                ],
                      { cancelable: false },
                    );
                  }}
                        >
                            <MaterialIcons name='delete' size={20} color= 'red' />
                        </TouchableOpacity>
                        : null}
                </View>
            </TouchableOpacity>
        )
    }
    onRefresh = () => {
        this.setState({ refreshing: true });
        this.getSuggestedtTime()

    }
    render() {
        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }} >
                <Toastt ref="toast"></Toastt>
                <BackgroundImage >

                    <SafeAreaView />
                    <View style={{ height: Height(1), backgroundColor: colors.white }}></View>

                    <Content>
                        <View style={{ height: Height(5), alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                            <TouchableOpacity
                                style={{ left: Width(3), position: 'absolute' }}
                                onPress={() => { this.props.navigation.goBack() }}>
                                <Image source={blue_left} />
                            </TouchableOpacity>
                            <Text style={{ textAlign: 'center', fontSize: FontSize(20), color: colors.appColor, fontFamily: fonts.Raleway_Medium }}>Suggested Dates & Times</Text>
                            <TouchableOpacity
                                style={{ right: Width(3), position: 'absolute' }}
                                onPress={() => { this.props.navigation.navigate('dashboard') }}>
                                <Text style={{ textAlign: 'center', fontSize: FontSize(15), color: colors.appColor, fontFamily: fonts.Raleway_Medium }}>Done</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ alignSelf: 'center', width: '85%' }}>
                            {this.state.login_id === this.state.other_id ?
                                <Text style={{ textAlign: 'center', fontSize: FontSize(16), fontFamily: fonts.Raleway_Regular, color: colors.black, lineHeight: 25 }}>
                                    Select from the list below to set the date & time.</Text> :
                                <Text style={{ textAlign: 'center', fontSize: FontSize(16), fontFamily: fonts.Raleway_Regular, color: colors.black, lineHeight: 25 }}>
                                    Your friends have suggested the following dates & times. Select one of the options below or make a suggestion.</Text>
                            }
                        </View>

                        <FlatList
                            style={{}}
                            data={this.state.suggestDateTimeList}
                            //refreshing={this.state.refreshing} onRefresh={this.onRefresh}
                            renderItem={
                                this._renderItem
                            }
                            keyExtractor={(item, index) => index.toString()}
                        />


                        <View style={{ height: Height(2) }} />
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('DateTime', { plan_id: this.state.plan_id })}
                            style={{ alignItems: 'center', flexDirection: 'row' }}>

                            <Image style={{ left: Width(3), position: 'absolute' }} source={add} />

                            <Text style={{ marginLeft: Width(10), fontSize: FontSize(16), color: colors.blue, fontFamily: fonts.Roboto_Regular }}>Suggest another date & time</Text>
                        </TouchableOpacity>
                    </Content>
                    {this.state.common_id == '' ?
                     <View style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }}>
                     <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                         {this.state.login_id === this.state.other_id ?
                             <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Set Date & Time</Text>
                             :
                             <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Vote</Text>

                         }
                     </ImageBackground>
                 </View>:
                    <TouchableOpacity style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }} onPress={this.goBack} >
                        <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                            {this.state.login_id === this.state.other_id ?
                                <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Set Date & Time</Text>
                                :
                                <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Vote</Text>

                            }
                        </ImageBackground>
                    </TouchableOpacity>}
                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    box1: {
        flexDirection: 'row',
        paddingHorizontal: Width(5),
        marginTop: Height(2),
        paddingVertical:Height(2),
        width: Width(93),
        borderColor:colors.pink,
        borderWidth: 0.5,
        borderRadius: 35,
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor:'#FDF4FD'
    },
    box2: {
        flexDirection: 'row',
        paddingHorizontal: Width(5),
        marginTop: Height(2),
        paddingVertical:Height(2),
        width: Width(93),
        borderColor: '#E8E8E8',
        borderWidth: 0.5,
        borderRadius: 35,
        alignItems: 'center',
        alignSelf: 'center'
    },
    activity: {
        fontSize: FontSize(16),
        fontFamily: fonts.Roboto_Bold,
        color: colors.dargrey
    },
    suggestAct: {
        fontSize: FontSize(14),
        marginTop: 3,
        letterSpacing: 0.7,
        fontFamily: fonts.Roboto_Regular,
        color: colors.fontDarkGrey
    },
    suggestVote: {
        fontSize: FontSize(16),
        fontFamily: fonts.Roboto_Regular,
        color: colors.dargrey
    },
    headerAndroidnav: {
        backgroundColor: '#41199B',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        height: isIphoneX() ? 85 : 64,
        ...Platform.select({
            ios: {
                height: HEADER_SIZE,
                paddingTop: HEADER_PADDING_SIZE,
            },
            android: {

                height: HEADER_SIZE,
                paddingTop: HEADER_PADDING_SIZE,
            },
        }),
    },
});
export default SuggestDateTime;