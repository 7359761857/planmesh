import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Alert, ImageBackground, TouchableOpacity, StatusBar, Platform } from "react-native";
import { Container, View, Text, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import FloatingLabel from 'react-native-floating-labels';
import { forgotPassword } from "../../redux/actions/auth";
import NavigationService from "../../services/NavigationService";
import { Height, FontSize, Width, colors } from "../../config/dimensions";
import { fonts, API_ROOT } from "../../config/constant";

let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height


const backWhite = require("../../../assets/images/backWhite.png");
const bottomRight = require("../../../assets/images/Login/bottomRight.png");
const btnBg = require("../../../assets/images/btnBg.png");
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");

class ForgotPassword extends ValidationComponent {

    constructor(props) {
        super(props);
        this.state = {
            labelStyleEmail: styles.labelInputUnFocus, styleEmail: styles.formInputUnFocus, email: "",
            btnDoneDisable: false, btnDoneImage: btnBgGrey, wsError: false,
            isEmailFocus: false,
            emailTouch: false

        };
    }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <View style={styles.headerAndroidnav}>
                    <StatusBar barStyle="light-content" backgroundColor="#41199B" />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack() }}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity></View>
                    <View style={{ bottom: Height(2), position: 'absolute', alignSelf: 'center' }}>
                        <Text style={styles.headerTitle} >Forgot password?</Text>
                    </View>
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', right: Width(4) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack() }} >
                            <Text style={{ color: 'white' }} >Cancel</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            )
        }
    }

    onBlurEmail() {
        if (this.state.email.length <= 0) {
            this.setState({
                labelStyleEmail: styles.labelInputUnFocus,
                styleEmail: styles.formInputUnFocus

            }, () => {
                this.validtion()
            })
        } else {
            //add some text
            this.emailvalidtion()
        }
        this.setState({ isEmailFocus: false, emailTouch: true })
    }
    onFocusEmail() {
        this.setState({
            labelStyleEmail: styles.labelInput,
            styleEmail: styles.formInputFocus,
            isEmailFocus: true
        }, () => {
            this.validtion()
        })
    }
    onChangeTextEmail = (email) => {
        this.setState({ email }, () => {
            this.validtion()
            this.emailvalidtion()
        })

    }

    onForgotPasswrodClick() {
        
        if(this.state.email != '')
        {

            var data = new FormData()
            data.append('email', this.state.email)
            
            fetch(API_ROOT + 'forgot_detail/get', {
            method: 'post',
            body: data
            }).then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {
                    this.props.navigation.navigate('ResetPassord', { email: this.state.email, userdetails:responseData.data })
                } else {
                    
                    Alert.alert(
                        '',
                        "Sorry, we couldn't find an account with that email address",
                        [
                          { text: 'OK'},
                        ],
                        { cancelable: false },
                      );
                }
            })
            .catch((error) => {
               // alert('Oops your connection seems off, Check your connection and try again')
            })
        }

        //this.props.navigation.push('createPassword',{email:this.state.email})
        //this.wscall(this.state.email); 
        // forgotPassword({email:this.state.email}).then(res=>{
        //     if(res.success){
        //         this.props.navigation.navigate('ResetPassord')
        //         // this.props.navigation.navigate('createPassword',{email:this.state.email})
        //     }
        //     else{
        //         this.setState({emailValidation:false,emailValidationMsg:res.text,labelStyleEmail:[styles.labelInput, styles.labelInputError]})
        //     }
        // }).catch(err=>{
        //     alert(err)
        // })
    }

    emailvalidtion = () => {
        this.validate({
            email: { email: true, required: true },
        });
        if (this.isFieldInError('email')) {
            this.setState({ emailValidation: false, emailValidationMsg:'Please enter your email address in format: yourname@example.com', labelStyleEmail: [styles.labelInput], })
            return false
        }
        this.setState({ emailValidation: true, emailValidationMsg: "" }, () => {
            this.validtion()
        })
        return true
    }
    validtion = () => {
        this.validate({
            email: { email: true },
        });
        if (this.isFieldInError('email')) {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
            return false
        }
        this.setState({ btnDoneDisable: false, btnDoneImage: btnBg })
        return true
    }


    wscall = async (email) => {
        let httpMethod = 'POST'
        let strURL = userforgotpassword
        let headers = {
            'Content-Type': 'application/json',
            'device_token': deviceToken,
            'device_type': Platform.OS === 'ios' ? 1 : 2,
            'build_version': '1.0',
        }
        let params = {
            'email': email,
        }

        APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
            console.log('Forgot Password -> ');
            console.log(resJSON)
            if (resJSON.status == 200) {
                this.props.navigation.push('createPassword', { email: email })
                return
            } else {
                this.refs.toast.show(resJSON.message);
            }
        })
    }
    render() {
        return (
            <Container flex-direction={"row"} style={{ flex: 1,backgroundColor:'#FFF' }} >
            <Toastt ref="toast"></Toastt>
            <BackgroundImage >
              <SafeAreaView />
    
              <View style={{ position: 'absolute', bottom: 0, right: 0,backgroundColor:'#FFF' }}>
                <ImageBackground source={bottomRight} style={{ width: 165, height: 143 }}>
                </ImageBackground>
              </View>
                    <Content>
                        <View style={{ marginHorizontal: Width(4) }}>
                            <Text style={{ width: Width(70), lineHeight: 28, fontSize: FontSize(18), color: colors.black, fontFamily: fonts.Raleway_Semibold, marginTop: 17 }}>
                                Enter your email below to reset your password.</Text>

                            <FloatingLabel
                                labelStyle={[this.state.labelStyleEmail, this.state.email.length == 0 || (!this.state.isEmailFocus && this.state.emailTouch) ? styles.labelInputUnFocus : '', this.state.emailValidation == false && this.state.isEmailFocus && this.state.emailTouch && this.state.email.length ? styles.labelInputError : {}]} inputStyle={styles.input}
                                inputStyle={styles.input}
                                style={[styles.formInput, this.state.styleEmail]}
                                onChangeText={(email) => this.onChangeTextEmail(email)} value={this.state.email}
                                onBlur={() => this.onBlurEmail()}
                                onFocus={() => this.onFocusEmail()}
                                keyboardType={"email-address"}
                                touch={this.state.emailTouch && this.state.email.length}
                                valid={this.state.email.length == 0 ? false : this.state.emailValidation}
                            >Email Address</FloatingLabel>
                            {
                                this.state.emailValidation == false && this.state.email.length && this.state.emailTouch && this.state.isEmailFocus ?
                                    <Text style={[{ marginLeft: 0, marginRight: 30, marginTop: 10, color: "red", fontSize: 10, fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.emailValidationMsg}</Text>
                                    : null
                            }
                            <TouchableOpacity style={{ width: '100%', marginTop: 50 }} disabled={this.state.btnDoneDisable} onPress={() => this.onForgotPasswrodClick()} >
                                <ImageBackground source={this.state.btnDoneImage} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Continue</Text>
                                </ImageBackground>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => NavigationService.goBack()} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ color: '#007aff', fontSize: 15, fontFamily: "Roboto-Regular", letterSpacing: 0.005 }}> Cancel</Text>
                            </TouchableOpacity>
                        </View>

                    </Content>

                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default ForgotPassword;
