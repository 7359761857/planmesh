import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, RefreshControl, ImageBackground, TouchableOpacity, StatusBar, Text, Platform, Animated, TouchableHighlight } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import { TabView } from 'react-native-tab-view';
import { FontSize, Height, Width, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ActionSheet from "react-native-actionsheet";
import { Badge, Icon } from 'react-native-elements'
import RNProgressHUB from 'react-native-progresshub';


import { store } from "../../redux/store";
import { API_ROOT } from "../../config/constant";
const avtar = require("../../../assets/images/Dashboard/avtar.png");
import moment from 'moment';
import 'moment/locale/en-ca'  // without this line it didn't work
import { number } from "prop-types";
import NotificationIcon from "./NotificationIcon";
import FriendRequestIcon from "./FriendRequestIcon";
import { NOTIFICATION, FRIENDREQUEST, NOTIFICATION_COUNT } from "../../redux/actions/types";
import NotificationCountIcon from "./NotificationCountIcon";
moment.locale('en-ca')
const backWhite = require("../../../assets/images/backWhite.png");
const friend_null = require("../../../assets/images/FriendList/friend_null.png");
const notification_null = require("../../../assets/images/FriendList/NotificationsEmpty.png");//Upcomign_empty
const upcoming_null = require("../../../assets/images/FriendList/Upcomign_empty.png");
const btnBgGrey = require("../../../assets/images/btnBg.png");
const chai_cup = require("../../../assets/images/Dashboard/chai_cup.png");
const location = require("../../../assets/images/Dashboard/location_home.png");
const calendar = require("../../../assets/images/Dashboard/calendar.png");
const Userpic = require("../../../assets/images/Dashboard/Userpic.png");
const Userphoto2 = require("../../../assets/images/Dashboard/Userphoto2.png");
const ic_chat = require("../../../assets/images/Dashboard/chat_icon.png");

const flowers = require("../../../assets/images/Dashboard/avtar.png");
import MixpanelManager from '../../../Analytics'
var subThis;
class Notification extends ValidationComponent {

  _didFocusSubscription

  constructor(props) {
    super(props);
    this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
    this.isApiCalled = false;
    this.state = {
      btnDoneDisable: true, btnDoneImage: btnBgGrey,
      index: this.props.navigation.state.params != null ? this.props.navigation.state.params.index : 0,
      accepted: false,
      friend_request_list: [],
      notifications_list: [],
      earlier_notifications_list: [],
      token: store.getState().auth.user.token,
      login_id: store.getState().auth.user.login_id,
      noti_count: store.getState().auth.notification_count,
      UpcomingList: [],
      request_id: '',
      status: false,
      notificaitonid: this.props.navigation.state.params != null ? this.props.navigation.state.params.notificaitonid : '',
      is_from_notification: this.props.navigation.state.params != null ? true : false,
      routes: [{ key: 'first', title: 'Notifications' }, { key: 'second', title: 'Friend Requests' }, { key: 'third', title: 'Upcoming' }]
    };
    subThis = this

    this._didFocusSubscription = props.navigation.addListener('didFocus', payload => {

      this.reloadData()
    }
    )
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
      header: null,
      //tabBarIcon: () => <NotificationIcon/>
      tabBarIcon: ({ tintColor }) => (
        <NotificationIcon tintColor={tintColor} />

        //   <View>
        //     <MaterialIcons name='notifications-none' size={25} color={tintColor} />
        //     <View style= {{backgroundColor:'red', position:'absolute',height:17,width:17,top:-2,right:-3, borderRadius:8.5, justifyContent:'center'}}>
        //     <Text style = {{alignSelf:'center', color:'#fff', fontSize:FontSize(9)}}>{store.getState().auth.notification_count}</Text>
        //     </View>
        // </View>
      )
    }
  }

  componentDidMount() {

    // this.getNotificationsList()
    // this.getFriendRequests()
    // this.getUpcomingList()
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('light-content');
      Platform.OS == 'android' && StatusBar.setBackgroundColor('#41199B');

    });
  }
  componentWillUnmount() {
    this._navListener.remove();
  }

  reloadData() {
    AsyncStorage.getItem('frienddate').then((value) => {
      if (value != null) {
        this.getFriendRequestsCount(value)
      }
      else {
        if (global.friend_request_count > 0) {
          global.noti_count = global.friend_request_count
        }
        else {
          if (this.state.index == 0) {
            global.noti_count = 0
          }

          this.getFriendRequestsCount('')

        }

        store.dispatch({ type: NOTIFICATION, data: global.noti_count })
        var date = moment.utc().subtract(5, 'seconds').format('YYYY-MM-DD HH:mm:ss');
        AsyncStorage.setItem('notidate', date)
      }

      if (this.state.is_from_notification == true) {
        this.readNotification()
        global.friend_request_count = 0
        store.dispatch({ type: FRIENDREQUEST, data: global.friend_request_count })
        var date = moment.utc().subtract(5, 'seconds').format('YYYY-MM-DD HH:mm:ss');
        AsyncStorage.setItem('frienddate', date)
      }

      if (this.state.index == 0) {
        global.noti_tab_count = 0
        store.dispatch({ type: NOTIFICATION_COUNT, data: global.noti_tab_count })
      }

    })

    if (this.state.index == 0) {
      this.getNotificationsList(false)
    }
    else if (this.state.index == 1) {
      this.getFriendRequests(false)
    }
    else {
      this.getUpcomingList()
    }


  }

  getFriendRequests(isRefresh) {
    if (!isRefresh) {
      RNProgressHUB.showSpinIndeterminate();
    }

    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    fetch(API_ROOT + 'friend_request_list', {
      method: 'post',
      body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        RNProgressHUB.dismiss()
        //alert(JSON.stringify(responseData))
        if (responseData.success) {
          AsyncStorage.getItem('frienddate').then((value) => {
            if (value == null || value == undefined) {
              global.friend_request_count = responseData.data.length
              var date = moment.utc().subtract(5, 'seconds').format('YYYY-MM-DD HH:mm:ss');
              AsyncStorage.setItem('frienddate', date)
              store.dispatch({ type: FRIENDREQUEST, data: global.friend_request_count })
            }
          })
          if (isRefresh) {
            global.friend_request_count = 0
            store.dispatch({ type: FRIENDREQUEST, data: global.friend_request_count })
            var date = moment.utc().subtract(5, 'seconds').format('YYYY-MM-DD HH:mm:ss');
            AsyncStorage.setItem('frienddate', date)
          }

          this.setState({ friend_request_list: responseData.data, request_id: responseData.data.request_id, status: responseData.success, refreshing: false })
        } else {
          this.setState({ status: responseData.success, refreshing: false })
        }
      })
      .catch((error) => {
        RNProgressHUB.dismiss()
        console.warn(error);
      })
  }

  getFriendRequestsCount(dt) {

    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('datetime', dt)

    //alert('FriendCount Request: '+ JSON.stringify(data))

    fetch(API_ROOT + 'friend_request_count', {
      method: 'post',
      body: data
    })
      .then((response) => response.json())
      .then((responseData) => {

        //alert('Noti Counts: '+JSON.stringify(responseData))

        if (responseData.success) {

          //alert('Global Counts: '+global.noti_count + ' New Count: '+responseData.noti_count)

          var date = moment.utc().subtract(5, 'seconds').format('YYYY-MM-DD HH:mm:ss');
          AsyncStorage.setItem('frienddate', date)

          if (global.friend_request_count == null || global.friend_request_count == 0 || global.friend_request_count == NaN) {
            global.friend_request_count = responseData.count
          }
          else {
            global.friend_request_count = global.friend_request_count + responseData.count
            global.noti_count = global.friend_request_count

            // if(global.noti_count > 0)
            // {
            //   global.noti_count = global.noti_count - global.friend_request_count
            // }
            // else
            // {
            //   global.noti_count = global.friend_request_count
            // }

            store.dispatch({ type: NOTIFICATION, data: global.noti_count })
            var date = moment.utc().subtract(5, 'seconds').format('YYYY-MM-DD HH:mm:ss');
            AsyncStorage.setItem('notidate', date)
          }

          store.dispatch({ type: FRIENDREQUEST, data: global.friend_request_count })

          RNProgressHUB.dismiss();

        } else {


          // if(global.noti_count > 0)
          // {
          //   global.noti_count = global.noti_count - global.friend_request_count
          // }
          // else
          // {
          //   global.noti_count = global.friend_request_count
          // }

          global.noti_count = global.friend_request_count
          store.dispatch({ type: NOTIFICATION, data: global.noti_count })
          var date = moment.utc().subtract(5, 'seconds').format('YYYY-MM-DD HH:mm:ss');
          AsyncStorage.setItem('notidate', date)

          var date = moment.utc().subtract(5, 'seconds').format('YYYY-MM-DD HH:mm:ss');
          AsyncStorage.setItem('frienddate', date)
          this.setState({ status: responseData.success })
          RNProgressHUB.dismiss();
        }
      })
      .catch((error) => {
        console.warn(error);
      })
  }

  getUpcomingList() {
    RNProgressHUB.showSpinIndeterminate();

    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    fetch(API_ROOT + 'upcomiing/plan_list', {
      method: 'post',
      body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {

          console.log('Upcoming Events: ' + JSON.stringify(responseData))

          this.setState({ UpcomingList: responseData.data })
          RNProgressHUB.dismiss();

        } else {
          this.setState({ status: responseData.success })
          RNProgressHUB.dismiss();

        }
      })
      .catch((error) => {
        RNProgressHUB.dismiss();

        console.warn(error);
      })
  }

  getNotificationsList(isRefresh) {
    if (!isRefresh) {
      RNProgressHUB.showSpinIndeterminate();
    }

    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('limit', 200)
    data.append('offset', 0)

    //alert(JSON.stringify(data))

    fetch(API_ROOT + 'notification/list', {
      method: 'post',
      body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {

          if (isRefresh) {
            global.noti_count = 0
            store.dispatch({ type: NOTIFICATION, data: global.noti_count })
            var date = moment.utc().subtract(5, 'seconds').format('YYYY-MM-DD HH:mm:ss');
            AsyncStorage.setItem('notidate', date)

            global.noti_tab_count = 0
            store.dispatch({ type: NOTIFICATION_COUNT, data: global.noti_tab_count })
          }

          this.isApiCalled = true
          this.setState({ notifications_list: responseData.new_data, earlier_notifications_list: responseData.old_data, refreshing: false })
          RNProgressHUB.dismiss();
          //this.getFriendRequests()
          //this.getUpcomingList()
        } else {
          this.isApiCalled = true
          this.setState({ status: responseData.success, refreshing: false })
          RNProgressHUB.dismiss();
          //this.getFriendRequests()
          //this.getUpcomingList()
        }
      })
      .catch((error) => {
        RNProgressHUB.dismiss();
        //this.getFriendRequests()
        //this.getUpcomingList()
        console.warn(error);
      })

  }

  renderTopBar() {
    return (
      // <View style={styles.headerAndroidnav}>
      //   <StatusBar barStyle="light-content" backgroundColor="#41199B" />
      //   <View style={{ alignItems: 'flex-start', justifyContent: 'flex-start', flex: 0.2, }} >
      //   </View>
      //   <View style={{ alignSelf: 'center', flexDirection: 'row', flex: 0.6, marginBottom:Height(2), backgroundColor:'transparent' }}>
      //     <Text style={[styles.headerTitle]} >Notifications</Text>
      //   </View>

      // </View>

      <View style={styles.headerAndroidnav}>
        <StatusBar barStyle="light-content" backgroundColor="#41199B" />
        <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
        </View>
        <View style={{ bottom: Height(2), position: 'absolute', alignSelf: 'center' }}>
          <Text style={styles.headerTitle} >Notifications</Text>
        </View>
        <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', right: Width(4) }}>
          {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('settings')} >
          <Image source={settings} resizeMode="contain" style={{ tintColor: "white" }} />
        </TouchableOpacity> */}
        </View>

      </View>

    )
  }



  tabViewIndexChange = index => {
    console.log('tab changed')
    // this.setState({ index });
    // if(index == 0)
    // {
    //     this.getNotificationsList()
    // }

  };

  addfriends = (item, index) => {

    var arrrequests = this.state.friend_request_list
    var curitem = arrrequests[index]
    curitem.is_accept = 1
    arrrequests.splice(index, 1, curitem)
    this.setState({ friend_request_list: arrrequests })

    var data = new FormData()
    data.append('token', this.state.token)
    data.append('request_id', item.request_id)
    data.append('response', '1')

    fetch(API_ROOT + 'friends_request/response', {
      method: 'post',
      body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          this.mixpanel.identify(store.getState().auth.user.email_address)
          this.mixpanel.people.increment("Accept Friend Request", 1);
          this.setState({ accepted: true })
        } else {
          //this.setState({ accepted: false })
        }
      })
      .catch((error) => {
        console.warn(error);
      })
  }

  readNotification = () => {
    //alert(this.state.notificaitonid)
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('notification_id', this.state.notificaitonid)

    fetch(API_ROOT + 'notification/read', {
      method: 'post',
      body: data
    }).then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {


        } else {


        }
      })
      .catch((error) => {


        //alert(error)
      })
  }

  onEarlierNotificationSelected(item, index) {

    //var selecteditem = item
    this.mixpanel.identify(store.getState().auth.user.email_address);
    this.mixpanel.track('Notifications - Notifications');
    this.navigateToScreen(item)

    setTimeout(() => {
      var arrnotifications = this.state.earlier_notifications_list
      var curitem = arrnotifications[index]
      curitem.is_read = 1
      arrnotifications.splice(index, 1, curitem)
      this.setState({ earlier_notifications_list: arrnotifications })
    }, 500);

  }

  onNotificationSelected(item, index) {
    this.mixpanel.identify(store.getState().auth.user.email_address);
    this.mixpanel.track('Notifications - Notifications');
    //var selecteditem = item
    this.navigateToScreen(item)

    setTimeout(() => {
      var arrnotifications = this.state.notifications_list
      var curitem = arrnotifications[index]
      curitem.is_read = 1
      arrnotifications.splice(index, 1, curitem)
      this.setState({ notifications_list: arrnotifications })
    }, 500);

  }

  navigateToScreen(item) {

    //alert(JSON.stringify(item))

    if (item.noti_type == 'is_invite') {
      //this.props.navigation.navigate('Friend_Status',{tabIndex:2, plan_id:item.common_id,notificaitonid:item.notification_id})

      if (item.plan_data.plan_type == 1) {
        var name = item.plan_data.name.split(" ")
        this.props.navigation.navigate('EventDetail', { item: item.plan_data, plan_id: item.common_id, other_id: item.plan_data.login_id, plan_type: item.plan_data.plan_type, title_header: "Let's meet up!", Final_date: '', eventby: item.plan_data.name, notificaitonid: item.notification_id })
      }
      else if (item.plan_data.plan_type == 2) {
        this.props.navigation.navigate('EventDetail', { item: item.plan_data, plan_id: item.common_id, other_id: item.plan_data.login_id, plan_type: item.plan_data.plan_type, title_header: item.plan_data.activity, Final_date: '', eventby: item.plan_data.name, notificaitonid: item.notification_id })
      }
      else if (item.plan_data.plan_type == 3) {
        this.props.navigation.navigate('EventDetail', { item: item.plan_data, plan_id: item.common_id, other_id: item.plan_data.login_id, plan_type: item.plan_data.plan_type, title_header: "Wanna go to " + item.plan_data.line_1 + "?", Final_date: '', eventby: item.plan_data.name, notificaitonid: item.notification_id })
      }
      else if (item.plan_data.plan_type == 4) {
        this.props.navigation.navigate('EventDetail', { item: item.plan_data, plan_id: item.common_id, other_id: item.plan_data.login_id, plan_type: item.plan_data.plan_type, title_header: "I'm free, wanna hangout?", Final_date: '', eventby: item.plan_data.name, notificaitonid: item.notification_id })
      }

    }
    else if (item.noti_type == 'is_intrested_my_plan') {
      this.props.navigation.navigate('Friend_Status', { tabIndex: 1, plan_id: item.common_id, notificaitonid: item.notification_id, Title_New: item.plan_data.activity, plan_type: item.plan_data.plan_type })
    }
    else if (item.noti_type == 'is_goin_my_plan') {
      this.props.navigation.navigate('Friend_Status', { tabIndex: 0, plan_id: item.common_id, notificaitonid: item.notification_id, Title_New: item.plan_data.activity, plan_type: item.plan_data.plan_type })
    }
    else if (item.noti_type == 'is_intrested_other_going') {
      this.props.navigation.navigate('Friend_Status', { tabIndex: 0, plan_id: item.common_id, notificaitonid: item.notification_id, Title_New: item.plan_data.activity, plan_type: item.plan_data.plan_type })
    }
    else if (item.noti_type == 'is_suggestion_my_plan' || item.noti_type == 'is_intrested_plan_suggestion' || item.noti_type == 'is_set_final_suggestion' || item.noti_type == 'is_upvote_my_plan' || item.noti_type == 'is_intrested_other_upvote') {

      if (item.s_type == 'activity') {
        this.props.navigation.navigate('SuggestActivity', { other_id: item.plan_data.login_id, plan_id: item.common_id, is_direct: false, notificaitonid: item.notification_id })
      }
      else if (item.s_type == 'datetime') {
        this.props.navigation.navigate('SuggestDateTime', { other_id: item.plan_data.login_id, plan_id: item.common_id, is_direct: false, notificaitonid: item.notification_id })
      }
      else if (item.s_type == 'location') {
        this.props.navigation.navigate('SuggestLocation', { other_id: item.plan_data.login_id, plan_id: item.common_id, is_direct: false, notificaitonid: item.notification_id })
      }

    }
    else if (item.noti_type == 'is_intrested_plan_chat') {
      if (item.plan_data.plan_type == 1) {
        var name = item.plan_data.receiver_name
        this.props.navigation.navigate('EventDetail', { item: item.plan_data, plan_id: item.common_id, other_id: item.plan_data.login_id, plan_type: item.plan_data.plan_type, title_header: "Let's meet up!", Final_date: '', eventby: item.plan_data.name, notificaitonid: item.notification_id })
      }
      else if (item.plan_data.plan_type == 2) {
        this.props.navigation.navigate('EventDetail', { item: item.plan_data, plan_id: item.common_id, other_id: item.plan_data.login_id, plan_type: item.plan_data.plan_type, title_header: item.plan_data.activity, Final_date: '', eventby: item.plan_data.name, notificaitonid: item.notification_id })
      }
      else if (item.plan_data.plan_type == 3) {
        this.props.navigation.navigate('EventDetail', { item: item.plan_data, plan_id: item.common_id, other_id: item.plan_data.login_id, plan_type: item.plan_data.plan_type, title_header: "Wanna go to " + item.plan_data.line_1 + "?", Final_date: '', eventby: item.plan_data.name, notificaitonid: item.notification_id })
      }
      else if (item.plan_data.plan_type == 4) {
        this.props.navigation.navigate('EventDetail', { item: item.plan_data, plan_id: item.common_id, other_id: item.plan_data.login_id, plan_type: item.plan_data.plan_type, title_header: "I'm free, wanna hangout?", Final_date: '', eventby: item.plan_data.name, notificaitonid: item.notification_id })
      }
    }
    else if (item.noti_type == 'is_chat_my_plan' || item.noti_type == 'is_invite_my_plan' || item.noti_type == 'is_invite_their_plan') {

      if (item.plan_data.plan_type == 1) {
        var name = item.plan_data.receiver_name
        this.props.navigation.navigate('EventDetail', { item: item.plan_data, plan_id: item.common_id, other_id: item.plan_data.login_id, plan_type: item.plan_data.plan_type, title_header: "Let's meet up!", Final_date: '', eventby: item.plan_data.name, notificaitonid: item.notification_id })
      }
      else if (item.plan_data.plan_type == 2) {
        this.props.navigation.navigate('EventDetail', { item: item.plan_data, plan_id: item.common_id, other_id: item.plan_data.login_id, plan_type: item.plan_data.plan_type, title_header: item.plan_data.activity, Final_date: '', eventby: item.plan_data.name, notificaitonid: item.notification_id })
      }
      else if (item.plan_data.plan_type == 3) {
        this.props.navigation.navigate('EventDetail', { item: item.plan_data, plan_id: item.common_id, other_id: item.plan_data.login_id, plan_type: item.plan_data.plan_type, title_header: "Wanna go to " + item.plan_data.line_1 + "?", Final_date: '', eventby: item.plan_data.name, notificaitonid: item.notification_id })
      }
      else if (item.plan_data.plan_type == 4) {
        this.props.navigation.navigate('EventDetail', { item: item.plan_data, plan_id: item.common_id, other_id: item.plan_data.login_id, plan_type: item.plan_data.plan_type, title_header: "I'm free, wanna hangout?", Final_date: '', eventby: item.plan_data.name, notificaitonid: item.notification_id })
      }
    }
    else if (item.noti_type == 'Friend Request') {
      this.setState({ index: 1, notificaitonid: item.notification_id }, () => {
        this.readNotification()
      })

      global.friend_request_count = 0
      store.dispatch({ type: FRIENDREQUEST, data: global.friend_request_count })
      var date = moment.utc().subtract(5, 'seconds').format('YYYY-MM-DD HH:mm:ss');
      AsyncStorage.setItem('frienddate', date)
      this.getFriendRequests(false)
    }
    else if (item.noti_type == 'Friend Request Accepted') {
      AsyncStorage.setItem('other_id', item.sender_id)
      this.props.navigation.navigate('ohter_user_profile', { notificaitonid: item.notification_id })
    }
    else if (item.noti_type == 'New Friend Register') {
      AsyncStorage.setItem('other_id', item.sender_id)
      this.props.navigation.navigate('ohter_user_profile', { notificaitonid: item.notification_id })
    }
  }


  renderItem = (item, index) => {
    return (
      <View>
        {index == 0 ? <View style={{ height: Height(4), width: Height(4), borderRadius: Height(4), overflow: 'hidden' }}>
          <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.image }} />
        </View> :
          <View style={{ marginHorizontal: -6, height: Height(4), width: Height(4), borderRadius: Height(4), overflow: 'hidden' }}>
            <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.image }} />
          </View>}
      </View>
    )
  }

  // <Text style={{ marginLeft: Width(3), marginTop: Height(2), marginBottom: Height(1), fontSize: FontSize(16), color: colors.appColor, fontFamily: fonts.Roboto_Bold }}>NEW</Text>

  getTimeFromNow(datetime) {
    var dateTime = ''
    //moment.utc(item.datetime).local().fromNow()
    var gmtDateTime = moment.utc(datetime)
    var date1 = gmtDateTime.local();

    if (moment(date1).format("ll") == moment().format("ll")) {
      dateTime = date1.fromNow();
    } else {
      dateTime = date1.calendar();
    }
    return dateTime
  }

  goToEventDetails(item) {
    if (item.plan_type == 1) {
      var name = item.friend_name.split(" ")
      this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "Let's meet up!", Final_date: '', eventby: item.name })
    }
    else if (item.plan_type == 2) {
      this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: item.activity, Final_date: '', eventby: item.name })
    }
    else if (item.plan_type == 3) {
      this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "Wanna go to " + item.line_1 + "?", Final_date: '', eventby: item.name })
    }
    else if (item.plan_type == 4) {
      this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "I'm free, wanna hangout?", Final_date: '', eventby: item.name })
    }
  }

  setFinalDate(item) {
    let Final_date = ''
    var currentDate = moment().format("ll");
    var tomorrow = moment().add(1, 'days').format("ll")

    var start = item.start_date.split(" ")
    var end = item.end_date.split(" ")

    if (item.start_date != '') {
      var gmtStartDateTime = moment.utc(item.start_date)
      var localstartdate = gmtStartDateTime.local();

      var gmtEndDateTime = moment.utc(item.end_date)
      var localenddate = gmtEndDateTime.local();

      if (item.is_now == '1') {
        if (currentDate == moment(localstartdate).format('ll') && currentDate == moment(localenddate).format('ll')) {
          Final_date = 'Now - ' + moment(localenddate).format('LT')
        } else {
          if (currentDate == moment(localstartdate).format('ll') && tomorrow == moment(localenddate).format('ll')) {
            Final_date = 'Now - Tomorrow · ' + moment(localstartdate).format('LT')
          } else {
            Final_date = 'Now - ' + moment(localenddate).format('ddd, MMM D \u00B7 LT')
          }

        }
      } else {
        if (currentDate == moment(localstartdate).format('ll') && currentDate == moment(localenddate).format('ll')) {
          if (start[1] == '00:00:00' && end[1] == '00:00:00') {

            Final_date = 'Today · All Day'
          } else {
            Final_date = 'Today · ' + moment(localstartdate).format('LT')
          }
        } else {
          if (start[1] == '00:00:00' && end[1] == '00:00:00') {
            if (tomorrow == moment(localstartdate).format('ll') && tomorrow == moment(localenddate).format('ll')) {
              Final_date = 'Tomorrow · All Day'

            } else {
              if (tomorrow == moment(localstartdate).format('ll')) {
                Final_date = 'Tomorrow · All Day'

              } else {
                if (currentDate == moment(localstartdate).format('ll')) {
                  Final_date = 'Today' + ' ·All Day '

                } else {

                  Final_date = moment(localstartdate).format('ddd, MMM D') + ' · All Day '
                }

              }

            }
          } else {
            if (tomorrow == moment(localstartdate).format('ll') && tomorrow == moment(localenddate).format('ll')) {

              if (item.is_allday == 1) {
                Final_date = 'Tomorrow · All Day'
              }
              else {
                Final_date = 'Tomorrow · ' + moment(localstartdate).format('LT')
              }


            } else {
              if (tomorrow == moment(localstartdate).format('ll')) {
                Final_date = 'Tomorrow · ' + moment(localstartdate).format('LT')

              } else {
                if (currentDate == moment(localstartdate).format('ll')) {
                  Final_date = 'Today ·' + moment(localstartdate).format('LT')

                } else {

                  if (item.is_allday == 1) {
                    Final_date = moment(localstartdate).format('ddd, MMM D') + ' · All Day '
                  }
                  else {
                    Final_date = moment(localstartdate).format('ddd, MMM D \u00B7 LT')
                  }


                }

              }

            }

          }

        }

      }
    }
    return Final_date
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.getNotificationsList(true)
  }

  _onRefreshFriendRequest = () => {
    this.setState({ refreshing: true });
    this.getFriendRequests(true)
  }


  onUsersPressed(item) {

    let plan_id = item.plan_id

    //alert(plan_id)

    var title = ""

    if (item.plan_type == 1) {
      title = ""
    }
    else if (item.plan_type == 2) {
      title = item.activity
    }
    else if (item.plan_type == 3) {
      title = item.line_1
    }
    else if (item.plan_type == 4) {
      title = ""
    }

    if (item.is_going_count > 0) {
      this.props.navigation.navigate('Friend_Status', { tabIndex: 0, plan_id: plan_id, Title_New: title, plan_type: item.plan_type })
    }
    else {
      this.props.navigation.navigate('Friend_Status', { tabIndex: 1, plan_id: plan_id, Title_New: title, plan_type: item.plan_type })
    }
  }


  renderScene = ({ route }) => {
    //console.log('renderScene')

    switch (route.key) {
      case 'first':
        return (
          <View style={{ flex: 1 }}>
            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
            >

              {(this.state.notifications_list.length == 0 && this.state.earlier_notifications_list.length == 0 && this.isApiCalled == true) ?
                <View style={{ alignSelf: 'center' }}>
                  <Image style={{ marginTop: Height(9), alignSelf: 'center' }} source={notification_null} />
                  <View style={{ height: Height(3) }} />
                  <Text style={{ fontSize: 26, fontFamily: fonts.Raleway_Bold, color: colors.appColor, textAlign: 'center' }}>Nothing Here!</Text>
                  <View style={{ height: Height(1.5) }} />
                  <Text style={{ alignSelf: 'center', fontSize: 18, fontFamily: fonts.Roboto_Regular, color: colors.dargrey, textAlign: 'center', width: Width(65) }}></Text>
                </View> :
                <View>
                  <View>
                    {this.state.notifications_list.length > 0 ? <Text style={{ marginLeft: Width(3), marginTop: Height(2), marginBottom: Height(1), fontSize: FontSize(16), color: colors.appColor, fontFamily: fonts.Roboto_Bold }}>NEW</Text> : null}
                    {this.state.notifications_list.map((item, index) => {
                      return (
                        <TouchableOpacity onPress={() => this.onNotificationSelected(item, index)}>
                          {/* <View style={{ height: Height(8), flexDirection: 'row', alignItems: 'center', backgroundColor: item.is_read == 1 ? '#fff' : '#DEEEFF' }}> */}
                          <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: item.is_read == 1 ? '#fff' : '#DEEEFF' }}>
                            <View style={{ width: Width(20), alignItems: 'center', marginVertical: 7 }}>
                              <TouchableHighlight onPress={() => { }} underlayColor="transparent" style={{ height: Height(5), width: Height(5), borderRadius: Height(5), overflow: 'hidden' }}>
                                <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.sender_image }} />
                              </TouchableHighlight>
                              {/* <TouchableHighlight style={{ position: 'absolute', bottom: Height(0.3), right: Width(3), height: Height(3), width: Height(3), borderRadius: Height(3), backgroundColor: '#65DA65', justifyContent: 'center', alignItems: 'center' }} underlayColor="transparent">
                        <Text style={{ color: '#FFF', fontFamily: fonts.Raleway_Regular, fontSize: FontSize(12) }}>5+</Text>
                      </TouchableHighlight> */}
                            </View>
                            <View style={{ justifyContent: 'center', width: Width(75), marginVertical: 7 }}>
                              <Text style={{ fontSize: FontSize(14), color: '#3A4759', fontFamily: fonts.Roboto_Bold, fontWeight: 'bold' }}>{item.line_1}<Text style={{ color: '#3A4759', flexWrap: 'wrap', fontWeight: 'normal', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(14) }}>{item.line_2}</Text></Text>
                              {/* <Text style={{ color: '#3A4759', flexWrap: 'wrap', fontWeight: 'normal', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(14) }}>{item.msg}</Text> */}
                              <Text style={{ fontSize: FontSize(10), color: '#8F8F90', fontFamily: fonts.Roboto_Regular }}>{this.getTimeFromNow(item.datetime)}</Text>
                            </View>
                          </View>
                          <View style={{ height: 1, width: '100%', backgroundColor: '#fff' }}></View>
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                  <View>
                    {
                      this.state.earlier_notifications_list.length > 0 ?
                        <Text style={{ marginLeft: Width(3), marginTop: Height(2), marginBottom: Height(1), fontSize: FontSize(16), color: colors.appColor, fontFamily: fonts.Roboto_Bold }}>EARLIER</Text>
                        : null
                    }
                    {this.state.earlier_notifications_list.map((item, index) => {
                      return (
                        <TouchableOpacity onPress={() => this.onEarlierNotificationSelected(item, index)}>
                          {/* <View style={{ height: Height(8), flexDirection: 'row', alignItems: 'center', backgroundColor: item.is_read == 1 ? '#fff' : '#DEEEFF' }}> */}
                          <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: item.is_read == 1 ? '#fff' : '#DEEEFF' }}>
                            <View style={{ width: Width(20), alignItems: 'center', marginVertical: 7 }}>
                              <TouchableHighlight onPress={() => { }} underlayColor="transparent" style={{ height: Height(5), width: Height(5), borderRadius: Height(5), overflow: 'hidden' }}>
                                <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.sender_image }} />
                              </TouchableHighlight>
                              {/* <TouchableHighlight style={{ position: 'absolute', bottom: Height(0.3), right: Width(3), height: Height(3), width: Height(3), borderRadius: Height(3), backgroundColor: '#65DA65', justifyContent: 'center', alignItems: 'center' }} underlayColor="transparent">
                      <Text style={{ color: '#FFF', fontFamily: fonts.Raleway_Regular, fontSize: FontSize(12) }}>5+</Text>
                    </TouchableHighlight> */}
                            </View>
                            <View style={{ justifyContent: 'center', width: Width(75), marginVertical: 7 }}>
                              <Text style={{ fontSize: FontSize(14), color: '#3A4759', fontFamily: fonts.Roboto_Bold, fontWeight: 'bold' }}>{item.line_1}<Text style={{ color: '#3A4759', flexWrap: 'wrap', fontWeight: 'normal', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(14) }}>{'' + item.line_2}</Text></Text>
                              {/* <Text style={{ color: '#3A4759', flexWrap: 'wrap', fontWeight: 'normal', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(14) }}>{item.msg}</Text> */}
                              <Text style={{ fontSize: FontSize(10), color: '#8F8F90', fontFamily: fonts.Roboto_Regular }}>{this.getTimeFromNow(item.datetime)}</Text>
                            </View>

                          </View>
                          <View style={{ height: 1, width: '100%', backgroundColor: '#fff' }}></View>
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                </View>
              }
            </ScrollView>
          </View>
        )
      case 'second':
        return (<View style={{ flex: 1 }}>
          <ScrollView

            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefreshFriendRequest}
              />
            }
          >
            {this.state.friend_request_list.length == 0 ?
              <View style={{ alignSelf: 'center' }}>
                <Image style={{ marginTop: Height(9), alignSelf: 'center' }} source={friend_null} />
                <View style={{ height: Height(3) }} />
                <Text style={{ fontSize: 26, fontFamily: fonts.Raleway_Bold, color: colors.appColor, textAlign: 'center' }}>Nothing Here!</Text>
                <View style={{ height: Height(1.5) }} />
                <Text style={{ alignSelf: 'center', fontSize: 18, fontFamily: fonts.Roboto_Regular, color: colors.dargrey, textAlign: 'center', width: Width(65) }}>Tap the button below to find your friends</Text>
              </View> :
              <View>
                {this.state.friend_request_list.map((item, index) => {
                  return (
                    <View style={{ height: Height(6.5), flexDirection: 'row', alignItems: 'center', backgroundColor: '#FFF' }}>
                      <View style={{ width: Width(15), alignItems: 'center', marginLeft: Width(1) }}>
                        <TouchableHighlight onPress={() => { AsyncStorage.setItem('other_id', item.sender_id), this.props.navigation.navigate('ohter_user_profile') }} underlayColor="transparent" style={{ height: Height(4.5), width: Height(4.5), borderRadius: Height(4.5), overflow: 'hidden' }}>
                          <Image style={{ height: '100%', width: '100%' }} source={item.sender_photo == '' ? flowers : { uri: item.sender_photo }} />
                        </TouchableHighlight>
                      </View>
                      <View style={{ justifyContent: 'center', width: item.is_accept == 1 ? Width(54.5) : Width(56) }}>
                        <Text style={{ fontSize: FontSize(16), color: '#000', fontFamily: fonts.Roboto_Medium }} onPress={() => { AsyncStorage.setItem('other_id', item.sender_id), this.props.navigation.navigate('ohter_user_profile') }}>{item.sender_name}</Text>
                      </View>
                      <View style={{ marginRight: Height(2), alignItems: 'flex-end' }}>
                        <TouchableHighlight underlayColor="transparent" style={{}}>
                          {item.is_accept == 1 ? <View style={{ flexDirection: 'row', backgroundColor: '#FFF', justifyContent: 'center', alignItems: 'center', height: Height(5.5), width: Width(24), borderRadius: Width(10) }}>
                            <AntDesign style={{ paddingLeft: Width(1) }} name="check" color="#007AFF" size={18} />
                            <Text style={{ color: colors.fontDarkGrey, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12) }}>Request accepted</Text>
                          </View> :
                            <TouchableOpacity onPress={() => this.addfriends(item, index)} style={{ flexDirection: 'row', backgroundColor: '#FFF', borderWidth: 1, borderColor: '#007AFF', justifyContent: 'center', alignItems: 'center', height: Height(3.5), width: Width(22), borderRadius: Height(1.75) }}>
                              <Text style={{ color: '#007AFF', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12) }}>Confirm</Text>
                              {/* <AntDesign style={{ paddingLeft: Width(1) }} name="plus" color="#007AFF" size={12} /> */}
                            </TouchableOpacity>}
                        </TouchableHighlight>
                      </View>
                    </View>
                  );
                })}
              </View>}
          </ScrollView>

          <TouchableOpacity style={{ width: '103%', marginBottom: Height(1), alignSelf: 'center' }} onPress={() => { this.props.navigation.navigate('FindFriends_Setting') }} >
            <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Find Your Friends</Text>
            </ImageBackground>
          </TouchableOpacity>
        </View>
        )
      case 'third':
        var items = []

        return <View style={{}}>
          <ScrollView>

            {
              this.state.UpcomingList.length == 0 ?

                <View style={{ alignSelf: 'center' }}>
                  <Image style={{ marginTop: Height(9), alignSelf: 'center' }} source={upcoming_null} />
                  <View style={{ height: Height(3) }} />
                  <Text style={{ fontSize: 26, fontFamily: fonts.Raleway_Bold, color: colors.appColor, textAlign: 'center' }}>Nothing Here!</Text>
                  <View style={{ height: Height(1.5) }} />
                  {/* <Text style={{ alignSelf: 'center', fontSize: 18, fontFamily: fonts.Roboto_Regular, color: colors.dargrey, textAlign: 'center', width: Width(65) }}>Tap the button below to find your friends</Text> */}
                </View>

                :

                this.state.UpcomingList.map((item, index) => {
                  items = item.participants
                  let dateTime = '';
                  var gmtDateTime = moment.utc(item.start_date)
                  var date1 = gmtDateTime.local();

                  if (moment(date1).format("ll") == moment().format("ll")) {
                    dateTime = "TODAY, " + date1.fromNow().toUpperCase();
                  } else {

                    var tomorrow = moment().add(1, 'days').format("ll")
                    var gmtStartDateTime = moment.utc(item.start_date)
                    var localstartdate = gmtStartDateTime.local();

                    if (tomorrow == moment(localstartdate).format('ll')) {
                      dateTime = "TOMORROW";
                    }
                    else {
                      dateTime = moment(localstartdate).format('ddd, MMM D')
                    }
                  }

                  var activity = ''

                  if (item.plan_type == 1) {
                    var name = item.friend_name.split(" ")
                    //activity = name[0]+", let's meet up!"
                    activity = "Let's meet up!"
                  }
                  else if (item.plan_type == 3) {
                    activity = "Wanna go to " + item.line_1 + "?"
                  }
                  else if (item.plan_type == 4) {
                    activity = "I'm free, wanna hangout?"
                  }
                  return (
                    <View>
                      <Text style={{ marginLeft: Width(4), marginVertical: Height(2), fontSize: FontSize(12), color: colors.appColor, fontFamily: fonts.Roboto_Bold }}>{dateTime.toUpperCase()}</Text>

                      {
                        item.plan_type == 1 ?

                          <View style={{
                            marginHorizontal: Width(4), borderRadius: 5, backgroundColor: '#F2F4F7',
                            shadowColor: "#000",
                            shadowOffset: {
                              width: 0,
                              height: 2,
                            },
                            shadowOpacity: 0.0,
                            shadowRadius: 0.0,
                            borderRadius: 10,
                            elevation: 5,
                          }}>
                            <TouchableOpacity style={{ marginTop: Height(2), flexDirection: 'row' }} onPress={() => this.goToEventDetails(item)}>
                              <View style={{ marginLeft: Width(5) }}>
                                <TouchableHighlight onPress={() => { }} underlayColor="transparent" style={{ height: Height(5), width: Height(5), borderRadius: Height(5), overflow: 'hidden' }}>
                                  <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.photo }} />
                                </TouchableHighlight>
                              </View>
                              <View style={{ marginLeft: Width(4) }}>
                                <Text style={{ fontSize: FontSize(16), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, width: '88%' }} numberOfLines={1}>{item.plan_type == 2 ? item.activity : activity}</Text>
                                <Text style={{ fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Bold }}>{item.login_id == this.state.login_id ? 'Me' : item.name}</Text>
                                {/* <View style={{ flexDirection: 'row',  marginTop: Height(3),alignItems: 'center' }}>
                              <Image source={calendar} />
                              <View style={{ marginLeft: Width(2), }}>
                                <Text style={{ color: colors.dargrey, fontFamily: fonts.Roboto_Bold, fontSize: FontSize(13) }}>{moment(item.start_date).format('ddd, MMM D \u00B7 LT')}</Text>
                              </View>
                            </View>
                            <View style={{ flexDirection: 'row' ,marginTop: Height(3)}}>
                              <Image source={location} />
                              <View style={{ marginLeft: Width(2) }}>
                                <Text style={{ fontFamily: fonts.Roboto_Bold, fontSize: FontSize(13), color: colors.dargrey }}>{item.line_1}</Text>
                                <Text style={{ width: Width(55), marginTop: Height(0.5), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(10), color: colors.fontDarkGrey }}>{item.line_2}</Text>
    
                              </View>
                            </View> */}

                                <TouchableOpacity
                                  style={{ flexDirection: 'row', marginTop: Height(2) }}>
                                  <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={chai_cup} />
                                  {item.activity == '' ? <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6', marginRight: Width(5) }}
                                    onPress={item.total_activity_suggestions == 0 ? () => this.props.navigation.navigate('Activity', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true, title_header: "Let's meet up!" }) : () => this.props.navigation.navigate('SuggestActivity', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false, title_header: "Let's meet up!" })}>{item.total_activity_suggestions == 0 ? 'Suggest an activity' : 'View activity suggestions'}</Text>
                                    : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey, marginRight: Width(5) }}>{item.activity}</Text>}
                                </TouchableOpacity>

                                <TouchableOpacity
                                  style={{ flexDirection: 'row', marginTop: Height(1), marginBottom: Height(1) }}>
                                  <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={calendar} />
                                  {item.start_date == '' ? <Text
                                    onPress={item.total_datetime_suggestions == 0 ? () => this.props.navigation.navigate('DateTime', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true }) : () => this.props.navigation.navigate('SuggestDateTime', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false })}
                                    style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6', marginRight: Width(5) }}>{item.total_datetime_suggestions == 0 ? 'Suggest a date & time' : 'View date & time suggestions'}</Text>
                                    : <Text style={{ width: Width(60), paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey, marginRight: Width(5) }}>{this.setFinalDate(item)}</Text>}
                                  {/* : <Text style={{ width: Width(60), paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey, marginRight:Width(5) }}>{moment(item.start_date).format('ddd, MMM D \u00B7 LT')}</Text>} */}
                                </TouchableOpacity>

                                <TouchableOpacity
                                  style={{ flexDirection: 'row', marginTop: Height(0), marginRight: Width(4), marginBottom: Height(1.5) }}>
                                  <Image resizeMode="contain" style={{ tintColor: colors.whatFontColor, marginTop: 3 }} source={location} />
                                  {

                                    item.line_1 == '' ?
                                      <Text
                                        onPress={item.total_location_suggestions == 0 ? () => this.props.navigation.navigate('Location', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true }) : () => this.props.navigation.navigate('SuggestLocation', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false })}
                                        style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6', marginRight: Width(5) }}>{item.total_location_suggestions == 0 ? 'Suggest a location' : 'View location suggestions'}</Text>
                                      :
                                      <View style={{ width: '88%' }}>
                                        <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.line_1}</Text>
                                        <Text style={{ fontSize: FontSize(10), fontFamily: fonts.Roboto_Regular, color: '#c1c1c1', paddingLeft: Width(3.3) }}>{item.line_2}</Text>
                                      </View>
                                  }

                                </TouchableOpacity>

                              </View>
                            </TouchableOpacity>

                            {item.participants.length > 0 ?

                              <View style={{ flexDirection: 'row', marginTop: Height(0.5), marginBottom: 24, marginLeft: Width(6), height: Height(3) }}>
                                {/* <FlatList
                          data={items}
                          renderItem={({ item, index }) => this.renderItem(item, index)}
                        /> */}

                                <View style={{ height: Height(3), width: Height(3), borderRadius: Height(3) }}>
                                  <Image style={{ height: '100%', width: '100%', borderRadius: Height(1.5) }} source={item.participants[0].image != '' ? { uri: item.participants[0].image } : avtar} />
                                </View>
                                <TouchableOpacity onPress={() => this.onUsersPressed(item)}>
                                  <View style={{ height: Height(3), width: Height(3), borderRadius: Height(3), marginLeft: -11, zIndex: 1 }}>
                                    <Image style={{ height: '100%', width: '100%', borderRadius: Height(1.5) }} source={require('../../../assets/images/dots.png')} />
                                  </View>
                                </TouchableOpacity>

                                <TouchableOpacity style={{ bottom: 0, position: 'absolute', right: Width(8) }} onPress={() => {
                                  this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "Let's meet up!", Final_date: '', eventby: item.name })
                                }}>
                                  <Image resizeMode="contain" source={ic_chat} />
                                </TouchableOpacity>
                              </View>
                              :
                              <View style={{ flexDirection: 'row', marginTop: Height(0.5), marginBottom: 24, marginLeft: Width(6), height: Height(2) }}>
                                <TouchableOpacity style={{ bottom: 0, position: 'absolute', right: Width(8) }} onPress={() => {
                                  this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "Let's meet up!", Final_date: '', eventby: item.name })
                                }}>
                                  <Image resizeMode="contain" source={ic_chat} />
                                </TouchableOpacity>
                              </View>
                            }

                            {/* <View style={{ marginTop: Height(2), height: Height(2) }} /> */}
                          </View>

                          :

                          null
                      }

                      {
                        item.plan_type == 2 ?

                          <View style={{
                            marginHorizontal: Width(4), borderRadius: 5, backgroundColor: '#F2F4F7',
                            shadowColor: "#000",
                            shadowOffset: {
                              width: 0,
                              height: 2,
                            },
                            shadowOpacity: 0.0,
                            shadowRadius: 0.0,
                            borderRadius: 10,
                            elevation: 5,
                          }}>
                            <TouchableOpacity style={{ marginTop: Height(2), flexDirection: 'row' }} onPress={() => this.goToEventDetails(item)}>
                              <View style={{ marginLeft: Width(5) }}>
                                <TouchableHighlight onPress={() => { }} underlayColor="transparent" style={{ height: Height(5), width: Height(5), borderRadius: Height(5), overflow: 'hidden' }}>
                                  <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.photo }} />
                                </TouchableHighlight>
                              </View>
                              <View style={{ marginLeft: Width(4) }}>
                                <Text style={{ fontSize: FontSize(16), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, width: '88%' }} numberOfLines={1}>{item.plan_type == 2 ? item.activity : activity}</Text>
                                <Text style={{ fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Bold }}>{item.login_id == this.state.login_id ? 'Me' : item.name}</Text>
                                {/* <View style={{ flexDirection: 'row',  marginTop: Height(3),alignItems: 'center' }}>
                              <Image source={calendar} />
                              <View style={{ marginLeft: Width(2), }}>
                                <Text style={{ color: colors.dargrey, fontFamily: fonts.Roboto_Bold, fontSize: FontSize(13) }}>{moment(item.start_date).format('ddd, MMM D \u00B7 LT')}</Text>
                              </View>
                            </View>
                            <View style={{ flexDirection: 'row' ,marginTop: Height(3)}}>
                              <Image source={location} />
                              <View style={{ marginLeft: Width(2) }}>
                                <Text style={{ fontFamily: fonts.Roboto_Bold, fontSize: FontSize(13), color: colors.dargrey }}>{item.line_1}</Text>
                                <Text style={{ width: Width(55), marginTop: Height(0.5), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(10), color: colors.fontDarkGrey }}>{item.line_2}</Text>
    
                              </View>
                            </View> */}

                                <TouchableOpacity
                                  style={{ flexDirection: 'row', marginTop: Height(2) }}>
                                  <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={chai_cup} />
                                  {item.activity == '' ? <Text
                                    onPress={item.total_activity_suggestions == 0 ? () => this.props.navigation.navigate('Activity', { plan_id: item.plan_id, is_direct: true, title_header: item.activity }) : () => this.props.navigation.navigate('SuggestActivity', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false, title_header: item.activity })}
                                    style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}></Text>
                                    : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey, marginRight: Width(5) }}>{item.activity}</Text>}
                                </TouchableOpacity>

                                <TouchableOpacity
                                  style={{ flexDirection: 'row', marginTop: Height(1), marginBottom: Height(1) }}>
                                  <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={calendar} />
                                  {item.start_date == '' ? <Text
                                    onPress={item.total_datetime_suggestions == 0 ? () => this.props.navigation.navigate('DateTime', { plan_id: item.plan_id, is_direct: true }) : () => this.props.navigation.navigate('SuggestDateTime', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false })}
                                    style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>{item.total_datetime_suggestions == 0 ? 'Suggest a date & time' : 'View date & time suggestions'}</Text>
                                    : <Text style={{ width: Width(60), paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey, marginRight: Width(5) }}>{this.setFinalDate(item)}</Text>}
                                  {/* : <Text style={{ width: Width(60), paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey, marginRight:Width(5) }}>{moment(item.start_date).format('ddd, MMM D \u00B7 LT')}</Text>} */}
                                </TouchableOpacity>

                                <TouchableOpacity
                                  style={{ flexDirection: 'row', marginTop: Height(0), marginRight: Width(4), marginBottom: Height(1.5) }}>
                                  <Image resizeMode="contain" style={{ tintColor: colors.whatFontColor, marginTop: 3 }} source={location} />
                                  {
                                    item.line_1 == '' ?
                                      <Text
                                        onPress={item.total_location_suggestions == 0 ? () => this.props.navigation.navigate('Location', { plan_id: item.plan_id, is_direct: true }) : () => this.props.navigation.navigate('SuggestLocation', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false })}
                                        style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>{item.total_location_suggestions == 0 ? 'Suggest a location' : 'View location suggestions'}</Text>
                                      :
                                      <View style={{ width: '88%' }}>
                                        <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.line_1}</Text>
                                        <Text style={{ fontSize: FontSize(10), fontFamily: fonts.Roboto_Regular, color: '#c1c1c1', paddingLeft: Width(3.3) }}>{item.line_2}</Text>
                                      </View>
                                  }

                                </TouchableOpacity>

                              </View>
                            </TouchableOpacity>

                            {item.participants.length > 0 ?

                              <View style={{ flexDirection: 'row', marginTop: Height(0.5), marginBottom: 24, marginLeft: Width(6), height: Height(3) }}>
                                {/* <FlatList
                          data={items}
                          renderItem={({ item, index }) => this.renderItem(item, index)}
                        /> */}

                                <View style={{ height: Height(3), width: Height(3), borderRadius: Height(3) }}>
                                  <Image style={{ height: '100%', width: '100%', borderRadius: Height(1.5) }} source={item.participants[0].image != '' ? { uri: item.participants[0].image } : avtar} />
                                </View>
                                <TouchableOpacity onPress={() => this.onUsersPressed(item)}>
                                  <View style={{ height: Height(3), width: Height(3), borderRadius: Height(3), marginLeft: -11, zIndex: 1 }}>
                                    <Image style={{ height: '100%', width: '100%', borderRadius: Height(1.5) }} source={require('../../../assets/images/dots.png')} />
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ bottom: 0, position: 'absolute', right: Width(8) }} onPress={() => {
                                  this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: item.activity, Final_date: '', eventby: item.name })
                                }}>
                                  <Image resizeMode="contain" source={ic_chat} />
                                </TouchableOpacity>

                              </View>

                              :
                              <View style={{ flexDirection: 'row', marginTop: Height(0.5), marginBottom: 24, marginLeft: Width(6), height: Height(2) }}>
                                <TouchableOpacity style={{ bottom: 0, position: 'absolute', right: Width(8) }} onPress={() => {
                                  this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: item.activity, Final_date: '', eventby: item.name })
                                }}>
                                  <Image resizeMode="contain" source={ic_chat} />
                                </TouchableOpacity>
                              </View>

                            }

                            {/* <View style={{ marginTop: Height(2), height: Height(2) }} /> */}
                          </View>

                          :

                          null
                      }

                      {
                        item.plan_type == 3 ?

                          <View style={{
                            marginHorizontal: Width(4), borderRadius: 5, backgroundColor: '#F2F4F7',
                            shadowColor: "#000",
                            shadowOffset: {
                              width: 0,
                              height: 2,
                            },
                            shadowOpacity: 0.0,
                            shadowRadius: 0.0,
                            borderRadius: 10,
                            elevation: 5,
                          }}>
                            <TouchableOpacity style={{ marginTop: Height(2), flexDirection: 'row' }} onPress={() => this.goToEventDetails(item)}>
                              <View style={{ marginLeft: Width(5) }}>
                                <TouchableHighlight onPress={() => { }} underlayColor="transparent" style={{ height: Height(5), width: Height(5), borderRadius: Height(5), overflow: 'hidden' }}>
                                  <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.photo }} />
                                </TouchableHighlight>
                              </View>
                              <View style={{ marginLeft: Width(4) }}>
                                <Text style={{ fontSize: FontSize(16), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, width: '88%' }} numberOfLines={1}>{item.plan_type == 2 ? item.activity : activity}</Text>
                                <Text style={{ fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Bold }}>{item.login_id == this.state.login_id ? 'Me' : item.name}</Text>
                                {/* <View style={{ flexDirection: 'row',  marginTop: Height(3),alignItems: 'center' }}>
                              <Image source={calendar} />
                              <View style={{ marginLeft: Width(2), }}>
                                <Text style={{ color: colors.dargrey, fontFamily: fonts.Roboto_Bold, fontSize: FontSize(13) }}>{moment(item.start_date).format('ddd, MMM D \u00B7 LT')}</Text>
                              </View>
                            </View>
                            <View style={{ flexDirection: 'row' ,marginTop: Height(3)}}>
                              <Image source={location} />
                              <View style={{ marginLeft: Width(2) }}>
                                <Text style={{ fontFamily: fonts.Roboto_Bold, fontSize: FontSize(13), color: colors.dargrey }}>{item.line_1}</Text>
                                <Text style={{ width: Width(55), marginTop: Height(0.5), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(10), color: colors.fontDarkGrey }}>{item.line_2}</Text>
    
                              </View>
                            </View> */}

                                <TouchableOpacity
                                  style={{ flexDirection: 'row', marginTop: Height(2) }}>
                                  <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={chai_cup} />
                                  {item.activity == '' ? <Text
                                    onPress={item.total_activity_suggestions == 0 ? () => this.props.navigation.navigate('Activity', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true, title_header: "Wanna go to " + item.line_1 + "?" }) : () => this.props.navigation.navigate('SuggestActivity', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false, title_header: "Wanna go to " + item.line_1 + "?" })}
                                    style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6', marginRight: Width(5) }}>{item.total_activity_suggestions == 0 ? 'Suggest an activity' : 'View activity suggestions'}</Text>
                                    : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey, marginRight: Width(5) }}>{item.activity}</Text>}
                                </TouchableOpacity>

                                <TouchableOpacity
                                  style={{ flexDirection: 'row', marginTop: Height(1), marginBottom: Height(1) }}>
                                  <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={calendar} />
                                  {item.start_date == '' ? <Text
                                    onPress={item.total_datetime_suggestions == 0 ? () => this.props.navigation.navigate('DateTime', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true }) : () => this.props.navigation.navigate('SuggestDateTime', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false })}
                                    style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6', marginRight: Width(5) }}>{item.total_datetime_suggestions == 0 ? 'Suggest a date & time' : 'View date & time suggestions'}</Text>
                                    : <Text style={{ width: Width(60), paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey, marginRight: Width(5) }}>{this.setFinalDate(item)}</Text>}
                                  {/* : <Text style={{ width: Width(60), paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey, marginRight:Width(5) }}>{moment(item.start_date).format('ddd, MMM D \u00B7 LT')}</Text>} */}
                                </TouchableOpacity>

                                <TouchableOpacity
                                  style={{ flexDirection: 'row', marginTop: Height(0), marginRight: Width(4), marginBottom: Height(1.5) }}>
                                  <Image resizeMode="contain" style={{ tintColor: colors.whatFontColor, marginTop: 3 }} source={location} />
                                  {

                                    item.line_1 == '' ?

                                      <Text
                                        onPress={item.total_location_suggestions == 0 ? () => this.props.navigation.navigate('Location', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true }) : () => this.props.navigation.navigate('SuggestLocation', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false })}
                                        style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6', marginRight: Width(5) }}>{item.total_location_suggestions == 0 ? 'Suggest a location' : 'View location suggestions'}</Text>
                                      :
                                      <View style={{ width: '88%' }}>
                                        <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.line_1}</Text>
                                        <Text style={{ fontSize: FontSize(10), fontFamily: fonts.Roboto_Regular, color: '#c1c1c1', paddingLeft: Width(3.3) }}>{item.line_2}</Text>
                                      </View>
                                  }

                                </TouchableOpacity>

                              </View>
                            </TouchableOpacity>

                            {item.participants.length > 0 ?

                              <View style={{ flexDirection: 'row', marginTop: Height(0.5), marginBottom: 24, marginLeft: Width(6), height: Height(3) }}>
                                {/* <FlatList
                          data={items}
                          renderItem={({ item, index }) => this.renderItem(item, index)}
                        /> */}

                                <View style={{ height: Height(3), width: Height(3), borderRadius: Height(3) }}>
                                  <Image style={{ height: '100%', width: '100%', borderRadius: Height(1.5) }} source={item.participants[0].image != '' ? { uri: item.participants[0].image } : avtar} />
                                </View>
                                <TouchableOpacity onPress={() => this.onUsersPressed(item)}>
                                  <View style={{ height: Height(3), width: Height(3), borderRadius: Height(3), marginLeft: -11, zIndex: 1 }}>
                                    <Image style={{ height: '100%', width: '100%', borderRadius: Height(1.5) }} source={require('../../../assets/images/dots.png')} />
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ bottom: 0, position: 'absolute', right: Width(8) }} onPress={() => { this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "Wanna go to " + item.line_1 + "?", Final_date: '', eventby: item.name }) }}>
                                  <Image resizeMode="contain" source={ic_chat} />
                                </TouchableOpacity>
                              </View>

                              :
                              <View style={{ flexDirection: 'row', marginTop: Height(0.5), marginBottom: 24, marginLeft: Width(6), height: Height(2) }}>
                                <TouchableOpacity style={{ bottom: 0, position: 'absolute', right: Width(8) }} onPress={() => { this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "Wanna go to " + item.line_1 + "?", Final_date: '', eventby: item.name }) }}>
                                  <Image resizeMode="contain" source={ic_chat} />
                                </TouchableOpacity>
                              </View>

                            }

                            {/* <View style={{ marginTop: Height(2), height: Height(2) }} /> */}
                          </View>

                          :

                          null
                      }

                      {
                        item.plan_type == 4 ?

                          <View style={{
                            marginHorizontal: Width(4), borderRadius: 5, backgroundColor: '#F2F4F7',
                            shadowColor: "#000",
                            shadowOffset: {
                              width: 0,
                              height: 2,
                            },
                            shadowOpacity: 0.0,
                            shadowRadius: 0.0,
                            borderRadius: 10,
                            elevation: 5,
                          }}>
                            <TouchableOpacity style={{ marginTop: Height(2), flexDirection: 'row' }} onPress={() => this.goToEventDetails(item)}>
                              <View style={{ marginLeft: Width(5) }}>
                                <TouchableHighlight onPress={() => { }} underlayColor="transparent" style={{ height: Height(5), width: Height(5), borderRadius: Height(5), overflow: 'hidden' }}>
                                  <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.photo }} />
                                </TouchableHighlight>
                              </View>
                              <View style={{ marginLeft: Width(4) }}>
                                <Text style={{ fontSize: FontSize(16), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, width: '88%' }} numberOfLines={1}>{item.plan_type == 2 ? item.activity : activity}</Text>
                                <Text style={{ fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Bold }}>{item.login_id == this.state.login_id ? 'Me' : item.name}</Text>
                                {/* <View style={{ flexDirection: 'row',  marginTop: Height(3),alignItems: 'center' }}>
                              <Image source={calendar} />
                              <View style={{ marginLeft: Width(2), }}>
                                <Text style={{ color: colors.dargrey, fontFamily: fonts.Roboto_Bold, fontSize: FontSize(13) }}>{moment(item.start_date).format('ddd, MMM D \u00B7 LT')}</Text>
                              </View>
                            </View>
                            <View style={{ flexDirection: 'row' ,marginTop: Height(3)}}>
                              <Image source={location} />
                              <View style={{ marginLeft: Width(2) }}>
                                <Text style={{ fontFamily: fonts.Roboto_Bold, fontSize: FontSize(13), color: colors.dargrey }}>{item.line_1}</Text>
                                <Text style={{ width: Width(55), marginTop: Height(0.5), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(10), color: colors.fontDarkGrey }}>{item.line_2}</Text>
    
                              </View>
                            </View> */}

                                <TouchableOpacity
                                  style={{ flexDirection: 'row', marginTop: Height(2) }}>
                                  <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={chai_cup} />
                                  {item.activity == '' ? <Text
                                    onPress={item.total_activity_suggestions == 0 ? () => this.props.navigation.navigate('Activity', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true, title_header: "I'm free, wanna hangout?" }) : () => this.props.navigation.navigate('SuggestActivity', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false, title_header: "I'm free, wanna hangout?" })}

                                    style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6', marginRight: Width(5) }}>{item.total_activity_suggestions == 0 ? 'Suggest an activity' : 'View activity suggestions'}</Text>
                                    : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey, marginRight: Width(5) }}>{item.activity}</Text>}
                                </TouchableOpacity>

                                <TouchableOpacity
                                  style={{ flexDirection: 'row', marginTop: Height(1), marginBottom: Height(1) }}>
                                  <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={calendar} />
                                  {item.start_date == '' ? <Text
                                    onPress={item.total_datetime_suggestions == 0 ? () => this.props.navigation.navigate('DateTime', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true }) : () => this.props.navigation.navigate('SuggestDateTime', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false })}

                                    style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6', marginRight: Width(5) }}>{item.total_datetime_suggestions == 0 ? 'Suggest a date & time' : 'View date & time suggestions'}</Text>
                                    : <Text style={{ width: Width(60), paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey, marginRight: Width(5) }}>{this.setFinalDate(item)}</Text>}
                                  {/* : <Text style={{ width: Width(60), paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey, marginRight:Width(5) }}>{moment(item.start_date).format('ddd, MMM D \u00B7 LT')}</Text>} */}
                                </TouchableOpacity>

                                <TouchableOpacity
                                  style={{ flexDirection: 'row', marginTop: Height(0), marginRight: Width(4), marginBottom: Height(1.5) }}>
                                  <Image resizeMode="contain" style={{ tintColor: colors.whatFontColor, marginTop: 3 }} source={location} />
                                  {
                                    item.line_1 == '' ?
                                      <Text
                                        onPress={item.total_location_suggestions == 0 ? () => this.props.navigation.navigate('Location', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true }) : () => this.props.navigation.navigate('SuggestLocation', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false })}
                                        style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6', marginRight: Width(5) }}>{item.total_location_suggestions == 0 ? 'Suggest a location' : 'View location suggestions'}</Text>
                                      :
                                      <View style={{ width: '88%' }}>
                                        <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.line_1}</Text>
                                        <Text style={{ fontSize: FontSize(10), fontFamily: fonts.Roboto_Regular, color: '#c1c1c1', paddingLeft: Width(3.3) }}>{item.line_2}</Text>
                                      </View>
                                  }

                                </TouchableOpacity>

                              </View>
                            </TouchableOpacity>

                            {item.participants.length > 0 ?

                              <View style={{ flexDirection: 'row', marginTop: Height(0.5), marginBottom: 24, marginLeft: Width(6), height: Height(3) }}>
                                {/* <FlatList
                          data={items}
                          renderItem={({ item, index }) => this.renderItem(item, index)}
                        /> */}

                                <View style={{ height: Height(3), width: Height(3), borderRadius: Height(3) }}>
                                  <Image style={{ height: '100%', width: '100%', borderRadius: Height(1.5) }} source={item.participants[0].image != '' ? { uri: item.participants[0].image } : avtar} />
                                </View>
                                <TouchableOpacity onPress={() => this.onUsersPressed(item)}>
                                  <View style={{ height: Height(3), width: Height(3), borderRadius: Height(3), marginLeft: -11, zIndex: 1 }}>
                                    <Image style={{ height: '100%', width: '100%', borderRadius: Height(1.5) }} source={require('../../../assets/images/dots.png')} />
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ bottom: 0, position: 'absolute', right: Width(8) }} onPress={() => { this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "I'm free, wanna hangout?", Final_date: '', eventby: item.name }) }}>
                                  <Image resizeMode="contain" source={ic_chat} />
                                </TouchableOpacity>
                              </View>

                              :
                              <View style={{ flexDirection: 'row', marginTop: Height(0.5), marginBottom: 24, marginLeft: Width(6), height: Height(2) }}>
                                <TouchableOpacity style={{ bottom: 0, position: 'absolute', right: Width(8) }} onPress={() => { this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "I'm free, wanna hangout?", Final_date: '', eventby: item.name }) }}>
                                  <Image resizeMode="contain" source={ic_chat} />
                                </TouchableOpacity>
                              </View>
                            }

                            {/* <View style={{ marginTop: Height(2), height: Height(2) }} /> */}
                          </View>

                          :

                          null
                      }

                    </View>
                  );
                })


            }

            {
              this.state.UpcomingList.length == 0 ? null :
                <View style={{ height: Height(8) }} />
            }
          </ScrollView>

        </View>
      default:
        return null;
    }
  };

  onTabChange(idx) {
    //alert(idx)

    this.setState({ index: idx })

    if (idx == 0) {
      if (global.friend_request_count > 0) {
        global.noti_count = global.friend_request_count
      }
      else {
        global.noti_count = 0
      }
      global.noti_tab_count = 0
      store.dispatch({ type: NOTIFICATION_COUNT, data: global.noti_tab_count })

      store.dispatch({ type: NOTIFICATION, data: global.noti_count })
      var date = moment.utc().subtract(5, 'seconds').format('YYYY-MM-DD HH:mm:ss');
      AsyncStorage.setItem('notidate', date)
      this.getNotificationsList(false)
   
    }
    else if (idx == 1) {
      this.mixpanel.identify(store.getState().auth.user.email_address);
      this.mixpanel.track('Notifications - Friend requests');
      if (global.noti_count > 0) {
        global.noti_count = global.noti_count - global.friend_request_count
      }

      store.dispatch({ type: NOTIFICATION, data: global.noti_count })
      var date = moment.utc().subtract(5, 'seconds').format('YYYY-MM-DD HH:mm:ss');
      AsyncStorage.setItem('notidate', date)

      global.friend_request_count = 0
      store.dispatch({ type: FRIENDREQUEST, data: global.friend_request_count })
      var date = moment.utc().subtract(5, 'seconds').format('YYYY-MM-DD HH:mm:ss');
      AsyncStorage.setItem('frienddate', date)
      this.getFriendRequests(false)
    }
    else {
      this.mixpanel.identify(store.getState().auth.user.email_address);
      this.mixpanel.track('Notifications - Upcoming');
      this.getUpcomingList()
    }

  }

  renderTabBar = (props) => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    return (
      <View style={styles.tabBar}>
        {props.navigationState.routes.map((route, i) => {
          //   const color = props.position.interpolate({
          //     inputRange,
          //     outputRange: inputRange.map(inputIndex => (inputIndex === i ? '#333333' : '#767676'))
          //   });
          const color = "#363169"
          return (
            <TouchableOpacity
              key={i}
              style={[styles.tabItem, this.state.index === i ? styles.selectedTab : styles.notSelected]}
              //onPress={() => this.setState({ index: i })}>
              onPress={() => this.onTabChange(i)}>
              <View>
                {i == 1 ? <FriendRequestIcon /> : null}
                {i == 0 ? <NotificationCountIcon /> : null}
                <Animated.Text style={{ color, fontSize: FontSize(16), fontFamily: fonts.Raleway_Bold }}>{route.title}</Animated.Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };
  showActionSheet = () => {
    this.ActionSheet.show();
  };
  cancelEvent = (index) => {

  }



  render() {
    StatusBar.setBarStyle('light-content', true);

    return (
      <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: colors.white }} >
        <Toastt ref="toast"></Toastt>
        <BackgroundImage >
          {this.renderTopBar()}

          <SafeAreaView />

          <View style={{ position: 'absolute', bottom: 0, right: 0 }}>

          </View>
          <TabView
            swipeEnabled={false}
            navigationState={this.state}
            renderScene={this.renderScene}
            renderTabBar={this.renderTabBar}
            //onIndexChange={this.tabViewIndexChange}
            onIndexChange={index => this.setState({ index })}

          />
          <ActionSheet
            ref={o => (this.ActionSheet = o)}
            title={"This event will be cancelled and guests" + "\n" + "will be notified of the cancelation."}
            options={["Cancel Event", "Cancel"]}
            cancelButtonIndex={1}
            destructiveButtonIndex={1}
            onPress={index => this.cancelEvent(index)}
          />


          <SafeAreaView />
        </BackgroundImage>
      </Container>
    );
  }
}

export default Notification;
