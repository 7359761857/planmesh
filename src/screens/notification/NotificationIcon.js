import React from 'react';
import { Text, Image, View } from 'react-native';
import { connect } from 'react-redux';
import { store } from "../../redux/store";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { FontSize} from "../../config/dimensions";

const ic_chat = require("../../../assets/images/Dashboard/chat_icon.png");

class NotificationIcon extends React.Component {
  constructor(props) {
    // anything you need in the constructor
    super(props)
  }

  render() {
    const { notifications, tintColor } = this.props;

   // below is an example notification icon absolutely positioned 
    return (
        
        <View>
            <MaterialIcons name='notifications-none' size={25} color={tintColor} />
            {
                notifications > 0 ?
                notifications > 99 ?
                <View style= {{backgroundColor:'red', position:'absolute',height:17,width:25,top:-11,right:-11, borderRadius:8.5, justifyContent:'center'}}>
                    <Text style = {{alignSelf:'center', color:'#fff', fontSize:FontSize(10), fontWeight:'400', textAlign:'center'}}>99+</Text>
                </View>
                :
                <View style= {{backgroundColor:'red', position:'absolute',height:17,width:17,top:-9,right:-9, borderRadius:8.5, justifyContent:'center'}}>
                    <Text style = {{alignSelf:'center', color:'#fff', fontSize:FontSize(10), fontWeight:'400'}}>{notifications}</Text>
                </View>
                :
                null
            }
        </View>
        


    //   <View style={{
    //     zIndex: 0,
    //     flex: 1,
    //     alignSelf: 'stretch',
    //     justifyContent: 'space-around',
    //     alignItems: 'center'}}>
    //    <MaterialIcons name='notifications-none' size={25} color={tintColor} />
    //    {notifications > 0 ?
    //     <View style={{
    //       position: 'absolute',
    //       top: 5,
    //       right: 5,
    //       borderRadius: 50,
    //       backgroundColor: 'red',
    //       zIndex: 2}}>
    //       <Text>{notifications}</Text>
    //     </View>
    //     : undefined}
    //   </View>
    );
}

}
const mapStateToProps = state => ({ notifications: store.getState().auth.notification_count});
export default connect(mapStateToProps, null)(NotificationIcon);