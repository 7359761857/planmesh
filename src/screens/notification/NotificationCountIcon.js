import React from 'react';
import { Text, Image, View } from 'react-native';
import { connect } from 'react-redux';
import { store } from "../../redux/store";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { FontSize} from "../../config/dimensions";

const ic_chat = require("../../../assets/images/Dashboard/chat_icon.png");

class NotificationCountIcon extends React.Component {
  constructor(props) {
    // anything you need in the constructor
    super(props)
  }

  render() {
    const { notifications} = this.props;

   // below is an example notification icon absolutely positioned 
    return (
        
        <View>
            {
                notifications > 0 ?
                notifications > 99 ?
                <View style= {{backgroundColor:'red', position:'absolute',height:17,width:25,top:-13,right:-13, borderRadius:8.5, justifyContent:'center'}}>
                    <Text style = {{alignSelf:'center', color:'#fff', fontSize:FontSize(10), fontWeight:'400', textAlign:'center'}}>99+</Text>
                </View>
                :
                <View style= {{backgroundColor:'red', position:'absolute',height:17,width:17,top:-11,right:-11, borderRadius:8.5, justifyContent:'center'}}>
                    <Text style = {{alignSelf:'center', color:'#fff', fontSize:FontSize(10), fontWeight:'400'}}>{notifications}</Text>
                </View>
                :
                null
            }
        </View>
    );
}

}
const mapStateToProps = state => ({ notifications: store.getState().auth.new_notification_count});
export default connect(mapStateToProps, null)(NotificationCountIcon);