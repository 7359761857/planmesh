const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import { isIphoneX } from '../Default/is-iphone-x'
import { Width, colors } from '../../config/dimensions';
const HEADER_SIZE = isIphoneX() ? 84 : 64;
const HEADER_PADDING_SIZE = isIphoneX() ? 40 : 16;

export default {

    headerAndroidnav: {
        backgroundColor: '#41199B',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        height:isIphoneX() ? 85 : 64,

        ...Platform.select({
            ios: {
                height: HEADER_SIZE,
                paddingTop: HEADER_PADDING_SIZE,
            },
            android: {
            },
        }),
    },
    headerTitle: {
        letterSpacing: 0.02,
        fontSize: 20,
        fontFamily: "Raleway-Medium",
        color: 'white',
        ...Platform.select({
            ios: {
              fontWeight: '500',
            },
            android: {
            },
          })
    },
    labelInput: {
        color: '#363169',
        fontSize: 15,
        fontFamily: "Roboto-Bold",
        paddingLeft:0
    },
    formInput: {
        color: '#3a4759',
        fontSize: 16,
        fontFamily: "Roboto-Regular",
        marginLeft:25,
        marginRight:30,
        marginTop:25,
    },
    formInputFocus:{
        borderBottomWidth: 2,
        borderColor: '#3A4759',
        paddingLeft:0,
    },
    formInputUnFocus:{
        borderBottomWidth: 2,
        borderColor: '#BBC1C8',
    },
    labelInputUnFocus:{
        color: '#8f8f90',
        fontSize: 16,
        fontFamily: "Roboto-Regular",
        paddingLeft:0
    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        fontFamily: "Roboto-Regular",

    },
    labelInputError: {
        color:'red'
    },
    tabBar: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        justifyContent:'space-between',
      },
      tabItem: {
        alignItems: 'center',
        paddingVertical:16,
        paddingHorizontal:15
      },
      selectedTab: {
        borderBottomColor: '#EC4AB9',
        borderBottomWidth: 3,
      },
      notSelected: {
        borderBottomColor: colors.lightgrey,
        borderBottomWidth:0.5,
      }
      
};
