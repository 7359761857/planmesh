import React, { Component } from "react";
import { SafeAreaView, Text, Dimensions, Image, ScrollView, Alert, ImageBackground, TouchableOpacity } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View,  Button } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import SplashScreen from 'react-native-splash-screen'
import PageControl from 'react-native-page-control';
// import firebase from 'react-native-firebase';
// import Orientation from 'react-native-orientation';
//import branch, { BranchEvent } from 'react-native-branch'

import ValidationComponent from 'react-native-form-validator';

let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height


const intro1 = require("../../../assets/images/Introductions/Intro_Screen1.png");
const intro2 = require("../../../assets/images/Introductions/Intro_Screen2.png");
const intro3 = require("../../../assets/images/Introductions/Intro_Screen3.png");
const intro4 = require("../../../assets/images/Introductions/Intro_Screen4.png");
const intro5 = require("../../../assets/images/Introductions/Intro_Screen51.png");
const btnBg = require("../../../assets/images/btnBg.png");

class Splash extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = { 
      page: 0,
      loading:false
     };
  }
  

  handlePageChange = (e) => {
    var offset = e.nativeEvent.contentOffset;
    if (offset) {
      var page = Math.round(offset.x / deviceWidth);
      if (this.state.page != page) {
        this.setState({ page: page });
      }
    }
  }
  componentWillMount() {
    
    console.log('componentWillMount');

    //this.props.navigation.push('Home')

    //return

    var navigation = this.props.navigation
    Pref.getData(Pref.kUserId, (value) => {
      if (value >= 1) {
        userid = value;
        Pref.getData(Pref.kUserInfo, (userinfo) => {
          var obj = userinfo;
          token = obj.token
          is_premium = obj.is_premium
          navigation.push('dashboard')
          //call myinfo ws in background here.
          this.wsUserInfocall()
        })
      } else {
        //navigation.push('Home')
        Pref.getData(Pref.kTourVisit, (value) => {
          console.log(value)
          if (value == 1) {
             this.setState({loading:true})

             navigation.push('Home')
          }
          else{
            this.setState({loading:true})
            SplashScreen.hide()
          }
        })
      }
    });
  }

  onGetStartedClick = (page) => {
    // this.refs.scrollView.scrollTo({ x: page * deviceWidth, y: 0, animated: true });
    Pref.setData(Pref.kTourVisit, 1)
    this.props.navigation.navigate('Home')
  }

  componentDidMount(){
    // branch.subscribe(({ error, params }) => {
    //   if (error) {
    //     console.error('Error from Branch: ' + error)
    //     return
    //   }
    //   // params will never be null if error is null
    // })
  }
/*
  async componentDidMount() {
    SplashScreen.hide();
    this.checkPermission();
    Orientation.lockToPortrait()
  }

  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
      this.createNotificationListeners(); //add this line
    } else {
      this.requestPermission();
    }
  }

  //3
  async getToken() {
    Pref.getData(Pref.kdeviceToken, (value) => {
      console.log('value push', value);
      if (value == null) {
        firebase.messaging().getToken().then(token => {
          deviceToken = token
          Pref.setData(Pref.kdeviceToken, token)
        })
      } else {
        deviceToken = value
      }
    })

  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }


  wsUserInfocall = async () => {
    let httpMethod = 'POST'
    let strURL = userinfo
    let headers = {
      'Content-Type': 'application/json',
      'token': token,
      'userid': userid,
    }
    let params = {
    }

    APICall.callWebServiceBackground(httpMethod, strURL, headers, params, (resJSON) => {
      console.log('wsFavcall Screen -> ');
      console.log(resJSON)
      if (resJSON.status == 200) {
        let data = resJSON.data
        is_premium = data.is_premium
        console.log('userinfocall', data)
        Pref.setData(Pref.kUserInfo, data)
        return
      } else {
        this.refs.toast.show(resJSON.message);
      }

    })
  }
*/

  createNotificationListeners = async () => {
    console.log('createNotificationListeners');
    //   /*
    //   * Triggered when a particular notification has been received in foreground
    //   * */
    // this.notificationListener = firebase.notifications().onNotification((notification) => {
    //   console.log('onNotification');
    //   const { title, body } = notification;
    //   this.showAlert(title, body);
    // });



    //   /*
    //   * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    //   * */
    // this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
    //   console.log('onNotificationOpened');
    //   const { title, body } = notificationOpen.notification;
    //   this.showAlert(title, body);
    // });

    //   /*
    //   * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    //   * */
    //  firebase.notifications().getInitialNotification().then(notificationOpen => {
    //   console.log('getInitialNotification');

    //     const { title, body } = notificationOpen.notification;
    //     this.showAlert(title, body);
    //   })
    /*
    * Triggered for data only payload in foreground
    * */
    // this.messageListener = firebase.messaging().onMessage((message) => {
    //   //process data message
    //   console.log(JSON.stringify(message));
    //   const { title, body } = message.data;
    //   this.showAlert(title, body);
    //   this.props.navigation.setParams({ newConversations: true })
    // });
  }

  showAlert(title, body) {
    console.log('alert call')
    Alert.alert(
      title, body,
      [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false },
    );
  }
  //Remove listeners allocated in createNotificationListeners()
  componentWillUnmount() {
    //this.notificationListener();
    //this.notificationOpenedListener();
  }

  render() {
    if(this.state.loading==false){
      return null
    }
    return (
      <Container flex-direction={"row"} style={{ flex: 1,backgroundColor:'#FFF' }} >

        <BackgroundImage >
          <ScrollView ref="scrollView" horizontal={true} pagingEnabled={true} bounces={false} showsHorizontalScrollIndicator={false} scrollEventThrottle={160} onMomentumScrollEnd={(e) => this.handlePageChange(e)}>
            <View style={{ width: deviceWidth, alignItems: "center", justifyContent: 'center',backgroundColor:'#FFF' }}>
              <Image source={intro1} style={[styles.appicon, { alignContent: 'center', resizeMode: 'contain' }]}></Image>
              <Text style={[styles.textHeader]}>Welcome to Planmesh</Text>
              <Text style={[styles.textSubHeader]}>Plan get togethers, brunch, study sessions, your next date, you name it</Text>
            </View>
            <View style={{ width: deviceWidth, alignItems: "center", justifyContent: 'center',backgroundColor:'#FFF' }}>
              <Image source={intro2} style={[styles.appicon, { alignContent: 'center', resizeMode: 'contain' }]}></Image>
              <Text style={[styles.textHeader]}>What or Where</Text>
              <Text style={[styles.textSubHeader]}>Let your friends know what you wanna do or where you wanna go</Text>
            </View>
            <View style={{ width: deviceWidth, alignItems: "center", justifyContent: 'center',backgroundColor:'#FFF' }}>
              <Image source={intro3} style={[styles.appicon, { alignContent: 'center', resizeMode: 'contain' }]}></Image>
              <Text style={[styles.textHeader]}>When</Text>
              <Text style={[styles.textSubHeader]}>You can also let your friends know when you're free to hang</Text>
            </View>
            <View style={{ width: deviceWidth, alignItems: "center", justifyContent: 'center',backgroundColor:'#FFF' }}>
              <Image source={intro4} style={[styles.appicon, { alignContent: 'center', resizeMode: 'contain' }]}></Image>
              <Text style={[styles.textHeader]}>Create Polls</Text>
              <Text style={[styles.textSubHeader]}>Find the best time, activity or location for any event with polls and invite participants to vote</Text>
            </View>
            <View style={{ width: deviceWidth, alignItems: "center", justifyContent: 'center',backgroundColor:'#FFF' }}>
              <Image source={intro5} style={{ alignContent: 'center', resizeMode: 'contain',height:300,width:300, }}></Image>
              <Text style={[styles.textHeader]}>Event Chats</Text>
              <Text style={[styles.textSubHeader]}>Chat with friends one-on-one or in groups to clarify details</Text>
            </View>
          </ScrollView>
          <View style={{ alignItems: 'center', justifyContent: 'center', bottom: 25 }} >
            {
              this.state.page == 4 ?
               <TouchableOpacity style={{ width: '100%',marginTop:-10}} onPress={() => this.onGetStartedClick()}>
                <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' },checkFontWeight("700")]}>Continue</Text>
                </ImageBackground>
              </TouchableOpacity> 
              : <View>
                  <PageControl
                    style={{}}
                    numberOfPages={5}
                    currentPage={this.state.page}
                    hidesForSinglePage
                    pageIndicatorTintColor='#D6D6D6'
                    currentPageIndicatorTintColor='#FF00BA'
                    indicatorStyle={{ borderRadius: 3 }}
                    currentIndicatorStyle={{ borderRadius: 3 }}
                    indicatorSize={{ width: 33, height: 6 }}
                    onPageIndicatorPress={this.onItemTap}
                  />
                  <Button uppercase = {false} transparent style={{ alignSelf: 'center', marginTop: 20 }} onPress={() => this.onGetStartedClick()} ><Text style={{ fontSize: 15, fontFamily: "Roboto-Regular", color: "#007aff", textAlign: 'center' }}>Skip</Text></Button>

                </View>
            }
            {/* <PageControl
            style={{}}
            numberOfPages={5}
            currentPage={this.state.page}
            hidesForSinglePage
            pageIndicatorTintColor='#D6D6D6'
            currentPageIndicatorTintColor='#FF00BA'
            indicatorStyle={{ borderRadius: 3 }}
            currentIndicatorStyle={{ borderRadius: 3 }}
            indicatorSize={{ width: 33, height: 6 }}
            onPageIndicatorPress={this.onItemTap}
          />
          <Button transparent style={{alignSelf:'center',marginTop:20}}><Text style={{fontSize:15,fontFamily:"Roboto-Regular",color:"#007aff",textAlign:'center'}}>Skip</Text></Button>
          */}

          </View>
        </BackgroundImage>
      </Container>
    );
  }
}

export default Splash;
