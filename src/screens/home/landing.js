import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, StatusBar, ImageBackground, TouchableOpacity, BackHandler, Alert, Platform } from "react-native";
import { Container, View, Text, Button } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import HandleBack from "../Default/HandleBack";//../../screens/Default/HandleBack
import publicIP from 'react-native-public-ip';
import SplashScreen from 'react-native-splash-screen'
import { Width, Height, FontSize } from "../../config/dimensions";
import NavigationService from "../../services/NavigationService";
//import DeviceInfo from 'react-native-device-info';
import * as DeviceUUID from './../../../src/services/device-uuid';
let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height


const intro5 = require("../../../assets/images/landingIcon.png");
const appIcon = require("../../../assets/images/planMeshIcon.png");
const btnBg = require("../../../assets/images/btnBg.png");
import MixpanelManager from '../../../Analytics';


class Landing extends ValidationComponent {

    constructor(props) {
        super(props);
        this.state = { page: 0 };
        // this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
        publicIP()
            .then(ip => {
                console.log(ip);
                // '47.122.71.234'
            })
            .catch(error => {
                console.log(error);
                // 'Unable to get IP address.'
            });
    }



    componentDidMount() {
        SplashScreen.hide()
        this._navListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBarStyle('dark-content');
            Platform.OS == 'android' && StatusBar.setBackgroundColor('transparent');

        });
        //alert(JSON.stringify(DeviceUUID.default))
        //var uuid = new DeviceUUID().get();

    }

    componentWillUnmount() {
        this._navListener.remove();
    }
    onLoginClick = () => {
        //this.props.navigation.navigate('signup')
        this.props.navigation.push('SignupOptions')
    }
    onSignUpClick = () => {

        this.props.navigation.navigate('SocialLogin')
        //this.props.navigation.navigate('Login')
        //NavigationService.navigate('SignupStep1')
    }

    onBack = () => {
        Alert.alert(
            'Exit',
            'Are you sure you want to exit?',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'OK', onPress: () => BackHandler.exitApp() },
            ],
            { cancelable: false },
        );
        return true;
    };


    render() {
        return (

            <HandleBack onBack={this.onBack}>
                <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#FFF' }} >
                    <BackgroundImage >
                        <SafeAreaView />
                        <View style={{ paddingLeft: Width(5), marginTop: Height(3), flexDirection: 'row' }}>
                            <Image source={appIcon} style={{ height: Height(6), resizeMode: 'contain' }}></Image>
                        </View>
                        <View style={{ paddingLeft: Width(5) }}>
                            <Text style={[{ color: '#41199b', paddingTop: Height(2), fontSize: FontSize(27), fontFamily: "Raleway-ExtraBold" }, checkFontWeight("800")]}>The easiest way to</Text>
                            <Text style={[{ color: '#41199b', paddingTop: Height(0.1), fontSize: FontSize(27), fontFamily: "Raleway-ExtraBold" }, checkFontWeight("800")]}>plan with your friends.</Text>
                            <Text style={[{ color: '#41199b', paddingTop: Height(1), fontSize: FontSize(22), fontFamily: "Raleway-Medium", letterSpacing: 0.02 }, checkFontWeight("500")]}>Get started today!</Text>
                        </View>
                        <View style={{ flex: 1.2, alignContent: 'center', alignItems: 'center', justifyContent: 'center', paddingBottom: Height(4) }}>
                            <Image source={intro5} resizeMode="contain" style={{ alignSelf: 'center', height: Height(40), width: Width(90) }}></Image>
                        </View>
                        <View style={{ bottom: Height(5) }}>
                            <TouchableOpacity style={{ width: '100%' }} onPress={() => this.onLoginClick()}>
                                <ImageBackground source={btnBg} style={{ width: '100%', height: Height(9), alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Sign Up</Text>
                                </ImageBackground>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} onPress={() => this.onSignUpClick()}>
                                <Text style={{ color: '#41199b', fontSize: FontSize(17), fontFamily: "Roboto-Regular", letterSpacing: 0.02 }}>Already have an account?</Text><Text style={{ color: '#ff00ba', fontSize: FontSize(17), fontFamily: "Roboto-Regular", letterSpacing: 0.02 }}> Sign In!</Text>
                            </TouchableOpacity>
                        </View>
                        <SafeAreaView />
                    </BackgroundImage>
                </Container>
            </HandleBack>

        );
    }
}

export default Landing;
