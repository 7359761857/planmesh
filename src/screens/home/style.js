const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {

  welcome: {
    fontSize: 34.5,
    color: 'white',
    fontFamily: font_SFProDisplay_Bold,
    marginTop: 83,
  },
  appName: {
    fontSize: 34.5,
    color: '#ff9b00',
    fontFamily: font_SFProDisplay_Bold,
  },
  appicon: {
    width: 216 * deviceWidth / 320,
    height: 231 * deviceWidth / 320,
  },
  appicon1: {
    width: 500,
    height: 500
  },
  bottomView: {
    paddingTop: 0,
    backgroundColor: 'transparent',
  },
  textHeader: {
    textAlign: 'center',
    marginTop: 30,
    color: '#41199b',
    fontSize: 26,
    fontFamily: 'Raleway-ExtraBold',
    letterSpacing: 0.02,
    ...Platform.select({
      ios: {
        fontWeight: '800',
      },
      android: {
      },
    })
  },
  textSubHeader: {
    textAlign: 'center',
    marginTop: 10,
    marginLeft: 35,
    marginRight: 35,
    color: '#3a4759',
    fontSize: 18,
    fontFamily: "Roboto-Regular",
    letterSpacing: 0.02
  }

};
