import React, { Component } from "react";
import AsyncStorage from '@react-native-community/async-storage';
import { View } from "native-base";
import { store } from "../redux/store";
import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyARwBSs2WkmniGOWlyVPaRxWRm-VVyb6Vg",
    authDomain: "planmesh-415e3.firebaseapp.com",
    databaseURL: "https://planmesh-415e3.firebaseio.com",
    projectId: "planmesh-415e3",
    storageBucket: "planmesh-415e3.appspot.com",
    messagingSenderId: "80422853890",
    appId: "1:80422853890:web:2ce10c52f6c8c41d12761b",
    measurementId: "G-V6R8C9XSZS"
  };

class AuthLoadingScreen extends Component {
        constructor (props)
        {
            super(props);
            this.state = {
                    token:'',
                    login:false,
                    loading:false,
            }
        }

        componentDidMount()
        {
            //alert('hiii')

            if (!firebase.apps.length) {
                firebase.initializeApp(firebaseConfig); // add by utsav
            }

            if(store.getState().auth.isLoggedIn){
                this.setState({login:true,loading:false})
                global.noti_count = store.getState().auth.user.notification_count
                this.props.navigation.navigate('App')
                //console.log('App.js after logged in: '+global.noti_count)
                //return
              }
              else{
                this.setState({login:false,loading:false})
                this.props.navigation.navigate('Auth')
                //return
              }

            // await AsyncStorage.getItem('authtoken').then((value)=>{
            //     this.props.navigation.navigate((value && value != '') ? 'App' : 'Auth')
            //   });
        }

        render()
        {
            return (
                <View style = {{flex:1}}></View>
            )
        }

}

export default AuthLoadingScreen;