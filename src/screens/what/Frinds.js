import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, Platform, BackHandler, ImageBackground, TouchableOpacity, StatusBar, TouchableHighlight, Text, Switch, TextInput } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Button, Header, Left, Right, Content, Item, Body } from "native-base";
import styles from "./style";
import HandleBack from "../Default/HandleBack";//../../screens/Default/HandleBack
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import ImagePicker from 'react-native-image-crop-picker';
import RNProgressHUB from 'react-native-progresshub';

import AntDesign from 'react-native-vector-icons/AntDesign';
import FloatingLabel from 'react-native-floating-labels';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
const backWhite = require("../../../assets/images/backWhite.png");
const btnBgGrey = require("../../../assets/images/btnBg.png");
const who = require("../../../assets/images/Dashboard/who_1.png");
const cup = require("../../../assets/images/Dashboard/full_cup.png");
const Location_new = require("../../../assets/images/Dashboard/full_Location.png");
const time = require("../../../assets/images/Dashboard/full_time.png");
const unSelectRadio = require("../../../assets/images/Dashboard/unSelectRadio.png");
const SelectRadio = require("../../../assets/images/selectRadio.png");
import moment from 'moment';
import { API_ROOT } from "../../config/constant";
import { store } from "../../redux/store";
import MixpanelManager from '../../../Analytics'

const current_location = require("../../../assets/images/current_location.png");
const bg = require("../../../assets/images/Dashboard/bg.png");
const ic_add_cover = require("../../../assets/images/Dashboard/ic_add_cover_photo.png");
const ic_edit_event = require("../../../assets/images/Dashboard/ic_edit_event.png");
const ic_edit_cover = require("../../../assets/images/Dashboard/ic_edit_cover.png");
const default_placeholder = require("../../../assets/images/Dashboard/default_placeholder.png");
const ic_close_black = require("../../../assets/images/Dashboard/ic_cross_black.png");
const ic_unchecked = require("../../../assets/images/Dashboard/circle_empty.png");
const ic_checked = require("../../../assets/images/Dashboard/circle_fill.png");
const btn_continue = require("../../../assets/images/Dashboard/btn_continue.png");

var activity = ''
import Modal from "react-native-modal";

class Frinds extends ValidationComponent {
    _didFocusSubscription
    constructor(props) {
        super(props);
        this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
        this.state = {
            token: store.getState().auth.user.token,
            login_id: store.getState().auth.user.login_id,
            name: 'Leaticia',
            value: 'friend',
            sel_val: true,
            modalVisible: false,
            frinds_name: 'Invite friends',
            friendName: 'Friends',
            activity: 'Add an activity ',
            location: 'Add a location',
            full_address: '',
            current_time: 'Add a date & time',
            description: '',
            start_date: '',
            end_date: '',
            is_now: '0',
            share_with: 1,
            value: 'friend',
            friends: '',
            contacts:'',
            shareList: [{
                name: 'Friends',
                key: 'friend',
                id: 1,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },
            {
                name: 'Friends of friends',
                key: 'fof',
                id: 2,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },
            {
                name: 'Only me',
                key: 'me',
                id: 0,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },

            ],
            is_edit_plan: this.props.navigation.state.params.is_edit_plan,
            is_from_profile: this.props.navigation.state.params.is_from_profile != null ? this.props.navigation.state.params.is_from_profile : false,
            item: '',
            plan_id:'',
            is_allday:'0',
            photo_path:'',
            is_from_feed: this.props.navigation.state.params.is_from_feed != null ? true : false,
            sharemodalVisible:false,
            ischecked:false,
        }
        this._didFocusSubscription = props.navigation.addListener('didFocus', payload => {
            this.retriveData()
        })
    }

    async retriveData() {


        if(this.state.is_edit_plan == true)
        {
            this.setState(
                {   
                    item:this.props.navigation.state.params.item,
                    sel_val:this.props.navigation.state.params.item.is_guest == '1' ? true : false,
                    full_address:this.props.navigation.state.params.item.line_2,
                    location:this.props.navigation.state.params.item.line_1,
                    description:this.props.navigation.state.params.item.description,
                    plan_id:this.props.navigation.state.params.item.plan_id,
                    is_now:this.props.navigation.state.params.item.is_now,
                    is_allday:this.props.navigation.state.params.item.is_allday,
                    photo_path:this.props.navigation.state.params.item.plan_image,
                }
                ,
                    () => {
                        if(this.state.item.share_with == 'Friends')
                        {
                            this.setState({share_with:1,friendName:'Friends', value:'friend'})
                        }
                        else if(this.state.item.share_with == 'Friends of friends')
                        {
                            this.setState({share_with:2,friendName:'Friends of friends',value:'fof'})
                        }
                        else
                        {
                            this.setState({share_with:0,friendName:'Only me',value:'me'})
                        }
                    }
                )

                //alert(this.state.item.share_with)
                
        }

        await AsyncStorage.getItem('What_Activity').then((value) => {
            this.setState({ activity: value })
        });
        await AsyncStorage.getItem('what_Location').then((value) => {
            this.setState({ location: value })
        });
        await AsyncStorage.getItem('what_FullAddress').then((value) => {
            this.setState({ full_address: value })
        });
        await AsyncStorage.getItem('what_CurrentDate').then((value) => {
            this.setState({ current_time: value })
        });
        await AsyncStorage.getItem('what_start').then((value) => {
            this.setState({ start_date: value })
        });
        await AsyncStorage.getItem('what_end').then((value) => {
            this.setState({ end_date: value })
        });
        await AsyncStorage.getItem('what_friends').then((value) => {
            this.setState({ friends: JSON.parse(value) })
        });
        await AsyncStorage.getItem('what_friends_name').then((value) => {
            this.setState({ frinds_name: JSON.parse(value) })
        });
        await AsyncStorage.getItem('what_contacts').then((value) => {
            this.setState({ contacts: JSON.parse(value) })
        });
        await AsyncStorage.getItem('what_is_now').then((value) => {
            this.setState({ is_now: value })
        });
        await AsyncStorage.getItem('what_is_allday').then((value) => {
            this.setState({ is_allday: value })
        });

        await AsyncStorage.getItem('what_desc').then((value) => {
            if(value == null)
            {
                this.setState({description:''})
            }
            else
            {
                this.setState({ description: value })
            }
        });

    }
    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={[styles.headerAndroidnav,{height:44}]}>
                    <StatusBar barStyle="light-content" backgroundColor="#41199B" />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                        <TouchableOpacity onPress={() => {
                            AsyncStorage.removeItem('What_Activity')
                            AsyncStorage.removeItem('what_Location')
                            AsyncStorage.removeItem('what_CurrentDate')
                            AsyncStorage.removeItem('what_start')
                            AsyncStorage.removeItem('what_end')
                            AsyncStorage.removeItem('what_friends_name')
                            AsyncStorage.removeItem('what_friends')
                            AsyncStorage.removeItem('what_desc')
                            AsyncStorage.removeItem('what_contacts')

                            navigation.goBack()
                        }}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity></View>
                    <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                        {theNavigation.state.params.is_edit_plan == true ? <Text style={styles.headerTitle} >Edit Event</Text> : <Text style={styles.headerTitle} >Review Event</Text>}
                    </View>

                </Header>
            )
        }
    }
    renderModal() {
        const { value } = this.state;

        return (
            <View>
                <Modal
                    isVisible={this.state.modalVisible} style={{ margin: 0 }} onBackdropPress={() => this.setState({ modalVisible: false })} onBackButtonPress={() => this.setState({ modalVisible: false })}>
                    <View style={{ height: Height(30), borderRadius: 5, width: '100%', position: 'absolute', bottom: 0, backgroundColor: '#FFF' }}>
                        
                        {
                            this.state.is_from_profile == true ? null
                            :

                            <View style={{ flexDirection: 'row', height: Height(7), justifyContent: 'center' }}>
                            <AntDesign
                                onPress={() => this.setState({ modalVisible: false })}
                                color={colors.black}
                                name="close" size={25} style={{ left: Width(3), position: 'absolute', top: Height(2) }} />
                            <Text style={{ alignSelf: 'center', fontFamily: fonts.Raleway_Bold, fontSize: FontSize(18), color: colors.dargrey }}> Share With</Text>

                            </View>
                        }
                        
                        <View style={{ borderBottomColor: '#EEEEEE', borderBottomWidth: 1 }} />
                        <View style={{ height: Height(1) }}></View>
                        {this.state.shareList.map(item => {
                            return (
                                <View >
                                    <TouchableOpacity onPress={() => {
                                        this.setState({
                                            share_with: item.id,
                                            value: item.key,
                                            friendName: item.name
                                        });
                                        this.setState({ modalVisible: false })
                                    }} style={{ margin: '1%', flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white }}>
                                        <View style={{ marginLeft: Width(3), width: Width(20), justifyContent: 'center', flex: 4.5 }}>
                                            <Text style={{ fontSize: FontSize(16), color: colors.dargrey, fontFamily: fonts.Roboto_Regular }}>{item.name} {item.lname}</Text>
                                        </View>
                                        <View style={{ alignItems: 'flex-end', right: Width(3), flex: 2 }}>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setState({
                                                        share_with: item.id,
                                                        value: item.key,
                                                        friendName: item.name
                                                    });
                                                    this.setState({ modalVisible: false })
                                                }} style={{
                                                    height: 20,
                                                    width: 20,
                                                    borderRadius: 100,
                                                    borderWidth: 1,
                                                    borderColor: colors.dargrey,
                                                    alignItems: 'center',
                                                    justifyContent: 'center',
                                                }} >
                                                {value === item.key && <View>
                                                    <Image source={SelectRadio} style={{}} />
                                                </View>}
                                            </TouchableOpacity>
                                        </View>
                                    </TouchableOpacity>
                                    <View style={{ height: Height(0.5) }} />

                                </View>
                            );
                        })}

                    </View>

                </Modal>
            </View>
        )
    }

    renderShareWithModal()
    {
        const { value } = this.state;

        return (
            <View>
                <Modal isVisible={this.state.sharemodalVisible} style={{ margin: 0, alignItems:'center'}} onBackdropPress={() => this.setState({ sharemodalVisible: false })} onBackButtonPress={() => this.setState({ sharemodalVisible: false })}>
                    <View style = {{width:Width(90),height:350,backgroundColor:'#fff',borderRadius:10}}>
                        
                        <TouchableOpacity onPress = {() => {this.setState({sharemodalVisible:false})}} style={{position:'absolute',right:20,top:20}}>
                            <Image source={ic_close_black}/>
                        </TouchableOpacity>
                        <Text style = {{fontSize:FontSize(20),alignSelf:'center',marginTop:50,fontFamily:fonts.Raleway_Bold,color:'#333333'}}>Share with all friends</Text>
                        <Text style = {{fontSize:FontSize(16),alignSelf:'center',marginTop:30,fontFamily:fonts.Raleway_Regular,color:'#333333',width:Width(65),textAlign:'center'}}>Are you sure you want to share this post with all your friends?</Text>
                        <TouchableOpacity style={{ width: Width(100), alignSelf: 'center', marginTop:20, height:80, alignItems:'center'}} onPress={this.addPlan} >
                            {/* <ImageBackground source={btn_continue} style={{ width: Width(100), height: 80, alignItems: 'center', justifyContent: 'center' }} resizeMode = 'contain'>
                                <Text style={[{ marginBottom: 10, fontSize: 17, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Continue</Text>
                            </ImageBackground> */}
                            <Image source = {btn_continue} resizeMode = 'cover'></Image>
                        </TouchableOpacity>
                        <Text style = {{fontSize:FontSize(13),alignSelf:'center',marginTop:15, marginBottom:10,fontFamily:fonts.Roboto_Regular,color:'#007AFF',width:Width(65),textAlign:'center'}} onPress = { () => this.onPressedNoDontShare()}>No, don’t send to all my friends</Text>
                        <View style = {{backgroundColor:'#EEEEEE', height:57, width:Width(90), position:'absolute',bottom:0,borderBottomLeftRadius:10,borderBottomRightRadius:10}}>
                                <TouchableOpacity style = {{height:57,justifyContent:'center',alignItems:'center', flexDirection:'row', marginLeft:20, alignSelf:'flex-start'}} onPress = { () => this.onPressedDontShowAgain()} activeOpacity = {0.9}>
                                        {
                                            this.state.ischecked == true ?
                                            <Image source={ic_checked} style = {{width:25,height:25}}/>
                                            :
                                            <Image source={ic_unchecked} style = {{width:25,height:25}} />
                                        }
                                        <Text style = {{fontSize:FontSize(13),marginLeft:10,fontFamily:fonts.Roboto_Regular,color:'#333333',textAlign:'left'}}>Don't show me again</Text>
                                </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        )

    }

    onPressedNoDontShare()
    {
        this.setState({sharemodalVisible:false})
        setTimeout(() => {this.setState({modalVisible: true})}, 400)
        if(this.state.ischecked == true)
        {
            AsyncStorage.setItem('sharewithdone','true')
        }
        else
        {
            AsyncStorage.setItem('sharewithdone','false')
        }
        //this.addPlan
        //this.setModalVisible(!this.state.modalVisible)
    }

    onPressedDontShowAgain()
    {
        if(this.state.ischecked == true)
        {
            this.setState({ischecked:false})
        }
        else
        {
            this.setState({ischecked:true})
        }
        
    }

    onPostAction = async () =>
    {
        if((this.state.share_with == 1) && (this.state.friends == null || this.state.friends == '') && (this.state.contacts == null || this.state.contacts == ''))
        {
            await AsyncStorage.getItem('sharewithdone').then((value) => {
                if(value == 'true')
                {
                    this.addPlan()
                }
                else
                {
                    this.setState({sharemodalVisible:true})
                }
            });
        }
        else
        {
            this.addPlan()
        }
    }

    addPlan = () => {
        
        if(this.state.ischecked == true)
        {
            AsyncStorage.setItem('sharewithdone','true')
        }
        else
        {
            AsyncStorage.setItem('sharewithdone','false')
        }

        this.setState({sharemodalVisible:false}, () => {
            RNProgressHUB.showSpinIndeterminate();

            let is_guest = 0
            if (this.state.sel_val == true) {
                is_guest = 1
            } else {
                is_guest = 0
            }
            var data = new FormData()
            data.append('token', this.state.token)
            data.append('login_id', this.state.login_id)
            data.append('plan_type', 2)
            data.append('description', this.state.description == null ? '' : this.state.description)
    
            if ((this.state.friends == null || this.state.friends == '') && (this.state.contacts == null || this.state.contacts == '')) {
                data.append('friend_id', '')
                data.append('share_with', this.state.share_with)
            } else {
                data.append('share_with', 3)
                if(this.state.contacts != null && this.state.contacts.length > 0)
                {
                    data.append('contact',this.state.contacts.toString())
                }
                
                if(this.state.friends != null && this.state.friends.length > 0)
                {
                    data.append('friend_id', this.state.friends.toString())
                }
                else
                {
                    data.append('friend_id', '')
                }
                
            }
    
            if (this.state.is_now == '1') {
                data.append('is_now', 1)
            } else {
                data.append('is_now', 0)
            }
            data.append('is_guest', is_guest)
            data.append('activity', this.state.activity)
            data.append('add_line_1', this.state.location)
            data.append('add_line_2', this.state.full_address)
            data.append('pin_code', '')
            data.append('city', '')
            data.append('state', '')
            data.append('country', '')
            data.append('start_date', this.state.start_date != null ? moment.utc(new Date(this.state.start_date)).format('LLL') : '')
            data.append('end_date', this.state.end_date != null ? moment.utc(new Date(this.state.end_date)).format('LLL') : '')
            data.append('is_publish', 1)
            data.append('is_allday', this.state.is_allday)
            if(this.state.photo_path == '' || this.state.photo_path == null || this.state.photo_path == undefined)
            {
                data.append("plan_image",'');
            }
            else
            {
                data.append("plan_image", {
                    name: 'event.jpg',
                    type: 'image/jpg',
                    uri:
                      Platform.OS === "android" ? this.state.photo_path : this.state.photo_path.replace("file://", "")
                  });
            }
    
             //alert(JSON.stringify(data))
             //return
             console.log('Final Dat===>', data)
             const MixpanelData = {
                'Friends Invited' : this.state.friends != null ? this.state.friends.length > 0 : '0',
                '$email' : store.getState().auth.user.email_address,
                'Activity': this.state.activity,
                // ActivitySelected: this.state.activity,
                'Location': this.state.full_address,
                'Plan Type': 'What',
                'Description': this.state.description == null ? '' : this.state.description,
                'Guest Can Invite' : is_guest == 1 ? 'True' : 'False',
                'Cover Photo' : this.state.photo_path == '' || this.state.photo_path == null || this.state.photo_path == undefined ? 'False' : 'True',
                'Date & Time' : this.state.current_time,
                // 'Friends Invited': this.state.friends.length
                // 'Plan Name' : `I wanna see ${this.props.navigation.state.params.friendName}`
                // GuestInvite: is_guest
            }
            if((this.state.friends == null || this.state.friends == '') && (this.state.contacts == null || this.state.contacts == '')) {
                MixpanelData.Privacy = this.state.share_with == 0 ? 'Only Me' : this.state.share_with == 2 ? 'Friends of Freinds'  : this.state.share_with ==  1 ? 'Friends' : ''
            }else {
                MixpanelData.Privacy = ''
            }
            // console.log('MixPanel Data...', MixpanelData)
            fetch(API_ROOT + 'plan/create', {
                method: 'post',
                body: data
            })
                .then((response) => response.json())
                .then((responseData) => {
    
                    RNProgressHUB.dismiss()
    
                    if (responseData.success) {
                        this.clearData()
                        this.props.navigation.navigate('dashboard', { refreshing: true })
                        this.mixpanel.identify(store.getState().auth.user.email_address);
                        this.mixpanel.getPeople().increment("Lifetime Plan Post", 1);
                        this.mixpanel.getPeople().set("Last Plan Post", moment().format('MMMM Do YYYY, h:mm:ss a'));
                        this.mixpanel.track('Plan Post', MixpanelData);
                        this.mixpanel.getPeople().setOnce({ 'First Plan Post': moment().format('MMMM Do YYYY, h:mm:ss a') });
                        // this.mixpanel.registerSuperPropertiesOnce({ 'First Plan Post': moment().format('MMMM Do YYYY, h:mm:ss a') });
    
                    } else {
                        alert(responseData.text)
    
                    }
                })
                .catch((error) => {
                    RNProgressHUB.dismiss()
                   // alert('Oops your connection seems off, Check your connection and try again')
                })
        })

        // return


            // setTimeout( () => {
            //     this.props.navigation.navigate('dashboard', { refreshing: true })
            //     },3000);

    }

    editPlan = () => {

        RNProgressHUB.showSpinIndeterminate()

        let is_guest = 0
        if (this.state.sel_val == true) {
            is_guest = 1
        } else {
            is_guest = 0
        }
        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('plan_type', 2)
        data.append('description', this.state.description)
        data.append('plan_id', this.state.plan_id)

        if ((this.state.friends == null || this.state.friends == '') && (this.state.contacts == null || this.state.contacts == '')) {
            data.append('friend_id', '')
            data.append('share_with', this.state.share_with)
        } else {
            data.append('share_with', 3)
            if(this.state.contacts != null && this.state.contacts.length > 0)
            {
                data.append('contact',this.state.contacts.toString())
            }
            if(this.state.friends != null && this.state.friends.length > 0)
            {
                data.append('friend_id', this.state.friends.toString())
            }
            else
            {
                data.append('friend_id', '')
            }
        }


        // if (this.state.friends == null || this.state.friends == '') {
        //     data.append('friend_id', '')
        //     data.append('share_with', this.state.share_with)
        // } else {
        //     data.append('share_with', 3)
        //     data.append('friend_id', this.state.friends.toString())

        // }

        // if(this.state.contacts != null && this.state.contacts.length > 0)
        // {
        //     data.append('contact',this.state.contacts.toString())
        // }

        if (this.state.is_now == '1') {
            data.append('is_now', 1)
        } else {
            data.append('is_now', 0)
        }
        data.append('is_guest', is_guest)
        data.append('activity', this.state.activity)
        data.append('add_line_1', this.state.location)
        data.append('add_line_2', this.state.full_address)
        data.append('pin_code', '')
        data.append('city', '')
        data.append('state', '')
        data.append('country', '')
        data.append('start_date', this.state.start_date != null ? moment.utc(new Date(this.state.start_date)).format('LLL') : '')
        data.append('end_date', this.state.end_date != null ? moment.utc(new Date(this.state.end_date)).format('LLL') : '')
        data.append('is_publish', 1)
        data.append('is_allday', this.state.is_allday)
        if(this.state.photo_path == '' || this.state.photo_path == null || this.state.photo_path == undefined)
        {
            data.append("plan_image",'');
        }
        else
        {
            data.append("plan_image", {
                name: 'event.jpg',
                type: 'image/jpg',
                uri:
                  Platform.OS === "android" ? this.state.photo_path : this.state.photo_path.replace("file://", "")
              });
        }
        //console.log('Edit Request: '+JSON.stringify(data))

        //alert(JSON.stringify(data))
        //return

        fetch(API_ROOT + 'plan/edit', {
            method: 'post',
            body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                RNProgressHUB.dismiss()
                if (responseData.success) {
                    const MixpanelData = {
                        '$email' : store.getState().auth.user.email_address,
                        'Post ID' : this.state.plan_id
                        }
                        this.mixpanel.identify(store.getState().auth.user.email_address);
                        this.mixpanel.track('Post Edit', MixpanelData);

                    this.clearData()
                    this.props.navigation.goBack()



                } else {
                    alert(responseData.text)
                }
            })
            .catch((error) => {
                RNProgressHUB.dismiss()
               // alert('Oops your connection seems off, Check your connection and try again')
            })

            // setTimeout( () => {
            //     this.props.navigation.navigate('dashboard', { refreshing: true })
            //     },3000);

    }

    goBack = () => {

        if(this.state.is_edit_plan == true)
        {
            this.props.navigation.navigate('what1',{is_edit_plan:true})
        }
        else if(this.state.is_from_feed == true)
        {
            this.props.navigation.navigate('what1',{is_edit_plan:false,selectActivity:this.state.activity})
        }
        else
        {
            this.clearData()
            this.props.navigation.goBack()
        }
    }
    clearData = () => {
        AsyncStorage.removeItem('What_Activity')
        AsyncStorage.removeItem('what_Location')
        AsyncStorage.removeItem('what_CurrentDate')
        AsyncStorage.removeItem('what_friends_name')
        AsyncStorage.removeItem('what_friends')
        AsyncStorage.removeItem('what_start')
        AsyncStorage.removeItem('what_end')
        AsyncStorage.removeItem('what_desc')
        AsyncStorage.removeItem('what_contacts')

    }

    onPressCoverPhoto()
    {
        ImagePicker.openPicker({
            width:Width(100),
            height:265,
            cropping: true
          }).then(image => {
            //console.log(image);
            //alert(image.path)
            this.setState({photo_path:image.path})
          });
    }

    render() {
        var str = String(this.state.activity);
        if (str.includes('Wanna')) {
            activity = str.substr(6);
            if (activity.slice(-1) == '?') {
                activity = activity.slice(0, -1);
            }
        } else {
            activity = str
        }
        return (
            <HandleBack onBack={this.clearData}>

                <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#EFEFF4' }} >
                    <Toastt ref="toast"></Toastt>
                    <BackgroundImage >
                        <SafeAreaView />
                        <Content>
                            {this.renderModal()}
                            {this.renderShareWithModal()}
                            {/* <ImageBackground source={bg} style={{ height: Height(12), justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{ height: Height(12), flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: 18, fontFamily: fonts.Roboto_Medium, color: colors.dargrey }}>{activity}</Text>
                                <View style={{ height: Height(1) }} />
                                <Text onPress={this.goBack} style={{ fontSize: 13, fontFamily: "Raleway-Medium", color: colors.blue }}>Select different activity</Text>
                            </View>
                            </ImageBackground> */}
                            <ImageBackground source={this.state.photo_path == '' || this.state.photo_path == null || this.state.photo_path == undefined ? default_placeholder : {uri:this.state.photo_path}} style={{ height: 265}}>
                            <View style={{ height: 265, flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent' }}>
                                {
                                    this.state.photo_path =='' || this.state.photo_path == null ?
                                    <TouchableOpacity onPress = { () => this.onPressCoverPhoto()}>
                                        <Image source={ic_add_cover} />
                                    </TouchableOpacity>
                                    : null
                                }
                            </View>
                            {
                                this.state.photo_path != '' && this.state.photo_path != null ?

                                <TouchableOpacity onPress = { () => this.onPressCoverPhoto()} style = {{position:'absolute',bottom:10,left:10}}>
                                    <Image source={ic_edit_cover} />
                                </TouchableOpacity>

                                : null
                            }
                            </ImageBackground>
                            <View style = {{backgroundColor:'#fff', width:'100%'}}>
                                <View style = {{flexDirection:'row', marginHorizontal:'4%', marginTop:15,marginBottom:15, alignItems:'center'}}>
                                    <Text style={{ fontSize: 19, fontFamily: fonts.Raleway_Bold, color: colors.dargrey, marginRight:10}} numberOfLines = {2}>{this.state.activity}</Text>
                                    <TouchableOpacity onPress = {this.goBack}>
                                        <Image source={ic_edit_event} style = {{marginRight:16}}/>
                                    </TouchableOpacity>
                                </View>
                                {(this.state.friends == null || this.state.friends == '') && (this.state.contacts == null || this.state.contacts == '')?
                                
                                    <TouchableOpacity onPress={() => {
                                        this.setModalVisible(!this.state.modalVisible);
                                    }} style={{ backgroundColor: colors.white, borderColor: '#C1C1C1', height: Height(3),marginLeft:'4%',justifyContent: 'center',borderWidth:1, borderRadius:5,marginBottom:16,width:this.state.friendName == 'Friends of friends' ? (Platform.OS == 'android' ? Width(46) : Width(43)): (Platform.OS == 'android' ? Width(33) : Width(30))}}>

                                        <Text  style={{ left: Width(3.2),position: 'absolute', fontSize: FontSize(11), fontFamily: fonts.Roboto_Regular, color: colors.dargrey }}>Share with  <Text style={{ color: colors.blue,fontSize: FontSize(11), fontFamily: fonts.Roboto_Regular,marginRight:Width(3.2) }}>{this.state.friendName}</Text>
                                        </Text>

                                    </TouchableOpacity>
                            : null}
                            </View>
                            <View style={{ height: Height(5), width: '100%', backgroundColor: colors.lightPink, justifyContent: 'center' }}>
                                <Text style={{ fontSize: 18, fontFamily: "Raleway-Medium", color: colors.appColor, marginLeft: '4%' }}>Optional</Text>
                            </View>
                            <View style={{}}>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate('EventDescription',{plan_type:2,description:this.state.description,is_from_eventdetails:false}) }} style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white, height:Height(6.6)}}>
                                    <View style={{ marginLeft: '4%', marginTop: '3%', marginBottom: '3%', justifyContent: 'center', flex: 6 }}>
                                        {
                                            this.state.description == '' || this.state.description == null ?
                                            <Text style={{ width: Width(90), fontSize: FontSize(18), color: '#8F8F90', fontFamily: "Raleway-Regular" }}>Description</Text>
                                            :
                                            <Text style={{ width: Width(85), fontSize: FontSize(18), color: '#000', fontFamily: "Raleway-Regular" }} numberOfLines = {1}>{this.state.description}</Text>
                                        }
                                    </View>
                                    <View style={{ alignItems: 'flex-end', right: Width(3), flex: 4 }}>
                                        <TouchableHighlight underlayColor="transparent" style={{}}>
                                            <AntDesign size={Width(5)} name="right" color={colors.dargrey} />
                                        </TouchableHighlight>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{ height: Height(1), marginTop: 1 }} />
                            <View style={{}}>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate('what2', { friend_id: this.state.friends,activity:this.state.activity, plan_id:this.state.plan_id, previous_contacts:this.state.contacts,is_edit_plan:this.state.is_edit_plan}) }} style={{ height: Height(6.6), flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white }}>
                                    <View style={{ width: Width(8), alignItems: 'center', flex: 1.6, marginLeft: Width(1) }}>
                                        <Image source={who} />
                                    </View>
                                    <View style={{ justifyContent: 'center', flex: 6 }}>
                                        <Text style={{ width: Width(60), fontSize: FontSize(18), color: colors.dargrey, fontFamily: "Raleway-Medium" }}>{this.state.frinds_name == null || this.state.frinds_name == '' ? 'Invite friends' : this.state.frinds_name} </Text>
                                    </View>
                                    <View style={{ alignItems: 'flex-end', right: Width(3), flex: 4 }}>
                                        <TouchableHighlight underlayColor="transparent" style={{}}>
                                            <AntDesign size={Width(5)} name="right" color={colors.dargrey} />
                                        </TouchableHighlight>
                                    </View>
                                </TouchableOpacity>
                            </View>


                            {/* <View style={{ height: Height(0.2) }} /> */}

                            {this.state.location == null ?
                                <View style={{}}>
                                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('what3') }}
                                        style={{ height: Height(6.6), flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white }}>
                                        <View style={{ alignItems: 'center', flex: 1.6, marginLeft: Width(1) }}>
                                            <Image source={Location_new} />
                                        </View>
                                        <View style={{ justifyContent: 'center', flex: 6 }}>
                                            <Text style={{ width: Width(60), fontSize: FontSize(18), color: colors.dargrey, fontFamily: "Raleway-Medium" }}>Add a location</Text>
                                        </View>
                                        <View style={{ alignItems: 'flex-end', right: Width(3), flex: 4 }}>
                                            <TouchableHighlight underlayColor="transparent" style={{}}>
                                                <AntDesign size={Width(5)} name="right" color={colors.dargrey} />
                                            </TouchableHighlight>
                                        </View>
                                    </TouchableOpacity>
                                </View> :
                                <View style={{}}>
                                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('what3') }} style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white, height:Height(6.6)}}>
                                        <View style={{ width: Width(9), alignItems: 'center', flex: 1.6, marginLeft: Width(1) }}>
                                            <Image source={Location_new} />
                                        </View>
                                        <View style={{ marginTop: '3%', marginBottom: '3%', justifyContent: 'center', flex: 6 }}>
                                            <Text style={{ width: Width(60), fontSize: FontSize(18), color: colors.dargrey, fontFamily: "Raleway-Medium" }}>{this.state.location}</Text>
                                        </View>
                                        <View style={{ alignItems: 'flex-end', right: Width(3), flex: 4 }}>
                                            <TouchableHighlight underlayColor="transparent" style={{}}>
                                                <AntDesign size={Width(5)} name="right" color={colors.dargrey} />
                                            </TouchableHighlight>
                                        </View>
                                    </TouchableOpacity>
                                </View>}
                            {/* <View style={{ height: Height(0.2) }} /> */}
                            {this.state.current_time == null ?
                                <View style={{}}>
                                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('what5',{item:this.state.item,is_edit_plan:this.state.is_edit_plan}) }}
                                        style={{ height: Height(6.6), flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white }}>
                                        <View style={{ alignItems: 'center', flex: 1.6, marginLeft: Width(1) }}>
                                            <Image source={time} />
                                        </View>
                                        <View style={{ justifyContent: 'center', flex: 6 }}>
                                            <Text style={{ width: Width(60), fontSize: FontSize(18), color: colors.dargrey, fontFamily: "Raleway-Medium" }}>Add a date & time</Text>
                                        </View>
                                        <View style={{ alignItems: 'flex-end', right: Width(3), flex: 4 }}>
                                            <TouchableHighlight underlayColor="transparent" style={{}}>
                                                <AntDesign size={Width(5)} name="right" color={colors.dargrey} />
                                            </TouchableHighlight>
                                        </View>
                                    </TouchableOpacity>
                                </View> :
                                <View style={{}}>
                                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('what5',{item:this.state.item,is_edit_plan:this.state.is_edit_plan}) }} style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white}}>
                                        <View style={{ width: Width(9), alignItems: 'center', flex: 1.6, marginLeft: Width(1) }}>
                                            <Image source={time} />
                                        </View>
                                        <View style={{ marginTop: '3%', marginBottom: '3%', justifyContent: 'center', flex: 6 }}>
                                            <Text style={{ width: Width(60), fontSize: FontSize(18), color: colors.dargrey, fontFamily: "Raleway-Medium" }}>{this.state.current_time}</Text>
                                        </View>
                                        <View style={{ alignItems: 'flex-end', right: Width(3), flex: 4 }}>
                                            <TouchableHighlight underlayColor="transparent" style={{}}>
                                                <AntDesign size={Width(5)} name="right" color={colors.dargrey} />
                                            </TouchableHighlight>
                                        </View>
                                    </TouchableOpacity>
                                </View>}
                            <View style={{ height: Height(1), marginTop: 1 }} />
                            {/* <TextInput
                                style={{ width: '100%', backgroundColor: colors.white, height: Height(9), paddingVertical: 0, marginTop: 1, marginBottom: 1, paddingHorizontal: Width(3.2), fontSize: 16 }}
                                placeholder='More info'
                                placeholderTextColor='#8F8F90'
                                maxLength={120}
                                multiline={true}
                                numberOfLines={2}
                                keyboardType='ascii-capable'
                                onChangeText={(description) => { this.setState({ description: description }) }}
                                value={this.state.description} />
                            <View style={{ height: Height(1) }} /> */}

                            <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row', backgroundColor: colors.white, borderColor: colors.white, height: Height(6.5), justifyContent: 'center', width: '100%' }}>
                                <Text style={{ left: Width(3.2), position: 'absolute', fontSize: 18, fontFamily: "Raleway-Medium", color: colors.dargrey }}>Guests can invite friends</Text>
                                {/* <TouchableOpacity onPress={() =>
                                    this.setState({
                                        sel_val: !this.state.sel_val
                                    })} style={{ right: Width(4), position: 'absolute', alignItems: 'center', justifyContent: 'center' }}>
                                    {this.state.sel_val ?
                                        <Image source={SelectRadio} style={{}} />
                                        : <Image source={unSelectRadio} style={{}} />}
                                </TouchableOpacity> */}
                                {
                                    Platform.OS == 'ios' ?
                                    <Switch ios_backgroundColor={'#E4E4E5'} trackColor={{ true: '#4bd964' }}
                                    value={this.state.sel_val}
                                    style = {{position:'absolute', right:Width(3)}}
                                    onValueChange={(sel_val) => this.setState({ sel_val })} />
                                    :
                                    <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964',false: '#ccc' }}
                                            value={this.state.sel_val}
                                            style = {{position:'absolute', right:Width(3)}}
                                            onValueChange={(sel_val) => this.setState({ sel_val })} />
                                }
                                
                            </View>

                            <View style={{ height: Height(5) }}></View>


                        </Content>
                        <TouchableOpacity style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }} onPress={this.state.is_edit_plan ? this.editPlan : this.onPostAction} >
                            <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                                {
                                    this.state.is_edit_plan ? <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Save</Text>
                                    :
                                    <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Post</Text>

                                }
                            </ImageBackground>
                        </TouchableOpacity>
                        <SafeAreaView />
                    </BackgroundImage>
                </Container>
            </HandleBack>
        );
    }
}

export default Frinds;