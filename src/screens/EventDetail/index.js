// added by utsav
import React, { Component } from "react";
import { SafeAreaView, Alert, ImageBackground, Image, ScrollView, FlatList, Dimensions, TextInput, TouchableOpacity, StatusBar, Text, ToastAndroid, Share, InputAccessoryView, Platform, Keyboard, Animated, AppState, Clipboard, PermissionsAndroid } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Button, Header, Left, Item, Icon, Right, Content, Input, Form } from "native-base";
import styles from "./style";
import { getMyFriends } from "../../redux/actions/auth";
import ValidationComponent from 'react-native-form-validator';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts, API_ROOT } from "../../config/constant";
import { TouchableHighlight, } from "react-native-gesture-handler";
//import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import RNCalendarEvents from 'react-native-calendar-events';
import LaunchNavigator from 'react-native-launch-navigator';
import Toast from 'react-native-simple-toast';
import RNProgressHUB from 'react-native-progresshub';

const blue_left = require("../../../assets/images/Dashboard/blue-left.png");
//const white_left = require("../../../assets/images/Dashboard/white-left.png");
const white_left = require("../../../assets/images/backWhite.png");
const chai_cup = require("../../../assets/images/Dashboard/chai_cup.png");
const location = require("../../../assets/images/Dashboard/location_home.png");
const calendar = require("../../../assets/images/Dashboard/calendar.png");
const editdatetime = require("../../../assets/images/Dashboard/ic_edit_datetime.png");
const copyaddress = require("../../../assets/images/Dashboard/ic_copy_address.png");
const iccalendar = require("../../../assets/images/Dashboard/ic_calendar.png");
const openmap = require("../../../assets/images/Dashboard/ic_open_map.png");
const chatphoto = require("../../../assets/images/Dashboard/ic_photo_chat.png");
const btnsend = require("../../../assets/images/Dashboard/btn_send.png");


import MixpanelManager from '../../../Analytics'
import Modal from "react-native-modal";
import { FRIENDLIST } from "../../redux/actions/types";
import { store } from "../../redux/store";
import moment from 'moment';

const Userpic = require("../../../assets/images/Dashboard/Userpic.png");
const Userphoto2 = require("../../../assets/images/Dashboard/Userphoto2.png");
const avtar = require("../../../assets/images/Dashboard/avtar.png");
const Delete = require("../../../assets/images/Dashboard/Delete.png");
const Edit = require("../../../assets/images/Dashboard/Edit.png");
const share = require("../../../assets/images/Dashboard/share.png");
import AntDesign from 'react-native-vector-icons/AntDesign';
import ImagePicker from 'react-native-image-picker';
import Ionicons from 'react-native-vector-icons/Ionicons';
import CurrentDateTime from "../../Functions/CurrentDateTime";
import firebase from 'firebase';
import ActionSheet from 'react-native-actionsheet'
import UploadImage from '../../Functions/UploadImage';
const going1 = require("../../../assets/images/Dashboard/going1.png");
const circle_favorite = require("../../../assets/images/Dashboard/circle_favorite.png");
//import { API_ROOT } from "../../config/constant";
const black_heart = require("../../../assets/images/Dashboard/black_heart.png");
const going = require("../../../assets/images/Dashboard/Tick_uncolor.png");
//const chatbg = require("../../../assets/images/Bg.png");
const chatbg = require("../../../assets/images/bg_new.png");
const sendarrow = require("../../../assets/images/ic_send_arrow.png");
const default_placeholder = require("../../../assets/images/Dashboard/default_placeholder.png");

const screenHeight = Dimensions.get('window').height;

var MassageShow = false;
var OptionShow = []
var PhotoOption = []
var _this
var TimeInterval
var isFirstTime = true
class EventDetail extends Component {

	constructor(props) {
		super(props);
		_this = this
		this.arrayholder = [];
		this.prevmsg = '',
			this.curdaydiff = '',
			//this.previousdaydiff = '',
			this.curmsgtime = '',
			//this.prevmsgtime = '',
			//this.previmsgloginid = '',
			this.curmsgloginid = '',
			this.firsttime = true,
			this.imagepickeropen = false,
			this.weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Today', 'Yesterday']
		this.state = {
			Msg: '',
			deleteModal: false,
			locationModal: false,
			dateTimeModal: false,
			token: store.getState().auth.user.token,
			login_id: store.getState().auth.user.login_id,
			Name: store.getState().auth.user.name,
			title_header: this.props.navigation.state.params.title_header,
			eventby: this.props.navigation.state.params.eventby,
			item: this.props.navigation.state.params.item != null ? this.props.navigation.state.params.item : null,
			Final_date: this.props.navigation.state.params.Final_date,
			Final_Enddate: '',
			Final_StartTime: '',
			Final_EndTime: '',
			event_ends_sameday: false,
			notificaitonid: this.props.navigation.state.params.notificaitonid,
			// myImage: store.getState().auth.photo,
			isPhotoPreviewVisible:false,
			previewurl:'',
			data: [
				{
					images: avtar
				},
				{
					images: Userpic
				}, {
					images: Userphoto2
				}, {
					images: Userpic
				},
				{
					images: Userphoto2
				},
				{
					images: Userpic,
				},
			],
			chat: [
				{
					id: 1,
					message:
						"Power is a curious thing, my lord. Are you fond of riddles?",
					sentBy: "janice",
					date: "Yesterday, 2:30 pm"
				},
				{
					id: 2,
					message:
						"Why? Am I about to hear one?",
					sentBy: "gunther",
					date: "Yesterday, 2:30 pm"
				},
				{
					id: 3,
					message: "Did you guys really memorize that conversation?? Nothing better to do?",
					sentBy: "janice",
					date: "Yesterday, 2:30 pm"
				},
				{
					id: 4,
					message: "They should have casted us!",
					sentBy: "gunther",
					date: "Yesterday, 2:30 pm"
				},

			],
			Massages: [],
			Icon: white_left,
			plan_id: null,
			MassageShow: false,
			participants: ['', '', ''],
			today: null,
			Edit: false,
			backgroundColor: 'transparent',
			safebackgroundColor: '#fff',
			color: '#fff',
			headerfontsize: 20,
			is_going: 0,
			is_interested: 0,
			is_textfield_focused: false,
			animationValue: Platform.OS == 'ios' ? new Animated.Value(Height(16)) : new Animated.Value(Height(17)),
			viewState: true,
			viewopacity: 1,
			inputheight: Height(4),
			status: [],
			timer: null,
			invitedList: [],
			counter: 0,
			overlay_shown:true
		};
		this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
		isFirstTime = true;
		this.send = this.send.bind(this);
		this.showActionSheet = this.showActionSheet.bind(this);
		this.CemeraRoll = this.CemeraRoll.bind(this);
		this.UploadEvent = this.UploadEvent.bind(this);
		this.scrollView = null;
		this.deletePlan = this.deletePlan.bind(this);
		this.messagefield = null;
		this.flat_list = null;
		this.onInputFocus = this.onInputFocus.bind(this);
		this.onInputBlur = this.onInputBlur.bind(this);
		this._didBlurSubscription = props.navigation.addListener('didBlur', payload => {
			global.planID = ''
			//clearInterval(this.state.timer);
			//this.keyboardDismissed()
			global.isEventDetails = false
		});
		this.focusListener = this.props.navigation.addListener('didFocus', () => {
			this.firsttime = true
			this.imagepickeropen = false

			this.didFocusComp()
			global.isEventDetails = true
			//this.setUpTimer()
		});
		this._keyboardDidHide = this._keyboardDidHide.bind(this);
		this._keyboardDidShow = this._keyboardDidShow.bind(this);
		this.headertap = false
		this.PhotoActionSheet = null
	}

async didFocusComp() {


	await AsyncStorage.getItem('overlay_shown').then((value) => {
		console.log('OVERLAY: '+value)
		this.setState({ overlay_shown: value == 'true' ? true : false })
	});

		this.getPlanDetails()

		if (Platform.OS == 'ios') {
			RNCalendarEvents.authorizeEventStore()
		} else {
			// this library crashes on android when allowing calender permission
			// RNCalendarEvents.authorizeEventStore()
			this.requestCalendarPermission()
		}

	}

	checkForCalendarPermissionAndroid() {
		try {
			PermissionsAndroid.check(
				PermissionsAndroid.PERMISSIONS.WRITE_CALENDAR,
				PermissionsAndroid.PERMISSIONS.READ_CALENDAR
			).then(granted => {
				if (!granted) {
					this.requestCalendarPermission();
				}
			}).catch(error => {
				console.log("error while checking calendar permission!")
			})

		} catch (error) {
			console.log("error while checking calendar permission!")
		}
	}

	requestCalendarPermission() {
		PermissionsAndroid.requestMultiple(
			[
				PermissionsAndroid.PERMISSIONS.READ_CALENDAR,
				PermissionsAndroid.PERMISSIONS.WRITE_CALENDAR
			]
		).then((result) => {
			if (result['android.permission.READ_CALENDAR'] && result['android.permission.WRITE_CALENDAR'] === 'granted') {
				console.log("calendar permission granted...")
			} else {
				Toast.show("Please allow Calender permissions...")
			}
		})
	}
	toggleAnimation = () => {

		//console.log('toggleAnimation')

		if (this.state.viewState == false) {
			this.headertap = true
			Animated.timing(this.state.animationValue, {
				toValue: (this.state.participants.length > 0 || this.state.item.is_guest == 1) ? (Platform.OS == 'ios' ? Height(16) : Height(17)) : (Platform.OS == 'ios' ? Height(12) : Height(15)),
				timing: 100
			}).start(() => {
				//Keyboard.dismiss()
				this.setState({ viewState: true, is_textfield_focused: false, backgroundColor: 'transparent', Icon: white_left, safebackgroundColor: '#fff', color: '#fff', headerfontsize: 20 })
				//this.setState({viewopacity:1,viewState :true,is_textfield_focused:false})
			});
			setTimeout(() => { this.setState({ viewopacity: 1 }) }, 300)

			Keyboard.dismiss()
		}
	}

	collapseHeader = () => {
		this.setState({ viewopacity: 0 }, () => {
			Animated.timing(this.state.animationValue, {
				toValue: Height(0),
				timing: 100
			}).start(
				this.setState({ viewState: false, backgroundColor: '#41199B', Icon: white_left, safebackgroundColor: '#41199B', color: '#fff', headerfontsize: 13 })
				//this.setState({viewState : false})
			);
		})

	}

	onOverlayTapped()
	{
		//alert('Hiiii')
		this.setState({overlay_shown:true}, () => {
			AsyncStorage.setItem('overlay_shown','true')
		})
		
	}

	deletePlan() {

		let plan_id = this.props.navigation.getParam('plan_id', null);

		var data = new FormData()
		data.append('token', this.state.token)
		data.append('plan_id', plan_id)
		data.append('login_id', this.state.login_id)

		fetch(API_ROOT + 'delete/plan', {
			method: 'post', body: data
		})
			.then((response) => response.json())
			.then((responseData) => {
				// alert(JSON.stringify(this.state.plan_id));
				if (responseData.success) {
					const MixpanleData = {
						'$email' : store.getState().auth.user.email_address,
						'Post ID' : plan_id
					  }
					  this.mixpanel.identify(store.getState().auth.user.email_address);
					  this.mixpanel.track('Post Cancel', MixpanleData);
					this.setState({ deleteModal: false })
					this.props.navigation.navigate('dashboard')
				} else {
					alert(responseData.text)
				}
			})
			.catch((error) => {
				 //alert('Oops your connection seems off, Check your connection and try again')
				 })
	}

	showActionSheet = () => {

		this.messagefield.blur()

		if (this.state.item.line_1 == '' || this.state.item.activity == '' || this.state.item.start_date == '') {
			this.imagepickeropen = false
			setTimeout(() => { this.ActionSheet.show() }, 300)
		}
	}
	showPhotoActionSheet = () => {

		this.messagefield.blur()

		if (this.state.item.line_1 == '' || this.state.item.activity == '' || this.state.item.start_date == '') {
			this.imagepickeropen = false
			setTimeout(() => { this.PhotoActionSheet.show() }, 300)
		}
	}
	async goToShare(item) {
		// Share.open({ url: 'Hey,join Planmesh app to share your pics',message:});
		const MixpanelData = {
			'$email': store.getState().auth.user.email_address,
			'Post ID': item.plan_id,
		  }
		  this.mixpanel.identify(store.getState().auth.user.email_address);
		  this.mixpanel.track('Share', MixpanelData);

		var message = ''

		if (item.plan_type == 1) {
			var name = item.friend_name.split(" ")
			//message = 'Hey, '+"i'm inviting you to "+name[0]+", let's meet up!" + '\n'+ "Let me know on Planmesh if you're in"+ '\n\n' +'https://planmeshapp.com/plan_details/'+item.plan_id
			message = 'Hey, ' + "i'm inviting you to " + ", Let's meet up!" + '\n' + "Let me know on Planmesh if you're in" + '\n\n' + 'https://planmeshapp.com/plan_details/' + item.plan_id
		}
		else if (item.plan_type == 2) {
			message = 'Hey, ' + "i'm inviting you to " + item.activity + " Let me know on Planmesh if you're in" + '\n\n' + 'https://planmeshapp.com/plan_details/' + item.plan_id
		}
		else if (item.plan_type == 3) {
			message = 'Hey, ' + "i'm inviting you to " + "Wanna go to " + item.line_1 + "?" + " Let me know on Planmesh if you're in" + '\n\n' + 'https://planmeshapp.com/plan_details/' + item.plan_id
		}
		else if (item.plan_type == 4) {
			message = 'Hey, ' + "i'm inviting you to " + "I'm free, wanna hangout?" + " Let me know on Planmesh if you're in" + '\n\n' + 'https://planmeshapp.com/plan_details/' + item.plan_id
		}

		try {
			const result = await Share.share({
				message: message,
			});

			if (result.action === Share.sharedAction) {
				if (result.activityType) {
					// shared with activity type of result.activityType
				} else {
					// shared
				}
			} else if (result.action === Share.dismissedAction) {
				// dismissed
			}
		} catch (error) {
			alert(error.message);
		}

	}
	goToNoIntersted = (item) => {

		var tempitem = this.state.item
		tempitem.is_interested = 0
		this.setState({item:tempitem})

		this.setState({ is_interested: 0 })

		var data = new FormData()
		data.append('token', this.state.token)
		data.append('plan_id', item.plan_id)
		data.append('login_id', this.state.login_id)
		data.append('is_interested', 2)
		fetch(API_ROOT + 'plan/interested_response', {
			method: 'post', body: data
		})
			.then((response) => response.json())
			.then((responseData) => {
				if (responseData.success) {
					const MixpanelData = {
						'$email': store.getState().auth.user.email_address,
						'Post ID': item.plan_id,
						'Interested': 'False'
					  }
					  this.mixpanel.identify(store.getState().auth.user.email_address);
					  this.mixpanel.track('Interested', MixpanelData);
					//this.getPlanDetails()
				} else {
					this.setState({ is_interested: 1 })
					//alert(responseData.text)
				}
			})
			.catch((error) => {
				this.setState({ is_interested: 1 })
				//alert('Oops your connection seems off, Check your connection and try again')
			})
	}
	goToIntersted = (item) => {

		var tempitem = this.state.item
		tempitem.is_interested = 1
		tempitem.is_going = 0
		this.setState({item:tempitem})

		this.setState({ is_interested: 1 })

		if (this.state.is_going == 1) {
			this.setState({ is_going: 0 })
		}

		var data = new FormData()
		data.append('token', this.state.token)
		data.append('plan_id', item.plan_id)
		data.append('login_id', this.state.login_id)
		data.append('is_interested', 1)
		fetch(API_ROOT + 'plan/interested_response', {
			method: 'post', body: data
		})
			.then((response) => response.json())
			.then((responseData) => {
				if (responseData.success) {
					const MixpanelData = {
						'$email': store.getState().auth.user.email_address,
						'Post ID': item.plan_id,
						'Interested': 'True'
					  }
					  this.mixpanel.identify(store.getState().auth.user.email_address);
					  this.mixpanel.track('Interested', MixpanelData);
					//this.setState({is_interested: 1})
					//this.getPlanDetails()
				} else {
					this.setState({ is_interested: 0, is_going: 0 })
					//alert(responseData.text)
				}
			})
			.catch((error) => {
				this.setState({ is_interested: 0, is_going: 0 })
				//alert('Oops your connection seems off, Check your connection and try again')
			})
	}
	goToGoing = (item) => {

		var tempitem = this.state.item
		tempitem.is_interested = 0
		tempitem.is_going = 1
		this.setState({item:tempitem})

		this.setState({ is_going: 1 })

		if (this.state.is_interested == 1) {
			this.setState({ is_interested: 0 })
		}

		var data = new FormData()
		data.append('token', this.state.token)
		data.append('plan_id', item.plan_id)
		data.append('login_id', this.state.login_id)
		data.append('is_going', 1)

		//alert(JSON.stringify(data))
		//return
		fetch(API_ROOT + 'plan/going_response', {
			method: 'post', body: data
		})
			.then((response) => response.json())
			.then((responseData) => {
				if (responseData.success) {
					const MixpanelData = {
						'$email': store.getState().auth.user.email_address,
						'Post ID': item.plan_id,
						'Going': 'True'
					  }
					  this.mixpanel.identify(store.getState().auth.user.email_address);
					  this.mixpanel.track('Going', MixpanelData);
					// this.onRefresh()

				} else {
					this.setState({ is_going: 0, is_interested: 0 })
					//alert(responseData.text)
				}
			})
			.catch((error) => {
				this.setState({ is_going: 0, is_interested: 0 })
				//alert('Oops your connection seems off, Check your connection and try again')
			})
	}
	goToNoGoing = (item) => {

		var tempitem = this.state.item
		tempitem.is_going = 0
		this.setState({item:tempitem})

		this.setState({ is_going: 0 })

		var data = new FormData()
		data.append('token', this.state.token)
		data.append('plan_id', item.plan_id)
		data.append('login_id', this.state.login_id)
		data.append('is_going', 2)
		fetch(API_ROOT + 'plan/going_response', {
			method: 'post', body: data
		})
			.then((response) => response.json())
			.then((responseData) => {
				if (responseData.success) {
					const MixpanelData = {
						'$email': store.getState().auth.user.email_address,
						'Post ID': item.plan_id,
						'Going': 'False'
					  }
					  this.mixpanel.identify(store.getState().auth.user.email_address);
					  this.mixpanel.track('Going', MixpanelData);
				} else {
					this.setState({ is_going: 1 })
					//alert(responseData.text)
				}
			})
			.catch((error) => {
				this.setState({ is_going: 1 })
				//alert('Oops your connection seems off, Check your connection and try again')
			})
	}

	// getPlanDetails()
	// {
	//     var data = new FormData()
	//     data.append('token', this.state.token)
	//     data.append('plan_id', this.state.item.plan_id)
	//     fetch(API_ROOT + 'plan/details', {
	//       method: 'post', body: data
	//     })
	//       .then((response) => response.json())
	//       .then((responseData) => {
	//         if (responseData.success) {

	//             alert(JSON.stringify(responseData))

	//         } else {
	//           alert(responseData.text)
	//         }
	//       })
	//       .catch((error) => { 
	//         alert(error) })
	// }

	async UploadEvent(uri) {
		var filename = uri.substring(uri.lastIndexOf('/') + 1);
		let plan_id = this.props.navigation.getParam('plan_id', null);
		var sentBy = this.state.login_id;
		var Name = this.state.Name;
		var Avtar = store.getState().auth.user.photo;
		UploadImage(plan_id, sentBy, Name, Avtar, uri, filename)
			.then(async (response) => {
				ToastAndroid.showWithGravityAndOffset(
					'image Send Successfully!',
					ToastAndroid.LONG,
					ToastAndroid.BOTTOM,
					25,
					50,
				);
			})
	}

	send() {
		//if(this.state.MassageShow || this.state.is_interested == 1 ||  this.state.is_going == 1)

		//if(this.state.MassageShow)
		//        { 

		this.keyboardDismissed()

		let plan_id = this.props.navigation.getParam('plan_id', null);

		//console.log('Chat Plan ID: '+plan_id)

		var objcurdate = new Date()
		var date = moment.utc(objcurdate).format("YYYY-MM-DD HH:mm:ss")
		//console.log('Message Date: '+strdate)
		//var date = CurrentDateTime();
		var sentBy = this.state.login_id;
		var message = this.state.Msg;
		var Name = this.state.Name;
		var Avtar = store.getState().auth.user.photo;

		firebase.database().ref('Groups').child(plan_id + '/Massages').push({
			message, sentBy, Name, Avtar, date
		})
		this.sendMessageNotification()
		this.setState({ Msg: '' })

		//setTimeout(() => {this.messagefield.focus()}, 1000)

		//} 
	}

	async CemeraRoll(index) {

		this.imagepickeropen = true

		try {
			const options = {
				quality: 1.0,
				mediaType: 'images',
				storageOptions: {
					skipBackup: true,
				},
			};

			var action = OptionShow[index]

			if (action == 'Suggest an Activity') {
				this.imagepickeropen = false

				if (this.state.item.total_activity_suggestions == 0) {
					this.props.navigation.navigate('Activity', { plan_id: this.state.item.plan_id, other_id: this.state.item.login_id, is_from_event: true, is_direct: false, item: this.state.item })
				}
				else {
					this.props.navigation.navigate('SuggestActivity', { other_id: this.state.item.login_id, plan_id: this.state.item.plan_id, is_from_event: true, item: this.state.item })
				}
			}
			else if (action == 'Suggest a Location') {
				this.imagepickeropen = false

				if (this.state.item.total_location_suggestions == 0) {
					this.props.navigation.navigate('Location', { plan_id: this.state.item.plan_id, other_id: this.state.item.login_id, is_from_event: true, is_direct: false, item: this.state.item })
				}
				else {
					this.props.navigation.navigate('SuggestLocation', { other_id: this.state.item.login_id, plan_id: this.state.item.plan_id, is_from_event: true, item: this.state.item })
				}

			}
			else if (action == 'Suggest a Date & time') {
				this.imagepickeropen = false

				if (this.state.item.total_datetime_suggestions == 0) {
					this.props.navigation.navigate('DateTime', { plan_id: this.state.item.plan_id, other_id: this.state.item.login_id, is_from_event: true, is_direct: false, item: this.state.item })
				}
				else {
					this.props.navigation.navigate('SuggestDateTime', { other_id: this.state.item.login_id, plan_id: this.state.item.plan_id, is_from_event: true, item: this.state.item })
				}

			}
			else if (action == 'Cancel') {
				this.imagepickeropen = false
			}
			else {

				if (action == 'Camera') {
					ImagePicker.launchCamera(options, response => {
						if (response.didCancel) {
							this.imagepickeropen = false
							this.setState({ loadingindex: false })
							//alert('User cancelled photo picker');
						} else if (response.error) {
							this.imagepickeropen = false
							this.setState({ loadingindex: false })
							//alert('ImagePicker Error: ', response.error);
						} else if (response.customButton) {
							this.imagepickeropen = false
							this.setState({ loadingindex: false })
							//alert('User tapped custom button: ', response.customButton);
						} else {
							//this.imagepickeropen = false
							//this.UploadEvent(response.uri);
							this.setState({isPhotoPreviewVisible:true,previewurl:response.uri})
						}
					});
				}
				else if (action == 'Photo Gallery') {
					ImagePicker.launchImageLibrary(options, response => {
						if (response.didCancel) {
							this.imagepickeropen = false
							this.setState({ loadingindex: false })
							//                  alert('User cancelled photo picker');
						} else if (response.error) {
							this.imagepickeropen = false
							this.setState({ loadingindex: false })
							//alert('ImagePicker Error: ', response.error);
						} else if (response.customButton) {
							this.imagepickeropen = false
							this.setState({ loadingindex: false })
							//alert('User tapped custom button: ', response.customButton);
						} else {
							//this.imagepickeropen = false
							//this.UploadEvent(response.uri);
							this.setState({isPhotoPreviewVisible:true,previewurl:response.uri})
						}
					});
				}

			}

		}
		catch (error) {
			//alert(error);
		}
	}

	async PhotoSheetAction(index)
	{
		this.imagepickeropen = true

		try {
			const options = {
				quality: 1.0,
				mediaType: 'images',
				storageOptions: {
					skipBackup: true,
				},
			};

			var action = PhotoOption[index]

				if (action == 'Camera') {
					ImagePicker.launchCamera(options, response => {
						if (response.didCancel) {
							this.imagepickeropen = false
							this.setState({ loadingindex: false })
							//alert('User cancelled photo picker');
						} else if (response.error) {
							this.imagepickeropen = false
							this.setState({ loadingindex: false })
							//alert('ImagePicker Error: ', response.error);
						} else if (response.customButton) {
							this.imagepickeropen = false
							this.setState({ loadingindex: false })
							//alert('User tapped custom button: ', response.customButton);
						} else {
							//this.imagepickeropen = false
							this.setState({isPhotoPreviewVisible:true,previewurl:response.uri})
							//this.UploadEvent(response.uri);
						}
					});
				}
				else if (action == 'Photo Gallery') {
					ImagePicker.launchImageLibrary(options, response => {
						if (response.didCancel) {
							this.imagepickeropen = false
							this.setState({ loadingindex: false })
							//                  alert('User cancelled photo picker');
						} else if (response.error) {
							this.imagepickeropen = false
							this.setState({ loadingindex: false })
							//alert('ImagePicker Error: ', response.error);
						} else if (response.customButton) {
							this.imagepickeropen = false
							this.setState({ loadingindex: false })
							//alert('User tapped custom button: ', response.customButton);
						} else {
							//this.imagepickeropen = false
							this.setState({isPhotoPreviewVisible:true,previewurl:response.uri})
							//this.UploadEvent(response.uri);
						}
					});
				}

			}
		catch (error) {
			//alert(error);
		}

	}

	async componentDidMount() {

		//const myitems = this.props.navigation.getParam('item');
		//this.setState({ is_going: myitems.is_going , is_interested: myitems.is_interested  }) 

		//alert('PlanData: ' + JSON.stringify(this.state.item))

		AppState.addEventListener('change', this.handleAppStateChange);

		this.keyboardDidShowListener = Keyboard.addListener(
			'keyboardDidShow',
			this._keyboardDidShow,
		);
		this.keyboardDidHideListener = Keyboard.addListener(
			'keyboardDidHide',
			this._keyboardDidHide,
		);

		let plan_id = this.props.navigation.getParam('plan_id', null);
		let now = new Date();
		var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		var day = days[now.getDay() == 0 ? 6 : now.getDay() - 1];
		//   this.setState({today: day})

		//this.getPlanDetails()
		this.setUpChatInfo()
		this.setFinalEventDate()
		this.readNotification()


		// })   

	}

	//   componentWillUnMount() {
	//     this.ref.off();
	// } 

	componentWillUnmount() {
		//console.log('Removed....')
		AppState.removeEventListener('change', this.handleAppStateChange);
	}

	handleAppStateChange = (nextAppState) => {
		//console.log('the app state: '+nextAppState);
		if (nextAppState === 'inactive' || nextAppState === 'background') {
			this.keyboardDismissed()
		}
	}

	setUpTimer() {
		let timer = setInterval(() => this.checkInputState(), 1000 * 3);
		this.setState({ timer });
	}

	componentWillReceiveProps(props) {

		//console.log('componentWillReceiveProps')

		if (props.navigation.state.params != null) {
			//this.getPlanDetails()
			this.setFinalEventDate()
			this.readNotification()
		}

	}

	getPlanDetails() {
		
		let plan_id = this.props.navigation.getParam('plan_id', null);
		global.planID = plan_id

		//alert(plan_id)

		//this.setState({ plan_id }) 
		const form = new FormData();
		form.append('token', this.state.token);
		form.append('login_id', this.state.login_id);
		form.append('plan_id', plan_id);

		//alert(this.state.login_id)


		fetch(API_ROOT + 'plan/details', {
			method: 'POST',
			body: form
		})
			.then(Resp => Resp.json())
			.then(resp => {
				// 523 

				//alert('Plan Details: '+JSON.stringify(resp))

				//this.setState({is_interested:resp.data.is_intrested,is_going:resp.data.is_going,participants: resp.data.participants,item:resp.data,plan_id:resp.data.plan_id})
				this.setState({is_interested:resp.data.is_intrested,is_going: resp.data.is_going,item:resp.data,plan_id:resp.data.plan_id,invitedList:resp.data.is_invited_list, eventby:resp.data.name })
				
				this.setFinalEventDate()


        if (resp.data.login_id == this.state.login_id) {
					this.setState({ MassageShow: true, Edit: true, })
				}
				if (resp.data.participants.length != 0) {
					this.setState({ participants: resp.data.participants })
					resp.data.participants.map((item) => {

						if (this.state.login_id == item.accepted_id) {
							this.setState({ MassageShow: true })
							// this.setState({ MassageShow: true })
						}

					})
				}
				else {
					// if(resp.data.is_guest != 1)
					// {
					//     this.setState({animationValue: new Animated.Value(Platform.OS == 'ios' ? Height(9) : Height(12))})
					// }
				}

				//this.forceUpdate()
				// 371 
				
				this.firsttime = false
			})
	}

	sendMessageNotification() {
		let plan_id = this.props.navigation.getParam('plan_id', null);
		this.setState({ plan_id })

		const form = new FormData();
		form.append('token', this.state.token);
		form.append('plan_id', plan_id);
		form.append('login_id', this.state.login_id);
		fetch(API_ROOT + 'chat/my/plan/noti', {
			method: 'POST',
			body: form
		})
			.then(Resp => Resp.json())
			.then(resp => {

			})
	}

	readNotification() {
		if (this.state.notificaitonid != '') {
			
			var data = new FormData()
			data.append('token', this.state.token)
			data.append('notification_id', this.state.notificaitonid)
			RNProgressHUB.showSpinIndeterminate();

			fetch(API_ROOT + 'notification/read', {
				method: 'post',
				body: data
			}).then((response) => response.json())
				.then((responseData) => {
					//alert(responseData)
					if (responseData.success) {
						RNProgressHUB.dismiss()

					} else {
						RNProgressHUB.dismiss()

					}
				})
				.catch((error) => {
					//alert('Oops your connection seems off, Check your connection and try again')
					RNProgressHUB.dismiss()
				})
		}
	}

	checkInputState() {
		// if((this.prevmsg == this.state.Msg) && (this.prevmsg.length > 0))
		// {
		//     this.keyboardDismissed()
		// }
		// else
		// {
		//   if(this.state.is_textfield_focused)
		//   {
		//     this.keyboardPresented()
		//   }
		// }
	}

	setUpChatInfo() {
		let plan_id = this.props.navigation.getParam('plan_id', null);


		/////////////////////////////////////////////////////////////////////////////////

		this.refstatus = firebase.database().ref('Groups').child(plan_id)
		this.refstatus.on('value', statuss => {
			if (statuss.val() != null) {
				if ((statuss.val().status == undefined)) {
					this.setState({ status: [] })
				}
				else {
					var multityper = statuss.val().status;
					var index = multityper.findIndex(x => x.id === this.state.login_id);
					if (index != -1) {
						multityper.splice(index, 1);
					}
					this.setState({ status: multityper })
				}
			}
			else {
				this.setState({ status: [] })
			}
		})

		//////////////////////////////////////////////////////////////////////

		firebase.database().ref('Groups').child(plan_id + '/Massages').on('value', snapshot => {

			//console.log('SNAPSHOT '+snapshot.val()+' Plan: '+plan_id) 
			if (snapshot.val() != null) {

				var Massages = Object.values(snapshot.val())

				//console.log('MESSAGES: '+JSON.stringify(Massages))

				// var a = 0; 
				// let now = new Date();
				// var todaydate = now.getDate();

				/*      const monthNames = ["January", "February", "March", "April", "May", "June",
			   "July", "August", "September", "October", "November", "December"
			 ];
					 var months = monthNames[now.getMonth()];
					 var years =  now.getFullYear();
					 var a = 0; 
					 var b = 0;
					 var c = 0;
					 var d = 0;
					 var e = 0;
		  
					Massages.map((item ,index) =>
					 {
		
					   // var gmtDateTime = moment.utc(item.date)
					   // var date1 = gmtDateTime.local();
		
					   //   console.log('From Now Date :'+ date1.fromNow())
		
						if(item.date.search(todaydate +' '+months+' '+ years) != -1)
						{ 
							a++;
						 item.statusdate = "Today";
						 item.stop = a;
						}
					   else if(item.date.search((todaydate - 1) +' '+months+' '+ years) != -1)
						{ 
						 
							b++;
						 item.statusdate = "Yesterday";
						 item.stop = b;
						}
					   else if(item.date.search((todaydate - 2) +' '+months+' '+ years) != -1)
						{ 
						  
							c++;
						 item.statusdate = item.date.split(",")[1]; 
						 item.stop = c;
						  
					  } 
					  else if(item.date.search((todaydate - 3) +' '+months+' '+ years) != -1)
						{ 
						  
							d++;
							item.statusdate = item.date.split(",")[1]; 
						 item.stop = d;  
					  } 
					  else if(item.date.search((todaydate - 4) +' '+months+' '+ years) != -1)
					  { 
					    
						  e++;
						  item.statusdate = item.date.split(",")[1]; 
					   item.stop = e;  
					}
					 })*/

				this.setState({ Massages })
				//setTimeout(() => {this.scrollView.scrollToEnd({animated:false})}, 200)

			}
		})
	}

	setDeleteModalVisible(visible) {
		this.setState({ deleteModal: visible });
	}

	onLocationClicked() {
		this.setState({ dateTimeModal: false, deleteModal: false, locationModal: !this.state.locationModal })
	}

	onDateTimeClicked() {
		this.setState({ dateTimeModal: !this.state.dateTimeModal, deleteModal: false, locationModal: false })
	}

	onAddToCalendarPressed() {

		this.setState({ dateTimeModal: false }, () => {
			//let startdt = moment.utc(this.state.item.start_date).format('YYYY-MM-DDTHH:mm:ss.000Z')
			//let edndt = moment.utc(this.state.item.end_date).format('YYYY-MM-DDTHH:mm:ss.000Z')
			let startdt = moment.utc(this.state.item.start_date).toISOString()
			let edndt = moment.utc(this.state.item.end_date).toISOString()
			RNCalendarEvents.saveEvent(this.state.title_header, {
				startDate: startdt,
				endDate: edndt,
				location: this.state.item.line_1 + '\n\n' + this.state.item.line_2,
				allDay: this.state.item.is_allday == 1 ? true : false
			})
		})
		setTimeout(() => { Toast.show('Added to your calendar.') }, 1000)
		//alert('Start: '+startdt + ' End: '+edndt)
	}

	onEditPlanAction() {
		this.imagepickeropen = true
		this.setState({ dateTimeModal: false, locationModal: false }, () => { this.editPlan() })
	}

	onOpenInMapsPressed() {

		this.setState({ locationModal: false }, () => {
			LaunchNavigator.navigate(this.state.item.line_2)
				.then(() => console.log("Launched navigator"))
				.catch((err) => console.error("Error launching navigator: " + err));
		})


	}

	onOpenInGoogleMapsPressed() {

		this.setState({ locationModal: false }, () => {
			let app = null;

			LaunchNavigator.isAppAvailable(LaunchNavigator.APP.GOOGLE_MAPS).then((isWazeAvailable) => {
				if (isWazeAvailable) {
					app = LaunchNavigator.APP.GOOGLE_MAPS;
				} else {
					console.warn("Google Maps not available - falling back to default navigation app");
				}
				LaunchNavigator.navigate(this.state.item.line_2, {
					app: app
				}).then(() => console.log("Launched navigator"))
					.catch((err) => console.error("Error launching navigator: " + err));
			});
		})
	}

	onCopyAddressAction() {
		this.setState({ locationModal: false }, () => {
			Clipboard.setString(this.state.item.line_1 + ', ' + this.state.item.line_2);
		})

	}

	renderDeleteModal() {
		const { value } = this.state;
		return (
			<View>
				<Modal
					isVisible={this.state.deleteModal} style={{ margin: 0 }} onBackdropPress={() => this.setState({ deleteModal: false })} onBackButtonPress={() => this.setState({ deleteModal: false })}>
					<View style={{ height: 'auto', width: '82%', alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff', alignSelf: 'center', borderRadius: 10, }}>
						<Text style={{ marginTop: 25, width: '100%', alignSelf: 'center', fontFamily: fonts.Raleway_Semibold, fontSize: FontSize(15), color: colors.black, textAlign: 'center' }}>Are you sure you want to {'\n'} cancel this event?</Text>
						<View style={{ flexDirection: 'row', width: '100%', borderTopWidth: 1, marginTop: 10, borderColor: 'gray', marginTop: 25, }}>
							<View style={{ width: '50%', borderRightWidth: 1, alignItems: 'center', justifyContent: 'center', borderColor: 'gray', padding: 10 }}>
								<Text onPress={() => this.setState({ deleteModal: false })} style={{ fontFamily: fonts.Overpass_Regular, fontSize: FontSize(14), color: '#1986fb', fontWeight: 'bold', textAlign: 'center' }}>CANCEL</Text>
							</View>
							<View style={{ width: '50%', alignItems: 'center', justifyContent: 'center', padding: 10 }}>
								<Text onPress={this.deletePlan} style={{ fontFamily: fonts.Overpass_Regular, fontSize: FontSize(14), color: '#1986fb', textAlign: 'center', fontWeight: 'bold', }}>OK</Text>
							</View>
						</View>
					</View>

					{/* <View style={{ height: 'auto', borderRadius: 10, width: '80%', backgroundColor: '#fff', alignSelf: 'center' , padding: 10}}>

                        <Text style={{ marginTop: Height(1), width: Width(60), alignSelf: 'center', fontFamily: fonts.Raleway_Semibold, fontSize: FontSize(17), color: colors.black , textAlign: 'center'}}>Are you sure you want to delete this event?</Text>
                        <View style={{ height: Height(0.1), width: '100%',backgroundColor:colors.black , marginTop: Height(5) }} />
                        <View style={{ justifyContent: 'center', flexDirection: 'row', alignItems:'center'}}>
                            <Text onPress={() => this.setState({ deleteModal: false })} style={{ fontFamily: fonts.Overpass_Regular, fontSize: FontSize(14), color: colors.blue }}>CANCEL</Text>
                            <View style={{ width: Width(12)  }} />  
   
                            <View style={{ width: Width(0.1),height:Height(4),backgroundColor:colors.black }} />
                            <View style={{ width: Width(12) }} />

                            <Text onPress={this.deletePlan} style={{ fontFamily: fonts.Overpass_Regular, fontSize: FontSize(14), color: colors.blue }}>OK</Text>

                        </View>
                    </View> */}

				</Modal>
			</View>
		)
	}

	renderLocationModal() {
		const { value } = this.state;
		return (
			<View>
				<Modal
					isVisible={this.state.locationModal} style={{ margin: 0, zIndex: 2 }} onBackdropPress={() => this.setState({ locationModal: false })} onBackButtonPress={() => this.setState({ locationModal: false })}>
					{/* <View style={{ height: Platform.OS == 'ios' ? this.state.item.login_id == this.state.login_id ? Height(35):Height(30):Height(24),width: '100%',backgroundColor: '#fff',borderTopLeftRadius: 10,borderTopRightRadius:10, position:'absolute',bottom:0}}> */}
					<View style={{ width: '100%', backgroundColor: '#fff', borderTopLeftRadius: 10, borderTopRightRadius: 10, position: 'absolute', bottom: 0 }}>
						<View style={{ marginBottom: 25 }}>
							<View style={{ paddingLeft: Width(3), paddingRight: Width(2.3), marginBottom: 5 }}>
								<Text style={{ fontFamily: fonts.Roboto_Medium, fontSize: FontSize(15), color: colors.dargrey, marginTop: 20 }}>{this.state.item.line_1}</Text>
								<Text style={{ fontFamily: fonts.Roboto_Regular, fontSize: FontSize(13), color: colors.dargrey, marginTop: 10 }}>{this.state.item.line_2}</Text>
							</View>
							<View style={{ marginHorizontal: Width(3), height: 0.5, backgroundColor: '#B4B7BA', marginTop: 15 }}></View>
							{
								Platform.OS == 'ios' ?
									<TouchableOpacity activeOpacity={0.9} style={{ marginTop: 20, marginHorizontal: Width(3), backgroundColor: 'transparent', flexDirection: 'row' }} onPress={() => this.onOpenInMapsPressed()}>
										<Image style={{ width: 20, height: 20 }} source={openmap}></Image>
										<View style={{ marginLeft: 10, flexDirection: 'column', marginTop: 2 }}>
											<Text style={{ color: '#3A4759', fontSize: FontSize(16), fontFamily: fonts.Roboto_Regular }}>Open In Maps</Text>
										</View>
									</TouchableOpacity>
									:
									null
							}
							<TouchableOpacity activeOpacity={0.9} style={{ marginTop: 20, marginHorizontal: Width(3), backgroundColor: 'transparent', flexDirection: 'row' }} onPress={() => this.onOpenInGoogleMapsPressed()}>
								<Image style={{ width: 20, height: 20 }} source={openmap}></Image>
								<View style={{ marginLeft: 10, flexDirection: 'column', marginTop: 2 }}>
									<Text style={{ color: '#3A4759', fontSize: FontSize(16), fontFamily: fonts.Roboto_Regular }}>Open In Google Maps</Text>
								</View>
							</TouchableOpacity>

							<TouchableOpacity activeOpacity={0.9} style={{ marginTop: 20, marginHorizontal: Width(3), backgroundColor: 'transparent', flexDirection: 'row' }} onPress={() => this.onCopyAddressAction()}>
								<Image style={{ width: 20, height: 20 }} source={copyaddress}></Image>
								<View style={{ marginLeft: 10, flexDirection: 'column', marginTop: 2 }}>
									<Text style={{ color: '#3A4759', fontSize: FontSize(16), fontFamily: fonts.Roboto_Regular }}>Copy Address</Text>
								</View>
							</TouchableOpacity>
							{
								this.state.item.login_id == this.state.login_id ?
									<TouchableOpacity activeOpacity={0.9} style={{ marginTop: 15, marginHorizontal: Width(3), backgroundColor: 'transparent', flexDirection: 'row' }} onPress={() => this.onEditPlanAction()}>
										<Image style={{ width: 20, height: 20 }} source={editdatetime}></Image>
										<View style={{ marginLeft: 10, flexDirection: 'column', marginTop: 3 }}>
											<Text style={{ color: '#3A4759', fontSize: FontSize(16), fontFamily: fonts.Roboto_Regular }}>Edit Plan</Text>
										</View>
									</TouchableOpacity>
									:
									null
							}
						</View>
					</View>
				</Modal>
			</View>
		)
	}

	renderTimeModal() {
		const { value } = this.state;
		let char = ' \u00B7 '
		return (
			<View>
				<Modal
					isVisible={this.state.dateTimeModal} style={{ margin: 0 }} onBackdropPress={() => this.setState({ dateTimeModal: false })} onBackButtonPress={() => this.setState({ dateTimeModal: false })}>
					<View style={{ height: this.state.item.login_id == this.state.login_id ? Height(22) : Height(19), width: '100%', backgroundColor: '#fff', borderTopLeftRadius: 10, borderTopRightRadius: 10, position: 'absolute', bottom: 0 }}>
						{
							this.state.event_ends_sameday == true ?
								<View style={{ flexDirection: 'row', paddingLeft: Width(3), paddingRight: Width(2.3) }}>
									<Text style={{ fontFamily: fonts.Roboto_Medium, fontSize: FontSize(15), color: colors.dargrey, marginTop: 20 }}>{this.state.Final_date}{char}</Text>
									<Text style={{ fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey, marginTop: 20 }}>{this.state.Final_Enddate}</Text>
								</View>
								:
								<View>
									<View style={{ flexDirection: 'row', paddingLeft: Width(3), paddingRight: Width(2.3), marginTop: 20 }}>
										<Text style={{ fontFamily: fonts.Roboto_Medium, fontSize: FontSize(15), color: colors.dargrey }}>{this.state.Final_date} </Text>
										<Text style={{ fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{this.state.Final_StartTime}{char}</Text>
										<Text style={{ fontFamily: fonts.Roboto_Medium, fontSize: FontSize(15), color: colors.dargrey }}>{this.state.Final_Enddate} </Text>
										<Text style={{ fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{this.state.Final_EndTime}</Text>
									</View>
								</View>
						}
						<View style={{ marginHorizontal: Width(3), height: 0.5, backgroundColor: '#B4B7BA', marginTop: 20 }}></View>
						<View style={{ marginTop: 15, marginHorizontal: Width(3), height: Height(5), backgroundColor: 'transparent', flexDirection: 'row' }}>
							<Image style={{ width: 20, height: 22 }} source={iccalendar}></Image>
							<TouchableOpacity activeOpacity={0.9} style={{ marginLeft: 10, flexDirection: 'column' }} onPress={() => this.onAddToCalendarPressed()}>
								<Text style={{ color: '#3A4759', fontSize: FontSize(16), fontFamily: fonts.Roboto_Regular }}>Add to Calendar</Text>
								<Text style={{ color: '#C1C1C1', fontSize: FontSize(13), fontFamily: fonts.Roboto_Regular, marginTop: 8 }}>This event will be added to your phone's calendar</Text>
							</TouchableOpacity>
						</View>

						{
							this.state.item.login_id == this.state.login_id ?
								<TouchableOpacity activeOpacity={0.9} style={{ marginTop: 15, marginHorizontal: Width(3), height: Height(3), backgroundColor: 'transparent', flexDirection: 'row' }} onPress={() => this.onEditPlanAction()}>
									<Image style={{ width: 20, height: 20 }} source={editdatetime}></Image>
									<View style={{ marginLeft: 10, flexDirection: 'column', marginTop: 3 }}>
										<Text style={{ color: '#3A4759', fontSize: FontSize(16), fontFamily: fonts.Roboto_Regular }}>Edit Plan</Text>
									</View>
								</TouchableOpacity>
								:
								null
						}

					</View>
				</Modal>
			</View>
		)
	}

	renderTopBar() {
		return (
			<ImageBackground source = {this.state.item.plan_image == '' || this.state.item.plan_image == null || this.state.item.plan_image == undefined ? default_placeholder : {uri:this.state.item.plan_image}} style = {{width:'100%',height:screenHeight >= 812 ? 85 : 64}}>
			<View style = {{width:'100%',height:screenHeight >= 812 ? 85 : 64, position:'absolute',backgroundColor:'#41199B', opacity:0.7}}></View>
			<View style={[styles.headerAndroidnav, { backgroundColor: this.state.backgroundColor, height:screenHeight >= 812 ? 85 : 64}]}>
				
				<StatusBar barStyle="light-content" backgroundColor="#41199B" />
				
				<TouchableOpacity style={{bottom: this.state.viewState == false ? Height(1) : Height(1), position: 'absolute', left: Width(4), zIndex: 1, backgroundColor:'transparent', width:Width(8), height:Height(4.5), justifyContent:'center'}} onPress={() => {
						subThs = this
						subThs.imagepickeropen = true
						subThs.forceUpdate()
						//subThs.props.navigation.goBack()
						setTimeout(function () { 
							subThs.props.navigation.goBack() }, 200);
					}} activeOpacity = {0.9}>
					
					<Image source={this.state.Icon} />
					
					{/* <TouchableOpacity onPress={() => {
						subThs = this
						subThs.imagepickeropen = true
						subThs.forceUpdate()
						//subThs.props.navigation.goBack()
						setTimeout(function () { 
							subThs.props.navigation.goBack() }, 200);
					}} activeOpacity={0.9}>
						<Image source={this.state.Icon} />
					</TouchableOpacity>
					<TouchableOpacity onPress={() => {
						subThs = this
						subThs.imagepickeropen = true
						subThs.forceUpdate()
						//subThs.props.navigation.goBack()
						setTimeout(function () { subThs.props.navigation.goBack() }, 200);
					}} activeOpacity={0.9}>
						<View backgroundColor="transparent" position='absolute' height={70} width={70} marginTop={-30} marginLeft={-30}>
						</View>
					</TouchableOpacity> */}
				</TouchableOpacity>
				<View style={{ bottom: Height(3), position: 'absolute', alignSelf: this.state.viewState == false ? 'flex-start' : 'center', marginLeft: Width(13), marginRight: this.state.viewState == false ? Width(20) : Width(8) }}>
					<Text style={[styles.headerTitle, { color: this.state.color, fontSize: this.state.headerfontsize }]} numberOfLines={1}>{this.state.title_header}</Text>
				</View>
				{
					this.state.viewState == false ?
						<Text style={Platform.OS == "ios" ? { alignSelf: 'flex-start', fontSize: FontSize(10), fontFamily: fonts.Roboto_Regular, color: '#fff', bottom: 10, position: 'absolute', marginLeft: Width(13)} : { alignSelf: 'flex-start', fontSize: FontSize(10), fontFamily: fonts.Roboto_Regular, color: '#fff', bottom: 7, position: 'absolute', marginLeft: Width(13) }}>Organized by {this.state.item.name}</Text>
						:
						<Text style={Platform.OS == "ios" ? { alignSelf: 'center', fontSize: FontSize(10), fontFamily: fonts.Roboto_Regular, color: '#fff', bottom: 10, position: 'absolute' } : { alignSelf: 'center', fontSize: FontSize(10), fontFamily: fonts.Roboto_Regular, color: '#fff', bottom: 7, position: 'absolute' }}>Organized by {this.state.item.name}</Text>
				}
				{
					this.state.viewState == false ?
						<Text style={{ fontSize: FontSize(13), fontFamily: fonts.Roboto_Regular, color: 'white', right: 10, bottom: 20, position: 'absolute' }} onPress={() => this.toggleAnimation()} >View Details</Text>
						: null
				}
				
			</View>
			</ImageBackground>
			
		)
	}

	setFinalEventDate() {
		let Final_date = ''
		let Final_Enddate = ''
		let Final_StartTime = ''
		let Final_EndTime = ''
		let samedayevent = false
		let dateTime = '';
		//alert(Final_date)

		if (this.state.item.start_date != '' && this.state.item.start_date) {

			//alert(this.state.item.start_date)

			var currentDate = moment().format("ll");
			var tomorrow = moment().add(1, 'days').format("ll")

			var start = this.state.item.start_date.split(" ")
			var end = this.state.item.end_date.split(" ")

			var gmtStartDateTime = moment.utc(this.state.item.start_date)
			var localstartdate = gmtStartDateTime.local();

			var gmtEndDateTime = moment.utc(this.state.item.end_date)
			var localenddate = gmtEndDateTime.local();

			var startDate = start[0]
			var endDate = end[0]

			//alert(startDate+'  '+endDate)

			if (this.state.item.is_now == '1') {
				if (currentDate == moment(localstartdate).format('ll') && currentDate == moment(localenddate).format('ll')) {
					samedayevent = true
					//Final_date = 'Now - ' + moment(localenddate).format('LT')
					Final_date = 'Now untill '
					Final_Enddate = moment(localenddate).format('LT')

				} else {
					if (currentDate == moment(localstartdate).format('ll') && tomorrow == moment(localenddate).format('ll')) {
						samedayevent = true
						//Final_date = 'Now - Tomorrow · ' + moment(this.state.date).format('LT')
						Final_date = 'Now'
						Final_Enddate = 'Tomorrow · ' + moment(localenddate).format('LT')
					} else {
						//Final_date = 'Now - ' + moment(localenddate).format('ddd, MMM D \u00B7 LT')
						samedayevent = true
						Final_date = 'Now'
						Final_Enddate = moment(localenddate).format('ddd, MMM D \u00B7 LT')
					}
				}
			} else {
				if (currentDate == moment(localstartdate).format('ll') && currentDate == moment(localenddate).format('ll')) {
					samedayevent = true
					if (start[1] == '00:00:00' && end[1] == '00:00:00') {

						//Final_date = 'Today · All Day'
						Final_date = 'Today'
						Final_Enddate = 'All Day'

					} else {

						if (this.state.item.is_allday == 1) {
							//Final_date = 'Today · All Day'
							Final_date = 'Today'
							Final_Enddate = 'All Day'
						}
						else {
							//Final_date = 'Today · ' + moment(localstartdate).format('LT')
							Final_date = 'Today'
							Final_Enddate = moment(localstartdate).format('LT').replace('AM', '').replace('PM', '') + ' - ' + moment(localenddate).format('LT')
						}
					}
				} else {

					//alert(start[1]+' '+end[1])

					if (start[1] == '00:00:00' && end[1] == '00:00:00') {
						samedayevent = true
						if (tomorrow == moment(localstartdate).format('ll') && tomorrow == moment(localenddate).format('ll')) {

							//Final_date = 'Tomorrow · All Day'
							Final_date = 'Tomorrow'
							Final_Enddate = 'All Day'
						} else {
							if (tomorrow == moment(localstartdate).format('ll')) {
								//Final_date = 'Tomorrow · All Day'
								Final_date = 'Tomorrow'
								Final_Enddate = 'All Day'
							} else {
								if (currentDate == moment(localstartdate).format('ll')) {
									//Final_date = 'Today' + ' ·All Day '
									Final_date = 'Today'
									Final_Enddate = 'All Day '
								} else {
									//Final_date = moment(localstartdate).format('ddd, MMM D') + ' · All Day '
									Final_date = moment(localstartdate).format('ddd, MMM D')
									Final_Enddate = 'All Day'
								}

							}

						}
					} else {

						if (tomorrow == moment(localstartdate).format('ll') && tomorrow == moment(localenddate).format('ll')) {

							samedayevent = true

							if (this.state.item.is_allday == 1) {
								//Final_date = 'Tomorrow · All Day'
								Final_date = 'Tomorrow'
								Final_Enddate = 'All Day'
							}
							else {
								//Final_date = 'Tomorrow · ' + moment(localstartdate).format('LT')
								Final_date = 'Tomorrow'
								Final_Enddate = moment(localstartdate).format('LT').replace('AM', '').replace('PM', '') + ' - ' + moment(localenddate).format('LT')
							}

						} else {

							if (tomorrow == moment(localstartdate).format('ll')) {
								samedayevent = false
								//Final_date = 'Tomorrow · ' + moment(localstartdate).format('LT')
								Final_date = 'Tomorrow'
								Final_StartTime = ' ' + moment(localstartdate).format('LT')
								Final_Enddate = moment(localenddate).format('MMM D')
								Final_EndTime = ' ' + moment(localenddate).format('LT')
							} else {
								samedayevent = false
								if (currentDate == moment(localstartdate).format('ll')) {

									if (currentDate == moment(localenddate).format('ll')) {
										samedayevent = true
										//Final_date = 'Today ·' + moment(localstartdate).format('LT')
										Final_date = 'Today'
										Final_Enddate = moment(localstartdate).format('LT').replace('AM', '').replace('PM', '') + ' - ' + moment(localenddate).format('LT')
									}
									else {
										Final_date = 'Today' + ' \u00B7 ' + moment(localstartdate).format('LT')
										Final_Enddate = moment(localenddate).format('MMM D \u00B7 LT')
									}

								} else {

									if (this.state.item.is_allday == 1) {
										//Final_date = moment(localstartdate).format('ddd, MMM D') + ' · All Day '
										samedayevent = true
										Final_date = moment(localstartdate).format('ddd, MMM D')
										Final_Enddate = 'All Day '
									}
									else {
										//Final_date = moment(localstartdate).format('ddd, MMM D \u00B7 LT')

										if (startDate == endDate) {
											samedayevent = true
											Final_date = moment(localstartdate).format('ddd, MMM D')//\u00B7
											Final_Enddate = moment(localstartdate).format('LT').replace('AM', '').replace('PM', '') + ' - ' + moment(localenddate).format('LT')
										}
										else {
											samedayevent = false
											Final_date = moment(localstartdate).format('MMM D')//\u00B7
											Final_Enddate = moment(localenddate).format('MMM D')
											Final_StartTime = ' ' + moment(localstartdate).format('LT')
											Final_EndTime = ' ' + moment(localenddate).format('LT')

										}

									}

								}

							}

						}

					}

				}

			}

		}

		this.setState({ Final_date: Final_date, Final_Enddate: Final_Enddate, event_ends_sameday: samedayevent, Final_StartTime: Final_StartTime, Final_EndTime: Final_EndTime })

	}

	onUsersPressed() {

		let plan_id = this.props.navigation.getParam('plan_id', null);

    //alert(plan_id)

		var title = ""

		if(this.state.item.plan_type == 1)
		{
			title = ""
		}
		else if(this.state.item.plan_type == 2)
		{
			title = this.state.item.activity
		}
		else if(this.state.item.plan_type == 3)
		{
			title = this.state.item.line_1
		}
		else if (this.state.item.plan_type == 4)
		{
			title = ""
		}

		if (this.state.item.is_going_count > 0) {
			this.props.navigation.navigate('Friend_Status', { tabIndex: 0, plan_id: plan_id, Title_New: title, plan_type: this.state.item.plan_type })
		}
		else {
			this.props.navigation.navigate('Friend_Status', { tabIndex: 1, plan_id: plan_id, Title_New: title, plan_type: this.state.item.plan_type })
		}
	}

	editPlan() {

		if (this.state.item.plan_type == 1) {

			if (this.state.item.is_invited_list.length > 0) {

				var arrfriends = this.state.item.is_invited_list
				var nameIDs = []
				var count = arrfriends.length - 1
				var name = ''

				if (arrfriends.length > 0) {
					name = arrfriends[0].receiver_name
					arrfriends.map((item) => {
						nameIDs.push(item.receiver_id)
					});

					AsyncStorage.setItem('receiver_id', String(nameIDs))
					AsyncStorage.setItem('Activity', this.state.item.activity)
					AsyncStorage.setItem('Location', this.state.item.line_1)
					AsyncStorage.setItem('who_desc',this.state.item.description)
					this.setDateValue(this.state.item.start_date, this.state.item.end_date)
					var invitedusername = ''

					if (arrfriends.length > 1) {
						invitedusername = name + ' and ' + (arrfriends.length - 1) + ' More'
					}
					else {
						invitedusername = name
					}
					this.props.navigation.navigate('Who2', { friendName: invitedusername, is_edit_plan: true, item: this.state.item, invitedList: this.state.invitedList })

				}
			}
		}
		else if (this.state.item.plan_type == 2) {

			var arrfriends = this.state.item.is_invited_list
			var nameIDs = []
			var count = arrfriends.length - 1
			var name = ''
			var contacts = this.state.item.contacts

			if (arrfriends.length > 0) {
				name = arrfriends[0].receiver_name
				arrfriends.map((item) => {

					nameIDs.push(item.receiver_id)

				});

			}
			AsyncStorage.setItem('what_friends', JSON.stringify(nameIDs))
			AsyncStorage.setItem('what_contacts', JSON.stringify(contacts))

			if(arrfriends.length == 0)
              {
                AsyncStorage.setItem('what_friends_name', '')
              }
              else if (arrfriends.length == 1) {
                  AsyncStorage.setItem('what_friends_name', JSON.stringify(name))
      
              } else {
                  AsyncStorage.setItem('what_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
      
              }

		/*	if (arrfriends.length == 0) {
				//AsyncStorage.setItem('what_friends_name', '')
				if(contacts.length == 0)
                {
                  AsyncStorage.setItem('what_friends_name', '')
                }
                else
                {
                  var cnt = contacts.length
                  if(contacts.length == 1)
                  {
                    AsyncStorage.setItem('what_friends_name', JSON.stringify(contacts))
                  }
                  else
                  {
                    AsyncStorage.setItem('what_friends_name', JSON.stringify(contacts[0] + ' and ' + (cnt - 1) + ' more'))
                  }
                }
			}
			else if (arrfriends.length == 1) {
				//AsyncStorage.setItem('what_friends_name', JSON.stringify(name))

				if(contacts.length > 0)
                  {
                    var cnt = contacts.length
                    AsyncStorage.setItem('what_friends_name', JSON.stringify(name + ' and ' + cnt + ' more'))
                  }
                  else
                  {
                    AsyncStorage.setItem('what_friends_name', JSON.stringify(name))
                  }

			} else {
				//AsyncStorage.setItem('what_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
				if(contacts.length > 0)
                  {
                    var cnt = contacts.length
                    count = count + cnt
                    AsyncStorage.setItem('what_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
                  }
                  else
                  {
                    AsyncStorage.setItem('what_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
                  }
				}
				*/


			AsyncStorage.setItem('What_Activity', this.state.item.activity)
			AsyncStorage.setItem('what_Location', this.state.item.line_1)//what_FullAddress
			AsyncStorage.setItem('what_FullAddress', this.state.item.line_2)
			AsyncStorage.setItem('what_desc',this.state.item.description)
			this.setDateValueForWhat(this.state.item.start_date, this.state.item.end_date)
			this.props.navigation.navigate('Frinds', { is_edit_plan: true, item: this.state.item })
		}
		else if (this.state.item.plan_type == 3) {

			var arrfriends = this.state.item.is_invited_list
			var nameIDs = []
			var count = arrfriends.length - 1
			var name = ''
			var contacts = this.state.item.contacts

			if (arrfriends.length > 0) {
				name = arrfriends[0].receiver_name
				arrfriends.map((item) => {

					nameIDs.push(item.receiver_id)

				});

			}
			AsyncStorage.setItem('where_friends', JSON.stringify(nameIDs))
			AsyncStorage.setItem('where_contacts', JSON.stringify(contacts))

			if(arrfriends.length == 0)
              {
                AsyncStorage.setItem('where_friends_name', '')
              }
              else if (arrfriends.length == 1) {
                  AsyncStorage.setItem('where_friends_name', JSON.stringify(name))
      
              } else {
                  AsyncStorage.setItem('where_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
      
              }

		/*	if (arrfriends.length == 0) {
				//AsyncStorage.setItem('where_friends_name', '')
				if(contacts.length == 0)
                {
                  AsyncStorage.setItem('where_friends_name', '')
                }
                else
                {
                  var cnt = contacts.length
                  if(contacts.length == 1)
                  {
                    AsyncStorage.setItem('where_friends_name', JSON.stringify(contacts))
                  }
                  else
                  {
                    AsyncStorage.setItem('where_friends_name', JSON.stringify(contacts[0] + ' and ' + (cnt - 1) + ' more'))
                  }
                }
			}
			else if (arrfriends.length == 1) {
				//AsyncStorage.setItem('where_friends_name', JSON.stringify(name))
				if(contacts.length > 0)
                  {
                    var cnt = contacts.length
                    AsyncStorage.setItem('where_friends_name', JSON.stringify(name + ' and ' + cnt + ' more'))
                  }
                  else
                  {
                    AsyncStorage.setItem('where_friends_name', JSON.stringify(name))
                  }

			} else {
				//AsyncStorage.setItem('where_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
				if(contacts.length > 0)
                  {
                    var cnt = contacts.length
                    count = count + cnt
                    AsyncStorage.setItem('where_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
                  }
                  else
                  {
                    AsyncStorage.setItem('where_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
                  }

			}
			*/

			AsyncStorage.setItem('where_Location', this.state.item.line_1)
			AsyncStorage.setItem('where_FullAddress', this.state.item.line_2)
			AsyncStorage.setItem('where_Activity', this.state.item.activity)
			AsyncStorage.setItem('where_desc',this.state.item.description)
			this.setDateValueForWhere(this.state.item.start_date, this.state.item.end_date)
			this.props.navigation.navigate('where2', { is_edit_plan: true, item: this.state.item })
		}
		else if (this.state.item.plan_type == 4) {

			var arrfriends = this.state.item.is_invited_list
			var nameIDs = []
			var count = arrfriends.length - 1
			var name = ''
			var  contacts = this.state.item.contacts

			if (arrfriends.length > 0) {
				name = arrfriends[0].receiver_name
				arrfriends.map((item) => {

					nameIDs.push(item.receiver_id)

				});

			}
			AsyncStorage.setItem('when_friends', JSON.stringify(nameIDs))
			AsyncStorage.setItem('when_contacts', JSON.stringify(contacts))

			if(arrfriends.length == 0)
			{
			AsyncStorage.setItem('when_friends_name', '')
			}
			else if (arrfriends.length == 1) {
				AsyncStorage.setItem('when_friends_name', JSON.stringify(name))
			} else {
				AsyncStorage.setItem('when_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
				
			}

		/*	if (arrfriends.length == 0) {
				//AsyncStorage.setItem('where_friends_name', '')
				if(contacts.length == 0)
                {
                  AsyncStorage.setItem('when_friends_name', '')
                }
                else
                {
                  var cnt = contacts.length
                  if(contacts.length == 1)
                  {
                    AsyncStorage.setItem('when_friends_name', JSON.stringify(contacts))
                  }
                  else
                  {
                    AsyncStorage.setItem('when_friends_name', JSON.stringify(contacts[0] + ' and ' + (cnt - 1) + ' more'))
                  }
                }
			}
			else if (arrfriends.length == 1) {
				//AsyncStorage.setItem('when_friends_name', JSON.stringify(name))
				if(contacts.length > 0)
                  {
                    var cnt = contacts.length
                    AsyncStorage.setItem('when_friends_name', JSON.stringify(name + ' and ' + cnt + ' more'))
                  }
                  else
                  {
                    AsyncStorage.setItem('when_friends_name', JSON.stringify(name))
                  }
			} else {
				//AsyncStorage.setItem('when_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
				if(contacts.length > 0)
                  {
                    var cnt = contacts.length
                    count = count + cnt
                    AsyncStorage.setItem('when_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
                  }
                  else
                  {
                    AsyncStorage.setItem('when_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
                  }
			}
			*/
			AsyncStorage.setItem('when_Location', this.state.item.line_1)
			AsyncStorage.setItem('when_FullAddress', this.state.item.line_2)
			AsyncStorage.setItem('when_Activity', this.state.item.activity)
			AsyncStorage.setItem('when_desc',this.state.item.description)
			this.setDateValueForWhen(this.state.item.start_date, this.state.item.end_date)
			this.props.navigation.navigate('when2', { is_edit_plan: true, item: this.state.item })
		}

	}

	setDateValue(sdate, edate) {

		//var datestartObj = new Date(start_date);
		//var dateendObj = new Date(end_date);

		//var startdate = moment(start_date).format("llll")
		//var enddate = moment(end_date).format("llll")

		//alert(startdate+ ' '+ enddate)

		//return

		var currentDate = moment().format("ll");
		let tomorrow = moment().add(1, 'days').format("ll")
		var date = moment(edate).format('ll')

		if (sdate != '') {

			var gmtStartDateTime = moment.utc(sdate)
			var startdate = gmtStartDateTime.local();

			var gmtEndDateTime = moment.utc(edate)
			var enddate = gmtEndDateTime.local();

			// if (this.state.item.is_now) {
			//   if (date == currentDate) {
			//     AsyncStorage.setItem('CurrentDate', 'Now - ' + moment(enddate).format('LT'))
			//   } else {
			//     if (date == tomorrow) {
			//       AsyncStorage.setItem('CurrentDate', 'Now - Tomorrow · ' + moment(enddate).format('LT'))

			//     } else {
			//       AsyncStorage.setItem('CurrentDate', 'Now - ' + moment(date).format('ddd, MMM D') + ' · ' + moment(enddate).format('LT'))
			//     }
			//   }
			//   AsyncStorage.setItem('start', currentDate)
			//   AsyncStorage.setItem('end', moment(enddate).format('lll'))
			//   AsyncStorage.setItem('is_now', '1')
			//   AsyncStorage.setItem('is_allday', '0')
			// }
			// else 
			if (this.state.item.is_allday == 1) {
				if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
					AsyncStorage.setItem('CurrentDate', 'Today · All Day')
				}
				else {
					if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
						AsyncStorage.setItem('CurrentDate', 'Today - Tomorrow · All Day')

					} else {
						if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
							AsyncStorage.setItem('CurrentDate', 'Tomorrow · All Day')

						} else {
							if (tomorrow == moment(startdate).format('ll')) {
								AsyncStorage.setItem('CurrentDate', 'Tomorrow - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

							} else {
								if (currentDate == moment(startdate).format('ll')) {
									AsyncStorage.setItem('CurrentDate', 'Today - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

								} else {
									if (moment(startdate).format('ll') == moment(enddate).format('ll')) {
										AsyncStorage.setItem('CurrentDate', 'All Day - ' + moment(enddate).format('ddd, MMM D'))

									} else {
										AsyncStorage.setItem('CurrentDate', moment(startdate).format('ddd, MMM D') + ' - ' + moment(enddate).format('ddd, MMM D') + ' · ALL DAY')

									}
								}

							}

						}

					}

				}
				AsyncStorage.setItem('start', moment(startdate).format('ll'))
				AsyncStorage.setItem('end', moment(enddate).format('ll'))
				AsyncStorage.setItem('is_now', '0')
				AsyncStorage.setItem('is_allday', '1')
			}
			else {

				if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
					AsyncStorage.setItem('CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))

				} else {
					if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
						AsyncStorage.setItem('CurrentDate', 'Tomorrow \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))
					} else {
						if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
							AsyncStorage.setItem('CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - Tomorrow \u00B7 ' + moment(enddate).format('LT'))
						} else {
							if (tomorrow == moment(startdate).format('ll')) {
								AsyncStorage.setItem('CurrentDate', ' Tomorrow - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
							} else {
								if (currentDate == moment(startdate).format('ll')) {
									if(currentDate == moment(enddate).format('ll'))
									{
										AsyncStorage.setItem('CurrentDate', 'Tomorrow · All Day')
									}
									else
									{
										AsyncStorage.setItem('CurrentDate', 'Today - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
									}
									

								} else {
									AsyncStorage.setItem('CurrentDate', moment(startdate).format('ddd, MMM D \u00B7 LT') + ' to ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
								}
							}
						}
					}
				}
				AsyncStorage.setItem('start', moment(startdate).format('lll'))
				AsyncStorage.setItem('end', moment(enddate).format('lll'))
				AsyncStorage.setItem('is_now', '0')
				AsyncStorage.setItem('is_allday', '0')
			}
		}
		else {
			AsyncStorage.setItem('start', '')
			AsyncStorage.setItem('end', '')
			AsyncStorage.setItem('CurrentDate', '')
		}
	}

	setDateValueForWhat(sdate, edate) {
		var currentDate = moment().format("ll");
		let tomorrow = moment().add(1, 'days').format("ll")
		var date = moment(edate).format('ll')



		if (sdate != '') {

			var gmtStartDateTime = moment.utc(sdate)
			var startdate = gmtStartDateTime.local();

			var gmtEndDateTime = moment.utc(edate)
			var enddate = gmtEndDateTime.local();

			//     if (this.state.item.is_now) {
			//       if (date == currentDate) {
			//         AsyncStorage.setItem('what_CurrentDate', 'Now - ' + moment(enddate).format('LT'))
			//       } else {
			//         if (date == tomorrow) {
			//           AsyncStorage.setItem('what_CurrentDate', 'Now - Tomorrow · ' + moment(enddate).format('LT'))

			//         } else {
			//           AsyncStorage.setItem('what_CurrentDate', 'Now - ' + moment(date).format('ddd, MMM D') + ' · ' + moment(enddate).format('LT'))

			//         }

			//       }

			//       AsyncStorage.setItem('what_start', currentDate)
			//       AsyncStorage.setItem('what_end', moment(enddate).format('lll'))
			//       AsyncStorage.setItem('what_is_now', '1')
			//       AsyncStorage.setItem('what_is_allday', '0')
			// }
			// else 
			if (this.state.item.is_allday == 1) {
				if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
					AsyncStorage.setItem('what_CurrentDate', 'Today · All Day')
				}
				else {
					if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
						AsyncStorage.setItem('what_CurrentDate', 'Today - Tomorrow · All Day')

					} else {
						if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
							AsyncStorage.setItem('what_CurrentDate', 'Tomorrow · All Day')

						} else {
							if (tomorrow == moment(startdate).format('ll')) {
								AsyncStorage.setItem('what_CurrentDate', 'Tomorrow - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

							} else {
								if (currentDate == moment(startdate).format('ll')) {
									AsyncStorage.setItem('what_CurrentDate', 'Today - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

								} else {
									if (moment(startdate).format('ll') == moment(enddate).format('ll')) {
										AsyncStorage.setItem('what_CurrentDate', 'All Day - ' + moment(enddate).format('ddd, MMM D'))

									} else {
										AsyncStorage.setItem('what_CurrentDate', moment(startdate).format('ddd, MMM D') + ' - ' + moment(enddate).format('ddd, MMM D') + ' · ALL DAY')

									}
								}
							}

						}

					}

				}
				AsyncStorage.setItem('what_start', moment(startdate).format('ll'))
				AsyncStorage.setItem('what_end', moment(enddate).format('ll'))
				AsyncStorage.setItem('what_is_now', '0')
				AsyncStorage.setItem('what_is_allday', '1')
			}
			else {
				////////////////////////
				if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
					AsyncStorage.setItem('what_CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))

				} else {
					if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
						AsyncStorage.setItem('what_CurrentDate', 'Tomorrow \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))
					} else {
						if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
							AsyncStorage.setItem('what_CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - Tomorrow \u00B7 ' + moment(enddate).format('LT'))
						} else {
							if (tomorrow == moment(startdate).format('ll')) {
								AsyncStorage.setItem('what_CurrentDate', ' Tomorrow  ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
							} else {
								if (currentDate == moment(startdate).format('ll')) {
									if(currentDate == moment(enddate).format('ll'))
									{
										AsyncStorage.setItem('what_CurrentDate', 'Today · All Day')
									}
									else
									{
										AsyncStorage.setItem('what_CurrentDate', 'Today - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
									}
									

								} else {
									AsyncStorage.setItem('what_CurrentDate', moment(startdate).format('ddd, MMM D \u00B7 LT') + ' to ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
								}
							}
						}


					}
				}
				AsyncStorage.setItem('what_start', moment(startdate).format('lll'))
				AsyncStorage.setItem('what_end', moment(enddate).format('lll'))
				AsyncStorage.setItem('what_is_now', '0')
				AsyncStorage.setItem('what_is_allday', '0')
			}
		}
		else {
			AsyncStorage.setItem('what_start', '')
			AsyncStorage.setItem('what_end', '')
			AsyncStorage.setItem('what_CurrentDate', '')
		}

	}

	setDateValueForWhere(sdate, edate) {
		var currentDate = moment().format("ll");
		let tomorrow = moment().add(1, 'days').format("ll")
		var date = moment(edate).format('ll')

		if (sdate != '') {

			var gmtStartDateTime = moment.utc(sdate)
			var startdate = gmtStartDateTime.local();

			var gmtEndDateTime = moment.utc(edate)
			var enddate = gmtEndDateTime.local();

			// if (this.state.item.is_now) {
			//   if (date == currentDate) {
			//     AsyncStorage.setItem('where_CurrentDate', 'Now - ' + moment(enddate).format('LT'))
			//   } else {
			//     if (date == tomorrow) {
			//       AsyncStorage.setItem('where_CurrentDate', 'Now - Tomorrow · ' + moment(enddate).format('LT'))

			//     } else {
			//       AsyncStorage.setItem('where_CurrentDate', 'Now - ' + moment(date).format('ddd, MMM D') + ' · ' + moment(enddate).format('LT'))

			//     }

			//   }

			//   AsyncStorage.setItem('where_start', currentDate)
			//   AsyncStorage.setItem('where_end', moment(enddate).format('lll'))
			//   AsyncStorage.setItem('where_is_now', '1')
			//   AsyncStorage.setItem('where_is_allday', '0')


			// }
			// else 
			if (this.state.item.is_allday == 1) {
				if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
					AsyncStorage.setItem('where_CurrentDate', 'Today · All Day')
				}
				else {
					if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
						AsyncStorage.setItem('where_CurrentDate', 'Today - Tomorrow · All Day')

					} else {
						if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
							AsyncStorage.setItem('where_CurrentDate', 'Tomorrow · All Day')

						} else {
							if (tomorrow == moment(startdate).format('ll')) {
								AsyncStorage.setItem('where_CurrentDate', 'Tomorrow - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

							} else {
								if (currentDate == moment(startdate).format('ll')) {
									AsyncStorage.setItem('where_CurrentDate', 'Today - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

								} else {
									if (moment(startdate).format('ll') == moment(enddate).format('ll')) {
										AsyncStorage.setItem('where_CurrentDate', 'All Day - ' + moment(enddate).format('ddd, MMM D'))

									} else {
										AsyncStorage.setItem('where_CurrentDate', moment(startdate).format('ddd, MMM D') + ' - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

									}

								}
							}

						}

					}

				}
				AsyncStorage.setItem('where_start', moment(startdate).format('ll'))
				AsyncStorage.setItem('where_end', moment(startdate).format('ll'))
				AsyncStorage.setItem('where_is_now', '0')
				AsyncStorage.setItem('where_is_allday', '1')
			}
			else {
				/////////////

				if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
					AsyncStorage.setItem('where_CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))

				} else {
					if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
						AsyncStorage.setItem('where_CurrentDate', 'Tomorrow \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))
					} else {
						if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
							AsyncStorage.setItem('where_CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - Tomorrow \u00B7 ' + moment(enddate).format('LT'))
						} else {
							if (tomorrow == moment(startdate).format('ll')) {
								AsyncStorage.setItem('where_CurrentDate', ' Tomorrow - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
							} else {
								if (currentDate == moment(startdate).format('ll')) {
									AsyncStorage.setItem('where_CurrentDate', ' Today - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))

								} else {
									AsyncStorage.setItem('where_CurrentDate', moment(startdate).format('ddd, MMM D \u00B7 LT') + ' to ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))

								}

							}
						}


					}
				}
				AsyncStorage.setItem('where_start', moment(startdate).format('lll'))
				AsyncStorage.setItem('where_end', moment(enddate).format('lll'))
				AsyncStorage.setItem('where_is_now', '0')
				AsyncStorage.setItem('where_is_allday', '0')
			}
		}
		else {
			AsyncStorage.setItem('where_start', '')
			AsyncStorage.setItem('where_end', '')
			AsyncStorage.setItem('where_CurrentDate', '')
		}
	}

	setDateValueForWhen(sdate, edate) {

		var currentDate = moment().format("ll");
		let tomorrow = moment().add(1, 'days').format("ll")
		var date = moment(edate).format('ll')


		if (sdate != '') {


			var gmtStartDateTime = moment.utc(sdate)
			var startdate = gmtStartDateTime.local();

			var gmtEndDateTime = moment.utc(edate)
			var enddate = gmtEndDateTime.local();

			// if (this.state.item.is_now) {
			//   if (date == currentDate) {
			//     AsyncStorage.setItem('when_CurrentDate', 'Now - ' + moment(enddate).format('LT'))
			//   } else {
			//     if (date == tomorrow) {
			//       AsyncStorage.setItem('when_CurrentDate', 'Now - Tomorrow · ' + moment(enddate).format('LT'))

			//     } else {
			//       AsyncStorage.setItem('when_CurrentDate', 'Now - ' + moment(this.state.date).format('ddd, MMM D') + ' · ' + moment(enddate).format('LT'))

			//     }

			//   }

			//   AsyncStorage.setItem('when_start', currentDate)
			//   AsyncStorage.setItem('when_end', moment(enddate).format('lll'))
			//   AsyncStorage.setItem('when_is_now', '1')
			//   AsyncStorage.setItem('when_is_allday', '0')
			// }
			// else 
			if (this.state.item.is_allday == 1) {
				if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
					AsyncStorage.setItem('when_CurrentDate', 'Today · All Day')
				}
				else {
					if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
						AsyncStorage.setItem('when_CurrentDate', 'Today - Tomorrow · All Day')

					} else {
						if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
							AsyncStorage.setItem('when_CurrentDate', 'Tomorrow · All Day')

						} else {
							if (tomorrow == moment(startdate).format('ll')) {
								AsyncStorage.setItem('when_CurrentDate', 'Tomorrow - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

							} else {
								if (currentDate == moment(startdate).format('ll')) {
									AsyncStorage.setItem('when_CurrentDate', 'Today - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

								} else {
									if (moment(startdate).format('ll') == moment(enddate).format('ll')) {
										AsyncStorage.setItem('when_CurrentDate', 'All Day - ' + moment(enddate).format('ddd, MMM D'))

									} else {
										AsyncStorage.setItem('when_CurrentDate', moment(startdate).format('ddd, MMM D') + ' - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

									}
								}
							}

						}

					}

				}
				AsyncStorage.setItem('when_start', moment(startdate).format('ll'))
				AsyncStorage.setItem('when_end', moment(enddate).format('ll'))
				AsyncStorage.setItem('when_is_now', '0')
				AsyncStorage.setItem('when_is_allday', '1')
			}
			else {
				////////////////////////////////////////////////

				if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
					AsyncStorage.setItem('when_CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))

				} else {
					if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
						AsyncStorage.setItem('when_CurrentDate', 'Tomorrow \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))
					} else {
						if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
							AsyncStorage.setItem('when_CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - Tomorrow \u00B7 ' + moment(enddate).format('LT'))
						} else {
							if (tomorrow == moment(startdate).format('ll')) {
								AsyncStorage.setItem('when_CurrentDate', ' Tomorrow - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
							} else {
								if (currentDate == moment(startdate).format('ll')) {
									AsyncStorage.setItem('when_CurrentDate', ' Today - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))

								} else {
									AsyncStorage.setItem('when_CurrentDate', moment(startdate).format('ddd, MMM D \u00B7 LT') + ' - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
								}
							}
						}


					}
				}
				AsyncStorage.setItem('when_start', moment(startdate).format('lll'))
				AsyncStorage.setItem('when_end', moment(enddate).format('lll'))
				AsyncStorage.setItem('when_is_now', '0')
				AsyncStorage.setItem('when_is_allday', '0')
			}
		}
		else {
			AsyncStorage.setItem('when_start', '')
			AsyncStorage.setItem('when_end', '')
			AsyncStorage.setItem('when_CurrentDate', '')
		}

	}

	_keyboardDidHide() {

		//alert('_keyboardDidHide')
		if (Platform.OS == 'android') {
			if(_this.messagefield != null && _this.messagefield != undefined)
			{
				_this.messagefield.blur()
			}
		}

		if (this.headertap == false) {
			this.setState({ is_textfield_focused: false })
		}
		else {
			this.headertap = false
		}

		// var multityper = []; 
		// let plan_id = this.props.navigation.getParam('plan_id',null);

		// this.refstatus =  firebase.database().ref('Groups').child(plan_id)
		// this.refstatus.on('value',statuss =>
		//     { 
		//       if(statuss.val()!=null)
		//       {
		//        multityper = statuss.val().status;
		//       }
		//     })
		//     var index = multityper.findIndex(x => x.id === this.state.login_id);
		//      if (index != -1) {   
		//       multityper.splice(index, 1); 
		//     }
		//   firebase.database().ref('Groups').child(plan_id).update({
		//     status: multityper 
		//   })  
		//   this.setState({marginBottom: 0 }) 
		// end ----- added by utsav --- 
	}
	// end 

	// typing status     
	_keyboardDidShow() {

		console.log('_keyboardDidShow')

		// let plan_id = this.props.navigation.getParam('plan_id',null);

		//   var isnull = true;
		//   var Name = this.state.Name;
		//   var multityper = this.state.status;
		//  var Avtar = store.getState().auth.user.photo;

		//     if(multityper.length == 0)
		//     {
		//       firebase.database().ref('Groups').child(plan_id).update({
		//                       status: [{Avtar, name: Name, id: this.state.login_id}] 
		//                   })
		//     } 
		//     else
		//     {
		//         if(multityper!=undefined)
		//         {
		//       var index = multityper.findIndex(x => x.id === this.state.login_id);
		//       if (index == -1) { 
		//         multityper.push({Avtar, name: Name, id: this.state.login_id})
		//                     firebase.database().ref('Groups').child(plan_id).update({
		//                       status: multityper 
		//                   })
		//       } 
		//     } 
		//     }    
		// this.setState({marginBottom: 10 })
		// end --- added by utsav ---
	}
	tick = () => {
		this.setState({
			counter: this.state.counter + 1
		});
		if (this.state.counter > 1) {
			this.setState({
				counter: 0
			});
			clearInterval(TimeInterval)
			var multityper = [];
			let plan_id = this.props.navigation.getParam('plan_id', null);

			this.refstatus = firebase.database().ref('Groups').child(plan_id)
			this.refstatus.on('value', statuss => {
				if (statuss.val() != null) {
					multityper = statuss.val().status;
				}
			})
			if (multityper != null && multityper != undefined) {
				var index = multityper.findIndex(x => x.id === this.state.login_id);
				if (index != -1) {
					multityper.splice(index, 1);
				}
				firebase.database().ref('Groups').child(plan_id).update({
					status: multityper
				})
			}
		}


	}

	keyboardPresented() {

		console.log('keyboardPresented')

		clearInterval(TimeInterval)
		this.setState({
			counter: 0
		});

		TimeInterval = setInterval(this.tick, 1000);

		let plan_id = this.props.navigation.getParam('plan_id', null);
		var isnull = true;
		var Name = this.state.Name;
		var multityper = this.state.status;
		var Avtar = store.getState().auth.user.photo;

		if (multityper.length == 0) {
			firebase.database().ref('Groups').child(plan_id).update({
				status: [{ Avtar, name: Name, id: this.state.login_id }]
			})
		}
		else {
			if (multityper != undefined) {
				var index = multityper.findIndex(x => x.id === this.state.login_id);
				if (index == -1) {
					multityper.push({ Avtar, name: Name, id: this.state.login_id })
					firebase.database().ref('Groups').child(plan_id).update({
						status: multityper
					})
				}
			}
		}
		this.setState({ marginBottom: 10 })
	}

	keyboardDismissed() {
		//this.setState({is_textfield_focused:false})

		console.log('keyboardDismissed')

		var multityper = [];
		let plan_id = this.props.navigation.getParam('plan_id', null);

		this.refstatus = firebase.database().ref('Groups').child(plan_id)
		this.refstatus.on('value', statuss => {
			if (statuss.val() != null) {
				multityper = statuss.val().status;
			}
		})
		if (multityper != null && multityper != undefined) {
			var index = multityper.findIndex(x => x.id === this.state.login_id);
			if (index != -1) {
				multityper.splice(index, 1);
			}
			firebase.database().ref('Groups').child(plan_id).update({
				status: multityper
			})
		}

		this.setState({ marginBottom: 0 })
	}

	onInputFocus() {
		this.setState({ is_textfield_focused: false })

		//alert('onInputFocus')
		//if(Platform.OS == 'ios')
		{
			//this.toggleAnimation()
			//this.keyboardPresented()
			this.collapseHeader()
			this.setState({ is_textfield_focused: true })
			if (this.flat_list != null) {
				setTimeout(() => { this.flat_list.scrollToEnd({ animated: false }) }, 200)
			}
		}

	}

	onInputBlur() {
		this.keyboardDismissed()
		//alert('onInputBlur')
		//if(Platform.OS == 'ios')
		{
			//this.toggleAnimation()
			//this.setState({is_textfield_focused:false})
		}
	}

	renderItem(source, i) {
		var datelabeltext = moment.utc(source.date).local().calendar()
		this.curdaydiff = datelabeltext.split(' ')[0]
		this.curmsgtime = moment.utc(source.date).local().format('hh:mm A')
		this.curmsgloginid = source.sentBy

		var prevmsgtime = ''
		var previmsgloginid = ''
		var previousdaydiff = ''

		//console.log('Messageday: '+this.curdaydiff)

		if (!this.weekdays.includes(this.curdaydiff)) {
			this.curdaydiff = moment.utc(source.date).local().format('LL')
		}

		var previoussource = ''

		if (i > 0) {

			previoussource = this.state.Massages[i - 1]
			var prevdatelabeltext = moment.utc(previoussource.date).local().calendar()
			previousdaydiff = prevdatelabeltext.split(' ')[0]
			prevmsgtime = moment.utc(previoussource.date).local().format('hh:mm A')
			previmsgloginid = previoussource.sentBy

			if (!this.weekdays.includes(previousdaydiff)) {
				previousdaydiff = moment.utc(previoussource.date).local().format('LL')
			}
		}

		//console.log('CurrentDiff: '+this.curdaydiff+'   '+this.previousdaydiff +'   i: '+i)



		return (
			<View
				style={{
					// marginLeft: Width(5),
					// marginRight: Width(5),
					width: "100%",
					backgroundColor: 'transparent'
				}}>
				{

					(previousdaydiff == '' || this.curdaydiff != previousdaydiff) ?
						<View style={{ alignItems: 'center', justifyContent: 'center', paddingHorizontal: 15, backgroundColor: '#b1b6bb', height: Height(2.5), borderRadius: Height(1.25), alignSelf: 'center', marginVertical: 5 }}>
							<Text
								style={{
									textAlign: "center",
									color: 'white',
									fontSize: 12,
									fontFamily: fonts.Roboto_Regular,
									height: Height(2)
								}}
							>
								{
									this.curdaydiff
								}
							</Text>
						</View>

						:
						null
				}

				<View
					style={{
						flexDirection:
							source.sentBy === this.state.login_id
								? null
								: 'row',

						alignItems:
							source.sentBy === this.state.login_id
								? "flex-end"
								: "flex-start",
						width: "100%",
					}}
				>
					{
						source.sentBy === this.state.login_id ? null :
							<View style={{ overflow: 'hidden', alignSelf: 'flex-end', marginBottom: 5, height: 26, width: 26, borderRadius: (26 / 2), resizeMode: 'stretch', marginLeft: 7 }}>
								<Image
									style={
										{
											height: '100%',
											width: '100%'
										}
									}
									source={{ uri: source.Avtar }}
								/>
							</View>

					}


					{

						source.Image == undefined ?
							<View>
								{/* {
                    source.item.sentBy === this.state.login_id ? 
                    <Text style={{ color:colors.dargrey, marginLeft: Width(2), width: Width(75), textAlign:'right', fontSize:8, fontFamily:fonts.Roboto_Regular}}> {source.item.date.split(",")[2]}</Text>
                    :
                  <Text style={{ color: colors.dargrey, marginLeft: Width(2), fontSize:8, fontFamily:fonts.Roboto_Regular}}> {source.item.Name}, {source.item.date.split(",")[2]}</Text>
                    }   */}
								{
									(prevmsgtime == '' || previmsgloginid != this.curmsgloginid || this.curmsgtime != prevmsgtime) ?
										source.sentBy === this.state.login_id ?
											<Text style={{ color: '#999999', marginLeft: Width(2), width: Width(75), textAlign: 'right', fontSize: 10, fontFamily: fonts.Roboto_Regular, marginTop: 10 }}> {moment.utc(source.date).local().format('hh:mm A')}</Text>
											:
											<Text style={{ color: '#999999', marginLeft: Width(2), fontSize: 10, fontFamily: fonts.Roboto_Regular, marginTop: 10 }}> {source.Name}, {moment.utc(source.date).local().format('hh:mm A')}</Text>
										:
										null
								}
								<View style={{
									backgroundColor:
										source.sentBy === this.state.login_id ? colors.appColor : "#fff",
									width: Width(75), borderRadius: 5,
									marginTop: Height(0.3),
									marginBottom: Height(0.3),
									padding: Width(3),
									marginLeft: Width(2),
									marginRight: Width(2)
								}}>
									{/* {
                    source.item.sentBy === this.state.login_id ? null :
                    <Text style={{ color: source.item.sentBy === this.state.login_id ? "#fff" : colors.black , marginBottom: 4 }}> {source.Name} </Text>
                }  */}

									<Text
										style={{
											color: source.sentBy === this.state.login_id ? "#fff" : colors.black,
											fontSize: FontSize(15),
											lineHeight: 20,
											fontFamily: fonts.Roboto_Regular,
										}}>
										{source.message}
									</Text>
									{/* {
                        source.Image == undefined ? null :
                        <Image source={{uri : source.Image }} style={{ height: 200 , width: 200 }} />
                    }                                                 */}
								</View>
							</View>
							:
							<View>
								{/* {
                    source.item.sentBy === this.state.login_id ? 
                    <Text style={{ color:colors.dargrey, marginLeft: Width(2), width: Width(75), textAlign:'right', fontSize:8, fontFamily:fonts.Roboto_Regular}}> {source.item.date.split(",")[2]}</Text>
                    :
                  <Text style={{ color: colors.dargrey, marginLeft: Width(2), fontSize:8, fontFamily:fonts.Roboto_Regular}}> {source.item.Name}, {source.item.date.split(",")[2]}</Text>
                    } */}
								{
									(prevmsgtime == '' || previmsgloginid != this.curmsgloginid || this.curmsgtime != prevmsgtime) ?
										source.sentBy === this.state.login_id ?
											<Text style={{ color: '#999999', marginLeft: Width(2), width: Width(75), textAlign: 'right', fontSize: 10, fontFamily: fonts.Roboto_Regular, marginTop: 10 }}> {moment.utc(source.date).local().format('hh:mm A')}</Text>
											:
											<Text style={{ color: '#999999', marginLeft: Width(2), fontSize: 10, fontFamily: fonts.Roboto_Regular, marginTop: 10 }}> {source.Name}, {moment.utc(source.date).local().format('hh:mm A')}</Text>
										: null
								}
								<View style={{
									backgroundColor: 'transparent',
									width: Width(75), borderRadius: 5,
									marginTop: Height(0.3),
									marginBottom: Height(0.3),
									padding: Width(3),
									marginLeft: Width(2),
									marginRight: Width(2)
								}}>
									{/* {
                    source.item.sentBy === this.state.login_id ? null :
                    <Text style={{ color: source.item.sentBy === this.state.login_id ? "#fff" : colors.black , marginBottom: 4 }}> {source.Name} </Text>
                }  */}
									{/* <Text
                        style={{
                            color: source.sentBy === this.state.login_id ? "#fff" : colors.black,
                            fontSize: FontSize(15),
                            lineHeight: 20,
                            fontFamily: fonts.Roboto_Regular,
                        }}>
                        {source.message}
                    </Text> */}
									{
										source.Image == undefined ? null :
											<Image source={{ uri: source.Image }} style={{ height: 200, width: '100%', borderRadius: 20 }} />
									}
								</View>
							</View>

					}

				</View>

				{
					i == this.state.Massages.length - 1 ?
						this.state.status.length != 0 ?
							this.state.login_id == this.state.status[0].id ? null :
								//<View style={{ marginLeft: Width(5),marginRight: Width(5),width: "100%"}}>  
								<View style={{ width: "100%", marginHorizontal: 7, flexDirection: 'row', alignItems: "flex-start", marginTop: 10 }}>
									{/* <View style={{ flexDirection: 'row',alignItems:"flex-start",width: "100%"}}> */}

									<Image
										style={[
											{
												alignSelf: 'flex-end',
												marginBottom: 5,
												height: 26,
												width: 26,
												borderRadius: (26 / 2),
												resizeMode: 'stretch',
											},
										]}
										source={{ uri: this.state.status[0].Avtar }}
									/>
									{/* <View style={{
                    
                      backgroundColor:"transparent",
                      width: Width(75), borderRadius: Height(3),
                      marginTop: Height(0.3),
                      marginBottom: Height(0.3),
                      padding: Width(3),
                      marginLeft: Width(2),
                      marginRight: Width(2)
                    }}> */}
									<View style={{ backgroundColor: 'transparent', width: Width(75), borderRadius: Height(3), marginTop: Height(0.3), marginBottom: Height(0.3), marginLeft: Width(2), marginRight: Width(2) }}>

										<Text
											style={{
												color: '#3A4759',
												fontSize: FontSize(12),
												lineHeight: 20,
												fontFamily: fonts.Roboto_Regular,
											}}>
											{
												this.state.status.length > 2 ? this.state.status[0].name + ' and ' + (this.state.status.length - 1) + ' others are typing...'
													: this.state.status.length == 2
														? ((this.state.status[0].id == this.state.login_id) || (this.state.status[1].id == this.state.login_id)) ? this.state.status[0].name + ' is typing...' : this.state.status[0].name + ' and ' + this.state.status[1].name + ' are typing...'
														: this.state.status[0].name + ' is typing...'
											}
										</Text>
									</View>
									{/* </View> */}
									{/* </View>   */}
								</View>
							: null
						:
						null
				}

			</View>
		);
	}

	_onMomentumScrollBegin = () => {
		this.setState({ is_textfield_focused: false })
		this.headertap = false
		//alert('Hiii')
		this.collapseHeader()
		Keyboard.dismiss()
	};

	_onScrollBeginDrag = () => {

		this.setState({ is_textfield_focused: false })
		this.headertap = false
		//alert('Hiii')
		this.collapseHeader()
		Keyboard.dismiss()
	}


	onPhotoClick(index) {
		//alert(index.Image) 
		this.props.navigation.navigate('ProfilePicView', { UserDetail: null, is_my_photo: false, photo: index.Image, is_chat_photo: true })
	}

	renderImagePreviiew()
	{
		return (
			<Modal isVisible = {this.state.isPhotoPreviewVisible} style = {{margin:0,backgroundColor:'#000'}}>
                {/* <View style={{top: 20, position: 'absolute', left: Width(5),width:35,height:35}}> */}
                    <TouchableOpacity onPress={() => { 
						//this.imagepickeropen = false
						this.setState({isPhotoPreviewVisible:false}, () => {
							setTimeout(() => { this.PhotoSheetAction(1) }, 1000)
						})
						}} style = {{width:35,height:35,justifyContent:'center',top: 35, position: 'absolute', left: Width(5),zIndex:2}}>
                        <Image source={white_left} style={{ tintColor: "white" }} />
                    </TouchableOpacity>
                {/* </View> */}
				<View style = {{flex:1, alignItems:'center', justifyContent:'center'}}>
					<Image style={{ height: '65%', width: '100%'}} source={{ uri: this.state.previewurl}} resizeMode = 'contain'/>
				</View>
				<TouchableOpacity onPress={() => { 
						
						this.setState({isPhotoPreviewVisible:false}, () => {
							this.imagepickeropen = false
							this.UploadEvent(this.state.previewurl)
						})
						}} style = {{width:Width(80),height:50,justifyContent:'center', alignItems:'center',bottom: 40,position: 'absolute',zIndex:2, alignSelf:'center'}}>
                        <Image source={btnsend} resizeMode = 'contain' />
                </TouchableOpacity>
        </Modal>
		)
	}

	render() {

		// this.previousdaydiff = ''
		// this.previmsgloginid = ''
		// this.prevmsgtime = ''

		const animatedStyle = {
			height: this.state.animationValue
		}

		var textstyles = { fontSize: FontSize(20), color: '#007AFF' };
		var cencelstyles = [textstyles, { fontWeight: 'bold' }];

		const { value, participants } = this.state;
		const myitems = this.state.item //this.props.navigation.getParam('item');


//		alert(JSON.stringify(myitems))


		OptionShow = []

		if (OptionShow.length == 0) {
			if (myitems.activity == '') {
				OptionShow.push('Suggest an Activity')
			}

			if (myitems.line_1 == '') {
				OptionShow.push('Suggest a Location')
			}

			if (myitems.start_date == '') {
				OptionShow.push('Suggest a Date & time')
			}

			OptionShow.push('Camera')
			OptionShow.push('Photo Gallery')
			OptionShow.push('Cancel')
		}

		PhotoOption = []

		if (PhotoOption.length == 0) {
			PhotoOption.push('Camera')
			PhotoOption.push('Photo Gallery')
			PhotoOption.push('Cancel')
		}


		// var OptionShow = [

		//     'Suggest an Activity'
		//   ,

		//    'Suggest a Location'
		//   ,

		//      'Suggest a Date & time'
		// ,

		//      'Camera'
		// ,

		//      'Photo Gallery'
		// , 

		//      'Cancel'

		//   ];



		return (
			<ImageBackground source={chatbg} style={Platform.OS == "ios" ? { width: '100%', height: '100%', resizeMode: 'center' } : { width: '100%', resizeMode: 'center', flex: 1 }}>
			<View style={{ flex: 1, backgroundColor: 'transparent' }} >
				
					<Toastt ref="toast"></Toastt>
					<Modal isVisible={!this.state.overlay_shown} style={{ margin: 0}} animationInTiming = {5}>
						<TouchableOpacity style = {{width:'100%',height:'100%',backgroundColor:'rgba(0,122,255,0.7)', justifyContent:'center'}} onPress = { () => this.onOverlayTapped()} activeOpacity = {0.9}>
							<Text style = {{fontSize:24,fontFamily:fonts.Raleway_Bold, width:Width(50), color:'#fff', alignSelf:'flex-start',marginLeft:16}} numberOfLines = {2}>
								Swipe right from
							</Text>
							<Text style = {{fontSize:24,fontFamily:fonts.Raleway_Bold, width:Width(50), color:'#fff', alignSelf:'flex-start',marginLeft:16}} numberOfLines = {2}>
								edge to go back
							</Text>
							
						</TouchableOpacity>
					</Modal>
					
					{/* <BackgroundImage >  */}

					{/* <SafeAreaView style={{ backgroundColor: this.state.safebackgroundColor }} /> */}
					{this.renderTopBar()}
					{/* <Content> */}
					{/* <ScrollView 
                        style = {{backgroundColor:'transparent', marginBottom:Platform.OS == 'ios' ? 55 : 0}}
                        //stickyHeaderIndices={[1]}
                        //scrollEnabled = {Platform.OS == 'ios' ? false : true}
                        scrollEnabled = {false}
                        keyboardShouldPersistTaps = 'always'
                        onMomentumScrollBegin={this._onMomentumScrollBegin}
                        ref={(ref) => { this.scrollView = ref; }}
                    onContentSizeChange={ () => {          
                        this.scrollView.scrollToEnd( {animated: false })
                    }} 
                    onScroll={(event) => { 
                        // if(event.nativeEvent.contentOffset.y > 161)
                        // {
                        //     this.setState({backgroundColor: '#41199B' , color: '#fff', Icon: white_left, safebackgroundColor:'#41199B',headerfontsize:13}) 
                        // }
                        // if(event.nativeEvent.contentOffset.y < 50)
                        // {
                        //     this.setState({backgroundColor: '#fff' , color: '#41199B',Icon: blue_left, safebackgroundColor:'#fff',headerfontsize:20}) 
                        // }
                        // alert(event.nativeEvent.contentOffset.y) 
                       }}     
                        >  */}
					<View style={Platform.OS == "ios" ? { backgroundColor: 'transparent', marginBottom: 55 } : { backgroundColor: 'transparent', marginBottom: 0, flex: 1 }}>
						<Animated.View style={[{ backgroundColor: '#fff' }, animatedStyle]}>
							{/* <Animated.View style={this.firsttime == true ? [{backgroundColor:'#fff'},animatedStyle,{height:(this.state.participants.length > 0 || this.state.item.is_guest == 1 ) ? (Platform.OS == 'ios' ? Height(14.5) :Height(15.5))  : (Platform.OS == 'ios' ? Height(10.5) : Height(13.5))}] : [{backgroundColor:'#fff'},animatedStyle]}> */}
							<TouchableOpacity
								style={{ flexDirection: 'row', marginHorizontal: Width(4), marginBottom: Height(0.5), marginTop:Height(1.5)}}>
								<Image resizeMode="contain" style={{ alignSelf: 'center', opacity: this.state.viewopacity }} source={chai_cup} />
								{
									myitems.activity == '' ?
										<Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(14), color: '#358AF6' }}
											onPress={myitems.total_activity_suggestions == 0 ? () => this.props.navigation.navigate('Activity', { plan_id: myitems.plan_id, other_id: myitems.login_id, is_from_event: true, is_direct: false, item: this.state.item, title_header: this.state.title_header }) : () => this.props.navigation.navigate('SuggestActivity', { other_id: myitems.login_id, plan_id: myitems.plan_id, is_from_event: true, item: this.state.item, title_header: this.state.title_header })}>{myitems.total_activity_suggestions == 0 ? 'Suggest an activity' : 'View activity suggestions'}</Text>
										:
										// <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(14), color: colors.dargrey }} onPress={() => this.props.navigation.navigate('SuggestActivity', { other_id: myitems.login_id })}>
										<Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(14), color: colors.dargrey }}>
											{
												myitems.activity.includes('Wanna') ? myitems.activity.substr(6).slice(0, -1) : myitems.activity
											}
										</Text>
								}
							</TouchableOpacity>
							<View style={{ flexDirection: 'row', marginHorizontal: Width(4), marginBottom: Height(0.5), justifyContent: 'space-between'}}>

								<View
									style={{ flexDirection: 'row', marginTop: Height(1), borderColor: '#EEEEEE', borderWidth: 1, height: Height(6), width: Width(44), borderRadius: 5,opacity: this.state.viewopacity }}>
									<Image resizeMode="contain" style={{ tintColor: colors.whatFontColor, marginTop: 13, marginLeft: 10, opacity: this.state.viewopacity }} source={location} />
									{

										myitems.line_1 == '' ?
											<View style={{ opacity: this.state.viewopacity, justifyContent: 'center' }}>
												<Text style={{ paddingLeft: Width(3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(14), color: '#358AF6', opacity: this.state.viewopacity, width: Width(37) }} onPress={myitems.total_location_suggestions == 0 ? () => this.props.navigation.navigate('Location', { plan_id: myitems.plan_id, other_id: myitems.login_id, is_from_event: true, is_direct: false, item: this.state.item }) : () => this.props.navigation.navigate('SuggestLocation', { other_id: myitems.login_id, plan_id: myitems.plan_id, is_from_event: true, item: this.state.item })} >
													{myitems.total_location_suggestions == 0 ? 'Suggest a location' : 'View location suggestions'}
												</Text>
												{
													myitems.total_location_suggestions == 0 ?
														<Text style={{ fontSize: FontSize(10), fontFamily: fonts.Roboto_Regular, color: '#c1c1c1', paddingLeft: Width(3.3), paddingRight: Width(1.3), width: Width(37), opacity: this.state.viewopacity }} numberOfLines={1}>Powered By Google Maps</Text>
														: null
												}

											</View>
											:
											<TouchableOpacity style={{ opacity: this.state.viewopacity, justifyContent: 'center' }} onPress={() => this.onLocationClicked()}>
												<Text style={{ paddingLeft: Width(3), paddingRight: Width(2.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey, width: Width(37), opacity: this.state.viewopacity }} numberOfLines={1}>{myitems.line_1}</Text>
												<Text style={{ fontSize: FontSize(10), fontFamily: fonts.Roboto_Regular, color: '#c1c1c1', paddingLeft: Width(2), paddingRight: Width(2.3), width: Width(37), opacity: this.state.viewopacity }} numberOfLines={1}>{myitems.line_2}</Text>
											</TouchableOpacity>
									}

								</View>

								<View
									style={{ flexDirection: 'row', marginTop: Height(1), borderColor: '#EEEEEE', borderWidth: 1, height: Height(6), width: Width(44), borderRadius: 5,opacity: this.state.viewopacity }}>
									<Image resizeMode="contain" style={{ marginTop: 13, marginLeft: 10, opacity: this.state.viewopacity }} source={calendar} />
									{
										myitems.start_date == '' ?

											<Text style={{ paddingLeft: Width(3), paddingRight: Width(2.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(14), color: '#358AF6', marginTop: 5, width: Width(37), opacity: this.state.viewopacity }} onPress={myitems.total_datetime_suggestions == 0 ? () => this.props.navigation.navigate('DateTime', { plan_id: myitems.plan_id, other_id: myitems.login_id, is_from_event: true, is_direct: false, item: this.state.item }) : () => this.props.navigation.navigate('SuggestDateTime', { other_id: myitems.login_id, plan_id: myitems.plan_id, is_from_event: true, item: this.state.item })}>{myitems.total_datetime_suggestions == 0 ? 'Suggest a date & time' : 'View date & time suggestions'}</Text>
											:
											<TouchableOpacity style={{ opacity: this.state.viewopacity, justifyContent: 'center' }} onPress={() => this.onDateTimeClicked()}>
												{
													this.state.event_ends_sameday == true ?
														<Text style={{ paddingLeft: Width(3), paddingRight: Width(2.3), fontFamily: fonts.Roboto_Medium, fontSize: FontSize(13), color: colors.dargrey, width: Width(37), opacity: this.state.viewopacity }}>{this.state.Final_date}</Text>
														:
														<View>
															<View style={{ flexDirection: 'row', paddingLeft: Width(3), paddingRight: Width(1), opacity: this.state.viewopacity }}>
																<Text style={{ fontFamily: fonts.Roboto_Medium, fontSize: FontSize(13), color: colors.dargrey, opacity: this.state.viewopacity }}>{this.state.Final_date}</Text>
																<Text style={{ fontFamily: fonts.Roboto_Regular, fontSize: FontSize(13), color: colors.dargrey, opacity: this.state.viewopacity }}>{this.state.Final_StartTime} -</Text>
															</View>
															<View style={{ flexDirection: 'row', paddingLeft: Width(3), paddingRight: Width(1) }}>
																<Text style={{ fontFamily: fonts.Roboto_Medium, fontSize: FontSize(13), color: colors.dargrey, opacity: this.state.viewopacity }}>{this.state.Final_Enddate}</Text>
																<Text style={{ fontFamily: fonts.Roboto_Regular, fontSize: FontSize(13), color: colors.dargrey, opacity: this.state.viewopacity }}>{this.state.Final_EndTime}</Text>
															</View>
														</View>
												}

												{
													this.state.event_ends_sameday == true ?
														<Text style={{ paddingLeft: Width(3), paddingRight: Width(1), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(13), color: colors.dargrey, width: Width(37), opacity: this.state.viewopacity }}>{this.state.Final_Enddate}</Text>
														: null
												}

											</TouchableOpacity>
									}
								</View>
							</View>

							<View style={{ flexDirection: 'row', alignItems: 'center', opacity: this.state.viewopacity, marginTop: Height(0.5) }}>

								{/* { (this.state.participants.length != 0)?  */}

								<View style={{ flexDirection: 'row', marginHorizontal: Width(4), marginBottom: Height(1), alignItems: 'center', width: '95%' }}>
									<View style={{ height: Height(3), width: Height(3), borderRadius: Height(3) }}>
										{/* <Image style={{ height: '100%', width: '100%' , borderRadius:Height(1.5)}} source={this.state.participants[0].photo != '' ? {uri: this.state.participants[0].photo}:avtar } /> */}
										<Image style={{ height: '100%', width: '100%', borderRadius: Height(1.5) }} source={myitems.photo != '' ? { uri: myitems.photo } : avtar} />
									</View>
									<TouchableOpacity onPress={() => this.onUsersPressed()}>
										<View style={{ height: Height(3), width: Height(3), borderRadius: Height(1.5), marginLeft: -11, zIndex: 1 }}>
											<Image style={{ height: '100%', width: '100%', borderRadius: Height(1.5) }} source={require('../../../assets/images/dots.png')} />
										</View>
									</TouchableOpacity>
									{
										myitems.is_guest == 1 ? <Text style={{ color: colors.blue, fontSize: FontSize(13), fontFamily: fonts.Roboto_Regular, position: 'absolute', left: Width(12) }} onPress={() => this.props.navigation.navigate('InviteFriends', { item: myitems, plan_id: myitems.plan_id, is_my_event: myitems.login_id == this.state.login_id ? true : false, plan_creator_id: myitems.login_id })}>Invite Friends</Text>
											:
											this.state.item.login_id == this.state.login_id ? 
											<Text style={{ color: colors.blue, fontSize: FontSize(13), fontFamily: fonts.Roboto_Regular, position: 'absolute', left: Width(12) }} onPress={() => this.props.navigation.navigate('InviteFriends', { item: myitems, plan_id: myitems.plan_id, is_my_event: myitems.login_id == this.state.login_id ? true : false, plan_creator_id: myitems.login_id })}>Invite Friends</Text>
											: null
									}

									{
										this.state.item.description == null || this.state.item.description == undefined || this.state.item.description == '' ?
										// <TouchableOpacity style = {{width:Width(15),height:28, borderWidth:1,borderColor:'#fff',borderRadius:14, position:'absolute',right:Width(5), alignItems:'center',justifyContent:'center'}}>
										// <Text style={{ color: colors.blue, fontSize: FontSize(13), fontFamily: fonts.Roboto_Regular, textAlign:'center'}} >    </Text>
										// </TouchableOpacity>
										null
										:
										<TouchableOpacity style = {{width:Width(15),height:28, borderWidth:1,borderColor:'#EEEEEE',borderRadius:14, position:'absolute',right:Width(5), alignItems:'center',justifyContent:'center'}}
										onPress={() => { this.props.navigation.navigate('EventDescription',{plan_type:this.state.item.plan_type,description:this.state.item.description,is_from_eventdetails:true}) }}
										>
											<Text style={{ color: colors.blue, fontSize: FontSize(13), fontFamily: fonts.Roboto_Regular, textAlign:'center'}} >+ Info</Text>
										</TouchableOpacity>
									}
									

								</View>
								{/* :
                                  //marginBottom:Height(1.5), marginTop:Height(0.5)
                                    myitems.is_guest == 1 ? 
                                    <View style={{ flexDirection: 'row',marginHorizontal:Width(4),marginBottom:Height(1), alignItems:'center',width:'95%',height:Height(3)}}> 
                                      <Text style={{ color: colors.blue, fontSize: FontSize(13), fontFamily: fonts.Roboto_Regular}} onPress = {() => this.props.navigation.navigate('InviteFriends',{item:myitems,plan_id:myitems.plan_id,is_my_event: myitems.login_id == this.state.login_id ? true :false,plan_creator_id:myitems.login_id})}>Invite Friends</Text>
                                    </View>
                                    : null
                                }  */}
								{/* <Text style={{ color: colors.blue, fontSize: FontSize(13), fontFamily: fonts.Roboto_Regular, marginHorizontal: Width(4), marginBottom:Height(1.7) }} onPress = {() => this.props.navigation.navigate('InviteFriends')}>Invite Friends</Text> */}
							</View>
							{/* <View style={{ height: Height(0.3), backgroundColor: '#EFEFF4', marginTop: Height(2) }} /> */}

						</Animated.View>

						{
							this.state.Edit == false && (myitems.login_id != this.state.login_id) ?
								<View
									style={{ backgroundColor: 'white' }}>
									<View style={{ height: Height(0.15), backgroundColor: '#EFEFF4', marginHorizontal: Width(4) }} />
									<View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: "center", marginBottom: Height(1), marginTop: Height(1), height: Height(4) }}>

										{/* {(item.is_going == 0 && item.is_interested == 0 && (item.activity != '' && item.line_1 != '' && item.start_date != ''))? */}
										{((myitems.activity != '' || myitems.line_1 != '')) ?
											<View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>

												{(this.state.is_going == 1 || myitems.is_going == 1) ?
													<View style={{ flexDirection: 'row' }}>
														<TouchableOpacity onPress={() => this.goToNoGoing(myitems)} style={{ flexDirection: 'row', alignItems: 'center' }}>
															<Image source={going1} />
															<Text style={{ color: colors.pink, fontFamily: fonts.Roboto_Regular, marginLeft: Width(1), fontSize: FontSize(16) }} >Going</Text>
														</TouchableOpacity>
													</View>
													:
													<View style={{ flexDirection: 'row' }}>
														<TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => this.goToGoing(myitems)}><Image source={going} />
															<Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1), fontSize: FontSize(16) }}>Going</Text>
														</TouchableOpacity>
													</View>

												}
												<View style={{ width: Width(10) }} />

												{(this.state.is_interested == 1 || myitems.is_interested == 1)
													?
													<View style={{ flexDirection: 'row' }}>
														<TouchableOpacity onPress={() => this.goToNoIntersted(myitems)} style={{ flexDirection: 'row', alignItems: 'center' }} >
															<Image source={circle_favorite} />
															<Text style={{ marginLeft: Width(1), color: colors.pink, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Interested</Text>
														</TouchableOpacity>
													</View>
													:
													<View style={{ flexDirection: 'row' }}>
														<TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => this.goToIntersted(myitems)}><Image source={black_heart} />
															<Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1), fontSize: FontSize(16) }}>Interested</Text>
														</TouchableOpacity>
													</View>
												}
												<View style={{ width: Width(10) }} />

												<View style={{ flexDirection: 'row' }}>
													<TouchableOpacity
														onPress={() => this.goToShare(myitems)}
														style={{ flexDirection: 'row', alignItems: 'center' }}>
														< Image source={share} />
														<Text style={{ color: colors.dargrey, fontFamily: fonts.Raleway_Regular, marginLeft: Width(1), fontSize: FontSize(16) }} >Share</Text>
													</TouchableOpacity>
												</View>
											</View>
											:

											<View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
												
												{ (this.state.is_interested == 1 || myitems.is_interested == 1) ?
													<View style={{ flexDirection: 'row' }}>
														<TouchableOpacity onPress={() => this.goToNoIntersted(myitems)} style={{ flexDirection: 'row', alignItems: 'center' }} >
															<Image source={circle_favorite} />
															<Text style={{ marginLeft: Width(1), color: colors.pink, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Interested</Text>
														</TouchableOpacity>
													</View>
													:
													<View style={{ flexDirection: 'row' }}>
														<TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => this.goToIntersted(myitems)}><Image source={black_heart} />
															<Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1), fontSize: FontSize(16) }}>Interested</Text>
														</TouchableOpacity>
													</View>
												}
												<View style={{ width: Width(10) }} />
												<View style={{ flexDirection: 'row' }}>
													<TouchableOpacity
														onPress={() => this.goToShare(myitems)}
														style={{ flexDirection: 'row', alignItems: 'center' }}>
														< Image source={share} />
														<Text style={{ color: colors.dargrey, fontFamily: fonts.Raleway_Regular, marginLeft: Width(1), fontSize: FontSize(16) }} >Share</Text>
													</TouchableOpacity>
												</View>
											</View>

										}
									</View>
								</View> :
								<View
									style={{ backgroundColor: '#fff' }}>
									<View style={{ height: Height(0.15), backgroundColor: '#EFEFF4', marginHorizontal: Width(4) }} />
									<View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: "center", marginBottom: Height(1), marginTop: Height(1), height: Height(4) }}>
										<View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: "center" }}>
											<TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => this.editPlan()}><Image source={Edit} />
												<Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1.5) }}>Edit</Text>
											</TouchableOpacity>
											<View style={{ width: Width(8) }} />
											<TouchableOpacity
												onPress={() => {
													Alert.alert(
														'Cancel Event',
														'Guests will be notified that this event was canceled',
														[
															{
																text: 'Cancel',
																onPress: () => console.log('Cancel Pressed'),
																style: 'cancel',
															},
															{ text: 'OK', onPress: this.deletePlan },
														],
														{ cancelable: false },
													);
												}}
												style={{ flexDirection: 'row', alignItems: 'center' }} ><Image source={Delete} />
												<Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1.5) }}>Cancel</Text></TouchableOpacity>
											<View style={{ width: Width(8) }} />
											<TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => this.goToShare(myitems)}>
												<Image source={share} />
												<Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1.5) }}>Share</Text>
											</TouchableOpacity>
										</View>
									</View>
								</View>
						}

						{
							(Platform.OS == 'ios' || Platform.OS == 'android') ?

								<ScrollView
									nestedScrollEnabled={true}
									onMomentumScrollBegin={this._onMomentumScrollBegin}
									ref={(ref) => { this.flat_list = ref; }}//36.5
									//style = {Platform.OS == 'ios' ? {backgroundColor:'transparent', marginTop:2, height: this.state.is_textfield_focused ? Height(36.5) : (this.state.viewState ? Height(58.5) : Height(71))} : {backgroundColor:'transparent', marginTop:2, height:Height(58.5)}}
									style={Platform.OS == 'ios' ?
										{ backgroundColor: 'transparent', marginTop: 0, height: this.state.is_textfield_focused ? Height(40) : (this.state.viewState ? (this.state.participants.length != 0 || this.state.item.is_guest == 1) ? (screenHeight > 812 ? Height(59) : Height(57.5)) : (screenHeight > 812 ? Height(64.5) : Height(61.5)) : (screenHeight > 812 ? Height(74.5) : Height(72.8))) }
										:
										{}
									}
									contentContainerStyle={Platform.OS == 'ios' ? { alignItems: 'center' } : { alignItems: 'center', flexGrow: 1 }}
									keyboardDismissMode='on-drag'
									onContentSizeChange={() => {
										if (this.flat_list != null)
											if (this.flat_list != null) {
												if (isFirstTime) {
													this.flat_list.scrollToEnd({ animated: false })
													setTimeout(() => { isFirstTime = false }, 4000)
												}
												else {
													this.flat_list.scrollToEnd({ animated: true })

												}

											}
									}}
								>

									{

										this.state.Massages.length > 0 ?

											this.state.Massages.map((source, i) => {

												var datelabeltext = moment.utc(source.date).local().calendar()
												this.curdaydiff = datelabeltext.split(' ')[0]
												this.curmsgtime = moment.utc(source.date).local().format('hh:mm A')
												this.curmsgloginid = source.sentBy
												var prevmsgtime = ''
												var previmsgloginid = ''
												var previousdaydiff = ''

												//console.log('Messageday: '+this.curdaydiff)

												if (!this.weekdays.includes(this.curdaydiff)) {
													this.curdaydiff = moment.utc(source.date).local().format('LL')
												}

												var previoussource = ''

												if (i > 0) {

													previoussource = this.state.Massages[i - 1]
													var prevdatelabeltext = moment.utc(previoussource.date).local().calendar()
													previousdaydiff = prevdatelabeltext.split(' ')[0]
													prevmsgtime = moment.utc(previoussource.date).local().format('hh:mm A')
													previmsgloginid = previoussource.sentBy

													if (!this.weekdays.includes(previousdaydiff)) {
														previousdaydiff = moment.utc(previoussource.date).local().format('LL')
													}
												}

												//console.log('CurrentDiff: '+this.curdaydiff+'   '+this.previousdaydiff +'   i: '+i)
												return (
													<View
														style={{
															// marginLeft: Width(5),
															// marginRight: Width(5),
															width: "100%",
															backgroundColor: 'transparent'
														}}>
														{

															(previousdaydiff == '' || this.curdaydiff != previousdaydiff) ?
																<View style={{ alignItems: 'center', justifyContent: 'center', paddingHorizontal: 15, backgroundColor: '#b1b6bb', height: Height(2.5), borderRadius: Height(1.25), alignSelf: 'center', marginVertical: 5 }}>
																	<Text
																		style={{
																			textAlign: "center",
																			color: 'white',
																			fontSize: 12,
																			fontFamily: fonts.Roboto_Regular,
																			height: Height(2),
																			marginTop: 2,
																			marginBottom: Platform.OS == 'android' ? 4 : 0
																		}}
																	>
																		{
																			this.curdaydiff
																		}
																	</Text>
																</View>

																:
																null
														}

														<View
															style={{
																flexDirection:
																	source.sentBy === this.state.login_id
																		? null
																		: 'row',

																alignItems:
																	source.sentBy === this.state.login_id
																		? "flex-end"
																		: "flex-start",
																width: "100%",
															}}
														>
															{
																source.sentBy === this.state.login_id ? null :
																	Platform.OS == 'ios' ?
																		<Image
																			style={[
																				{
																					alignSelf: 'flex-end',
																					marginBottom: 5,
																					height: 26,
																					width: 26,
																					borderRadius: (26 / 2),
																					resizeMode: 'stretch',
																					marginLeft: 7
																				},
																			]}
																			source={{ uri: source.Avtar }}
																		/>
																		:
																		<View style={{ overflow: 'hidden', alignSelf: 'flex-end', marginBottom: 5, height: 26, width: 26, borderRadius: (26 / 2), resizeMode: 'stretch', marginLeft: 7 }}>
																			<Image
																				style={
																					{
																						height: '100%',
																						width: '100%'
																					}
																				}
																				source={{ uri: source.Avtar }}
																			/>
																		</View>
															}


															{

																source.Image == undefined ?
																	<View>
																		{/* {
                                              source.item.sentBy === this.state.login_id ? 
                                              <Text style={{ color:colors.dargrey, marginLeft: Width(2), width: Width(75), textAlign:'right', fontSize:8, fontFamily:fonts.Roboto_Regular}}> {source.item.date.split(",")[2]}</Text>
                                              :
                                            <Text style={{ color: colors.dargrey, marginLeft: Width(2), fontSize:8, fontFamily:fonts.Roboto_Regular}}> {source.item.Name}, {source.item.date.split(",")[2]}</Text>
                                              }   */}
																		{
																			(prevmsgtime == '' || previmsgloginid != this.curmsgloginid || this.curmsgtime != prevmsgtime) ?
																				source.sentBy === this.state.login_id ?
																					<Text style={{ color: '#999999', marginLeft: Width(2), width: Width(75), textAlign: 'right', fontSize: 10, fontFamily: fonts.Roboto_Regular, marginTop: 10 }}> {moment.utc(source.date).local().format('hh:mm A')}</Text>
																					:
																					<Text style={{ color: '#999999', marginLeft: Width(2), fontSize: 10, fontFamily: fonts.Roboto_Regular, marginTop: 10 }}> {source.Name}, {moment.utc(source.date).local().format('hh:mm A')}</Text>
																				:
																				null
																		}
																		<View style={{
																			backgroundColor:
																				source.sentBy === this.state.login_id ? colors.appColor : "#fff",
																			width: Width(75), borderRadius: 5,
																			marginTop: Height(0.3),
																			marginBottom: Height(0.5),
																			padding: Width(3),
																			marginLeft: Width(2),
																			marginRight: Width(2),
																			shadowColor: "#000000",
																			shadowOffset: {
																				width: 0.5,
																				height: 0.5,
																			},
																			shadowOpacity: 0.16,
																			shadowRadius: 3
																		}}>
																			{/* {
                                              source.item.sentBy === this.state.login_id ? null :
                                              <Text style={{ color: source.item.sentBy === this.state.login_id ? "#fff" : colors.black , marginBottom: 4 }}> {source.Name} </Text>
                                          }  */}

																			<Text
																				style={{
																					color: source.sentBy === this.state.login_id ? "#fff" : colors.black,
																					fontSize: FontSize(15),
																					lineHeight: 20,
																					fontFamily: fonts.Roboto_Regular,
																				}}>
																				{source.message}
																			</Text>
																			{/* {
                                                  source.Image == undefined ? null :
                                                  <Image source={{uri : source.Image }} style={{ height: 200 , width: 200 }} />
                                              }                                                 */}
																		</View>
																	</View>
																	:
																	<View>
																		{/* {
                                              source.item.sentBy === this.state.login_id ? 
                                              <Text style={{ color:colors.dargrey, marginLeft: Width(2), width: Width(75), textAlign:'right', fontSize:8, fontFamily:fonts.Roboto_Regular}}> {source.item.date.split(",")[2]}</Text>
                                              :
                                            <Text style={{ color: colors.dargrey, marginLeft: Width(2), fontSize:8, fontFamily:fonts.Roboto_Regular}}> {source.item.Name}, {source.item.date.split(",")[2]}</Text>
                                              } */}
																		{
																			(prevmsgtime == '' || previmsgloginid != this.curmsgloginid || this.curmsgtime != prevmsgtime) ?
																				source.sentBy === this.state.login_id ?
																					<Text style={{ color: '#999999', marginLeft: Width(2), width: Width(75), textAlign: 'right', fontSize: 10, fontFamily: fonts.Roboto_Regular, marginTop: 10 }}> {moment.utc(source.date).local().format('hh:mm A')}</Text>
																					:
																					<Text style={{ color: '#999999', marginLeft: Width(2), fontSize: 10, fontFamily: fonts.Roboto_Regular, marginTop: 10 }}> {source.Name}, {moment.utc(source.date).local().format('hh:mm A')}</Text>
																				: null
																		}
																		<View style={{
																			backgroundColor: source.sentBy === this.state.login_id ? colors.appColor : '#fff',
																			width: Width(75), borderRadius: 5,
																			marginTop: Height(0.3),
																			marginBottom: Height(0.3),
																			padding: Width(1),
																			marginLeft: Width(2),
																			marginRight: Width(2),

																		}}>
																			{/* {
                                              source.item.sentBy === this.state.login_id ? null :
                                              <Text style={{ color: source.item.sentBy === this.state.login_id ? "#fff" : colors.black , marginBottom: 4 }}> {source.Name} </Text>
                                          }  */}
																			{/* <Text
                                                  style={{
                                                      color: source.sentBy === this.state.login_id ? "#fff" : colors.black,
                                                      fontSize: FontSize(15),
                                                      lineHeight: 20,
                                                      fontFamily: fonts.Roboto_Regular,
                                                  }}>
                                                  {source.message}
                                              </Text> */}
																			{
																				source.Image == undefined ? null :
																					<TouchableOpacity onPress={() => this.onPhotoClick(source)}>
																						<Image source={{ uri: source.Image }} style={{ height: 200, width: '100%', borderRadius: 5 }} resizeMode = 'cover' />
																					</TouchableOpacity>
																			}
																		</View>
																	</View>

															}

														</View>
														{
															i == this.state.Massages.length - 1 ?

																this.state.status.length != 0 ?
																	this.state.login_id == this.state.status[0].id ? null :
																		//<View style={{ marginLeft: Width(5),marginRight: Width(5),width: "100%"}}>  
																		<View style={{ width: "100%", marginHorizontal: 7, flexDirection: 'row', alignItems: "flex-start", marginTop: 10 }}>
																			{/* <View style={{ flexDirection: 'row',alignItems:"flex-start",width: "100%"}}> */}
																			{
																				Platform.OS == 'ios' ?
																					<Image
																						style={[
																							{
																								alignSelf: 'flex-end',
																								marginBottom: 5,
																								height: 26,
																								width: 26,
																								borderRadius: (26 / 2),
																								resizeMode: 'stretch',
																								marginLeft: 10
																							},
																						]}
																						source={{ uri: this.state.status[0].Avtar }}
																					/>
																					:

																					<View style={{ overflow: 'hidden', alignSelf: 'flex-end', marginBottom: 5, height: 26, width: 26, borderRadius: (26 / 2), resizeMode: 'stretch' }}>
																						<Image
																							style={
																								{
																									height: '100%',
																									width: '100%'
																								}
																							}
																							source={{ uri: this.state.status[0].Avtar }}
																						/>
																					</View>
																			}

																			<View style={{ backgroundColor: 'transparent', width: Width(75), borderRadius: Height(3), marginTop: Height(0.3), marginBottom: Height(0.3), marginLeft: Width(2), marginRight: Width(2) }}>

																				<Text
																					style={{
																						color: '#3A4759',
																						fontSize: FontSize(12),
																						lineHeight: 20,
																						fontFamily: fonts.Roboto_Regular,
																					}}>
																					{
																						this.state.status.length > 2 ? this.state.status[0].name + ' and ' + (this.state.status.length - 1) + ' others are typing...'
																							: this.state.status.length == 2
																								? ((this.state.status[0].id == this.state.login_id) || (this.state.status[1].id == this.state.login_id)) ? this.state.status[0].name + ' is typing...' : this.state.status[0].name + ' and ' + this.state.status[1].name + ' are typing...'
																								: this.state.status[0].name + ' is typing...'

																					}
																				</Text>
																			</View>
																		</View>
																	: null
																:
																null
														}
													</View>
												);
											})

											:

											this.state.status.length != 0 ?
												this.state.login_id == this.state.status[0].id ? 
												Platform.OS == 'android' ?
												<View style = {{margin:0, width:'100%', height:Height(80)}}>
												</View>
												:null
												:
													//<View style={{ marginLeft: Width(5),marginRight: Width(5),width: "100%"}}>  
													
													
															<View style={{ width: "100%", marginHorizontal: 7, flexDirection: 'row', alignItems: "flex-start", marginTop: 10}}>
														{/* <View style={{ flexDirection: 'row',alignItems:"flex-start",width: "100%"}}> */}

														{
															Platform.OS == 'ios' ?

																<Image
																	style={[
																		{
																			alignSelf: 'flex-end',
                                                        					marginBottom: 5 ,
																			height: 26,
																			width: 26,
																			borderRadius: (26 / 2),
																			resizeMode: 'stretch',
																			marginLeft: 10
																		},
																	]}
																	source={{ uri: this.state.status[0].Avtar }}
																/>

																:

																<View style={{ overflow: 'hidden', alignSelf: 'flex-end', marginTop: 5, height: 26, width: 26, borderRadius: (26 / 2), resizeMode: 'stretch' }}>
																	<Image
																		style={
																			{
																				height: '100%',
																				width: '100%'
																			}
																		}
																		source={{ uri: this.state.status[0].Avtar }}
																	/>
																</View>
														}

														<View style={{ backgroundColor: 'transparent', width: Width(75), borderRadius: Height(3), marginTop: Height(0.3), marginBottom: Height(0.3), marginLeft: Width(2), marginRight: Width(2)}}>
															<Text
																style={{
																	color: '#3A4759',
																	fontSize: FontSize(12),
																	lineHeight: 20,
																	fontFamily: fonts.Roboto_Regular,
																}}>
																{
																	this.state.status.length > 2 ? this.state.status[0].name + ' and ' + (this.state.status.length - 1) + ' others are typing...'
																		: this.state.status.length == 2
																			? ((this.state.status[0].id == this.state.login_id) || (this.state.status[1].id == this.state.login_id)) ? this.state.status[0].name + ' is typing...' : this.state.status[0].name + ' and ' + this.state.status[1].name + ' are typing...'
																			: this.state.status[0].name + ' is typing...'
																}
															</Text>
														</View>
													</View>
													
												:
												Platform.OS == 'android' ?
												<View style = {{margin:0, width:'100%', height:Height(80)}}>
													
												</View>
												: null

									}
								</ScrollView>

								:
								this.state.Massages.length > 0 ?

									<FlatList data={this.state.Massages} extraData={this.state.Massages}
										style={{ width: Width(100), backgroundColor: 'transparent', backgroundColor: 'red' }}
										scrollEnabled={true}
										ref={(ref) => { this.flat_list = ref; }}
										renderItem={
											({ item, index }) => this.renderItem(item, index)
										} />
									:

									this.state.status.length != 0 ?
										this.state.login_id == this.state.status[0].id ? null :
											//<View style={{ marginLeft: Width(5),marginRight: Width(5),width: "100%"}}>  
											<View style={{ width: "100%", marginHorizontal: 7, flexDirection: 'row', alignItems: "flex-start", marginTop: 10 }}>
												{/* <View style={{ flexDirection: 'row',alignItems:"flex-start",width: "100%"}}> */}
												<Image
													style={[
														{
															alignSelf: 'flex-end',
															marginBottom: 5,
															height: 26,
															width: 26,
															borderRadius: (26 / 2),
															resizeMode: 'stretch',
															marginLeft: 10
														},
													]}
													source={{ uri: this.state.status[0].Avtar }}
												/>

												<View style={{ backgroundColor: 'transparent', width: Width(75), borderRadius: Height(3), marginTop: Height(0.3), marginBottom: Height(0.3), marginLeft: Width(2), marginRight: Width(2) }}>

													<Text
														style={{
															color: '#3A4759',
															fontSize: FontSize(12),
															lineHeight: 20,
															fontFamily: fonts.Roboto_Regular,
														}}>
														{
															this.state.status.length > 2 ? this.state.status[0].name + ' and ' + (this.state.status.length - 1) + ' others are typing...'
																: this.state.status.length == 2
																	? ((this.state.status[0].id == this.state.login_id) || (this.state.status[1].id == this.state.login_id)) ? this.state.status[0].name + ' is typing...' : this.state.status[0].name + ' and ' + this.state.status[1].name + ' are typing...'
																	: this.state.status[0].name + ' is typing...'
														}
													</Text>
												</View>
											</View>
										:
										null
						}

						{this.renderDeleteModal()}
						{this.renderLocationModal()}
						{this.renderTimeModal()}
						{this.renderImagePreviiew()}

					</View>
					{/* </ScrollView> */}
					{/* </Content> */}

					{
						Platform.OS == 'ios' ?
							(!this.imagepickeropen) && <InputAccessoryView>
								<View style={styles.typeBox}>
									<TouchableOpacity style={{ marginLeft: 10, marginTop: 2 }} onPress={this.showActionSheet}>
										<AntDesign name='plus' color={colors.pink} size={22} />
									</TouchableOpacity>
									<View style={{ marginLeft: Width(3), flexDirection: 'row', backgroundColor: '#F2F4F7', width: Width(76), marginHorizontal: '1%', borderRadius: Height(3), alignItems: 'center', borderWidth: 0.3, borderColor: '#EFEFF4', marginTop: 8, marginBottom: 8 }}>
										<TextInput
											// inputAccessoryViewID = {inputAccessoryViewID}
											ref={(ref) => { this.messagefield = ref; }}
											onFocus={this.onInputFocus}
											onBlur={this.onInputBlur}
											value={this.state.Msg}
											onContentSizeChange={(event) => {
												this.setState({
													inputheight: event.nativeEvent.contentSize.height,
												});
											}}
											//style={{ flex: 1, left: 10, position: 'absolute', fontSize: FontSize(15), fontFamily: fonts.Poppins_Light }}
											style={this.state.inputheight < Height(6) ? { marginLeft: 15, marginRight: 5, fontSize: FontSize(15), fontFamily: fonts.Poppins_Light, textAlign: 'left', marginBottom: 8, marginTop: 3, width: Width(70) } : { marginLeft: 15, marginRight: 5, fontSize: FontSize(15), fontFamily: fonts.Poppins_Light, textAlign: 'left', marginBottom: 8, marginTop: 3, height: Height(6), width: Width(70) }}
											multiline={true}
											numberOfLines={3}
											placeholder='Message...'
											onChangeText={(Msg) => {
												this.keyboardPresented()
												this.prevmsg = Msg
												this.setState({ Msg })
											}} />
									</View>

									{this.state.Msg.length == 0 ? 
									<TouchableOpacity
										style={{ height: Height(4), width: Height(4), borderRadius: Height(4), backgroundColor:'transparent', right: 7, position: 'absolute', alignItems: 'center', justifyContent: 'center' }}
										onPress={this.showPhotoActionSheet}>
										{/* <Ionicons color={colors.white} name='md-send'/> */}
										<Image style={{ width: 24, height: 24 }} source={chatphoto}></Image>
									</TouchableOpacity> 
									:
										<TouchableOpacity
											style={{ height: Height(4), width: Height(4), borderRadius: Height(4), backgroundColor: colors.pink, right: 7, position: 'absolute', alignItems: 'center', justifyContent: 'center' }}
											onPress={this.send}>
											{/* <Ionicons color={colors.white} name='md-send'/> */}
											<Image style={{ width: 15, height: 15 }} source={sendarrow}></Image>
										</TouchableOpacity>
									}
								</View>
							</InputAccessoryView>


							:

							<View style={styles.typeBox}>
								<TouchableOpacity style={{ marginLeft: 10 }} onPress={this.showActionSheet}>
									<AntDesign name='plus' color={colors.pink} size={25} />
								</TouchableOpacity>
								{/* <View style={{ marginLeft:Width(3),height: Height(4), flexDirection: 'row', backgroundColor: '#F2F4F7', width: Width(76), marginHorizontal: '1%', borderRadius: Height(3), alignItems: 'center', justifyContent:'center',borderWidth:0.3,borderColor:'#EFEFF4', marginTop:8, marginBottom:8}}> */}
								<View style={this.state.inputheight < Height(10) ? { marginLeft: Width(3), flexDirection: 'row', backgroundColor: '#F2F4F7', width: Width(76), marginHorizontal: '1%', borderRadius: Height(3), alignItems: 'center', borderWidth: 0.3, borderColor: '#EFEFF4', marginTop: 8, marginBottom: 8, height: this.state.inputheight } : { marginLeft: Width(3), flexDirection: 'row', backgroundColor: '#F2F4F7', width: Width(76), marginHorizontal: '1%', borderRadius: Height(3), alignItems: 'center', borderWidth: 0.3, borderColor: '#EFEFF4', marginTop: 8, marginBottom: 8, height: Height(10) }}>
									<TextInput
										ref={(ref) => { this.messagefield = ref; }}
										onFocus={this.onInputFocus}
										onBlur={this.onInputBlur}
										value={this.state.Msg}
										onContentSizeChange={(event) => {
											this.setState({
												inputheight: event.nativeEvent.contentSize.height,
											});
										}}
										//style={{ marginLeft:15, marginRight:45,fontSize: FontSize(15), fontFamily: fonts.Poppins_Light, textAlign:'left',width:Width(62)}}
										style={{ marginLeft: 15, marginRight: 5, fontSize: FontSize(15), fontFamily: fonts.Poppins_Light, textAlign: 'left', width: Width(70) }}
										//style={this.state.inputheight < Height(6) ? { marginLeft:15,marginRight:5,fontSize: FontSize(15), fontFamily: fonts.Poppins_Light, textAlign:'left',width:Width(70),height:this.state.inputheight} : { marginLeft:15,marginRight:5,fontSize: FontSize(15), fontFamily: fonts.Poppins_Light, textAlign:'left',height:Height(6),width:Width(70)}}
										multiline={true}
										placeholder='Message...'
										//onChangeText={(Msg)=>this.setState({Msg})}
										onChangeText={(Msg) => {
											this.keyboardPresented()
											this.prevmsg = Msg
											this.setState({ Msg })
										}}
									/>
								</View>

								{this.state.Msg.length == 0 ?
								 <TouchableOpacity
								 	style={{ height: Height(4), width: Height(4), borderRadius: Height(4), backgroundColor: 'transparent', right: 7, position: 'absolute', alignItems: 'center', justifyContent: 'center' }}
								 	onPress={this.showPhotoActionSheet}>
								 {/* <Ionicons color={colors.white} name='md-send'/> */}
								 	<Image style={{ width: 24, height: 24 }} source={chatphoto}></Image>
							 	</TouchableOpacity>
								 :
									<TouchableOpacity
										style={{ height: Height(4), width: Height(4), borderRadius: Height(4), backgroundColor: colors.pink, right: 7, position: 'absolute', alignItems: 'center', justifyContent: 'center' }}
										onPress={this.send}>
										{/* <Ionicons color={colors.white} name='md-send'/> */}
										<Image style={{ width: 15, height: 15 }} source={sendarrow}></Image>
									</TouchableOpacity>
								}
							</View>

					}


					{/* </ImageBackground> */}

					<View>
						<ActionSheet
							tintColor="#007AFF"
							ref={o => this.ActionSheet = o}
							options={OptionShow}
							cancelButtonIndex={OptionShow.length - 1}
							//destructiveButtonIndex={5}  
							onPress={(index) => this.CemeraRoll(index)}
						/>
					</View>
					<View>
						<ActionSheet
							tintColor="#007AFF"
							ref={o => this.PhotoActionSheet = o}
							options={PhotoOption}
							cancelButtonIndex={PhotoOption.length - 1}
							//destructiveButtonIndex={5}  
							onPress={(index) => this.PhotoSheetAction(index)}
						/>
					</View>
				
				
			</View>
			<SafeAreaView  style = {{backgroundColor:'#fff'}}/>
			</ImageBackground>

		);
	}
}

export default EventDetail; 