import React, { Component } from "react";
import { Image,TouchableOpacity, StatusBar} from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Text, Button, Header, Left, Right, Content } from "native-base";
import ValidationComponent from 'react-native-form-validator';
import { Height, FontSize, Width, colors } from "../../config/dimensions";
import { isIphoneX } from '../Default/is-iphone-x'
import { TextInput } from "react-native-gesture-handler";
import { fonts } from "../../config/constant";



const backWhite = require("../../../assets/images/backWhite.png");

class EventDescription extends ValidationComponent {


    constructor(props) {
        super(props);
        this.state = {
            plan_type : this.props.navigation.state.params.plan_type,
            description  : this.props.navigation.state.params.description,
            is_from_eventdetails: this.props.navigation.state.params.is_from_eventdetails,
        };
        this.messagefield = null
    }

    componentDidMount()
    {
        //this.messagefield.focus()
        setTimeout(()=>{
            this.messagefield.focus()
        },100)
    }

    // static navigationOptions = ({ navigation }) => {
    //     theNavigation = navigation;

    //     return {
    //         header: (
    //             <Header style={{ backgroundColor: colors.appColor,shadowOpacity: 0,borderBottomWidth: 0,height:isIphoneX() ? 85 : 64}}>
    //             <StatusBar barStyle="light-content" backgroundColor="#41199B" />
    //             <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
    //               <TouchableOpacity onPress={() => { navigation.goBack() }}>
    //                 <Image source={backWhite} style={{ tintColor: "white" }} />
    //               </TouchableOpacity>
    //             </View>
    //             <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
    //               <Text style={{letterSpacing: 0.02,fontSize: 20,fontFamily: "Raleway-Medium",color: 'white'}} >Description</Text>
    //             </View>
    //             <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', right: Width(5) }}>
    //               <TouchableOpacity onPress={() => onDonePressed()}>
    //                     <Text style = {{color:'#fff',fontSize:FontSize(15)}}>Done</Text>
    //               </TouchableOpacity>
    //             </View>
    //           </Header>
    //         )
    //     }
    // }

    renderTopBar() {
        return (
            <Header style={{ backgroundColor: colors.appColor,shadowOpacity: 0,borderBottomWidth: 0,height:isIphoneX() ? 44 : 44}}>
                <StatusBar barStyle="light-content" backgroundColor="#41199B" />
                <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                  <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                    <Image source={backWhite} style={{ tintColor: "white" }} />
                  </TouchableOpacity>
                </View>
                <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                  <Text style={{letterSpacing: 0.02,fontSize: 20,fontFamily: "Raleway-Medium",color: 'white'}} >Description</Text>
                </View>
                {
                    this.state.is_from_eventdetails == false ? 
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', right: Width(5) }}>
                        <TouchableOpacity onPress={() => this.onDonePressed()}>
                                <Text style = {{color:'#fff',fontSize:FontSize(15)}}>Done</Text>
                        </TouchableOpacity>
                    </View>
                    : null
                }
                
            </Header>
        )
    }

    onDonePressed()
    {
        if(this.state.plan_type == 1)
        {
            AsyncStorage.setItem('who_desc',this.state.description)
        }
        else if(this.state.plan_type == 2)
        {
            AsyncStorage.setItem('what_desc',this.state.description)
        }
        else if(this.state.plan_type == 3)
        {
            AsyncStorage.setItem('where_desc',this.state.description)
        }
        else
        {
            AsyncStorage.setItem('when_desc',this.state.description)
        }
        this.props.navigation.goBack()
    }

    render()
    {
        return (
            <Container style={{ flex: 1,backgroundColor:'#FFF' }} >
                {this.renderTopBar()}
                <View style = {{width:Width(100)}}>
                    <TextInput ref={(ref) => { this.messagefield = ref; }} style = {{marginHorizontal:Width(4),marginTop:15,marginBottom:15, color:'#000', fontSize:FontSize(15), fontFamily:fonts.Roboto_Regular}} multiline = {true} 
                        placeholder = 'Description' value = {this.state.description} onChangeText = {(desc) => this.setState({description:desc})} editable = {this.state.is_from_eventdetails ? false : true}></TextInput>
                    { this.state.is_from_eventdetails == false ?
                    <View style = {{width:Width(100),height:1,backgroundColor:'#F6F6F6'}}></View>
                    : null
                    }
                </View>
            </Container>
        );
    }


}

export default EventDescription;