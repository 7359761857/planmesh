import React, { Component } from "react";
import { SafeAreaView, Keyboard, Image, ScrollView, Animated, TouchableOpacity, StatusBar, FlatList, Platform, PermissionsAndroid, Text } from "react-native";
import { Container, View, Button, Header, Left, Right, Content, Item, Input, Icon, Switch, List, ListItem, Body, } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import Contacts from 'react-native-contacts';

import Toast from 'react-native-simple-toast';
import { TabView } from 'react-native-tab-view';
import { fonts } from "../../config/constant";
const search_img = require("../../../assets/images/Dashboard/search.png");
import { SearchBar } from 'react-native-elements';

import { Height, Width, FontSize, colors } from "../../config/dimensions";

import { getFriends, snedFriendRequest } from "../../redux/actions/auth";
import { API_ROOT } from "../../config/constant";
import SplashScreen from "react-native-splash-screen";
import NavigationService from "../../services/NavigationService";
import { store } from "../../redux/store";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SendSMS from 'react-native-sms'
import RNProgressHUB from 'react-native-progresshub';


const backWhite = require("../../../assets/images/backWhite.png");
const btnBg = require("../../../assets/images/btnBg.png");
const right_arrow = require("../../../assets/images/right_arrow.png");
const unSelectRadio = require("../../../assets/images/unSelectRadio.png");
const selectRadio = require("../../../assets/images/selectRadio.png");
const avtar = require("../../../assets/images/Dashboard/avtar.png");
const btn_invite_friends = require("../../../assets/images/Dashboard/btn_invite_friends.png");
import MixpanelManager from '../../../Analytics'
var subThis;

class MyListItem extends React.PureComponent {
    _onPress = () => {
        this.props.onPressItem(this.props.id);
    };

    render() {
        console.log(this.props.item)
        let item = this.props.item
        const textColor = this.props.selected ? true : false;
        return item.header == true ? <ListItem itemDivider style={{ marginLeft: 0, backgroundColor: '#DCCFFA' }}>
            <Left style={{ alignItems: 'flex-start' }}>
                <Text style={[{ color: "#41199b", textAlign: 'left', fontFamily: 16, fontFamily: "Raleway-ExtraBold", letterSpacing: 0.02 }, checkFontWeight("800")]}>{item.initial}</Text>
            </Left>
            <Body />
            <Right />
        </ListItem> :
            <TouchableOpacity key={this.props.id} onPress={() => { }}>
                <View style={{ flexDirection: 'row', height: 70, flex: 1, }}>
                    <View style={{ paddingLeft: 15, alignItems: 'flex-start', justifyContent: 'center' }}>
                        <View style={{ width: 38, height: 38, borderRadius: 19, backgroundColor: "#D6D6D6", justifyContent: 'center' }}>
                            <Text style={[{ textAlign: 'center', fontFamily: 15, fontFamily: "Raleway-ExtraBold", letterSpacing: 0.02 }, checkFontWeight("800")]}>{item.initial}</Text>
                        </View>
                    </View>
                    <View style={{ paddingLeft: 15, alignItems: 'flex-start', justifyContent: 'center', flex: 1 }}>
                        <Text style={[{ color: "#363169", fontFamily: 16, fontFamily: "Roboto-Medium", letterSpacing: 0.02 }, checkFontWeight("500")]}>{item.title}</Text>
                        <Text style={[{ color: "#363169", fontFamily: 16, fontFamily: "Roboto-Medium", letterSpacing: 0.02 }, checkFontWeight("500")]}>{item.phoneNumbers && item.phoneNumbers[0].number}</Text>

                    </View>
                    <View style={{ alignItems: 'flex-end', justifyContent: 'center', marginRight: 17 }}>
                        <Image source={textColor == false ? unSelectRadio : selectRadio} style={{ width: 25, height: 25 }}></Image>
                    </View>
                </View>
            </TouchableOpacity>
    }
}




class InviteFriends extends ValidationComponent {

    constructor(props) {
        super(props);
        issearching = false,
            issearchingcontact = false,
            this.search = null,
            this.contactsearch = null,
            this.state = {
                Data: [],
                text: '',
                contacttext: '',
                searchdata: [],
                searchcontactdata: [],
                selectAll: false,
                selectAllContact: false,
                originalData: [],
                inviteList: [],
                token: store.getState().auth.user.token,
                login_id: store.getState().auth.user.login_id,
                index: 0,
                plan_id: this.props.navigation.state.params.plan_id,
                plan_creator_id: this.props.navigation.state.params.plan_creator_id,
                is_my_event: this.props.navigation.state.params.is_my_event,
                selectedfriends: [],
                selectedcontacts: [],
                invitedcontacts: [],
                routes: [{ key: 'first', title: 'Friends' }, { key: 'second', title: 'Contacts' }],
                Data1: [{ header: true, initial: "A", id: 0 },
                { select: false, title: "Ahdieh Ashrafi 1Fall11", initial: "AA" },
                { select: false, title: "Ahdieh Ashrafi 1Fall11", initial: "AA" },
                { select: true, title: "Ahdieh Ashrafi 1Fall11", initial: "AA" },
                { select: false, title: "Ahdieh Ashrafi 1Fall11", initial: "AA" },
                { select: false, title: "Ahdieh Ashrafi 1Fall11", initial: "AA" },
                { select: false, title: "Ahdieh Ashrafi 1Fall11", initial: "AA" },
                { select: false, title: "Ahdieh Ashrafi 1Fall11", initial: "AA" }],
                selectAll: false,
                referral_url: store.getState().auth.user.referral_url,
                selected: (new Map(): Map<string, boolean>),
                plandata: this.props.navigation.state.params.item

            };
        subThis = this
        this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
    }


    componentDidMount() {
        SplashScreen.hide()

        //this.search.focus()

        //alert(JSON.stringify(this.state.item))

        this.getMyFriends()
        setTimeout(function () {
            if (Platform.OS === 'android') {
                // PermissionsAndroid.request(
                //     PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                //     {
                //         'title': 'Contacts',
                //         'message': 'This app would like to view your contacts.'
                //     }
                // ).then(() => {
                //     subThis.loadContacts();
                // })

                PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                    {
                        title: 'Planmesh',
                        message: 'Planmesh syncs only phone numbers from your address book to Planmesh servers to help you connect with other Planmesh users.',
                        buttonPositive: "OK"
                    }
                ).then((results) => {

                    if (results === PermissionsAndroid.RESULTS.GRANTED) {
                        subThis.loadContacts();
                    }
                })

            } else {
                subThis.loadContacts();
            }
        }, 200);
        this._navListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBarStyle('light-content');
            Platform.OS == 'android' && StatusBar.setBackgroundColor('#41199B');

        });
    }
    componentWillUnmount() {
        this._navListener.remove();
    }

    loadContacts() {

        Contacts.getAll().then(contacts => {
            RNProgressHUB.showSpinIndeterminate();
            let newData = []

            let form = new FormData()
            let str = "";
            contacts.map((contactData) => {

                if (contactData.givenName != null) {
                    const words = contactData.givenName.split(' ')
                    var initial = ""
                    var seactionStr = ""
                    if (words.length >= 1) {
                        initial = words[0].charAt(0)
                        seactionStr = words[0].charAt(0)
                    }
                    if (words.length >= 2) {
                        initial = initial + words[1].charAt(0)
                    }
                    let dict = { key: contactData.rawContactId, select: false, title: ((contactData.givenName) + ' ' + (contactData.familyName != null ? contactData.familyName : '')), initial: initial.toUpperCase(), sectionStr: seactionStr.toUpperCase(), phoneNumbers: contactData.phoneNumbers, is_invited: 0 }
                    contactData.phoneNumbers ? contactData.phoneNumbers.map((mob1) => {
                        str = str + mob1.number + ',';
                        return mob1.number
                    }) : ''
                    newData.push(dict)
                    //console.log('Call Log', str)

                }
            });
            form.append('login_id', this.state.login_id)
            form.append('mobile_number', str)
            form.append('plan_id', this.state.plan_id)

            fetch(API_ROOT + 'get_friends',
                {
                    body: form,
                    method: "post"
                }).then(res => res.json()).then(resJson => {
                    console.log('Call 2222' + JSON.stringify(resJson))
                    if (resJson.success) {
                        let invi = resJson.invite_friend_list;

                        let send = resJson.friend_request_list;
                        let invi_list = this.invi_friends(newData, invi)
                        //console.log("inv",invi_list)
                        let send_list = this.send_friends(newData, send)
                        //this.setState({ Data: send_list, originalData: send_list, inviteList: invi_list });
                        this.setState({ originalData: send_list, inviteList: invi_list });
                        RNProgressHUB.dismiss();
                        //alert(JSON.stringify(this.state.inviteList))
                        if (send_list.length == 0) {
                            // this.inviteFriends()
                        }
                    }
                    else {
                        RNProgressHUB.dismiss();
                    }
                }).catch(err => {
                    RNProgressHUB.dismiss();
                    //alert('Oops your connection seems off, Check your connection and try again')
                });
        })

        /*
            Contacts.getAll((err, contacts) => {
                console.log(contacts)
                console.log(err)
                if (err === 'denied') {
                    console.warn('Permission to access contacts was denied');
                    //RNProgressHUB.dismiss();
                } else {
                    let newData = []
    
                    let form = new FormData()
                    let str = "";
                    contacts.map((contactData) => {
    
                        if (contactData.givenName != null) {
                            const words = contactData.givenName.split(' ')
                            var initial = ""
                            var seactionStr = ""
                            if (words.length >= 1) {
                                initial = words[0].charAt(0)
                                seactionStr = words[0].charAt(0)
                            }
                            if (words.length >= 2) {
                                initial = initial + words[1].charAt(0)
                            }
                            let dict = { key: contactData.rawContactId, select: false, title: ((contactData.givenName) + ' ' + (contactData.familyName != null ? contactData.familyName : '')), initial: initial.toUpperCase(), sectionStr: seactionStr.toUpperCase(), phoneNumbers: contactData.phoneNumbers }
                            contactData.phoneNumbers ? contactData.phoneNumbers.map((mob1) => {
                                str = str + mob1.number + ',';
                                return mob1.number
                            }) : ''
                            newData.push(dict)
                            //console.log('Call Log', str)
    
                        }
                    });
                    form.append('login_id', this.state.login_id)
                    form.append('mobile_number', str)
                    fetch(API_ROOT + 'get_friends',
                        {
                            body: form,
                            method: "post"
                        }).then(res => res.json()).then(resJson => {
                            //alert('Call 2222' + JSON.stringify(resJson))
                            if (resJson.success) {
                                let invi = resJson.invite_friend_list;
    
                                let send = resJson.friend_request_list;
                                let invi_list = this.invi_friends(newData, invi)
                                //console.log("inv",invi_list)
                                let send_list = this.send_friends(newData, send)
                                //this.setState({ Data: send_list, originalData: send_list, inviteList: invi_list });
                                this.setState({ originalData: send_list, inviteList: invi_list });
                                //RNProgressHUB.dismiss();
                                //alert(JSON.stringify(this.state.inviteList))
                                if (send_list.length == 0) {
                                    // this.inviteFriends()
                                }
                            }
                            else {
                                //RNProgressHUB.dismiss();
                            }
                        }).catch(err => {
                            //RNProgressHUB.dismiss();
                        });
                }
            })
            */
    }

    getMyFriends() {
        RNProgressHUB.showSpinIndeterminate();

        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)

        fetch(API_ROOT + 'get_my_friends', {
            method: 'post',
            body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {

                    //alert(JSON.stringify(responseData))   

                    let friends = []

                    responseData.data.map((item) => {

                        if (item.receiver_id != this.state.plan_creator_id) {
                            friends.push(item)
                        }

                    })
                    this.setState({ Data: friends })
                    RNProgressHUB.dismiss();

                } else {
                    RNProgressHUB.dismiss();

                }
            })
            .catch((error) => {
                RNProgressHUB.dismiss();

                //alert('Oops your connection seems off, Check your connection and try again')
            })

    }

    updateInvitedContactsAPI() {
        RNProgressHUB.showSpinIndeterminate();

        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('plan_id', this.state.plan_id)
        data.append('mobile_number', String(this.state.selectedcontacts))
        //alert(JSON.stringify(data))
        //return
        fetch(API_ROOT + 'invite/plan', {
            method: 'post',
            body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {

                    //console.log('update invite contact: '+JSON.stringify(responseData))
                    this.issearchingcontact = false
                    this.loadContacts()
                    this.setState({ contacttext: '' })
                    this.contactsearch.clear()
                    setTimeout(() => {
                        this.contactsearch.blur()

                    }, 100)
                    RNProgressHUB.dismiss();

                } else {

                    RNProgressHUB.dismiss();

                }
            })
            .catch((error) => {
                RNProgressHUB.dismiss();

                //alert('Oops your connection seems off, Check your connection and try again')
            })
    }

    inviteContacts() {

        //alert(this.state.selectedcontacts)

        //this.updateInvitedContactsAPI()

        if (this.state.selectedcontacts.length > 0) {
            var item = this.state.plandata
            var message = ''

            if (item.plan_type == 1) {
                var name = item.friend_name.split(" ")

                message = 'Hey, ' + "i'm inviting you to " + name[0] + ", let's meet up!" + '\n' + "Let me know on Planmesh if you're in" + '\n\n' + 'https://planmeshapp.com/plan_details/' + item.plan_id
            }
            else if (item.plan_type == 2) {
                message = 'Hey, ' + "i'm inviting you to " + item.activity + " Let me know on Planmesh if you're in" + '\n\n' + 'https://planmeshapp.com/plan_details/' + item.plan_id
            }
            else if (item.plan_type == 3) {
                message = 'Hey, ' + "i'm inviting you to " + "Wanna go to " + item.line_1 + "?" + " Let me know on Planmesh if you're in" + '\n\n' + 'https://planmeshapp.com/plan_details/' + item.plan_id
            }
            else if (item.plan_type == 4) {
                message = 'Hey, ' + "i'm inviting you to " + "I'm free, wanna hangout?" + " Let me know on Planmesh if you're in" + '\n\n' + 'https://planmeshapp.com/plan_details/' + item.plan_id
            }


            SendSMS.send({
                //body: `Hey! I'm inviting you to Planmesh. Use my referral code E6C0AJ to register and own a piece of Planmesh. Download Now!`+this.state.referral_url,
                body: message,
                recipients: this.state.selectedcontacts,
                successTypes: ['sent', 'queued'],
                allowAndroidSendWithoutReadPermission: true
            }, (completed, cancelled, error) => {
                if (completed) {
                    //console.log("Invitation send successfully")
                    //alert('Invitation sent successfully')
                    //NavigationService.reset('dashboard')

                    setTimeout(() => {
                        this.updateInvitedContactsAPI()
                    }, 100)

                }
                else if (cancelled) {
                    //Toast.show("Invite send successfully")
                }
                else {
                    //alert(error)
                }
            });
        }

    }

    inviteFriendsAPICall() {

        var friendsids = ''

        if (this.issearching == true) {
            this.state.searchdata.map((facet) => {

                if (facet.select == true) {
                    if (friendsids == '') {
                        friendsids = facet.receiver_id
                    }
                    else {
                        friendsids = friendsids + ',' + facet.receiver_id
                    }

                }

            });
        }
        else {
            this.state.Data.map((facet) => {

                if (facet.select == true) {
                    if (friendsids == '') {
                        friendsids = facet.receiver_id
                    }
                    else {
                        friendsids = friendsids + ',' + facet.receiver_id
                    }

                }

            });
        }

        if (friendsids == '') {
            return
        }

        RNProgressHUB.showSpinIndeterminate();

        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('type', this.state.is_my_event == true ? 'my' : 'other')
        data.append('friend_id', friendsids)
        data.append('plan_id', this.state.plan_id)

        fetch(API_ROOT + 'invite/friends', {
            method: 'post',
            body: data
        })
            .then((response) => response.json())
            .then((responseData) => {

                if (responseData.success) {

                    const MixpanelData = {
                        '$email': store.getState().auth.user.email_address,
                        'Post ID': this.state.plan_id
                    }
                    this.mixpanel.identify(store.getState().auth.user.email_address);
                    this.mixpanel.track('Post Invite', MixpanelData);

                    //alert(JSON.stringify(responseData))   
                    RNProgressHUB.dismiss();
                    this.props.navigation.goBack()

                } else {
                    RNProgressHUB.dismiss();

                }
            })
            .catch((error) => {
                RNProgressHUB.dismiss();

                //alert('Oops your connection seems off, Check your connection and try again')
            })
    }

    tabViewIndexChange(index) {
        this.setState({ index });
    };
    renderTabBar = (props) => {
        const inputRange = props.navigationState.routes.map((x, i) => i);
        return (
            <View style={styles.tabBar}>
                {props.navigationState.routes.map((route, i) => {
                    const color = "#363169"
                    return (
                        <TouchableOpacity
                            activeOpacity={0.9}
                            key={i}
                            style={[styles.tabItem, this.state.index === i ? styles.selectedTab : styles.notSelected]}
                            onPress={() => this.setState({ index: i })}>
                            <View>
                                <Animated.Text style={{ color, fontSize: FontSize(16), fontFamily: fonts.Raleway_Bold }}>{route.title}</Animated.Text>
                            </View>
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    };
    invi_friends = (newData, invi) => {
        //let data = newData.filter(fruit => fruit.phoneNumbers.some(m => invi.includes(m.number)))
        //let datatemp = newData.filter(fruit => fruit.phoneNumbers.some(m => invi.some(e => e.number === m.number)))
        var data = []
        //console.log('CONTACTS----: '+JSON.stringify(newData))
        for (item of newData) {

            var number = ''
            //console.log('NUMBER: '+item.phoneNumbers)
            //console.log('NAME: '+item.title)
            if (item.phoneNumbers != undefined && item.phoneNumbers != null && item.phoneNumbers.length > 0 && item.phoneNumbers && item.phoneNumbers[0].number) {
                //console.log('Number Found')
                number = item.phoneNumbers[0].number
            }

            for (invtcnct of invi) {
                if (invtcnct.number == number) {
                    item.is_invited = invtcnct.is_invited
                    data.push(item)
                }
            }
        }
        //alert('Contacts to Invite: '+JSON.stringify(data))
        return data
    }

    send_friends = (newData, send) => {
        return newData.filter(fruit => fruit.phoneNumbers.some(m => send.includes(m.number)))
    }
    FlatListItemSeparator = () => <View style={styles.line} />;

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={[styles.headerAndroidnav1, { backgroundColor: '#41199B', height: 44 }]}>
                    <StatusBar barStyle="light-content" backgroundColor="#41199B" />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack() }}>
                            <Image source={backWhite} />
                        </TouchableOpacity></View>
                    <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                        <Text style={[styles.box1, { color: '#fff', fontSize: FontSize(20) },]} >Invite Friends</Text>
                    </View>

                </Header>
            )
        }
    }

    // static navigationOptions = ({ navigation }) => {
    //     theNavigation = navigation;
    //     return {
    //         header: null
    //     }
    // }


    renderTopBar() {
        return (
            <Header style={[styles.headerAndroidnav, { backgroundColor: '#41199B' }]}>
                <StatusBar barStyle="light-content" backgroundColor="#41199B" />
                <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
                    <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                        <Image source={backWhite} />
                    </TouchableOpacity></View>
                <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                    <Text style={[styles.box1, { color: '#fff', fontSize: FontSize(20) },]} >Invite Friends</Text>
                </View>
            </Header>
        )
    }

    inviteFriends = () => {
        let selectedArray = this.state.inviteList
        this.props.navigation.push('sendFriendRequest', {
            selectedArray: selectedArray.sort((a, b) => a.sectionStr > b.sectionStr)
        })
    }

    onRequestSend = () => {
        let mbl = ''
        let selectedArray = []
        this.state.originalData.map((facet) => {
            if (facet.select == true) {
                selectedArray.push(facet)
                facet.phoneNumbers ? facet.phoneNumbers.map((mob1) => {
                    mbl = mbl + mob1.number + ',';
                    return mob1.number
                }) : ''
            }
        });

        let data = { token: this.state.token, login_id: this.state.login_id, mobile_number: mbl }
        console.log(store.getState().auth.user.token)
        console.log(store.getState().auth.user)
        snedFriendRequest(data).then(res => {
            if (res.success) {
                Toast.show(res.text)
                //NavigationService.reset('dashboard')
            }
            else {
                Toast.show(res.text)
            }
        }).catch(err => {
            //alert('Oops your connection seems off, Check your connection and try again')
        })
        // var selectedArray = []
        // this.state.originalData.map((facet) => {
        //     if (facet.select == true) {
        //         selectedArray.push(facet)
        //     }
        // });
        // console.log(selectedArray)
        // this.props.navigation.push('sendFriendRequest', {
        //     selectedArray: selectedArray.sort((a, b) => a.sectionStr > b.sectionStr)
        // })
    }

    loadTableCells(item, index) {
        return (this.cellVideoList(item, index));
    }
    onSelectList(facets, index) {

        if (this.issearching == true) {
            var dataArray = this.state.searchdata;
            var facet = dataArray[index]
            if (facet.select == true) {
                facet.select = false
                var list = this.state.selectedfriends
                list.pop(facet.receiver_id)
                dataArray.splice(index, 1, facet)
                this.setState({ selectedfriends: list, searchdata: dataArray })
            } else {
                facet.select = true
                var list = this.state.selectedfriends
                list.push(facet.receiver_id)
                dataArray.splice(index, 1, facet)
                this.setState({ selectedfriends: list, searchdata: dataArray })
            }
        }
        else {
            var dataArray = this.state.Data;
            var facet = dataArray[index]
            if (facet.select == true) {
                facet.select = false
                var list = this.state.selectedfriends
                list.pop(facet.receiver_id)
                dataArray.splice(index, 1, facet)
                this.setState({ selectedfriends: list, Data: dataArray })
            } else {
                facet.select = true
                var list = this.state.selectedfriends
                list.push(facet.receiver_id)
                dataArray.splice(index, 1, facet)
                this.setState({ selectedfriends: list, Data: dataArray })
            }
        }

        this.forceUpdate()
        this.checkIfAllSelected()
        //this.setState({data: facet})
    }

    onSelectAll(value) {

        this.setState({ selectedfriends: [] })

        if (this.issearching == true) {
            var list = this.state.selectedfriends

            this.state.searchdata.map((facet) => {
                facet.select = value.value

                if (facet.select == true) {
                    list.push(facet.receiver_id)
                }
                else {
                    list.pop(facet.receiver_id)
                }
                this.setState({ data: facet })
            });

            if (value.value == true) {
                this.setState({ selectAll: true, selectedfriends: list })
            } else {
                this.setState({ selectAll: false, selectedfriends: list })
            }
        }
        else {
            var list = this.state.selectedfriends

            this.state.Data.map((facet) => {
                facet.select = value.value

                if (facet.select == true) {
                    list.push(facet.receiver_id)
                }
                else {
                    list.pop(facet.receiver_id)
                }
                this.setState({ data: facet })
            });

            if (value.value == true) {
                this.setState({ selectAll: true, selectedfriends: list })
            } else {
                this.setState({ selectAll: false, selectedfriends: list })
            }
        }


    }

    onSelectContact(item, index) {
        if (this.issearchingcontact == true) {
            var dataArray = this.state.searchcontactdata;
            var facet = dataArray[index]
            if (facet.select == true) {
                facet.select = false
                var list = this.state.selectedcontacts
                list.pop(facet.phoneNumbers[0].number)
                dataArray.splice(index, 1, facet)
                this.setState({ selectedcontacts: list, searchcontactdata: dataArray })
            } else {
                facet.select = true
                var list = this.state.selectedcontacts
                list.push(facet.phoneNumbers[0].number)
                dataArray.splice(index, 1, facet)
                this.setState({ selectedcontacts: list, searchcontactdata: dataArray })
            }
        }
        else {
            var dataArray = this.state.inviteList;
            var facet = dataArray[index]
            if (facet.select == true) {
                facet.select = false
                var list = this.state.selectedcontacts
                list.pop(facet.phoneNumbers[0].number)
                dataArray.splice(index, 1, facet)
                this.setState({ selectedcontacts: list, inviteList: dataArray })
            } else {
                facet.select = true
                var list = this.state.selectedcontacts
                list.push(facet.phoneNumbers[0].number)
                dataArray.splice(index, 1, facet)
                this.setState({ selectedcontacts: list, inviteList: dataArray })
            }
        }
        this.forceUpdate()
        this.checkIfAllContactsSelected()
    }

    onSelectAllContacts(value) {
        this.setState({ selectedcontacts: [] })

        if (this.issearchingcontact == true) {
            var list = this.state.selectedcontacts

            this.state.searchcontactdata.map((facet) => {
                facet.select = value.value

                if (facet.select == true) {
                    list.push(facet.phoneNumbers[0].number)
                }
                else {
                    list.pop(facet.phoneNumbers[0].number)
                }
                this.setState({ data: facet })
            });

            if (value.value == true) {
                this.setState({ selectAllContact: true, selectedcontacts: list })
            } else {
                this.setState({ selectAllContact: false, selectedcontacts: list })
            }
        }
        else {
            var list = this.state.selectedcontacts

            this.state.inviteList.map((facet) => {
                facet.select = value.value

                if (facet.select == true) {
                    list.push(facet.phoneNumbers[0].number)
                }
                else {
                    list.pop(facet.phoneNumbers[0].number)
                }
                this.setState({ data: facet })
            });

            if (value.value == true) {
                this.setState({ selectAllContact: true, selectedcontacts: list })
            } else {
                this.setState({ selectAllContact: false, selectedcontacts: list })
            }
        }
    }

    checkIfAllSelected() {
        var allselected = true
        this.state.Data.map((facet) => {
            if (facet.select == false || facet.select == null) {
                allselected = false
            }
        });

        if (allselected == true) {
            this.setState({ selectAll: true })
        }
        else {
            this.setState({ selectAll: false })
        }

    }

    checkIfAllContactsSelected() {
        var allselected = true
        this.state.inviteList.map((facet) => {
            if (facet.select == false || facet.select == null) {
                allselected = false
            }
        });

        if (allselected == true) {
            this.setState({ selectAllContact: true })
        }
        else {
            this.setState({ selectAllContact: false })
        }
    }

    formatPhoneNumber(str) {
        let cleaned = ('' + str).replace(/\D/g, '');

        //        alert(cleaned)

        let match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);

        if (match) {
            let intlCode = (match[1] ? '+1 ' : '')
            return [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('')
        }
        return null;
    }

    contactCellList = (item, index) => {

        var number = ''
        if (item.phoneNumbers && item.phoneNumbers[0].number) {
            number = this.formatPhoneNumber(item.phoneNumbers[0].number)

            if (number == null) {
                number = item.phoneNumbers && item.phoneNumbers[0].number
            }

        }

        return (
            <TouchableOpacity onPress={() => { this.onSelectContact(item, index) }}>
                <View style={{ flexDirection: 'row', height: 55, flex: 1, alignItems: 'center' }}>
                    <View style={{ paddingLeft: 15, alignItems: 'flex-start', justifyContent: 'center' }}>
                        <View style={{ width: 38, height: 38, borderRadius: 19, backgroundColor: "#D6D6D6", justifyContent: 'center' }}>
                            <Text style={[{ textAlign: 'center', fontFamily: 15, fontFamily: "Raleway-ExtraBold", letterSpacing: 0.02 }, checkFontWeight("800")]}>{item.initial}</Text>
                        </View>
                    </View>
                    <View style={{ paddingLeft: 15, alignItems: 'flex-start', justifyContent: 'center', flex: 1 }}>
                        <Text style={[{ color: "#363169", fontFamily: 16, fontFamily: "Roboto-Medium", letterSpacing: 0.02 }]}>{item.title}</Text>
                        <Text style={[{ color: "#363169", fontFamily: 16, fontFamily: "Roboto-Regular", letterSpacing: 0.02 }]}>{number}</Text>
                    </View>
                    {
                        item.is_invited == 0 ?
                            <View style={{ alignItems: 'flex-end', justifyContent: 'center', marginRight: 17 }}>
                                <Image source={item.select == false ? unSelectRadio : selectRadio} style={{ width: 25, height: 25 }}></Image>
                            </View>
                            :
                            <View style={{ alignItems: 'flex-end', justifyContent: 'center', marginRight: 17 }}>
                                <Text style={{ fontSize: 11, fontFamily: 'Roboto-Regular', color: '#495362', textAlign: 'right' }}>Invited</Text>
                            </View>
                    }

                </View>
            </TouchableOpacity>
        )

    }

    cellVideoList = (facet, rowID) => {
        //sectionID = 0;
        //console.log(facet)
        return (
            <TouchableOpacity key={rowID} onPress={() => { this.onSelectList(facet, rowID) }}>
                <View style={{ flexDirection: 'row', height: 55, flex: 1 }}>
                    <View style={{ paddingLeft: 15, alignItems: 'flex-start', justifyContent: 'center' }}>
                        <View style={{ width: 40, height: 40, borderRadius: 20, backgroundColor: "#D6D6D6", justifyContent: 'center' }}>
                            {/* <Text style={[{ textAlign: 'center', fontFamily: 15, fontFamily: "Raleway-ExtraBold", letterSpacing: 0.02 }, checkFontWeight("800")]}>{facet.initial}</Text> */}
                            <Image source={facet.receiver_photo != '' ? { uri: facet.receiver_photo } : avtar} style={{ width: 40, height: 40, borderRadius: 20 }}></Image>
                        </View>
                    </View>
                    <View style={{ paddingLeft: 15, alignItems: 'flex-start', justifyContent: 'center', flex: 1 }}>
                        <Text style={[{ color: "#363169", fontFamily: 16, fontFamily: "Roboto-Medium", letterSpacing: 0.02 }]}>{facet.receiver_name}</Text>
                        {/* <Text style={[{ color: "#363169", fontFamily: 16, fontFamily: "Roboto-Medium", letterSpacing: 0.02 }, checkFontWeight("500")]}>{facet.receiver_mobile_number}</Text> */}
                    </View>
                    <View style={{ alignItems: 'flex-end', justifyContent: 'center', marginRight: 17 }}>
                        <Image source={facet.select == true ? selectRadio : unSelectRadio} style={{ width: 25, height: 25 }}></Image>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
    _renderItem(item, index) {
        // <MyListItem
        //     id={item.id}
        //     onPressItem={this._onPressItem}
        //     selected={!!this.state.selected.get(item.id)}
        //     title={item.title}
        //     item={item}
        // />

        return (this.contactCellList(item, index));
    }

    handleChange = (searchKey) => {
        console.log(searchKey.searchKey)
        var tempData = []
        this.state.originalData.map((contactData) => {
            if (contactData.title.includes(searchKey.searchKey)) {
                tempData.push(contactData)
            }
        });
        this.setState({ Data: tempData })
    }

    onClearText() {
        this.issearching = false
        this.setState({ text: '' })
        this.search.clear()
        setTimeout(() => {
            this.search.blur()

        }, 100)
    }

    SearchFilterFunction(text) {

        this.setState({ text: text })

        if (this.state.Data.length > 0) {
            if (text != '') {
                this.issearching = true
                var filteredarray = []
                this.state.Data.map((facet) => {

                    if (facet.receiver_name.toLowerCase().includes(text.toLowerCase())) {
                        filteredarray.push(facet)
                    }
                });

                this.setState({ searchdata: filteredarray })
                //this.forceUpdate()
            }
            else {
                this.issearching = false
                //this.forceUpdate()
            }

            if (this.state.text.length == 1) {
                this.issearching = false
                //this.forceUpdate()
            }
        }
    }

    onClearContactText() {
        this.issearchingcontact = false
        this.setState({ contacttext: '' })
        this.contactsearch.clear()
        setTimeout(() => {
            this.contactsearch.blur()

        }, 100)
    }

    SearchContactFunction(text) {
        this.setState({ contacttext: text })

        if (this.state.inviteList.length > 0) {
            if (text != '') {
                this.issearchingcontact = true
                var filteredarray = []
                this.state.inviteList.map((facet) => {
                    if (facet.title.toLowerCase().includes(text.toLowerCase())) {
                        filteredarray.push(facet)
                    }
                });
                this.setState({ searchcontactdata: filteredarray })
                //this.forceUpdate()
            }
            else {
                this.issearchingcontact = false
                //this.forceUpdate()
            }

            if (this.state.contacttext.length == 1) {
                this.issearchingcontact = false
                //this.forceUpdate()
            }
        }
    }

    _onMomentumScrollBegin = () => {
        Keyboard.dismiss()
    };

    renderScene = ({ route }) => {
        switch (route.key) {
            case 'first':
                return <View style={{ height: Height(72) }}>
                    {/* <View style={{ backgroundColor: '#fff', width: Width(100), height: Height(5)}}> */}
                    <SearchBar
                        containerStyle={{ backgroundColor: "#fff", borderBottomColor: 'transparent', borderTopColor: 'transparent', fontFamily: fonts.Roboto_Regular, marginTop: Height(0.5) }}
                        inputContainerStyle={{ borderBottomColor: 'transparent', borderTopColor: 'transparent', backgroundColor: '#EEEEEE', width: Width(90), marginLeft: Width(2.5), alignItems: 'center', fontFamily: fonts.Roboto_Regular }}
                        placeholderTextColor='#8F8F90'
                        inputStyle={{ fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}
                        placeholder="Search"
                        round={true}
                        ref={search => this.search = search}
                        onChangeText={text => this.SearchFilterFunction(text)}
                        value={this.state.text}
                        searchIcon={() => <Image source={search_img} style={{ tintColor: '#B4B7BA', marginLeft: Width(1) }} />}
                        clearIcon={() => <TouchableOpacity onPress={() => this.onClearText()}><Ionicons name='md-close-circle' color='#B4B7BA' size={22} /></TouchableOpacity>}

                    />
                    {/* </View> */}
                    {/* <View style={{ height: Height(0.3), backgroundColor: '#EFEFF4', marginTop: Height(2) }} /> */}
                    <View style={{ backgroundColor: "#FFFFFF", height: 50, paddingLeft: 20, flexDirection: 'row', borderTopWidth: 2, borderTopColor: '#EFEFF4', marginTop: Height(0.5) }}>
                        <Text style={[{ color: colors.dargrey, fontFamily: "Roboto-Bold", fontSize: 16, letterSpacing: 0.005, alignItems: 'flex-start', alignSelf: 'center', flex: 1 }, checkFontWeight("700")]}>Select All</Text>

                        {
                            Platform.OS == 'ios' ?
                                <Switch trackColor={{ true: "#4BD964" }} ios_backgroundColor={'#E4E4E5'} value={this.state.selectAll} onValueChange={(value) => this.onSelectAll({ value })} style={{ alignItems: 'flex-end', alignSelf: 'center', marginRight: 20 }} />
                                :
                                <Switch thumbTintColor={'#ffffff'} trackColor={{ true: "#4BD964", false: '#ccc' }} value={this.state.selectAll} onValueChange={(value) => this.onSelectAll({ value })} style={{ alignItems: 'flex-end', alignSelf: 'center', marginRight: 20 }} />
                        }
                    </View>

                    {
                        this.state.Data != null && this.state.Data.length > 0 ?

                            this.issearching == true ?
                                <ScrollView keyboardDismissMode='on-drag'>
                                    <FlatList
                                        data={this.state.searchdata}
                                        renderItem={
                                            ({ item, index }) => this.loadTableCells(item, index)
                                        }
                                        keyExtractor={(item) => item.key}
                                        extraData={this.state}
                                        onScrollBeginDrag={this._onMomentumScrollBegin}
                                        ItemSeparatorComponent={this.FlatListItemSeparator}
                                    />
                                </ScrollView>
                                :
                                <ScrollView keyboardDismissMode='on-drag'>
                                    <FlatList
                                        data={this.state.Data}
                                        renderItem={
                                            ({ item, index }) => this.loadTableCells(item, index)
                                        }
                                        keyExtractor={(item) => item.key}
                                        extraData={this.state}
                                        onScrollBeginDrag={this._onMomentumScrollBegin}
                                        ItemSeparatorComponent={this.FlatListItemSeparator}
                                    />
                                </ScrollView>
                            :

                            <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 80 }}>

                                <Text style={{ width: Width(45), fontFamily: fonts.Raleway_Bold, fontSize: FontSize(18), color: '#41199B', textAlign: 'center' }}>Planmesh is better with friends</Text>
                                <Text style={{ width: Width(45), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#3A4759', marginTop: 10, textAlign: 'center', marginBottom: 10 }}>Invite your contacts and start making plans.</Text>
                                <TouchableOpacity onPress={this.inviteFriends}>
                                    <Image source={btn_invite_friends} resizeMode='contain'></Image>
                                </TouchableOpacity>

                            </View>
                    }


                </View>
            case 'second':
                return (<View style={{ height: Height(72) }}>
                    <View style={{ backgroundColor: '#fff', width: Width(100), height: Height(5) }}>
                        <SearchBar
                            containerStyle={{ backgroundColor: "#fff", borderBottomColor: 'transparent', borderTopColor: 'transparent', fontFamily: fonts.Roboto_Regular }}
                            inputContainerStyle={{ borderBottomColor: 'transparent', borderTopColor: 'transparent', backgroundColor: '#EEEEEE', width: Width(90), marginLeft: Width(2.5), alignItems: 'center', fontFamily: fonts.Roboto_Regular }}
                            placeholderTextColor='#8F8F90'
                            inputStyle={{ fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}
                            placeholder="Search "
                            round={true}
                            ref={contactsearch => this.contactsearch = contactsearch}
                            onChangeText={text => this.SearchContactFunction(text)}
                            value={this.state.contacttext}
                            searchIcon={() => <Image source={search_img} style={{ tintColor: '#B4B7BA', marginLeft: Width(1) }} />}
                            clearIcon={() => <TouchableOpacity onPress={() => this.onClearContactText()}><Ionicons name='md-close-circle' color='#B4B7BA' size={22} /></TouchableOpacity>}

                        />
                    </View>
                    <View style={{ height: Height(0.3), backgroundColor: '#EFEFF4', marginTop: Height(2) }} />
                    <View style={{ backgroundColor: "#FFFFFF", height: 50, paddingLeft: 20, flexDirection: 'row' }}>
                        <Text style={[{ color: colors.dargrey, fontFamily: "Roboto-Bold", fontSize: 16, letterSpacing: 0.005, alignItems: 'flex-start', alignSelf: 'center', flex: 1 }, checkFontWeight("700")]}>Select All</Text>
                        {
                            Platform.OS == 'ios' ?
                                <Switch trackColor={{ true: "#4BD964" }} ios_backgroundColor={'#E4E4E5'} value={this.state.selectAllContact} onValueChange={(value) => this.onSelectAllContacts({ value })} style={{ alignItems: 'flex-end', alignSelf: 'center', marginRight: 20 }} />
                                :
                                <Switch thumbTintColor={'#ffffff'} trackColor={{ true: "#4BD964", false: '#ccc' }} value={this.state.selectAllContact} onValueChange={(value) => this.onSelectAllContacts({ value })} style={{ alignItems: 'flex-end', alignSelf: 'center', marginRight: 20 }} />
                        }
                    </View>

                    {
                        this.issearchingcontact == true ?

                            <FlatList

                                data={this.state.searchcontactdata}
                                // extraData={this.state}
                                renderItem={
                                    //this._renderItem
                                    ({ item, index }) => this._renderItem(item, index)
                                }
                                onEndReachedThreshold={50}
                                maxToRenderPerBatch={10}
                                windowSize={10}
                                extraData={this.state}
                                onScrollBeginDrag={this._onMomentumScrollBegin}
                                keyExtractor={(item, index) => index.toString()}
                            // stickyHeaderIndices={this.state.stickyHeaderIndices}
                            />
                            :
                            <FlatList

                                data={this.state.inviteList}
                                // extraData={this.state}
                                renderItem={
                                    //this._renderItem
                                    ({ item, index }) => this._renderItem(item, index)
                                }
                                onEndReachedThreshold={50}
                                maxToRenderPerBatch={10}
                                windowSize={10}
                                extraData={this.state}
                                onScrollBeginDrag={this._onMomentumScrollBegin}
                                keyExtractor={(item, index) => index.toString()}
                            // stickyHeaderIndices={this.state.stickyHeaderIndices}
                            />

                    }


                </View>
                )

            default:
                return null;
        }
    };
    render() {

        return (
            <Container style={{ flex: 1, backgroundColor: colors.white }} >
                <Toastt ref="toast"></Toastt>
                <SafeAreaView style={{ backgroundColor: '#41199B' }} />
                {/* {this.renderTopBar()} */}
                <BackgroundImage >
                    {/* <SafeAreaView /> */}
                    <TabView
                        swipeEnabled={false}
                        navigationState={this.state}
                        renderScene={this.renderScene}
                        renderTabBar={this.renderTabBar}
                        // onIndexChange={(index) => {this.tabViewIndexChange(index)}}
                        onIndexChange={index => this.setState({ index })}
                    />
                    {/* {this.state.selectedfriends.length > 0 || this.state.selectedcontacts.length > 0 ? */}


                    {
                        this.state.index == 0 ?

                            this.state.Data != null && this.state.Data.length > 0 ?

                                <TouchableOpacity
                                    activeOpacity={0.9}
                                    style={(this.state.selectedfriends.length > 0 || this.state.selectedcontacts.length > 0) ? {
                                        bottom: 20, position: 'absolute', height: Height(6), width: '93%', alignItems: 'center', backgroundColor: colors.pink, borderRadius: Height(3), marginBottom: Height(3), alignSelf: 'center', justifyContent: 'center',
                                        shadowColor: "#000",
                                        shadowOffset: {
                                            width: 0,
                                            height: 3,
                                        },
                                        shadowOpacity: 0.29,
                                        shadowRadius: 4.65,

                                        elevation: 7,
                                    }
                                        :
                                        {
                                            bottom: 20, position: 'absolute', height: Height(6), width: '93%', alignItems: 'center', backgroundColor: '#8F8F90', borderRadius: Height(3), marginBottom: Height(3), alignSelf: 'center', justifyContent: 'center',
                                            shadowColor: "#000",
                                            shadowOffset: {
                                                width: 0,
                                                height: 3,
                                            },
                                            shadowOpacity: 0.29,
                                            shadowRadius: 4.65,

                                            elevation: 7,
                                        }
                                    }
                                    onPress={(this.state.selectedfriends.length > 0 || this.state.selectedcontacts.length > 0) ? () => this.state.index == 0 ? this.inviteFriendsAPICall() : this.inviteContacts() : () => { }}>

                                    <Text style={[{ fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Select</Text>

                                </TouchableOpacity>

                                :

                                null

                            :

                            <TouchableOpacity
                                activeOpacity={0.9}
                                style={(this.state.selectedfriends.length > 0 || this.state.selectedcontacts.length > 0) ? {
                                    bottom: 20, position: 'absolute', height: Height(6), width: '93%', alignItems: 'center', backgroundColor: colors.pink, borderRadius: Height(3), marginBottom: Height(3), alignSelf: 'center', justifyContent: 'center',
                                    shadowColor: "#000",
                                    shadowOffset: {
                                        width: 0,
                                        height: 3,
                                    },
                                    shadowOpacity: 0.29,
                                    shadowRadius: 4.65,

                                    elevation: 7,
                                }
                                    :
                                    {
                                        bottom: 20, position: 'absolute', height: Height(6), width: '93%', alignItems: 'center', backgroundColor: '#8F8F90', borderRadius: Height(3), marginBottom: Height(3), alignSelf: 'center', justifyContent: 'center',
                                        shadowColor: "#000",
                                        shadowOffset: {
                                            width: 0,
                                            height: 3,
                                        },
                                        shadowOpacity: 0.29,
                                        shadowRadius: 4.65,

                                        elevation: 7,
                                    }
                                }
                                onPress={(this.state.selectedfriends.length > 0 || this.state.selectedcontacts.length > 0) ? () => this.state.index == 0 ? this.inviteFriendsAPICall() : this.inviteContacts() : () => { }}>

                                <Text style={[{ fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Select</Text>

                            </TouchableOpacity>

                    }



                    {/* :

                         <TouchableOpacity 
                        activeOpacity = {0.9}
                        style={{
                            bottom: 20, position: 'absolute', height: Height(6), width: '93%', alignItems: 'center', backgroundColor: '#8F8F90', borderRadius: Height(3), marginBottom: Height(3), alignSelf: 'center', justifyContent: 'center',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 3,
                            },
                            shadowOpacity: 0.29,
                            shadowRadius: 4.65,

                            elevation: 7,
                        }}>
                            <Text style={[{ fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}> Select</Text>
                        </TouchableOpacity>
                        } */}
                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default InviteFriends;
