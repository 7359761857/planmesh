import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Alert, ImageBackground, TouchableOpacity, StatusBar, Text, Platform, TextInput } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Button, Header, Left, Right, Content, Item, Input } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FloatingLabel from 'react-native-floating-labels';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import Modal from "react-native-modal";




const backWhite = require("../../../assets/images/backWhite.png");

const btnBg = require("../../../assets/images/btnBg.png");

const search_img = require("../../../assets/images/Dashboard/search.png");
const current_location = require("../../../assets/images/Dashboard/location_home.png");

class when_location extends ValidationComponent {
  constructor(props) {
    super(props);
    this.search = null;
    this.state = {
      labelStylePassword: styles.labelInputUnFocus, stylePassword: styles.formInputUnFocus, password: "", passwordValidation: true, passwordValidationMsg: "",
      labelStyleEmail: styles.labelInputUnFocus, styleEmail: styles.formInputUnFocus, email: "", emailValidation: true, emailValidationMsg: "",
      btnDoneDisable: true, wsError: false,destination:'',searchKey:'',
      latitude:'',
      longitude:'',
      predictions:[],
    };
    subThis = this
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
      header: (
        <Header style={[styles.headerAndroidnav,{height:44}]}>
        <StatusBar barStyle="light-content" backgroundColor="#41199B" />
        <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
          <TouchableOpacity onPress={() => { navigation.goBack() }}>
            <Image source={backWhite} style={{ tintColor: "white" }} />
          </TouchableOpacity></View>
        <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
          <Text style={styles.headerTitle} >Add a location</Text></View>

      </Header>
      )
    }
  }
  async  requestLocationPermission(){
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'Example App',
          'message': 'Example App access to your location '
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the location")
        alert("You can use the location");
      } else {
        console.log("location permission denied")
        alert("Location permission denied");
      }
    } catch (err) {
      console.warn(err)
    }
  }
  async componentDidMount(){
    this.workaroundFocus()
    await requestLocationPermission()
    this.watchID = navigator.geolocation.watchPosition((position) => {
      // Create the object to update this.state.mapRegion through the onRegionChange function
      this.setState({latitude:position.coords.latitude,
                      longitude:position.coords.longitude})
     
    }, (error)=>console.log(error));
   
  }

  workaroundFocus() {
    this.search.blur();
    setTimeout(() => {
      this.search.focus();
    }, 100);
  }

 async onChangeDestination(destination){
 try{
  const result=await fetch(`https://maps.googleapis.com/maps/api/place/textsearch/json?location=${this.state.latitude},${this.state.longitude}&radius=3000&sensor=true&query=${destination}&key=AIzaSyA6JsG7CPXtIfYJJF3UFdz8z2eBDqohKA8`);
  const json=await result.json();
  this.setState({predictions:json.results})
 }catch(err){
  //alert('Oops your connection seems off, Check your connection and try again')
 }


}
goBack=(predictions)=>{

  AsyncStorage.setItem('when_Location',predictions.name)
  AsyncStorage.setItem('when_FullAddress',predictions.formatted_address)
   this.props.navigation.goBack()
}
  render() {
  
    return (
      <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }} >
        <Toastt ref="toast"></Toastt>
        <BackgroundImage >
          <SafeAreaView />

          <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row',backgroundColor:colors.white }}>
              <View style={{ height: Height(7), alignItems: 'center', width: Width(100),flexDirection:'row',borderBottomColor:colors.lightgrey, borderBottomWidth:1 }}>
                <Image source={search_img} style={{ marginLeft: Width(5) }} />
                   <TextInput
                  placeholderTextColor={colors.fontDarkGrey}
                  placeholder="Enter a place or city "
                  ref={search => this.search = search}
                  onChangeText={destination => this.onChangeDestination(destination)} 
                  style={{ flex: 1, fontFamily: fonts.Raleway_Medium, fontSize: 16, marginLeft: Width(4.5),marginRight:60}} />
              </View>
            </View>


          <SafeAreaView style={{ backgroundColor: colors.white }}>
          <ScrollView>
          {this.state.predictions.map(predictions => {
                                    return (
                                      <TouchableOpacity onPress={()=>this.goBack(predictions)} style={{minHeight: Height(10), flexDirection: 'row', alignItems: 'center', borderBottomColor: '#ccc', borderBottomWidth: 1, padding:5 }}>
                                      <View style={{ marginLeft: Width(5), justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={current_location} />
                                      </View>
                                      <View style={{ marginLeft: Width(5.5),marginRight:Width(5) }}>
                                      <Text numberOfLines={1} ellipsizeMode="tail" style={{ fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16), color: '#3A4759',width:'60%' }}>
                                         {predictions.name}
                                                </Text>
                                        <Text numberOfLines={2} ellipsizeMode="tail"  style={{ fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: '#8F8F90',marginRight:Width(5.5)}}>
                                         {predictions.formatted_address}
                                                </Text>
                                      
                        
                                      </View>
                                    </TouchableOpacity>
                                    );
                                })} 
            
           

</ScrollView>
          </SafeAreaView>
         
          <SafeAreaView />
        </BackgroundImage>
      </Container>

    );
  }
}

export default when_location;