import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Alert, ImageBackground,TouchableOpacity, StatusBar, Text, NativeModules, Platform } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Button, Header, Left, Right, Content, Input } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import { Height, FontSize, Width } from "../../config/dimensions";
import { registerUser, emailCheck } from "../../redux/actions/auth";
import NavigationService from "../../services/NavigationService";
import { store } from "../../redux/store";
import { LOGIN, SIGNUP_STEP1 } from "../../redux/actions/types";
import { dateformat } from "../../services/dateformat";
import DeviceInfo from 'react-native-device-info';
import moment from 'moment';
import appleAuth,{ AppleButton, AppleAuthRequestOperation,AppleAuthRequestScope,AppleAuthCredentialState} from '@invertase/react-native-apple-authentication';
import { fonts } from "../../config/constant";
import { sub } from "react-native-reanimated";
//import { GoogleSignin,statusCodes } from 'react-native-google-signin';

let GoogleSignin,statusCodes ;

if(Platform.OS == 'ios')
{
    GoogleSignin = require('react-native-google-signin').GoogleSignin
    statusCodes = require('react-native-google-signin').statusCodes

}

let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height


const backWhite = require("../../../assets/images/backWhite.png");
const bottomRight = require("../../../assets/images/Login/bottomRight.png");
const ic_continue_email = require("../../../assets/images/Signup/ic_continue_email.png");
const ic_continue_facebook = require("../../../assets/images/Signup/ic_continue_facebook.png");
const ic_continue_twitter = require("../../../assets/images/Signup/ic_continue_twitter.png");
const ic_continue_google = require("../../../assets/images/Signup/ic_continue_google.png");
const ic_continue_apple = require("../../../assets/images/Signup/ic_apple.png");

// const { RNTwitterSignIn } = NativeModules

// const Constants = {
//   //Dev Parse keys
//   TWITTER_COMSUMER_KEY: "00TAtRQvqXYydc9n3EYJzkdEI",
//   TWITTER_CONSUMER_SECRET: "y5jB8ZF85LT8iYMBIV0WSpDtjUVafAJbRgpUUBiHT7Oeh1s3YZ"
// }


var subThis;

var dateFormat = require('dateformat');

const FBSDK = require('react-native-fbsdk');
const {
    LoginManager,
    GraphRequest,
    GraphRequestManager,
    AccessToken,
} = FBSDK;

// const infoRequest = new GraphRequest(
//     '/me',
//     null,
//     this._responseInfoCallback,
// );


_responseInfoCallback = (error, result) => {
    console.log('test');
    if (error) {
        alert('Error fetching data: ' + error.toString());
    } else {
        alert('Success fetching data: ' + result.toString());
    }
}


class SignupOptions extends ValidationComponent {

    constructor(props) {
        super(props);
        this.state = {
            uuid:DeviceInfo.getUniqueId(),
        };
        subThis = this

    }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={[styles.headerAndroidnav,{height:44}]}>
                    <StatusBar barStyle="light-content" backgroundColor="#41199B" />
                    <Left style={{ alignItems: 'flex-start', justifyContent: 'flex-start', position:'absolute',left:5,bottom:10}} >
                        <Button transparent onPress={() => {
                            navigation.goBack()
                        }} style={{ width: Width(9), height: Height(3),marginLeft:Width(2)}} >
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </Button>
                    </Left>
                    {/* <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', width:'100%', zIndex:-1 }}>
                        <Text style={[styles.headerTitle]} >Sign Up</Text>
                    </View> */}
                    <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                        <Text style={styles.headerTitle} >Sign Up</Text>
                    </View>
                    {/* <Right style={{ alignItems: 'flex-end', justifyContent: 'flex-end', flex: 0.25 }}> */}
                        {/* <Button transparent style={{ alignSelf: 'baseline' }} onPress={() => { subThis.onDoneInClick() }}><Text style={{ color: 'white' }}>Done</Text></Button> */}
                    {/* </Right> */}
                </Header>
            )
        }
}

fbLogin = (email,name,profile_pic,birthday) =>{

    //NavigationService.reset('TabNavigator')
    //return

    //alert('Email: '+email+' and bday: '+birthday)
    //return

    let data = {social_type:1,dob:moment(birthday).format("YYYYMMDD"),email:email,android_token:global.android_token,ios_token:global.ios_token,device_type:Platform.OS === 'ios' ? 1 : 0}
    registerUser(data).then(res=>{

    //alert(JSON.stringify(res))

      if(res.success){

        if(res.data.is_profile_complete==1){

            store.dispatch({type:LOGIN})
            AsyncStorage.setItem('overlay','false')
            // var date = moment.utc().format('YYYY-MM-DD HH:mm:ss');
            // AsyncStorage.setItem('notidate',date)
             global.isfromlogin = true
            NavigationService.reset('TabNavigator')
            //subThis.props.navigation.navigate('App')
           
        }
        else{
            //store.dispatch({type:LOGIN})
            if(birthday == undefined || birthday == null || birthday == ''){
                NavigationService.navigate('Birthday',{email:email,name:name,is_from_social:true,profile_pic:profile_pic})
            }
            else
            {
                NavigationService.navigate('SignupStep1',{name:name,profile_pic:profile_pic,birthday:false,email:email})
            }

          //subThis.props.navigation.navigate('SignupStep1',{name:name,profile_pic:profile_pic,birthday:false,email:email})
        }
      }
      else{
          alert(res.text)
           //NavigationService.reset('SignupStep1',{name:name,profile_pic:profile_pic,birthday:false})

      }
    }).catch(err=>{
        //alert('Oops your connection seems off, Check your connection and try again')
    })
  }

facebookLogin()
{
let self = this

if(Platform.OS == 'android')
{
    LoginManager.setLoginBehavior("web_only")
}

LoginManager
    .logInWithPermissions(['public_profile', 'email',"user_birthday"])
    .then(function (result) {
    if (result.isCancelled) {
        //console.log('Login cancelled');
        //alert('Login cancelled')
    } else {
        AccessToken
        .getCurrentAccessToken()
        .then((data) => {
            let accessToken = data.accessToken
            console.log(accessToken.toString())
            const responseInfoCallback = (error, result) => {
            if (error) {
                console.log(error)
                alert('Facebook request failed. Try Again.')
            } else {
                console.log('FBlogin',result)
                //alert('FbLogin')
                self.fbLogin(result.email,result.name,result.picture.data.url,result.birthday)
                //checkfbFunc(result.id, result.email, result.name, result.picture.data.url,result.birthday)
            }
            }

            const infoRequest = new GraphRequest(
            '/me?fields=email,name,picture.height(480),birthday',
            null,
            responseInfoCallback, 
            );
            new GraphRequestManager().addRequest(infoRequest).start();
            console.log('responseInfoCallback',responseInfoCallback)

        }).catch(err=>{
            alert('Facebook request failed. Try Again.')
        })
    }
    }, function (error) {
    //console.log('Login fail with error: ' + error);
    //alert('Facebook request failed. Try Again.' + error)
    self.customFacebookLogout()
    });
checkfbFunc = (facebook_id, email, name, photo,birthday) => {
    if (email.length >= 1) {
    this.verifyFbIdws(facebook_id, email, name, photo,birthday)
    } else {
    this.props.navigation.push("SignupStep1", {
        'facebook_id': facebook_id,
        'email': email,
        'name': name,
        'photo': photo,
        'birthday':birthday,
        'code': this.state.validCode
    })
    }
}
}

customFacebookLogout(){
    let self = this
    if(Platform.OS == 'ios'){
    LoginManager.logOut();
    setTimeout(() => {
      self.facebookLogin()
      }, 4);
    }else{
      self.facebookLogin()
  }
}

twitterLogin(email,name)
{
    let data = {social_type:3,dob:'',email:email,android_token:global.android_token,ios_token:global.ios_token,device_type:Platform.OS === 'ios' ? 1 : 0, uuid:this.state.uuid}
    
    //  console.log('DATAAA: '+JSON.stringify(data))
    //  return

    registerUser(data).then(res=>{

      if(res.success){
        if(res.data.is_profile_complete==1){
            store.dispatch({type:LOGIN})
            AsyncStorage.setItem('overlay','false')
            // var date = moment.utc().format('YYYY-MM-DD HH:mm:ss');
            // AsyncStorage.setItem('notidate',date)
             global.isfromlogin = true
            NavigationService.reset('TabNavigator')
            //subThis.props.navigation.navigate('App')
        }
        else{
            //store.dispatch({type:LOGIN})
            NavigationService.navigate('Birthday',{email:email,name:'',is_from_social:true})
            //subThis.props.navigation.navigate('Birthday',{email:email,name:'',is_from_social:true})
          //NavigationService.navigate('SignupStep1',{name:name,profile_pic:'',birthday:false,email:email})
        }
      }
      else{
          alert(res.text)
           //NavigationService.reset('SignupStep1',{name:name,profile_pic:profile_pic,birthday:false})

      }
    }).catch(err=>{
       // alert('Oops your connection seems off, Check your connection and try again')
    })
}

googleLogin(email,name,photo)
{
    let data = {social_type:2,dob:'',email:email,android_token:global.android_token,ios_token:global.ios_token,device_type:Platform.OS === 'ios' ? 1 : 0, uuid:this.state.uuid}
    
    //  console.log('DATAAA: '+JSON.stringify(data))
    //  return

    registerUser(data).then(res=>{

        //alert(JSON.stringify(res))

      if(res.success){
        if(res.data.is_profile_complete==1){
            store.dispatch({type:LOGIN})
            AsyncStorage.setItem('overlay','false')
            // var date = moment.utc().format('YYYY-MM-DD HH:mm:ss');
            // AsyncStorage.setItem('notidate',date)
             global.isfromlogin = true
            NavigationService.reset('TabNavigator')

            //subThis.props.navigation.navigate('App')
        }
        else{
            //store.dispatch({type:LOGIN})
            //NavigationService.navigate('signup',{email:email,name:name,is_from_social:true})
            NavigationService.navigate('Birthday',{email:email,name:name,profile_pic:photo,is_from_social:true})
            //subThis.props.navigation.navigate('Birthday',{email:email,name:name,is_from_social:true})
        }
      }
      else{
          alert(res.text)
           //NavigationService.reset('SignupStep1',{name:name,profile_pic:profile_pic,birthday:false})

      }
    }).catch(err=>{
       // alert('Oops your connection seems off, Check your connection and try again')
    })
}


onPressUseEmail()
{
    NavigationService.navigate('signup')
    //subThis.props.navigation.navigate('signup')
    //NavigationService.reset('TabNavigator')
}

onPressContinueFacebook()
{
    //let self = this
    subThis.customFacebookLogout()
}

async onPressContinueGoogle()
{
    if(Platform.OS == 'ios')
    {
        GoogleSignin.configure();

        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            //this.setState({ userInfo });
            //alert('User Info: '+JSON.stringify(userInfo.user))
    
            if(userInfo != null){
                subThis.googleLogin(userInfo.user.email,userInfo.user.name,userInfo.user.photo)
            }
    
          } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
              // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
              // operation (f.e. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
              // play services not available or outdated
            } else {
              // some other error happened
            }
          }
    }
}

onPressContinueTwitter()
{
    // RNTwitterSignIn.init(Constants.TWITTER_COMSUMER_KEY, Constants.TWITTER_CONSUMER_SECRET)
    // RNTwitterSignIn.logOut()
    // RNTwitterSignIn.logIn()
    //   .then(loginData => {
    //     console.log('TwitterLoginData '+JSON.stringify(loginData))
    //     const { authToken, authTokenSecret, email,userName } = loginData
    //     if (authToken && authTokenSecret) {
    //       //alert('LoggedIn')
    //       subThis.twitterLogin(email,userName)
    //     }
    //   })
    //   .catch(error => {
    //     //alert('Failed '+error)
    //   }
    // )
}

appleLogin(email,name,apple_id)
{

    let data = {social_type:4,dob:'',email:email,apple_user_id:apple_id,android_token:global.android_token,ios_token:global.ios_token,device_type:Platform.OS === 'ios' ? 1 : 0, uuid:this.state.uuid}
    
      console.log('DATAAA: '+JSON.stringify(data))
      //return

    registerUser(data).then(res=>{

        //alert(JSON.stringify(res))

      if(res.success){
        if(res.data.is_profile_complete==1){
            store.dispatch({type:LOGIN})
            AsyncStorage.setItem('overlay','false')
            // var date = moment.utc().format('YYYY-MM-DD HH:mm:ss');
            // AsyncStorage.setItem('notidate',date)
             global.isfromlogin = true
            NavigationService.reset('TabNavigator')
            //subThis.props.navigation.navigate('App')
        }
        else{
            //store.dispatch({type:LOGIN})
            //NavigationService.navigate('signup',{email:email,name:name,is_from_social:true})
            NavigationService.navigate('Birthday',{email:email,name:name,is_from_social:true,is_for_apple:true})
            //subThis.props.navigation.navigate('Birthday',{email:email,name:name,is_from_social:true})
        }
      }
      else{
          //alert(res.text)
           //NavigationService.reset('SignupStep1',{name:name,profile_pic:profile_pic,birthday:false})

      }
    }).catch(err=>{
       // alert('Oops your connection seems off, Check your connection and try again')
    })
}

async onAppleButtonPress() {
    
    const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: AppleAuthRequestOperation.LOGIN,
        requestedScopes: [AppleAuthRequestScope.EMAIL, AppleAuthRequestScope.FULL_NAME],
      });
     
      console.log('appleAuthRequestResponse User: ', appleAuthRequestResponse.user);

      //global.apple_user_id = appleAuthRequestResponse.user

      console.log('Apple: '+appleAuthRequestResponse.fullName.familyName + ' '+ appleAuthRequestResponse.fullName.givenName + ' '+ appleAuthRequestResponse.email)


      if(appleAuthRequestResponse.email == null || appleAuthRequestResponse.email == undefined)
      {
        subThis.appleLogin(appleAuthRequestResponse.user,'',appleAuthRequestResponse.user)
      }
      else
      {
        subThis.appleLogin(appleAuthRequestResponse.email,appleAuthRequestResponse.fullName.givenName + ' '+ appleAuthRequestResponse.fullName.familyName,appleAuthRequestResponse.user)
      }

      // get current authentication state for user
    //   const credentialState = await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user);
     
    //     //alert(credentialState.toString())

    //   // use credentialState response to ensure the user is authenticated
    //   if (credentialState === AppleAuthCredentialState.AUTHORIZED) {
    //         console.log('user is authenticated')
    //   }

}

onPressTerms()
{
    NavigationService.navigate('TermsOfUse')
    //subThis.props.navigation.navigate('TermsOfUse')
}

onPressPrivacyPolicy()
{
    NavigationService.navigate('PrivacyPolicy')
    //subThis.props.navigation.navigate('PrivacyPolicy')
}

onPressEULA()
{
    NavigationService.navigate('EULA')
    //subThis.props.navigation.navigate('PrivacyPolicy')
}

 onLoginClick = () => {
        this.props.navigation.navigate('SocialLogin')
    }

componentDidMount(){
    //alert(store.getState().auth.isLoggedIn)
}
    render() {
        return (
            <Container flex-direction={"row"} style={{ flex: 1,backgroundColor:'#FFFFFF' }} >
                <BackgroundImage >
                    <SafeAreaView />
                    <View style={{backgroundColor:'#F2F4F7', width: '100%', height:220,position: 'absolute', bottom: 0, right: 0}}>
                        <ImageBackground source={bottomRight} style={{ width: 165, height: 143, position: 'absolute', bottom: 0, right: 0}}>
                        </ImageBackground>
                    </View>
                    <View style = {{alignItems:'center'}}>
                        <Text style = {{width:Width(65), fontSize:FontSize(13), fontFamily:fonts.Roboto_Regular, marginTop:26, color:'#8F8F90', textAlign:'center'}}>Create events, view event invites, share when you’re free, and more</Text>
                        <TouchableOpacity style = {{alignItems:'center',backgroundColor:'#fff', borderRadius:5,height:Height(5), width:Width(90),borderWidth:0.5, borderColor:'#C1C1C1', marginTop:Height(3.5), flexDirection:'row'}} activeOpacity = {0.9} onPress = {this.onPressUseEmail}>
                            <Image source = {ic_continue_email} style = {{width:'7%', marginLeft:'2%'}} resizeMode = 'contain'></Image>
                            <Text style = {{fontSize:FontSize(15), fontFamily:fonts.Roboto_Medium,color:'#333333', textAlign:'center', width:'100%', position:'absolute', zIndex:-1}}>Use Email</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style = {{alignItems:'center',backgroundColor:'#fff', borderRadius:5,height:Height(5), width:Width(90),borderWidth:0.5, borderColor:'#C1C1C1', marginTop:Height(1), flexDirection:'row'}} activeOpacity = {0.9} onPress = {this.onPressContinueFacebook}>
                            <Image source = {ic_continue_facebook} style = {{width:'7%', marginLeft:'2%'}} resizeMode = 'contain'></Image>
                            <Text style = {{fontSize:FontSize(15), fontFamily:fonts.Roboto_Medium,color:'#333333', textAlign:'center', width:'100%', position:'absolute', zIndex:-1}}>Continue with Facebook</Text>
                        </TouchableOpacity>
                        {
                            Platform.OS == 'ios' ?
                            <TouchableOpacity style = {{alignItems:'center',backgroundColor:'#fff', borderRadius:5,height:Height(5), width:Width(90),borderWidth:0.5, borderColor:'#C1C1C1', marginTop:Height(1), flexDirection:'row'}} activeOpacity = {0.9} onPress = {this.onPressContinueGoogle}>
                                <Image source = {ic_continue_google} style = {{width:'7%', marginLeft:'2%'}} resizeMode = 'contain'></Image>
                                <Text style = {{fontSize:FontSize(15), fontFamily:fonts.Roboto_Medium,color:'#333333', textAlign:'center', width:'100%', position:'absolute', zIndex:-1}}>Continue with Google</Text>
                            </TouchableOpacity>
                            :
                            null
                        }
                        
                        {/* <TouchableOpacity style = {{alignItems:'center',backgroundColor:'#fff', borderRadius:5,height:Height(5), width:Width(90),borderWidth:0.5, borderColor:'#C1C1C1', marginTop:Height(1), flexDirection:'row'}} activeOpacity = {0.9} onPress = {this.onPressContinueTwitter}>
                            <Image source = {ic_continue_twitter} style = {{width:'7%', marginLeft:'2%'}} resizeMode = 'contain'></Image>
                            <Text style = {{fontSize:FontSize(15), fontFamily:fonts.Roboto_Medium,color:'#333333', textAlign:'center', width:'100%', position:'absolute', zIndex:-1}}>Continue with Twitter</Text>
                        </TouchableOpacity> */}
                        {
                            Platform.OS == 'ios' ?
                            <TouchableOpacity style = {{alignItems:'center',backgroundColor:'#fff', borderRadius:5,height:Height(5), width:Width(90),borderWidth:0.5, borderColor:'#C1C1C1', marginTop:Height(1), flexDirection:'row'}} activeOpacity = {0.9}>
                                <AppleButton buttonStyle={AppleButton.Style.WHITE} buttonType={AppleButton.Type.CONTINUE} style={{width: Width(89),height:Height(4.5)}} onPress={() => this.onAppleButtonPress()}/>
                            </TouchableOpacity>
                            :
                            null
                            // <TouchableOpacity style = {{alignItems:'center',backgroundColor:'#fff', borderRadius:5,height:Height(5), width:Width(90),borderWidth:0.5, borderColor:'#C1C1C1', marginTop:Height(1), flexDirection:'row', justifyContent:'center'}} activeOpacity = {0.9} onPress={() => this.onAppleButtonPress()}>
                            //     <Image source = {ic_continue_apple} style = {{width:15,height:15, marginRight:5}} resizeMode = 'contain'></Image>
                            //     <Text style = {{fontSize:FontSize(15), fontFamily:fonts.Roboto_Medium,color:'#333333', textAlign:'center'}}>Continue with Apple</Text>
                            // </TouchableOpacity>
                        }
                    </View>
                    <View style = {{position:'absolute', bottom:230, backgroundColor:'#fff', height:105, width:'100%', alignItems:'center', justifyContent:'center'}}>
                        <View style = {{flexDirection:'row'}}>
                            <Text style = {{color:'#8F8F90',fontFamily:fonts.Roboto_Regular,fontSize:FontSize(11)}}>By continuing, you agree to Planmesh's </Text>
                            <Text style = {{color:'#007AFF',fontFamily:fonts.Roboto_Regular,fontSize:FontSize(12), textDecorationLine: 'underline'}} onPress = {this.onPressTerms}>Terms of use </Text>
                        </View>
                        <View style = {{flexDirection:'row'}}>
                            <Text style = {{color:'#8F8F90',fontFamily:fonts.Roboto_Regular,fontSize:FontSize(11)}}>and </Text>
                            <Text style = {{color:'#007AFF',fontFamily:fonts.Roboto_Regular,fontSize:FontSize(12), textDecorationLine: 'underline'}} onPress = {this.onPressEULA}>End User License Agreement, </Text>
                            <Text style = {{color:'#8F8F90',fontFamily:fonts.Roboto_Regular,fontSize:FontSize(11)}}>and confirm</Text>
                        </View>
                        <View style = {{flexDirection:'row', marginTop:3}}>
                            <Text style = {{color:'#8F8F90',fontFamily:fonts.Roboto_Regular,fontSize:FontSize(11)}}>that you have read Planmesh's </Text>
                            <Text style = {{color:'#007AFF',fontFamily:fonts.Roboto_Regular,fontSize:FontSize(12), textDecorationLine: 'underline'}} onPress = {this.onPressPrivacyPolicy}>Privacy Policy</Text>
                        </View>
                    </View>
                    <View style = {{position:'absolute', width:'100%',bottom:180, alignItems:'center'}}>
                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: Height(3) }} onPress={() => this.onLoginClick()}>
                            <Text style={{ color: '#333333', fontSize: FontSize(15), fontFamily: "Roboto-Regular", letterSpacing: 0.005 }}>Already have an account? </Text><Text style={{ color: '#007aff', fontSize: FontSize(15), fontFamily: "Roboto-Regular", letterSpacing: 0.005 }}>Log in</Text>
                        </TouchableOpacity>
                    </View>
                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default SignupOptions;
