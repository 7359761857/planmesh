import React, { Component } from "react";
import { SafeAreaView, StyleSheet, Image, ScrollView, Text, FlatList, TouchableOpacity, StatusBar } from "react-native";
import { Container, View, Button, Header, Left, Item, Icon, Right, Content, Input } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import { fonts } from "../../config/constant";
import { getSubCategoryList, AddSubCategoryList } from "../../redux/actions/auth";
import { store } from "../../redux/store";
import { Height, FontSize, Width, colors } from "../../config/dimensions";
import { TextInput } from "react-native-gesture-handler";
import MixpanelManager from '../../../Analytics'
import moment from 'moment';
const backWhite = require("../../../assets/images/backWhite.png");
const searchicon = require("../../../assets/images/Dashboard/search.png");
const closeicon = require("../../../assets/images/Dashboard/close.png");


class Interest extends ValidationComponent {

    constructor(props) {
        super(props);
        issearching = false,
            this.search = null,
            this.array = []
        this.catergory_name = []
        this.state = {
            text: '',
            interest: ['Movies & Videos', 'Dining out', 'Music & Concerts', 'Coffee Breaks', 'Camping', 'Watching Sports', 'Shopping', 'Museums & Arts', 'Fishing & Hunting',],
            count: 0,
            count1: 0,
            cat_id: this.props.navigation.state.params.Category_id,
            Category: [],
            empty: true,
            subCategory: [],
            token: store.getState().auth.user.token,
            login_id: store.getState().auth.user.login_id,
            selected_array: [],
            is_close_visible: false,
            selected_categoryname: []

        };
        this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
        console.log("this.props.navigation.state.params.Category_id", this.props.navigation.state.params.Category_id)
    }

    componentDidMount() {

        //this.search.focus()
        console.log('Category name..', this.state.categoryname)
        let data = {
            token: this.state.token,
            login_id: this.state.login_id,
            category_id: String(this.state.cat_id),
            type: 'register'
        }
        getSubCategoryList(data).then(res => {
            if (res.success) {
                this.setState({ Category: res.data, subCategory: res.data.sub_category_list })
            }
            else {
                alert(res.text)

                this.setState({ empty: false })

            }
        }).catch(err => {
            //alert('Oops your connection seems off, Check your connection and try again')


        })

    }
    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={[styles.headerAndroidnav, { height: 44 }]}>
                    <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack() }}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                        <Text style={styles.headerTitle} >Select Interests</Text>
                    </View>
                </Header>
            )
        }
    }

    makeStyles() {
        return StyleSheet.create({
            view: {
                flexDirection: 'row',
                flexWrap: 'wrap',
                backgroundColor: 'transparent',
                alignItems: 'center',
            },

            text: {
                fontSize: FontSize(14),
                paddingLeft: '4%',
                paddingRight: '4%',
                paddingTop: '1.8%',
                paddingBottom: '1.8%',
                textAlign: 'center',
                color: '#676868',

            },
            selectedText: {
                fontSize: FontSize(14),
                paddingLeft: '4%',
                paddingRight: '4%',
                paddingTop: '1.8%',
                paddingBottom: '1.8%',
                textAlign: 'center',
                color: '#fff',

            },
            touchable: {
                marginLeft: '1.1%',
                marginTop: '1.1%',
                borderRadius: 5,
                borderColor: '#676868',
                borderWidth: 1,
            },
            selectedTouchable: {
                marginLeft: '1.1%',
                marginTop: '1.1%',
                borderRadius: 5,
                borderColor: '#41199B',
                borderWidth: 1,
                backgroundColor: '#41199B',

            }
        })
    }
    checkForFriends = (item) => {
        if (this.state.selected_array.includes(item.sub_category_id)) {
            return true
        } else {
            return false
        }

    }

    gotoPlaces = () => {


        let data = {
            token: this.state.token,
            login_id: this.state.login_id,
            sub_category_id: String(this.state.selected_array)
        }
        AddSubCategoryList(data).then(res => {
            if (res.success) {
                const SELECTED_INTEREST = this.state.selected_categoryname.map(item => item)
                const SELECTED_CATEGORY = this.state.Category.map(item => item.category_name)

                //Event Property
                const Interest = {
                    '$email': store.getState().auth.user.email_address,
                    'During Sign Up': 'True',
                    'Interests Category Selected': JSON.stringify(SELECTED_CATEGORY),
                    'Interests Selected': JSON.stringify(SELECTED_INTEREST),
                    'Total Interests': SELECTED_INTEREST.length,
                    'Total Interests Category': SELECTED_CATEGORY.length

                }
                //User Property
                this.mixpanel.identify(store.getState().auth.user.email_address);
                this.mixpanel.getPeople().increment('Total Interests Added', SELECTED_INTEREST.length);
                this.mixpanel.getPeople().setOnce({ 'First Add Interests': moment().format('MMMM Do YYYY, h:mm:ss a') });
                this.mixpanel.getPeople().set("Last Add Interests", moment().format('MMMM Do YYYY, h:mm:ss a'));
                this.mixpanel.getPeople().set("Interests Category Selected", JSON.stringify(SELECTED_CATEGORY));
                this.mixpanel.getPeople().set("Interests Selected", JSON.stringify(SELECTED_INTEREST));
                this.mixpanel.getPeople().set("Total Interests", SELECTED_INTEREST.length);
                this.mixpanel.getPeople().set("Total Interests Category", SELECTED_INTEREST.length);

                // this.mixpanel.track('Add Interests Test', Interest);
                this.mixpanel.track('Add Interests', Interest);

                console.log('Final Data ==> ', Interest)
                this.props.navigation.push('findFriends')
            }
            else {
                alert(res.text)

            }
        }).catch(err => {
            //alert('Oops your connection seems off, Check your connection and try again')


        })

    }

    onClearTextPressed() {
        this.setState({ is_close_visible: false, text: '' })
    }

    SearchFilterFunction(text) {

        if (text != '') {
            this.issearching = true
        }

        this.setState({ text: text, is_close_visible: true })

        if (this.state.text.length == 1) {
            this.issearching = false
        }

        if (text == '') {
            this.setState({ is_close_visible: false })
        }


    }

    render() {
        const styles1 = this.makeStyles()

        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }} >
                <Toastt ref="toast"></Toastt>
                <BackgroundImage >
                    <SafeAreaView />
                    <View style={{ height: Height(1), backgroundColor: colors.white }}></View>
                    <SafeAreaView>

                        <View style={{ marginHorizontal: Width(5), height: Height(5), backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center', marginTop: 7, marginBottom: 10 }}>
                            <View style={{ flexDirection: 'row', backgroundColor: '#EEEEEE', height: '90%', borderRadius: 10, alignItems: 'center', width: '100%', paddingHorizontal: 12 }}>
                                <Image source={searchicon} resizeMode='contain' />
                                <TextInput style={{
                                    fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16), width: '100%', height: '100%', marginLeft: 12,
                                    paddingVertical: 0,
                                }} value={this.state.text} placeholder='Search interests' placeholderTextColor='#8F8F90'
                                    onChangeText={text => this.SearchFilterFunction(text)}
                                    ref={search => this.search = search}
                                >
                                </TextInput>

                                {
                                    this.state.is_close_visible == true ?
                                        <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', height: 25, width: 25, position: 'absolute', right: 10 }} onPress={() => this.onClearTextPressed()}>
                                            <Image source={closeicon} resizeMode='stretch' style={{ height: 17, width: 17 }} />
                                        </TouchableOpacity>
                                        :
                                        null
                                }

                            </View>
                        </View>

                        <ScrollView style={{ marginBottom: 120 }} keyboardDismissMode='on-drag'>
                            <View style={{ backgroundColor: '#fff' }}>
                                {this.state.empty == false ?
                                    <Text style={{ textAlign: 'center', marginTop: Height(15), marginBottom: Height(15), alignItems: 'center', justifyContent: 'center', fontFamily: fonts.Raleway_Medium, FontSize: 20, color: colors.fontDarkGrey }}> No Interests!</Text> :
                                    <View style={{ backgroundColor: 'transparent' }}>
                                        {this.state.Category.map(item => {
                                            let items = [];
                                            items = item.sub_category_list
                                            return (
                                                <View style={{ marginLeft: '4%', marginRight: '4%', marginTop: 24, backgroundColor: 'transparent' }}>
                                                    <Text style={{ width: '70%', fontFamily: fonts.Raleway_Bold, color: colors.dargrey, fontSize: FontSize(16), lineHeight: 20, marginBottom: 10, marginLeft: 2 }}>
                                                        {item.category_name}
                                                    </Text>
                                                    <View style={styles1.view}>
                                                        {items.map(item1 => {
                                                            if (item1.sub_category_name.includes(this.state.text)) {
                                                                return (
                                                                    <TouchableOpacity
                                                                        onPress={() => {
                                                                            this.setState({ selected_array: '' })
                                                                            if (this.array.includes(item1.sub_category_id) == true) {
                                                                                let item_index = this.array.indexOf(item1.sub_category_id)
                                                                                this.array.splice(item_index, 1)
                                                                                this.catergory_name.splice(item_index, 1)
                                                                            } else {
                                                                                this.array.push(item1.sub_category_id)
                                                                                this.catergory_name.push(item1.sub_category_name)
                                                                            }
                                                                            this.setState({ selected_array: this.array, selected_categoryname: this.catergory_name })
                                                                        }}
                                                                        style={this.checkForFriends(item1) == true ? styles1.selectedTouchable : styles1.touchable}>
                                                                        <Text style={this.checkForFriends(item1) == true ? styles1.selectedText : styles1.text}>{item1.sub_category_name}</Text>
                                                                    </TouchableOpacity>
                                                                )
                                                            }
                                                            else {
                                                                return null
                                                            }

                                                        })}
                                                    </View>
                                                </View>
                                            );
                                        })}
                                    </View>
                                }

                            </View>
                            <View style={{ height: Height(8) }}></View>

                        </ScrollView>
                    </SafeAreaView>

                    {this.state.selected_array.length < 5 ?
                        <View style={{
                            bottom: 0, position: 'absolute', height: Height(6), width: '93%', alignItems: 'center', backgroundColor: '#8F8F90', borderRadius: Height(3), marginBottom: Height(3), alignSelf: 'center', justifyContent: 'center',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 3,
                            },
                            shadowOpacity: 0.29,
                            shadowRadius: 4.65,

                            elevation: 7,
                        }}>
                            <Text style={[{ fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}> Next ({this.state.selected_array.length}/5)</Text>
                        </View>
                        :
                        <TouchableOpacity style={{
                            bottom: 0, position: 'absolute', height: Height(6), width: '93%', alignItems: 'center', backgroundColor: colors.pink, borderRadius: Height(3), marginBottom: Height(3), alignSelf: 'center', justifyContent: 'center',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 3,
                            },
                            shadowOpacity: 0.29,
                            shadowRadius: 4.65,

                            elevation: 7,
                        }}
                            onPress={this.gotoPlaces}>
                            <Text style={[{ fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Next</Text>
                        </TouchableOpacity>}


                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default Interest;
