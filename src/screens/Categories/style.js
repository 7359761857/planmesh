const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import { isIphoneX } from '../Default/is-iphone-x'
const HEADER_SIZE = isIphoneX() ? 84 : 64;
const HEADER_PADDING_SIZE = isIphoneX() ? 40 : 16;
import { Height, FontSize, Width, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";

export default {

    headerAndroidnav: {
        backgroundColor: '#41199B',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        ...Platform.select({
            ios: {
                height: HEADER_SIZE,
                paddingTop: HEADER_PADDING_SIZE,
            },
            android: {
            },
        }),
    },
    headerTitle: {
        letterSpacing: 0.02,
        fontSize: 20,
        fontFamily: "Raleway-Medium",
        color: 'white',
        ...Platform.select({
            ios: {
              fontWeight: '500',
            },
            android: {
            },
          })
    },
    containerView:{
        marginLeft:20,justifyContent:'center',flex:1
    },
    containerTitiel:{
        fontSize:18,fontFamily:"Raleway-ExtraBold",color:"#41199b",letterSpacing:0.02,
        ...Platform.select({
            ios: {
              fontWeight: '800',
            },
            android: {
            },
          })
    },
    containerSubTitle:{
        fontSize:16,fontFamily:"Raleway-Medium",color:"#3a4759",letterSpacing:0.02,
        ...Platform.select({
            ios: {
              fontWeight: '500',
            },
            android: {
            },
          })
    },
   
    boxViews:
    {
        columns: 2,
        marginTop: Height(3),
        justifyContent: 'space-between',
        marginHorizontal: Width(3.5)


    },
    box1:
    {
        width: Platform.OS == 'ios' ? Height(20):Height(23),
        height:Platform.OS == 'ios' ? Height(17):Height(20),
        borderWidth: 1.5,
        borderColor:'#B4B7BA'


    },

    text:
    {
      marginTop:Height(0.3),
        color:colors.dargrey,
        fontSize:FontSize(13),
        fontFamily:fonts.Raleway_Bold
    },
   
    TopText: {
        alignSelf: 'center',
        marginTop: Height(2),
        fontFamily: fonts.Raleway_Bold,
        fontWeight: 'bold',
        fontSize: FontSize(16),
        color:colors.dargrey
    },
    bottomText: {
        marginTop: Height(1),
        textAlign: 'center',
        alignSelf:'center',
        width:'60%',
        lineHeight: 20,
        fontFamily:fonts.Roboto_Regular,
        fontSize: FontSize(13),
        color: '#8F8F90',
    },
    item: {
        borderWidth: 1,
        borderColor: '#333',    
        backgroundColor: '#FFF',
      },
      label: {
        color: '#333'
      },
      itemSelected: {
        backgroundColor: '#333',
      },
      labelSelected: {
        color: '#FFF',
      },
};
