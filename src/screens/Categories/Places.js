import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView,StyleSheet, Text, TextInput, TouchableOpacity, StatusBar } from "react-native";
import { Container, View, Button, Header, Left, Item, Icon, Right, Content, Input } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import FloatingLabel from 'react-native-floating-labels';
import { fonts } from "../../config/constant";
import { PlaceList,AddPlaceList } from "../../redux/actions/auth";
//import TagSelector from 'react-native-tag-selector';

import { Height, FontSize, Width, colors } from "../../config/dimensions";
import SplashScreen from "react-native-splash-screen";
//import SelectableChips from 'react-native-chip/SelectableChips'
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
import { store } from "../../redux/store";


const backWhite = require("../../../assets/images/backWhite.png");
const btnBg = require("../../../assets/images/btnBg.png");

const searchicon = require("../../../assets/images/Dashboard/search.png");
const closeicon = require("../../../assets/images/Dashboard/close.png");

 var votes = [];
        let array = [];
class Places extends ValidationComponent {

    constructor(props) {
        super(props);
        
        this.state = {
            text: '',
            placeList:[],
            interest: ['Movies & Videos', 'Dining out', 'Music & Concerts', 'Coffee Breaks', 'Camping', 'Watching Sports', 'Shopping', 'Museums & Arts', 'Fishing & Hunting',],
            selected_array:[],
             token: store.getState().auth.user.token,
            login_id: store.getState().auth.user.login_id,
            is_close_visible:false,
        };
    }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={[styles.headerAndroidnav,{height:44}]}>
                    <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack() }}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity></View>
                    <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                        <Text style={styles.headerTitle} >Places</Text>
                    </View>
                </Header>
            )
        }
    }
  
    componentDidMount() {
        SplashScreen.hide()
         let data = {
            token: this.state.token,
            login_id: this.state.login_id,
           limit:'1000',
           offset:'0'
        }
        PlaceList(data).then(res => {
            if (res.success) {
                this.setState({placeList:res.data})
            }
            else {

                this.setState({ empty: false })

            }
        }).catch(err => {
            //alert('Oops your connection seems off, Check your connection and try again')


        })

    }
      onFreeMoneyClick = () => {
           let data = {
            token: this.state.token,
            login_id: this.state.login_id,
            place_id: String(this.state.selected_array)
        }
        AddPlaceList(data).then(res => {
            if (res.success) {
               this.props.navigation.push('findFriends')
            }
            else {


            }
        }).catch(err => {


        })
        
       
    }
    makeStyles() {
        return StyleSheet.create({
            view: {
                flexDirection: 'row',
                flexWrap: 'wrap',
                backgroundColor: '#fff',
                alignItems: 'center',

            },

            text: {
                fontSize: FontSize(14),
                paddingLeft: '4%',
                paddingRight: '4%',
                paddingTop: '1.8%',
                paddingBottom: '1.8%',
                textAlign: 'center',
                color: '#676868',

            },
            selectedText:{
                fontSize: FontSize(14),
                paddingLeft: '4%',
                paddingRight: '4%',
                paddingTop: '1.8%',
                paddingBottom: '1.8%',
                textAlign: 'center',
                color: '#fff',
            },
            touchable: {
                marginLeft: '1.1%',
                marginTop: '1.1%',
                borderRadius: 5,
                borderColor: '#676868',
                borderWidth: 1,
            },
            selectedTouchable: {
                marginLeft: '1.1%',
                marginTop: '1.1%',
                borderRadius: 5,
                borderColor: '#41199B',
                borderWidth: 1,
                backgroundColor: '#41199B',
               
            }
        })
    }
    checkForFriends = (item) => {
        if (this.state.selected_array.includes(item.place_id)) {
            return true
        } else {
            return false
        }

    }

    onClearTextPressed()
    {
        this.setState({is_close_visible:false,text:''})
    }

    SearchFilterFunction(text)
    {
    
        if(text != '')
        {
            this.issearching = true
        }

        this.setState({text:text, is_close_visible:true})

        if(this.state.text.length == 1)
        {
            this.issearching = false
        }

        if(text == '')
        {
            this.setState({is_close_visible:false})
        }

    }

    render() {
        const styles1 = this.makeStyles()

        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }} >
                <BackgroundImage >
                    <SafeAreaView />
                    <View style={{ height: Height(1), backgroundColor: colors.white }}></View>

                    <View style={{ backgroundColor: '#fff' }}>
                            <View style = {{marginHorizontal:Width(5), height:Height(5), backgroundColor:'transparent', alignItems:'center', justifyContent:'center', marginTop:7, marginBottom:10}}>
                                <View style = {{flexDirection:'row', backgroundColor:'#EEEEEE',height:'90%', borderRadius:10, alignItems:'center', width:'100%', paddingHorizontal:12}}>
                                    <Image source={searchicon} resizeMode = 'contain'/>
                                    <TextInput style={{fontFamily:fonts.Roboto_Regular,fontSize:FontSize(16), width:'100%', height:'100%', marginLeft:12}} value = {this.state.text} placeholder = 'Search places' placeholderTextColor='#8F8F90'
                                        onChangeText={text => this.SearchFilterFunction(text)}
                                    >
                                    </TextInput>
                                    {
                                        this.state.is_close_visible == true ?
                                        <TouchableOpacity style ={{justifyContent:'center',alignItems:'center',height:25,width:25, position:'absolute',right:10}} onPress={ () => this.onClearTextPressed()}>
                                            <Image source={closeicon} resizeMode = 'stretch' style = {{height:17,width:17}}/>
                                        </TouchableOpacity>
                                        :
                                        null
                                    }
                                </View>
                            </View>
                    </View>

                    <Text style={{ width:'100%',textAlign:'center',alignSelf:"center",fontFamily: fonts.Raleway_Bold, color: colors.dargrey, fontSize: FontSize(16),lineHeight: 20,marginVertical:10}}>Choose five or more places your like</Text>

                    <ScrollView style={{alignSelf:'center', marginBottom:50}}>
                    <View style={{ marginHorizontal: Width(3) }}>
                            {/* <View style={{ margin: '3%' }}> */}
                                {/* <Text style={{ width:'100%',textAlign:'center',alignSelf:"center",fontFamily: fonts.Raleway_Bold, color: colors.dargrey, fontSize: FontSize(16),lineHeight: 20,marginVertical:10}}>Choose five or more places your like</Text> */}
                            {/* </View> */}
                            <View style={styles1.view}>
                                                    {this.state.placeList.map(item1=>{

                                                        if(item1.place_name.includes(this.state.text))
                                                        {
                                                            return(
                                                                <TouchableOpacity
                                                                onPress={() => {
                                                                    this.setState({ selected_array: '' })
                                                                    if (array.includes(item1.place_id) == true) {
                                                                        let item_index = array.indexOf(item1.place_id)
                                                                        array.splice(item_index, 1)
                                
                                                                    } else {
                                                                        array.push(item1.place_id)
                                
                                                                    }
                                                                    this.setState({ selected_array: array })
                                                                }}
                                                                style={this.checkForFriends(item1) == true ? styles1.selectedTouchable : styles1.touchable}>
                                                                <Text style={this.checkForFriends(item1) == true ? styles1.selectedText : styles1.text}>{item1.place_name}</Text>
                                                            </TouchableOpacity>
                                                            )
                                                        }
                                                        else
                                                        {
                                                                return null
                                                        }
                                                    })}
                                                    </View>
                      

                        </View>
                    </ScrollView>

                    {this.state.selected_array.length < 5 ?
                        <View style={{
                            bottom: 0, position: 'absolute', height: Height(6), width: '93%', alignItems: 'center', backgroundColor: '#8F8F90', borderRadius: Height(3), marginBottom: Height(3), alignSelf: 'center', justifyContent: 'center',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 3,
                            },
                            shadowOpacity: 0.29,
                            shadowRadius: 4.65,

                            elevation: 7,
                        }}>
                            <Text style={[{ fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}> Continue ({this.state.selected_array.length}/5)</Text>
                        </View>
                        :
                        <TouchableOpacity style={{
                            bottom: 0, position: 'absolute', height: Height(6), width: '93%', alignItems: 'center', backgroundColor: colors.pink, borderRadius: Height(3), marginBottom: Height(3), alignSelf: 'center', justifyContent: 'center',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 3,
                            },
                            shadowOpacity: 0.29,
                            shadowRadius: 4.65,

                            elevation: 7,
                        }}
                          onPress={this.onFreeMoneyClick}>
                            <Text style={[{ fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Continue</Text>
                        </TouchableOpacity>}
                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default Places;
