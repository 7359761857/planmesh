import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, FlatList, Platform, TouchableOpacity, StatusBar, Text, TouchableHighlight } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Button, Header, Left, Item, Icon, Right, Content, Input } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import { getCategoryList, AddCategoryList } from "../../redux/actions/auth";
const backWhite = require("../../../assets/images/backWhite.png");
const SelectRadio = require("../../../assets/images/selectRadio.png");
const btnBg = require("../../../assets/images/btnBg.png");
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
import { store } from "../../redux/store";
const unSelectRadio = require("../../../assets/images/Dashboard/unSelectRadio.png");
const flowers = require("../../../assets/images/Dashboard/avtar.png");
const selectedcat = require("../../../assets/images/selected_cat.png");
import Toast from 'react-native-simple-toast';
import { tupleTypeAnnotation } from "@babel/types";
import SplashScreen from "react-native-splash-screen";
import RNProgressHUB from 'react-native-progresshub';

const search_img = require("../../../assets/images/Dashboard/search.png");


class when_friends extends ValidationComponent {
    constructor(props) {
        super(props);
        this.arrayholder = [];
        this.array = [],
            this.name = []
        this.state = {
            count: 0,
            value: null,
            limit: '',
            offset: '',
            friendName: '',
            text: '',
            selected_array: [],
            isActive: false,
            Category: [],
            value: '',
            receiver_id: '',
            empty: true,
            token: store.getState().auth.user.token,
            login_id: store.getState().auth.user.login_id,

        };
    }
    componentDidMount() {
        SplashScreen.hide()
        RNProgressHUB.showSpinIndeterminate();

        let data = {
            token: this.state.token,
            login_id: this.state.login_id,
            limit: this.state.limit,
            offset: this.state.offset
        }
        getCategoryList(data).then(res => {
            if (res.success) {
                this.setState({ Category: res.data })
                RNProgressHUB.dismiss();
            }
            else {
                this.setState({ empty: false })
                RNProgressHUB.dismiss();

            }
        }).catch(err => {
            RNProgressHUB.dismiss();
        })

    }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={[styles.headerAndroidnav, { height: 44 }]}>
                    <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                        {/* <TouchableOpacity onPress={() => { navigation.goBack()}}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity> */}
                    </View>
                    <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                        <Text style={styles.headerTitle} >Interests</Text>
                    </View>
                </Header>
            )
        }
    }
    checkForFriends = (item) => {

        if (this.state.selected_array.includes(item.category_id)) {
            return true
        } else {
            return false
        }

    }
    gotoFavorite = () => {
        let data = {
            token: this.state.token,
            login_id: this.state.login_id,
            category_id: String(this.state.selected_array)
        }
        AddCategoryList(data).then(res => {
            if (res.success) {
                const filterarray = this.state.Category.filter(item => this.state.selected_array.includes(item.category_id))
                this.props.navigation.navigate('Interest', { Category_id: this.state.selected_array, Category_Name: filterarray })
            }

            else {
                this.setState({ empty: false })

            }
        }).catch(err => {
            //alert('Oops your connection seems off, Check your connection and try again')
        })


    }
    render() {
        let value = ''
        str = ''
        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }} >
                <Toastt ref="toast"></Toastt>
                <BackgroundImage >
                    <SafeAreaView />
                    <View style={{ height: Height(1), backgroundColor: colors.white }}></View>
                    <SafeAreaView>
                        <ScrollView style={{ marginBottom: 70 }}>
                            <View style={{ backgroundColor: '#fff' }}>
                                <Text style={styles.TopText}>
                                    Select a few categories to get started
                                </Text>
                                <Text style={styles.bottomText}>
                                    Your interests helps your friends make plans with you
                                </Text>
                                {this.state.empty == false ?
                                    <Text style={{ textAlign: 'center', marginTop: Height(15), marginBottom: Height(15), alignItems: 'center', justifyContent: 'center', fontFamily: fonts.Raleway_Medium, FontSize: 20, color: colors.fontDarkGrey }}> No Friends!</Text> :
                                    <View style={{ flexWrap: 'wrap', flexDirection: 'row' }}>
                                        {this.state.Category.map((item, index) => {
                                            return (
                                                <View key={index} style={[index % 2 != 0 ? { width: '48%', } : { width: '48%', }, { paddingTop: Height(1), backgroundColor: '#FFF' }, index % 2 != 0 ? { paddingLeft: '4%' } : { paddingLeft: '4%' }]}>
                                                    <TouchableOpacity onPress={() => {

                                                        if (this.array.includes(item.category_id) == true) {
                                                            let item_index = this.array.indexOf(item.category_id)
                                                            this.array.splice(item_index, 1)
                                                            this.name.splice(item_index, 1)

                                                        } else {
                                                            this.array.push(item.category_id)
                                                        }
                                                        this.setState({ selected_array: this.array })
                                                        this.setState({ count: this.state.selected_array.length })

                                                    }} style={this.checkForFriends(item) == true ? { height: Platform.OS == 'ios' ? Height(17) : Height(20), width: Platform.OS == 'ios' ? Height(20) : Height(23), borderWidth: 0, borderColor: colors.pink, borderRadius: 5 } :
                                                        { height: Platform.OS == 'ios' ? Height(17) : Height(20), width: Platform.OS == 'ios' ? Height(20) : Height(23), borderWidth: 0, borderColor: '#B4B7BA', borderRadius: 5 }}>
                                                        <Image style={{ height: '100%', width: '100%', borderRadius: 5 }} source={this.checkForFriends(item) == true ? selectedcat : { uri: item.category_image }} resizeMode='contain' />
                                                    </TouchableOpacity>
                                                    <Text style={styles.text}>{item.category_name}</Text>
                                                </View>

                                            );
                                        })}
                                    </View>}
                                <View style={{ height: Height(2) }}></View>

                            </View>
                        </ScrollView>
                    </SafeAreaView>
                    {this.state.count < 5 ?
                        <View style={{
                            bottom: 0, position: 'absolute', height: Height(6), width: '93%', alignItems: 'center', backgroundColor: '#8F8F90', borderRadius: Height(3), marginBottom: Height(3), alignSelf: 'center', justifyContent: 'center',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 3,
                            },
                            shadowOpacity: 0.29,
                            shadowRadius: 4.65,

                            elevation: 7,
                        }}>
                            <Text style={[{ fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}> Next ({this.state.count}/5)</Text>
                        </View>
                        :
                        <TouchableOpacity style={{
                            bottom: 0, position: 'absolute', height: Height(6), width: '93%', alignItems: 'center', backgroundColor: colors.pink, borderRadius: Height(3), marginBottom: Height(3), alignSelf: 'center', justifyContent: 'center',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 3,
                            },
                            shadowOpacity: 0.29,
                            shadowRadius: 4.65,

                            elevation: 7,
                        }}
                            onPress={this.gotoFavorite}>
                            <Text style={[{ fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Next</Text>
                        </TouchableOpacity>}


                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default when_friends;