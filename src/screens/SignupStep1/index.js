import React, { Component } from "react";
import { SafeAreaView, Linking, Dimensions, Image, Alert, ImageBackground, TouchableOpacity, StatusBar, Platform } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Text, Button, Header, Left, Right, Content, Item, Input } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import FloatingLabel from 'react-native-floating-labels';
import ImagePicker from 'react-native-image-picker';
//import PhoneInput from 'react-native-phone-input'
import publicIP from 'react-native-public-ip';
import { Width, Height, FontSize } from "../../config/dimensions";
import CountryPiker from './../../services/countries.json'
import _ from "lodash";
import { updateUser, registerUser } from "../../redux/actions/auth";
import NavigationService from "../../services/NavigationService";
import { store } from "../../redux/store";
import { UPDATE_USER, LOGIN } from "../../redux/actions/types";
import SplashScreen from "react-native-splash-screen";
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import { capitalize_Words } from "../../services/util";
import { fonts } from "../../config/constant";
import AntDesign from 'react-native-vector-icons/AntDesign';
import DeviceInfo from 'react-native-device-info';
import Geolocation from '@react-native-community/geolocation';
import moment from 'moment'
let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height
import reactNativeProgresshub from "react-native-progresshub";


const backWhite = require("../../../assets/images/backWhite.png");
const camera_icon = require("../../../assets/images/Signup/camera_icon.png");
const current_location_icon = require("../../../assets/images/Signup/current_location_icon.png");
const filled_current_location_icon = require("../../../assets/images/current_location.png");

const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
const btnBg = require("../../../assets/images/btnBg.png");

import MixpanelManager from '../../../Analytics'
class SignUpStep1 extends ValidationComponent {

    constructor(props) {
        super(props);
        this.prevPhone = '+1'
        let params = props.navigation.state.params
        //let params = {mm:''}
        this.state = {
            labelStyleBio: styles.labelInputUnFocus,
            styleBio: styles.formInputUnFocus,
            labelStyleEmail: styles.labelInputUnFocus,
            labelStylePhone: styles.labelInputUnFocus,
            styleEmail: styles.formInputUnFocus,
            stylePhone: styles.formInputUnFocus,
            login_id: store.getState().auth.user.login_id,
            fullName: params.name ? params.name : '',
            email: params.email ? params.email : '',
            birthday: params.birthday ? params.birthday : '',
            phone: '',
            isPhotochange: params.profile_pic ? true : false, btnDoneDisable: true, btnDoneImage: btnBgGrey,
            labelStylePhoneError: null,
            selectedImageURI: params.profile_pic ? params.profile_pic : '',
            bio: "",
            currentLocation: "",
            country: '',
            isenableGps: false,
            fullNameTouch: false,
            isFullNameFocus: false,
            isPhoneFocus: false,
            phoneTouch: false,
            isFullNameValid: false,
            signup_step1: store.getState().auth.signup_step1,
            device_type: Platform.OS == 'android' ? 0 : 1,
            uuid: DeviceInfo.getUniqueId(),
            is_from_social: this.props.navigation.state.params.is_from_social != null ? this.props.navigation.state.params.is_from_social : false

        };
        this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
        publicIP()
            .then(ip => {
                console.log(ip);
                this.setState({ ip: ip })
            })
            .catch(error => {
                console.log(error);
                // 'Unable to get IP address.'
            });
        this.props.navigation.addListener('didFocus', () => {
            //this.getLocation()
        })
    }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={[styles.headerAndroidnav, { height: 44 }]}>
                    <StatusBar barStyle="light-content" backgroundColor="#41199B" />
                    {/* <Left style={{ alignItems: 'flex-start', justifyContent: 'flex-start', flex: 0.2, }} >
                        <Button transparent onPress={() => {
                            navigation.goBack()
                        }} style={{ width: Width(9), height: Height(3),marginLeft:Width(2) }} >
                            <Image source={backWhite} style={{  tintColor: "white" }} />
                        </Button>
                    </Left>
                    <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', flex: 0.6 }}>
                        <Text style={[styles.headerTitle]} >Sign Up</Text>
                    </View>
                    <Right style={{ alignItems: 'flex-end', justifyContent: 'flex-end', flex: 0.25 }}>
                    </Right> */}
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack() }}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                        <Text style={styles.headerTitle} >Sign Up</Text>
                    </View>
                </Header>
            )
        }
    }
    onChangeTextEmail = (fullName) => {
        const self = this;

        this.setState({ fullName: fullName }, () => {
            //this.validtion()
            this.onBlurfullNamel()
        })

    }

    onClickSelectPhoto = () => {

        if (this.state.is_from_social == true) {
            return
        }

        var options = {
            title: '',
            allowsEditing: true,
            maxWidth: 300,
            maxHeight: 300,
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                console.log('Response = ', source);
                this.setState({
                    selectedImageURI: response.uri,
                    isPhotochange: true,
                }, () => this.validtion())
            }
        });
    }

    onNextClick = () => {

        let { fullName, bio, currentLocation, login_id, email } = this.state
        var username = this.titleCase(this.state.fullName)

        if (this.validtion()) {
            if (this.props.navigation.state.params.name || this.state.is_from_social) {
                // let country_code =  `+`+this.phone.getCountryCode()
                let country_code = '+1'

                var data = {}
                if (this.state.is_from_social) {
                    data = { email: email, name: username.trim(), bio: bio, city: currentLocation, country_code: country_code, mobile_number: this.state.phone, profile_pic: this.state.selectedImageURI, dob: this.state.birthday }
                }
                else {
                    data = { email: email, name: username.trim(), bio: bio, city: currentLocation, country_code: country_code, mobile_number: this.state.phone, profile_pic: this.state.selectedImageURI }
                }

                let data1 = { name: fullName, bio: bio, city: currentLocation, country_code: country_code, mobile_number: this.state.phone, profile_pic: this.state.selectedImageURI }

                console.log('SignUpRequest1: ' + JSON.stringify(data))
                //return
                console.log('Request ==> ', social_type)
                const mixpanel_data_with_social = {
                    'Login Type' : 'Social',
                    'Brithday': this.state.birthday,
                    'Phone Number': this.state.phone,
                    '$name': fullName,
                    'Bio': bio,   
                    '$email': email,
                }
                console.log('mixpanel_data_with', mixpanel_data_with_social)
                registerUser(Object.assign(data, { device_type: this.state.device_type, uuid: this.state.uuid, android_token: global.android_token, ios_token: global.ios_token, social_type: 1 })).then(res => {
                    if (res.success) {
                        store.dispatch({ type: LOGIN })
                        store.dispatch({ type: UPDATE_USER, data: res.data })
                        AsyncStorage.setItem('overlay', 'true')
                        // this.mixpanel.identify(email);
                        // this.mixpanel.registerSuperProperties({ 'email': email });
                        this.mixpanel.identify(email);
                        this.mixpanel.getPeople().set('Login Type', 'Social');
                        this.mixpanel.getPeople().set('$email', email);
                        this.mixpanel.getPeople().set('Brithday', this.state.birthday);
                        this.mixpanel.getPeople().set('Phone Number', this.state.phone);
                        this.mixpanel.getPeople().set('$name', fullName);
                        this.mixpanel.getPeople().set('Bio', bio);
                        // this.mixpanel.getPeople().set('$avatar', this.state.selectedImageURI)
                        this.mixpanel.track('Sign Up', mixpanel_data_with_social);
                        NavigationService.reset('Categories')
                        //NavigationService.navigate('Categories')
                    }
                    else {
                        alert(res.text)
                    }
                }).catch(err => {
                    // alert('Oops your connection seems off, Check your connection and try again')
                })
                registerUser(Object.assign(this.state.signup_step1,data1)).then(res=>{
                        if(res.success){
                        store.dispatch({type:UPDATE_USER,data:res.data})
                        store.dispatch({type:LOGIN})
                        NavigationService.reset('equityShare')
                    }
                    else{
                        alert(res.text)
                    }
                }).catch(err=>{
                    alert(err)
                })
            }
            else {

                //let country_code =  `+`+this.phone.getCountryCode()
                //alert(country_code)
                let country_code = '+1'
                let data = { login_id: login_id, name: fullName, bio: bio, city: currentLocation, country_code: country_code, mobile_number: this.state.phone }
                let data1 = { name: fullName, bio: bio, city: currentLocation, country_code: country_code, mobile_number: this.state.phone }
                const mixpanel_data_without_social = {
                    'Login Type' : 'Email',
                    'Brithday': this.state.birthday,
                    'Phone Number': this.state.phone,
                    '$name': fullName,
                    'Bio': bio,   
                    '$email': email,
                }
                console.log('mixpanel_data_without_social', mixpanel_data_without_social)
                console.log('SignUpRequest: ' + JSON.stringify(data))

                console.log('RegisterREQUEST: ' + JSON.stringify(Object.assign(this.state.signup_step1, data1)))
       
                if (this.state.selectedImageURI == '') {
                    registerUser(Object.assign(this.state.signup_step1, data1)).then(res => {

                        //console.log('RegisterRESPONSE: '+JSON.stringify(res))

                        if (res.success) {
                            store.dispatch({ type: UPDATE_USER, data: res.data })
                            store.dispatch({ type: LOGIN })
                            // this.mixpanel.registerSuperPropertiesOnce({ 'email': email });
                            this.mixpanel.identify(email);
                            this.mixpanel.getPeople().set('$email', email);
                            this.mixpanel.getPeople().set('Brithday', this.state.birthday);
                            this.mixpanel.getPeople().set('Phone Number', this.state.phone);
                            this.mixpanel.getPeople().set('$name', fullName);
                            this.mixpanel.getPeople().set('Bio', bio);
                        
                            this.mixpanel.getPeople().set('Login Type', 'Email');
                            this.mixpanel.track('Sign Up', mixpanel_data_without_social);
                            // this.mixpanel.timeEvent("Signup Complete");

                            AsyncStorage.setItem('overlay', 'true')
                            NavigationService.reset('Categories')
                            // this.mixpanel.identify(email);

                            //NavigationService.navigate('Categories')
                        }
                        else {
                            alert(res.text)
                        }
                    }).catch(err => {
                        //alert('Oops your connection seems off, Check your connection and try again')
                    })
                }
                else {

                    registerUser(Object.assign(this.state.signup_step1, data1), { profile_pic: this.state.selectedImageURI }).then(res => {
                        if (res.success) {
                            store.dispatch({ type: UPDATE_USER, data: res.data })
                            store.dispatch({ type: LOGIN })
                        
                            // this.mixpanel.registerSuperProperties({ 'email': email });
                            // this.mixpanel.identify(email);
                            // this.mixpanel.getPeople().set("Email", email);
                            this.mixpanel.identify(email);
                            this.mixpanel.getPeople().set('$email', email);
                            this.mixpanel.getPeople().set('Brithday', this.state.birthday);
                            this.mixpanel.getPeople().set('Phone Number', this.state.phone);
                            this.mixpanel.getPeople().set('$name', fullName);
                            this.mixpanel.getPeople().set('Bio', bio);
                            this.mixpanel.getPeople().set('$avatar', res.data.photo);
                            this.mixpanel.track('Sign Up', mixpanel_data_without_social);
                            AsyncStorage.setItem('overlay', 'true')
                            NavigationService.reset('Categories')
                            //NavigationService.navigate('Categories')
                        }
                        else {
                            alert(res.text)
                        }
                    }).catch(err => {
                        // alert('Oops your connection seems off, Check your connection and try again')
                    })
                }
            }
        }
    }

    onCurrentLocationClick = () => {
        let self = this
        reactNativeProgresshub.showSpinIndeterminate()
        if (Platform.OS == 'android') {
            RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 10000, fastInterval: 5000 })
                .then(data => {
                    Geolocation.getCurrentPosition(
                        (position) => {
                            console.log(position);
                            this.fetchCurrentAddress(position.coords.latitude, position.coords.longitude)
                        },
                        (error) => {
                            alert('Please turn on your device location')
                            reactNativeProgresshub.dismiss()
                            this.setState({ error: error.message })
                        },
                        { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
                    );
                }).catch(err => {
                    reactNativeProgresshub.dismiss()

                });
        }
        else {
            // geolocation.requestAuthorization()
            Geolocation.getCurrentPosition(
                (position) => {
                    console.log(position);
                    this.fetchCurrentAddress(position.coords.latitude, position.coords.longitude)
                },
                (error) => {
                    reactNativeProgresshub.dismiss()
                    Linking.openURL('app-settings:').then(res => {
                        setTimeout(() => {
                            self.getLocation()
                        }, 500)
                    }).catch(err => {
                        alert(err)
                    })
                    this.setState({ error: error.message })
                },
                { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
            );
        }
    }
    onFocusBio = () => {
        this.setState({
            labelStyleBio: styles.labelInput,
            styleBio: styles.formInputFocus,

        })
    }
    onBlurBio = () => {
        this.setState({
            labelStyleBio: styles.labelInputUnFocus,
            styleBio: styles.formInputUnFocus,


        }, () => {
            this.validtion()
        })
    }
    onFocusPhone = () => {
        this.setState({
            labelStylePhone: styles.labelInput,
            stylePhone: styles.formInputFocus,
            isPhoneFocus: true
        })
    }
    onBlurPhone = () => {
        this.setState({
            labelStylePhone: styles.labelInputUnFocus,
            stylePhone: styles.formInputUnFocus,
            isPhoneFocus: false,
            phoneTouch: true
        }, () => {
            this.validtion()
        })
    }
    onFocusfullName = () => {
        this.setState({
            labelStyleEmail: styles.labelInput,
            styleEmail: styles.formInputFocus,
            isFullNameFocus: true
        })
    }
    onBlurfullNamel() {
        this.setState({ fullNameTouch: true })
        if (this.state.fullName.length <= 2) {
            this.setState({
                labelStyleNameError: styles.inputError,
                labelStyleEmail: styles.labelInputUnFocus,
                styleEmail: styles.formInputUnFocus,
                isFullNameValid: false
            }, () => {
                this.validtion()
            })
        } else {
            this.fullNameValidation()
        }
        this.setState({
            labelStyleEmail: styles.labelInputUnFocus,
            styleEmail: styles.formInputUnFocus,
            isFullNameFocus: false
        })
    }

    onChangeTextfullName = (fullName) => {
        //let name = capitalize_Words(fullName)
        this.setState({ fullName }, () => {
            this.validtion()
        })
    }

    fullNameValidation = () => {

        const reg = /^[A-Z][a-z]+\s[A-Z][a-z]+$/

        //if (!reg.test(this.state.fullName)) {
        if (this.state.fullName.length <= 2) {
            this.setState({ labelStyleNameError: styles.inputError, isFullNameValid: false }, () => {
                this.validtion()
            })
            return false
        }
        this.setState({ labelStyleNameError: null, isFullNameValid: true }, () => {
            this.validtion()
        })
        return true
    }
    onChangeTextphone = (phone) => {
        //alert(this.prevPhone)
        //alert(this.phone.getValue())
        //var b = _.find(CountryPiker, ['dialCode', this.prevPhone.toString().replace('+','')]);
        let mobile = phone.toString().trim().replace(/ /g, '')
        mobile = mobile.toString().replace('+1', '')
        mobile = mobile.toString().replace('+', '')
        mobile = mobile.toString().replace('-', '')
        mobile = mobile.toString().replace('+', '')
        mobile = mobile.toString().replace('-', '')
        mobile = mobile.toString().replace('+', '')
        mobile = mobile.toString().replace('-', '')
        mobile = mobile.toString().replace('(', '')
        mobile = mobile.toString().replace(')', '')
        mobile = mobile.toString().replace('(', '')
        mobile = mobile.toString().replace(')', '')
        if (phone.length != 10) {
            this.setState({ phone: mobile, labelStylePhoneError: styles.inputError }, () => { this.validtion() })
            return false
        } else {
            this.setState({ phone: mobile, labelStylePhoneError: null }, () => { this.validtion() })
        }

    }

    validtion = () => {

        const reg = /^[A-Z][a-z]+\s[A-Z][a-z]+$/
        //const reg = /^[A-Z]+\s[A-Z]+$/
        //if (this.state.fullName.length <=0) {

        //if (!reg.test(this.state.fullName)) {
        if (this.state.fullName.length <= 2) {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
            return false
        }
        // else if (this.state.phone.length != 10 && this.state.phone.length != 11) {
        //     this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
        //     return false
        // }
        // else if (this.state.bio=='') {
        //     this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
        //     return false
        // }
        // else if (this.state.currentLocation =='') {
        //     this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
        //     return false
        // }
        // else if (this.state.selectedImageURI =='') {
        //     this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
        //     return false
        // }
        this.setState({ btnDoneDisable: false, btnDoneImage: btnBg })
        return true
    }

    fetchCurrentAddress = async (lat, long) => {
        let httpMethod = 'GET'
        let strURL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + long + "&sensor=true&key=AIzaSyA6JsG7CPXtIfYJJF3UFdz8z2eBDqohKA8"
        //let strURL = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+long+"&sensor=true&key=AIzaSyCaOM5PnZsBBTF7j2vBMDOOfcr6OVUJ5Kw"
        //AIzaSyCaOM5PnZsBBTF7j2vBMDOOfcr6OVUJ5Kw
        let headers = {
        }
        let params = {
        }

        APICall.callGoogleWebService(httpMethod, strURL, headers, params, (resJSON) => {
            //console.log(resJSON)
            console.log('Google Response: ' + JSON.stringify(resJSON))
            if (resJSON.results.length >= 1) {
                resJSON.results[0].address_components.map((locationInfo) => {
                    if (locationInfo.types.indexOf("locality") != -1) {
                        this.setState({ isenableGps: false, currentLocation: locationInfo.long_name }, () => this.validtion())
                    }
                    if (locationInfo.types.indexOf("administrative_area_level_1") != -1) {
                        this.setState({ isenableGps: false, currentLocation: this.state.currentLocation + ', ' + locationInfo.short_name }, () => this.validtion())
                    }
                })
            }
        })
    }

    titleCase(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        return splitStr.join(' ');
    }

    wscall = async () => {

        let httpMethod = 'POST'
        let strURL = userRegister
        let headers = {
            'Content-Type': 'application/json',
            'device_token': deviceToken,
            'device_type': Platform.OS === 'ios' ? 1 : 2,
            'build_version': '1.0',
        }
        let params = {
            'email': theNavigation.getParam('email'),
            'password': theNavigation.getParam('password'),
            'name': username.trim(),
            'fbid': '',
            'birthdate': theNavigation.getParam('DOB'),
            'bio': this.state.bio,
            'currentLocation': this.state.currentLocation,
            'phone_number': this.phone.getValue(),
            'ip': this.state.ip
        }
        console.log('call api');
        if (this.state.selectedImageURI.length != 0) {
            console.log('uploadPhoto');
            headers = {
                'Content-Type': 'multipart/form-data',
                'device_token': deviceToken,
                'device_type': Platform.OS === 'ios' ? 1 : 2,
                'build_version': '1.0',
            }
            APICall.uploadPhoto('photo', this.state.selectedImageURI, 'image/jpg', 'userPhoto', strURL, headers, params, (resJSON) => {
                console.log('uploadPhoto -> ');
                console.log(resJSON)
                if (resJSON.status == 200) {
                    let data = resJSON.data
                    console.log(data);
                    Pref.setData(Pref.kUserInfo, data)
                    Pref.setData(Pref.kUserId, data.id)
                    Pref.setData(Pref.kaccessToken, data.token)
                    this.props.navigation.push('Categories', {
                        data: data,
                    })
                    return
                } else {
                    this.refs.toast.show(resJSON.message);
                }
            })
        } else {
            console.log('callWebService');
            APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
                console.log(resJSON)
                if (resJSON.status == 200) {
                    let data = resJSON.data
                    console.log(data);
                    Pref.setData(Pref.kUserInfo, data)
                    Pref.setData(Pref.kUserId, data.id)
                    Pref.setData(Pref.kaccessToken, data.token)
                    this.props.navigation.push('Categories', {
                        data: data,
                    })
                    return
                } else {
                    this.refs.toast.show(resJSON.message);
                }

            })
        }

    }
    getLocation = () => {
        reactNativeProgresshub.showSpinIndeterminate()
        Geolocation.getCurrentPosition(
            (position) => {
                console.log(position);
                reactNativeProgresshub.dismiss()
                this.fetchCurrentAddress(position.coords.latitude, position.coords.longitude)
            },
            (error) => {
                this.setState({ isenableGps: true })
                reactNativeProgresshub.dismiss()
                this.setState({ error: error.message })
            },
            { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
        );
    }
    componentDidMount() {
        //this.getLocation()
        SplashScreen.hide()

        if (this.state.is_from_social == true) {
            if (this.state.fullName != undefined && this.state.fullName != '' && this.state.fullName != null) {
                this.setState({ btnDoneDisable: false, btnDoneImage: btnBg })
            }
        }
    }
    render() {
        let currentLocation = (this.state.currentLocation)
        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#FFF' }} >
                <Toastt ref="toast"></Toastt>
                <BackgroundImage >
                    <SafeAreaView />
                    <Content>
                        <Text style={[{ textAlign: 'center', fontSize: FontSize(17), fontFamily: "Raleway-ExtraBold", color: "#363169", marginTop: Height(4), letterSpacing: 0.02 }, checkFontWeight("800")]}>Complete your profile so that friends</Text>
                        <Text style={[{ textAlign: 'center', fontSize: FontSize(17), fontFamily: "Raleway-ExtraBold", color: "#363169", letterSpacing: 0.02 }, checkFontWeight("800")]}>can recognize you</Text>
                        <Button transparent style={{ alignItems: 'center', justifyContent: 'center', alignSelf: 'center', marginTop: Height(6), height: Height(8) }} onPress={() => this.onClickSelectPhoto()} >
                            {
                                this.state.isPhotochange == true ?
                                    <View style={{ top: Height(1), alignSelf: 'center', height: Height(15), width: Height(15), borderRadius: Height(15), borderWidth: Width(0.8), borderColor: '#795EB9', overflow: 'hidden', backgroundColor: '#5432B1', justifyContent: 'center', alignItems: 'center' }}>
                                        <View style={{ alignSelf: 'center', height: Height(12), width: Height(12), borderRadius: Height(12), borderWidth: Width(0.6), borderColor: '#FFF', overflow: 'hidden' }}>
                                            <Image source={{ uri: this.state.selectedImageURI }} resizeMode="cover" style={{ width: '100%', height: '100%' }} />
                                        </View>
                                    </View>
                                    :
                                    <Image source={camera_icon} style={{ width: Width(30), height: Height(12) }} ></Image>
                            }

                        </Button>
                        <Text
                            onPress={() => this.onClickSelectPhoto()}
                            style={[{
                                textAlign: 'center', fontSize: FontSize(15), fontFamily: fonts.Raleway_Regular
                                , color: "#007aff", marginTop: Height(5), letterSpacing: 0.005
                            }]}>
                            Tap to select profile photo</Text>
                        <FloatingLabel
                            autoCapitalize="none"
                            textContentType="telephoneNumber"
                            keyboardType="numeric"
                            labelStyle={[this.state.labelStylePhone, this.state.phone.length == 0 ? styles.labelInputUnFocus : '', this.state.phone.length != 10 && this.state.isPhoneFocus && this.state.phoneTouch && this.state.phone.length != 0 ? styles.labelInputError : {}]}
                            inputStyle={[styles.input1]}
                            style={[styles.formInput, this.state.stylePhone]}
                            onChangeText={(fullName) => this.onChangeTextphone(fullName)} value={this.state.phone.toString().trim().replace(/ /g, '')}
                            onBlur={() => this.onBlurPhone()}
                            onFocus={() => this.onFocusPhone()}

                            touch={this.state.phoneTouch && this.state.phone.length != 0}
                            valid={this.state.phone.length != 10 ? false : true}

                        >Phone</FloatingLabel>
                        {
                            this.state.phone.length != 10 && this.state.isPhoneFocus && this.state.phoneTouch && this.state.phone.length != 0 ?
                                <Text style={[{ marginLeft: Width(6), marginRight: Width(6), marginTop: Height(2), color: "red", fontSize: FontSize(10), fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>Please enter a 10 digit phone number</Text>
                                : null
                        }
                        {/* <View style={[styles.input, this.state.labelStylePhoneError, { marginTop: Height(2) }]}>
                            <View style={{ flexDirection: 'row', height: Height(8), justifyContent: 'center', alignItems: 'center' }}>
                                <Item floatingLabel style={{ flex: 1, height: Height(8), borderBottomWidth: 0 }}>
                                    <PhoneInput  ref={ref => { this.phone = ref; }} textProps={{ placeholder: 'Phone*', placeholderTextColor: '#b4b7ba',textContentType:"telephoneNumber" }} textStyle={styles.inputText} onChangePhoneNumber={(phone) => this.onChangeTextphone(phone)} value={this.state.phone} />
                                </Item>
                            </View>
                        </View> */}
                        {/* <View style={[styles.input, this.state.labelStyleNameError, { marginTop: Height(2) }]}>
                            <View style={{ flexDirection: 'row', height: Height(8), justifyContent: 'center', alignItems: 'center' }}>
                                <Item style={{ flex: 1, height: Height(8), borderBottomWidth: 0 }}>
                                    <Input  textContentType={"name"} placeholderTextColor="#b4b7ba" placeholder="Full name*" style={[styles.inputText]} onChangeText={(fullName) => this.onChangeTextfullName(fullName)} value={this.state.fullName}  autoCapitalize="words"/>
                                </Item>
                            </View>
                        </View> */}
                        <FloatingLabel
                            autoCapitalize="words"
                            textContentType={"name"}
                            labelStyle={[this.state.labelStyleEmail, this.state.fullName.length == 0 ? styles.labelInputUnFocus : '', this.state.fullName.length != 0 && this.state.isFullNameFocus && this.state.fullNameTouch ? {} : {}]}
                            inputStyle={styles.input1}
                            style={[styles.formInput, this.state.styleEmail]}
                            onChangeText={(fullName) => this.onChangeTextEmail(fullName)} value={this.state.fullName}
                            onBlur={() => this.onBlurfullNamel()}
                            onFocus={() => this.onFocusfullName()}

                            //touch ={this.state.fullNameTouch && this.state.fullName.length}
                            //valid={this.state.fullName.length==0?false:true}
                            touch={this.state.fullNameTouch && this.state.fullName.length}
                            valid={this.state.isFullNameValid == false ? false : true}

                        >Full name*</FloatingLabel>
                        {
                            (this.state.isFullNameValid == false && this.state.isFullNameFocus && this.state.fullNameTouch) ?
                                <Text style={[{ marginLeft: Width(6), marginRight: Width(6), marginTop: Height(2), color: "red", fontSize: FontSize(10), fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>Full name is required</Text>
                                : null
                        }
                        {/* <View style={[styles.input, { marginTop: Height(1) }]}>
                            <View style={{ flexDirection: 'row',flexWrap:'wrap', height: Height(8), justifyContent: 'center', alignItems: 'center' }}>
                                <Item style={{ flex: 1, minHeight: Height(12), borderBottomWidth: 0 }}>
                                    <Input  maxLength={100} multiline={true} placeholderTextColor="#b4b7ba" placeholder="Bio" style={[styles.inputText,{}]} onChangeText={(bio) => this.setState({bio},()=>this.validtion())} />
                                </Item>
                            </View>
                        </View> */}
                        <FloatingLabel
                            autoCapitalize="none"
                            maxLength={110} multiline={true}
                            labelStyle={[this.state.labelStyleBio, this.state.bio.length == 0 ? styles.labelInputUnFocus : '']}
                            inputStyle={styles.input1}
                            style={[styles.formInput, this.state.styleBio]}
                            onChangeText={(bio) => this.setState({ bio }, () => this.validtion())} value={this.state.bio}
                            onBlur={() => this.onBlurBio()}
                            onFocus={() => this.onFocusBio()}

                        // touch ={this.state.fullNameTouch}
                        // valid={this.state.fullName.length==0?false:true}

                        >Bio</FloatingLabel>
                        <View style={{ flexDirection: 'row', marginLeft: Width(6), marginRight: Width(6), marginTop: Height(1), }}>
                            <Text style={[{ color: "#B3B3B3", fontSize: FontSize(10), fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>Tell people about your self</Text>
                            <Text style={{ position: 'absolute', right: 0, color: "#B3B3B3", fontSize: FontSize(10), fontFamily: "Roboto-Bold" }}>{this.state.bio.length}/110</Text>
                        </View>
                        {/* {this.state.isenableGps?<TouchableOpacity onPress={()=>this.onCurrentLocationClick()} style={{height:Height(9),marginTop:Height(2),backgroundColor:'#5856D6',justifyContent:'center'}}>
                            <View style={{marginHorizontal:Width(6),flexDirection:'row'}}>
                                <View style={{width:Width(82),justifyContent:'center'}}>
                                <Text style={{fontSize:FontSize(14),fontFamily:fonts.Raleway_Regular,color:'#FFF'}}>To find your current location automatically,</Text>
                                <Text style={{fontSize:FontSize(14),fontFamily:fonts.Raleway_Regular,color:'#FFF'}}>turn on location services</Text>
                                </View>
                                <View style={{alignItems:'flex-end',flex:1}}>
                                    <AntDesign name="right" color="white" size={25}/>
                                </View>
                            </View>
                        </TouchableOpacity>:null} */}
                        {/* <View style={[styles.input, { marginTop: Height(1) }]}>
                            <View style={{ flexDirection: 'row', height: Height(8), justifyContent: 'center', alignItems: 'center' }}>
                                <Item style={{ flex: 1, height: Height(8), borderBottomWidth: 0 }}>
                                    <Input disabled={true} placeholderTextColor="#b4b7ba" placeholder="Current City" style={styles.inputText} onChangeText={(currentLocation) => this.setState({currentLocation},()=>this.validtion())} value={currentLocation} />
                                    
                                    
                                </Item>
                            </View>

                        </View> */}
                        {/* <FloatingLabel
                        disabled={true}
                        editable={false}
                        autoCapitalize="none"
                            
                            labelStyle={[styles.labelInputUnFocus,this.state.currentLocation.length==0?styles.labelInputUnFocus:{}]}
                            inputStyle={styles.input1}
                            style={[styles.formInput, styles.formInputUnFocus]}
                            onChangeText={(currentLocation) => this.setState({currentLocation},()=>this.validtion())} value={this.state.currentLocation}
                            // onBlur={() => this.onBlurBio()}
                            // onFocus={() => this.onFocusBio()}
                           
                            touch={this.state.currentLocation.length!=0}
                            valid={this.state.currentLocation.length==0?false:true}

                        >Current city*</FloatingLabel> */}
                        {/* <Text style={[{ marginLeft: Width(6), marginRight: Width(6), marginTop: Height(1), color: "#B3B3B3", fontSize: FontSize(10), fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>Let your friends know where you are so they can make plus</Text> */}

                        <TouchableOpacity style={{ width: '100%', marginTop: Height(4) }} disabled={this.state.btnDoneDisable} onPress={() => this.onNextClick()}>
                            <ImageBackground source={this.state.btnDoneImage} style={{ width: '100%', height: Height(9), alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[{ alignSelf: 'center', marginBottom: Height(2), fontSize: FontSize(19), fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Next</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                    </Content>
                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default SignUpStep1;
