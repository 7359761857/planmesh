const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import { isIphoneX } from '../Default/is-iphone-x'
import { FontSize,Width, Height } from '../../config/dimensions';
const HEADER_SIZE = isIphoneX() ? 84 : 64;
const HEADER_PADDING_SIZE = isIphoneX() ? 40 : 16;

export default {

    headerAndroidnav: {
        backgroundColor: '#41199B',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        ...Platform.select({
            ios: {
                height: HEADER_SIZE,
                paddingTop: HEADER_PADDING_SIZE,
            },
            android: {
            },
        }),
    },
    headerTitle: {
        letterSpacing: 0.02,
        fontSize: FontSize(20),
        fontFamily: "Raleway-Medium",
        color: 'white',
        ...Platform.select({
            ios: {
              fontWeight: '500',
            },
            android: {
            },
          })
    },
    input: {
        marginLeft:Width(6),
        marginRight:Width(6),
        minHeight: Height(6),
        borderBottomColor: "#3a4759",
        borderBottomWidth: 0.3,
        fontSize: FontSize(16),
        fontFamily: "Roboto-Regular",

    },
    inputText: {
        paddingLeft:0,
        marginLeft:0,
        fontSize: FontSize(16),
        fontFamily: "Roboto-Regular",
        color: "#363169"
    },
    inputTitle: {
        color: "#363169",
        fontSize: FontSize(16),
        fontFamily: "Roboto-Medium",
        flex: 0.65,
        ...Platform.select({
            ios: {
              fontWeight: '800',
            },
            android: {
            },
          })
    },inputError: {
        color:'red',
        borderBottomColor: "red",
    },
    labelInput: {
        color: '#363169',
        fontSize: FontSize(15),
        fontFamily: "Roboto-Bold",
        paddingLeft:0
    },
    formInput: {
        color: '#3a4759',
        fontSize: FontSize(16),
        fontFamily: "Roboto-Regular",
        marginLeft:Width(6),
        marginRight:Width(6),
        marginTop:Height(2),
    },
    formInputFocus:{
        borderBottomWidth: 2,
        borderColor: '#3A4759',
        paddingLeft:0,
    },
    formInputUnFocus:{
        borderBottomWidth: 2,
        borderColor: '#BBC1C8',
    },
    labelInputUnFocus:{
        color: '#8f8f90',
        fontSize: FontSize(16),
        fontFamily: "Roboto-Regular",
        paddingLeft:0,
    },
    input1: {
        borderWidth: 0,
        fontSize: FontSize(16),
        fontFamily: "Roboto-Regular",

    },
    labelInputError: {
        color:'red'
    }
    
};
