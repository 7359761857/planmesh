import React, { Component } from "react";
import { SafeAreaView, StyleSheet, Image, ScrollView, Text, ImageBackground, TouchableOpacity, StatusBar } from "react-native";
import { Container, View, Button, Header, Left, Item, Icon, Right, Content, Input } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import { fonts, API_ROOT } from "../../config/constant";
import { store } from "../../redux/store";

import { Height, FontSize, Width, colors } from "../../config/dimensions";

const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");


const backWhite = require("../../../assets/images/backWhite.png");
const btnBg = require("../../../assets/images/btnBg.png");

const search_img = require("../../../assets/images/Dashboard/search.png");
var array = []

class OtherProfileInterest extends ValidationComponent {

    constructor(props) {
        super(props);
        this.state = {
            text: '',
            interest: ['Movies & Videos', 'Dining out', 'Music & Concerts', 'Coffee Breaks', 'Camping', 'Watching Sports', 'Shopping', 'Museums & Arts', 'Fishing & Hunting',],
            count: 0,
            count1: 0,
            Category: this.props.navigation.state.params.subcategories,
            token: store.getState().auth.user.token,
            login_id: store.getState().auth.user.login_id,
            
        };
    }

    componentWillMount()
    {
        
    }

    componentDidMount() {
        
    }

    renderTopBar() {
        return (
            <View style={[styles.headerAndroidnav]}>
                <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
                <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                    <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                        <Image source={backWhite} style={{ tintColor: "white" }} />
                    </TouchableOpacity></View>
                <View style={{ bottom: Height(1.8), position: 'absolute', alignItems: 'center',alignSelf:'center' }}>
                    <Text style={styles.headerTitle} >Interests</Text>
                </View>
            </View>
        )
    }


    makeStyles() {
        return StyleSheet.create({
            view: {
                flexDirection: 'row',
                flexWrap: 'wrap',
                backgroundColor: '#fff',
                marginHorizontal: Width(3)

            },

            text: {
                fontSize: FontSize(14),
                paddingLeft: '4%',
                paddingRight: '4%',
                paddingTop: '1.8%',
                paddingBottom: '1.8%',
                textAlign: 'center',
                color: '#676868',

            },
            selectedText: {
                fontSize: FontSize(14),
                paddingLeft: '4%',
                paddingRight: '4%',
                paddingTop: '1.8%',
                paddingBottom: '1.8%',
                textAlign: 'center',
                color: '#fff',
            },
            touchable: {
                marginLeft: '1.1%',
                marginTop: '1.1%',
                borderRadius: 5,
                borderColor: '#676868',
                borderWidth: 1,
            },
            selectedTouchable: {
                marginLeft: '1.1%',
                marginTop: '1.1%',
                borderRadius: 5,
                borderColor: '#41199B',
                borderWidth: 1,
                backgroundColor: '#41199B',

            }
        })
    }

    SearchFilterFunction(text)
    {
        
    }

    render() {
        const styles1 = this.makeStyles()

        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }} >
                <BackgroundImage >
                    {this.renderTopBar()}
                    <SafeAreaView />
                    {/* <View style={{ height: Height(1), backgroundColor: colors.white }}></View> */}

                    {/* <View style={{ backgroundColor: '#fff' }}>
                        <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', margin: '1%' }}>
                            <Item style={{ backgroundColor: colors.lightgrey, borderColor: colors.white, borderRadius: 15, height: Height(6), justifyContent: 'center', width: Width(92.5) }}>
                                <Image source={search_img} tintColor='#B4B7BA' style={{ marginLeft: Width(4.5) }} />
                                <Input
                                    // onChangeText={text => this.SearchFilterFunction(text)}
                                    onChangeText={text => this.SearchFilterFunction(text)}
                                    value={this.state.text}
                                    placeholderTextColor='#8F8F90'
                                    placeholder="Search interests"
                                    style={{ fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16), marginLeft: Width(2) }} />
                            </Item>
                        </View>
                    </View> */}
                    <Content >

                        <View style={{ height: Height(2) }} />
                        <View style={styles1.view}>
                        {this.state.Category.map(item => {
                            return (
                                <TouchableOpacity
                                    style={styles1.touchable}>
                                    <Text style={styles1.text}>{item}</Text>
                                </TouchableOpacity>
                            )
                        })}
                        </View>
                    </Content>
                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default OtherProfileInterest;
