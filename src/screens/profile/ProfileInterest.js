import React, { Component } from "react";
import { SafeAreaView, StyleSheet, Image, ScrollView, Text, ImageBackground, TouchableOpacity, StatusBar } from "react-native";
import { Container, View, Button, Header, Left, Item, Icon, Right, Content, Input } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import FloatingLabel from 'react-native-floating-labels';
import { fonts, API_ROOT } from "../../config/constant";
import { getSubCategoryList, AddSubCategoryList } from "../../redux/actions/auth";
import { store } from "../../redux/store";

import { Height, FontSize, Width, colors } from "../../config/dimensions";
import SplashScreen from "react-native-splash-screen";
//import SelectableChips from 'react-native-chip/SelectableChips'
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");


const backWhite = require("../../../assets/images/backWhite.png");
const btnBg = require("../../../assets/images/btnBg.png");

const search_img = require("../../../assets/images/Dashboard/search.png");
const closeicon = require("../../../assets/images/Dashboard/close.png");
import MixpanelManager from '../../../Analytics'
import moment from 'moment';

class ProfileInterest extends ValidationComponent {

    constructor(props) {
        super(props);
        this.array = []
        this.search = null,
        this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
        this.state = {
            text: '',
            interest: ['Movies & Videos', 'Dining out', 'Music & Concerts', 'Coffee Breaks', 'Camping', 'Watching Sports', 'Shopping', 'Museums & Arts', 'Fishing & Hunting',],
            count: 0,
            count1: 0,
            Category: [],

            cat_id: this.props.navigation.state.params.Category_id,
            cat_name: this.props.navigation.state.params.category_name,

            token: store.getState().auth.user.token,
            login_id: store.getState().auth.user.login_id,
            selected_array: [],
            is_close_visible: false,


        };
    }

    componentWillMount() {
        this.getUserData()
    }

    componentDidMount() {

        let data = {
            token: this.state.token,
            login_id: this.state.login_id,
            category_id: String(this.state.cat_id),
            type: 'other',
        }
        getSubCategoryList(data).then(res => {
            if (res.success) {
                this.setState({ Category: res.data, subCategory: res.data.sub_category_list })
            }
            else {

                this.setState({ empty: false })

            }
        }).catch(err => {
            alert(err)


        })



    }

    getUserData = () => {

        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('type', 0)
        data.append('other_id', '')

        fetch(API_ROOT + 'user/details', {
            method: 'post',
            body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {

                    //alert(JSON.stringify(responseData))

                    var interestslist = responseData.data.fav_sub_category_list
                    var temparr = []
                    interestslist.map((item) => {
                        temparr.push(item.sub_category_id)
                        this.array.push(item.sub_category_id)
                    });

                    //alert(JSON.stringify(temparr))
                    this.setState({ selected_array: temparr })

                } else {
                    if (responseData.text === 'Invalid token key') {
                        alert(responseData.text)
                        this.props.navigation.navigate('Login')
                    }
                }
            })

            .catch((error) => {

            })
    }


    renderTopBar() {
        return (
            <View style={[styles.headerAndroidnav]}>
                <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
                <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                    <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                        <Image source={backWhite} style={{ tintColor: "white" }} />
                    </TouchableOpacity></View>
                <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center', alignSelf: 'center' }}>
                    <Text style={styles.headerTitle} >{this.state.cat_name}</Text></View>
            </View>
        )
    }

    onFreeMoneyClick = () => {

        //alert('Hiii')

        //if(this.state.selected_array.length > 0)
        //{

        let data = {
            token: this.state.token,
            login_id: this.state.login_id,
            sub_category_id: String(this.state.selected_array)
        }
        console.log('data...', this.state.selected_array)
        //console.log('SUB CAT IDs: '+ data.sub_category_id)

        AddSubCategoryList(data).then(res => {
            //  this.mixpanel.track('Category');    
            if (res.success) {
                console.log('Number of Category Selected..', this.state.cat_id)
                console.log('Interest..', this.state.selected_array)
                console.log('Interest..', this.state.selected_array.length)

                const Interest = {
                    '$email': store.getState().auth.user.email_address,
                    'During Sign Up': 'False',
                    'Total Interests': this.state.selected_array.length,
                }
                //User Property
                this.mixpanel.identify(store.getState().auth.user.email_address);
                this.mixpanel.getPeople().increment('Total Interests Added', this.state.selected_array.length);
                // this.mixpanel.getPeople().setOnce({ 'First Add Interests': moment().format('MMMM Do YYYY, h:mm:ss a') });
                this.mixpanel.getPeople().set("Last Add Interests", moment().format('MMMM Do YYYY, h:mm:ss a'));
                // this.mixpanel.getPeople().set("Interests Category Selected", JSON.stringify(SELECTED_CATEGORY));
                // this.mixpanel.getPeople().set("Interests Selected", JSON.stringify(SELECTED_INTEREST));
                this.mixpanel.getPeople().set("Total Interests", this.state.selected_array.length);
                // this.mixpanel.getPeople().set("Total Interests Category", SELECTED_INTEREST.length);

                // this.mixpanel.track('Add Interests Test', Interest);
                this.mixpanel.track('Add Interests', Interest);
                // const MixpanelData = {
                //  SelectedCategory: this.state.selected_array,
                // }
                // this.mixpanel.identify(store.getState().auth.user.email_address);
                // this.mixpanel.track('ProfileCategory', MixpanelData);
                this.props.navigation.navigate('myprofile')
            }
            else {
                alert(res.text)

            }
        }).catch(err => {
            alert(err)

        })
        //}
        // else
        // {
        //     this.props.navigation.navigate('myprofile')
        // }


        // this.props.navigation.push('Places')
    }



    makeStyles() {
        return StyleSheet.create({
            view: {
                flexDirection: 'row',
                flexWrap: 'wrap',
                backgroundColor: '#fff',
                marginHorizontal: Width(3)
            },

            text: {
                fontSize: FontSize(14),
                paddingLeft: '4%',
                paddingRight: '4%',
                paddingTop: '1.8%',
                paddingBottom: '1.8%',
                textAlign: 'center',
                color: '#676868',
                height: 30,

            },
            selectedText: {
                fontSize: FontSize(14),
                paddingLeft: '4%',
                paddingRight: '4%',
                paddingTop: '1.8%',
                paddingBottom: '1.8%',
                textAlign: 'center',
                color: '#fff',
                height: 30,

            },
            touchable: {
                marginLeft: '1.1%',
                marginTop: '1.1%',
                borderRadius: 5,
                borderColor: '#676868',
                borderWidth: 1,
            },
            selectedTouchable: {
                marginLeft: '1.1%',
                marginTop: '1.1%',
                borderRadius: 5,
                borderColor: '#41199B',
                borderWidth: 1,
                backgroundColor: '#41199B',
            }
        })
    }
    checkForFriends = (item) => {
        if (this.state.selected_array.includes(item.sub_category_id)) {
            return true
        } else {
            return false
        }

    }

    onClearTextPressed() {
        this.setState({ is_close_visible: false, text: '' })
        setTimeout(() => {
            this.search.blur()

        }, 100)
    }


    SearchFilterFunction(text) {
        this.setState({ is_close_visible: true, text: text })

        if (text == '') {
            this.setState({ is_close_visible: false })
        }

    }

    render() {
        const styles1 = this.makeStyles()

        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }} >
                <BackgroundImage >
                    {this.renderTopBar()}

                    <SafeAreaView />
                    <View style={{ height: Height(1), backgroundColor: colors.white }}></View>

                    <View style={{ backgroundColor: '#fff' }}>
                        <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', margin: '1%' }}>
                            <Item style={{ backgroundColor: colors.lightgrey, borderColor: colors.white, borderRadius: 15, height: Height(6), justifyContent: 'center', width: Width(92.5) }}>
                                <Image source={search_img} tintColor='#B4B7BA' style={{ marginLeft: Width(4.5) }} />
                                <Input
                                    // onChangeText={text => this.SearchFilterFunction(text)}
                                    onChangeText={text => this.SearchFilterFunction(text)}
                                    value={this.state.text}
                                    ref={search => this.search = search}
                                    placeholderTextColor='#8F8F90'
                                    placeholder="Search interests"
                                    style={{ fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16), marginLeft: Width(2) }} />
                                {
                                    this.state.is_close_visible == true ?
                                        <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', height: 25, width: 25, position: 'absolute', right: 10 }} onPress={() => this.onClearTextPressed()}>
                                            <Image source={closeicon} resizeMode='stretch' style={{ height: 17, width: 17 }} />
                                        </TouchableOpacity>
                                        :
                                        null
                                }
                            </Item>
                        </View>
                    </View>
                    <Content >

                        <View style={{ height: Height(2) }} />
                        {this.state.Category.map(item => {
                            let items = [];
                            items = item.sub_category_list
                            return (
                                <View style={{}}>
                                    <View style={styles1.view}>
                                        {items.map(item1 => {

                                            if (item1.sub_category_name.includes(this.state.text)) {
                                                return (
                                                    <TouchableOpacity
                                                        onPress={() => {
                                                            //this.setState({ selected_array: '' })
                                                            if (this.array.includes(item1.sub_category_id) == true) {
                                                                let item_index = this.array.indexOf(item1.sub_category_id)
                                                                this.array.splice(item_index, 1)

                                                            } else {
                                                                this.array.push(item1.sub_category_id)

                                                            }
                                                            this.setState({ selected_array: this.array })
                                                        }}
                                                        style={this.checkForFriends(item1) == true ? styles1.selectedTouchable : styles1.touchable}>
                                                        <Text style={this.checkForFriends(item1) == true ? styles1.selectedText : styles1.text}>{item1.sub_category_name}</Text>
                                                    </TouchableOpacity>
                                                )
                                            }
                                        })}
                                    </View>


                                </View>
                            );
                        })}

                    </Content>

                    {/* {this.state.selected_array.length < 1 ?
                        <View style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }} >
                            <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Save</Text>
                            </ImageBackground>
                        </View>
                        : */}
                    <TouchableOpacity style={{ width: '100%', marginBottom: Height(2) }} onPress={() => this.onFreeMoneyClick()}>
                        <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Save</Text>
                        </ImageBackground>
                    </TouchableOpacity>
                    {/* } */}
                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default ProfileInterest;
