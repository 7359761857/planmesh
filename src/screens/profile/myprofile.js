import React, { Component } from "react";
import { SafeAreaView, FlatList, StyleSheet, Image, Share, TouchableHighlight, Alert, View, ScrollView, ImageBackground, TouchableOpacity, StatusBar, Text, Platform } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import Entypo from 'react-native-vector-icons/Entypo';
import { Height, Width, FontSize, colors, isIphoneX } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import { API_ROOT } from "../../config/constant";
import { store } from "../../redux/store";
import Modal from "react-native-modal";
//import SelectableChips from 'react-native-chip/SelectableChips'
import ImagePicker from 'react-native-image-picker';
import FloatingLabel from 'react-native-floating-labels';
import DateTimePicker from 'react-native-modal-datetime-picker';
import SeeMore from 'react-native-see-more-inline';
import RNProgressHUB from 'react-native-progresshub';
import DatePicker from 'react-native-date-picker'

import { Container, Body, Header, List, ListItem as Item, ScrollableTab, Tab, TabHeading, Tabs, Title, Content } from "native-base";
import moment from 'moment';
import 'moment/locale/en-au'  // without this line it didn't work
import { NOTIFICATION, EDIT_PROFILE_PRESENT } from "../../redux/actions/types";
import { UPDATE_USER } from "../../redux/actions/types";
import { editProfile } from "../../redux/actions/auth";
import MixpanelManager from '../../../Analytics'

moment.locale('en-au')
const backWhite = require("../../../assets/images/backWhite.png");
const closeWhite = require("../../../assets/images/ic_close_white.png");
const profile_border = require("../../../assets/images/Profile/profile_border.png");
const BG_images = require("../../../assets/images/Profile/Profile_bg.png");
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
const btnBg = require("../../../assets/images/btnBg.png");
const settings = require("../../../assets/images/Profile/settings.png");
const user = require("../../../assets/images/Profile/user.png");
const location = require("../../../assets/images/Dashboard/location_home.png");
const pink_chai_cup = require("../../../assets/images/Profile/cup.png");
const pink_clock = require("../../../assets/images/Profile/clock.png");
const pink_location = require("../../../assets/images/Profile/location.png");
const chai_cup = require("../../../assets/images/Dashboard/chai_cup.png");
const calendar = require("../../../assets/images/Dashboard/calendar.png");
const new_user = require("../../../assets/images/Dashboard/new_user.png");
const flowers = require("../../../assets/images/Dashboard/avtar.png");
const bg = require("../../../assets/images/Dashboard/bg.png");
const share = require("../../../assets/images/Dashboard/share.png");
const Delete = require("../../../assets/images/Dashboard/Delete.png");
const Edit = require("../../../assets/images/Dashboard/Edit.png");
const ic_chat = require("../../../assets/images/Dashboard/chat_icon.png");
const default_placeholder = require("../../../assets/images/Dashboard/default_placeholder.png");



let isloading = false

var dateFormat = require('dateformat');
var place = [], votes = []
class Profile extends ValidationComponent {

  _didFocusSubscription
  constructor(props) {
    offset = 0

    super(props);
    this.eventtitle = '';
    this.is_photo_selected = false;
    this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
    this.state = {
      interest_list: [],
      token: store.getState().auth.user.token,
      login_id: store.getState().auth.user.login_id,
      post_name: '',
      other_id: '',
      plan_id: '',
      fullname: '',
      bio: '',
      offset: 0,
      limit: 150,
      profile: '',
      city: '',
      bod: '',
      refreshing: false,
      friendList: [],
      friends: '',
      UserDetail: [],
      friendCount: '',
      planList: [],
      interestList: [],
      placeList: [],
      p_list: [],
      isModalVisible: false,
      isProfile: true,
      isPost: false,
      height: 0,
      currentitem: '',
      isProfilePicVisible: false,
      isEditProfileVisible: false,
      routes: [{ key: 'first', title: 'Profile' }, { key: 'second', title: 'Posts' }],

      //Edit Profile

      labelStylebirthday: styles.labelInputUnFocus,
      stylebirthday: styles.formInputUnFocus,
      labelStylePassword: styles.labelInputUnFocus, stylePassword: styles.formInputUnFocus, password: "", passwordValidation: true, passwordValidationMsg: "",
      labelStyleCity: styles.labelInputUnFocus, labelStyleBio: styles.labelInputUnFocus, labelStyleName: styles.labelInputUnFocus, styleName: styles.formInputUnFocus, styleBio: styles.formInputUnFocus, styleCity: styles.formInputUnFocus, email: "", emailValidation: true, emailValidationMsg: "",
      btnDoneDisable: true, btnDoneImage: btnBg, wsError: false,
      photo_path: null,
      name1: '',
      photo1: '',
      city1: '',
      DOB1: new Date(),
      bdayforpicker: new Date(),
      bio1: '',
      DOBPass1: "",
      isProfilePicVisible1: false,

    };
    subThis = this
    this._didFocusSubscription = props.navigation.addListener('didFocus', payload => {
      this.getUserData()
      this.reloadPost()
    }
    )
  }

  renderTopBar() {
    return (
      <View style={[styles.headerAndroidnav]}>
        <StatusBar barStyle="light-content" backgroundColor="#41199B" />
        <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
        </View>
        <View style={{ bottom: Height(2), position: 'absolute', alignSelf: 'center' }}>
          <Text style={styles.headerTitle} >Profile</Text>
        </View>
        <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', right: Width(4) }}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('settings')} >
            <Image source={settings} resizeMode="contain" style={{ tintColor: "white" }} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
  // static navigationOptions = ({ navigation }) => {
  //   theNavigation = navigation;
  //   return {
  //     header: (
  //       <Header style={styles.headerAndroidnav}>
  //         <StatusBar barStyle="light-content" backgroundColor="#41199B" />
  //         <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
  //           <TouchableOpacity onPress={() => { navigation.navigate('dashboard', { refreshing: true }) }}>
  //             <Image source={backWhite} style={{ tintColor: "white" }} />
  //           </TouchableOpacity></View>
  //         <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
  //           <Text style={styles.headerTitle} >Profile</Text>
  //         </View>
  //         <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', right: Width(4) }}>
  //           <TouchableOpacity onPress={() => navigation.navigate('settings')} >
  //             <Image source={settings} resizeMode="contain" style={{ tintColor: "white" }} />
  //           </TouchableOpacity>
  //         </View>

  //       </Header>

  //     )
  //   }
  // }

  componentDidMount() {
    this.getUserData()
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('light-content');
      Platform.OS == 'android' && StatusBar.setBackgroundColor('#41199B');

    });

  }
  componentWillUnmount() {
    this._navListener.remove();
  }
  deleteplan = () => {
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('plan_id', this.state.plan_id)
    data.append('login_id', this.state.login_id)
    fetch(API_ROOT + 'delete/plan', {
      method: 'post', body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          const MixpanleData = {
            '$email' : store.getState().auth.user.email_address,
            'Post ID' : this.state.plan_id
          }
          this.mixpanel.identify(store.getState().auth.user.email_address);
          this.mixpanel.track('Post Cancel', MixpanleData);
          this.setState({ isModalVisible: false })
          this.getPostList()
        } else {
          alert(responseData.text)

        }
      })
      .catch((error) => {
        //  alert(error) 
      })
  }

  reloadPost() {
    if (this.state.isPost == true) {
      this.setState({
        isProfile: false,
        isPost: true,
      });
    }
    isloading = false
    this.getPostList()
  }

  async goToShare(item) {
    // Share.open({ url: 'Hey,join Planmesh app to share your pics',message:});
    const MixpanelData = {
      '$email': store.getState().auth.user.email_address,
      'Post ID': item.plan_id,
    }
    this.mixpanel.identify(store.getState().auth.user.email_address);
    this.mixpanel.track('Share', MixpanelData);
    var message = ''

    if (item.plan_type == 1) {
      var name = item.friend_name.split(" ")
      //message = 'Hey, '+"i'm inviting you to "+ name[0]+", let's meet up!" + '\n'+ "Let me know on Planmesh if you're in" + '\n\n' +'https://planmeshapp.com/plan_details/'+item.plan_id
      message = 'Hey, ' + "i'm inviting you to " + ", Let's meet up!" + '\n' + "Let me know on Planmesh if you're in" + '\n\n' + 'https://planmeshapp.com/plan_details/' + item.plan_id
    }
    else if (item.plan_type == 2) {
      message = 'Hey, ' + "i'm inviting you to " + item.activity + " Let me know on Planmesh if you're in" + '\n\n' + 'https://planmeshapp.com/plan_details/' + item.plan_id
    }
    else if (item.plan_type == 3) {
      message = 'Hey, ' + "i'm inviting you to " + "Wanna go to " + item.line_1 + "?" + " Let me know on Planmesh if you're in" + '\n\n' + 'https://planmeshapp.com/plan_details/' + item.plan_id
    }
    else if (item.plan_type == 4) {
      message = 'Hey, ' + "i'm inviting you to " + "I'm free, wanna hangout?" + " Let me know on Planmesh if you're in" + '\n\n' + 'https://planmeshapp.com/plan_details/' + item.plan_id
    }

    try {
      const result = await Share.share({
        message: message,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  }

  getUserData = () => {
    var votes = [], interest = []

    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('type', 0)
    data.append('other_id', '')

    console.log('Profile Request: ' + JSON.stringify(data))

    fetch(API_ROOT + 'user/details', {
      method: 'post',
      body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {

          //alert(JSON.stringify(responseData))

          let data = responseData.data
          this.setState({ placeList: responseData.data.fav_place_list, interestList: responseData.data.fav_sub_category_list })
          if (data.dob == '0000-00-00') {
            this.setState({ bod: moment().format("MMM D") })
          } else {
            this.setState({ bod: data.dob })

          }
          this.setState({
            fullname: data.name, bio: data.bio, profile: data.photo, city: data.city,
            friends: data.total_friends, UserDetail: responseData.data, friendList: data.friends_list, friendCount: data.friends_list.length,

          })

          this.state.placeList.map((product) => {

            votes.push(product.place_name);

          })
          this.setState({ p_list: votes })

          this.state.interestList.map((i) => {

            interest.push(i.sub_category_name);

          })
          this.setState({ interest_list: interest })

          AsyncStorage.setItem('friendList', JSON.stringify(data.friends_list))
          AsyncStorage.setItem('name', data.name)
          AsyncStorage.setItem('email', data.email_address)
          AsyncStorage.setItem('mobile', data.mobile_number)
          AsyncStorage.setItem('mobile_code', data.country_code)
          AsyncStorage.setItem('mycity', data.city)

          if (data.dob != '' && data.dob != '0000-00-00') {
            AsyncStorage.setItem('birthdate', moment(data.dob).format('MMM D'))
          }
          else {
            AsyncStorage.setItem('birthdate', '')
          }

        } else {
          if (responseData.text === 'Invalid token key') {
            alert(responseData.text)
            this.props.navigation.navigate('Login')
          }
        }
      })

      .catch((error) => {

      })
  }

  getPostList = () => {

    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('limit', this.state.limit)
    data.append('offset', this.state.offset)
    data.append('type', 'my')
    this.setState({ refreshing: true })
    fetch(API_ROOT + 'plan/list', {
      method: 'post',
      body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          if (this.offset == 0) {
            this.offset = this.offset + this.state.limit
          }
          this.setState({
            planList: responseData.data, refreshing: false,
          });

          this.arrayholder = responseData.data;
        } else {
          this.setState({ refreshing: false })
          if (responseData.text == 'Invalid token key') {
            this.setState({ status: false })
            this.refs.toast.show(responseData.text);
            this.props.navigation.navigate('Login')
          }
        }
      })
      .catch((error) => {
        this.setState({ refreshing: false })

      })
  }

  onEndReached = () => {
    if (isloading == false) {
      isloading = true
      let data = new FormData();
      data.append('token', this.state.token)
      data.append('login_id', this.state.login_id)
      data.append('limit', this.state.limit)
      data.append('offset', this.offset)
      data.append('type', 0)
      //RNProgressHUB.showSpinIndeterminate()
      fetch(API_ROOT + 'plan/list', {
        method: 'post',
        body: data
      }).then(response => response.json())

        .then(responseData => {
          if (responseData.success) {
            this.offset = this.offset + this.state.limit;

            if (responseData.data.length == 0) {
              // response mathi blank array or aray length blank made to kai nai karvanu
            }
            else {
              let oldData = this.state.planList
              let newData = oldData.concat(responseData.data)
              this.setState({ planList: newData, refreshing: false })
              this.arrayholder = responseData.data;
              isloading = false
            }
            //RNProgressHUB.dismiss();

          } else {
            //RNProgressHUB.dismiss();
            this.setState({ status: false })
          }
        })
        .catch(err => {
          // RNProgressHUB.dismiss();
        });
    }
  }

  goToFriendProfile(item) {
    //alert(JSON.stringify(item))
    AsyncStorage.setItem('other_id', item.receiver_id)
    this.props.navigation.navigate('ohter_user_profile')
  }


  gotoEditEvent(item) {

    this.setState({ currentitem: item },

      () => {

        if (item.plan_type == 1) {

          if (item.is_invited_list.length > 0) {
            var arrfriends = item.is_invited_list
            var nameIDs = []
            var count = arrfriends.length - 1
            var name = ''

            if (arrfriends.length > 0) {
              name = arrfriends[0].receiver_name
              arrfriends.map((item) => {

                nameIDs.push(item.receiver_id)

              });

              AsyncStorage.setItem('receiver_id', String(nameIDs))
              AsyncStorage.setItem('Activity', item.activity)
              AsyncStorage.setItem('Location', item.line_1)
              AsyncStorage.setItem('who_desc', item.description)
              this.setDateValue(item.start_date, item.end_date)
              //var invitedusername = item.friend_name

              var invitedusername = ''

              if (arrfriends.length > 1) {
                invitedusername = item.friend_name + ' and ' + (arrfriends.length - 1) + ' More'
              }
              else {
                invitedusername = item.friend_name
              }

              this.props.navigation.navigate('Who2', { friendName: invitedusername, is_edit_plan: true, item: item, invitedList: item.is_invited_list })
            }
          }
        }
        else if (item.plan_type == 2) {

          var arrfriends = item.is_invited_list
          var nameIDs = []
          var count = arrfriends.length - 1
          var name = ''
          var contacts = item.contacts

          if (arrfriends.length > 0) {
            name = arrfriends[0].receiver_name
            arrfriends.map((item) => {

              nameIDs.push(item.receiver_id)

            });

          }
          AsyncStorage.setItem('what_friends', JSON.stringify(nameIDs))
          AsyncStorage.setItem('what_contacts', JSON.stringify(nameIDs))

          if (arrfriends.length == 0) {
            AsyncStorage.setItem('what_friends_name', '')
          }
          else if (arrfriends.length == 1) {
            AsyncStorage.setItem('what_friends_name', JSON.stringify(name))

          } else {
            AsyncStorage.setItem('what_friends_name', JSON.stringify(name + ' and ' + count + ' more'))

          }

          /* if (arrfriends.length == 0) {
             //AsyncStorage.setItem('what_friends_name', '')
               if(contacts.length == 0)
               {
                 AsyncStorage.setItem('what_friends_name', '')
               }
               else
               {
                 var cnt = contacts.length
                 if(contacts.length == 1)
                 {
                   AsyncStorage.setItem('what_friends_name', JSON.stringify(contacts))
                 }
                 else
                 {
                   AsyncStorage.setItem('what_friends_name', JSON.stringify(contacts[0] + ' and ' + (cnt - 1) + ' more'))
                 }
               }
             }
          else if (arrfriends.length == 1) {
               //AsyncStorage.setItem('what_friends_name', JSON.stringify(name))
               if(contacts.length > 0)
                 {
                   var cnt = contacts.length
                   AsyncStorage.setItem('what_friends_name', JSON.stringify(name + ' and ' + cnt + ' more'))
                 }
                 else
                 {
                   AsyncStorage.setItem('what_friends_name', JSON.stringify(name))
                 }
   
           } else {
               //AsyncStorage.setItem('what_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
               if(contacts.length > 0)
                 {
                   var cnt = contacts.length
                   count = count + cnt
                   AsyncStorage.setItem('what_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
                 }
                 else
                 {
                   AsyncStorage.setItem('what_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
                 }
           }
           */

          AsyncStorage.setItem('What_Activity', item.activity)
          AsyncStorage.setItem('what_Location', item.line_1)
          AsyncStorage.setItem('what_FullAddress', item.line_2)
          AsyncStorage.setItem('what_desc', item.description)
          this.setDateValueForWhat(item.start_date, item.end_date)
          this.props.navigation.navigate('Frinds', { is_edit_plan: true, item: item })
        }
        else if (item.plan_type == 3) {

          var arrfriends = item.is_invited_list
          var nameIDs = []
          var count = arrfriends.length - 1
          var name = ''
          var contacts = item.contacts

          if (arrfriends.length > 0) {
            name = arrfriends[0].receiver_name
            arrfriends.map((item) => {

              nameIDs.push(item.receiver_id)

            });

          }
          AsyncStorage.setItem('where_friends', JSON.stringify(nameIDs))
          AsyncStorage.setItem('where_contacts', JSON.stringify(contacts))

          if (arrfriends.length == 0) {
            AsyncStorage.setItem('where_friends_name', '')
          }
          else if (arrfriends.length == 1) {
            AsyncStorage.setItem('where_friends_name', JSON.stringify(name))

          } else {
            AsyncStorage.setItem('where_friends_name', JSON.stringify(name + ' and ' + count + ' more'))

          }

          /*  if (arrfriends.length == 0) {
              //AsyncStorage.setItem('where_friends_name', '')
              if(contacts.length == 0)
                {
                  AsyncStorage.setItem('where_friends_name', '')
                }
                else
                {
                  var cnt = contacts.length
                  if(contacts.length == 1)
                  {
                    AsyncStorage.setItem('where_friends_name', JSON.stringify(contacts))
                  }
                  else
                  {
                    AsyncStorage.setItem('where_friends_name', JSON.stringify(contacts[0] + ' and ' + (cnt - 1) + ' more'))
                  }
                }
            }
            else if (arrfriends.length == 1) {
                //AsyncStorage.setItem('where_friends_name', JSON.stringify(name))
                if(contacts.length > 0)
                  {
                    var cnt = contacts.length
                    AsyncStorage.setItem('where_friends_name', JSON.stringify(name + ' and ' + cnt + ' more'))
                  }
                  else
                  {
                    AsyncStorage.setItem('where_friends_name', JSON.stringify(name))
                  }
            } else {
                //AsyncStorage.setItem('where_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
                if(contacts.length > 0)
                  {
                    var cnt = contacts.length
                    count = count + cnt
                    AsyncStorage.setItem('where_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
                  }
                  else
                  {
                    AsyncStorage.setItem('where_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
                  }
            }
            */
          AsyncStorage.setItem('where_Location', item.line_1)
          AsyncStorage.setItem('where_FullAddress', item.line_2)
          AsyncStorage.setItem('where_Activity', item.activity)
          AsyncStorage.setItem('where_desc', item.description)
          this.setDateValueForWhere(item.start_date, item.end_date)
          this.props.navigation.navigate('where2', { is_edit_plan: true, item: item })
        }
        else if (item.plan_type == 4) {

          var arrfriends = item.is_invited_list
          var nameIDs = []

          var count = arrfriends.length - 1
          var name = ''
          var contacts = item.contacts

          if (arrfriends.length > 0) {
            name = arrfriends[0].receiver_name
            arrfriends.map((item) => {

              nameIDs.push(item.receiver_id)


            });

          }
          AsyncStorage.setItem('when_friends', JSON.stringify(nameIDs))
          AsyncStorage.setItem('when_contacts', JSON.stringify(contacts))

          if (arrfriends.length == 0) {
            AsyncStorage.setItem('when_friends_name', '')
          }
          else if (arrfriends.length == 1) {
            AsyncStorage.setItem('when_friends_name', JSON.stringify(name))
          } else {
            AsyncStorage.setItem('when_friends_name', JSON.stringify(name + ' and ' + count + ' more'))

          }

          /* if (arrfriends.length == 0) {
             //AsyncStorage.setItem('when_friends_name', '')
             if(contacts.length == 0)
               {
                 AsyncStorage.setItem('when_friends_name', '')
               }
               else
               {
                 var cnt = contacts.length
                 if(contacts.length == 1)
                 {
                   AsyncStorage.setItem('when_friends_name', JSON.stringify(contacts))
                 }
                 else
                 {
                   AsyncStorage.setItem('when_friends_name', JSON.stringify(contacts[0] + ' and ' + (cnt - 1) + ' more'))
                 }
               }
           }
           else if (arrfriends.length == 1) {
               //AsyncStorage.setItem('when_friends_name', JSON.stringify(name))
               if(contacts.length > 0)
                 {
                   var cnt = contacts.length
                   AsyncStorage.setItem('when_friends_name', JSON.stringify(name + ' and ' + cnt + ' more'))
                 }
                 else
                 {
                   AsyncStorage.setItem('when_friends_name', JSON.stringify(name))
                 }
           } else {
               //AsyncStorage.setItem('when_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
               
               if(contacts.length > 0)
                 {
                   var cnt = contacts.length
                   count = count + cnt
                   AsyncStorage.setItem('when_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
                 }
                 else
                 {
                   AsyncStorage.setItem('when_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
                 }
           }
           */
          AsyncStorage.setItem('when_Location', item.line_1)
          AsyncStorage.setItem('when_FullAddress', item.line_2)
          AsyncStorage.setItem('when_Activity', item.activity)
          AsyncStorage.setItem('when_desc', item.description)
          this.setDateValueForWhen(item.start_date, item.end_date)
          this.props.navigation.navigate('when2', { is_edit_plan: true, item: item })
        }

      }

    )

  }

  gotoEventDetail() {

    //alert(JSON.stringify(this.state.currentitem))

    this.setState({ isModalVisible: false })

    this.gotoEditEvent(this.state.currentitem)

    this.setState({ isMyModalVisible: false, isMyDeleteModalVisible: false })

  }

  setDateValue(sdate, edate) {

    var currentDate = moment().format("ll");
    let tomorrow = moment().add(1, 'days').format("ll")
    var date = moment(edate).format('ll')

    if (sdate != '') {

      var gmtStartDateTime = moment.utc(sdate)
      var startdate = gmtStartDateTime.local();

      var gmtEndDateTime = moment.utc(edate)
      var enddate = gmtEndDateTime.local();

      // if (this.state.currentitem.is_now) {
      //   if (date == currentDate) {
      //     AsyncStorage.setItem('CurrentDate', 'Now - ' + moment(enddate).format('LT'))
      //   } else {
      //     if (date == tomorrow) {
      //       AsyncStorage.setItem('CurrentDate', 'Now - Tomorrow · ' + moment(enddate).format('LT'))

      //     } else {
      //       AsyncStorage.setItem('CurrentDate', 'Now - ' + moment(date).format('ddd, MMM D') + ' · ' + moment(enddate).format('LT'))
      //     }
      //   }
      //   AsyncStorage.setItem('start', currentDate)
      //   AsyncStorage.setItem('end', moment(enddate).format('lll'))
      //   AsyncStorage.setItem('is_now', '1')
      //   AsyncStorage.setItem('is_allday', '0')
      // }
      // else 
      if (this.state.currentitem.is_allday == 1) {
        if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
          AsyncStorage.setItem('CurrentDate', 'Today · All Day')
        }
        else {
          if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
            AsyncStorage.setItem('CurrentDate', 'Today - Tomorrow · All Day')

          } else {
            if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
              AsyncStorage.setItem('CurrentDate', 'Tomorrow · All Day')

            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                AsyncStorage.setItem('CurrentDate', 'Tomorrow - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  AsyncStorage.setItem('CurrentDate', 'Today - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

                } else {
                  if (moment(startdate).format('ll') == moment(enddate).format('ll')) {
                    AsyncStorage.setItem('CurrentDate', 'All Day - ' + moment(enddate).format('ddd, MMM D'))

                  } else {
                    AsyncStorage.setItem('CurrentDate', moment(startdate).format('ddd, MMM D') + ' - ' + moment(enddate).format('ddd, MMM D') + ' · ALL DAY')

                  }
                }

              }

            }

          }

        }
        AsyncStorage.setItem('start', moment(startdate).format('ll'))
        AsyncStorage.setItem('end', moment(enddate).format('ll'))
        AsyncStorage.setItem('is_now', '0')
        AsyncStorage.setItem('is_allday', '1')
      }
      else {

        if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
          AsyncStorage.setItem('CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))

        } else {
          if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
            AsyncStorage.setItem('CurrentDate', 'Tomorrow \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))
          } else {
            if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
              AsyncStorage.setItem('CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - Tomorrow \u00B7 ' + moment(enddate).format('LT'))
            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                AsyncStorage.setItem('CurrentDate', ' Tomorrow - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  if (currentDate == moment(enddate).format('ll')) {
                    AsyncStorage.setItem('CurrentDate', 'Today · All Day')
                  }
                  else {
                    AsyncStorage.setItem('CurrentDate', 'Today - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
                  }


                } else {
                  AsyncStorage.setItem('CurrentDate', moment(startdate).format('ddd, MMM D \u00B7 LT') + ' to ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
                }
              }
            }
          }
        }
        AsyncStorage.setItem('start', moment(startdate).format('lll'))
        AsyncStorage.setItem('end', moment(enddate).format('lll'))
        AsyncStorage.setItem('is_now', '0')
        AsyncStorage.setItem('is_allday', '0')
      }
    }
    else {
      AsyncStorage.setItem('start', '')
      AsyncStorage.setItem('end', '')
      AsyncStorage.setItem('CurrentDate', '')
    }
  }

  setDateValueForWhat(sdate, edate) {
    var currentDate = moment().format("ll");
    let tomorrow = moment().add(1, 'days').format("ll")
    var date = moment(edate).format('ll')


    if (sdate != '') {

      var gmtStartDateTime = moment.utc(sdate)
      var startdate = gmtStartDateTime.local();

      var gmtEndDateTime = moment.utc(edate)
      var enddate = gmtEndDateTime.local();

      //     if (this.state.currentitem.is_now) {
      //       if (date == currentDate) {
      //         AsyncStorage.setItem('what_CurrentDate', 'Now - ' + moment(enddate).format('LT'))
      //       } else {
      //         if (date == tomorrow) {
      //           AsyncStorage.setItem('what_CurrentDate', 'Now - Tomorrow · ' + moment(enddate).format('LT'))

      //         } else {
      //           AsyncStorage.setItem('what_CurrentDate', 'Now - ' + moment(date).format('ddd, MMM D') + ' · ' + moment(enddate).format('LT'))

      //         }

      //       }

      //       AsyncStorage.setItem('what_start', currentDate)
      //       AsyncStorage.setItem('what_end', moment(enddate).format('lll'))
      //       AsyncStorage.setItem('what_is_now', '1')
      //       AsyncStorage.setItem('what_is_allday', '0')
      // }
      // else 
      if (this.state.currentitem.is_allday == 1) {
        if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
          AsyncStorage.setItem('what_CurrentDate', 'Today · All Day')
        }
        else {
          if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
            AsyncStorage.setItem('what_CurrentDate', 'Today - Tomorrow · All Day')

          } else {
            if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
              AsyncStorage.setItem('what_CurrentDate', 'Tomorrow · All Day')

            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                AsyncStorage.setItem('what_CurrentDate', 'Tomorrow - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  AsyncStorage.setItem('what_CurrentDate', 'Today - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

                } else {
                  if (moment(startdate).format('ll') == moment(enddate).format('ll')) {
                    AsyncStorage.setItem('what_CurrentDate', 'All Day - ' + moment(enddate).format('ddd, MMM D'))

                  } else {
                    AsyncStorage.setItem('what_CurrentDate', moment(startdate).format('ddd, MMM D') + ' - ' + moment(enddate).format('ddd, MMM D') + ' · ALL DAY')

                  }
                }
              }

            }

          }

        }
        AsyncStorage.setItem('what_start', moment(startdate).format('ll'))
        AsyncStorage.setItem('what_end', moment(enddate).format('ll'))
        AsyncStorage.setItem('what_is_now', '0')
        AsyncStorage.setItem('what_is_allday', '1')
      }
      else {
        ////////////////////////
        if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
          AsyncStorage.setItem('what_CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))

        } else {
          if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
            AsyncStorage.setItem('what_CurrentDate', 'Tomorrow \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))
          } else {
            if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
              AsyncStorage.setItem('what_CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - Tomorrow \u00B7 ' + moment(enddate).format('LT'))
            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                AsyncStorage.setItem('what_CurrentDate', ' Tomorrow  ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  if (currentDate == moment(enddate).format('ll')) {
                    AsyncStorage.setItem('what_CurrentDate', 'Today · All Day')
                  }
                  else {
                    AsyncStorage.setItem('what_CurrentDate', 'Today - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
                  }

                } else {
                  AsyncStorage.setItem('what_CurrentDate', moment(startdate).format('ddd, MMM D \u00B7 LT') + ' to ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
                }
              }
            }


          }
        }
        AsyncStorage.setItem('what_start', moment(startdate).format('lll'))
        AsyncStorage.setItem('what_end', moment(enddate).format('lll'))
        AsyncStorage.setItem('what_is_now', '0')
        AsyncStorage.setItem('what_is_allday', '0')
      }
    }
    else {
      AsyncStorage.setItem('what_start', '')
      AsyncStorage.setItem('what_end', '')
      AsyncStorage.setItem('what_CurrentDate', '')
    }

  }

  setDateValueForWhere(sdate, edate) {
    var currentDate = moment().format("ll");
    let tomorrow = moment().add(1, 'days').format("ll")
    var date = moment(edate).format('ll')

    if (sdate != '') {

      var gmtStartDateTime = moment.utc(sdate)
      var startdate = gmtStartDateTime.local();

      var gmtEndDateTime = moment.utc(edate)
      var enddate = gmtEndDateTime.local();

      // if (this.state.currentitem.is_now) {
      //   if (date == currentDate) {
      //     AsyncStorage.setItem('where_CurrentDate', 'Now - ' + moment(enddate).format('LT'))
      //   } else {
      //     if (date == tomorrow) {
      //       AsyncStorage.setItem('where_CurrentDate', 'Now - Tomorrow · ' + moment(enddate).format('LT'))

      //     } else {
      //       AsyncStorage.setItem('where_CurrentDate', 'Now - ' + moment(date).format('ddd, MMM D') + ' · ' + moment(enddate).format('LT'))

      //     }

      //   }

      //   AsyncStorage.setItem('where_start', currentDate)
      //   AsyncStorage.setItem('where_end', moment(enddate).format('lll'))
      //   AsyncStorage.setItem('where_is_now', '1')
      //   AsyncStorage.setItem('where_is_allday', '0')


      // }
      // else 
      if (this.state.currentitem.is_allday == 1) {
        if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
          AsyncStorage.setItem('where_CurrentDate', 'Today · All Day')
        }
        else {
          if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
            AsyncStorage.setItem('where_CurrentDate', 'Today - Tomorrow · All Day')

          } else {
            if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
              AsyncStorage.setItem('where_CurrentDate', 'Tomorrow · All Day')

            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                AsyncStorage.setItem('where_CurrentDate', 'Tomorrow - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  AsyncStorage.setItem('where_CurrentDate', 'Today - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

                } else {
                  if (moment(startdate).format('ll') == moment(enddate).format('ll')) {
                    AsyncStorage.setItem('where_CurrentDate', 'All Day - ' + moment(enddate).format('ddd, MMM D'))

                  } else {
                    AsyncStorage.setItem('where_CurrentDate', moment(startdate).format('ddd, MMM D') + ' - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

                  }

                }
              }

            }

          }

        }
        AsyncStorage.setItem('where_start', moment(startdate).format('ll'))
        AsyncStorage.setItem('where_end', moment(startdate).format('ll'))
        AsyncStorage.setItem('where_is_now', '0')
        AsyncStorage.setItem('where_is_allday', '1')
      }
      else {
        /////////////

        if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
          AsyncStorage.setItem('where_CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))

        } else {
          if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
            AsyncStorage.setItem('where_CurrentDate', 'Tomorrow \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))
          } else {
            if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
              AsyncStorage.setItem('where_CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - Tomorrow \u00B7 ' + moment(enddate).format('LT'))
            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                AsyncStorage.setItem('where_CurrentDate', ' Tomorrow - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  AsyncStorage.setItem('where_CurrentDate', ' Today - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))

                } else {
                  AsyncStorage.setItem('where_CurrentDate', moment(startdate).format('ddd, MMM D \u00B7 LT') + ' to ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))

                }

              }
            }


          }
        }
        AsyncStorage.setItem('where_start', moment(startdate).format('lll'))
        AsyncStorage.setItem('where_end', moment(enddate).format('lll'))
        AsyncStorage.setItem('where_is_now', '0')
        AsyncStorage.setItem('where_is_allday', '0')
      }
    }
    else {
      AsyncStorage.setItem('where_start', '')
      AsyncStorage.setItem('where_end', '')
      AsyncStorage.setItem('where_CurrentDate', '')
    }
  }

  setDateValueForWhen(sdate, edate) {

    var currentDate = moment().format("ll");
    let tomorrow = moment().add(1, 'days').format("ll")
    var date = moment(edate).format('ll')


    if (sdate != '') {

      var gmtStartDateTime = moment.utc(sdate)
      var startdate = gmtStartDateTime.local();

      var gmtEndDateTime = moment.utc(edate)
      var enddate = gmtEndDateTime.local();

      // if (this.state.currentitem.is_now) {
      //   if (date == currentDate) {
      //     AsyncStorage.setItem('when_CurrentDate', 'Now - ' + moment(enddate).format('LT'))
      //   } else {
      //     if (date == tomorrow) {
      //       AsyncStorage.setItem('when_CurrentDate', 'Now - Tomorrow · ' + moment(enddate).format('LT'))

      //     } else {
      //       AsyncStorage.setItem('when_CurrentDate', 'Now - ' + moment(this.state.date).format('ddd, MMM D') + ' · ' + moment(enddate).format('LT'))

      //     }

      //   }

      //   AsyncStorage.setItem('when_start', currentDate)
      //   AsyncStorage.setItem('when_end', moment(enddate).format('lll'))
      //   AsyncStorage.setItem('when_is_now', '1')
      //   AsyncStorage.setItem('when_is_allday', '0')
      // }
      // else 
      if (this.state.currentitem.is_allday == 1) {
        if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
          AsyncStorage.setItem('when_CurrentDate', 'Today · All Day')
        }
        else {
          if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
            AsyncStorage.setItem('when_CurrentDate', 'Today - Tomorrow · All Day')

          } else {
            if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
              AsyncStorage.setItem('when_CurrentDate', 'Tomorrow · All Day')

            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                AsyncStorage.setItem('when_CurrentDate', 'Tomorrow - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  AsyncStorage.setItem('when_CurrentDate', 'Today - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

                } else {
                  if (moment(startdate).format('ll') == moment(enddate).format('ll')) {
                    AsyncStorage.setItem('when_CurrentDate', 'All Day - ' + moment(enddate).format('ddd, MMM D'))

                  } else {
                    AsyncStorage.setItem('when_CurrentDate', moment(startdate).format('ddd, MMM D') + ' - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')

                  }
                }
              }

            }

          }

        }
        AsyncStorage.setItem('when_start', moment(startdate).format('ll'))
        AsyncStorage.setItem('when_end', moment(enddate).format('ll'))
        AsyncStorage.setItem('when_is_now', '0')
        AsyncStorage.setItem('when_is_allday', '1')
      }
      else {
        ////////////////////////////////////////////////

        if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
          AsyncStorage.setItem('when_CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))

        } else {
          if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
            AsyncStorage.setItem('when_CurrentDate', 'Tomorrow \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))
          } else {
            if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
              AsyncStorage.setItem('when_CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - Tomorrow \u00B7 ' + moment(enddate).format('LT'))
            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                AsyncStorage.setItem('when_CurrentDate', ' Tomorrow - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  AsyncStorage.setItem('when_CurrentDate', ' Today - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))

                } else {
                  AsyncStorage.setItem('when_CurrentDate', moment(startdate).format('ddd, MMM D \u00B7 LT') + ' - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
                }
              }
            }


          }
        }
        AsyncStorage.setItem('when_start', moment(startdate).format('lll'))
        AsyncStorage.setItem('when_end', moment(enddate).format('lll'))
        AsyncStorage.setItem('when_is_now', '0')
        AsyncStorage.setItem('when_is_allday', '0')
      }
    }
    else {
      AsyncStorage.setItem('when_start', '')
      AsyncStorage.setItem('when_end', '')
      AsyncStorage.setItem('when_CurrentDate', '')
    }

  }


  modelVisible = (item) => {

    var name = item.friend_name.split(" ")

    if (item.plan_type == 1) {
      //this.eventtitle = name[0]+", let's meet up!"
      this.eventtitle = "Let's meet up!"
    }
    else if (item.plan_type == 2) {
      this.eventtitle = item.activity
    }
    else if (item.plan_type == 3) {
      this.eventtitle = "Wanna go to " + item.line_1 + "?"
    }
    else if (item.plan_type == 4) {
      this.eventtitle = "I'm free, wanna hangout?"
    }

    this.setState({ isModalVisible: true, post_name: item.name, plan_id: item.plan_id, currentitem: item })
  }
  _keyExtractor = (item, index) => item.plan_id;
  _renderItem = ({ item }) => {

    let Final_date = ''
    let dateTime = '';
    var currentDate = moment().format("ll");
    var tomorrow = moment().add(1, 'days').format("ll")

    var currentDateTime = moment().format("lll")
    var name = item.friend_name.split(" ")
    var start = item.start_date.split(" ")
    var end = item.end_date.split(" ")

    var gmtDateTime = moment.utc(item.created_date)
    var date1 = gmtDateTime.local();

    if (moment(date1).format("ll") == moment().format("ll")) {
      dateTime = date1.fromNow();
    } else {
      dateTime = date1.calendar();

    }
    var activity = ''
    var str = String(item.activity);
    if (str.includes('Wanna')) {
      activity = str.substr(6).slice(0, -1);
    } else {
      activity = str
    }
    if (activity.includes('?')) {
      activity = activity.slice(0, -1)
    }

    if (item.start_date != '') {

      var gmtStartDateTime = moment.utc(item.start_date)
      var startdate = gmtStartDateTime.local();

      var gmtEndDateTime = moment.utc(item.end_date)
      var enddate = gmtEndDateTime.local();


      //     if (this.state.currentitem.is_now) {
      //       if (date == currentDate) {
      //         AsyncStorage.setItem('what_CurrentDate', 'Now - ' + moment(enddate).format('LT'))
      //       } else {
      //         if (date == tomorrow) {
      //           AsyncStorage.setItem('what_CurrentDate', 'Now - Tomorrow · ' + moment(enddate).format('LT'))

      //         } else {
      //           AsyncStorage.setItem('what_CurrentDate', 'Now - ' + moment(date).format('ddd, MMM D') + ' · ' + moment(enddate).format('LT'))

      //         }

      //       }

      //       AsyncStorage.setItem('what_start', currentDate)
      //       AsyncStorage.setItem('what_end', moment(enddate).format('lll'))
      //       AsyncStorage.setItem('what_is_now', '1')
      //       AsyncStorage.setItem('what_is_allday', '0')
      // }
      // else 

      if (item.is_allday == 1) {

        if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
          //AsyncStorage.setItem('what_CurrentDate', 'Today · All Day')
          Final_date = 'Today · All Day'
        }
        else {
          if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
            //AsyncStorage.setItem('what_CurrentDate', 'Today - Tomorrow · All Day')
            Final_date = 'Today - Tomorrow · All Day'

          } else {
            if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
              //AsyncStorage.setItem('what_CurrentDate', 'Tomorrow · All Day')
              Final_date = 'Tomorrow · All Day'
            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                //AsyncStorage.setItem('what_CurrentDate', 'Tomorrow - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')
                Final_date = 'Tomorrow - ' + moment(enddate).format('ddd, MMM D') + ' · All Day'

              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  //AsyncStorage.setItem('what_CurrentDate', 'Today - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')
                  Final_date = 'Today - ' + moment(enddate).format('ddd, MMM D') + ' · All Day'
                } else {
                  if (moment(startdate).format('ll') == moment(enddate).format('ll')) {
                    //AsyncStorage.setItem('what_CurrentDate', 'All Day - ' + moment(enddate).format('ddd, MMM D'))
                    Final_date = 'All Day - ' + moment(enddate).format('ddd, MMM D')
                  } else {
                    //AsyncStorage.setItem('what_CurrentDate', moment(startdate).format('ddd, MMM D') + ' - ' + moment(enddate).format('ddd, MMM D') + ' · ALL DAY')
                    Final_date = moment(startdate).format('ddd, MMM D') + ' - ' + moment(enddate).format('ddd, MMM D') + ' · All Day'
                  }
                }
              }

            }

          }

        }
      }
      else {
        ////////////////////////

        if (currentDate == moment(startdate).format('ll') && currentDate == moment(enddate).format('ll')) {
          //AsyncStorage.setItem('what_CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))
          Final_date = 'Today \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT')
        } else {
          if (tomorrow == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
            //AsyncStorage.setItem('what_CurrentDate', 'Tomorrow \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))
            Final_date = 'Tomorrow \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT')
          } else {
            if (currentDate == moment(startdate).format('ll') && tomorrow == moment(enddate).format('ll')) {
              //AsyncStorage.setItem('what_CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - Tomorrow \u00B7 ' + moment(enddate).format('LT'))
              Final_date = 'Today \u00B7 ' + moment(startdate).format('LT') + ' - Tomorrow \u00B7 ' + moment(enddate).format('LT')
            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                //AsyncStorage.setItem('what_CurrentDate', ' Tomorrow  ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
                Final_date = ' Tomorrow  ' + moment(enddate).format('ddd, MMM D \u00B7 LT')
              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  if (currentDate == moment(enddate).format('ll')) {
                    //AsyncStorage.setItem('what_CurrentDate', 'Today · All Day')
                    Final_date = 'Today · All Day'
                  }
                  else {
                    //AsyncStorage.setItem('what_CurrentDate', 'Today - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
                    Final_date = 'Today - ' + moment(enddate).format('ddd, MMM D \u00B7 LT')
                  }

                } else {
                  //AsyncStorage.setItem('what_CurrentDate', moment(startdate).format('ddd, MMM D \u00B7 LT') + ' to ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
                  Final_date = moment(startdate).format('ddd, MMM D \u00B7 LT') + ' to ' + moment(enddate).format('ddd, MMM D \u00B7 LT')
                }
              }
            }
          }
        }
      }
    }

    /*
        if(item.start_date != '')
        {
    
          var gmtStartDateTime = moment.utc(item.start_date)
          var localstartdate = gmtStartDateTime.local();
    
          var gmtEndDateTime = moment.utc(item.end_date)
          var localenddate = gmtEndDateTime.local();
    
          if (item.is_now == '1') {
            if (currentDate == moment(localstartdate).format('ll') && currentDate == moment(localenddate).format('ll')) {
              Final_date = 'Now - ' + moment(localenddate).format('LT')
            } else {
              if (currentDate == moment(localstartdate).format('ll') && tomorrow == moment(localenddate).format('ll')) {
                Final_date = 'Now - Tomorrow · ' + moment(localenddate).format('LT')
              } else {
                Final_date = 'Now - ' + moment(localenddate).format('ddd, MMM D \u00B7 LT')
              }
      
            }
          } else {
            if (currentDate == moment(localstartdate).format('ll') && currentDate == moment(localenddate).format('ll')) {
              if (start[1] == '00:00:00' && end[1] == '00:00:00') {
      
                Final_date = 'Today · All Day'
              } else {
                if(item.is_allday == 1)
                {
                   Final_date = 'Today · All Day'
                }
                else
                {
                  Final_date = 'Today · ' + moment(localstartdate).format('LT')
                }
              }
            } else {
              if (start[1] == '00:00:00' && end[1] == '00:00:00') {
                if (tomorrow == moment(localstartdate).format('ll') && tomorrow == moment(localenddate).format('ll')) {
                  Final_date = 'Tomorrow · All Day'
      
                } else {
                  if (tomorrow == moment(localstartdate).format('ll')) {
                    Final_date = 'Tomorrow · All Day'
      
                  } else {
                    if (currentDate == moment(localstartdate).format('ll')) {
                      Final_date = 'Today' + ' · All Day '
      
                    } else {
      
                      Final_date = moment(localstartdate).format('ddd, MMM D') + ' · All Day '
                    }
      
                  }
      
                }
              } else {
                if (tomorrow == moment(localstartdate).format('ll') && tomorrow == moment(localenddate).format('ll')) {
                  if(item.is_allday == 1)
                  {
                    Final_date = 'Tomorrow · All Day'
                  }
                  else
                  {
                    Final_date = 'Tomorrow · ' + moment(localstartdate).format('LT')
                  }
      
                } else {
                  if (tomorrow == moment(localstartdate).format('ll')) {
                    Final_date = 'Tomorrow · ' + moment(localstartdate).format('LT')
      
                  } else {
                    if (currentDate == moment(localstartdate).format('ll')) {
                      Final_date = 'Today ·' + moment(localstartdate).format('LT')
      
                    } else {
      
                      if(item.is_allday == 1)
                      {
                        Final_date = moment(localstartdate).format('ddd, MMM D') + ' · All Day '
                      }
                      else
                      {
                        Final_date = moment(localstartdate).format('ddd, MMM D \u00B7 LT')
                      }
                    }
      
                  }
      
                }
      
              }
      
            }
      
          }
        }
        */
    return (<View>
      {item.plan_type == 1 ?
        <View style={{ backgroundColor: colors.white }}>
          <View style={{ height: Height(9), flexDirection: 'row', alignItems: 'center', backgroundColor: 'white' }}>
            <View style={{ alignItems: 'center', marginLeft: Width(3) }}>
              <View underlayColor="transparent" style={{ height: Height(5), width: Height(5), borderRadius: Height(7), overflow: 'hidden' }}>
                {item.photo == '' ? <Image source={avtar} style={{ height: '100%', width: '100%' }} /> :
                  <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.photo }} />}
              </View>
            </View>
            <View style={{ justifyContent: 'center', marginLeft: Width(3) }}>
              <Text style={{ fontSize: FontSize(18), color: colors.dargrey, fontFamily: fonts.Raleway_Bold }}>{item.login_id == this.state.login_id ? 'Me' : name[0]}</Text>
              <Text style={{ fontSize: FontSize(12), color: colors.fontDarkGrey, fontFamily: fonts.Roboto_Regular }}>{dateTime}{item.share_with == '' ? null : '\u00B7 '}{item.share_with}</Text>

            </View>
            <View style={{ alignItems: 'flex-end', alignSelf: 'center', marginRight: Width(4), flex: 4 }}>
              <TouchableOpacity onPress={() => this.modelVisible(item)} underlayColor="transparent" style={{ justifyContent: 'center', height: Height(6), width: Width(6) }}>
                <Entypo size={Width(6)} name="dots-three-horizontal" color="#000" />
              </TouchableOpacity>
            </View>
          </View>
          <TouchableOpacity onPress={() => {
            //this.props.navigation.navigate('EventDetail', { item: item , plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type , title_header:name[0]+", let's meet up!", Final_date : Final_date, eventby:item.name}) 
            this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "Let's meet up!", Final_date: Final_date, eventby: item.name, invitedList: item.is_invited_list })
          }}>
            <ImageBackground source={item.plan_image == '' || item.plan_image == null || item.plan_image == undefined ? default_placeholder : { uri: item.plan_image }} style={{ backgroundColor: '#F7F7F7', height: 265, justifyContent: 'center', alignItems: 'center' }}>
              {/* <Text style={{ fontSize: FontSize(20), color: colors.dargrey, fontFamily: fonts.Raleway_Semibold,marginHorizontal:Width(5), lineHeight:27,textAlign:'center'}}>{name[0]}, let's meet up!</Text> */}
              {/* <Text style={{ fontSize: FontSize(20), color: colors.dargrey, fontFamily: fonts.Raleway_Semibold,marginHorizontal:Width(5), lineHeight:27,textAlign:'center'}}>Let's meet up!</Text>
            {item.description == '' ? null : <Text style={{ textAlign: 'center', fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, width: '85%', marginTop: 2 }}>{item.description}</Text>} */}
            </ImageBackground>
          </TouchableOpacity>
          <View style={{ paddingHorizontal: Width(4.5) }}>
            <Text style={{ fontSize: FontSize(19), color: colors.dargrey, fontFamily: fonts.Raleway_Bold, lineHeight: 27, textAlign: 'left', marginTop: 10 }}>Let's meet up!</Text>
            {/* {item.description == '' ? null : <Text style={{ textAlign: 'left', fontSize: FontSize(13), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, marginTop: 4,marginHorizontal:Width(4.5)}}>{item.description}</Text>} */}
            {item.description == '' ? null : <SeeMore style={{ textAlign: 'left', fontSize: FontSize(13), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, marginTop: 4 }} numberOfLines={2} seeMoreText='more'>{item.description}</SeeMore>}
          </View>
          <View style={{ backgroundColor: '#FFF', alignItems: 'center', flexDirection: 'row' }}>
            <View style={{ flex: 1, marginLeft: Height(2.5) }}>

              <TouchableOpacity
                style={{ flexDirection: 'row', marginTop: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={chai_cup} />
                {item.activity == '' ? <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}
                  onPress={item.total_activity_suggestions == 0 ? () => this.props.navigation.navigate('Activity', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true, title_header: "Let's meet up!" }) : () => this.props.navigation.navigate('SuggestActivity', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false, title_header: "Let's meet up!" })}>{item.total_activity_suggestions == 0 ? 'Suggest an activity' : 'View activity suggestions'}</Text>
                  : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.activity}</Text>}
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginTop: Height(2), marginBottom: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={calendar} />
                {item.start_date == '' ? <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}
                  onPress={item.total_datetime_suggestions == 0 ? () => this.props.navigation.navigate('DateTime', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true }) : () => this.props.navigation.navigate('SuggestDateTime', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false })}>{item.total_datetime_suggestions == 0 ? 'Suggest a date & time' : 'View date & time suggestions'}</Text>
                  : <Text style={{ width: Width(60), paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{Final_date}</Text>}
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginBottom: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center', tintColor: colors.whatFontColor }} source={location} />
                {item.line_1 == '' ? <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}
                  onPress={item.total_location_suggestions == 0 ? () => this.props.navigation.navigate('Location', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true }) : () => this.props.navigation.navigate('SuggestLocation', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false })}>{item.total_location_suggestions == 0 ? 'Suggest a location' : 'View location suggestions'}</Text>
                  : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.line_1}</Text>}
              </TouchableOpacity>

              <View style={{ height: Height(0.15), backgroundColor: '#EFEFF4', marginRight: Width(4.5) }} />

              {item.is_going_count != 0 || item.is_interested_count != 0 || item.is_invited_count != 0 ?
                <View
                  style={{ flexDirection: 'row', marginBottom: Height(1) }}>
                  {/* {item.is_going_count != 0 || item.is_interested_count != 0 || item.is_invited_count != 0 ?
                  <Image resizeMode="contain" style={{ marginTop: Height(2), alignSelf: 'center', tintColor: colors.whatFontColor }} source={new_user} /> : null} */}
                  {item.is_going_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 0, Title_New: item.activity, plan_type: item.plan_type }) }} style={{ marginTop: Height(1.5), paddingLeft: Width(0), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey }}>{item.is_going_count + ' going'}</Text> : null}
                  {item.is_interested_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 1, Title_New: item.activity, plan_type: item.plan_type }) }} style={item.is_going_count == 0 ? { marginTop: Height(1.5), paddingLeft: Width(0), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey } : { marginTop: Height(1.5), paddingLeft: Width(1), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey }}>{item.is_going_count != 0 ? '\u00B7 ' : null}{item.is_interested_count + ' interested'}</Text> : null}
                  {item.is_invited_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 2, Title_New: item.activity, plan_type: item.plan_type }) }} style={item.is_going_count == 0 && item.is_interested_count == 0 ? { marginTop: Height(1.5), paddingLeft: Width(0), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey } : { marginTop: Height(1.5), paddingLeft: Width(1), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey }}>{item.is_interested_count != 0 || item.is_going_count != 0 ? '\u00B7 ' : null}{item.is_invited_count + ' invited'}</Text> : null}
                  <TouchableOpacity style={{ marginTop: Height(1.5), position: 'absolute', right: Width(4), width: 35, height: 35 }} onPress={() => {
                    //this.props.navigation.navigate('EventDetail', { item: item , plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type , title_header:name[0]+", let's meet up!", Final_date : Final_date, eventby:item.name}) 
                    this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "Let's meet up!", Final_date: Final_date, eventby: item.name, invitedList: item.is_invited_list })
                  }}>
                    <Image resizeMode="contain" source={ic_chat} />
                  </TouchableOpacity>
                </View>
                :
                <View>
                  {item.login_id != this.state.login_id ?
                    null
                    :
                    <Text style={{ marginTop: Height(1.5), marginBottom: Height(1), paddingLeft: Width(0), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey }}>No guests yet</Text>
                  }
                  <TouchableOpacity style={{ marginTop: Height(1.5), marginBottom: Height(1.5), position: 'absolute', right: Width(4), width: 35, height: 35 }} onPress={() => {
                    //this.props.navigation.navigate('EventDetail', { item: item , plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type , title_header:name[0]+", let's meet up!", Final_date : Final_date, eventby:item.name}) 
                    this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "Let's meet up!", Final_date: Final_date, eventby: item.name, invitedList: item.is_invited_list })
                  }}>
                    <Image resizeMode="contain" source={ic_chat} />
                  </TouchableOpacity>
                </View>
              }

            </View>

          </View>
          <View style={{ height: Height(0.5) }} />
          <View
            style={{ backgroundColor: 'white' }}>
            <View style={{ height: Height(0.15), backgroundColor: '#EFEFF4', marginRight: Width(4.5), marginLeft: Width(5) }} />
            <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: "center", marginBottom: Height(1), marginTop: Height(1), height: Height(4) }}>
              <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: "center" }}>
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={
                  () => this.gotoEditEvent(item)
                }><Image source={Edit} />
                  <Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1.5), fontSize: FontSize(16) }}>Edit</Text>
                </TouchableOpacity>
                <View style={{ width: Width(8) }} />
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ plan_id: item.plan_id })
                    if (item.plan_type == 1) {
                      //this.eventtitle = name[0]+", let's meet up!"
                      this.eventtitle = "Let's meet up!"
                    }
                    else if (item.plan_type == 2) {
                      this.eventtitle = item.activity
                    }
                    else if (item.plan_type == 3) {
                      this.eventtitle = "Wanna go to " + item.line_1 + "?"
                    }
                    else if (item.plan_type == 4) {
                      this.eventtitle = "I'm free, wanna hangout?"
                    }
                    Alert.alert(
                      'Cancel Event',
                      'Guests will be notified that this event was canceled',
                      [
                        {
                          text: 'Cancel',
                          onPress: () => console.log('Cancel Pressed'),
                          style: 'cancel',
                        },
                        { text: 'OK', onPress: this.deleteplan },
                      ],
                      { cancelable: false },
                    );
                  }}
                  style={{ flexDirection: 'row', alignItems: 'center' }} ><Image source={Delete} />
                  <Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1.5), fontSize: FontSize(16) }}>Cancel</Text></TouchableOpacity>
                <View style={{ width: Width(8) }} />
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => this.goToShare(item)}><Image source={share} />
                  <Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1.5), fontSize: FontSize(16) }}>Share</Text></TouchableOpacity>
              </View>
            </View>
          </View>
        </View> : null}
      {item.plan_type == 2 ?
        <View style={{ backgroundColor: colors.white }}>
          <View style={{ height: Height(9), flexDirection: 'row', alignItems: 'center', backgroundColor: 'white' }}>
            <View style={{ alignItems: 'center', marginLeft: Width(3) }}>
              <View underlayColor="transparent" style={{ height: Height(5), width: Height(5), borderRadius: Height(7), overflow: 'hidden' }}>
                {item.photo == '' ? <Image source={avtar} style={{ height: '100%', width: '100%' }} /> :
                  <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.photo }} />}
              </View>
            </View>
            <View style={{ justifyContent: 'center', marginLeft: Width(3) }}>
              <Text style={{ fontSize: FontSize(18), color: colors.dargrey, fontFamily: fonts.Raleway_Bold }}>{item.login_id == this.state.login_id ? 'Me' : name[0]}</Text>
              <Text style={{ fontSize: FontSize(12), color: colors.fontDarkGrey }}>{dateTime}{item.share_with == '' ? null : '\u00B7 '}{item.share_with}</Text>
            </View>
            <View style={{ alignItems: 'flex-end', marginRight: Width(4), flex: 4 }}>

              <TouchableOpacity onPress={() => this.modelVisible(item)} underlayColor="transparent" style={{ justifyContent: 'center', height: Height(6), width: Width(6) }}>
                <Entypo size={Width(6)} name="dots-three-horizontal" color="#000" />
              </TouchableOpacity>
            </View>
          </View>
          <TouchableOpacity onPress={() => {
            this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: item.activity, Final_date: Final_date, eventby: item.name })
          }}>
            <ImageBackground source={item.plan_image == '' || item.plan_image == null || item.plan_image == undefined ? default_placeholder : { uri: item.plan_image }} style={{ backgroundColor: '#F7F7F7', height: 265, justifyContent: 'center', alignItems: 'center' }}>
              {/* <Text style={{ fontSize: FontSize(20), color: colors.dargrey, fontFamily: fonts.Raleway_Semibold,marginHorizontal:Width(5), lineHeight:27,textAlign:'center'}}>{item.activity} </Text>
            {item.description == '' ? null : <Text style={{ textAlign: 'center', fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, width: '85%', marginTop: 2 }}>{item.description}</Text>} */}
            </ImageBackground>
          </TouchableOpacity>
          <View style={{ paddingHorizontal: Width(4.5) }}>
            <Text style={{ fontSize: FontSize(19), color: colors.dargrey, fontFamily: fonts.Raleway_Bold, lineHeight: 27, textAlign: 'left', marginTop: 10 }}>{item.activity}</Text>
            {/* {item.description == '' ? null : <Text style={{ textAlign: 'left', fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, marginTop: 4,marginHorizontal:Width(4.5)}}>{item.description}</Text>} */}
            {item.description == '' ? null : <SeeMore style={{ textAlign: 'left', fontSize: FontSize(13), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, marginTop: 4 }} numberOfLines={2} seeMoreText='more'>{item.description}</SeeMore>}
          </View>
          <View style={{ backgroundColor: '#FFF', alignItems: 'center', flexDirection: 'row' }}>

            <View style={{ flex: 1, marginLeft: Height(2.5) }}>

              <TouchableOpacity
                style={{ flexDirection: 'row', marginTop: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center', tintColor: colors.whatFontColor }} source={chai_cup} />
                {item.activity == '' ? <Text
                  onPress={item.total_activity_suggestions == 0 ? () => this.props.navigation.navigate('Activity', { plan_id: item.plan_id, is_direct: true, title_header: item.activity }) : () => this.props.navigation.navigate('SuggestActivity', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false, title_header: item.activity })}
                  style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}></Text>
                  : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{activity}</Text>}
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginTop: Height(2), marginBottom: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={calendar} />
                {item.start_date == '' ? <Text
                  onPress={item.total_datetime_suggestions == 0 ? () => this.props.navigation.navigate('DateTime', { plan_id: item.plan_id, is_direct: true }) : () => this.props.navigation.navigate('SuggestDateTime', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false })}
                  style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>{item.total_datetime_suggestions == 0 ? 'Suggest a date & time' : 'View date & time suggestions'}</Text>
                  : <Text style={{ width: Width(60), paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{Final_date}</Text>}
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginBottom: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center', tintColor: colors.whatFontColor }} source={location} />
                {item.line_1 == '' ? <Text
                  onPress={item.total_location_suggestions == 0 ? () => this.props.navigation.navigate('Location', { plan_id: item.plan_id, is_direct: true }) : () => this.props.navigation.navigate('SuggestLocation', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false })}
                  style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>{item.total_location_suggestions == 0 ? 'Suggest a location' : 'View location suggestions'}</Text>
                  : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.line_1}</Text>}
              </TouchableOpacity>

              <View style={{ height: Height(0.15), backgroundColor: '#EFEFF4', marginRight: Width(4.5) }} />

              {item.is_going_count != 0 || item.is_interested_count != 0 || item.is_invited_count != 0 ?
                <View
                  style={{ flexDirection: 'row', marginBottom: Height(1) }}>
                  {/* {item.is_going_count != 0 || item.is_interested_count != 0 || item.is_invited_count != 0 ?
                  <Image resizeMode="contain" style={{ marginTop: Height(2), alignSelf: 'center', tintColor: colors.whatFontColor }} source={new_user} /> : null} */}
                  {item.is_going_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 0, Title_New: item.activity, plan_type: item.plan_type }) }} style={{ marginTop: Height(1.5), paddingLeft: Width(0), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey }}>{item.is_going_count + ' going'}</Text> : null}
                  {item.is_interested_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 1, Title_New: item.activity, plan_type: item.plan_type }) }} style={item.is_going_count == 0 ? { marginTop: Height(1.5), paddingLeft: Width(0), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey } : { marginTop: Height(1.5), paddingLeft: Width(1), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey }}>{item.is_going_count != 0 ? '\u00B7 ' : null}{item.is_interested_count + ' interested'}</Text> : null}
                  {item.is_invited_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 2, Title_New: item.activity, plan_type: item.plan_type }) }} style={item.is_going_count == 0 && item.is_interested_count == 0 ? { marginTop: Height(1.5), paddingLeft: Width(0), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey } : { marginTop: Height(1.5), paddingLeft: Width(1), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey }}>{item.is_interested_count != 0 || item.is_going_count != 0 ? '\u00B7 ' : null}{item.is_invited_count + ' invited'}</Text> : null}
                  <TouchableOpacity style={{ marginTop: Height(1.5), position: 'absolute', right: Width(4), width: 35, height: 35 }} onPress={() => {
                    this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: item.activity, Final_date: Final_date, eventby: item.name })
                  }}>
                    <Image resizeMode="contain" source={ic_chat} />
                  </TouchableOpacity>
                </View>
                :
                <View>
                  {item.login_id != this.state.login_id ?
                    null
                    :
                    <Text style={{ marginTop: Height(1.5), marginBottom: Height(1), paddingLeft: Width(0), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey }}>No guests yet</Text>
                  }

                  <TouchableOpacity style={{ marginTop: Height(1.5), marginBottom: Height(1.5), position: 'absolute', right: Width(4), width: 35, height: 35 }} onPress={() => {
                    this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: item.activity, Final_date: Final_date, eventby: item.name })
                  }}>
                    <Image resizeMode="contain" source={ic_chat} />
                  </TouchableOpacity>

                </View>
              }

            </View>

          </View>
          <View style={{ height: Height(0.5) }} />
          <View
            style={{ backgroundColor: 'white' }}>
            <View style={{ height: Height(0.15), backgroundColor: '#EFEFF4', marginRight: Width(4.5), marginLeft: Width(5) }} />
            <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: "center", marginBottom: Height(1), marginTop: Height(1), height: Height(4) }}>
              <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: "center" }}>
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={
                  () => this.gotoEditEvent(item)
                }><Image source={Edit} />
                  <Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1.5), fontSize: FontSize(16) }}>Edit</Text>
                </TouchableOpacity>
                <View style={{ width: Width(8) }} />
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ plan_id: item.plan_id })
                    if (item.plan_type == 1) {
                      //this.eventtitle = name[0]+", let's meet up!"
                      this.eventtitle = "Let's meet up!"
                    }
                    else if (item.plan_type == 2) {
                      this.eventtitle = item.activity
                    }
                    else if (item.plan_type == 3) {
                      this.eventtitle = "Wanna go to " + item.line_1 + "?"
                    }
                    else if (item.plan_type == 4) {
                      this.eventtitle = "I'm free, wanna hangout?"
                    }
                    Alert.alert(
                      'Cancel Event',
                      'Guests will be notified that this event was canceled',
                      [
                        {
                          text: 'Cancel',
                          onPress: () => console.log('Cancel Pressed'),
                          style: 'cancel',
                        },
                        { text: 'OK', onPress: this.deleteplan },
                      ],
                      { cancelable: false },
                    );
                  }}
                  style={{ flexDirection: 'row', alignItems: 'center' }} ><Image source={Delete} />
                  <Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1.5), fontSize: FontSize(16) }}>Cancel</Text></TouchableOpacity>
                <View style={{ width: Width(8) }} />
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => this.goToShare(item)}><Image source={share} />
                  <Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1.5), fontSize: FontSize(16) }}>Share</Text></TouchableOpacity>
              </View>
            </View>
          </View>
        </View> : null}
      {item.plan_type == 3 ?
        <View style={{ backgroundColor: colors.white }}>
          <View style={{ height: Height(9), flexDirection: 'row', alignItems: 'center', backgroundColor: 'white' }}>
            <View style={{ alignItems: 'center', marginLeft: Width(3) }}>
              <View
                underlayColor="transparent" style={{ height: Height(5), width: Height(5), borderRadius: Height(7), overflow: 'hidden' }}>
                {item.photo == '' ? <Image source={avtar} style={{ height: '100%', width: '100%' }} /> :
                  <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.photo }} />}
              </View>
            </View>
            <View style={{ justifyContent: 'center', marginLeft: Width(3) }}>
              <Text style={{ fontSize: FontSize(18), color: colors.dargrey, fontFamily: fonts.Raleway_Bold }}>{item.login_id == this.state.login_id ? 'Me' : item.name}</Text>
              <Text style={{ fontSize: FontSize(12), color: colors.fontDarkGrey, fontFamily: fonts.Roboto_Regular }}>{dateTime}{item.share_with == '' ? null : '\u00B7 '}{item.share_with}</Text>
            </View>
            <View style={{ alignItems: 'flex-end', marginRight: Width(4), flex: 4 }}>
              <TouchableOpacity onPress={() => this.modelVisible(item)} underlayColor="transparent" style={{ justifyContent: 'center', height: Height(6), width: Width(6) }}>
                <Entypo size={Width(6)} name="dots-three-horizontal" color="#000" />
              </TouchableOpacity>
            </View>
          </View>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "Wanna go to " + item.line_1 + "?", Final_date: Final_date, eventby: item.name }) }}>
            <ImageBackground source={item.plan_image == '' || item.plan_image == null || item.plan_image == undefined ? default_placeholder : { uri: item.plan_image }} style={{ backgroundColor: '#F7F7F7', height: 265, justifyContent: 'center', alignItems: 'center' }}>
              {/* <Text style={{ textAlign: 'center', fontSize: FontSize(20), color: colors.dargrey, fontFamily: fonts.Raleway_Semibold,marginHorizontal:Width(5), lineHeight:27,textAlign:'center'}}>Wanna go to {item.line_1} ?</Text>
            {item.description == '' ? null : <Text style={{ textAlign: 'center', fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, width: '85%', marginTop: 2 }}>{item.description}</Text>} */}

            </ImageBackground>
          </TouchableOpacity>
          <View style={{ paddingHorizontal: Width(4.5) }}>
            <Text style={{ fontSize: FontSize(19), color: colors.dargrey, fontFamily: fonts.Raleway_Bold, lineHeight: 27, textAlign: 'left', marginTop: 10 }}>Wanna go to {item.line_1} ?</Text>
            {/* {item.description == '' ? null : <Text style={{ textAlign: 'left', fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, marginTop: 4,marginHorizontal:Width(4.5)}}>{item.description}</Text>} */}
            {item.description == '' ? null : <SeeMore style={{ textAlign: 'left', fontSize: FontSize(13), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, marginTop: 4 }} numberOfLines={2} seeMoreText='more'>{item.description}</SeeMore>}
          </View>
          <View style={{ backgroundColor: '#FFF', alignItems: 'center', flexDirection: 'row' }}>

            <View style={{ flex: 1, marginLeft: Height(2.5) }}>

              <TouchableOpacity
                style={{ flexDirection: 'row', marginTop: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center', tintColor: colors.whatFontColor }} source={chai_cup} />
                {item.activity == '' ? <Text
                  onPress={item.total_activity_suggestions == 0 ? () => this.props.navigation.navigate('Activity', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true, title_header: "Wanna go to " + item.line_1 + "?" }) : () => this.props.navigation.navigate('SuggestActivity', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false, title_header: "Wanna go to " + item.line_1 + "?" })}
                  style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>{item.total_activity_suggestions == 0 ? 'Suggest an activity' : 'View activity suggestions'}</Text>
                  : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.activity}</Text>}
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginTop: Height(2), marginBottom: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={calendar} />
                {item.start_date == '' ? <Text
                  onPress={item.total_datetime_suggestions == 0 ? () => this.props.navigation.navigate('DateTime', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true }) : () => this.props.navigation.navigate('SuggestDateTime', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false })}
                  style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>{item.total_datetime_suggestions == 0 ? 'Suggest a date & time' : 'View date & time suggestions'}</Text>
                  : <Text style={{ width: Width(60), paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{Final_date}</Text>}
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginBottom: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center', tintColor: colors.whatFontColor }} source={location} />
                {item.line_1 == '' ? <Text
                  onPress={item.total_location_suggestions == 0 ? () => this.props.navigation.navigate('Location', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true }) : () => this.props.navigation.navigate('SuggestLocation', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false })}
                  style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>{item.total_location_suggestions == 0 ? 'Suggest a location' : 'View location suggestions'}</Text>
                  : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.line_1}</Text>}
              </TouchableOpacity>

              <View style={{ height: Height(0.15), backgroundColor: '#EFEFF4', marginRight: Width(4.5) }} />

              {item.is_going_count != 0 || item.is_interested_count != 0 || item.is_invited_count != 0 ?
                <View
                  style={{ flexDirection: 'row', marginBottom: Height(1) }}>
                  {/* {item.is_going_count != 0 || item.is_interested_count != 0 || item.is_invited_count != 0 ?
                  <Image resizeMode="contain" style={{ marginTop: Height(2), alignSelf: 'center', tintColor: colors.whatFontColor }} source={new_user} /> : null} */}
                  {item.is_going_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 0, Title_New: item.line_1, plan_type: item.plan_type }) }} style={{ marginTop: Height(1.5), paddingLeft: Width(0), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey }}>{item.is_going_count + ' going'}</Text> : null}
                  {item.is_interested_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 1, Title_New: item.line_1, plan_type: item.plan_type }) }} style={item.is_going_count == 0 ? { marginTop: Height(1.5), paddingLeft: Width(0), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey } : { marginTop: Height(1.5), paddingLeft: Width(1), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey }}>{item.is_going_count != 0 ? '\u00B7 ' : null}{item.is_interested_count + ' interested'}</Text> : null}
                  {item.is_invited_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 2, Title_New: item.line_1, plan_type: item.plan_type }) }} style={item.is_going_count == 0 && item.is_interested_count == 0 ? { marginTop: Height(1.5), paddingLeft: Width(0), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey } : { marginTop: Height(1.5), paddingLeft: Width(1), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey }}>{item.is_interested_count != 0 || item.is_going_count != 0 ? '\u00B7 ' : null}{item.is_invited_count + ' invited'}</Text> : null}
                  <TouchableOpacity style={{ marginTop: Height(1.5), position: 'absolute', right: Width(4), width: 35, height: 35 }} onPress={() => { this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "Wanna go to " + item.line_1 + "?", Final_date: Final_date, eventby: item.name }) }}>
                    <Image resizeMode="contain" source={ic_chat} />
                  </TouchableOpacity>
                </View>
                :
                <View>
                  {item.login_id != this.state.login_id ?
                    null
                    :
                    <Text style={{ marginTop: Height(1.5), marginBottom: Height(1), paddingLeft: Width(0), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey }}>No guests yet</Text>
                  }
                  <TouchableOpacity style={{ marginTop: Height(1.5), marginBottom: Height(1.5), position: 'absolute', right: Width(4), width: 35, height: 35 }} onPress={() => { this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "Wanna go to " + item.line_1 + "?", Final_date: Final_date, eventby: item.name }) }}>
                    <Image resizeMode="contain" source={ic_chat} />
                  </TouchableOpacity>
                </View>
              }

            </View>

          </View>
          <View style={{ height: Height(0.5) }} />
          <View
            style={{ backgroundColor: 'white' }}>
            <View style={{ height: Height(0.15), backgroundColor: '#EFEFF4', marginRight: Width(4.5), marginLeft: Width(5) }} />
            <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: "center", marginBottom: Height(1), marginTop: Height(1), height: Height(4) }}>
              <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: "center" }}>
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={
                  () => this.gotoEditEvent(item)
                }><Image source={Edit} />
                  <Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1.5), fontSize: FontSize(16) }}>Edit</Text>
                </TouchableOpacity>
                <View style={{ width: Width(8) }} />
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ plan_id: item.plan_id })
                    if (item.plan_type == 1) {
                      //this.eventtitle = name[0]+", let's meet up!"
                      this.eventtitle = "Let's meet up!"
                    }
                    else if (item.plan_type == 2) {
                      this.eventtitle = item.activity
                    }
                    else if (item.plan_type == 3) {
                      this.eventtitle = "Wanna go to " + item.line_1 + "?"
                    }
                    else if (item.plan_type == 4) {
                      this.eventtitle = "I'm free, wanna hangout?"
                    }
                    Alert.alert(
                      'Cancel Event',
                      'Guests will be notified that this event was canceled',
                      [
                        {
                          text: 'Cancel',
                          onPress: () => console.log('Cancel Pressed'),
                          style: 'cancel',
                        },
                        { text: 'OK', onPress: this.deleteplan },
                      ],
                      { cancelable: false },
                    );
                  }}
                  style={{ flexDirection: 'row', alignItems: 'center' }} ><Image source={Delete} />
                  <Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1.5), fontSize: FontSize(16) }}>Cancel</Text></TouchableOpacity>
                <View style={{ width: Width(8) }} />
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => this.goToShare(item)}><Image source={share} />
                  <Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1.5), fontSize: FontSize(16) }}>Share</Text></TouchableOpacity>
              </View>
            </View>
          </View>
        </View> : null}
      {item.plan_type == 4 ?
        <View style={{ backgroundColor: colors.white }}>
          <View style={{ height: Height(9), flexDirection: 'row', alignItems: 'center', backgroundColor: 'white' }}>
            <View style={{ alignItems: 'center', marginLeft: Width(3) }}>
              <View underlayColor="transparent" style={{ height: Height(5), width: Height(5), borderRadius: Height(7), overflow: 'hidden' }}>
                {item.photo == '' ? <Image source={avtar} style={{ height: '100%', width: '100%' }} /> :
                  <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.photo }} />}
              </View>
            </View>
            <View style={{ justifyContent: 'center', marginLeft: Width(3) }}>
              <Text style={{ fontSize: FontSize(18), color: colors.dargrey, fontFamily: fonts.Raleway_Bold }}>{item.login_id == this.state.login_id ? 'Me' : item.name}</Text>
              <Text style={{ fontSize: FontSize(12), color: colors.fontDarkGrey }}>{dateTime}{item.share_with == '' ? null : '\u00B7 '}{item.share_with}</Text>
            </View>
            <View style={{ alignItems: 'flex-end', marginRight: Width(4), flex: 4 }}>
              <TouchableOpacity onPress={() => this.modelVisible(item)} underlayColor="transparent" style={{ justifyContent: 'center', height: Height(6), width: Width(6) }}>
                <Entypo size={Width(6)} name="dots-three-horizontal" color="#000" />
              </TouchableOpacity>
            </View>
          </View>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "I'm free, wanna hangout?", Final_date: Final_date, eventby: item.name }) }}>
            <ImageBackground source={item.plan_image == '' || item.plan_image == null || item.plan_image == undefined ? default_placeholder : { uri: item.plan_image }} style={{ backgroundColor: '#F7F7F7', height: 265, justifyContent: 'center', alignItems: 'center' }}>
              {/* <Text style={{ fontSize: FontSize(20), color: colors.dargrey, fontFamily: fonts.Raleway_Semibold,marginHorizontal:Width(5), lineHeight:27,textAlign:'center' }}>I'm free, wanna hangout?</Text>
            {item.description == '' ? null : <Text style={{ textAlign: 'center', fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, width: '85%', marginTop: 2 }}>{item.description}</Text>} */}

            </ImageBackground>
          </TouchableOpacity>
          <View style={{ paddingHorizontal: Width(4.5) }}>
            <Text style={{ fontSize: FontSize(19), color: colors.dargrey, fontFamily: fonts.Raleway_Bold, lineHeight: 27, textAlign: 'left', marginTop: 10 }}>I'm free, wanna hangout?</Text>
            {/* {item.description == '' ? null : <Text style={{ textAlign: 'left', fontSize: FontSize(13), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, marginTop: 4,marginHorizontal:Width(4.5)}}>{item.description}</Text>} */}
            {item.description == '' ? null : <SeeMore style={{ textAlign: 'left', fontSize: FontSize(13), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, marginTop: 4 }} numberOfLines={2} seeMoreText='more'>{item.description}</SeeMore>}
          </View>
          <View style={{ backgroundColor: '#FFF', alignItems: 'center', flexDirection: 'row' }}>

            <View style={{ flex: 1, marginLeft: Height(2.5) }}>

              <TouchableOpacity
                style={{ flexDirection: 'row', marginTop: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={chai_cup} />
                {item.activity == '' ? <Text
                  onPress={item.total_activity_suggestions == 0 ? () => this.props.navigation.navigate('Activity', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true, title_header: "I'm free, wanna hangout?" }) : () => this.props.navigation.navigate('SuggestActivity', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false, title_header: "I'm free, wanna hangout?" })}
                  style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>{item.total_activity_suggestions == 0 ? 'Suggest an activity' : 'View activity suggestions'}</Text>
                  : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.activity}</Text>}
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginTop: Height(2), marginBottom: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={calendar} />
                {item.start_date == '' ? <Text
                  onPress={item.total_datetime_suggestions == 0 ? () => this.props.navigation.navigate('DateTime', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true }) : () => this.props.navigation.navigate('SuggestDateTime', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false })}
                  style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>{item.total_datetime_suggestions == 0 ? 'Suggest a date & time' : 'View date & time suggestions'}</Text>
                  : <Text style={{ width: Width(60), paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{Final_date}</Text>}
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginBottom: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center', tintColor: colors.whatFontColor }} source={location} />
                {item.line_1 == '' ? <Text
                  onPress={item.total_location_suggestions == 0 ? () => this.props.navigation.navigate('Location', { plan_id: item.plan_id, other_id: item.login_id, is_direct: true }) : () => this.props.navigation.navigate('SuggestLocation', { other_id: item.login_id, plan_id: item.plan_id, is_direct: false })}
                  style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>{item.total_location_suggestions == 0 ? 'Suggest a location' : 'View location suggestions'}</Text>
                  : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.line_1}</Text>}
              </TouchableOpacity>

              <View style={{ height: Height(0.15), backgroundColor: '#EFEFF4', marginRight: Width(4.5) }} />


              {item.is_going_count != 0 || item.is_interested_count != 0 || item.is_invited_count != 0 ?
                <View
                  style={{ flexDirection: 'row', marginBottom: Height(1) }}>
                  {/* {item.is_going_count != 0 || item.is_interested_count != 0 || item.is_invited_count != 0 ?
                  <Image resizeMode="contain" style={{ marginTop: Height(2), alignSelf: 'center', tintColor: colors.whatFontColor }} source={new_user} /> : null} */}
                  {item.is_going_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 0, Title_New: item.activity, plan_type: item.plan_type }) }} style={{ marginTop: Height(1.5), paddingLeft: Width(0), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey }}>{item.is_going_count + ' going'}</Text> : null}
                  {item.is_interested_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 1, Title_New: item.activity, plan_type: item.plan_type }) }} style={item.is_going_count == 0 ? { marginTop: Height(1.5), paddingLeft: Width(0), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey } : { marginTop: Height(1.5), paddingLeft: Width(1), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey }}>{item.is_going_count != 0 ? '\u00B7 ' : null}{item.is_interested_count + ' interested'}</Text> : null}
                  {item.is_invited_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 2, Title_New: item.activity, plan_type: item.plan_type }) }} style={item.is_going_count == 0 && item.is_interested_count == 0 ? { marginTop: Height(1.5), paddingLeft: Width(0), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey } : { marginTop: Height(1.5), paddingLeft: Width(1), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey }}>{item.is_interested_count != 0 || item.is_going_count != 0 ? '\u00B7 ' : null}{item.is_invited_count + ' invited'}</Text> : null}
                  <TouchableOpacity style={{ marginTop: Height(1.5), position: 'absolute', right: Width(4), width: 35, height: 35 }} onPress={() => { this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "I'm free, wanna hangout?", Final_date: Final_date, eventby: item.name }) }}>
                    <Image resizeMode="contain" source={ic_chat} />
                  </TouchableOpacity>
                </View>
                :
                <View>
                  {item.login_id != this.state.login_id ?
                    null
                    :
                    <Text style={{ marginTop: Height(1.5), marginBottom: Height(1), paddingLeft: Width(0), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(12), color: colors.dargrey }}>No guests yet</Text>
                  }
                  <TouchableOpacity style={{ marginTop: Height(1.5), marginBottom: Height(1.5), position: 'absolute', right: Width(4), width: 35, height: 35 }} onPress={() => { this.props.navigation.navigate('EventDetail', { item: item, plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header: "I'm free, wanna hangout?", Final_date: Final_date, eventby: item.name }) }}>
                    <Image resizeMode="contain" source={ic_chat} />
                  </TouchableOpacity>
                </View>
              }

            </View>

          </View>
          <View style={{ height: Height(0.5) }} />
          <View
            style={{ backgroundColor: 'white' }}>
            <View style={{ height: Height(0.15), backgroundColor: '#EFEFF4', marginRight: Width(4.5), marginLeft: Width(5) }} />
            <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: "center", marginBottom: Height(1), marginTop: Height(1), height: Height(4) }}>
              <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: "center" }}>
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={
                  () => this.gotoEditEvent(item)
                }><Image source={Edit} />
                  <Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1.5), fontSize: FontSize(16) }}>Edit</Text>
                </TouchableOpacity>
                <View style={{ width: Width(8) }} />
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ plan_id: item.plan_id })
                    if (item.plan_type == 1) {
                      //this.eventtitle = name[0]+", let's meet up!"
                      this.eventtitle = "Let's meet up!"
                    }
                    else if (item.plan_type == 2) {
                      this.eventtitle = item.activity
                    }
                    else if (item.plan_type == 3) {
                      this.eventtitle = "Wanna go to " + item.line_1 + "?"
                    }
                    else if (item.plan_type == 4) {
                      this.eventtitle = "I'm free, wanna hangout?"
                    }
                    Alert.alert(
                      'Cancel Event',
                      'Guests will be notified that this event was canceled',
                      [
                        {
                          text: 'Cancel',
                          onPress: () => console.log('Cancel Pressed'),
                          style: 'cancel',
                        },
                        { text: 'OK', onPress: this.deleteplan },
                      ],
                      { cancelable: false },
                    );
                  }}
                  style={{ flexDirection: 'row', alignItems: 'center' }} ><Image source={Delete} />
                  <Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1.5), fontSize: FontSize(16) }}>Cancel</Text></TouchableOpacity>
                <View style={{ width: Width(8) }} />
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => this.goToShare(item)}><Image source={share} />
                  <Text style={{ fontFamily: fonts.Raleway_Regular, color: colors.dargrey, marginLeft: Width(1.5), fontSize: FontSize(16) }}>Share</Text></TouchableOpacity>
              </View>
            </View>
          </View>
        </View> : null}

    </View>
    )
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 5,
          width: '100%',
          backgroundColor: colors.lightgrey,
        }}
      />
    );
  };

  onRefresh = () => {

  }

  onClickSelectPhoto = () => {
    //this.props.navigation.navigate('ProfilePicView',{UserDetail: this.state.UserDetail,is_my_photo:true})
    this.setState({ isProfilePicVisible: true })
  }

  onEditProfilePic = () => {
    var options = {
      title: '',

      allowsEditing: true,
      storageOptions: {
        skipBackup: true,
        path: 'images'
      },
      maxWidth: 500,
      maxHeight: 500
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        this.is_photo_selected = false
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        this.is_photo_selected = false
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };
        this.is_photo_selected = true
        this.setState({
          profile: response.uri,
        })
      }
    });
  }

  titleCase(str) {

    if (str == undefined || str == null || str == '') {
      return
    }

    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
      splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(' ');
  }

  onSaveProfilePic = () => {

    var username = this.titleCase(this.state.UserDetail.name)

    RNProgressHUB.showSpinIndeterminate();
    const createFormData = (photo_path, body) => {
      const data = new FormData();
      data.append("profile_pic", {
        name: 'selfie.jpg',
        type: 'image/jpg',
        uri:
          Platform.OS === "android" ? this.state.profile : this.state.profile.replace("file://", "")
      });

      Object.keys(body).forEach(key => {
        data.append(key, body[key]);
      });

      return data;
    };

    fetch(API_ROOT + 'update/profile', {
      method: 'post',
      body: createFormData(this.state.profile, {
        login_id: this.state.login_id,
        name: username.trim(),
        bio: this.state.UserDetail.bio == null ? '' : this.state.UserDetail.bio,
        country_code: '',
        city: this.state.UserDetail.city,
        dob: this.state.UserDetail.dob,
        mobile_number: ''
      })
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          RNProgressHUB.dismiss();
          store.dispatch({ type: UPDATE_USER, data: responseData.data })
          this.is_photo_selected = false
          this.setState({ isProfilePicVisible: false })
        } else {
          RNProgressHUB.dismiss();

        }
      })

      .catch((error) => {
        //alert(error)
        RNProgressHUB.dismiss();
        // this.props.navigation.goBack()

      })
  }

  //Edit Profile Start

  onEditProfilePressed() {
    //this.props.navigation.navigate('editprofile', { UserDetail: this.state.UserDetail })
    const MixpanelData = {
      '$email': store.getState().auth.user.email_address
    }
    this.mixpanel.identify(store.getState().auth.user.email_address);
    this.mixpanel.track('Edit Profile Button Click');
    if (global.isConnected == false) {
      return
    }

    this.setState({
      name1: this.state.UserDetail.name,
      photo1: this.state.UserDetail.photo,
      bio1: this.state.UserDetail.bio,
      city1: this.state.UserDetail.city,
      DOB1: this.state.UserDetail.dob,
    },
      () => {
        console.log("ssss", this.state.UserDetail.dob)
        //console.log('DOB after adding day: '+moment(this.state.DOB).add('1','day').toDate())

        // if(Platform.OS == 'ios')
        // {

        if (this.state.UserDetail.dob == undefined || this.state.UserDetail.dob == null || this.state.UserDetail.dob.length == 0) {
          return
        }

        var date = new Date();
        this.offsetInHours = date.getTimezoneOffset() / 60;
        var bcomps = this.state.UserDetail.dob.split('-')
        if (bcomps.length > 1) {
          this.bmonth = bcomps[1]
        }

        // if(this.offsetInHours == 4)
        // {              
        //     console.log("ifff")
        //     let curDob = moment(this.state.DOB1).format('YYYY-MM-DD')
        //     let updatedDOB = curDob.setDate(curDob.getDate() + 1)
        //     console.log('Updated Bdate :'+updatedDOB)
        //     this.setState({bdayforpicker:updatedDOB,DOB1:updatedDOB})
        // }
        // else
        // {
        //   console.log("elsesss")
        //   let curDob = moment(this.state.DOB1).format('YYYY-MM-DD')
        //   console.log("curDob",curDob)
        //   console.log("zzz",moment(this.state.DOB1).format('YYYY-MM-DDTHH:mm:ss[Z]'))
        //   //let curDob1 = moment(this.state.DOB1).add('day',1).format('YYYY-MM-DD')
        //   this.setState({bdayforpicker:this.state.DOB1,DOB1:curDob})
        // }
        //alert(this.offsetInHours)
        if (this.offsetInHours == 4 || this.offsetInHours == 5 || this.offsetInHours == 3) {
          let curDob = new Date(this.state.DOB1)
          let updatedDOB = curDob.setDate(curDob.getDate() + 1)
          //alert('Updated Bdate :'+updatedDOB)
          //this.setState({bdayforpicker:updatedDOB,DOB1:moment(this.state.DOB1).format('YYYY-MM-DD')})
          this.setState({ bdayforpicker: updatedDOB, DOB1: updatedDOB })
        }
        else {
          let curDob = new Date(this.state.DOB1)
          //alert("curDob: "+curDob)
          //this.setState({bdayforpicker:curDob,DOB1:moment(this.state.DOB1).format('YYYY-MM-DD')})
          this.setState({ bdayforpicker: curDob, DOB1: curDob })
        }

        // }
      }
    )

    this.setState({ isEditProfileVisible: true })
  }

  titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
      splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(' ');
  }

  editProfile = () => {

    var username = this.titleCase(this.state.name1)

    RNProgressHUB.showSpinIndeterminate();
    var data = new FormData();
    data.append("profile_pic", {
      name: 'selfie.jpg',
      type: 'image/jpg',
      uri:
        Platform.OS === "android" ? this.state.photo1 : this.state.photo1.replace("file://", "")
    });
    data.append('login_id', this.state.login_id)
    data.append('name', username.trim())
    data.append('bio', this.state.bio1)
    data.append('country_code', '')
    data.append('city', this.state.city1)
    data.append('dob', this.state.DOBPass1)
    data.append('mobile_number', '')

    //console.log('PROFILE_DATA: '+JSON.stringify(data))

    fetch(API_ROOT + 'update/profile', {
      method: 'post',
      body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          console.log('API RESPONSE ===>', responseData.data)
          RNProgressHUB.dismiss();
          store.dispatch({ type: UPDATE_USER, data: responseData.data })
          this.getUserData()
          this.mixpanel.identify(store.getState().auth.user.email_address);

          this.mixpanel.identify(store.getState().auth.user.email_address);
          this.mixpanel.getPeople().set('Brithday', this.state.DOBPass1);
          this.mixpanel.getPeople().set('$name', username.trim());
          this.mixpanel.getPeople().set('Bio', this.state.bio1);
          this.mixpanel.getPeople().set('$avatar',  responseData.data.photo);
          this.mixpanel.track('Update Profile');
          this.setState({ isEditProfileVisible: false })
        } else {
          RNProgressHUB.dismiss();
          //this.props.navigation.goBack()
        }
      })

      .catch((error) => {
        //alert(error)
        RNProgressHUB.dismiss();
        // this.props.navigation.goBack()

      })

  }

  renderEditProfileTopBar() {
    return (
      <Header style={[styles.headerAndroidnav, { height: 44 }]}>
        <StatusBar barStyle="light-content" backgroundColor="#41199B" />
        <View style={{ bottom: 10, position: 'absolute', left: Width(5), width: 35, height: 35 }}>
          <TouchableOpacity onPress={() => { this.setState({ isEditProfileVisible: false }) }} style={{ width: 35, height: 35, justifyContent: 'center' }}>
            <Image source={closeWhite} style={{ tintColor: "white" }} />
          </TouchableOpacity>
        </View>
        <View style={{ bottom: 10, position: 'absolute', alignItems: 'center' }}>
          <Text style={styles.headerTitle} >Edit Profile</Text>
        </View>
      </Header>
    )
  }

  _showDateTimePicker = () => {
    if (Platform.OS == 'android') {
      if (this.state.isDateTimePickerVisible == true) {
        this.setState({ isDateTimePickerVisible: false });
      }
      else {
        this.setState({ isDateTimePickerVisible: true });
      }
    }
    else {
      this.setState({ isDateTimePickerVisible: true });
    }

  }

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (date) => {

    console.log('Selected Date: ' + date)

    if (date != undefined) {
      //console.log('A date has been picked: ', dateFormat(date, "dd/mm/yyyy"));

      this.setState({ bdayforpicker: date, DOB1: dateFormat(date, "dd-mmm-yyyy"), DOBPass1: dateFormat(date, "yyyy-mm-dd") }, () => { console.log('Selected DOB: ' + this.state.DOB1) })

      //this.setState({ DOB: moment(date).format(), DOBPass: dateFormat(date, "yyyy-mm-dd") })
      setTimeout(function () {
        subThis.Datevalidtion()

      }, 100);
    }

    this._hideDateTimePicker();
  };

  Datevalidtion = () => {

    var d1 = new Date()
    var d2 = new Date(this.state.DOB1)

    var g1 = d1.getTime()
    var g2 = d2.getTime()

    // if (this.state.DOB.length < 1) {
    //   this.setState({ DOBValidation: false, DOBValidationMSG: "Select BirthDay.", labelStyleDOB: [styles.labelInput, styles.labelInputError], })
    //   return false
    // }

    if (this.compare_dates(g1, g2) == 2 || this.compare_dates(g1, g2) == 0) {
      this.setState({ DOBValidation: false, DOBValidationMSG: "Invalid Birthday", labelStyleDOB: [styles.labelInput, styles.labelInputError], })
      return false
    }
    else {
      this.setState({ DOBValidation: true })
    }
    this.defaultdate = new Date(this.state.DOBPass1)

    return true
  }

  compare_dates = function (date1, date2) {
    if (date2 - date1 > 0) {
      return 2
    }
    else if (date2 - date1 < 0) {
      return 1
    }
    else {
      return 0
    }
    //   if (date1>date2) return 1;
    // else if (date1<date2) return 2;
    // else return 0; 
  }

  onBlurbirthday() {
    if (this.state.password.length <= 0) {
      this.setState({
        labelStylebirthday: styles.labelInputUnFocus,
        stylebirthday: styles.formInputUnFocus
      })
      return false
    } else {
      return true
    }
  }
  onFocusbirthday() {
    this.setState({
      labelStylebirthday: styles.labelInput,
      stylebirthday: styles.formInputFocus
    })
  }
  onBlurBio() {

    this.setState({
      labelStyleBio: styles.labelInputUnFocus,
      styleBio: styles.formInputUnFocus

    })

  }
  onFocusBio() {
    this.setState({
      labelStyleBio: styles.labelInput,
      styleBio: styles.formInputFocus
    })
  }

  onBlurName() {

    this.setState({
      labelStyleName: styles.labelInputUnFocus,
      styleName: styles.formInputUnFocus

    })

  }
  onFocusName() {
    this.setState({
      labelStyleName: styles.labelInput,
      styleName: styles.formInputFocus
    })
  }

  onBlurCity() {

    this.setState({
      labelStyleCity: styles.labelInputUnFocus,
      styleCity: styles.formInputUnFocus

    })

  }
  onFocusCity() {
    this.setState({
      labelStyleCity: styles.labelInput,
      styleCity: styles.formInputFocus
    })
  }
  onClickSelectPhoto1 = () => {

    this.setState({ isProfilePicVisible1: true })
  }

  onEditProfilePic1 = () => {
    var options = {
      title: '',

      allowsEditing: true,
      storageOptions: {
        skipBackup: true,
        path: 'images'
      },
      maxWidth: 500,
      maxHeight: 500
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        this.is_photo_selected1 = false
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        this.is_photo_selected1 = false
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };
        this.is_photo_selected1 = true
        this.setState({
          photo1: response.uri,
        })
      }
    });
  }

  titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
      splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(' ');
  }

  onSaveProfilePic1 = () => {

    var username = this.titleCase(this.state.UserDetail.name)

    RNProgressHUB.showSpinIndeterminate();
    const createFormData = (photo_path, body) => {
      const data = new FormData();
      data.append("profile_pic", {
        name: 'selfie.jpg',
        type: 'image/jpg',
        uri:
          Platform.OS === "android" ? this.state.photo1 : this.state.photo1.replace("file://", "")
      });

      Object.keys(body).forEach(key => {
        data.append(key, body[key]);
      });

      return data;
    };

    fetch(API_ROOT + 'update/profile', {
      method: 'post',
      body: createFormData(this.state.photo1, {
        login_id: this.state.login_id,
        name: username.trim(),
        bio: this.state.UserDetail.bio == null ? '' : this.state.UserDetail.bio,
        country_code: '',
        city: this.state.UserDetail.city,
        dob: this.state.UserDetail.dob,
        mobile_number: ''
      })
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          RNProgressHUB.dismiss();
          store.dispatch({ type: UPDATE_USER, data: responseData.data })
          this.is_photo_selected1 = false
          this.setState({ isProfilePicVisible1: false })
        } else {
          RNProgressHUB.dismiss();

        }
      })

      .catch((error) => {
        //alert(error)
        RNProgressHUB.dismiss();
        // this.props.navigation.goBack()

      })
  }

  renderEditProfile() {
    console.log("this.state.bdayforpicker", this.state.bdayforpicker)
    return (
      <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#EFEFF4' }} >
        <Toastt ref="toast"></Toastt>
        {this.renderEditProfileTopBar()}
        <BackgroundImage >
          <Content>
            {/* <View style = {{height:200, width:'100%', backgroundColor:colors.appColor,position:'absolute',zIndex:-1}}></View> */}
            <View style={{ backgroundColor: colors.appColor, height: Height(38) }}>
              <ImageBackground style={{}} source={BG_images}>
                <View >
                  <ImageBackground
                    source={profile_border}
                    style={{ marginTop: Height(5), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', height: Height(15), width: Height(15), overflow: 'hidden' }}>
                    <TouchableOpacity onPress={() => this.onClickSelectPhoto1()} style={{ alignSelf: 'center', height: Height(12), width: Height(12), borderRadius: Height(15), borderWidth: Width(1), borderColor: '#FFF', overflow: 'hidden' }}>
                      <Image style={{ height: '100%', width: '100%' }} source={{ uri: this.state.photo1 }} />
                    </TouchableOpacity>
                  </ImageBackground>
                </View>
                <Text style={{ color: '#FFF', marginTop: Height(2), alignSelf: 'center', fontSize: FontSize(18), fontFamily: fonts.Raleway_Medium }}>{this.state.name1}</Text>
                <TouchableOpacity
                  onPress={() => this.onClickSelectPhoto1()}
                  style={{ alignSelf: 'center', marginTop: Height(1), height: Height(5), width: Width(50), borderRadius: Width(7), borderColor: '#FFF', borderWidth: Width(0.5), justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ color: '#FFF', fontSize: FontSize(16), fontFamily: fonts.Raleway_Regular }}>Change Profile Picture</Text>
                </TouchableOpacity>

              </ImageBackground>
            </View>
            <View style={{}}>
              <FloatingLabel
                labelStyle={[this.state.labelStyleName, (this.state.name1 == undefined || this.state.name1 == null || this.state.name1.length == 0) ? styles.labelInputUnFocus : '']}
                inputStyle={styles.input}
                style={[styles.formInput, this.state.styleName]}
                onChangeText={(name1) => this.setState({ name1 })}
                value={this.state.name1}
                onBlur={() => this.onBlurName()}
                onFocus={() => this.onFocusName()}
                autoCapitalize={'words'}

              >Name</FloatingLabel>
              {
                this.state.emailValidation == false ?
                  <Text style={[{ marginLeft: 25, marginRight: 30, marginTop: Height(1.5), color: "red", fontSize: 10, fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.emailValidationMsg}</Text>
                  : null
              }
              <FloatingLabel
                maxLength={110} multiline={true}
                labelStyle={[this.state.labelStyleBio, (this.state.bio1 == undefined || this.state.bio1 == null || this.state.bio1.length == 0) ? styles.labelInputUnFocus : '']}
                inputStyle={styles.input}
                style={[styles.formInput, this.state.styleBio]}
                onChangeText={(text) => this.setState({ bio1: text })}
                value={this.state.bio1}
                onBlur={() => this.onBlurBio()}
                onFocus={() => this.onFocusBio()}

              >Bio</FloatingLabel>
              {
                this.state.emailValidation == false ?
                  <Text style={[{ marginLeft: 25, marginRight: 30, marginTop: Height(1.5), color: "red", fontSize: 10, fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.emailValidationMsg}</Text>
                  : null
              }
              <FloatingLabel
                disabled={true}
                editable={false}
                autoCapitalize="none"
                labelStyle={[this.state.labelStyleCity, (this.state.city1 == undefined || this.state.city1 == null || this.state.city1.length == 0) ? styles.labelInputUnFocus : '']}
                inputStyle={styles.input}
                style={[styles.formInput, this.state.styleCity]}
                onChangeText={(city1) => this.setState({ city1 })} value={this.state.city1}
                onBlur={() => this.onBlurCity()}
                onFocus={() => this.onFocusCity()}

              >Current City</FloatingLabel>
              {
                this.state.emailValidation == false ?
                  <Text style={[{ marginLeft: 25, marginRight: 30, marginTop: Height(1.5), color: "red", fontSize: 10, fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.emailValidationMsg}</Text>
                  : null
              }

              <View>
                <FloatingLabel
                  labelStyle={[this.state.labelStylebirthday, (this.state.DOB1 == undefined || this.state.DOB1 == null || this.state.DOB1.length == 0) ? styles.labelInputUnFocus : '']}
                  inputStyle={styles.input}
                  style={[styles.formInput, this.state.stylebirthday, { marginTop: Height(1.5) }]}
                  onChangeText={(DOB1) => this.setState({ DOB1 })}
                  value={dateFormat(this.state.DOB1, "mmmm dd")}
                  //value={moment(this.state.DOB1).format("MMM DD")}
                  //value={moment(this.state.DOB).format('MMMM D')}
                  onBlur={() => this.onBlurbirthday()}
                  onFocus={() => this.onFocusbirthday()}
                >Birthday</FloatingLabel>
                <TouchableOpacity style={{ position: 'absolute', top: 10, bottom: 0, left: 25, right: 30 }} onPress={() => this._showDateTimePicker()}>
                </TouchableOpacity>
              </View>
              {
                this.state.DOBValidation == false ?
                  <Text style={[{ marginLeft: 25, marginRight: 30, marginTop: 10, color: "red", fontSize: 10, fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.DOBValidationMSG}</Text>
                  : null
              }

              {this.state.isDateTimePickerVisible == true && Platform.OS == 'android' ?
                <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#EFEFF4' }}>
                  <DatePicker
                    date={this.state.bdayforpicker}
                    minimumDate={new Date()}
                    androidVariant="iosClone"
                    mode='date'
                    onDateChange={(date) => {
                      this.setState({ bdayforpicker: date, DOB1: dateFormat(date, "dd-mmm-yyyy"), DOBPass1: dateFormat(date, "yyyy-mm-dd") }, () => { console.log('Selected DOB: ' + this.state.DOB1) })
                      setTimeout(function () {
                        subThis.Datevalidtion()

                      }, 100);
                    }}
                  />
                </View>
                : null
              }

              {this.state.name1 == '' || this.state.DOB1 == '' ?
                <View style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }}  >
                  <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Save</Text>
                  </ImageBackground>
                </View> :
                <TouchableOpacity style={{ width: '100%', marginTop: Height(2) }} onPress={this.editProfile} >
                  <ImageBackground source={this.state.btnDoneImage} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Save</Text>
                  </ImageBackground>
                </TouchableOpacity>}
              {
                Platform.OS == 'ios' ?
                  <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                    mode='date'
                    //date={new Date(this.state.UserDetail.dob)}
                    //date={moment(this.state.DOB).toDate()}
                    //date={ Platform.OS == 'ios' ? ((this.offsetInHours == 4 && this.bmonth == '03') ? moment(this.state.DOB).add('1','day').toDate() : moment(this.state.DOB).toDate()) : moment(this.state.DOB).toDate()}
                    date={new Date(this.state.bdayforpicker)}
                  />
                  :
                  null
              }

            </View>
          </Content>
          <SafeAreaView />
        </BackgroundImage>
        <Modal isVisible={this.state.isProfilePicVisible1} style={{ margin: 0, backgroundColor: '#fff' }}>
          <View style={styles.headerAndroidnav}>
            <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
            <View style={{ bottom: 10, position: 'absolute', left: Width(5), width: 35, height: 35 }}>
              <TouchableOpacity onPress={() => { this.setState({ isProfilePicVisible1: false }) }} style={{ width: 35, height: 35, justifyContent: 'center' }}>
                <Image source={closeWhite} style={{ tintColor: "white" }} />
              </TouchableOpacity>
            </View>
            <View style={{ bottom: 10, position: 'absolute', alignItems: 'center', alignSelf: 'center' }}>
              <Text style={styles.headerTitle} >{'Profile Photo'}</Text>
            </View>
            <TouchableOpacity style={{ bottom: 10, right: 10, position: 'absolute', alignItems: 'center' }} onPress={this.is_photo_selected1 ? () => this.onSaveProfilePic1() : () => this.onEditProfilePic1()}>
              <Text style={styles.headerTitle} >{this.is_photo_selected1 == true ? 'Save' : 'Edit'}</Text>
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Image style={{ height: '50%', width: '100%' }} source={{ uri: this.state.photo1 }} resizeMode='cover' />
          </View>
        </Modal>
      </Container>
    );
  }

  //Edit Profile End

  render() {
    let { isModalVisible } = this.state
    const date = this.state.bod
    const styles1 = this.makeStyles()

    return (
      <View style={{ flex: 1 }}>
        <View style={{ height: 350, width: '100%', backgroundColor: colors.appColor, position: 'absolute', zIndex: -1 }}></View>
        {this.renderTopBar()}
        <ScrollView
          stickyHeaderIndices={[1]}
          contentContainerStyle={{ backgroundColor: '#e4e3ea' }}
        >
          <View>
            <View style={{ backgroundColor: colors.appColor }}>
              <ImageBackground style={{}} source={BG_images}>
                <ImageBackground
                  source={profile_border}
                  style={{ marginTop: Height(2), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', height: Height(12), width: Height(12), overflow: 'hidden' }}>
                  <TouchableOpacity activeOpacity={0.9} style={{ alignSelf: 'center', height: Height(9), width: Height(9), borderRadius: Height(9), backgroundColor: '#fff', borderColor: '#FFF', borderWidth: Width(0.5), overflow: 'hidden' }} onPress={() => this.onClickSelectPhoto()}>
                    <Image style={{ height: '100%', width: '100%' }} source={{ uri: this.state.profile }} />
                  </TouchableOpacity>
                </ImageBackground>
                <Text style={{ color: '#FFF', marginTop: Height(2), alignSelf: 'center', fontSize: FontSize(18), fontFamily: fonts.Raleway_Semibold }}>{this.state.fullname}</Text>
                <TouchableOpacity onPress={() => this.onEditProfilePressed()} style={{ alignSelf: 'center', marginTop: Height(2), marginBottom: this.state.bio != '' && this.state.bio != null ? Height(2) : 32, height: Height(5), width: Width(40), borderRadius: Width(7), borderColor: '#FFF', borderWidth: Width(0.5), justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ color: '#FFF', fontSize: FontSize(16), fontFamily: fonts.Raleway_Medium }}>Edit Profile</Text>
                </TouchableOpacity>
                {
                  this.state.bio != '' && this.state.bio != null ?
                    <Text style={{ textAlign: 'center', marginBottom: 32, color: '#FFF', fontSize: FontSize(14), fontFamily: fonts.Roboto_Regular, alignSelf: 'center', width: '85%', lineHeight: 21, }}>{this.state.bio}</Text>
                    :
                    null
                }
              </ImageBackground>
            </View>

            <View>
              <View style={isIphoneX ? { flex: 1, marginLeft: Height(2), marginTop: Height(0.5) } : { flex: 1, marginLeft: Height(2), marginTop: Height(0.5) }}>
                <View style={{ flexDirection: 'row', marginTop: Height(3) }}>
                  <Image resizeMode="contain" style={{ alignSelf: 'center', tintColor: colors.whatFontColor }} source={location} />
                  <Text style={{ marginLeft: 5, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>Currently in {this.state.city}</Text>
                </View>
                {
                  moment(this.state.bod).isValid() == true ?
                    <View

                      style={{ flexDirection: 'row', marginTop: Height(2) }}>
                      <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={calendar} />
                      <Text style={{ marginLeft: 5, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{moment(this.state.bod).format('MMM D')}</Text>
                    </View> : null
                }
                {/* <View

                  style={{ flexDirection: 'row', marginTop: Height(2) }}>
                  <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={calendar} />
                  <Text style={{ marginLeft: 5, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{moment(this.state.bod).format('MMM D')}</Text>
                </View> */}

                <View

                  style={{ flexDirection: 'row', marginTop: Height(2), marginBottom: Height(3) }}>
                  <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={user} />
                  {this.state.friends == '0' ? <Text style={{ paddingLeft: Width(2), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>Friends with 0 people</Text> :
                    <View>{this.state.friends == '1' ? <Text style={{ paddingLeft: Width(2), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>Friends with {this.state.friends} person</Text>
                      : <Text style={{ marginLeft: 5, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>Friends with {this.state.friends} people</Text>}
                    </View>}
                </View>
              </View>
              <View style={{ height: Height(0.1), width: '92%', backgroundColor: '#B4B7BA', alignSelf: 'center' }} />
              <Text style={{ marginLeft: Width(4), fontFamily: fonts.Raleway_Bold, fontSize: FontSize(20), marginTop: Height(2.5), color: '#4B467A' }}>Suggest a plan</Text>
              <View style={{ marginTop: Height(2.5), flexDirection: 'row', justifyContent: 'space-between', marginLeft: Width(4), marginRight: Width(5) }}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('what1', { is_edit_plan: false })}
                  style={{
                    shadowColor: "#000",
                    shadowOffset: {
                      width: 0,
                      height: 2,
                    },
                    shadowOpacity: 0.23,
                    shadowRadius: 2.62,
                    elevation: 4
                  }}>
                  <View style={{ backgroundColor: '#FFF', borderRadius: Width(4), height: Height(13), width: Height(13), justifyContent: 'center', alignItems: 'center', elevation: 8 }}>
                    <Image source={pink_chai_cup} resizeMode="contain" />
                  </View>
                  <Text style={{ color: colors.dargrey, lineHeight: 20, alignSelf: 'center', textAlign: 'center', marginTop: Height(1), width: Height(13), fontSize: FontSize(14), fontFamily: fonts.Roboto_Regular }}>
                    What do you wanna do?
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('where1', { is_edit_plan: false })}
                  style={{
                    shadowColor: "#000",
                    shadowOffset: {
                      width: 0,
                      height: 2,
                    },
                    shadowOpacity: 0.23,
                    shadowRadius: 2.62,
                    elevation: 4
                  }}>
                  <View style={{ backgroundColor: '#FFF', borderRadius: Width(4), height: Height(13), width: Height(13), justifyContent: 'center', alignItems: 'center', elevation: 8 }}>
                    <Image source={pink_location} resizeMode="contain" />
                  </View>
                  <Text style={{ color: colors.dargrey, lineHeight: 20, alignSelf: 'center', textAlign: 'center', marginTop: Height(1), width: Height(13), fontSize: FontSize(14), fontFamily: fonts.Roboto_Regular }}>
                    Where do you wanna go?
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('when1', { is_edit_plan: false })}
                  style={{
                    shadowColor: "#000",
                    shadowOffset: {
                      width: 0,
                      height: 2,
                    },
                    shadowOpacity: 0.23,
                    shadowRadius: 2.62,
                    elevation: 4
                  }}>
                  <View style={{ backgroundColor: '#FFF', borderRadius: Width(4), height: Height(13), width: Height(13), justifyContent: 'center', alignItems: 'center', elevation: 8 }}>
                    <Image source={pink_clock} resizeMode="contain" />
                  </View>
                  <Text style={{ color: colors.dargrey, lineHeight: 20, alignSelf: 'center', textAlign: 'center', marginTop: Height(1), width: Height(13), fontSize: FontSize(14), fontFamily: fonts.Roboto_Regular }}>
                    When are you free?
                  </Text>
                </TouchableOpacity>

              </View>
              <View style={{ height: Height(2.5) }} />

            </View>
            <Modal isVisible={isModalVisible} style={{ margin: 0 }} onBackdropPress={() => this.setState({ isModalVisible: false })} onBackButtonPress={() => this.setState({ isModalVisible: false })}>
              <View style={{ height: Height(22), width: '100%', position: 'absolute', bottom: 0, backgroundColor: '#FFF' }}>
                <View style={{ justifyContent: 'center', height: Height(7) }}>
                  <Text onPress={() => this.gotoEventDetail()} style={{ marginLeft: Width(3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(18), color: colors.dargrey }}>Edit Plan</Text>
                </View>
                <View style={{ borderBottomColor: '#ccc', borderBottomWidth: 1 }} />
                <View style={{ justifyContent: 'center', height: Height(7) }}>
                  <Text onPress={() => {
                    this.setState({ isMyModalVisible: false, isModalVisible: false },
                      () => {
                        this.props.navigation.navigate('EditPrivacy', { plan_id: this.state.plan_id, privacy: this.state.currentitem.share_with })
                      }
                    )
                  }} style={{ marginLeft: Width(3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(18), color: colors.dargrey }}>Edit Privacy</Text>
                </View>
                <View style={{ borderBottomColor: '#ccc', borderBottomWidth: 1 }} />
                <View style={{ justifyContent: 'center', height: Height(7) }}>
                  <Text onPress={() => {
                    Alert.alert(
                      'Cancel Event',
                      'Guests will be notified that this event was canceled',
                      [
                        {
                          text: 'Cancel',
                          onPress: () => console.log('Cancel Pressed'),
                          style: 'cancel',
                        },
                        { text: 'OK', onPress: this.deleteplan },
                      ],
                      { cancelable: false },
                    );
                  }} style={{ marginLeft: Width(3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(18), color: colors.dargrey }}>Cancel</Text>
                </View>
              </View>

            </Modal>
          </View>

          <View>
            <View style={{ flexDirection: 'row', backgroundColor: colors.white }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    isProfile: true,
                    isPost: false,
                  });
                }}
                style={this.state.isProfile ? styles.selectedHeader : styles.notSelected}>
                <Text style={styles.subdigit}>Profile</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    isProfile: false,
                    isPost: true,
                  });
                  isloading = false
                  this.getPostList()

                }}
                style={this.state.isPost ? styles.selectedHeader : styles.notSelected}>
                <Text style={styles.subdigit}>Posts</Text>
              </TouchableOpacity>
            </View>
          </View>
          {this.state.isPost ?
            <View style={{ fleX: 1 }}>
              <FlatList
                data={this.state.planList}
                renderItem={this._renderItem}
                // refreshing={this.state.refreshing}
                // onRefresh={this.onRefresh}
                keyExtractor={this._keyExtractor}
                ItemSeparatorComponent={this.renderSeparator}
              />
            </View>
            : null}

          {this.state.isProfile ?
            <View style={{ backgroundColor: '#fff' }}>
              <View style={{ marginTop: Height(3), flexDirection: 'row' }}>
                <Text style={{ flex: 1, marginLeft: Width(4), fontFamily: fonts.Raleway_Bold, fontSize: FontSize(20), color: '#4B467A' }}>Friends {'\u00B7'} {this.state.friendCount}</Text>
                <Text onPress={() => { this.props.navigation.navigate('FriendsfriendList') }} style={{ flex: 1, marginRight: Width(4), alignSelf: 'flex-end', textAlign: 'right', fontFamily: fonts.Raleway_Regular, fontSize: FontSize(16), color: '#358AF6' }}>See All</Text>
              </View>
              {this.state.friendList !== '' ?
                <ScrollView showsHorizontalScrollIndicator={false} horizontal contentContainerStyle={{ marginTop: Height(2) }}>
                  {this.state.friendList.map(item => {
                    return (
                      <View>
                        <TouchableHighlight underlayColor="transparent" style={{ marginHorizontal: Width(3.5), height: Height(6), width: Height(6), borderRadius: Height(5), overflow: 'hidden', alignSelf: 'center' }} onPress={() => this.goToFriendProfile(item)}>
                          {item.receiver_photo == '' ? <Image source={flowers} style={{ height: '100%', width: '100%' }} /> :
                            <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.receiver_photo }} />}
                        </TouchableHighlight>
                        <Text style={{ marginTop: 8, textAlign: 'center', alignSelf: 'center', fontSize: FontSize(13), color: colors.dargrey, fontFamily: fonts.Raleway_Regular, width: Width(18) }}> {item.receiver_name}</Text>
                        <View style={{ height: Height(2) }} />
                      </View>
                    );
                  })}
                </ScrollView> : null}
              <TouchableOpacity style={{ width: '103%', marginBottom: Height(1), alignSelf: 'center' }} onPress={() => { this.props.navigation.navigate('FindFriends_Setting') }} >
                <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Find Your Friends</Text>
                </ImageBackground>
              </TouchableOpacity>
              <View style={{ marginHorizontal: Width(4) }}>
                <Text style={{ fontFamily: fonts.Raleway_Bold, color: colors.appColor, fontSize: FontSize(18) }}>Interests</Text>
              </View>
              <View style={{ marginHorizontal: Width(2.7) }}>
                <View style={{ height: Height(2) }} />
                <View style={styles1.view}>
                  {this.state.interest_list.map(item => {
                    return (
                      <Text style={styles1.text}>{item}</Text>
                    );
                  })}
                </View>
              </View>
              <View style={{ height: Height(2) }} />

              <TouchableOpacity style={{ width: '103%', marginBottom: Height(1), alignSelf: 'center' }} onPress={() => { this.props.navigation.navigate('ProfileCategories') }} >
                <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Add Your Interests</Text>
                </ImageBackground>
              </TouchableOpacity>

              {/* <View style={{ marginHorizontal: Width(4) }}>
                <Text style={{ fontFamily: fonts.Raleway_Bold, color: colors.appColor, fontSize: FontSize(18) }}>Places I Like</Text>
              </View>
              <View style={{ marginHorizontal: Width(2.7) }}>
                <View style={{ height: Height(2) }} />
                  <View style={styles1.view}>
                    {this.state.p_list.map(item => {
                      return (
                        <Text style={styles1.text}>{item}</Text>
                      );
                    })}
                  </View>
               
              </View>
              <View style={{ height: Height(2) }} />

              <TouchableOpacity style={{ width: '103%', marginBottom: Height(1), alignSelf: 'center' }} onPress={() => { this.props.navigation.navigate('ProfilePlaes') }} >
                <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Add Your Places</Text>
                </ImageBackground>
              </TouchableOpacity> */}
            </View>
            : null}
        </ScrollView>
        <Modal isVisible={this.state.isProfilePicVisible} style={{ margin: 0, backgroundColor: '#fff' }}>
          <View style={styles.headerAndroidnav}>
            <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
            <View style={{ bottom: 10, position: 'absolute', left: Width(5), width: 35, height: 35 }}>
              <TouchableOpacity onPress={() => { this.setState({ isProfilePicVisible: false }) }} style={{ width: 35, height: 35, justifyContent: 'center' }}>
                <Image source={closeWhite} style={{ tintColor: "white" }} />
              </TouchableOpacity></View>
            <View style={{ bottom: 10, position: 'absolute', alignItems: 'center', alignSelf: 'center' }}>
              <Text style={styles.headerTitle} >{'Profile Photo'}</Text>
            </View>
            <TouchableOpacity style={{ bottom: 10, right: 10, position: 'absolute', alignItems: 'center' }} onPress={this.is_photo_selected ? () => this.onSaveProfilePic() : () => this.onEditProfilePic()}>
              <Text style={styles.headerTitle} >{this.is_photo_selected == true ? 'Save' : 'Edit'}</Text>
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Image style={{ height: '50%', width: '100%' }} source={{ uri: this.state.profile }} resizeMode='cover' />
          </View>
        </Modal>
        <Modal isVisible={this.state.isEditProfileVisible} onBackdropPress={() => this.setState({ isEditProfileVisible: false })} style={{ margin: 0, backgroundColor: 'transparent' }}>
          {this.renderEditProfile()}
        </Modal>
      </View>

    );
  }
  makeStyles() {
    return StyleSheet.create({
      view: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        backgroundColor: '#fff',
        alignItems: 'center',

      },

      text: {
        fontSize: FontSize(14),
        marginLeft: '1.1%',
        marginTop: '1.1%',
        paddingLeft: '4%',
        paddingRight: '4%',
        paddingTop: '1.8%',
        paddingBottom: '1.8%',
        textAlign: 'center',
        color: '#676868',
        borderRadius: 5,
        borderColor: '#676868',
        borderWidth: 1,
      }
    })
  }
}

export default Profile;
