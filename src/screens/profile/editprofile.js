import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image,ScrollView, Alert, ImageBackground, TouchableOpacity, StatusBar, Text, Platform } from "react-native";
import { Container, View, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import FloatingLabel from 'react-native-floating-labels';
import { Height, Width, FontSize,isIphoneX,colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import DateTimePicker from 'react-native-modal-datetime-picker';
import Modal from "react-native-modal";
var dateFormat = require('dateformat');
const profile_border = require("../../../assets/images/Profile/profile_border.png");
import ImagePicker from 'react-native-image-picker';
import { API_ROOT } from "../../config/constant";
import { UPDATE_USER } from "../../redux/actions/types";
import { store } from "../../redux/store";
import moment from 'moment';
import RNProgressHUB from 'react-native-progresshub';

const BG_images = require("../../../assets/images/Profile/Profile_bg.png");
const backWhite = require("../../../assets/images/backWhite.png");
const bottomRight = require("../../../assets/images/Login/bottomRight.png");
const btnBgFB = require("../../../assets/images/Login/btnBgFB.png");
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
const btnBg = require("../../../assets/images/btnBg.png");
const settings = require("../../../assets/images/Profile/settings.png");
const chai_cup = require("../../../assets/images/Dashboard/chai_cup.png");
const location = require("../../../assets/images/Dashboard/location.png");
const calendar = require("../../../assets/images/Dashboard/calendar.png");
const pink_chai_cup = require("../../../assets/images/Dashboard/menu1.png");
const pink_clock = require("../../../assets/images/Dashboard/menu2.png");
const pink_location = require("../../../assets/images/Dashboard/menu3.png");
const closeWhite = require("../../../assets/images/ic_close_white.png");



var subThis;
var defaultdate = new Date();
class EditProfile extends ValidationComponent {

  constructor(props) {
    super(props);
    var offsetInHours = 0;
    var bmonth = '';
    this.is_photo_selected1 = false;
    this.state = {
      labelStylebirthday: styles.labelInputUnFocus,
      stylebirthday: styles.formInputUnFocus,
      labelStylePassword: styles.labelInputUnFocus, stylePassword: styles.formInputUnFocus, password: "", passwordValidation: true, passwordValidationMsg: "",
      labelStyleCity: styles.labelInputUnFocus, labelStyleBio: styles.labelInputUnFocus, labelStyleName: styles.labelInputUnFocus, styleName: styles.formInputUnFocus, styleBio: styles.formInputUnFocus, styleCity: styles.formInputUnFocus, email: "", emailValidation: true, emailValidationMsg: "",
      btnDoneDisable: true, btnDoneImage: btnBg, wsError: false,
      login_id: store.getState().auth.user.login_id,
      photo_path: null,
      name1: '',
      photo1: '',
      city1: '',
      DOB1: new Date(),
      bdayforpicker: new Date(),
      bio1: '',
      DOBPass1: "",
      UserDetail: this.props.userDetails, //global.userDetails, //this.props.navigation.state.params.UserDetail,
      isProfilePicVisible1:false,
    };
    subThis = this

  }

  componentDidMount() {

    //alert(JSON.stringify(this.state.UserDetail))
    //return
    this.setState({
      name1: this.state.UserDetail.name,
      photo1: this.state.UserDetail.photo,
      bio1: this.state.UserDetail.bio,
      city1: this.state.UserDetail.city,
       DOB1: this.state.UserDetail.dob,
    },
      () => {

          //console.log('DOB after adding day: '+moment(this.state.DOB).add('1','day').toDate())

        if(Platform.OS == 'ios')
        {
            var date = new Date();
            this.offsetInHours = date.getTimezoneOffset() / 60;
            var bcomps = this.state.UserDetail.dob.split('-')
            if(bcomps.length > 1)
            {
                this.bmonth = bcomps[1]
            }

            if(this.offsetInHours == 4)
            {              
                let curDob = new Date(this.state.DOB1)
                let updatedDOB = curDob.setDate(curDob.getDate() + 1)
                //alert('Updated Bdate :'+updatedDOB +"Original Date: "+this.state.UserDetail.dob)
                this.setState({bdayforpicker:updatedDOB,DOB1:updatedDOB})

            }
            else
            {
              let curDob = new Date(this.state.DOB1)
              this.setState({bdayforpicker:curDob,DOB1:curDob})
            }

        }
      }
    )
    
  }


  titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
    }
    return splitStr.join(' '); 
 }

  editProfile = () => {

    // alert(moment(this.state.DOB).toDate())
    // return
    var username = this.titleCase(this.state.name1)

    RNProgressHUB.showSpinIndeterminate();
    // const createFormData = (photo_path, body) => {
    //   const data = new FormData();
    //   data.append("profile_pic", {
    //     name: 'selfie.jpg',
    //     type: 'image/jpg',
    //     uri:
    //       Platform.OS === "android" ? this.state.photo : this.state.photo.replace("file://", "")
    //   });

    //   Object.keys(body).forEach(key => {
    //     data.append(key, body[key]);
    //   });

    //   return data;
    // };

    var data = new FormData();
    data.append("profile_pic", {
          name: 'selfie.jpg',
          type: 'image/jpg',
          uri:
            Platform.OS === "android" ? this.state.photo1 : this.state.photo1.replace("file://", "")
        });
    data.append('login_id',this.state.login_id)
    data.append('name',username.trim())
    data.append('bio',this.state.bio1)
    data.append('country_code','')
    data.append('city',this.state.city1)
    data.append('dob',this.state.DOBPass1)
    data.append('mobile_number','')
    
        //console.log('PROFILE_DATA: '+JSON.stringify(data))

    fetch(API_ROOT + 'update/profile', {
      method: 'post',
      body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {

          this.mixpanel.identify(store.getState().auth.user.email_address);
          this.mixpanel.getPeople().set('Brithday', this.state.DOBPass1);
          this.mixpanel.getPeople().set('$name', username.trim());
          this.mixpanel.getPeople().set('Bio', bio);
          this.mixpanel.getPeople().set('$avatar', this.state.photo1.replace("file://", ""));
          this.mixpanel.track('Update Profile', mixpanel_data_with_social);

          RNProgressHUB.dismiss();
          store.dispatch({type:UPDATE_USER,data:responseData.data})
           this.props.navigation.goBack()
        } else {
          RNProgressHUB.dismiss();
           this.props.navigation.goBack()

        }
      })

      .catch((error) => {
        //alert(error)
        RNProgressHUB.dismiss();
        // this.props.navigation.goBack()

      })

  }

  // static navigationOptions = ({ navigation }) => {
  //   theNavigation = navigation;
  //   return {
  //     header: (
  //       <Header style={styles.headerAndroidnav}>
  //         <StatusBar barStyle="light-content" backgroundColor="#41199B" />
  //         <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
  //           <TouchableOpacity onPress={() => { navigation.goBack() }}>
  //             <Image source={backWhite} style={{ tintColor: "white" }} />
  //           </TouchableOpacity></View>
  //         <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
  //           <Text style={styles.headerTitle} >Edit Profile</Text>
  //         </View>

  //       </Header>
  //     )
  //   }
  // }

  renderTopBar()
  {
    return (
        <Header style={[styles.headerAndroidnav,{height:44}]}>
          <StatusBar barStyle="light-content" backgroundColor="#41199B" />
          <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5)}}>
            {/* <TouchableOpacity onPress={() => { navigation.goBack() }}> */}
            <TouchableOpacity>
              <Image source={backWhite} style={{ tintColor: "white" }} />
            </TouchableOpacity>
          </View>
          <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
            <Text style={styles.headerTitle} >Edit Profile</Text>
          </View>
        </Header>
      )
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (date) => {

    console.log('Selected Date: '+date)

    if (date != undefined) {
      //console.log('A date has been picked: ', dateFormat(date, "dd/mm/yyyy"));

      this.setState({ bdayforpicker:date,DOB1: dateFormat(date,"dd-mmm-yyyy"), DOBPass1: dateFormat(date, "yyyy-mm-dd")},() => {console.log('Selected DOB: '+this.state.DOB1)})

      //this.setState({ DOB: moment(date).format(), DOBPass: dateFormat(date, "yyyy-mm-dd") })
      setTimeout(function () {
        subThis.Datevalidtion()

      }, 100);
    }

    this._hideDateTimePicker();
  };

  Datevalidtion = () => {

    var g1 = new Date()
    var g2 = new Date(this.state.DOB1)

    // if (this.state.DOB.length < 1) {
    //   this.setState({ DOBValidation: false, DOBValidationMSG: "Select BirthDay.", labelStyleDOB: [styles.labelInput, styles.labelInputError], })
    //   return false
    // }

    if (this.compare_dates(g1,g2) == 2 || this.compare_dates(g1,g2) == 0) {
      this.setState({ DOBValidation: false, DOBValidationMSG: "Invalid Birthday", labelStyleDOB: [styles.labelInput, styles.labelInputError], })
      return false
    }
    else
    {
        this.setState({DOBValidation:true})
    }
    this.defaultdate = new Date(this.state.DOBPass1)

    return true
  }

  compare_dates = function(date1,date2){
    if (date1>date2) return 1;
  else if (date1<date2) return 2;
  else return 0; 
 }

  onBlurbirthday() {
    if (this.state.password.length <= 0) {
      this.setState({
        labelStylebirthday: styles.labelInputUnFocus,
        stylebirthday: styles.formInputUnFocus
      })
      return false
    } else {
      return true
    }
  }
  onFocusbirthday() {
    this.setState({
      labelStylebirthday: styles.labelInput,
      stylebirthday: styles.formInputFocus
    })
  }
  onBlurBio() {

    this.setState({
      labelStyleBio: styles.labelInputUnFocus,
      styleBio: styles.formInputUnFocus

    })

  }
  onFocusBio() {
    this.setState({
      labelStyleBio: styles.labelInput,
      styleBio: styles.formInputFocus
    })
  }

  onBlurName() {

    this.setState({
      labelStyleName: styles.labelInputUnFocus,
      styleName: styles.formInputUnFocus

    })

  }
  onFocusName() {
    this.setState({
      labelStyleName: styles.labelInput,
      styleName: styles.formInputFocus
    })
  }

  onBlurCity() {

    this.setState({
      labelStyleCity: styles.labelInputUnFocus,
      styleCity: styles.formInputUnFocus

    })

  }
  onFocusCity() {
    this.setState({
      labelStyleCity: styles.labelInput,
      styleCity: styles.formInputFocus
    })
  }
  onClickSelectPhoto1 = () => {
    this.setState({isProfilePicVisible1:true})
  }

  onEditProfilePic1 = () => {
    var options = {
      title: '',

      allowsEditing: true,
      storageOptions: {
        skipBackup: true,
        path: 'images'
      },
      maxWidth: 500,
      maxHeight: 500
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        this.is_photo_selected1 = false
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        this.is_photo_selected1 = false
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };
        this.is_photo_selected1 = true
        this.setState({
          photo1: response.uri,
        })
      }
    });
  }

  titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
    }
    return splitStr.join(' '); 
 }

  onSaveProfilePic1 = () => {
      
    var username = this.titleCase(this.state.UserDetail.name)
    
        RNProgressHUB.showSpinIndeterminate();
        const createFormData = (photo_path, body) => {
          const data = new FormData();
          data.append("profile_pic", {
            name: 'selfie.jpg',
            type: 'image/jpg',
            uri:
              Platform.OS === "android" ? this.state.photo1 : this.state.photo1.replace("file://", "")
          });
    
          Object.keys(body).forEach(key => {
            data.append(key, body[key]);
          });
    
          return data;
        };
    
        fetch(API_ROOT + 'update/profile', {
          method: 'post',
          body: createFormData(this.state.photo1, {
            login_id: this.state.login_id,
            name: username.trim(),
            bio: this.state.UserDetail.bio == null ? '' : this.state.UserDetail.bio ,
            country_code: '',
            city: this.state.UserDetail.city,
            dob: this.state.UserDetail.dob,
            mobile_number: ''
          })
        })
          .then((response) => response.json())
          .then((responseData) => {
            if (responseData.success) {
              RNProgressHUB.dismiss();
              store.dispatch({type:UPDATE_USER,data:responseData.data})
              this.is_photo_selected1 = false
               this.setState({isProfilePicVisible1:false})
            } else {
              RNProgressHUB.dismiss();
               
            }
          })
    
          .catch((error) => {
            //alert(error)
            RNProgressHUB.dismiss();
            // this.props.navigation.goBack()
    
          })
  }

  render() {
    return (
      <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#EFEFF4' }} >
        <Toastt ref="toast"></Toastt>
        {this.renderTopBar()}
        <BackgroundImage >
          <Content>
            {/* <View style = {{height:200, width:'100%', backgroundColor:colors.appColor,position:'absolute',zIndex:-1}}></View> */}
            <View style={{ backgroundColor: colors.appColor, height: Height(38) }}>
              <ImageBackground style={{}} source={BG_images}>
                <View >
                  <ImageBackground
                  source={profile_border}
                  style={{ marginTop: Height(5), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', height: Height(15), width: Height(15), overflow: 'hidden' }}>
                  <TouchableOpacity onPress={() => this.onClickSelectPhoto1()} style={{ alignSelf: 'center', height: Height(12), width: Height(12), borderRadius: Height(15), borderWidth: Width(1), borderColor: '#FFF', overflow: 'hidden' }}>
                    <Image style={{ height: '100%', width: '100%' }} source={{ uri: this.state.photo1 }} />
                  </TouchableOpacity>
                </ImageBackground>
                </View>
                <Text style={{ color: '#FFF', marginTop: Height(2), alignSelf: 'center', fontSize: FontSize(18), fontFamily: fonts.Raleway_Medium }}>{this.state.name1}</Text>
                <TouchableOpacity
                  onPress={() => this.onClickSelectPhoto1()}
                  style={{ alignSelf: 'center', marginTop: Height(1), height: Height(5), width: Width(50), borderRadius: Width(7), borderColor: '#FFF', borderWidth: Width(0.5), justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={{ color: '#FFF', fontSize: FontSize(16), fontFamily: fonts.Raleway_Regular }}>Change Profile Picture</Text>
                </TouchableOpacity>

              </ImageBackground>
            </View>
            <View style={{}}>
              <FloatingLabel
                labelStyle={[this.state.labelStyleName, this.state.name1.length == 0 ? styles.labelInputUnFocus : '']}
                inputStyle={styles.input}
                style={[styles.formInput, this.state.styleName]}
                onChangeText={(name1) => this.setState({ name1 })} 
                value={this.state.name1}
                onBlur={() => this.onBlurName()}
                onFocus={() => this.onFocusName()}
                autoCapitalize = {'words'}

              >Name</FloatingLabel>
              {
                this.state.emailValidation == false ?
                  <Text style={[{ marginLeft: 25, marginRight: 30, marginTop: Height(1.5), color: "red", fontSize: 10, fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.emailValidationMsg}</Text>
                  : null
              }
              <FloatingLabel
                maxLength={110} multiline={true}
                labelStyle={[this.state.labelStyleBio, (this.state.bio1 == null || this.state.bio1.length == 0) ? styles.labelInputUnFocus : '']}
                inputStyle={styles.input}
                style={[styles.formInput, this.state.styleBio]}
                onChangeText={(text) => this.setState({ bio1:text })} 
                value={this.state.bio1}
                onBlur={() => this.onBlurBio()}
                onFocus={() => this.onFocusBio()}

              >Bio</FloatingLabel>
              {
                this.state.emailValidation == false ?
                  <Text style={[{ marginLeft: 25, marginRight: 30, marginTop: Height(1.5), color: "red", fontSize: 10, fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.emailValidationMsg}</Text>
                  : null
              }
              <FloatingLabel
                disabled={true}
                editable={false}
                autoCapitalize="none"
                labelStyle={[this.state.labelStyleCity, this.state.city1.length == 0 ? styles.labelInputUnFocus : '']}
                inputStyle={styles.input}
                style={[styles.formInput, this.state.styleCity]}
                onChangeText={(city1) => this.setState({ city1 })} value={this.state.city1}
                onBlur={() => this.onBlurCity()}
                onFocus={() => this.onFocusCity()}

              >Current City</FloatingLabel>
              {
                this.state.emailValidation == false ?
                  <Text style={[{ marginLeft: 25, marginRight: 30, marginTop: Height(1.5), color: "red", fontSize: 10, fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.emailValidationMsg}</Text>
                  : null
              }

              <View>
                <FloatingLabel
                  labelStyle={[this.state.labelStylebirthday, this.state.DOB1.length == 0 ? styles.labelInputUnFocus : '']}
                  inputStyle={styles.input}
                  style={[styles.formInput, this.state.stylebirthday, { marginTop: Height(1.5) }]}
                  onChangeText={(DOB1) => this.setState({ DOB1 })}
                  value={dateFormat(this.state.DOB1, "mmmm dd")}
                  //value={moment(this.state.DOB).format('MMMM D')}
                  onBlur={() => this.onBlurbirthday()}
                  onFocus={() => this.onFocusbirthday()}
                >Birthday</FloatingLabel>
                <TouchableOpacity style={{ position: 'absolute', top: 10, bottom: 0, left: 25, right: 30 }} onPress={() => this._showDateTimePicker()}>
                </TouchableOpacity>
              </View>
              {
                this.state.DOBValidation == false ?
                  <Text style={[{ marginLeft: 25, marginRight: 30, marginTop: 10, color: "red", fontSize: 10, fontFamily: "Roboto-Bold" }, checkFontWeight("700")]}>{this.state.DOBValidationMSG}</Text>
                  : null
              }
              {this.state.name1 == '' || this.state.DOB1 == '' || this.state.city1 == ''?
                <View style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }}  >
                  <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Save</Text>
                  </ImageBackground>
                </View> :
                <TouchableOpacity style={{ width: '100%', marginTop: Height(2) }} onPress={this.editProfile} >
                  <ImageBackground source={this.state.btnDoneImage} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Save</Text>
                  </ImageBackground>
                </TouchableOpacity>}
              <DateTimePicker
                isVisible={this.state.isDateTimePickerVisible}
                onConfirm={this._handleDatePicked}
                onCancel={this._hideDateTimePicker}
                mode='date'
                //date={new Date(this.state.UserDetail.dob)}
                //date={moment(this.state.DOB).toDate()}
                //date={ Platform.OS == 'ios' ? ((this.offsetInHours == 4 && this.bmonth == '03') ? moment(this.state.DOB).add('1','day').toDate() : moment(this.state.DOB).toDate()) : moment(this.state.DOB).toDate()}
                date={new Date(this.state.bdayforpicker)}
              />
            </View>
          </Content>
          <SafeAreaView />
        </BackgroundImage>
        <Modal isVisible = {this.state.isProfilePicVisible1} style = {{margin:0, backgroundColor:'#fff'}}>
              <View style={styles.headerAndroidnav}>
                <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
                <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                    <TouchableOpacity onPress={() => { this.setState({isProfilePicVisible1:false}) }}>
                        <Image source={closeWhite} style={{ tintColor: "white" }} />
                    </TouchableOpacity></View>
                <View style={{ bottom: Height(1.5), position: 'absolute', alignItems: 'center',alignSelf:'center' }}>
                  <Text style={styles.headerTitle} >{'Profile Photo'}</Text>
                </View>
                <TouchableOpacity style={{ bottom: Height(1.5),right:10, position: 'absolute', alignItems: 'center' }} onPress = { this.is_photo_selected1 ? () => this.onSaveProfilePic1() : () => this.onEditProfilePic1()}>
                    <Text style={styles.headerTitle} >{this.is_photo_selected1 == true ? 'Save' : 'Edit'}</Text>
                </TouchableOpacity>
            </View>
            <View style = {{flex:1, alignItems:'center', justifyContent:'center'}}>
                <Image style={{ height: '50%', width: '100%'}} source={{ uri: this.state.photo1}} resizeMode = 'cover'/>
            </View>
        </Modal>
      </Container>
    );
  }
}

export default EditProfile;
