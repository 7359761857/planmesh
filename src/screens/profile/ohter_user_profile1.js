import React, { Component } from "react";
import { SafeAreaView, FlatList, Dimensions, Image, StyleSheet, Modal, TouchableHighlight, TouchableWithoutFeedback, Alert, View, ScrollView, ImageBackground, TouchableOpacity, StatusBar, Text, Platform } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import FloatingLabel from 'react-native-floating-labels';
import Entypo from 'react-native-vector-icons/Entypo';
import { Height, Width, FontSize, colors, isIphoneX } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { API_ROOT } from "../../config/constant";
import { store } from "../../redux/store";
import { Body, Header, List, ListItem as Item } from "native-base";
import SplashScreen from 'react-native-splash-screen'
import RNProgressHUB from 'react-native-progresshub';


const { width: SCREEN_WIDTH } = Dimensions.get("window");
const IMAGE_HEIGHT = Platform.OS === "ios" ? Height(84) : Height(84);
const HEADER_HEIGHT = Platform.OS === "ios" ? 0 : 0;
const SCROLL_HEIGHT = IMAGE_HEIGHT - HEADER_HEIGHT;
const THEME_COLOR = "#fff";
const FADED_THEME_COLOR = "rgba(85,186,255, 0.8)";
const backWhite = require("../../../assets/images/backWhite.png");
const profile_border = require("../../../assets/images/Profile/profile_border.png");
const BG_images = require("../../../assets/images/Profile/Profile_bg.png");
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
const btnBg = require("../../../assets/images/btnBg.png");
const settings = require("../../../assets/images/Profile/settings.png");
const user = require("../../../assets/images/Profile/user.png");
const location = require("../../../assets/images/Dashboard/location_home.png");
const pink_chai_cup = require("../../../assets/images/Profile/cup.png");
const pink_clock = require("../../../assets/images/Profile/clock.png");
const pink_location = require("../../../assets/images/Profile/location.png");
const chai_cup = require("../../../assets/images/Dashboard/chai_cup.png");
const calendar = require("../../../assets/images/Dashboard/calendar.png");
const new_user = require("../../../assets/images/Dashboard/new_user.png");
const intersted = require("../../../assets/images/Dashboard/Favorite_uncolr.png");
const flowers = require("../../../assets/images/Dashboard/avtar.png");
const thumb = require("../../../assets/images/Dashboard/thumb.png");
const bg = require("../../../assets/images/Dashboard/bg.png");
const private_lock = require("../../../assets/images/Profile/lock_profile.png");
const closeWhite = require("../../../assets/images/ic_close_white.png");


import moment from 'moment';
import Icon from "react-native-vector-icons/Entypo";
var dateFormat = require('dateformat');
var subThis;
let isloading = false
var unfriendmsg = ''

class Profile extends ValidationComponent {


  _didFocusSubscription
  constructor(props) {
    super(props);
    offset = 0
    this.is_api_called = false;
    this.state = {
      interest_list: [],
      deleteModal: false,

      places: ['Arcade', 'Casino', 'Exhibit', 'Bowling Alley', 'Drive-in Theater', 'Jazz Club', 'Street Fair', 'Festival'],
      token: store.getState().auth.user.token,
      login_id: store.getState().auth.user.login_id,
      other_id: '',
      btnDoneDisable: true, btnDoneImage: btnBgGrey, wsError: false,
      post_name: '',
      plan_id: '',
      fullname: '',
      bio: '',
      refreshing: true,
      offset: 0,
      limit: 150,
      tabIndex: 0,
      profile: '',
      city: '',
      bod: '',
      friendList: [],
      friends: '',
      UserDetail: [],
      index: 0,
      friendCount: '',
      planList: [],
      interestList: [],
      placeList: [],
      p_list: [],
      isModalVisible: false,
      friendModal: false,
      isProfile: true,
      isPost: false,
      height: 0,
      is_Friend: 0,
      routes: [{ key: 'first', title: 'Profile' }, { key: 'second', title: 'Posts' }],
      isProfilePicVisible1:false,
    };
    this.gotoBlock = this.gotoBlock.bind(this)
    subThis = this
    this._didFocusSubscription = props.navigation.addListener('didFocus', payload => {
      this.is_api_called = false;
      this.getUserData()

    }
    )
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
      header: (
        <Header style={[styles.headerAndroidnav,{height:44}]}>
          <StatusBar barStyle="light-content" backgroundColor="#41199B" />
          <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
            <TouchableOpacity onPress={() => { navigation.goBack() }}>
              <Image source={backWhite} style={{ tintColor: "white" }} />
            </TouchableOpacity></View>
          <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
            <Text style={styles.headerTitle} >Profile</Text>
          </View>
          <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', right: Width(4) }}>
          </View>

        </Header>

      )
    }
  }
  async componentDidMount() {
    SplashScreen.hide()

    await AsyncStorage.getItem('other_id_1').then((value) => {
      this.setState({ other_id: value })
    });
    // this.getUserData()

  }
  componentWillReceiveProps(props) {
    if (props.navigation.state.params != null) {
      if (props.navigation.state.params.refreshing == true) {
        this.onRefresh()
      }
    }
  }
  deleteplan = () => {
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('plan_id', this.state.plan_id)
    fetch(API_ROOT + 'delete/plan', {
      method: 'post', body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          this.setState({ isModalVisible: false })
          this.getUserData()
        } else {
          alert(responseData.text)

        }
      })
      .catch((error) => { 
        //alert(error) 
      })
  }

  async getUserData() {
    var votes = [], interest = []

    await AsyncStorage.getItem('other_id_1').then((value) => {
      this.setState({ other_id: value })
    });

    
    var data = new FormData()

    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('type', 1)
    data.append('other_id', this.state.other_id)
    data.append('limit', this.state.limit)
    data.append('offset', this.state.offset)

    this.setState({ refreshing: true })
    RNProgressHUB.showSpinIndeterminate()
    fetch(API_ROOT + 'user/details', {
      method: 'post',
      body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {

          //alert(JSON.stringify(responseData))
          this.is_api_called = true;
          this.setState({ placeList: responseData.data.fav_place_list, interestList: responseData.data.fav_sub_category_list })

          let data = responseData.data
          if (data.dob == '0000-00-00') {
            this.setState({ bod: new Date })
          } else {

            if(data.dob == '' || data.dob == undefined || data.dob == "")
            {
              this.setState({ bod:'' })
            }
            else
            {
              this.setState({ bod: data.dob })
            }
          }

          

          this.setState({
            fullname: data.name, bio: data.bio, profile: data.photo, city: data.city,
            friends: data.total_friends, UserDetail: responseData.data, friendList: data.friends_list,
            is_Friend: data.is_friend,
            friendCount: data.friends_list.length, refreshing: false
          })
          this.state.placeList.map((product) => {

            votes.push(product.place_name);

          })
          this.setState({ p_list: votes })

          this.state.interestList.map((i) => {

            interest.push(i.sub_category_name);

          })
          this.setState({ interest_list: interest })

          unfriendmsg = 'Remove ' + this.state.fullname + ' as a friend?'

          AsyncStorage.setItem('friendList', JSON.stringify(data.friends_list))
          AsyncStorage.setItem('name', data.name)
          AsyncStorage.setItem('email', data.email_address)
          AsyncStorage.setItem('mobile', data.mobile_number)
          AsyncStorage.setItem('mobile_code', data.country_code)

          


        } else {
          this.is_api_called = true;
          if (responseData.text == 'Invalid token key') {
            this.setState({ status: false })
            this.refs.toast.show(responseData.text);
            this.props.navigation.navigate('Login')
          }
          

        }
        RNProgressHUB.dismiss()
      })

      .catch((error) => {
        RNProgressHUB.dismiss()

        console.warn(error);
      })
  }
  getPostList = () => {
    
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('limit', this.state.limit)
    data.append('other_id', this.state.other_id)
    data.append('offset', this.state.offset)
    data.append('type', 'other')
    this.setState({ refreshing: true })
    fetch(API_ROOT + 'plan/list', {
      method: 'post',
      body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          if (this.offset == 0) {
            this.offset = this.offset + this.state.limit
          }
          this.setState({
            planList: responseData.data, refreshing: false,
          });

          this.arrayholder = responseData.data;
        } else {
          this.setState({ refreshing: false })
          if (responseData.text == 'Invalid token key') {
            this.setState({ status: false })
            this.refs.toast.show(responseData.text);
            this.props.navigation.navigate('Login')
          }
        }
      })
      .catch((error) => {
        this.setState({ refreshing: false })

      })
  }
  onEndReached = () => {
    if (isloading == false) {
      isloading = true
      let data = new FormData();
      data.append('token', this.state.token)
      data.append('login_id', this.state.login_id)
      data.append('limit', this.state.limit)
      data.append('offset', this.offset)
      data.append('type', 1)
      RNProgressHUB.showSpinIndeterminate()
      fetch(API_ROOT + 'plan/list', {
        method: 'post',
        body: data
      }).then(response => response.json())

        .then(responseData => {
          if (responseData.success) {
            this.offset = this.offset + this.state.limit;


            if (responseData.data.length == 0) {
              // response mathi blank array or aray length blank made to kai nai karvanu
            }
            else {
              let oldData = this.state.planList
              let newData = oldData.concat(responseData.data)
              this.setState({ planList: newData, refreshing: false })
              isloading = false
            }
             RNProgressHUB.dismiss();

          } else {
             RNProgressHUB.dismiss();
            this.setState({ status: false })
          }
        })
        .catch(err => {
           RNProgressHUB.dismiss();
        });
    }
  }


  onRefresh = () => {

    this.setState({ refreshing: true });

    this.getUserData()

  }
  modelVisible = (item) => {
    this.setState({ isModalVisible: true, post_name: item.name, plan_id: item.plan_id })
  }
  setFriendModalVisible(visible) {
    this.setState({ friendModal: visible });
  }

  unBlockUserAPICall()
  {
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('other_id', this.state.other_id)
    data.append('is_block', '0')

    fetch(API_ROOT + 'block/user', {
      method: 'post', body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          this.getUserData()
        } else {
          alert(responseData.text)
        }
      })
      .catch((error) => { 
        //alert(error) 
      })
  }

  addFriendAPICall()
  {
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    //data.append('mobile_number', this.state.UserDetail.mobile_number)
    data.append('other_id', this.state.UserDetail.login_id)
    
    fetch(API_ROOT + 'friend_request', {
      method: 'post', body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          this.getUserData()
        } else {
          alert(responseData.text)
        }
      })
      .catch((error) => { 
        //alert(error) 
      })
  }

  cancelFriendRequestAPICall(other_id)
    {
        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('other_id', other_id)

        fetch(API_ROOT + 'friend_request/cancel', {
        method: 'post', body: data
        })
        .then((response) => response.json())
        .then((responseData) => {
            if (responseData.success) {
            
              this.getUserData()

            } else {
            alert(responseData.text)
            }
        })
        .catch((error) => { 
          //alert(error) 
        })
    }


  setDeleteModalVisible(visible) {
    this.setState({ deleteModal: visible });
  }
  _keyExtractor = (item, index) => item.plan_id;
  _renderItem = ({ item }) => {
    let Final_date = ''
    let dateTime = '';
    var tomorrow = moment().add(1, 'days').format("ll")

    var currentDate = moment().format("ll");
    var name = item.friend_name.split(" ")

    var start = item.start_date.split(" ")
    var end = item.end_date.split(" ")

    var gmtDateTime = moment.utc(item.created_date)
    var date1 = gmtDateTime.local();
    if (moment(date1).format("ll") == moment().format("ll")) {
      dateTime = date1.fromNow();
    } else {
      dateTime = date1.calendar();

    }
    var activity = ''
    var str = String(item.activity);
    if (str.includes('Wanna')) {
      activity = str.substr(6).slice(0, -1);
    } else {
      activity = str
    }
    if (activity.includes('?')) {
      activity = activity.slice(0, -1)
    }
    if (item.is_now == '1') {
      if (currentDate == moment(item.start_date).format('ll') && currentDate == moment(item.end_date).format('ll')) {
        Final_date = 'Now - ' + moment(item.end_date).format('LT')
      } else {
        if (currentDate == moment(item.start_date).format('ll') && tomorrow == moment(item.end_date).format('ll')) {
          Final_date = 'Now - Tomorrow · ' + moment(this.state.date).format('LT')
        } else {
          Final_date = 'Now - ' + moment(item.end_date).format('ddd, MMM D \u00B7 LT')
        }

      }
    } else {
      if (currentDate == moment(item.start_date).format('ll') && currentDate == moment(item.end_date).format('ll')) {
        if (start[1] == '00:00:00' && end[1] == '00:00:00') {

          Final_date = 'Today · All Day'
        } else {
          Final_date = 'Today · ' + moment(item.start_date).format('LT')
        }
      } else {
        if (start[1] == '00:00:00' && end[1] == '00:00:00') {
          if (tomorrow == moment(item.start_date).format('ll') && tomorrow == moment(item.end_date).format('ll')) {
            Final_date = 'Tomorrow · All Day'

          } else {
            if (tomorrow == moment(item.start_date).format('ll')) {
              Final_date = 'Tomorrow · All Day'

            } else {
              if (currentDate == moment(item.start_date).format('ll')) {
                Final_date = 'Today' + ' · All Day '

              } else {

                Final_date = moment(item.start_date).format('ddd, MMM D') + ' · All Day '
              }

            }

          }
        } else {
          if (tomorrow == moment(item.start_date).format('ll') && tomorrow == moment(item.end_date).format('ll')) {
            Final_date = 'Tomorrow · ' + moment(item.start_date).format('LT')

          } else {
            if (tomorrow == moment(item.start_date).format('ll')) {
              Final_date = 'Tomorrow · ' + moment(item.start_date).format('LT')

            } else {
              if (currentDate == moment(item.start_date).format('ll')) {
                Final_date = 'Today ·' + moment(item.start_date).format('LT')

              } else {

                Final_date = moment(item.start_date).format('ddd, MMM D \u00B7 LT')
              }

            }

          }

        }

      }

    }
    return (<View>
      {item.plan_type == 1 ?
        <View style={{ backgroundColor: colors.white }}>
          <View style={{ height: Height(9), flexDirection: 'row', alignItems: 'center', backgroundColor: 'white' }}>
            <View style={{ alignItems: 'center', marginLeft: Width(3) }}>
              <View underlayColor="transparent" style={{ height: Height(5), width: Height(5), borderRadius: Height(7), overflow: 'hidden'}}>
                {item.photo == '' ? <Image source={avtar} style={{ height: '100%', width: '100%' }} /> :
                  <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.photo }} />}
              </View>
            </View>
            <View style={{ justifyContent: 'center', marginLeft: Width(3) }}>
              <Text style={{ fontSize: FontSize(18), color: colors.dargrey, fontFamily: fonts.Raleway_Bold }}>{item.login_id == this.state.login_id ? 'Me' : item.name}</Text>
              <Text style={{ fontSize: FontSize(12), color: colors.fontDarkGrey, fontFamily: fonts.Roboto_Regular }}>{dateTime}{item.share_with == '' ? null : '\u00B7 '}{item.share_with}</Text>

            </View>
            {/* <View style={{ alignItems: 'flex-end', alignSelf: 'center', marginRight: Width(4), flex: 4 }}>
            <TouchableOpacity onPress={() => this.modelVisible(item)} underlayColor="transparent" style={{ justifyContent: 'center', height: Height(6), width: Width(6) }}>
              <Entypo size={Width(6)} name="dots-three-horizontal" color="#000" />
            </TouchableOpacity>
          </View> */}
          </View>
          <ImageBackground source={bg} style={{ backgroundColor: '#F7F7F7', height: Height(14), justifyContent: 'center', alignItems: 'center' }}>
            {/* <Text style={{ fontSize: FontSize(20), color: colors.dargrey, fontFamily: fonts.Raleway_Semibold }}>{name[0]}, let's meet up!</Text> */}
            <Text style={{ fontSize: FontSize(20), color: colors.dargrey, fontFamily: fonts.Raleway_Semibold }}>Let's meet up!</Text>
            {item.description == '' ? null : <Text style={{ textAlign: 'center', fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, width: '85%', marginTop: 2 }}>{item.description}</Text>}

          </ImageBackground>
          <View style={{ backgroundColor: '#FFF', alignItems: 'center', flexDirection: 'row' }}>
            <View style={{ flex: 1, marginLeft: Height(2.5) }}>
              <TouchableOpacity
                style={{ flexDirection: 'row' }}>
                {item.is_going_count != 0 || item.is_interested_count != 0 || item.is_invited_count != 0 ?
                  <Image resizeMode="contain" style={{ marginTop: Height(2), alignSelf: 'center', tintColor: colors.whatFontColor }} source={new_user} /> : null}
                {item.is_going_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 0,Title_New:item.activity,plan_type:item.plan_type }) }} style={{ marginTop: Height(2), paddingLeft: Width(2), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.is_going_count + ' going'}</Text> : null}
                {item.is_interested_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 1,Title_New:item.activity,plan_type:item.plan_type }) }} style={item.is_going_count == 0 ? { marginTop: Height(2), paddingLeft: Width(2), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey } : { marginTop: Height(2), paddingLeft: Width(1), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.is_going_count != 0 ? '\u00B7 ' : null}{item.is_interested_count + ' interested'}</Text> : null}
                {item.is_invited_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 2,Title_New:item.activity,plan_type:item.plan_type }) }} style={item.is_going_count == 0 && item.is_interested_count == 0 ? { marginTop: Height(2), paddingLeft: Width(2.5), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey } : { marginTop: Height(2), paddingLeft: Width(1), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.is_interested_count != 0 || item.is_going_count != 0 ? '\u00B7 ' : null}{item.is_invited_count + ' invited'}</Text> : null}

              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginTop: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={chai_cup} />
                {item.activity == '' ? <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>Suggest an activity</Text>
                  : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.activity}</Text>}
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginTop: Height(2), marginBottom: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={calendar} />
                {item.start_date == '' ? <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>Suggest a date & time</Text>
                  : <Text style={{ width: Width(60), paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{Final_date}</Text>}
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginBottom: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center', tintColor: colors.whatFontColor }} source={location} />
                {item.line_1 == '' ? <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>Suggest a location</Text>
                  : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.line_1}</Text>}

              </TouchableOpacity>

              
            </View>

          </View>
          <View style={{ height: Height(1) }} />

        </View> : null}
      {item.plan_type == 2 ?
        <View style={{ backgroundColor: colors.white }}>
          <View style={{ height: Height(9), flexDirection: 'row', alignItems: 'center', backgroundColor: 'white' }}>
            <View style={{ alignItems: 'center', marginLeft: Width(3) }}>
              <View underlayColor="transparent" style={{ height: Height(5), width: Height(5), borderRadius: Height(7), overflow: 'hidden' }}>
                {item.photo == '' ? <Image source={avtar} style={{ height: '100%', width: '100%' }} /> :
                  <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.photo }} />}
              </View>
            </View>
            <View style={{ justifyContent: 'center', marginLeft: Width(3) }}>
              <Text style={{ fontSize: FontSize(18), color: colors.dargrey, fontFamily: fonts.Raleway_Bold }}>{item.login_id == this.state.login_id ? 'Me' : item.name}</Text>
              <Text style={{ fontSize: FontSize(12), color: colors.fontDarkGrey }}>{dateTime}{item.share_with == '' ? null : '\u00B7 '}{item.share_with}</Text>
            </View>
            {/* <View style={{ alignItems: 'flex-end', marginRight: Width(4), flex: 4 }}>

           <TouchableOpacity onPress={() => this.modelVisible(item)} underlayColor="transparent" style={{ justifyContent: 'center', height: Height(6), width: Width(6) }}>
             <Entypo size={Width(6)} name="dots-three-horizontal" color="#000" />
           </TouchableOpacity>
         </View> */}
          </View>
          <ImageBackground source={bg} style={{ backgroundColor: '#F7F7F7', height: Height(14), justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: FontSize(20), color: colors.dargrey, fontFamily: fonts.Raleway_Semibold }}>{item.activity} </Text>
            {item.description == '' ? null : <Text style={{ textAlign: 'center', fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, width: '85%', marginTop: 2 }}>{item.description}</Text>}

          </ImageBackground>
          <View style={{ backgroundColor: '#FFF', alignItems: 'center', flexDirection: 'row' }}>

            <View style={{ flex: 1, marginLeft: Height(2.5) }}>
              <TouchableOpacity
                style={{ flexDirection: 'row' }}>
                {item.is_going_count != 0 || item.is_interested_count != 0 || item.is_invited_count != 0 ?
                  <Image resizeMode="contain" style={{ marginTop: Height(2), alignSelf: 'center', tintColor: colors.whatFontColor }} source={new_user} /> : null}
                {item.is_going_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 0,Title_New:item.activity,plan_type:item.plan_type }) }} style={{ marginTop: Height(2), paddingLeft: Width(2), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.is_going_count + ' going'}</Text> : null}
                {item.is_interested_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 1,Title_New:item.activity,plan_type:item.plan_type }) }} style={item.is_going_count == 0 ? { marginTop: Height(2), paddingLeft: Width(2), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey } : { marginTop: Height(2), paddingLeft: Width(1), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.is_going_count != 0 ? '\u00B7 ' : null}{item.is_interested_count + ' interested'}</Text> : null}
                {item.is_invited_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 2,Title_New:item.activity,plan_type:item.plan_type }) }} style={item.is_going_count == 0 && item.is_interested_count == 0 ? { marginTop: Height(2), paddingLeft: Width(2.5), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey } : { marginTop: Height(2), paddingLeft: Width(1), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.is_interested_count != 0 || item.is_going_count != 0 ? '\u00B7 ' : null}{item.is_invited_count + ' invited'}</Text> : null}

              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginTop: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center', tintColor: colors.whatFontColor }} source={chai_cup} />
                {item.activity == '' ? <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}></Text>
                  : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{activity}</Text>}
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginTop: Height(2), marginBottom: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={calendar} />
                {item.start_date == '' ? <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>Suggest a date & time</Text>
                  : <Text style={{ width: Width(60), paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{Final_date}</Text>}
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginBottom: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center', tintColor: colors.whatFontColor }} source={location} />
                {item.line_1 == '' ? <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>Suggest a location</Text>
                  : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.line_1}</Text>}
              </TouchableOpacity>
              
            </View>

          </View>
          <View style={{ height: Height(1) }} />

        </View> : null}
      {item.plan_type == 3 ?
        <View style={{ backgroundColor: colors.white }}>
          <View style={{ height: Height(9), flexDirection: 'row', alignItems: 'center', backgroundColor: 'white' }}>
            <View style={{ alignItems: 'center', marginLeft: Width(3) }}>
              <View
                underlayColor="transparent" style={{ height: Height(5), width: Height(5), borderRadius: Height(7), overflow: 'hidden' }}>
                {item.photo == '' ? <Image source={avtar} style={{ height: '100%', width: '100%' }} /> :
                  <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.photo }} />}
              </View>
            </View>
            <View style={{ justifyContent: 'center', marginLeft: Width(3) }}>
              <Text style={{ fontSize: FontSize(18), color: colors.dargrey, fontFamily: fonts.Raleway_Bold }}>{item.login_id == this.state.login_id ? 'Me' : item.name}</Text>
              <Text style={{ fontSize: FontSize(12), color: colors.fontDarkGrey, fontFamily: fonts.Roboto_Regular }}>{dateTime}{item.share_with == '' ? null : '\u00B7 '}{item.share_with}</Text>
            </View>
            {/* <View style={{ alignItems: 'flex-end', marginRight: Width(4), flex: 4 }}>
             <TouchableOpacity onPress={() => this.modelVisible(item)} underlayColor="transparent" style={{ justifyContent: 'center', height: Height(6), width: Width(6) }}>
               <Entypo size={Width(6)} name="dots-three-horizontal" color="#000" />
             </TouchableOpacity>
           </View> */}
          </View>
          <ImageBackground source={bg} style={{ backgroundColor: '#F7F7F7', height: Height(14), justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ textAlign: 'center', fontSize: FontSize(20), color: colors.dargrey, fontFamily: fonts.Raleway_Semibold }}>Wanna go to {item.line_1} ?</Text>
            {item.description == '' ? null : <Text style={{ textAlign: 'center', fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, width: '85%', marginTop: 2 }}>{item.description}</Text>}

          </ImageBackground>
          <View style={{ backgroundColor: '#FFF', alignItems: 'center', flexDirection: 'row' }}>

            <View style={{ flex: 1, marginLeft: Height(2.5) }}>
              <TouchableOpacity
                style={{ flexDirection: 'row' }}>
                {item.is_going_count != 0 || item.is_interested_count != 0 || item.is_invited_count != 0 ?
                  <Image resizeMode="contain" style={{ marginTop: Height(2), alignSelf: 'center', tintColor: colors.whatFontColor }} source={new_user} /> : null}
                {item.is_going_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 0,Title_New:item.line_1,plan_type:item.plan_type }) }} style={{ marginTop: Height(2), paddingLeft: Width(2), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.is_going_count + ' going'}</Text> : null}
                {item.is_interested_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 1,Title_New:item.line_1,plan_type:item.plan_type }) }} style={item.is_going_count == 0 ? { marginTop: Height(2), paddingLeft: Width(2), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey } : { marginTop: Height(2), paddingLeft: Width(1), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.is_going_count != 0 ? '\u00B7 ' : null}{item.is_interested_count + ' interested'}</Text> : null}
                {item.is_invited_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 2,Title_New:item.line_1,plan_type:item.plan_type }) }} style={item.is_going_count == 0 && item.is_interested_count == 0 ? { marginTop: Height(2), paddingLeft: Width(2.5), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey } : { marginTop: Height(2), paddingLeft: Width(1), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.is_interested_count != 0 || item.is_going_count != 0 ? '\u00B7 ' : null}{item.is_invited_count + ' invited'}</Text> : null}

              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginTop: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center', tintColor: colors.whatFontColor }} source={chai_cup} />
                {item.activity == '' ? <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>Suggest an activity</Text>
                  : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.activity}</Text>}
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginTop: Height(2), marginBottom: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={calendar} />
                {item.start_date == '' ? <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>Suggest a date & time</Text>
                  : <Text style={{ width: Width(60), paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{Final_date}</Text>}
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginBottom: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center', tintColor: colors.whatFontColor }} source={location} />
                {item.line_1 == '' ? <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>Suggest a location</Text>
                  : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.line_1}</Text>}
              </TouchableOpacity>
            </View>

          </View>
          <View style={{ height: Height(1) }} />

        </View> : null}
      {item.plan_type == 4 ?
        <View style={{ backgroundColor: colors.white }}>
          <View style={{ height: Height(9), flexDirection: 'row', alignItems: 'center', backgroundColor: 'white' }}>
            <View style={{ alignItems: 'center', marginLeft: Width(3) }}>
              <View underlayColor="transparent" style={{ height: Height(5), width: Height(5), borderRadius: Height(7), overflow: 'hidden' }}>
                {item.photo == '' ? <Image source={avtar} style={{ height: '100%', width: '100%' }} /> :
                  <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.photo }} />}
              </View>
            </View>
            <View style={{ justifyContent: 'center', marginLeft: Width(3) }}>
              <Text
                style={{ fontSize: FontSize(18), color: colors.dargrey, fontFamily: fonts.Raleway_Bold }}>{item.login_id == this.state.login_id ? 'Me' : item.name}</Text>
              <Text style={{ fontSize: FontSize(12), color: colors.fontDarkGrey }}>{dateTime}{item.share_with == '' ? null : '\u00B7 '}{item.share_with}</Text>
            </View>
            {/* <View style={{ alignItems: 'flex-end', marginRight: Width(4), flex: 4 }}>
           <TouchableOpacity onPress={() => this.modelVisible(item)} underlayColor="transparent" style={{ justifyContent: 'center', height: Height(6), width: Width(6) }}>
             <Entypo size={Width(6)} name="dots-three-horizontal" color="#000" />
           </TouchableOpacity>
         </View> */}
          </View>
          <ImageBackground source={bg} style={{ backgroundColor: '#F7F7F7', height: Height(14), justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: FontSize(20), color: colors.dargrey, fontFamily: fonts.Raleway_Semibold }}>I'm free, wanna hangout?</Text>
            {item.description == '' ? null : <Text style={{ textAlign: 'center', fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, width: '85%', marginTop: 2 }}>{item.description}</Text>}

          </ImageBackground>
          <View style={{ backgroundColor: '#FFF', alignItems: 'center', flexDirection: 'row' }}>

            <View style={{ flex: 1, marginLeft: Height(2.5) }}>
              <TouchableOpacity
                style={{ flexDirection: 'row' }}>
                {item.is_going_count != 0 || item.is_interested_count != 0 || item.is_invited_count != 0 ?
                  <Image resizeMode="contain" style={{ marginTop: Height(2), alignSelf: 'center', tintColor: colors.whatFontColor }} source={new_user} /> : null}
                {item.is_going_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 0,Title_New:item.activity,plan_type:item.plan_type }) }} style={{ marginTop: Height(2), paddingLeft: Width(2), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.is_going_count + ' going'}</Text> : null}
                {item.is_interested_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 1,Title_New:item.activity,plan_type:item.plan_type }) }} style={item.is_going_count == 0 ? { marginTop: Height(2), paddingLeft: Width(2), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey } : { marginTop: Height(2), paddingLeft: Width(1), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.is_going_count != 0 ? '\u00B7 ' : null}{item.is_interested_count + ' interested'}</Text> : null}
                {item.is_invited_count != 0 ? <Text onPress={() => { this.props.navigation.navigate('Friend_Status', { plan_id: item.plan_id, tabIndex: 2,Title_New:item.activity,plan_type:item.plan_type }) }} style={item.is_going_count == 0 && item.is_interested_count == 0 ? { marginTop: Height(2), paddingLeft: Width(2.5), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey } : { marginTop: Height(2), paddingLeft: Width(1), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.is_interested_count != 0 || item.is_going_count != 0 ? '\u00B7 ' : null}{item.is_invited_count + ' invited'}</Text> : null}

              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginTop: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={chai_cup} />
                {item.activity == '' ? <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>Suggest an activity</Text>
                  : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.activity}</Text>}
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginTop: Height(2), marginBottom: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={calendar} />
                {item.start_date == '' ? <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>Suggest a date & time</Text>
                  : <Text style={{ width: Width(60), paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{Final_date}</Text>}
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: 'row', marginBottom: Height(2) }}>
                <Image resizeMode="contain" style={{ alignSelf: 'center', tintColor: colors.whatFontColor }} source={location} />
                {item.line_1 == '' ? <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: '#358AF6' }}>Suggest a location</Text>
                  : <Text style={{ paddingLeft: Width(3.3), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{item.line_1}</Text>}
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ height: Height(1) }} />

        </View> : null}

    </View>
    )
  }
  renderSeparator = () => {
    return (
      <View
        style={{
          height: 5,
          width: '100%',
          backgroundColor: colors.lightgrey,
        }}
      />
    );
  };

  gotoUnfriend(){

    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('other_id', this.state.other_id)

    //alert(JSON.stringify(data))
    //return
    fetch(API_ROOT + 'friend_unfriend', {
      method: 'post', body: data
    })
      .then((response) => response.json())
      .then((responseData) => {

        //alert(JSON.stringify(responseData))

        if (responseData.success) {
          this.getUserData()
          this.setState({friendModal:false})
          //this.props.navigation.goBack()
        } else {
          this.setState({friendModal:false})
          alert(responseData.text)
        }
      })
      .catch((error) => { 
        this.setState({friendModal:false})
        //alert(error) 
      })

  }


  gotoBlock(){

    this.setState({friendModal:false,deleteModal:false})

    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('other_id', this.state.other_id)
    data.append('is_block', '1')

    fetch(API_ROOT + 'block/user', {
      method: 'post', body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          this.getUserData()
          //this.props.navigation.goBack()
        } else {
          alert(responseData.text)
        }
      })
      .catch((error) => { 
        //alert(error) 
      })
  }

  goToFriendProfile(item)
  {
    
    if(this.state.login_id != item.receiver_id)
    {
      AsyncStorage.setItem('other_id', item.receiver_id)
      this.props.navigation.navigate('ohter_user_profile')
    }

  }

  renderFriendModal() {
    return (
       
        <Modal
          //animationType="slide"
          transparent={true}
          visible={this.state.friendModal}
          onRequestClose={() => {
            this.setFriendModalVisible(!this.state.friendModal);
          }}>
          <TouchableHighlight style = {{backgroundColor: 'rgba(0, 0, 0, 0.5)', flex:1}} onPress = {() => this.setFriendModalVisible(!this.state.friendModal)}>
          <View style={this.state.UserDetail.is_friend == 1 ? { height: Height(23), width: Width(97), alignSelf: 'center', backgroundColor: 'white', bottom: Height(11), position: 'absolute', borderRadius: 13 } : { height: Height(10), width: Width(90), alignSelf: 'center', backgroundColor: 'white', bottom: Height(10), position: 'absolute', borderRadius: 10 }}>
            {this.state.UserDetail.is_friend == 1 ? 
                <View style = {{width:'100%',height:Height(7)}}>
                <TouchableOpacity
                 onPress={() => {

                  Alert.alert(
                    'Block ' + this.state.UserDetail.name+'?',
                    "They won't be able to see your profile. They won't be notified that you blocked them.",
                    [
                      {
                        text: 'Cancel',
                        onPress: () => {this.setState({friendModal:false,deleteModal:false})},
                        style: 'cancel',
                      },
                      { text: 'OK', onPress: this.gotoBlock},
                    ],
                    { cancelable: false },
                  );

              }}
                  style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ fontSize: FontSize(20), color: colors.blue }}>Block</Text>
                </TouchableOpacity>
                <View style={{ height: 1, width: '100%', backgroundColor: '#ccc' }} />
                </View>
            : null}
            {this.state.UserDetail.is_friend == 1 ?
            <View style = {{width:'100%',height:Height(7)}}>
            <TouchableOpacity onPress={()=>{
            //this.setState({friendModal:false})
            Alert.alert(
              unfriendmsg,
              '',
              [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel',
                },
                { text: 'OK', onPress: () => this.gotoUnfriend()},
              ],
              { cancelable: false },
            );
            
             //this.gotoUnfriend
            }
             } style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
             <Text style={{ fontSize: FontSize(20), color: colors.blue }}>Unfriend</Text>
            </TouchableOpacity>
            <View style={{ height: 1, width: '100%', backgroundColor: '#ccc' }} />
            </View>
            : null}
            
            <TouchableOpacity onPress={()=>{
              this.setState({friendModal:false})
              this.props.navigation.navigate('ReportPerson',{login_id:this.state.login_id,other_id:this.state.other_id})
            }} style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ fontSize: FontSize(20), color: colors.blue }}>Report this person</Text>
            </TouchableOpacity>
          </View>
          </TouchableHighlight>
          <View style={{ height: Height(7), width: Width(97), alignSelf: 'center', backgroundColor: '#fff', bottom: 20, position: 'absolute', borderRadius: 13}}>
            <TouchableOpacity
              onPress={() => {
                this.setFriendModalVisible(!this.state.friendModal);
              }}
              style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ fontSize: FontSize(20), color: colors.blue, fontFamily: fonts.Roboto_Bold }}>Cancel</Text>
            </TouchableOpacity>
          </View>
          
        </Modal>
      
    )
  }

  renderPhotoModal()
  {
    return (
      <Modal visible = {this.state.isProfilePicVisible1} style = {{margin:0, backgroundColor:'#fff'}} animationType="slide">
              <View style={styles.headerAndroidnav}>
                <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
                <View style={{ flexDirection: 'row', bottom: 10, position: 'absolute', left: Width(5), width:35,height:35 }}>
                    <TouchableOpacity onPress={() => { this.setState({isProfilePicVisible1:false}) }} style = {{width:35,height:35,justifyContent:'center'}}>
                        <Image source={closeWhite} style={{ tintColor: "white" }} />
                    </TouchableOpacity></View>
                <View style={{ bottom: 10, position: 'absolute', alignItems: 'center',alignSelf:'center' }}>
                  <Text style={styles.headerTitle} >{'Profile Photo'}</Text>
                </View>
                {/* <TouchableOpacity style={{ bottom: Height(1.5),right:10, position: 'absolute', alignItems: 'center' }} onPress = { this.is_photo_selected1 ? () => this.onSaveProfilePic1() : () => this.onEditProfilePic1()}>
                    <Text style={styles.headerTitle} >{this.is_photo_selected1 == true ? 'Save' : 'Edit'}</Text>
                </TouchableOpacity> */}
            </View>
            <View style = {{flex:1, alignItems:'center', justifyContent:'center'}}>
                <Image style={{ height: '50%', width: '100%'}} source={{ uri: this.state.profile}} resizeMode = 'cover'/>
            </View>
        </Modal>
    )
  }

  renderDelete() {
    return (
      //<View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.deleteModal}
          onRequestClose={() => {
            this.setDeleteModalVisible(!this.state.deleteModal);
          }}>
          <TouchableHighlight style = {{backgroundColor: 'rgba(0, 0, 0, 0.5)', flex:1}} onPress = { () => this.setDeleteModalVisible(!this.state.deleteModal)}>
          <View style={{ marginTop:Height(20),height: Height(28), borderRadius: 10, width: '80%', backgroundColor: '#fff', alignSelf:'center',alignItems:'center',justifyContent:'center'}}>

            <Text style={{ marginTop: Height(2), width: Width(65), alignSelf: 'center', fontFamily: fonts.Raleway_Semibold, fontSize: FontSize(17), color: colors.black, textAlign: 'center' }}>Block Leaticia Ehui?</Text>
            <Text style={{ marginTop: Height(2), width: Width(65), alignSelf: 'center', fontFamily: fonts.Raleway_Regular, fontSize: FontSize(13), color: colors.black, textAlign: 'center' }}>They won't be able to see your profile. They won't be notified that you blocked them.</Text>

            <View style={{ height: Height(0.1), width: '100%', backgroundColor: colors.black, marginTop: Height(7) }} />
            <View style={{ justifyContent: 'center', flexDirection: 'row', alignItems: 'center' }}>
              <TouchableOpacity onPress={() => this.setState({ deleteModal: false })} style={{ width: '49%', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontFamily: fonts.Overpass_Regular, fontSize: FontSize(17), color: colors.blue }}>Cancel</Text>
              </TouchableOpacity>
              <View style={{ width: Width(0.3), height: Height(8), backgroundColor: colors.black }} />

              <TouchableOpacity onPress={() => {
                this.setState({ deleteModal: false })
                this.gotoBlock()}}
                 style={{ width: '49%', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontFamily: fonts.Overpass_Regular, fontSize: FontSize(17), color: colors.blue }}>OK</Text>
              </TouchableOpacity>


            </View>
          </View>
        </TouchableHighlight>
        </Modal>
      //</View>
    )
  }

  onClickSelectPhoto = () => {
    //this.props.navigation.navigate('ProfilePicView',{UserDetail: this.state.UserDetail,is_my_photo:false})
    this.setState({isProfilePicVisible1:true})
  }


  render() {
    let { isModalVisible } = this.state
    const date = this.state.bod
    const styles1 = this.makeStyles()

    return (
      <View style={{ flex: 1 }}>
        <View style = {{height:this.state.UserDetail.is_friend == 1 ? 350 : 290, width:'100%', backgroundColor:colors.appColor,position:'absolute',zIndex:-1}}></View>
        <ScrollView scrollEnabled={this.state.UserDetail.is_friend == true ? true : false} showsVerticalScrollIndicator = {false}>
          <View>
            <View style={{ backgroundColor: colors.appColor }}>
              <ImageBackground style={{}} source={BG_images}>
                <ImageBackground
                  source={profile_border}
                  style={{ marginTop: Height(2), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', height: Height(12), width: Height(12), overflow: 'hidden' }}>
                  <TouchableOpacity activeOpacity = {0.9} style={{ alignSelf: 'center', height: Height(9), width: Height(9), borderRadius: Height(9), backgroundColor: '#fff', borderColor: '#FFF', borderWidth: Width(0.5), overflow: 'hidden' }} onPress = {() => this.onClickSelectPhoto()}>
                    <Image style={{ height: '100%', width: '100%' }} source={{ uri: this.state.profile }} />
                  </TouchableOpacity>
                </ImageBackground>
                <Text style={{ color: '#FFF', marginTop: Height(2), alignSelf: 'center', fontSize: FontSize(18), fontFamily: fonts.Raleway_Semibold }}>{this.state.fullname}</Text>

                {this.state.UserDetail.is_block == 1 && this.is_api_called == true ?
                    <View style={{ alignSelf: 'center', marginTop: Height(2), height: Height(5), width: Width(40), borderRadius: Width(7), borderColor: '#FFF', borderWidth: Width(0.5), justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                      onPress={() => {
                        //this.setFriendModalVisible(!this.state.friendModal);
                        this.unBlockUserAPICall()
                      }}
                      style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                      <Text style={{ color: '#FFF', fontSize: FontSize(16), fontFamily: fonts.Raleway_Medium }}>Unblock</Text>
                    </TouchableOpacity>
                  </View>
                :
               this.is_api_called == true && <View style= {this.state.UserDetail.is_friend == 2 ? { alignSelf: 'center', marginTop: Height(2), height: Height(4.5), width: Width(40), borderRadius: Width(7), borderColor: '#FFF',backgroundColor:'#fff', borderWidth: Width(0.5), justifyContent: 'center', alignItems: 'center'} : { alignSelf: 'center', marginTop: Height(2), height: Height(4.5), width: Width(40), borderRadius: Width(7), borderColor: '#FFF', borderWidth: Width(0.5), justifyContent: 'center', alignItems: 'center'}}>
                <TouchableOpacity
                  onPress={() => {

                    if (this.state.UserDetail.is_friend == 1)
                    {
                      this.setFriendModalVisible(!this.state.friendModal);
                    }
                    else if (this.state.UserDetail.is_friend == 2)
                    {
                        this.cancelFriendRequestAPICall(this.state.UserDetail.login_id)
                    }
                    else
                    {
                        this.addFriendAPICall()
                    }
                  }}
                  style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>

                  {this.state.UserDetail.is_friend == 1 ?
                  <View style = {{flexDirection:'row', alignItems:'center'}}>
                  <Text style={{ color: '#FFF', fontSize: FontSize(16), fontFamily: fonts.Raleway_Medium }}>Friends</Text>
                  <EvilIcons style={{ marginLeft:-4,alignItems:'flex-start'}} name='chevron-down' color='#fff' size={25} />
                  </View>
                  :
                  <View style = {{flexDirection:'row', alignItems:'center'}}>
                   {this.state.UserDetail.is_friend == 2 ? null : <Ionicons style={{ marginLeft: '3%', }} name='ios-add' color='#fff' size={30} />} 
                  {this.state.UserDetail.is_friend == 2 ? <Text style={{ color: '#41199B', fontSize: FontSize(16), fontFamily: fonts.Raleway_Medium }}> Cancel Request</Text> : <Text style={{ color: '#FFF', fontSize: FontSize(16), fontFamily: fonts.Raleway_Medium }}> Add Friend</Text>}
                  </View>
                }
                </TouchableOpacity>
              </View>
                }
                
                <Text style={{ textAlign: 'center', marginTop: Height(2), marginBottom: Height(2.5), color: '#FFF', fontSize: FontSize(14), fontFamily: fonts.Roboto_Regular, alignSelf: 'center', width: '85%', lineHeight: 20 }}>{this.state.bio}</Text>

              </ImageBackground>
            </View>
          
          {this.state.UserDetail.is_friend == 1 ? 
          
          <View style={{ backgroundColor: '#F2F4F7' }}>
              <View style={isIphoneX ? { flex: 1, marginLeft: Height(2), marginTop: Height(0.5) } : { flex: 1, marginLeft: Height(2) }}>
                
                {
                  this.state.city != '' ?
                  <View style={{ flexDirection: 'row', marginTop: Height(3) }}>
                  <Image resizeMode="contain" style={{ alignSelf: 'center', tintColor: colors.whatFontColor }} source={location} />
                  <Text style={{ marginLeft: 5, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>Currently in {this.state.city}</Text>
                </View>
                  : null
                }
                {
                  this.state.bod != '' ?
                  <View style={{ flexDirection: 'row', marginTop: Height(2) }}>
                  <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={calendar} />
                  <Text style={{ marginLeft: 5, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>{this.state.bod != '' ? moment(this.state.bod).format('MMM D') : ''}</Text>
                  </View>
                  : null
                }
                
                <View style={{ flexDirection: 'row', marginTop: Height(2), marginBottom: Height(3) }}>
                  <Image resizeMode="contain" style={{ alignSelf: 'center' }} source={user} />
                  {this.state.friends == '0' ? <Text style={{ paddingLeft: Width(2), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>Friends with 0 people</Text> :
                    <View>{this.state.friends == '1' ? <Text style={{ paddingLeft: Width(2), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>Friends with {this.state.friends} person</Text>
                      : <Text style={{ marginLeft: 5, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(15), color: colors.dargrey }}>Friends with {this.state.friends} people</Text>}
                    </View>}
                </View>
              </View>
              <View style={{ height: Height(0.1), width: '92%', backgroundColor: '#B4B7BA', alignSelf: 'center' }} />
              <Text style={{ marginLeft: Width(4), fontFamily: fonts.Raleway_Bold, fontSize: FontSize(18), marginTop: Height(2.5), color: '#4B467A' }}>{this.state.UserDetail.is_friend == 1 ? 'Suggest a plan with '+this.state.UserDetail.name : 'Suggest a plan'}</Text>
              <View style={{ marginTop: Height(3), flexDirection: 'row', justifyContent: 'space-between', marginLeft: Width(4), marginRight: Width(5) }}>
                <TouchableOpacity
                  onPress={() => {
                    AsyncStorage.setItem('what_friends', JSON.stringify(this.state.UserDetail.login_id))
                    AsyncStorage.setItem('what_friends_name', JSON.stringify(this.state.UserDetail.name))
                    this.props.navigation.navigate('what1',{is_edit_plan:false,is_from_profile:true})}}
                  style={{
                    shadowColor: "#000",
                    shadowOffset: {
                      width: 0,
                      height: 2,
                    },
                    shadowOpacity: 0.23,
                    shadowRadius: 2.62,
                    elevation: 4
                  }}>
                  <View style={{ backgroundColor: '#FFF', borderRadius: Width(4), height: Height(13), width: Height(13), justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={pink_chai_cup} resizeMode="contain" />
                  </View>
                  <Text style={{ color: colors.dargrey, alignSelf: 'center', textAlign: 'center', marginTop: Height(1), width: Height(13), fontSize: FontSize(14), lineHeight: 20, fontFamily: fonts.Roboto_Regular }}>
                    What do you wanna do?
                </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    AsyncStorage.setItem('where_friends', JSON.stringify(this.state.UserDetail.login_id))
                    AsyncStorage.setItem('where_friends_name', JSON.stringify(this.state.UserDetail.name))
                    this.props.navigation.navigate('where1', {is_edit_plan:false, is_from_profile:true})}}
                  style={{
                    shadowColor: "#000",
                    shadowOffset: {
                      width: 0,
                      height: 2,
                    },
                    shadowOpacity: 0.23,
                    shadowRadius: 2.62,
                    elevation: 4
                  }}>
                  <View style={{ backgroundColor: '#FFF', borderRadius: Width(4), height: Height(13), width: Height(13), justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={pink_location} resizeMode="contain" />
                  </View>
                  <Text style={{ color: colors.dargrey, alignSelf: 'center', lineHeight: 20, textAlign: 'center', marginTop: Height(1), width: Height(13), fontSize: FontSize(14), fontFamily: fonts.Roboto_Regular }}>
                    Where do you wanna go?
                </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    
                    AsyncStorage.setItem('when_friends', JSON.stringify(this.state.UserDetail.login_id))
                    AsyncStorage.setItem('when_friends_name', JSON.stringify(this.state.UserDetail.name))
                    this.props.navigation.navigate('when1', {is_edit_plan:false, is_from_profile:true})}}

                  style={{
                    shadowColor: "#000",
                    shadowOffset: {
                      width: 0,
                      height: 2,
                    },
                    shadowOpacity: 0.23,
                    shadowRadius: 2.62,
                    elevation: 4
                  }}>
                  <View style={{ backgroundColor: '#FFF', borderRadius: Width(4), height: Height(13), width: Height(13), justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={pink_clock} resizeMode="contain" />
                  </View>
                  <Text style={{ color: colors.dargrey, alignSelf: 'center', lineHeight: 20, textAlign: 'center', marginTop: Height(1), width: Height(13), fontSize: FontSize(14), fontFamily: fonts.Roboto_Regular }}>
                    When are you free?
                </Text>
                </TouchableOpacity>
              </View>
              <View style={{ height: Height(2.5) }} />

            </View>
          : 
            this.is_api_called == true ?
            <View style = {{backgroundColor:'transparent', height:Height(30), alignItems:'center',marginTop:10}}>
              <Image source={private_lock} resizeMode="contain" style={{marginTop:45}}/>
              <Text style = {{color:'#000',fontFamily:fonts.Raleway_Bold, fontSize:FontSize(16), marginTop:30,textAlign:'center', fontWeight:'800'}}>This account is private</Text>
              <Text style = {{color:'#3A4759',fontFamily:fonts.Roboto_Regular, fontSize:FontSize(15), marginTop:12,textAlign:'center'}}>Send a friend request to make plans</Text>
              <Text style = {{color:'#3A4759',fontFamily:fonts.Roboto_Regular, fontSize:FontSize(15), marginTop:3,textAlign:'center'}}>and see their interests.</Text>
            </View>
            : null
          }
          </View>
          {this.state.UserDetail.is_friend == 1 ? 
          <View>
          <View style={{ backgroundColor: '#fff' }}>
            <View style={{ marginTop: Height(4), flexDirection: 'row' }}>
              <Text style={{ flex: 1, marginLeft: Width(4), fontFamily: fonts.Raleway_Bold, fontSize: FontSize(18), color: '#4B467A' }}>Friends {'\u00B7'} {this.state.friendCount}</Text>
              <Text onPress={() => { this.props.navigation.navigate('FriendsfriendList') }} style={{ flex: 1, marginRight: Width(4), alignSelf: 'flex-end', textAlign: 'right', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(13), color: '#358AF6' }}>See All</Text>
            </View>
            <ScrollView showsHorizontalScrollIndicator={false} horizontal contentContainerStyle={{ marginTop: Height(2) }}>
              {this.state.friendList.map(item => {
                return (
                  <View>
                    <TouchableHighlight underlayColor="transparent" style={{ marginHorizontal: Width(3.5), height: Height(6), width: Height(6), borderRadius: Height(7), backgroundColor: '#EEEEEE', overflow: 'hidden', alignSelf: 'center' }} onPress = {() => this.goToFriendProfile(item)}>
                      {item.receiver_photo == '' ? <Image source={flowers} style={{ height: '100%', width: '100%' }} /> :
                        <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.receiver_photo }} />}
                    </TouchableHighlight>
                    <Text style={{ marginTop: 8, textAlign: 'center', alignSelf: 'center', fontSize: FontSize(13), color: colors.dargrey, fontFamily: fonts.Raleway_Regular, width: Width(18) }}> {item.receiver_name}</Text>
                    <View style={{ height: Height(2) }} />
                  </View>
                );
              })}
            </ScrollView>
            {(this.state.UserDetail.is_interest != 1  && this.state.interest_list.length != 0 ) ?
            <View style={{ marginTop: Height(2), flexDirection: 'row' }}>
              <Text style={{ flex: 1, marginLeft: Width(4), fontFamily: fonts.Raleway_Bold, fontSize: FontSize(18), color: '#4B467A' }}>Interests</Text>
              <Text onPress={() => { this.props.navigation.navigate('OtherProfileInterest',{subcategories:this.state.interest_list}) }} style={{ flex: 1, marginRight: Width(4), alignSelf: 'flex-end', textAlign: 'right', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(13), color: '#358AF6' }}>See All</Text>
            </View>
              :
                null
              }
            {
              (this.state.UserDetail.is_interest != 1  && this.state.interest_list.length != 0 ) ?
              <View style={{ marginHorizontal: Width(2.7) }}>
              <View style={{ height: Height(2) }} />
              <View style={styles1.view}>
                {this.state.interest_list.map(item => {
                  return (
                    <Text style={styles1.text}>{item}</Text>
                  );
                })}
              </View>
            </View>
              :
              null
            }
            {(this.state.UserDetail.is_interest != 1  && this.state.interest_list.length != 0) ? <View style={{ height: Height(2) }} /> : null}
            {
              (this.state.UserDetail.is_place != 1 && this.state.p_list.length != 0)? 
                // <View style={{ marginTop: Height(2), flexDirection: 'row' }}>
                //   <Text style={{ flex: 1, marginLeft: Width(4), fontFamily: fonts.Raleway_Bold, fontSize: FontSize(18), color: '#4B467A' }}>Places I Like</Text>
                //   <Text onPress={() => { this.props.navigation.navigate('OtherProfilePlaces',{subcategories:this.state.p_list}) }} style={{ flex: 1, marginRight: Width(4), alignSelf: 'flex-end', textAlign: 'right', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(13), color: '#358AF6' }}>See All</Text>
                // </View>
                null
              :
                null
            }
            {
              (this.state.UserDetail.is_place != 1 && this.state.p_list.length != 0) ?

            //   <View style={{ marginHorizontal: Width(2.7) }}>
            //   <View style={{ height: Height(2) }} />
            //   <View style={styles1.view}>
            //     {this.state.p_list.map(item => {
            //       return (
            //         <Text style={styles1.text}>{item}</Text>
            //       );
            //     })}
            //   </View>

            // </View>
              null
              :
              null
            }
            {(this.state.UserDetail.is_place != 1 && this.state.p_list.length != 0) ? <View style={{ height: Height(2) }} /> : null}
          </View>
        </View>
          :null}
          {this.renderFriendModal()}
          {this.renderDelete()}
          {this.renderPhotoModal()}
        </ScrollView>
      </View>
    );
  }
  makeStyles() {
    return StyleSheet.create({
      view: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        backgroundColor: '#fff',
        alignItems: 'center',

      },

      text: {
        fontSize: FontSize(14),
        marginLeft: '1.1%',
        marginTop: '1.1%',
        paddingLeft: '4%',
        paddingRight: '4%',
        paddingTop: '1.8%',
        paddingBottom: '1.8%',
        textAlign: 'center',
        color: '#676868',
        borderRadius: 5,
        borderColor: '#676868',
        borderWidth: 1,
      }
    })
  }
}

export default Profile;
