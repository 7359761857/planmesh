import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, ImageBackground, TouchableOpacity, StatusBar, Text } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Button, Header, Left, Item, Icon, Right, Content, Input } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import { TouchableHighlight } from "react-native-gesture-handler";
const backWhite = require("../../../assets/images/backWhite.png");
const flowers = require("../../../assets/images/Dashboard/avtar.png");
import { API_ROOT } from "../../config/constant";

import { store } from "../../redux/store";

import Toast from 'react-native-simple-toast';
const btnBg = require("../../../assets/images/btnBg.png");

const search_img = require("../../../assets/images/Dashboard/search.png");
const closeicon = require("../../../assets/images/Dashboard/close.png");


class FriendsfriendList extends ValidationComponent {
    _didFocusSubscription
    constructor(props) {
        super(props);
        this.arrayholder = [];
        this.search = null,
        this.data_received = false;
        this.state = {
          token: store.getState().auth.user.token,
          login_id: store.getState().auth.user.login_id,
            Friends: [],
            text: '',
            empty:true,
            is_close_visible:false,

        };
        this._didFocusSubscription = props.navigation.addListener('didFocus', payload => {
            //  AsyncStorage.getItem('friendList').then((value) => {
            //     this.setState({ Friends:JSON.parse(value) })
            //     this.data_received = true
            // });
          }
          )
    }
   

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={[styles.headerAndroidnav,{height:44}]}>
                    <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack()}}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity></View>
                    <View style={{bottom: Height(2), position: 'absolute',alignItems:'center'}}>
                        <Text style={styles.headerTitle} >Friends</Text></View>
                </Header>
            )
        }
    }

    async componentDidMount()
    {
       await AsyncStorage.getItem('friendList').then((value) => {
            this.setState({ Friends:JSON.parse(value) })
            //this.data_received = true
        });
    }

    onClearTextPressed()
    {
        this.setState({is_close_visible:false,text:''})
        setTimeout(()=>{
            this.search.blur()

        },100)
    }

    SearchFilterFunction(text) {

        this.setState({is_close_visible:true,text:text})

        //passing the inserted text in textinput
        // const newData = this.arrayholder.filter((item) => {
        //   //applying filter for the inserted text in search bar
        //   const itemData = item.receiver_name ? item.receiver_name.toUpperCase() : ''.toUpperCase();
        //   const textData = text.toUpperCase();
        //   return itemData.indexOf(textData) > -1;
        // });
        // this.setState({
        //   //setting the filtered newData on datasource
        //   //After setting the data it will automatically re-render the view
        //   Friends: newData,
        //   text: text,
        // });

        if(text == '')
        {
            this.setState({is_close_visible:false})
        }

      }

    render() {

        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor:colors.white }} >
                <Toastt ref="toast"></Toastt>
                <BackgroundImage >
                    <SafeAreaView />
                    <View style={{ height: Height(1), backgroundColor: colors.white }}></View>
                    <View style={{backgroundColor:'#fff'}}>
                    <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', margin: '1%' }}>
                                <Item style={{ backgroundColor: colors.lightgrey, borderColor: colors.white, borderRadius: 22, height: Height(5.5), justifyContent: 'center', width: Width(92.5) }}>
                                    <Image source={search_img} style={{ marginLeft: Width(4.5) }} />
                                    <Input
                                     onChangeText={text => this.SearchFilterFunction(text)}
                                     ref={(search)=>this.search = search}
                                     value={this.state.text}
                                        placeholderTextColor='#8F8F90'
                                        placeholder="Search Friends"
                                        style={{ flex: 1, fontFamily: fonts.Raleway_Medium,paddingHorizontal:5, fontSize: 16, marginLeft: Width(2)}} />
                                    {
                                        this.state.is_close_visible == true ?
                                        <TouchableOpacity style ={{justifyContent:'center',alignItems:'center',height:25,width:25, position:'absolute',right:10}} onPress={ () => this.onClearTextPressed()}>
                                            <Image source={closeicon} resizeMode = 'stretch' style = {{height:17,width:17}}/>
                                        </TouchableOpacity>
                                        :
                                        null
                                    }
                                </Item>
                                    
                            </View>
                            </View>
                    <Content>
                        <View style={{ backgroundColor: '#fff'}}>                          

                            <View style={{ height: 1.3, backgroundColor: colors.lightgrey, marginTop: Height(1) }}></View>
                            <View style={{ height: Height(1.5) }} />
                            {this.state.empty == false ?<View style={{flex:1,justifyContent:'center',alignItems:'center'}}> 
                                <Text style={{marginTop:Height(30),fontSize:16,fontFamily:fonts.Raleway_Medium,color:colors.fontDarkGrey}}>No Friends!</Text>
                            </View>:
                            <View style={{ marginBottom: Height(5) }}>
                                {this.state.Friends.map(item => {

                                    if(item.receiver_name.includes(this.state.text))
                                    {
                                        return (
                                            <View style={{}}>
                                                <TouchableOpacity   onPress={()=>{
                                                    AsyncStorage.setItem('other_id',item.receiver_id)
                                                    this.props.navigation.navigate('ohter_user_profile', { refreshing: true })}}  style={{ height: Height(6), flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white }}>
                                                    <View style={{ width: Width(15), alignItems: 'center', marginLeft:Width(1)}}>
                                                        <TouchableHighlight style={{height: Height(4.5), width: Height(4.5), borderRadius: Height(4.5), overflow: 'hidden' }}>
                                                        <Image style={{ height: '100%', width: '100%' }} source={item.receiver_photo == '' ? flowers:{ uri:item.receiver_photo }} />
                                                        </TouchableHighlight>
                                                    </View>
                                                    <View style={{ width: Width(20),justifyContent: 'center', flex: 4.5 }}>
                                                        <Text style={{ fontSize: FontSize(16), color: colors.dargrey, fontFamily: fonts.Roboto_Medium }}>{item.receiver_name}</Text>
                                                    </View>
                                                   
                                                </TouchableOpacity>
                                                <View style={{ height: Height(.5) }} />
    
                                            </View>
                                        );
                                    }
                                    else
                                    {
                                        return null
                                    }
                                    
                                })}
                            </View>}
                        </View>
                    </Content>
                    {this.state.Friends == '' ?
                     <TouchableOpacity style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }}onPress={() => this.props.navigation.push('FindFriends_Setting')} >
                     <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                       <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Find your friends</Text>
                     </ImageBackground>
                   </TouchableOpacity>
                    :null}
                                
                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default FriendsfriendList;