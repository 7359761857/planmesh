import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Alert, ImageBackground, TouchableOpacity, StatusBar, Text, Platform, TextInput } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Button, Header, Left, Right, Content, Item } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { getCategoryList, AddCategoryList } from "../../redux/actions/auth";
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
const unSelectRadio = require("../../../assets/images/Dashboard/unSelectRadio.png");
const SelectRadio = require("../../../assets/images/selectRadio.png");
import SplashScreen from "react-native-splash-screen";
import RNProgressHUB from 'react-native-progresshub';

import NavigationService from "../../services/NavigationService";
import { store } from "../../redux/store";
import { LOGOUT } from "../../redux/actions/types";
const btnBgGrey = require("../../../assets/images/btnBg.png");
const avtar = require("../../../assets/images/Dashboard/avtar.png");

const backWhite = require("../../../assets/images/backWhite.png");

var subThis;
class ProfileCategories extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {
      labelStylePassword: styles.labelInputUnFocus, stylePassword: styles.formInputUnFocus, password: "", passwordValidation: true, passwordValidationMsg: "",
      labelStyleEmail: styles.labelInputUnFocus, styleEmail: styles.formInputUnFocus, email: "", emailValidation: true, emailValidationMsg: "",
      btnDoneDisable: true, btnDoneImage: btnBgGrey, wsError: false,
      CategoryList: [],
      token: store.getState().auth.user.token,
      login_id: store.getState().auth.user.login_id,
      refreshing:true
    };
    subThis = this
  }
  componentDidMount() {
    SplashScreen.hide()

    RNProgressHUB.showSpinIndeterminate();

    let data = {
      token: this.state.token,
      login_id: this.state.login_id,
      limit: this.state.limit,
      offset: this.state.offset
    }
    this.setState({refreshing:true})
    getCategoryList(data).then(res => {
      if (res.success) {

        this.setState({ CategoryList: res.data,refreshing:false })
        RNProgressHUB.dismiss();
      }
      else {
        this.setState({ refreshing: false })
        RNProgressHUB.dismiss();

      }
    }).catch(err => {
      this.setState({ refreshing: false })

      RNProgressHUB.dismiss();

    })

  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
      header: (
        <Header style={[styles.headerAndroidnav,{height:44}]}>
          <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
          <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
            <TouchableOpacity onPress={() => { navigation.goBack() }}>
              <Image source={backWhite} style={{ tintColor: "white" }} />
            </TouchableOpacity></View>
          <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
            <Text style={styles.headerTitle} >Interests</Text></View>
        </Header>
      )
    }
  }
  onLogoutClick = () => {
    store.dispatch({ type: LOGOUT })
    AsyncStorage.clear()
    NavigationService.reset('Home')
  }

  gotoNext = () => {
    
    var all_id = []
    var selectall = this.state.CategoryList
    selectall.map((item) => {
      all_id.push(item.category_id)
    })
    let data = {
      token: this.state.token,
      login_id: this.state.login_id,
      category_id:String(all_id)
    }

    console.log('CATEGORy IDS: '+data.category_id)

    AddCategoryList(data).then(res => {
      if (res.success) {
       this.props.navigation.navigate('ProfileInterest', { Category_id:all_id,category_name:'All'})
      }
      else {
        this.setState({ empty: false })

      }
    }).catch(err => {
      alert(err)
    })

  }
  goAdd = (item) => {

   // this.props.navigation.navigate('ProfileInterest', { Category_id: item.category_id,category_name:item.category_name})

   let data = {
      token: this.state.token,
      login_id: this.state.login_id,
      category_id:item.category_id
    }

    console.log('CATEGORy IDS: '+data.category_id)

    AddCategoryList(data).then(res => {
      if (res.success) {
       this.props.navigation.navigate('ProfileInterest', { Category_id: item.category_id,category_name:item.category_name})
      }
      else {
        this.setState({ empty: false })
      }
    }).catch(err => {
      alert(err)

    })
  }

  render() {
    //   alert(this.props.navigation.state.params.category)
    let { isModalVisible } = this.state
    let { navigation } = this.props
    return (
      <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#FFF' }} >
        <Toastt ref="toast"></Toastt>
        <BackgroundImage >
          <SafeAreaView />


          <Content>
            {this.state.refreshing == false ?
            <View>
            <TouchableOpacity onPress={this.gotoNext} style={{ marginHorizontal: Width(4), marginVertical: Height(1.5), backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
              {/* <View style={{ height: Height(4), width: Height(4), borderRadius: Height(4), borderWidth: 1, borderColor: '#95989A' }} /> */}
              <Text style={{ marginLeft: Width(3), color: '#8F8F90', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>All</Text>
              <AntDesign style={{ position: 'absolute', right: Width(0) }} name="right" color="#C7C7CC" size={15} />
            </TouchableOpacity>
            <View style={{ borderBottomColor: '#CCC', borderBottomWidth: Width(0.3) }} />
            </View>:null}
            {this.state.CategoryList.map((item) => {
              return (
                <View>
                  <TouchableOpacity onPress={() => this.goAdd(item)} style={{ marginHorizontal: Width(4), marginVertical: Height(1.5), backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center' }}>
                    {/* <View style={{ height: Height(4), width: Height(4), borderRadius: Height(4), borderWidth: 1, borderColor: '#95989A' }} /> */}
                    <Text style={{ marginLeft: Width(3), color: '#8F8F90', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>{item.category_name}</Text>
                    <AntDesign style={{ position: 'absolute', right: Width(0) }} name="right" color="#C7C7CC" size={15} />
                  </TouchableOpacity>
                  <View style={{ borderBottomColor: '#CCC', borderBottomWidth: Width(0.3) }} />
                </View>
              )
            })}
        


          </Content>
          {/* <TouchableOpacity style={{ width: '100%' }} onPress={() => this.onLogoutClick()} >
            <ImageBackground source={this.state.btnDoneImage} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Logout
                                </Text>
            </ImageBackground>
          </TouchableOpacity> */}

          <SafeAreaView />
        </BackgroundImage>
      </Container>
    );
  }
}

export default ProfileCategories;
