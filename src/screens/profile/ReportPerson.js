import React, { Component } from "react";
import { SafeAreaView, StyleSheet, Image, ScrollView, Alert, ImageBackground, TouchableOpacity, StatusBar, Text, Platform, TextInput, Switch } from "react-native";
import { Container, View, Button, Header, Left, Right, Content } from "native-base";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import Ionicons from "react-native-vector-icons/Ionicons";
import FloatingLabel from 'react-native-floating-labels';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts,API_ROOT} from "../../config/constant";
import Modal from "react-native-modal";
import { store } from "../../redux/store";
const BG_images = require("../../../assets/images/Dashboard/Icons.png");
import { isIphoneX } from '../Default/is-iphone-x'
const HEADER_SIZE = isIphoneX() ? 84 : 64;
const HEADER_PADDING_SIZE = isIphoneX() ? 40 : 16;

class ReportPerson extends ValidationComponent {

    constructor(props) {
        super(props);
        this.state = {
            codeModal: false,
            login_id:store.getState().auth.user.login_id,
            other_id:this.props.navigation.state.params.other_id,
            token: store.getState().auth.user.token,
        };
    }
    setCodeModalVisible(visible) {
        this.setState({ codeModal: visible });
    }
    componentDidMount(){
        this._navListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBarStyle('dark-content');
            Platform.OS == 'android' && StatusBar.setBackgroundColor('#fff');

        });
    }
    componentWillUnmount() {
        this._navListener.remove();
    }
    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <View style={styles.headerAndroidnav}>
                <StatusBar barStyle="dark-content" backgroundColor="#fff" />
                <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
                 </View>
                <View style={{ bottom: Height(2), position: 'absolute', alignSelf: 'center' }}>
                  <Text style={styles.headerTitle} >Report</Text>
                </View>
                <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', right: Width(4) }}>
                  <TouchableOpacity onPress={() => {navigation.goBack()}} >
                             <Text style={{ color: 'black' }} >Cancel</Text>
                  </TouchableOpacity>
                </View>
        
              </View>
             
            )
        }
    }


    repostUserAPICall(text)
    {
        const form = new FormData();
        form.append('token', this.state.token);
        form.append('login_id', this.state.login_id ); 
        form.append('other_login_id', this.state.other_id );
        form.append('report_type', text );

        console.log('Report User Params: '+JSON.stringify(form))

        fetch(API_ROOT+'user/report',{
          method: 'POST',
          body: form
        }) 
          .then(Resp => Resp.json())
          .then(resp =>
          {   
                this.setCodeModalVisible(!this.state.codeModal);
                //this.props.navigation.goBack()
          }) 
    }

    
    renderCodeModal() {
        return (
            <View>
                <Modal
                    isVisible={this.state.codeModal} style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} onBackdropPress={() => this.setState({ codeModal: false })} onBackButtonPress={() => this.setState({ codeModal: false })}>
                    <View style={{ height: Height(42), width: '90%', backgroundColor: '#FFF',borderRadius:10,alignItems: 'center'}}>
                        <View style={{height:Height(20),width:'100%',overflow:'hidden',backgroundColor:colors.appColor,borderTopLeftRadius:10,borderTopRightRadius:10}}>
                        <Image   resizeMode='center' source={BG_images} style={{height:'100%',width:'100%',borderTopLeftRadius:10,borderTopRightRadius:10}} />
                        </View>
                        
                        <Text style={{ textAlign: 'center', marginTop: Height(3), width: Width(70), fontFamily: fonts.Raleway_Bold, fontSize: FontSize(16), color: '#3A4759' }}>Thanks for reporting this account</Text>
                        <Text style={{ alignSelf: 'center', marginVertical: Height(1), textAlign: 'center', width: Width(70), marginHorizontal: Width(5), fontFamily: fonts.Roboto_Regular, fontSize: FontSize(13), color: '#B4B7BA' }}>Thanks for helping us keep the Planmesh community safe.</Text>
                        <View style={{ borderTopColor:colors.lightgrey,borderTopWidth:Height(.2),height: Height(7), width: '100%',justifyContent:'center',alignItems:'center',bottom:0,position:'absolute' }}>
                            <Text  onPress={()=>{
                                this.setState({codeModal:false})
                                this.props.navigation.goBack()
                        }} style={{  fontFamily: fonts.Raleway_Bold, fontSize: FontSize(16), color: '#3A4759' }}>CLOSE</Text>

                        </View>

                    </View>

                </Modal>
            </View>
        )
    }
    render() {
        let { isModalVisible } = this.state

        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }} >
                <Toastt ref="toast"></Toastt>
                <BackgroundImage >
                    <SafeAreaView />
                    <Content>
                        <View style={{ marginHorizontal: Width(3), marginVertical: Height(3) }} >
                            <Text style={{ color: colors.black, fontSize: FontSize(18),fontFamily: fonts.Raleway_Regular }}>
                                Why do you want to report?
          </Text>
                        </View>
                        <View style={{ height: Height(.2), width: '100%', backgroundColor: colors.lightgrey }} />

                        <TouchableOpacity onPress={() => {
                            this.repostUserAPICall('Fake Account')
                        }} style={{ flexDirection: 'row', paddingHorizontal: Width(3), height: Height(6), alignItems: 'center', backgroundColor: '#fff' }}>
                            <Text style={styles.ReportText}>
                                Fake Account</Text>

                        </TouchableOpacity>
                        <View style={{ height: Height(.2), width: '100%', backgroundColor: colors.lightgrey }} />

                        <TouchableOpacity 
                        onPress={() => {
                            this.repostUserAPICall('Fake Name')
                        }}
                        style={{ flexDirection: 'row', paddingHorizontal: Width(3), height: Height(6), alignItems: 'center', backgroundColor: '#fff' }}>
                            <Text style={styles.ReportText}>
                                Fake Name</Text>

                        </TouchableOpacity>
                        <View style={{ height: Height(.2), width: '100%', backgroundColor: colors.lightgrey }} />

                        <TouchableOpacity 
                        onPress={() => {
                            this.repostUserAPICall('Pretending to Someone')
                        }}
                        style={{ flexDirection: 'row', paddingHorizontal: Width(3), height: Height(6), alignItems: 'center', backgroundColor: '#fff' }}>
                            <Text style={styles.ReportText}>
                                Pretending to Someone</Text>

                        </TouchableOpacity>

                        <View style={{ height: Height(.2), width: '100%', backgroundColor: colors.lightgrey }} />


                        <TouchableOpacity 
                        
                        onPress={() => {
                            this.repostUserAPICall('Posting Inappropriate things')
                        }}
                        
                        style={{ flexDirection: 'row', paddingHorizontal: Width(3), height: Height(6), alignItems: 'center', backgroundColor: '#fff' }}>
                            <Text style={styles.ReportText}>
                                Posting Inappropriate things</Text>

                        </TouchableOpacity>

                        <View style={{ height: Height(.2), width: '100%', backgroundColor: colors.lightgrey }} />

                      
                        {this.renderCodeModal()}
                    </Content>

                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}
const styles = StyleSheet.create({
    headerAndroidnav: {
        backgroundColor: '#fff',
        shadowOpacity: 0,
        borderBottomWidth: 1,
        borderBottomColor:colors.lightgrey,
        ...Platform.select({
          ios: {
            height: HEADER_SIZE,
            paddingTop: HEADER_PADDING_SIZE,
          },
          android: {
            height: HEADER_SIZE,
            paddingTop: HEADER_PADDING_SIZE,
          },
        }),
      },
      headerTitle: {
        letterSpacing: 0.02,
        fontSize: 20,
        fontFamily: "Raleway-Medium",
        color: 'black',
        ...Platform.select({
          ios: {
            fontWeight: '500',
          },
          android: {
          },
        })
      },
  });
export default ReportPerson;
