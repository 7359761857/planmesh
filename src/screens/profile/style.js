const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import { isIphoneX } from '../Default/is-iphone-x'
const HEADER_SIZE = isIphoneX() ? 80 : 100;
const HEADER_PADDING_SIZE = isIphoneX() ? 40 : 16;
import { Width ,colors,FontSize} from '../../config/dimensions';
import { fonts } from "../../config/constant";

export default {

    headerAndroidnav: {
        backgroundColor: '#41199B',
        shadowOpacity: 0,
        borderBottomWidth: 0,
        //height:isIphoneX() ? 85 : 64,
        height:isIphoneX() ? 104 : 74,
        ...Platform.select({
            ios: {
                height: HEADER_SIZE,
                paddingTop: HEADER_PADDING_SIZE,
            },
            android: {
              
            },
        }),
    },
    headerTitle: {
        letterSpacing: 0.02,
        fontSize: 20,
        fontFamily: "Raleway-Medium",
        color: 'white',
        ...Platform.select({
            ios: {
              fontWeight: '500',
            },
            android: {
            },
          })
    },
    labelInput: {
        color: '#363169',
        fontSize: 15,
        fontFamily: "Roboto-Bold",
        paddingLeft:0
    },
    formInput: {  
        marginLeft:25,
        marginRight:30,
        marginTop:25,
    },
    formInputFocus:{
        borderBottomWidth: 2,
        borderColor: '#3A4759',
        paddingLeft:0,
    },
    formInputUnFocus:{
        borderBottomWidth: 2,
        borderColor: '#BBC1C8',
    },
    labelInputUnFocus:{
        color: '#8f8f90',
        fontSize: 16,
        fontFamily: "Roboto-Regular",
        paddingLeft:0
    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        fontFamily: "Roboto-Regular",

    },
    labelInputError: {
        color:'red'
    },
    tabBar: {
        flexDirection: 'row',
        backgroundColor: '#fff'
      },
      tabItem: {
        flex: 1,
        alignItems: 'center',
        padding: 16,
        marginHorizontal:Width(2)

      },
      selectedTab: {
        borderBottomColor: '#EC4AB9',
        borderBottomWidth: 3,
      },
      notSelected: {
        alignSelf: 'center',
        padding: 5,
        height: 50,
        width: '50%',
        borderBottomColor: colors.lightgrey,
        borderBottomWidth:0.5,
        justifyContent: 'center',
      },
      selectedHeader: {
        alignSelf: 'center',
        padding: 5,
        height: 50,
        width: '50%',  
        borderBottomColor: colors.pink,
        borderBottomWidth:2,

        justifyContent: 'center'
      },
      subdigit: {
        alignSelf: 'center',
        color:'#363169',
        fontSize: FontSize(16), fontFamily: fonts.Raleway_Bold 
      },
      
};
