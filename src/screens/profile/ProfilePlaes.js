import React, { Component } from "react";
import { SafeAreaView, StyleSheet, Image, ScrollView, Text, ImageBackground, TouchableOpacity, StatusBar } from "react-native";
import { Container, View, Button, Header, Left, Item, Icon, Right, Content, Input } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import { fonts,API_ROOT } from "../../config/constant";
import { store } from "../../redux/store";
import { PlaceList,AddPlaceList } from "../../redux/actions/auth";

import { Height, FontSize, Width, colors } from "../../config/dimensions";
import SplashScreen from "react-native-splash-screen";
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
const backWhite = require("../../../assets/images/backWhite.png");
const btnBg = require("../../../assets/images/btnBg.png");
const search_img = require("../../../assets/images/Dashboard/search.png");

const closeicon = require("../../../assets/images/Dashboard/close.png");


class ProfilePlaes extends ValidationComponent {

    constructor(props) {
        super(props);
        this.array = [];
        this.search = null,
        this.state = {
            placeList:[],
            selected_array:[],
            token: store.getState().auth.user.token,
           login_id: store.getState().auth.user.login_id,
            text: '',
            interest: ['Movies & Videos', 'Dining out', 'Music & Concerts', 'Coffee Breaks', 'Camping', 'Watching Sports', 'Shopping', 'Museums & Arts', 'Fishing & Hunting',],
            count:0,
            is_close_visible:false,
            selected_array: [],
        };
    }

    componentWillMount()
    {
        this.getUserData()
    }


    componentDidMount() {
        SplashScreen.hide()
         let data = {
            token: this.state.token,
            login_id: this.state.login_id,
           limit:'1000',
           offset:''
        }
        PlaceList(data).then(res => {
            if (res.success) {
                this.setState({placeList:res.data})
            }
            else {

                this.setState({ empty: false })

            }
        }).catch(err => {
            alert(err)


        })

    }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={styles.headerAndroidnav}>
                    <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack() }}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity></View>
                    <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                        <Text style={styles.headerTitle} >Add Places</Text></View>
                </Header>
            )
        }
    }

    getUserData = () => {
        
        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('type', 0)
        data.append('other_id', '')

        fetch(API_ROOT + 'user/details', {
          method: 'post',
          body: data
        })
          .then((response) => response.json())
          .then((responseData) => {
            if (responseData.success) {
    
            //alert(JSON.stringify(responseData))
    
            var placeslist = responseData.data.fav_place_list
            var temparr = []
            placeslist.map((item) => {
                temparr.push(item.place_id)
                this.array.push(item.place_id)
            });

            this.setState({selected_array:temparr})
            //alert(JSON.stringify(selected_array))
    
            } else {
              if (responseData.text === 'Invalid token key') {
                alert(responseData.text)
                this.props.navigation.navigate('Login')
              }
            }
          })
    
          .catch((error) => {
            
          })
      }

    onFreeMoneyClick = () => {

        if(this.state.selected_array.length > 0)
        {
            let data = {
                token: this.state.token,
                login_id: this.state.login_id,
                place_id: String(this.state.selected_array)
            }
            AddPlaceList(data).then(res => {
                if (res.success) {
                   this.props.navigation.navigate('myprofile')
                }
                else {
                }
            }).catch(err => {
    
    
            })
        }
        else
        {
            this.props.navigation.navigate('myprofile')
        }
    }
    makeStyles() {
        return StyleSheet.create({
            view: {
                flexDirection: 'row',
                flexWrap: 'wrap',
                backgroundColor: '#fff',
                alignItems: 'center',

            },

            text: {
                fontSize: FontSize(14),
                paddingLeft: '4%',
                paddingRight: '4%',
                paddingTop: '1.8%',
                paddingBottom: '1.8%',
                textAlign: 'center',
                color: '#676868',

            },
            selectedText:{
                fontSize: FontSize(14),
                paddingLeft: '4%',
                paddingRight: '4%',
                paddingTop: '1.8%',
                paddingBottom: '1.8%',
                textAlign: 'center',
                color: '#fff',
            },
            touchable: {
                marginLeft: '1.1%',
                marginTop: '1.1%',
                borderRadius: 5,
                borderColor: '#676868',
                borderWidth: 1,
            },
            selectedTouchable: {
                marginLeft: '1.1%',
                marginTop: '1.1%',
                borderRadius: 5,
                borderColor: '#41199B',
                borderWidth: 1,
                backgroundColor: '#41199B',
               
            }
        })
    }
    checkForFriends = (item) => {
        if (this.state.selected_array.includes(item.place_id)) {
            return true
        } else {
            return false
        }

    }
   
    onClearTextPressed()
    {
        this.setState({is_close_visible:false,text:''})
        setTimeout(()=>{
            this.search.blur()

        },100)
    }

    SearchFilterFunction(text)
    {
        this.setState({is_close_visible:true,text:text})

        if(text == '')
        {
            this.setState({is_close_visible:false})
        }

    }


    render() {
        const styles1 = this.makeStyles()

        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }} >
                <BackgroundImage >
                    <SafeAreaView />
                    <View style={{ height: Height(1), backgroundColor: colors.white }}></View>

                    <View style={{ backgroundColor: '#fff' }}>
                        <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', margin: '1%' }}>
                            <Item style={{ backgroundColor: colors.lightgrey, borderColor: colors.white, borderRadius: 15, height: Height(6), justifyContent: 'center', width: Width(92.5) }}>
                            <Image source={search_img} tintColor='#B4B7BA' style={{ marginLeft: Width(4.5) }} />
                                <Input
                                    onChangeText={text => this.SearchFilterFunction(text)}
                                    value={this.state.text}
                                    placeholderTextColor='#8F8F90'
                                    placeholder="Search places"
                                    ref={search=>this.search = search}
                                    style={{ fontFamily: fonts.Roboto_Regular, fontSize:FontSize(16), marginLeft: Width(2) }} />
                                    {
                                        this.state.is_close_visible == true ?
                                        <TouchableOpacity style ={{justifyContent:'center',alignItems:'center',height:25,width:25, position:'absolute',right:10}} onPress={ () => this.onClearTextPressed()}>
                                            <Image source={closeicon} resizeMode = 'stretch' style = {{height:17,width:17}}/>
                                        </TouchableOpacity>
                                        :
                                        null
                                    }
                            </Item>
                        </View>
                    </View>
                    <Content style={{alignSelf:'center',marginTop:24}}>
                    <View style={{ marginHorizontal: Width(3)}}>
                            {/* <View style={{ margin: '3%', backgroundColor:'red' }}> */}
                                {/* <Text style={{ width:'100%',textAlign:'center',alignSelf:"center",fontFamily: fonts.Raleway_Bold, color: colors.dargrey, fontSize: FontSize(16),lineHeight: 20,marginVertical:10}}>Choose five or more places your like</Text> */}
                            {/* </View> */}
                            <View style={styles1.view}>
                                                    {this.state.placeList.map(item1=>{

                                                        if(item1.place_name.includes(this.state.text))
                                                        {
                                                            return(
                                                                <TouchableOpacity
                                                                onPress={() => {
                                                                    //this.setState({ selected_array: '' })
                                                                    if (this.array.includes(item1.place_id) == true) {
                                                                        let item_index = this.array.indexOf(item1.place_id)
                                                                        this.array.splice(item_index, 1)
                                
                                                                    } else {
                                                                        this.array.push(item1.place_id)
                                
                                                                    }
                                                                    this.setState({ selected_array: this.array })
                                                                }}
                                                                style={this.checkForFriends(item1) == true ? styles1.selectedTouchable : styles1.touchable}>
                                                                <Text style={this.checkForFriends(item1) == true ? styles1.selectedText : styles1.text}>{item1.place_name}</Text>
                                                            </TouchableOpacity>
                                                            )
                                                        }

                                                    })}
                                                    </View>
                      

                        </View>
                    </Content>

                   {/* {this.state.selected_array.length < 1 ?
                        <View style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }} >
                            <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Save</Text>
                            </ImageBackground>
                        </View>
                        : */}

                        <TouchableOpacity style={{ width: '100%', marginBottom: Height(2) }} onPress={() => this.onFreeMoneyClick()}>
                            <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Save</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                        {/* } */}
                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default ProfilePlaes;
