import React, { Component } from "react";
import { SafeAreaView, StyleSheet, Image, ScrollView, Text, Platform, TouchableOpacity, StatusBar,Dimensions} from "react-native";
import { Container, View, Button, Header, Left, Item, Icon, Right, Content, Input } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import ImagePicker from 'react-native-image-picker';

import { fonts,API_ROOT } from "../../config/constant";
import { store } from "../../redux/store";
import { UPDATE_USER } from "../../redux/actions/types";

import { Height, FontSize, Width, colors } from "../../config/dimensions";
import SplashScreen from "react-native-splash-screen";
import { isIphoneX } from "../Default/is-iphone-x";
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
//const backWhite = require("../../../assets/images/backWhite.png");
const closeWhite = require("../../../assets/images/ic_close_white.png");
const btnBg = require("../../../assets/images/btnBg.png");
const search_img = require("../../../assets/images/Dashboard/search.png");

const closeicon = require("../../../assets/images/Dashboard/close.png");
import RNProgressHUB from 'react-native-progresshub';
import ActionSheet from 'react-native-actionsheet';
import Share from 'react-native-share';
import CameraRoll from "@react-native-community/cameraroll";
import ImageZoom from 'react-native-image-pan-zoom';


class ProfilePicView extends ValidationComponent {

    constructor(props) {
        super(props);
        this.is_photo_selected = false;
        this.state = {
            token: store.getState().auth.user.token,
           login_id: store.getState().auth.user.login_id,
           UserDetail: this.props.navigation.state.params.UserDetail,
           photo:this.props.navigation.state.params.photo != null ? this.props.navigation.state.params.photo: '',
           is_my_photo:this.props.navigation.state.params.is_my_photo,
           is_chat_photo:this.props.navigation.state.params.is_chat_photo != null ? this.props.navigation.state.params.is_chat_photo : false,
        };
        this.showActionSheet = this.showActionSheet.bind(this);
    }

    componentWillMount()
    {
        
    }

    componentDidMount() {

      //alert(this.props.navigation.state.params.is_chat_photo)

      if(this.state.UserDetail != null || this.state.UserDetail != undefined)
      {
        this.setState({photo:this.state.UserDetail.photo})
      }

    }

    // static navigationOptions = ({ navigation }) => {
    //     theNavigation = navigation;
    //     return {
    //         header: (
    //             <Header style={styles.headerAndroidnav}>
    //                 <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
    //                 <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
    //                     <TouchableOpacity onPress={() => { navigation.goBack() }}>
    //                         <Image source={backWhite} style={{ tintColor: "white" }} />
    //                     </TouchableOpacity>
    //                 </View>
    //                 <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
    //                   <Text style={styles.headerTitle} >{theNavigation.state.params.is_chat_photo == true ? 'Photo' : 'Profile Photo'}</Text>
    //                 </View>
    //                 {
    //                     theNavigation.state.params.is_my_photo == true ?
    //                     <TouchableOpacity style={{ bottom: Height(1.5),right:10, position: 'absolute', alignItems: 'center' }} onPress = { () => ProfilePicView.onClickSelectPhoto()}>
    //                         <Text style={styles.headerTitle} >Edit</Text>
    //                     </TouchableOpacity>
    //                     : null
    //                 }
                    
    //             </Header>
    //         )
    //     }
    // }
    
    renderTopBar() {
        return (
            <View style={styles.headerAndroidnav}>
                <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
                <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                    <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                        <Image source={closeWhite} style={{ tintColor: "white" }} />
                    </TouchableOpacity></View>
                <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center',alignSelf:'center' }}>
                  <Text style={styles.headerTitle} >{this.state.is_chat_photo == true ? 'Photo' : 'Profile Photo'}</Text>
                </View>
                {
                        this.state.is_my_photo == true ?
                        <TouchableOpacity style={{ bottom: Height(1.5),right:10, position: 'absolute', alignItems: 'center' }} onPress = { () => this.onClickSelectPhoto()}>
                            <Text style={styles.headerTitle} >{this.is_photo_selected == true ? 'Save' : 'Edit'}</Text>
                        </TouchableOpacity>
                        : null
                }

                {
                  this.state.is_chat_photo == true ?
                  <TouchableOpacity style={{ bottom: Height(1.5),right:10, position: 'absolute', alignItems: 'center' }} onPress = { () => this.onClickMore()}>
                      <Text style={styles.headerTitle} >More</Text>
                  </TouchableOpacity>
                  : null
                }
            </View>
        )
    }

    async hasAndroidPermission() {
      const permission = PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;
    
      const hasPermission = await PermissionsAndroid.check(permission);
      if (hasPermission) {
        return true;
      }
    
      const status = await PermissionsAndroid.request(permission);
      return status === 'granted';
    }
    
    async  savePicture() {

      if (Platform.OS === "android" && !(await hasAndroidPermission())) {
        return;
      }
      CameraRoll.save(this.state.photo)
    }

    onClickSelectPhoto = () => {

        var options = {
          title: '',
          allowsEditing: true,
          storageOptions: {
            skipBackup: true,
            path: 'images'
          },
          maxWidth: 500,
          maxHeight: 500
        };
    
        ImagePicker.showImagePicker(options, (response) => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled image picker');
          }
          else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          }
          else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          }
          else {
            let source = { uri: response.uri };
    
            this.setState({
              photo: response.uri,
            }, () => {
                this.editProfile()
            })
          }
        });
      }

      titleCase(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
        }
        return splitStr.join(' '); 
     }

      editProfile = () => {

        // alert(moment(this.state.DOB).toDate())
        // return
        var username = this.titleCase(this.state.UserDetail.name)
    
        RNProgressHUB.showSpinIndeterminate();
        const createFormData = (photo_path, body) => {
          const data = new FormData();
          data.append("profile_pic", {
            name: 'selfie.jpg',
            type: 'image/jpg',
            uri:
              Platform.OS === "android" ? this.state.photo : this.state.photo.replace("file://", "")
          });
    
          Object.keys(body).forEach(key => {
            data.append(key, body[key]);
          });
    
          return data;
        };
    
        fetch(API_ROOT + 'update/profile', {
          method: 'post',
          body: createFormData(this.state.photo, {
            login_id: this.state.login_id,
            name: username.trim(),
            bio: this.state.UserDetail.bio,
            country_code: '',
            city: this.state.UserDetail.city,
            dob: this.state.UserDetail.dob,
            mobile_number: ''
          })
        })
          .then((response) => response.json())
          .then((responseData) => {
            if (responseData.success) {
              RNProgressHUB.dismiss();
              store.dispatch({type:UPDATE_USER,data:responseData.data})
               this.props.navigation.goBack()
            } else {
              RNProgressHUB.dismiss();
               this.props.navigation.goBack()
    
            }
          })
    
          .catch((error) => {
            //alert(error)
            RNProgressHUB.dismiss();
            // this.props.navigation.goBack()
    
          })
    
      }
    
      onClickMore = () => {
        this.showActionSheet()
    }

      showActionSheet = () => {
        setTimeout(() => { this.ActionSheet.show() }, 300)
    }

      onActionSelected(index)
      {
          if(index == 0)
          {

            const shareOptions = {
              title: 'Planmesh',
              failOnCancel: false,
              urls: [this.state.photo],
            };

            Share.open(shareOptions)
            .then((res) => {
              console.log(res);
            })
            .catch((err) => {
              err && console.log(err);
            });
          }
          else if(index == 1)
          {
              this.savePicture()
          }
      }

    render() {

      OptionShow = []

      if (OptionShow.length == 0) {
        OptionShow.push('Share')
        OptionShow.push('Save')
        OptionShow.push('Cancel')
      }

        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff'}} >
                {this.renderTopBar()}
                {/* <View style = {{flex:1, alignItems:'center', justifyContent:'center'}}> */}
                <ImageZoom cropWidth={Dimensions.get('window').width}
                       cropHeight={Dimensions.get('window').height - 80}
                       imageWidth={Dimensions.get('window').width}
                       imageHeight={Dimensions.get('window').height / 2}>
                  <Image style={{ height: Dimensions.get('window').height / 2, width: Dimensions.get('window').width}} source={{ uri: this.state.photo}} resizeMode ='contain'/>
                </ImageZoom>
                {/* </View> */}
                <View>
                  <ActionSheet
                    tintColor="#007AFF"
                    ref={o => this.ActionSheet = o}
                    options={OptionShow}
                    cancelButtonIndex={OptionShow.length - 1}
                    //destructiveButtonIndex={5}  
                    onPress={(index) => this.onActionSelected(index)}
                  />
					      </View>
            </Container>
        );
    }
}

export default ProfilePicView;
