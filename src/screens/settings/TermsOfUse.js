import React, { Component } from "react";
import { SafeAreaView, Image,TouchableOpacity, StatusBar, Text} from "react-native";
import { WebView } from 'react-native-webview';

import { Container, View, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import { Height, Width, FontSize,colors } from "../../config/dimensions";
import { store } from "../../redux/store";

const backWhite = require("../../../assets/images/backWhite.png");

var subThis;
class TermsOfUse extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {
      login_id:store.getState().auth.user.login_id,
      token:store.getState().auth.user.token,
    };
    subThis = this
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
      header: (
        <Header style={styles.headerAndroidnav}>
            <StatusBar barStyle="light-content" backgroundColor="#41199B" />
            <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
                <TouchableOpacity onPress={() => { navigation.goBack() }}>
                    <Image source={backWhite} style={{ tintColor: "white" }} />
                </TouchableOpacity></View>
            <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                <Text style={styles.headerTitle} >Terms Of Use</Text>
                </View>

        </Header>
      )
    }
  }


  render() {
    
    return (
      <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#FFF' }} >
        <Toastt ref="toast"></Toastt>
          <SafeAreaView />
          <View style = {{flex:1}}>
                <WebView source={{ uri: "https://app.termly.io/document/terms-of-use-for-ios-app/c16fcdf6-22ed-4554-946f-3883e0a422a9" }} style = {{flex:1}}/>
          </View>
          <SafeAreaView />
      </Container>
    );
  }
}

export default TermsOfUse;
