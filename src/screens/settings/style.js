const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import { isIphoneX } from '../Default/is-iphone-x'
const HEADER_SIZE = isIphoneX() ? 84 : 64;
const HEADER_PADDING_SIZE = isIphoneX() ? 40 : 16;
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from '../../config/constant';

export default {

  headerAndroidnav: {
    backgroundColor: '#41199B',
    shadowOpacity: 0,
    borderBottomWidth: 0,
    ...Platform.select({
      ios: {
        height: HEADER_SIZE,
        paddingTop: HEADER_PADDING_SIZE,
      },
      android: {
        height: HEADER_SIZE,
        paddingTop: HEADER_PADDING_SIZE,
      },
    }),
  },
  headerTitle: {
    letterSpacing: 0.02,
    fontSize: 20,
    fontFamily: "Raleway-Medium",
    color: 'white',
    ...Platform.select({
      ios: {
        fontWeight: '500',
      },
      android: {
      },
    })
  },
  labelInput: {
    color: '#363169',
    fontSize: 15,
    fontFamily: "Roboto-Bold",
    paddingLeft: 0
  },
  formInput: {
    color: '#3a4759',
    fontSize: 16,
    fontFamily: "Roboto-Regular",
    marginLeft: 25,
    marginRight: 30,
    marginTop: 25,
  },
  formInputFocus: {
    borderBottomWidth: 2,
    borderColor: '#3A4759',
    paddingLeft: 0,
  },
  formInputUnFocus: {
    borderBottomWidth: 2,
    borderColor: '#BBC1C8',
  },
  labelInputUnFocus: {
    color: '#8f8f90',
    fontSize: 16,
    fontFamily: "Roboto-Regular",
    paddingLeft: 0
  },
  input: {
    borderWidth: 0,
    fontSize: 16,
    fontFamily: "Roboto-Regular",

  },
  labelInputError: {
    color: 'red'
  },
  rowBack: {
    alignItems: 'center',
    backgroundColor: '#FFF',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 15
  },
  backRightBtn: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    width: Width(20)
  },
  backTextWhite: {
    color: '#FFF'
  },
  backTextBlack: {
    color: '#000'
  },
  backRightBtnLeft: {
    backgroundColor: 'rgb(244,179,189)',
    right: Width(20)
  },
  backRightBtnRight: {
    backgroundColor: '#EC4AB9',
    right: 0
  }, header: {
    backgroundColor: '#41199b',
    height: 50,
    justifyContent: 'center',
    flexDirection: 'row'

  },
  savebtn: {
    borderRadious: 25,
    width: '90%',
    height: 50,
    bottom: 5,
    postion: 'absolute',
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: '#ff00ba',
  },

  Sendevent:
  {
    backgroundColor: '#f2f4f7', height: 70, justifyContent: 'center',

  },
  listevent1:
  {
    flexDirection: 'row', height: 50,
  }, 
  top_text:
  {
    width: Width(80),
    color: colors.black,
    fontFamily: fonts.Raleway_Regular,
    fontSize: FontSize(18),
  },
  pickertext_view: {
    alignItems: 'center',
    marginHorizontal: Width(4),
    justifyContent: 'center',
    height: Height(7),
    flexDirection: 'row',
  },
  picker_text:
  {
    flex: 1, color: '#3A4759',
    fontFamily: fonts.Roboto_Regular,
    fontSize: FontSize(13),
  },
  
  singleLine: {
    marginTop: Height(1.5),
    height: Height(0.2),
    marginHorizontal: Width(3),
    backgroundColor: '#EEE',
  },
  singleLine2: {
    //marginTop: Height(1.5),
    marginTop: Height(0.5),
    height: Height(0.5),
    backgroundColor: '#EEE',
  },
  singleLine3: {
    height: Height(0.5),
    backgroundColor: '#EEE',
  },

  bordrview:
  {
    //marginTop: Height(1),
    alignSelf:'center',
    flexDirection: 'row',
    alignItems:'center',
    height: Height(4),
    width: Width(38),
    borderWidth:Height(0.2),
    borderColor: '#EEE'
  },
    bordrview1:
  {
    alignSelf:'center',

    flexDirection: 'row',
    alignItems:'center',
    height: Height(4),
    width: Width(38),
    borderWidth:Height(0.2),
    borderColor: '#EEE'
  },
pickerText:{
  flex:1,
  fontSize:FontSize(12),
  color:'#3A4759',
  marginLeft:Width(2),
  fontFamily:fonts.Roboto_Regular

},

 
  settingText: {
    flex: 1, color: '#38336a', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16)
  },
  ReportText: {
    flex: 1, color: colors.dargrey, fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16)

  },
  header: {
        backgroundColor: '#41199b',
        height: Height(7),
        justifyContent: 'center',
        flexDirection: 'row'

    },
    text: {
      fontSize:FontSize(16),
        color: '#38336a',
        flex: 1,
        fontFamily: 'Roboto-Regular',
        marginRight:24
    },
    bottomVIew: {
        width: Width(97),
        height: Height(0.2),
        backgroundColor: '#f2f4f7',
        marginHorizontal: Width(4),
    },

    headingText:
    {
        color: '#38336a',
        fontSize: FontSize(16),
        fontFamily: 'Raleway-Bold',
    },
    SWitch:
    {
    },
    headingView:
    {
      paddingHorizontal: Width(4),
        backgroundColor: '#f2f4f7',
        justifyContent: 'center',
        height: Height(9)
    },
    savebtn: {
        justifyContent: 'center',
        backgroundColor: '#ff00ba',
        height: Height(7),
        borderRadius: 25,
        marginHorizontal: Width(3),
        marginVertical: Height(5),
    },
      tabBar: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        justifyContent:'space-between',
       



      },
      tabItem: {
        alignItems: 'center',
        paddingVertical:10,
        paddingHorizontal:Width(17)


      },
      selectedTab: {
        borderBottomColor: '#EC4AB9',
        borderBottomWidth: 3,
      },
      UnselectedTab: {
        borderBottomColor: colors.dargrey,
        borderBottomWidth: 0.3,
      },

};
