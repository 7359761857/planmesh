import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Alert, ImageBackground, TouchableOpacity, StatusBar, Platform } from "react-native";
import { Container, View, Text, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import FloatingLabel from 'react-native-floating-labels';
import { changePassword } from "../../redux/actions/auth";
import NavigationService from "../../services/NavigationService";
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { API_ROOT } from "../../config/constant";
import { store } from "../../redux/store";
let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height


const backWhite = require("../../../assets/images/backWhite.png");
const bottomRight = require("../../../assets/images/Login/bottomRight.png");
const btnBg = require("../../../assets/images/btnBg.png");
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
import MixpanelManager from '../../../Analytics'
class change_password extends ValidationComponent {

    constructor(props) {
        super(props);
        this.state = {
            labelStyleOldpassword: styles.labelInputUnFocus, styleOlPassword: styles.formInputUnFocus, oldpassword: "", oldpasswordValidation: true, oldpasswordValidationMsg: "",
            labelStyleNewPassword: styles.labelInputUnFocus, styleNewPassword: styles.formInputUnFocus, newpassword: "", newpasswordValidation: true, newpasswordValidationMsg: "",
            labelStyleRepassword: styles.labelInputUnFocus, styleRePassword: styles.formInputUnFocus, reenterpassword: "", reenterValidation: true, reenterValidationMsg: "",
            btnDoneDisable: true, btnDoneImage: btnBgGrey, wsError: false,
            token: store.getState().auth.user.token,
            login_id: store.getState().auth.user.login_id,

        };
        this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
    }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;

        return {
            header: (
                // <Header style={styles.headerAndroidnav}>
                //     <StatusBar barStyle="light-content" backgroundColor="#41199B" />
                //     <Left style={{ alignItems: 'flex-start', justifyContent: 'flex-start', flex: 0.2, }} >
                //         <Button transparent onPress={() => {
                //             navigation.goBack()
                //         }} style={{ width: 44, height: 44, marginLeft: 5 }} >
                //             <Image source={backWhite} style={{ tintColor: "white" }} />
                //         </Button>
                //     </Left>
                //     <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', flex: 0.6 }}>
                //         <Text style={[styles.headerTitle]} >Set New Password</Text>
                //     </View>
                //     <Right style={{ alignItems: 'flex-end', justifyContent: 'flex-end', flex: 0.2 }}>
                //     </Right>
                // </Header>

                <Header style={[styles.headerAndroidnav,{height:44}]}>
                    <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack()}}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity></View>
                    <View style={{bottom: Height(2), position: 'absolute',alignItems:'center'}}>
                        <Text style={styles.headerTitle} >Set New Password</Text></View>
                </Header>
            )
        }
    }

    componentDidMount() {
        this.setState({
            email: theNavigation.getParam('email'),
        })
    }

    onBlurCode() {
        if (this.state.oldpassword.length <= 0) {
            this.setState({
                labelStyleCode: styles.labelInputUnFocus,
                styleCode: styles.formInputUnFocus
            })
        } else {
            this.codevalidtion()
        }
    }
    onFocusCode() {
        this.setState({
            labelStyleOldpassword: styles.labelInput,
            styleCode: styles.formInputFocus
        }, () => {
            this.validtion()
        })
    }
    onChangeTextCode = (code) => {
        this.setState({ code }, () => {
            this.validtion()
        })

    }

    codevalidtion = () => {
        if (this.state.oldpassword.length < 6) {
            this.setState({ oldpasswordValidation: false, oldpasswordValidationMsg: "The New password length  must be greater than 5.", labelStyleNewPassword: [styles.labelInput, styles.labelInputError], }, () => {
                this.validtion()
            })
            return false
        } else if (this.state.oldpassword != this.state.reenterpassword) {
            this.setState({ reenterValidation: false, reenterValidationMsg: "Re-enter password not match", labelStyleRepassword: [styles.labelInput, styles.labelInputError] }, () => {
                this.validtion()
            })
            return false
        }
        this.setState({ oldpasswordValidation: true, oldpasswordValidationMsg: "" }, () => {
            this.validtion()
        })
        return true
    }

    onBlurNewPassword() {
        if (this.state.newpassword.length <= 0) {
            this.setState({
                labelStyleNewPassword: styles.labelInputUnFocus,
                styleNewPassword: styles.formInputUnFocus
            }, () => {
                this.validtion()
            })
        } else {
            this.NewPasswordvalidtion()
        }
    }
    onFocusNewPassword() {
        this.setState({
            labelStyleNewPassword: styles.labelInput,
            styleNewPassword: styles.formInputFocus
        })
    }

    onChangeTextNewPassword = (newpassword) => {
        this.setState({ newpassword }, () => {
            this.validtion()
        })

    }
    onChangeTextOldPassword = (oldpassword) => {
        this.setState({ oldpassword }, () => {
            this.validtion()
        })

    }

    NewPasswordvalidtion = () => {

        if (this.state.newpassword.length < 6) {
            this.setState({ newpasswordValidation: false, newpasswordValidationMsg: "The New password length  must be greater than 5.", labelStyleNewPassword: [styles.labelInput, styles.labelInputError], }, () => {
                this.validtion()
            })
            return false
        } else if (this.state.newpassword != this.state.reenterpassword) {
            this.setState({ reenterValidation: false, reenterValidationMsg: "Re-enter password not match", labelStyleRepassword: [styles.labelInput, styles.labelInputError] }, () => {
                this.validtion()
            })
            return false
        }
        this.setState({ newpasswordValidation: true, newpasswordValidationMsg: "" }, () => {
            this.validtion()
        })
        return true
    }

    onBlurReNewPassword() {
        if (this.state.newpassword.length <= 0) {
            this.setState({
                labelStyleRepassword: styles.labelInputUnFocus,
                styleRePassword: styles.formInputUnFocus
            }, () => {
                this.validtion()
            })
        } else {
            this.ReenterPasswordvalidtion()
        }
    }
    onFocusReNewPassword() {
        this.setState({
            labelStyleRepassword: styles.labelInput,
            styleRePassword: styles.formInputFocus
        })
    }

    onChangeTextReenterPassword = (reenterpassword) => {
        this.setState({ reenterpassword }, () => {
            this.validtion()
        })

    }

    ReenterPasswordvalidtion = () => {
        if (this.state.reenterpassword.length < 6) {
            this.setState({ reenterValidation: false, reenterValidationMsg: "The Re-enter password length  must be greater than 5", labelStyleRepassword: [styles.labelInput, styles.labelInputError], }, () => {
                this.validtion()
            })
            return false
        } else if (this.state.newpassword != this.state.reenterpassword) {
            this.setState({ reenterValidation: false, reenterValidationMsg: "Re-enter password not match", labelStyleRepassword: [styles.labelInput, styles.labelInputError], }, () => {
                this.validtion()
            })
            return false
        }
        this.setState({ reenterValidation: true, reenterValidationMsg: "" }, () => {
            this.validtion()
        })
        return true
    }

    validtion = () => {
        this.validate({
            code: { minlength: 5, required: true },
        });
        if (this.isFieldInError('code')) {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
            return false
        } else if (this.state.newpassword.length < 6) {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
            return false
        } else if (this.state.reenterpassword.length < 6) {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
            return false
        } else if (this.state.newpassword != this.state.reenterpassword) {
            this.setState({ btnDoneDisable: true, btnDoneImage: btnBgGrey })
            return false
        }
        this.setState({ btnDoneDisable: false, btnDoneImage: btnBg, passwordValidation: true, })
        return true
    }
    onClickResetPassword = () => {
        //this.wscall(this.state.email, this.state.code, this.state.newpassword)
        changePassword({ email: this.state.email, verify_pin: this.state.code, new_password: this.state.newpassword }).then(res => {
            if (res.success) {
                //NavigationService.navigate('Login')
                this.props.navigation.navigate('Login')
            }
            else {

            }
            alert(res.text)
        }).catch(err => {
            alert(JSON.stringify(err))
        })
    }
    wscall = async (email, code, password) => {
        let httpMethod = 'POST'
        let strURL = checkCode
        let headers = {
            'Content-Type': 'application/json',
            'device_token': deviceToken,
            'device_type': Platform.OS === 'ios' ? 1 : 2,
            'build_version': '1.0',
        }
        let params = {
            'email': email,
            'code': code,
            'password': password,
        }

        APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
            console.log('Forgot Password -> ');
            console.log(resJSON)
            if (resJSON.status == 200) {
                this.props.navigation.pop(2)
                return
            } else {
                this.refs.toast.show(resJSON.message);
            }
        })
    }
    changePassword = () => {
        if (this.state.newpassword == this.state.reenterpassword) {
            var data = new FormData()
            data.append('token', this.state.token)
            data.append('login_id', this.state.login_id)
            data.append('old_password', this.state.oldpassword)
            data.append('new_password', this.state.newpassword)

            fetch(API_ROOT + 'profile_change_password', {
                method: 'post',
                body: data
            })
                .then((response) => response.json())
                .then((responseData) => {
                    if (responseData.success) {
                        //alert('Your password is changed sucessfully')
                           this.mixpanel.identify(store.getState().auth.user.email_address);
                           this.mixpanel.track('PasswordChange');
                           this.props.navigation.navigate('myprofile')
                        this.props.navigation.goBack()
                    } else {
                        this.refs.toast.show(responseData.text);
                    }
                })

                .catch((error) => {
                    this.refs.toast.show(rerror);
                })
        } else {
            alert('err')
        }

    }

    render() {
        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#FFF' }} >
                <SafeAreaView />

                <Content>
                    <FloatingLabel
                        labelStyle={[this.state.labelStyleOldpassword, this.state.oldpassword.length == 0 ? styles.labelInputUnFocus : '']}
                        inputStyle={styles.input}
                        style={[styles.formInput, this.state.styleOlPassword, { marginTop: 10 }]}
                        onChangeText={(oldpassword) => this.onChangeTextOldPassword(oldpassword)} value={this.state.oldpassword}
                        onBlur={() => this.onBlurCode()}
                        onFocus={() => this.onFocusCode()}
                        password={true}
                    >Old password</FloatingLabel>

                    <FloatingLabel
                        labelStyle={[this.state.labelStyleNewPassword, this.state.newpassword.length == 0 ? styles.labelInputUnFocus : '']}
                        inputStyle={styles.input}
                        style={[styles.formInput, this.state.styleNewPassword, { marginTop: 10 }]}
                        onChangeText={(newpassword) => this.onChangeTextNewPassword(newpassword)} value={this.state.newpassword}
                        onBlur={() => this.onBlurNewPassword()}
                        onFocus={() => this.onFocusNewPassword()}
                        password={true}
                    >New Password</FloatingLabel>

                    <FloatingLabel
                        labelStyle={[this.state.labelStyleRepassword, this.state.reenterpassword.length == 0 ? styles.labelInputUnFocus : '']}
                        inputStyle={styles.input}
                        style={[styles.formInput, this.state.styleRePassword, { marginTop: 10 }]}
                        onChangeText={(reenterpassword) => this.onChangeTextReenterPassword(reenterpassword)} value={this.state.reenterpassword}
                        onBlur={() => this.onBlurReNewPassword()}
                        onFocus={() => this.onFocusReNewPassword()}
                        password={true}
                    >Confirm New Password</FloatingLabel>


                </Content>
                <TouchableOpacity style={{ width: '103%', marginBottom: Height(5), alignSelf: 'center' }} onPress={this.changePassword}>
                    <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Change my password</Text>
                    </ImageBackground>
                </TouchableOpacity>
                <SafeAreaView />
            </Container>
        );
    }
}

export default change_password;
