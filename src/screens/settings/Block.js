import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, ImageBackground, TouchableOpacity, StatusBar, Text } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Button, Header, Left, Item, Icon, Right, Content, Input } from "native-base";
import styles from "./style";
import { getBlockList } from "../../redux/actions/auth";
import ValidationComponent from 'react-native-form-validator';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import { TouchableHighlight } from "react-native-gesture-handler";
const backWhite = require("../../../assets/images/backWhite.png");
const SelectRadio = require("../../../assets/images/selectRadio.png");
const btnBg = require("../../../assets/images/btnBg.png");
const btnBgGrey = require("../../../assets/images/Login/btnBgGrey.png");
import { API_ROOT } from "../../config/constant";
import { FRIENDLIST } from "../../redux/actions/types";
import { store } from "../../redux/store";
const flowers = require("../../../assets/images/Dashboard/avtar.png");

const search_img = require("../../../assets/images/Dashboard/search.png");
import RNProgressHUB from 'react-native-progresshub';
import MixpanelManager from '../../../Analytics'

class Block extends ValidationComponent {

    constructor(props) {
        super(props);
            this.arrayholder = [];
            this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
        this.state = {
            value: null,
            text: '',
            friendName:'',   
            token: store.getState().auth.user.token,
            login_id: store.getState().auth.user.login_id,     
            Friends: [],
            empty:true
        };
    }
    componentDidMount(){  
        const MixpanelData = {
            '$email' : store.getState().auth.user.email_address
          }
        this.mixpanel.identify(store.getState().auth.user.email_address);
        this.mixpanel.track('Profile Settings - Blocked', MixpanelData);
        let data = {
            token:this.state.token,
            login_id:this.state.login_id
        }
        RNProgressHUB.showSpinIndeterminate()
        getBlockList(data).then(res=>{
            if(res.success){
                this.setState({Friends:res.data})
                
            }
            else{
                this.setState({empty:false})
                
            }
            RNProgressHUB.dismiss()
          }).catch(err=>{
            RNProgressHUB.dismiss()
          })
                
      }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={[styles.headerAndroidnav,{height:44}]}>
                    <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack()}}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity></View>
                    <View style={{bottom: Height(2), position: 'absolute',alignItems:'center'}}>
                        <Text style={styles.headerTitle} >Blocked</Text></View>
                </Header>
            )
        }
    }

onItemPressed(item)
{
    AsyncStorage.setItem('other_id', item.other_id)
    this.props.navigation.navigate('ohter_user_profile')
}

goNext=()=>{
    AsyncStorage.setItem('receiver_id',this.state.value)
 this.props.navigation.navigate('Who2',{friendName:this.state.friendName})
}

    render() {
        const { value } = this.state;

        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }} >
                <Toastt ref="toast"></Toastt>
                <BackgroundImage >
                    <SafeAreaView />                   
                    <SafeAreaView>
                        <View style={{ backgroundColor: '#fff' }}>                        

                            <View style={{ height: Height(1.5) }} />
                            {this.state.empty == false ?
                            <Text style={{textAlign:'center',marginBottom:Height(15),alignItems:'center',justifyContent:'center',fontFamily:fonts.Raleway_Medium,FontSize:20,color:colors.fontDarkGrey}}></Text>:
                            <View style={{}}>
                                {this.state.Friends.map(item => {
                                    return (
                                        <View style={{}}>
                                            <TouchableHighlight onPress = {() => this.onItemPressed(item)} underlayColor='none'>
                                            <View  style={{ height: Height(5), flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white,marginBottom:'3%' }}>
                                                <View style={{ width: Width(15), alignItems: 'center', marginLeft:Width(1)}}>
                                                <View  underlayColor="transparent" 
                                                style={{ height: Height(4.5), width: Height(4.5), borderRadius: Height(4.5), overflow: 'hidden' }}>
                                                    <Image style={{ height: '100%', width: '100%' }}  source={item.photo == '' ? flowers:{ uri:item.photo }} />
                                                    </View>
                                                </View>
                                                <View style={{ width: Width(20),justifyContent: 'center', flex: 4.5 }}>
                                                    <Text style={{ fontSize: FontSize(15), color: colors.dargrey, fontFamily: fonts.Raleway_Bold }}> {item.name}</Text>
                                                </View>
                                            
                                            </View> 
                                                                                      
                                            </TouchableHighlight>
                                        </View>
                                    );
                                })}
                            </View>}

                        </View>

                    </SafeAreaView>
                  
                    
                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default Block;