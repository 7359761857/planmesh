import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Alert, ImageBackground,TouchableHighlight, TouchableOpacity, StatusBar, Text, Platform,TextInput } from "react-native";
import { Container, View, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FloatingLabel from 'react-native-floating-labels';
import { Height, Width, FontSize } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import Modal from "react-native-modal";
//import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';




const backWhite = require("../../../assets/images/backWhite.png");
const bottomRight = require("../../../assets/images/Login/bottomRight.png");
const btnBgFB = require("../../../assets/images/Login/btnBgFB.png");
const btnBgGrey = require("../../../assets/images/btnBg.png");
const btnBg = require("../../../assets/images/btnBg.png");

const menu1 = require("../../../assets/images/Dashboard/menu1.png");
const menu2 = require("../../../assets/images/Dashboard/menu2.png");
const menu3 = require("../../../assets/images/Dashboard/menu3.png");
const uncheck = require("../../../assets/images/unSelectRadio.png");
const current_location = require("../../../assets/images/current_location.png");

var subThis;
class Profile extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {
      labelStylePassword: styles.labelInputUnFocus, stylePassword: styles.formInputUnFocus, password: "", passwordValidation: true, passwordValidationMsg: "",
      labelStyleEmail: styles.labelInputUnFocus, styleEmail: styles.formInputUnFocus, email: "", emailValidation: true, emailValidationMsg: "",
      btnDoneDisable: true, btnDoneImage: btnBgGrey, wsError: false,
      data:[1,2,3,4]
    };
    subThis = this
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
      header: (
        <Header style={styles.headerAndroidnav}>
          <StatusBar barStyle="light-content" backgroundColor="#41199B" />
          <Left style={{ alignItems: 'flex-start', justifyContent: 'flex-start', flex: 0.2, }} >
            <Button transparent onPress={() => {
              navigation.goBack()
            }} style={{ width: 44, height: 44, marginLeft: 5 }} >
              <Image source={backWhite} style={{ width: 32, height: 23, tintColor: "white" }} />
            </Button>
          </Left>
          <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', flex: 0.7 }}>
            <Text style={[styles.headerTitle]} >Guest List (4)</Text>
          </View>
          <Right style={{ alignItems: 'flex-end', justifyContent: 'flex-end', flex: 0.25 }}>
            {/* <Button transparent style={{ alignSelf: 'baseline' }}  onPress={() => {}} >
            <Image source={settings} resizeMode="contain" style={{ width: 32, height: 23, tintColor: "white" }} />
            </Button> */}
          </Right>
        </Header>
      )
    }
  }
  renderItem = (item, rowMap) => {
    const read = item.unread === false;
    return (
      <SwipeRow leftOpenValue={0} rightOpenValue={-75}>
        <View style={styles.rowBack}>
        {/* <TouchableOpacity
              style={[styles.backRightBtn, styles.backRightBtnLeft]}
              
            >
            
              <Text style={styles.backTextBlack}>Read</Text>
            </TouchableOpacity> */}
          <TouchableOpacity
            style={[styles.backRightBtn, styles.backRightBtnRight]}
            onPress={() => {}}
          >
            
            <Text style={styles.backTextWhite}>Delete</Text>
          </TouchableOpacity>
         
        </View>
        <View style={{ height: Height(12), flexDirection: 'row', alignItems: 'center',backgroundColor:'#FFF',borderBottomColor:'#CCC',borderBottomWidth:Width(0.2) }}>
                  <View style={{ width: Width(25), alignItems: 'center', flex: 2 }}>
                    <TouchableHighlight  underlayColor="transparent" style={{ height: Height(7), width: Height(7), borderRadius: Height(7), overflow: 'hidden' }}>
                      <Image style={{ height: '100%', width: '100%' }} source={{ uri: 'https://i.imgur.com/j8U2QNy.png' }} />
                    </TouchableHighlight>
                  </View>
                  <View style={{ justifyContent: 'center', flex: 3 }}>
                    <Text style={{ fontSize: FontSize(18), color: '#000', fontWeight: 'bold' }}>Michele Lana</Text>
                    
                  </View>
                  <View style={{ alignItems: 'flex-end', right: Width(2), flex: 4 }}>
                    {/* <TouchableHighlight underlayColor="transparent" style={{}}>
                      <Entypo size={Width(8)} name="dots-three-horizontal" color="#000" />
                    </TouchableHighlight> */}
                  </View>
        </View>
        
        
      </SwipeRow>
    );
  };

 

  render() {
    let {isModalVisible} = this.state

    return (
      <View  style={{ flex: 1,backgroundColor:'#FFF' }} >
        
       
          <SafeAreaView />
            <SwipeListView
                useFlatList
                data={this.state.data || []}
                renderItem={(data, rowMap) => this.renderItem(data.item, rowMap)}
            />
          <SafeAreaView />
          <TouchableOpacity style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }} onPress={() => this.props.navigation.push('findFriends')} >
                     <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                       <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Find your friends</Text>
                     </ImageBackground>
                   </TouchableOpacity>
      </View>
    );
  }
}

export default Profile;
