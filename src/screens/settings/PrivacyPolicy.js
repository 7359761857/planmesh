import React, { Component } from "react";
import { SafeAreaView, Image,TouchableOpacity, StatusBar, Text} from "react-native";
import { WebView } from 'react-native-webview';
import { Container, View, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import { Height, Width, FontSize,colors } from "../../config/dimensions";
import { store } from "../../redux/store";
const backWhite = require("../../../assets/images/backWhite.png");

var subThis;
class PrivacyPolicy extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {
      login_id:store.getState().auth.user.login_id,
      token:store.getState().auth.user.token,
    };
    subThis = this
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
      header: (
        <Header style={[styles.headerAndroidnav,{height:44}]}>
            <StatusBar barStyle="light-content" backgroundColor="#41199B" />
            <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
                <TouchableOpacity onPress={() => { navigation.goBack() }}>
                    <Image source={backWhite} style={{ tintColor: "white" }} />
                </TouchableOpacity></View>
            <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                <Text style={styles.headerTitle} >Privacy Policy</Text>
            </View>
        </Header>
      )
    }
  }


  render() {
    
    return (
      <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#FFF' }} >
        <Toastt ref="toast"></Toastt>
          <SafeAreaView />
          <View style={{flex:1}}>
            <WebView source={{uri: "https://app.termly.io/document/privacy-policy/27e4071f-6a38-42ef-95c1-6b49ae632c7d"}} style = {{flex:1}} />
          </View>
          <SafeAreaView />
      </Container>
    );
  }
}

export default PrivacyPolicy;
