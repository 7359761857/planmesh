import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Alert, ImageBackground, TouchableOpacity, StatusBar, Text, Platform, TextInput, Switch } from "react-native";
import { Container, View, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import Ionicons from "react-native-vector-icons/Ionicons";
import FloatingLabel from 'react-native-floating-labels';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import Modal from "react-native-modal";
import RNProgressHUB from 'react-native-progresshub';



const backWhite = require("../../../assets/images/backWhite.png");
const bottomRight = require("../../../assets/images/Login/bottomRight.png");
const btnBgFB = require("../../../assets/images/Login/btnBgFB.png");
const btnBgGrey = require("../../../assets/images/btnBg.png");

const menu1 = require("../../../assets/images/Dashboard/menu1.png");
const menu2 = require("../../../assets/images/Dashboard/menu2.png");
const menu3 = require("../../../assets/images/Dashboard/menu3.png");
const uncheck = require("../../../assets/images/unSelectRadio.png");
const current_location = require("../../../assets/images/current_location.png");
import { API_ROOT } from "../../config/constant";
import { store } from "../../redux/store";
import MixpanelManager from '../../../Analytics'
var subThis;
class Profile extends ValidationComponent {

  constructor(props) {
    super(props);
    this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
    this.state = {
      token: store.getState().auth.user.token,
      login_id: store.getState().auth.user.login_id,
      on_day: true,
      before_day: true,
      before_1hour: true,
      before_30min: true,
      before_15min: true,


    };
  }

  componentDidMount() {
    
    RNProgressHUB.showSpinIndeterminate()
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('type', 3)

    fetch(API_ROOT + 'get/setting/detail', {
      method: 'post', body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          let data = responseData.data
          if (data.on_day == 1) {
            this.setState({ on_day: true })
          } else {
            this.setState({ on_day: false })

          }
          if (data.before_day == 1) {
            this.setState({ before_day: true })
          } else {
            this.setState({ before_day: false })

          } if (data.before_1hour == 1) {
            this.setState({ before_1hour: true })
          } else {
            this.setState({ before_1hour: false })

          } if (data.before_30min == 1) {
            this.setState({ before_30min: true })
          } else {
            this.setState({ before_30min: false })

          } if (data.before_15min == 1) {
            this.setState({ before_15min: true })
          } else {
            this.setState({ before_15min: false })

          }
          

        } else {
          

        }
        RNProgressHUB.dismiss()
      })
      .catch((error) => { 
        //alert(error)
        RNProgressHUB.dismiss()
      })
  }
  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
      header: (
        <Header style={[styles.headerAndroidnav,{height:44}]}>
          <StatusBar barStyle="light-content" backgroundColor="#41199B" />
          <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
            <TouchableOpacity onPress={() => { navigation.goBack() }}>
              <Image source={backWhite} style={{ tintColor: "white" }} />
            </TouchableOpacity></View>
          <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
            <Text style={styles.headerTitle} >Reminders</Text></View>

          <View style={{ bottom: Height(2), position: 'absolute', right: Width(4) }}>
            <Text style={{ color: 'white', opacity:0.0 }} >Done</Text>
          </View>
        </Header>
      )
    }
  }


  saveData = () => {
    var on_day = 0, before_day = 0, before_1hour = 0, before_30min = 0, before_15min = 0
    if (this.state.on_day) {
      on_day = 1
    } else {
      on_day = 0
    }
    if (this.state.before_day) {
      before_day = 1
    } else {
      before_day = 0
    } if (this.state.before_1hour) {
      before_1hour = 1
    } else {
      before_1hour = 0
    } if (this.state.before_30min) {
      before_30min = 1
    } else {
      before_30min = 0
    } if (this.state.before_15min) {
      before_15min = 1
    } else {
      before_15min = 0
    }
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('on_day', on_day)
    data.append('before_day', before_day)
    data.append('before_1hour', before_1hour)
    data.append('before_30min', before_30min)
    data.append('before_15min', before_15min)

    fetch(API_ROOT + 'reminders', {
      method: 'post', body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          const MixpanelData = {
            '$email' : store.getState().auth.user.email_address
          }
          this.mixpanel.identify(store.getState().auth.user.email_address);
          this.mixpanel.track('Profile Settings - Reminders', MixpanelData);
          this.props.navigation.goBack()
        } else {

        }
      })
      .catch((error) => {
         //alert(error)
         })
  }
  render() {
    let { isModalVisible } = this.state

    return (
      <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#F2F4F7' }} >
        <Toastt ref="toast"></Toastt>
        <BackgroundImage >
          <SafeAreaView />
          <Content>
            <View style={{ marginHorizontal: Width(4), marginVertical: Height(3) }} >
              <Text style={{ color: '#38336a', fontSize: FontSize(16), fontFamily: fonts.Raleway_Bold }}>
                Send me an event reminder
              </Text>
            </View>

            <View style={{ flexDirection: 'row', paddingHorizontal: Width(4), height: Height(6.5), alignItems: 'center', backgroundColor: '#fff' }}>
              <Text style={styles.settingText}>
                Day before the event</Text>
              {
                Platform.OS == 'ios' ?
                <Switch
                trackColor={{ true: "#4BD964" }}
                ios_backgroundColor = {'#E4E4E5'}
                value={this.state.on_day}
                onValueChange={(on_day) => this.setState({ on_day })} />
                :
                <Switch
                trackColor={{ true: "#4BD964",false:'#ccc' }}
                thumbTintColor='#ffffff'
                value={this.state.on_day}
                onValueChange={(on_day) => this.setState({ on_day })} />
              }
              
            </View>
            <View style={{ height: Height(.2), width: '100%', backgroundColor: colors.lightgrey }} />

            <View style={{ flexDirection: 'row', paddingHorizontal: Width(4), height: Height(6.5), alignItems: 'center', backgroundColor: '#fff' }}>
              <Text style={styles.settingText}>
                Day of the event</Text>
              {
                Platform.OS == 'ios' ?
                <Switch
                trackColor={{ true: "#4BD964" }}
                ios_backgroundColor = {'#E4E4E5'}
                value={this.state.before_day}
                onValueChange={(before_day) => this.setState({ before_day })} />
                :
                <Switch
                trackColor={{ true: "#4BD964" ,false:'#ccc'}}
                thumbTintColor='#ffffff'
                value={this.state.before_day}
                onValueChange={(before_day) => this.setState({ before_day })} />
              }
             
            </View>
            <View style={{ height: Height(.2), width: '100%', backgroundColor: colors.lightgrey }} />

            <View style={{ flexDirection: 'row', paddingHorizontal: Width(4), height: Height(6.5), alignItems: 'center', backgroundColor: '#fff' }}>
              <Text style={styles.settingText}>
                1 hour before</Text>
              {
                Platform.OS == 'ios' ?
                <Switch
                trackColor={{ true: "#4BD964" }}
                ios_backgroundColor={'#E4E4E5'}
                value={this.state.before_1hour}
                onValueChange={(before_1hour) => this.setState({ before_1hour })} />
                :
                <Switch
                trackColor={{ true: "#4BD964",false:'#ccc' }}
                thumbTintColor='#ffffff'
                value={this.state.before_1hour}
                onValueChange={(before_1hour) => this.setState({ before_1hour })} />
              }

              
            </View>

            <View style={{ height: Height(.2), width: '100%', backgroundColor: colors.lightgrey }} />


            <View style={{ flexDirection: 'row', paddingHorizontal: Width(4), height: Height(6.5), alignItems: 'center', backgroundColor: '#fff' }}>
              <Text style={styles.settingText}>
                30 mins before</Text>
              {
                Platform.OS == 'ios' ?
                <Switch
                trackColor={{ true: "#4BD964" }}
                ios_backgroundColor={'#E4E4E5'}
                value={this.state.before_30min}
                onValueChange={(before_30min) => this.setState({ before_30min })} />
                :
                <Switch
                trackColor={{ true: "#4BD964",false:'#ccc' }}
                thumbTintColor='#ffffff'
                value={this.state.before_30min}
                onValueChange={(before_30min) => this.setState({ before_30min })} />
              }
              
            </View>

            <View style={{ height: Height(.2), width: '100%', backgroundColor: colors.lightgrey }} />

            <View style={{ flexDirection: 'row', paddingHorizontal: Width(4), height: Height(6.5), alignItems: 'center', backgroundColor: '#fff' }}>
              <Text style={styles.settingText}>
                15 mins before</Text>
              {
                Platform.OS == 'ios' ?
                <Switch
                trackColor={{ true: "#4BD964" }}
                ios_backgroundColor={'#E4E4E5'}
                value={this.state.before_15min}
                onValueChange={(before_15min) => this.setState({ before_15min })} />
                :
                <Switch
                trackColor={{ true: "#4BD964" ,false:'#ccc'}}
                thumbTintColor='#ffffff'
                value={this.state.before_15min}
                onValueChange={(before_15min) => this.setState({ before_15min })} />
              }
              
            </View>

          </Content>
          <TouchableOpacity style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }} onPress={this.saveData} >
            <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }]}>Save</Text>
            </ImageBackground>
          </TouchableOpacity>
          <SafeAreaView />
        </BackgroundImage>
      </Container>
    );
  }
}

export default Profile;
