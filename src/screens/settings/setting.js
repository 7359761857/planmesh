import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Alert, ImageBackground, TouchableOpacity, StatusBar, Text, Platform, TextInput } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FloatingLabel from 'react-native-floating-labels';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts, API_ROOT } from "../../config/constant";
import Modal from "react-native-modal";
import NavigationService from "../../services/NavigationService";
import { store } from "../../redux/store";
import { LOGOUT } from "../../redux/actions/types";
import appleAuth, { AppleAuthRequestOperation } from '@invertase/react-native-apple-authentication';
import MixpanelManager from '../../../Analytics'
const btnBgGrey = require("../../../assets/images/btnBg.png");

const backWhite = require("../../../assets/images/backWhite.png");

var subThis;
class Profile extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {
      login_id: store.getState().auth.user.login_id,
      token: store.getState().auth.user.token,
      labelStylePassword: styles.labelInputUnFocus, stylePassword: styles.formInputUnFocus, password: "", passwordValidation: true, passwordValidationMsg: "",
      labelStyleEmail: styles.labelInputUnFocus, styleEmail: styles.formInputUnFocus, email: "", emailValidation: true, emailValidationMsg: "",
      btnDoneDisable: true, btnDoneImage: btnBgGrey, wsError: false
    };
    subThis = this
    this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
      header: (
        <Header style={[styles.headerAndroidnav, { height: 44 }]}>
          <StatusBar barStyle="light-content" backgroundColor="#41199B" />
          <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
            <TouchableOpacity onPress={() => { navigation.goBack() }}>
              <Image source={backWhite} style={{ tintColor: "white" }} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { navigation.goBack() }}>
              <View backgroundColor="transparent" position='absolute' height={70} width={70} marginTop={-30} marginLeft={-30} >
              </View>
            </TouchableOpacity>
          </View>
          <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
            <Text style={styles.headerTitle} >Settings</Text>
          </View>

        </Header>
      )
    }
  }

  updateNotiCount() {
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('noti_count', global.noti_count)

    //alert('NotiCount Request: '+ JSON.stringify(data))

    fetch(API_ROOT + 'add_noti_count', {
      method: 'post',
      body: data
    }).then((response) => response.json())
      .then((responseData) => {
        //alert(JSON.stringify(responseData))
        if (responseData.success) {
          console.log('noti count updated')
        } else {

        }
      })
      .catch((error) => {
        //alert(error)
      })
  }

  async appleLogout() {
    const appleAuthRequestResponse = await appleAuth.performRequest({
      requestedOperation: AppleAuthRequestOperation.LOGOUT,
      user: global.apple_user_id,
    });

    console.log('Apple Logout: ' + appleAuthRequestResponse)
  }

  onLogoutClick = () => {

    //this.appleLogout()

    this.updateNotiCount()

    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)

    fetch(API_ROOT + 'logout', {
      method: 'post',
      body: data
    }).then((response) => response.json())
      .then((responseData) => {
        //alert(responseData)
        if (responseData.success) {

          global.noti_count = 0
          store.dispatch({ type: LOGOUT })
          AsyncStorage.clear()
          NavigationService.reset('Home')
          //subThis.props.navigation.navigate('Auth')

        } else {

        }
      })
      .catch((error) => {
        //alert(error)
      })
  }


  render() {
    let { isModalVisible } = this.state
    let { navigation } = this.props
    return (
      <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#FFF' }} >
        <Toastt ref="toast"></Toastt>
        <BackgroundImage >
          <SafeAreaView />


          <Content>

            <TouchableOpacity onPress={() => {
              const MixpanelData = {
                '$email': store.getState().auth.user.email_address
              }
              this.mixpanel.identify(store.getState().auth.user.email_address);
              this.mixpanel.track('Profile Settings - Accounts', MixpanelData);
              navigation.navigate('account')
            }} style={{ marginVertical: Height(2), backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center', marginHorizontal: Width(4) }}>

              <Text style={{ flex: 1, color: '#363169', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Account</Text>
              <AntDesign name="right" color="#C7C7CC" size={16} />
            </TouchableOpacity>
            <View style={{ borderBottomColor: '#CCC', borderBottomWidth: Width(0.15) }} />
            <TouchableOpacity onPress={() => navigation.navigate('notifications')} style={{ marginVertical: Height(2), backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center', marginHorizontal: Width(4) }}>
              <Text style={{ flex: 1, color: '#363169', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Notifications</Text>
              <AntDesign name="right" color="#C7C7CC" size={16} />
            </TouchableOpacity>

            <View style={{ borderBottomColor: '#CCC', borderBottomWidth: Width(0.15) }} />
            <TouchableOpacity onPress={() => navigation.navigate('reminders')} style={{ marginVertical: Height(2), backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center', marginHorizontal: Width(4) }}>
              <Text style={{ flex: 1, color: '#363169', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Reminders</Text>
              <AntDesign name="right" color="#C7C7CC" size={16} />
            </TouchableOpacity>

            <View style={{ borderBottomColor: '#CCC', borderBottomWidth: Width(0.15) }} />
            <TouchableOpacity onPress={() => {
              const MixpanelData = {
                '$email': store.getState().auth.user.email_address
              }
              this.mixpanel.identify(store.getState().auth.user.email_address);
              this.mixpanel.track('Profile Settings - Find Friends', MixpanelData);
              navigation.navigate('FindFriends_Setting')
            }} style={{ marginVertical: Height(2), backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center', marginHorizontal: Width(4) }}>
              <Text style={{ flex: 1, color: '#363169', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Find Friends</Text>
              <AntDesign name="right" color="#C7C7CC" size={16} />
            </TouchableOpacity>

            <View style={{ borderBottomColor: '#CCC', borderBottomWidth: Width(0.15) }} />
            <TouchableOpacity onPress={() => navigation.navigate('friendslist')} style={{ marginVertical: Height(2), backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center', marginHorizontal: Width(4) }}>
              <Text style={{ flex: 1, color: '#363169', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Friends</Text>
              <AntDesign name="right" color="#C7C7CC" size={16} />
            </TouchableOpacity>

            <View style={{ borderBottomColor: '#CCC', borderBottomWidth: Width(0.15) }} />
            <TouchableOpacity onPress={() => navigation.navigate('Privacy')} style={{ marginVertical: Height(2), backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center', marginHorizontal: Width(4) }}>
              <Text style={{ flex: 1, color: '#363169', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Privacy</Text>
              <AntDesign name="right" color="#C7C7CC" size={16} />
            </TouchableOpacity>

            <View style={{ borderBottomColor: '#CCC', borderBottomWidth: Width(0.15) }} />
            <TouchableOpacity onPress={() => navigation.navigate('Block')} style={{ marginVertical: Height(2), backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center', marginHorizontal: Width(4) }}>
              <Text style={{ flex: 1, color: '#363169', fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}>Blocked</Text>
              <AntDesign name="right" color="#C7C7CC" size={16} />
            </TouchableOpacity>

            <View style={{ borderBottomColor: '#CCC', borderBottomWidth: Width(0.15) }} />


          </Content>
          <TouchableOpacity style={{ width: '100%' }} onPress={() => this.onLogoutClick()} >
            <ImageBackground source={this.state.btnDoneImage} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Logout
                                </Text>
            </ImageBackground>
          </TouchableOpacity>

          <SafeAreaView />
        </BackgroundImage>
      </Container>
    );
  }
}

export default Profile;
