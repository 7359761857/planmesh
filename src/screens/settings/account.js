import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, Alert, ImageBackground, TouchableOpacity, StatusBar, Text, Platform,TextInput } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FloatingLabel from 'react-native-floating-labels';
import { Height, Width, FontSize,colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import Modal from "react-native-modal";




const backWhite = require("../../../assets/images/backWhite.png");
const bottomRight = require("../../../assets/images/Login/bottomRight.png");
const btnBgFB = require("../../../assets/images/Login/btnBgFB.png");
const btnBgGrey = require("../../../assets/images/btnBg.png");

const menu1 = require("../../../assets/images/Dashboard/menu1.png");
const menu2 = require("../../../assets/images/Dashboard/menu2.png");
const menu3 = require("../../../assets/images/Dashboard/menu3.png");
const uncheck = require("../../../assets/images/unSelectRadio.png");
const current_location = require("../../../assets/images/current_location.png");

var subThis;
import MixpanelManager from '../../../Analytics'
class Profile extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {
      labelStylePassword: styles.labelInputUnFocus, stylePassword: styles.formInputUnFocus, password: "", passwordValidation: true, passwordValidationMsg: "",
      labelStyleEmail: styles.labelInputUnFocus, styleEmail: styles.formInputUnFocus, email: "", emailValidation: true, emailValidationMsg: "",
      btnDoneDisable: true, btnDoneImage: btnBgGrey, wsError: false,
      email:'',
      mobile:'',
      name:'',
      phn_code:''
    };
    subThis = this
    this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
  }

  async componentDidMount(){
    await AsyncStorage.getItem('name').then((value)=>{
      this.setState({name:value})
    });
    await AsyncStorage.getItem('email').then((value)=>{
      this.setState({email:value})
    });
   await AsyncStorage.getItem('mobile').then((value)=>{
    this.setState({mobile:value})
       })
       await AsyncStorage.getItem('mobile_code').then((value)=>{
        this.setState({phn_code:value})
           })
 
  }
  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
      header: (
        // <Header style={styles.headerAndroidnav}>
        //   <StatusBar barStyle="light-content" backgroundColor="#41199B" />
        //   <Left style={{ alignItems: 'flex-start', justifyContent: 'flex-start', flex: 0.2, zIndex:1}} >
        //     <Button transparent onPress={() => {
        //       navigation.goBack()
        //     }} style={{ width: 44, height: 44, marginLeft: 5 }} >
        //       <Image source={backWhite} style={{  tintColor: "white" }} />
        //     </Button>
        //   </Left>
        //   <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', width:'100%' }}>
        //     <Text style={[styles.headerTitle]} >Account</Text>
        //   </View>
        //   <Right style={{ alignItems: 'flex-end', justifyContent: 'flex-end', flex: 0.25 }}>
        //   </Right>
        // </Header>

                <Header style={[styles.headerAndroidnav,{height:44}]}>
                    <StatusBar barStyle="light-content" backgroundColor={colors.appColor} />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(5) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack()}}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity></View>
                    <View style={{bottom: Height(2), position: 'absolute',alignItems:'center'}}>
                        <Text style={styles.headerTitle} >Account</Text></View>
                </Header>
      )
    }
  }

 

  render() {
    let {isModalVisible} = this.state

    return (
      <Container flex-direction={"row"} style={{ flex: 1,backgroundColor:'#EFEFF4' }} >
        <Toastt ref="toast"></Toastt>
        <BackgroundImage >
          <SafeAreaView />

          
          <Content>
            <View style={{height:Height(2)}}/>
            <TouchableOpacity style={{backgroundColor:'#FFF',flexDirection:'row',alignItems:'center'}}>
                <Text style={{margin:Width(4),color:colors.whatFontColor,fontFamily:fonts.Roboto_Regular,fontSize:FontSize(16)}}>Full name</Text>
                <Text style={{position:'absolute',right:Width(4),fontFamily:fonts.Roboto_Regular,fontSize:FontSize(16),color:colors.fontDarkGrey}}>{this.state.name}</Text>
            </TouchableOpacity>
            <View style={{borderBottomColor:'#CCC',borderBottomWidth:Width(0.3)}}/>
            <TouchableOpacity style={{backgroundColor:'#FFF',flexDirection:'row',alignItems:'center'}}>
                <Text style={{margin:Width(4),color:colors.whatFontColor,fontFamily:fonts.Roboto_Regular,fontSize:FontSize(16), width:Width(20)}}>Email</Text>
                {/* <Text style={{position:'absolute',right:Width(4),fontFamily:fonts.Roboto_Regular,fontSize:FontSize(16),color:colors.fontDarkGrey}}>{this.state.email}</Text> */}
                <Text style={{position:'absolute',right:Width(4),fontFamily:fonts.Roboto_Regular,fontSize:FontSize(16),color:colors.fontDarkGrey,width:Width(70),textAlign:'right'}} numberOfLines = {1}>{this.state.email}</Text>
            </TouchableOpacity>
            <View style={{borderBottomColor:'#CCC',borderBottomWidth:Width(0.3)}}/>
            <TouchableOpacity style={{backgroundColor:'#FFF',flexDirection:'row',alignItems:'center'}}>
                <Text style={{margin:Width(4),color:colors.whatFontColor,fontFamily:fonts.Roboto_Regular,fontSize:FontSize(16)}}>Phone Number</Text>
                <Text style={{position:'absolute',right:Width(4),fontFamily:fonts.Roboto_Regular,fontSize:FontSize(16),color:colors.fontDarkGrey}}>{this.state.phn_code}{this.state.mobile}</Text>
            </TouchableOpacity>
            <View style={{height:Height(3)}}/>
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('change_password')}} style={{backgroundColor:'#FFF',flexDirection:'row',alignItems:'center'}}>
                <Text style={{margin:Width(4),color:colors.whatFontColor,fontFamily:fonts.Roboto_Regular,fontSize:FontSize(16)}}>Change your password</Text>
                <AntDesign style={{position:'absolute',right:Width(4)}} name="right" color={colors.fontDarkGrey} size={20}/>
            </TouchableOpacity> 
           

            
            
          </Content>
          {/* <TouchableOpacity style={{ width: '103%', marginBottom: Height(2),alignSelf:'center' }} onPress={() => {this.props.navigation.goBack()}} >
              <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Save</Text>
              </ImageBackground>
            </TouchableOpacity> */}
          <SafeAreaView />
        </BackgroundImage>
      </Container>
    );
  }
}

export default Profile;
