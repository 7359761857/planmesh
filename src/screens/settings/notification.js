import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Alert, ImageBackground, TouchableOpacity, StatusBar, Text, Platform, TextInput, Switch } from "react-native";
import { Container, View, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import { API_ROOT } from "../../config/constant";
import { store } from "../../redux/store";
import { Height, Width, FontSize } from "../../config/dimensions";

import RNProgressHUB from 'react-native-progresshub';


const backWhite = require("../../../assets/images/backWhite.png");

const btnBgGrey = require("../../../assets/images/btnBg.png");

import MixpanelManager from '../../../Analytics'

var subThis;
class Profile extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {
      btnDoneImage: btnBgGrey,
      is_invite: true,
      is_intrested_my_plan: true,
      is_goin_my_plan: true,
      is_intrested_other_going: true,
      is_suggestion_my_plan: true,
      is_intrested_plan_suggestion: true,
      is_chat_my_plan: true,
      is_intrested_plan_chat: true,
      is_friend_request: true,
      is_friend_request_accept: true,
      is_upvote_my_plan: true,
      is_intrested_other_upvote:true,
      is_set_final_suggestion:true,
      is_invite_their_plan:true,
      is_invite_my_plan:true,
      token: store.getState().auth.user.token,
      login_id: store.getState().auth.user.login_id,
    };
    this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
    subThis = this
  }

  componentDidMount() {
    

    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('type', 2)
    RNProgressHUB.showSpinIndeterminate()
    fetch(API_ROOT + 'get/setting/detail', {
      method: 'post', body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          let data = responseData.data
          if (data.is_invite == 1) {
            this.setState({ is_invite: true })
          } else {
            this.setState({ is_invite: false })

          }
          if (data.is_intrested_my_plan == 1) {
            this.setState({ is_intrested_my_plan: true })
          } else {
            this.setState({ is_intrested_my_plan: false })

          } if (data.is_goin_my_plan == 1) {
            this.setState({ is_goin_my_plan: true })
          } else {
            this.setState({ is_goin_my_plan: false })

          } if (data.is_intrested_other_going == 1) {
            this.setState({ is_intrested_other_going: true })
          } else {
            this.setState({ is_intrested_other_going: false })

          } if (data.is_suggestion_my_plan == 1) {
            this.setState({ is_suggestion_my_plan: true })
          } else {
            this.setState({ is_suggestion_my_plan: false })

          }
          if (data.is_intrested_plan_suggestion == 1) {
            this.setState({ is_intrested_plan_suggestion: true })
          } else {
            this.setState({ is_intrested_plan_suggestion: false })

          }
          if (data.is_chat_my_plan == 1) {
            this.setState({ is_chat_my_plan: true })
          } else {
            this.setState({ is_chat_my_plan: false })

          }
          if (data.is_intrested_plan_chat == 1) {
            this.setState({ is_intrested_plan_chat: true })
          } else {
            this.setState({ is_intrested_plan_chat: false })

          }if (data.is_friend_request == 1) {
            this.setState({ is_friend_request: true })
          } else {
            this.setState({ is_friend_request: false })

          }if (data.is_friend_request_accept == 1) {
            this.setState({ is_friend_request_accept: true })
          } else {
            this.setState({ is_friend_request_accept: false })

          }

          if (data.is_upvote_my_plan == 1) {
            this.setState({ is_upvote_my_plan: true })
          } else {
            this.setState({ is_upvote_my_plan: false })

          }

          if (data.is_intrested_other_upvote == 1) {
            this.setState({ is_intrested_other_upvote: true })
          } else {
            this.setState({ is_intrested_other_upvote: false })

          }

          if (data.is_set_final_suggestion == 1) {
            this.setState({ is_set_final_suggestion: true })
          } else {
            this.setState({ is_set_final_suggestion: false })

          }

          if (data.is_invite_my_plan == 1) {
            this.setState({ is_invite_my_plan: true })
          } else {
            this.setState({ is_invite_my_plan: false })

          }

          if (data.is_invite_their_plan == 1) {
            this.setState({ is_invite_their_plan: true })
          } else {
            this.setState({ is_invite_their_plan: false })

          }

          

        } else {
          

        }
        RNProgressHUB.dismiss()
      })
      .catch((error) => { 
        //alert(error)
        RNProgressHUB.dismiss()
      })
  }
  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation;
    return {
      header: (
        <Header style={[styles.headerAndroidnav,{height:44}]}>
          <StatusBar barStyle="light-content" backgroundColor="#41199B" />
          <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
            <TouchableOpacity onPress={() => { navigation.goBack() }}>
              <Image source={backWhite} style={{ tintColor: "white" }} />
            </TouchableOpacity></View>
          <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
            <Text style={styles.headerTitle} >Notifications</Text></View>

        </Header>
      )
    }
  }

  is_invite_change = (value) => {
    this.setState({ is_invite: value })
  }
  saveData = () => {
    var invite = 0, intrested_my_plan = 0, goin_my_plan = 0, intrested_other_going = 0, suggestion_my_plan = 0,
      intrested_plan_suggestion = 0, chat_my_plan = 0, intrested_plan_chat = 0, friend_request = 0, friend_request_accept = 0,
      is_upvote_my_plan = 0 , is_intrested_other_upvote = 0, is_set_final_suggestion = 0, is_invite_my_plan = 0, is_invite_their_plan = 0
    if (this.state.is_invite) {
      invite = 1
    } else {
      invite = 0
    }
    if (this.state.is_intrested_my_plan) {
      intrested_my_plan = 1
    } else {
      intrested_my_plan = 0
    }
    if (this.state.is_goin_my_plan) {
      goin_my_plan = 1
    } else {
      goin_my_plan = 0
    }
    if (this.state.is_intrested_other_going) {
      intrested_other_going = 1
    } else {
      intrested_other_going = 0
    }
    if (this.state.is_suggestion_my_plan) {
      suggestion_my_plan = 1
    } else {
      suggestion_my_plan = 0
    }
    if (this.state.is_intrested_plan_suggestion) {
      intrested_plan_suggestion = 1
    } else {
      intrested_plan_suggestion = 0
    }
    if (this.state.is_chat_my_plan) {
      chat_my_plan = 1
    } else {
      chat_my_plan = 0
    }
    if (this.state.is_intrested_plan_chat) {
      intrested_plan_chat = 1
    } else {
      intrested_plan_chat = 0
    }
    if (this.state.is_friend_request) {
      friend_request = 1
    } else {
      friend_request = 0
    }
    if (this.state.is_friend_request_accept) {
      friend_request_accept = 1
    } else {
      friend_request_accept = 0
    }

    if (this.state.is_upvote_my_plan) {
      is_upvote_my_plan = 1
    } else {
      is_upvote_my_plan = 0
    }
    if (this.state.is_intrested_other_upvote) {
      is_intrested_other_upvote = 1
    } else {
      is_intrested_other_upvote = 0
    }

    if (this.state.is_set_final_suggestion) {
      is_set_final_suggestion = 1
    } else {
      is_set_final_suggestion = 0
    }

    if (this.state.is_invite_my_plan) {
      is_invite_my_plan = 1
    } else {
      is_invite_my_plan = 0
    }

    if (this.state.is_invite_their_plan) {
      is_invite_their_plan = 1
    } else {
      is_invite_their_plan = 0
    }

//
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('is_invite', invite)
    data.append('is_intrested_my_plan', intrested_my_plan)
    data.append('is_goin_my_plan', goin_my_plan)
    data.append('is_intrested_other_going', intrested_other_going)

    data.append('is_suggestion_my_plan', suggestion_my_plan)
    data.append('is_intrested_plan_suggestion', intrested_plan_suggestion)
    data.append('is_chat_my_plan', chat_my_plan)
    data.append('is_intrested_plan_chat', intrested_plan_chat)
    data.append('is_friend_request', friend_request)
    data.append('is_friend_request_accept', friend_request_accept)
    data.append('is_upvote_my_plan', is_upvote_my_plan)
    data.append('is_intrested_other_upvote', is_intrested_other_upvote)
    data.append('is_set_final_suggestion', is_set_final_suggestion)
    data.append('is_invite_my_plan', is_invite_my_plan)
    data.append('is_invite_their_plan', is_invite_their_plan)

    fetch(API_ROOT + 'notification/setting', {
      method: 'post', body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          this.mixpanel.identify(store.getState().auth.user.email_address);
          this.mixpanel.track('Profile Settings - Notifications');
          this.props.navigation.goBack()
        } else {
        }
      })
      .catch((error) => { 
        //alert(error) 
      })


  }
  render() {

    return (
      <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#FFF' }} >
        <Toastt ref="toast"></Toastt>
        <BackgroundImage >
          <SafeAreaView />


          <Content>
            <View style={styles.headingView}>
              <Text style={styles.headingText}>
                Events
              </Text>
            </View>

            <View style={{ marginHorizontal: Width(4), alignItems: 'center', flexDirection: 'row', height: Height(7), justifyContent: 'center', }}>
              <Text style={styles.text}>
                Sends you an invite
                          </Text>
              {Platform.OS == 'ios' ? 
                <Switch trackColor={{ true: '#4bd964'}}
                ios_backgroundColor = {'#E4E4E5'}    
                value={this.state.is_invite}
                onValueChange={is_invite => this.setState({ is_invite })} />
              :
                <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964',false: '#ccc' }}
                  value={this.state.is_invite}
                  onValueChange={is_invite => this.setState({ is_invite })} />
              }

            </View>
            <View style={styles.bottomVIew}></View>

            <View style={{ marginHorizontal: Width(4), alignItems: 'center', flexDirection: 'row', height: Height(7), justifyContent: 'center', }}>
              <Text style={styles.text}>
                Interested in your invite
                          </Text>
              {/* <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964' }} */}

              {Platform.OS == 'ios' ? 
                
                <Switch trackColor={{ true: '#4bd964'}}
                ios_backgroundColor = {'#E4E4E5'} 
                value={this.state.is_intrested_my_plan}
                onValueChange={is_intrested_my_plan => this.setState({ is_intrested_my_plan })} />
                :
                <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964',false: '#ccc' }}
                value={this.state.is_intrested_my_plan}
                onValueChange={is_intrested_my_plan => this.setState({ is_intrested_my_plan })} />

              }

            </View>
            <View style={styles.bottomVIew}></View>

            <View style={{ marginHorizontal: Width(4), alignItems: 'center', flexDirection: 'row', height: Height(7), justifyContent: 'center', }}>
              <Text style={styles.text}>
                Going to your event
                          </Text>
              {/* <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964'}} */}

              {
                Platform.OS == 'ios' ?
                <Switch trackColor={{ true: '#4bd964'}}
                ios_backgroundColor = {'#E4E4E5'} 
                value={this.state.is_goin_my_plan}
                onValueChange={(is_goin_my_plan) => this.setState({ is_goin_my_plan })} />
                :
                <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964',false: '#ccc'}}
                value={this.state.is_goin_my_plan}
                onValueChange={(is_goin_my_plan) => this.setState({ is_goin_my_plan })} />
              }

            </View>
            <View style={styles.bottomVIew}></View>

            <View style={{ marginHorizontal: Width(4), alignItems: 'center', flexDirection: 'row', height: Height(7), justifyContent: 'center', }}>
              <Text style={styles.text}>
                Going to an event you’re interested in or going to
                          </Text>
              {/* <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964'}} */}

              {
                Platform.OS == 'ios' ? 

                <Switch trackColor={{ true: '#4bd964'}}
                ios_backgroundColor = {'#E4E4E5'} 
                value={this.state.is_intrested_other_going}
                onValueChange={(is_intrested_other_going) => this.setState({ is_intrested_other_going })} />

                :

                <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964',false: '#ccc'}}
                value={this.state.is_intrested_other_going}
                onValueChange={(is_intrested_other_going) => this.setState({ is_intrested_other_going })} />
              }

            </View>
            <View style={styles.bottomVIew}></View>
            <View style={{ marginHorizontal: Width(4), alignItems: 'center', flexDirection: 'row', height: Height(7), justifyContent: 'center', }}>
              <Text style={styles.text}>
                Activity, location or date & time suggested for your event
                          </Text>
              {/* <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964'}} */}
              {
                Platform.OS == 'ios' ?
                <Switch trackColor={{ true: '#4bd964'}}
                ios_backgroundColor = {'#E4E4E5'} 
                value={this.state.is_suggestion_my_plan}
                onValueChange={(is_suggestion_my_plan) => this.setState({ is_suggestion_my_plan })} />
                :
                <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964',false: '#ccc'}}
                value={this.state.is_suggestion_my_plan}
                onValueChange={(is_suggestion_my_plan) => this.setState({ is_suggestion_my_plan })} />
              }
              
            </View>
            <View style={styles.bottomVIew}></View>

            <View style={{ marginHorizontal: Width(4), alignItems: 'center', flexDirection: 'row', height: Height(9), justifyContent: 'center', }}>
              <Text style={styles.text}>
                Activity, location or date & time suggested to event you're interested in or going to
                          </Text>
              {/* <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964'}} */}

              {Platform.OS == 'ios' ?

                  <Switch trackColor={{ true: '#4bd964'}}
                  ios_backgroundColor = {'#E4E4E5'} 
                  value={this.state.is_intrested_plan_suggestion}
                  onValueChange={(is_intrested_plan_suggestion) => this.setState({ is_intrested_plan_suggestion })} />
                  :
                  <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964',false: '#ccc'}}
                value={this.state.is_intrested_plan_suggestion}
                onValueChange={(is_intrested_plan_suggestion) => this.setState({ is_intrested_plan_suggestion })} />
              }
            </View>
            <View style={styles.bottomVIew}></View>
            <View style={{ marginHorizontal: Width(4), alignItems: 'center', flexDirection: 'row', height: Height(7), justifyContent: 'center', }}>
              <Text style={styles.text}>
                Activity, location or date & time added to event you're interested in or going to
                          </Text>
              {/* <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964'}} */}

              {
                Platform.OS == 'ios' ? 

                <Switch trackColor={{ true: '#4bd964'}}
                ios_backgroundColor = {'#E4E4E5'} 
                value={this.state.is_set_final_suggestion}
                onValueChange={(is_set_final_suggestion) => this.setState({ is_set_final_suggestion })} />
                :

                <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964',false: '#ccc'}}
                value={this.state.is_set_final_suggestion}
                onValueChange={(is_set_final_suggestion) => this.setState({ is_set_final_suggestion })} />
              }

              
            </View>
            <View style={{ backgroundColor: '#FFF', height: Height(1) }}></View>

            <View style={styles.headingView}>
              <Text style={styles.headingText}>
                Upvotes
            </Text>
            </View>

            <View style={{ marginHorizontal: Width(4), alignItems: 'center', flexDirection: 'row', height: Height(7), justifyContent: 'center', }}>
              <Text style={styles.text}>
                Activity, location or date & time upvoted for your event
              </Text>
              {/* <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964'}} */}
              {
                Platform.OS == 'ios' ?
                <Switch trackColor={{ true: '#4bd964'}}
                ios_backgroundColor = {'#E4E4E5'} 
                value={this.state.is_upvote_my_plan}
                onValueChange={(is_upvote_my_plan) => this.setState({ is_upvote_my_plan })} />
                :
                <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964',false: '#ccc'}}
                value={this.state.is_upvote_my_plan}
                onValueChange={(is_upvote_my_plan) => this.setState({ is_upvote_my_plan })} />
              }
              
            </View>
            <View style={styles.bottomVIew}></View>

            <View style={{ marginHorizontal: Width(4), alignItems: 'center', flexDirection: 'row', height: Height(7), justifyContent: 'center', }}>
              <Text style={styles.text}>
              Activity, location or date & time upvoted for an event you’re interested in or going to
                          </Text>
              {/* <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964'}} */}
              {
                Platform.OS == 'ios' ? 
                <Switch trackColor={{ true: '#4bd964'}}
                ios_backgroundColor = {'#E4E4E5'} 
                value={this.state.is_intrested_other_upvote}
                onValueChange={(is_intrested_other_upvote) => this.setState({ is_intrested_other_upvote })} />
                :
                <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964',false: '#ccc'}}
                value={this.state.is_intrested_other_upvote}
                onValueChange={(is_intrested_other_upvote) => this.setState({ is_intrested_other_upvote })} />
              }
              
            </View>
            <View style={styles.bottomVIew}></View>

            <View style={styles.headingView}>
              <Text style={styles.headingText}>
                Chat
            </Text>
            </View>

            <View style={{ marginHorizontal: Width(4), alignItems: 'center', flexDirection: 'row', height: Height(7), justifyContent: 'center', }}>
              <Text style={styles.text}>
                Chats about your event
                                        </Text>
              {/* <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964'}} */}
              {
                Platform.OS == 'ios' ?
                <Switch trackColor={{ true: '#4bd964'}}
                ios_backgroundColor = {'#E4E4E5'} 
                value={this.state.is_chat_my_plan}
                onValueChange={(is_chat_my_plan) => this.setState({ is_chat_my_plan })} />
                :
                <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964',false: '#ccc'}}
                value={this.state.is_chat_my_plan}
                onValueChange={(is_chat_my_plan) => this.setState({ is_chat_my_plan })} />
              }
              
            </View>
            <View style={styles.bottomVIew}></View>

            <View style={{ marginHorizontal: Width(4), alignItems: 'center', flexDirection: 'row', height: Height(7), justifyContent: 'center', }}>
              <Text style={styles.text}>
                Chats about an event you're interest in or going to
                          </Text>
              {/* <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964'}} */}
              {
                Platform.OS == 'ios' ?
                <Switch trackColor={{ true: '#4bd964'}}
                ios_backgroundColor = {'#E4E4E5'} 
                value={this.state.is_intrested_plan_chat}
                onValueChange={(is_intrested_plan_chat) => this.setState({ is_intrested_plan_chat })} />
                :
                <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964',false: '#ccc'}}
                value={this.state.is_intrested_plan_chat}
                onValueChange={(is_intrested_plan_chat) => this.setState({ is_intrested_plan_chat })} />
              }
              
            </View>
            <View style={styles.bottomVIew}></View>

            <View style={styles.headingView}>
              <Text style={styles.headingText}>
                Friend Requests
                       </Text>
            </View>

            <View style={{ marginHorizontal: Width(4), alignItems: 'center', flexDirection: 'row', height: Height(7), justifyContent: 'center', }}>
              <Text style={styles.text}>
                Sends you a friend request
                          </Text>
              {/* <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964'}} */}
              {
                Platform.OS == 'ios' ?
                <Switch trackColor={{ true: '#4bd964'}}
                ios_backgroundColor = {'#E4E4E5'} 
                value={this.state.is_friend_request}
                onValueChange={(is_friend_request) => this.setState({ is_friend_request })} />
                :
                <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964',false: '#ccc'}}
                value={this.state.is_friend_request}
                onValueChange={(is_friend_request) => this.setState({ is_friend_request })} />
              }
              
            </View>
            <View style={styles.bottomVIew}></View>

            <View style={{ marginHorizontal: Width(4), alignItems: 'center', flexDirection: 'row', height: Height(7), justifyContent: 'center', }}>
              <Text style={styles.text}>
                Accepts your friend request
                          </Text>
              {/* <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964'}} */}
              {
                Platform.OS == 'ios' ?
                <Switch trackColor={{ true: '#4bd964'}}
                ios_backgroundColor = {'#E4E4E5'} 
                value={this.state.is_friend_request_accept}
                onValueChange={(is_friend_request_accept) => this.setState({ is_friend_request_accept })} />
                :
                <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964',false: '#ccc'}}
                value={this.state.is_friend_request_accept}
                onValueChange={(is_friend_request_accept) => this.setState({ is_friend_request_accept })} />
              }
              
            </View>

            <View style={styles.headingView}>
              <Text style={styles.headingText}>
                Invites
                       </Text>
            </View>

            <View style={{ marginHorizontal: Width(4), alignItems: 'center', flexDirection: 'row', height: Height(7), justifyContent: 'center', }}>
              <Text style={styles.text}>
                Invites you to their event
                          </Text>
              {/* <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964'}} */}
              {
                Platform.OS == 'ios' ?
                <Switch trackColor={{ true: '#4bd964'}}
                ios_backgroundColor = {'#E4E4E5'} 
                value={this.state.is_invite_my_plan}
                onValueChange={(is_invite_my_plan) => this.setState({ is_invite_my_plan })} />
                :
                <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964',false: '#ccc'}}
                value={this.state.is_invite_my_plan}
                onValueChange={(is_invite_my_plan) => this.setState({ is_invite_my_plan })} />
              }
              
            </View>
            <View style={styles.bottomVIew}></View>

            <View style={{ marginHorizontal: Width(4), alignItems: 'center', flexDirection: 'row', height: Height(7), justifyContent: 'center', }}>
              <Text style={styles.text}>
              Invites you to a friend's event
                          </Text>
              {/* <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964'}} */}
              {
                Platform.OS == 'ios' ?
                <Switch trackColor={{ true: '#4bd964'}}
                ios_backgroundColor = {'#E4E4E5'} 
                value={this.state.is_invite_their_plan}
                onValueChange={(is_invite_their_plan) => this.setState({ is_invite_their_plan })} />
                :
                <Switch thumbTintColor={'#ffffff'} trackColor={{ true: '#4bd964',false: '#ccc'}}
                value={this.state.is_invite_their_plan}
                onValueChange={(is_invite_their_plan) => this.setState({ is_invite_their_plan })} />
              }
              
            </View>

          </Content>
          <TouchableOpacity style={{ width: '100%' }} onPress={this.saveData} >
            <ImageBackground source={this.state.btnDoneImage} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Save
                                </Text>
            </ImageBackground>
          </TouchableOpacity>

          <SafeAreaView />
        </BackgroundImage>
      </Container>
    );
  }
}

export default Profile;
