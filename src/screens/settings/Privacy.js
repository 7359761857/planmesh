import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, Picker, ImageBackground, TouchableOpacity, StatusBar, Text, Platform, TextInput, Switch } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Button, Header, Left, Right, Content } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import Entypo from "react-native-vector-icons/Entypo";
import AntDesign from "react-native-vector-icons/AntDesign";
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import Modal from "react-native-modal";
const SelectRadio = require("../../../assets/images/selected_radio1.png");
import { store } from "../../redux/store";
import { API_ROOT } from "../../config/constant";
import RNProgressHUB from 'react-native-progresshub';
import MixpanelManager from '../../../Analytics'
const backWhite = require("../../../assets/images/backWhite.png");
const btnBgGrey = require("../../../assets/images/btnBg.png");
class report extends ValidationComponent {

    constructor(props) {
        super(props);
        this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
        this.state = {
            cityModal: false,
            birthdayModal: false,
            interestModal: false,
            placeModal: false,
            suggestionModal: false,
            token: store.getState().auth.user.token,
            login_id: store.getState().auth.user.login_id,
            btnDoneImage: btnBgGrey,
            valueCity: 'friend',
            valueBirthday: 'friend',
            valueInterest: 'friend',
            valuePlace: 'friend',
            valueSuggetion: 'friend',
            mycity:'',
            mybirthdate:'',

            CityList: [{
                name: 'Friends',
                key: 'friend',
                id: 2,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },
            {
                name: 'Friends of friends',
                key: 'fof',
                id: 3,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },
            {
                name: 'Only me',
                key: 'me',
                id: 1,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },

            ],
            BirthdayList: [{
                name: 'Friends',
                key: 'friend',
                id: 2,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },
            {
                name: 'Friends of friends',
                key: 'fof',
                id: 3,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },
            {
                name: 'Only me',
                key: 'me',
                id: 1,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },

            ],
            PlaceList: [{
                name: 'Friends',
                key: 'friend',
                id: 2,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },
            {
                name: 'Friends of friends',
                key: 'fof',
                id: 3,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },
            {
                name: 'Only me',
                key: 'me',
                id: 1,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },

            ],
            InterestList: [{
                name: 'Friends',
                key: 'friend',
                id: 2,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },
            {
                name: 'Friends of friends',
                key: 'fof',
                id: 3,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },
            {
                name: 'Only me',
                key: 'me',
                id: 1,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },

            ],
            suggestionList: [{
                name: 'Friends',
                key: 'friend',
                id: 2,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },
            {
                name: 'Friends of friends',
                key: 'fof',
                id: 3,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },

            ],
            cityName: '2',
            birthdayName: '2',
            interestName: '2',
            placeName: '2',
            suggestionName: '2',
            sharecityName: 'Friends',
            sharebirthdayName: 'Friends',
            shareinterestName: 'Friends',
            shareplaceName: 'Friends',
            sharesuggestionName: 'Friends',
        };
    }

    setCityModalVisible(visible) {
        this.setState({ cityModal: visible });
    }
    setBirthdayModalVisible(visible) {
        this.setState({ birthdayModal: visible });
    }
    setInterestModalVisible(visible) {
        this.setState({ interestModal: visible });
    }
    setPlaceModalVisible(visible) {
        this.setState({ placeModal: visible });
    }
    setSuggestionModalVisible(visible) {
        this.setState({ suggestionModal: visible });
    }

    async componentDidMount() {

        await AsyncStorage.getItem('birthdate').then((value) => {
            this.setState({ mybirthdate: value })
        });

        await AsyncStorage.getItem('mycity').then((value) => {
            this.setState({ mycity: value })
        });

        
    
        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('type', 1)
        RNProgressHUB.showSpinIndeterminate();     

        fetch(API_ROOT + 'get/setting/detail', {
          method: 'post', body: data
        })
          .then((response) => response.json())
          .then((responseData) => {
            if (responseData.success) {

              let data = responseData.data

              if(data.city == '3')
              {
                  this.setState({valueCity:'fof',sharecityName:'Friends of friends', cityName:'3'})
              }else if(data.city == '2')
              {
                this.setState({valueCity:'friend',sharecityName:'Friends', cityName:'2'})

              }else if(data.city == '1')
              {
                this.setState({valueCity:'me',sharecityName:'Only me', cityName:'1'})

              }
               if(data.birthday == '3')
              {
                this.setState({valueBirthday:'fof',sharebirthdayName:'Friends of friends', birthdayName:'3'})

              }else if(data.birthday == '2')
              {
                this.setState({valueBirthday:'friend',sharebirthdayName:'Friends', birthdayName:'2'})

              }else if(data.birthday == '1')
              {
                this.setState({valueBirthday:'me',sharebirthdayName:'Only me', birthdayName:'1'})
              }
               if(data.interest == '3')
              {
                this.setState({valueInterest:'fof',shareinterestName:'Friends of friends', interestName:'3'})

              }else if(data.interest == '2')
              {
                this.setState({valueInterest:'friend',shareinterestName:'Friends', interestName:'2'})

              }else if(data.interest == '1')
              {
                this.setState({valueInterest:'me',shareinterestName:'Only me', interestName:'1'})

              }
               if(data.place == '3')
              {
                this.setState({valuePlace:'fof',shareplaceName:'Friends of friends', placeName:'3'})

              }else if(data.place == '2')
              {
                this.setState({valuePlace:'friend',shareplaceName:'Friends', placeName:'2'})

              }else if(data.place == '1')
              {
                this.setState({valuePlace:'me',shareplaceName:'Only me', placeName:'1'})

              }
               if(data.suggestion == '3')
              {
                this.setState({valueSuggetion:'fof',sharesuggestionName:'Friends of friends', suggestionName:'3'})

              }else if(data.suggestion == '2')
              {
                this.setState({valueSuggetion:'friend',sharesuggestionName:'Friends', suggestionName:'2'})

              }
              
    
            } else {
              
    
            }
            RNProgressHUB.dismiss()
          })
          .catch((error) => { 
              //alert(error)
            RNProgressHUB.dismiss()
          })
      }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={[styles.headerAndroidnav,{height:44}]}>
                    <StatusBar barStyle="light-content" backgroundColor="#41199B" />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
                        <TouchableOpacity onPress={() => { navigation.goBack() }}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity></View>
                    <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                        <Text style={styles.headerTitle} >Privacy</Text></View>

                </Header>
            )
        }
    }

    renderCityModal() {
        const { valueCity } = this.state;
        return (
            <View>
                <Modal
                    isVisible={this.state.cityModal} style={{ margin: 0 }} onBackdropPress={() => this.setState({ cityModal: false })} onBackButtonPress={() => this.setState({ cityModal: false })}>
                    <View style={{ height: Height(25), borderTopLeftRadius: 10, borderTopRightRadius: 10, width: '100%', position: 'absolute', bottom: 0, backgroundColor: '#FFF' }}>
                        <View style={{ flexDirection: 'row', height: Height(7), justifyContent: 'center' }}>
                            <AntDesign
                                onPress={() => this.setState({ cityModal: false })}
                                color={colors.black}
                                name="close" size={25} style={{ left: Width(3), position: 'absolute', top: Height(2) }} />
                            <Text style={{ alignSelf: 'center', fontFamily: fonts.Raleway_Bold, fontSize: FontSize(18), color: colors.dargrey }}> Share Current City With</Text>
                        </View>
                        <View style={{ borderBottomColor: '#EEEEEE', borderBottomWidth: 1 }} />
                        <View style={{ height: Height(1) }}></View>
                        {this.state.CityList.map(item => {
                            return (
                                <View >
                                    <TouchableOpacity onPress={() => {
                                        this.setState({
                                            sharecityName: item.name,
                                            valueCity: item.key,
                                            cityName: item.id
                                        });
                                        this.setState({ cityModal: false })
                                    }} style={{ margin: '1%', marginVertical: '1.5%', flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white }}>
                                        <View style={{ marginLeft: Width(3), width: Width(20), justifyContent: 'center', flex: 4.5 }}>
                                            <Text style={{ fontSize: FontSize(16), color: colors.dargrey, fontFamily: fonts.Roboto_Regular }}>{item.name} {item.lname}</Text>
                                        </View>
                                        <View style={{ alignItems: 'flex-end', right: Width(3), flex: 2 }}>
                                            <View style={{
                                                height: 20,
                                                width: 20,
                                                borderRadius: 100,
                                                borderWidth: 1,
                                                borderColor: colors.dargrey,
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                            }} >
                                                {valueCity === item.key && <View>
                                                    <Image style={{ height: 18, width: 18 }} source={SelectRadio} style={{}} />
                                                </View>}
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                    <View style={{ height: Height(0.5) }} />

                                </View>
                            );
                        })}



                    </View>

                </Modal>
            </View>
        )
    }

    renderBirthdayModal() {
        const { valueBirthday } = this.state;
        return (
            <View>
                <Modal
                    isVisible={this.state.birthdayModal} style={{ margin: 0 }} onBackdropPress={() => this.setState({ birthdayModal: false })} onBackButtonPress={() => this.setState({ birthdayModal: false })}>
                    <View style={{ height: Height(25), borderTopLeftRadius: 10, borderTopRightRadius: 10, width: '100%', position: 'absolute', bottom: 0, backgroundColor: '#FFF' }}>
                        <View style={{ flexDirection: 'row', height: Height(7), justifyContent: 'center' }}>
                            <AntDesign
                                onPress={() => this.setState({ birthdayModal: false })}
                                color={colors.black}
                                name="close" size={25} style={{ left: Width(3), position: 'absolute', top: Height(2) }} />
                            <Text style={{ alignSelf: 'center', fontFamily: fonts.Raleway_Bold, fontSize: FontSize(18), color: colors.dargrey }}>Share Birthday With</Text>
                        </View>
                        <View style={{ borderBottomColor: '#EEEEEE', borderBottomWidth: 1 }} />
                        <View style={{ height: Height(1) }}></View>
                        {this.state.BirthdayList.map(item => {
                            return (
                                <View >
                                    <TouchableOpacity onPress={() => {
                                        this.setState({
                                            sharebirthdayName: item.name,
                                            valueBirthday: item.key,
                                            birthdayName: item.id
                                        });
                                        this.setState({ birthdayModal: false })
                                    }} style={{ margin: '1%', marginVertical: '1.5%', flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white }}>
                                        <View style={{ marginLeft: Width(3), width: Width(20), justifyContent: 'center', flex: 4.5 }}>
                                            <Text style={{ fontSize: FontSize(16), color: colors.dargrey, fontFamily: fonts.Roboto_Regular }}>{item.name} {item.lname}</Text>
                                        </View>
                                        <View style={{ alignItems: 'flex-end', right: Width(3), flex: 2 }}>
                                            <View style={{
                                                height: 20,
                                                width: 20,
                                                borderRadius: 100,
                                                borderWidth: 1,
                                                borderColor: colors.dargrey,
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                            }} >
                                                {valueBirthday === item.key && <View>
                                                    <Image source={SelectRadio} style={{}} />
                                                </View>}
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                    <View style={{ height: Height(0.5) }} />

                                </View>
                            );
                        })}



                    </View>

                </Modal>
            </View>
        )
    }
    renderInterestModal() {
        const { valueInterest } = this.state;
        return (
            <View>
                <Modal
                    isVisible={this.state.interestModal} style={{ margin: 0 }} onBackdropPress={() => this.setState({ interestModal: false })} onBackButtonPress={() => this.setState({ interestModal: false })}>
                    <View style={{ height: Height(25), borderTopLeftRadius: 10, borderTopRightRadius: 10, width: '100%', position: 'absolute', bottom: 0, backgroundColor: '#FFF' }}>
                        <View style={{ flexDirection: 'row', height: Height(7), justifyContent: 'center' }}>
                            <AntDesign
                                onPress={() => this.setState({ interestModal: false })}
                                color={colors.black}
                                name="close" size={25} style={{ left: Width(3), position: 'absolute', top: Height(2) }} />
                            <Text style={{ alignSelf: 'center', fontFamily: fonts.Raleway_Bold, fontSize: FontSize(18), color: colors.dargrey }}>Share Interests With</Text>
                        </View>
                        <View style={{ borderBottomColor: '#EEEEEE', borderBottomWidth: 1 }} />
                        <View style={{ height: Height(1) }}></View>
                        {this.state.InterestList.map(item => {
                            return (
                                <View >
                                    <TouchableOpacity onPress={() => {
                                        this.setState({
                                            shareinterestName: item.name,
                                            valueInterest: item.key,
                                            interestName: item.id
                                        });
                                        this.setState({ interestModal: false })
                                    }} style={{ margin: '1%', marginVertical: '1.5%', flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white }}>
                                        <View style={{ marginLeft: Width(3), width: Width(20), justifyContent: 'center', flex: 4.5 }}>
                                            <Text style={{ fontSize: FontSize(16), color: colors.dargrey, fontFamily: fonts.Roboto_Regular }}>{item.name} {item.lname}</Text>
                                        </View>
                                        <View style={{ alignItems: 'flex-end', right: Width(3), flex: 2 }}>
                                            <View style={{
                                                height: 20,
                                                width: 20,
                                                borderRadius: 100,
                                                borderWidth: 1,
                                                borderColor: colors.dargrey,
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                            }} >
                                                {valueInterest === item.key && <View>
                                                    <Image source={SelectRadio} style={{}} />
                                                </View>}
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                    <View style={{ height: Height(0.5) }} />

                                </View>
                            );
                        })}



                    </View>

                </Modal>
            </View>
        )
    }
    renderPlaceModal() {
        const { valuePlace } = this.state;
        return (
            <View>
                <Modal
                    isVisible={this.state.placeModal} style={{ margin: 0 }} onBackdropPress={() => this.setState({ placeModal: false })} onBackButtonPress={() => this.setState({ placeModal: false })}>
                    <View style={{ height: Height(25), borderTopLeftRadius: 10, borderTopRightRadius: 10, width: '100%', position: 'absolute', bottom: 0, backgroundColor: '#FFF' }}>
                        <View style={{ flexDirection: 'row', height: Height(7), justifyContent: 'center' }}>
                            <AntDesign
                                onPress={() => this.setState({ placeModal: false })}
                                color={colors.black}
                                name="close" size={25} style={{ left: Width(3), position: 'absolute', top: Height(2) }} />
                            <Text style={{ alignSelf: 'center', fontFamily: fonts.Raleway_Bold, fontSize: FontSize(18), color: colors.dargrey }}>Share Places You Like With </Text>
                        </View>
                        <View style={{ borderBottomColor: '#EEEEEE', borderBottomWidth: 1 }} />
                        <View style={{ height: Height(1) }}></View>
                        {this.state.PlaceList.map(item => {
                            return (
                                <View >
                                    <TouchableOpacity onPress={() => {
                                        this.setState({
                                            shareplaceName: item.name,
                                            valuePlace: item.key,
                                            placeName: item.id
                                        });
                                        this.setState({ placeModal: false })
                                    }} style={{ margin: '1%', marginVertical: '1.5%', flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white }}>
                                        <View style={{ marginLeft: Width(3), width: Width(20), justifyContent: 'center', flex: 4.5 }}>
                                            <Text style={{ fontSize: FontSize(16), color: colors.dargrey, fontFamily: fonts.Roboto_Regular }}>{item.name} {item.lname}</Text>
                                        </View>
                                        <View style={{ alignItems: 'flex-end', right: Width(3), flex: 2 }}>
                                            <View style={{
                                                height: 20,
                                                width: 20,
                                                borderRadius: 100,
                                                borderWidth: 1,
                                                borderColor: colors.dargrey,
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                            }} >
                                                {valuePlace === item.key && <View>
                                                    <Image source={SelectRadio} style={{}} />
                                                </View>}
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                    <View style={{ height: Height(0.5) }} />

                                </View>
                            );
                        })}



                    </View>

                </Modal>
            </View>
        )
    }
    renderSuggestionModal() {
        const { valueSuggetion } = this.state;
        return (
            <View>
                <Modal
                    isVisible={this.state.suggestionModal} style={{ margin: 0 }} onBackdropPress={() => this.setState({ suggestionModal: false })} onBackButtonPress={() => this.setState({ suggestionModal: false })}>
                    <View style={{ height: Height(21), borderTopLeftRadius: 10, borderTopRightRadius: 10, width: '100%', position: 'absolute', bottom: 0, backgroundColor: '#FFF' }}>
                        <View style={{ flexDirection: 'row', height: Height(7), justifyContent: 'center' }}>
                            <AntDesign
                                onPress={() => this.setState({ suggestionModal: false })}
                                color={colors.black}
                                name="close" size={25} style={{ left: Width(3), position: 'absolute', top: Height(2) }} />
                            <Text style={{ alignSelf: 'center', fontFamily: fonts.Raleway_Bold, fontSize: FontSize(18), color: colors.dargrey }}>Who can send you suggestions?</Text>
                        </View>
                        <View style={{ borderBottomColor: '#EEEEEE', borderBottomWidth: 1 }} />
                        <View style={{ height: Height(1) }}></View>
                        {this.state.suggestionList.map(item => {
                            return (
                                <View >
                                    <TouchableOpacity onPress={() => {
                                        this.setState({
                                            sharesuggestionName: item.name,
                                            valueSuggetion: item.key,
                                            suggestionName: item.id
                                        });
                                        this.setState({ suggestionModal: false })
                                    }} style={{ margin: '1%', marginVertical: '1.5%', flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white }}>
                                        <View style={{ marginLeft: Width(3), width: Width(20), justifyContent: 'center', flex: 4.5 }}>
                                            <Text style={{ fontSize: FontSize(16), color: colors.dargrey, fontFamily: fonts.Roboto_Regular }}>{item.name} {item.lname}</Text>
                                        </View>
                                        <View style={{ alignItems: 'flex-end', right: Width(3), flex: 2 }}>
                                            <View style={{
                                                height: 20,
                                                width: 20,
                                                borderRadius: 100,
                                                borderWidth: 1,
                                                borderColor: colors.dargrey,
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                            }} >
                                                {valueSuggetion === item.key && <View>
                                                    <Image source={SelectRadio} style={{}} />
                                                </View>}
                                            </View>
                                        </View>
                                    </TouchableOpacity>

                                </View>
                            );
                        })}



                    </View>

                </Modal>
            </View>
        )
    }
    save = () => {

        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('city', this.state.cityName)
        data.append('birthday', this.state.birthdayName)
        data.append('interest', this.state.interestName)
        data.append('place', this.state.placeName)
        data.append('suggestion', this.state.suggestionName)

        //alert(JSON.stringify(data))
        
        fetch(API_ROOT + 'privacy', {
            method: 'post',
            body: data
        }).then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {
                    const MixpanelData = {
                        '$email' : store.getState().auth.user.email_address
                      }
                    this.mixpanel.identify(store.getState().auth.user.email_address);
                    this.mixpanel.track('Profile Settings - Privacy', MixpanelData);
                    this.props.navigation.goBack()
                
                } else {

                }
            })
            .catch((error) => {

                //alert(error)
            })
    }
    render() {

        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#fff' }} >
                <Toastt ref="toast"></Toastt>
                <BackgroundImage >
                    <SafeAreaView />

                    <Content>

                        <View style={{ marginTop: Height(3), marginHorizontal: Width(4) }}>
                            <Text style={styles.top_text}>
                                Decide who you share you profile information with.
                       </Text>

                        </View>

                        <Text style={{ marginTop: Height(4.5), marginHorizontal: Width(4), color: '#3A4759', fontFamily: fonts.Raleway_Semibold, fontSize: FontSize(16) }}>
                            Current City
                    </Text>

                        <View style={styles.singleLine} />

                        <View style={styles.pickertext_view} >
                            <Text style={styles.picker_text} >
                                Currently in {this.state.mycity}
                        </Text>
                            <TouchableOpacity onPress={() => {
                                this.setCityModalVisible(!this.state.cityModal);
                            }} style={styles.bordrview}>
                                <Text style={styles.pickerText}>{this.state.sharecityName}</Text>
                                <Entypo name='chevron-small-down' size={25} color='#3A4759' />
                            </TouchableOpacity>
                        </View>

                        <Text style={{ marginTop: Height(1), marginHorizontal: Width(4), color: '#3A4759', fontFamily: fonts.Raleway_Semibold, fontSize: FontSize(16) }}>
                            Birthday
                    </Text>

                        <View style={styles.singleLine} />
                        <View style={styles.pickertext_view} >
                            <Text style={styles.picker_text} >
                                {this.state.mybirthdate}
                           </Text>
                            <TouchableOpacity onPress={() => {
                                this.setBirthdayModalVisible(!this.state.birthdayModal);
                            }} style={styles.bordrview}>
                                <Text style={styles.pickerText}>{this.state.sharebirthdayName}</Text>
                                <Entypo name='chevron-small-down' size={25} color='#3A4759' />

                            </TouchableOpacity>
                        </View>


                        <View style={styles.singleLine2} />

                        <View style={styles.pickertext_view} >
                            {/* <Text style={{ flex: 1, marginTop: Height(1), color: '#3A4759', fontFamily: fonts.Raleway_Semibold, fontSize: FontSize(16) }}> */}
                            <Text style={{ flex: 1, marginTop: Height(0), color: '#3A4759', fontFamily: fonts.Raleway_Semibold, fontSize: FontSize(16) }}>
                                Interests
                           </Text>
                            <TouchableOpacity onPress={() => {
                                this.setInterestModalVisible(!this.state.interestModal);
                            }} style={styles.bordrview}>
                                <Text style={styles.pickerText}>{this.state.shareinterestName}</Text>
                                <Entypo name='chevron-small-down' size={25} color='#3A4759' />

                            </TouchableOpacity>
                        </View>


                        {/* <View style={styles.pickertext_view} >
                            <Text style={{ flex: 1, color: '#3A4759', fontFamily: fonts.Raleway_Semibold, fontSize: FontSize(16) }}>
                                Places You Like
                           </Text>
                            <TouchableOpacity onPress={() => {
                                this.setPlaceModalVisible(!this.state.placeModal);
                            }} style={styles.bordrview}>
                                <Text style={styles.pickerText}>{this.state.shareplaceName}</Text>
                                <Entypo name='chevron-small-down' size={25} color='#3A4759' />

                            </TouchableOpacity>
                        </View> */}
                        <View style={styles.singleLine3} />

                        <View style={styles.pickertext_view}>
                            <Text style={{ flex: 1, color: '#3A4759', fontFamily: fonts.Raleway_Semibold, fontSize: FontSize(16) }}>
                                Who can send you Suggestions?</Text>
                            <TouchableOpacity onPress={() => {
                                this.setSuggestionModalVisible(!this.state.suggestionModal);
                            }} style={styles.bordrview}>
                                <Text style={styles.pickerText}>{this.state.sharesuggestionName}</Text>
                                <Entypo name='chevron-small-down' size={25} color='#3A4759' />

                            </TouchableOpacity>

                        </View>

                        {this.renderCityModal()}
                        {this.renderBirthdayModal()}
                        {this.renderInterestModal()}
                        {this.renderPlaceModal()}
                        {this.renderSuggestionModal()}


                    </Content>
                    <TouchableOpacity style={{ width: '100%' }} onPress={this.save} >
                        <ImageBackground source={this.state.btnDoneImage} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Save
                                </Text>
                        </ImageBackground>
                    </TouchableOpacity>

                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default report;
