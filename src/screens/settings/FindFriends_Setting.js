import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Alert, ImageBackground, TouchableOpacity, StatusBar, FlatList, Platform, PermissionsAndroid, Text } from "react-native";
import { Container, View, Button, Header, Left, Right, Content, Item, Input, Icon, Switch, List, ListItem, Body, } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import Contacts from 'react-native-contacts';
import Toast from 'react-native-simple-toast';


let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height
import { Height, FontSize, Width, colors } from "../../config/dimensions";
import { getFriends, snedFriendRequest } from "../../redux/actions/auth";
import { API_ROOT } from "../../config/constant";
import SplashScreen from "react-native-splash-screen";
import NavigationService from "../../services/NavigationService";
import { store } from "../../redux/store";
import { TextInput } from "react-native-gesture-handler";
import RNProgressHUB from 'react-native-progresshub';
import MixpanelManager from "../../../Analytics";


const backWhite = require("../../../assets/images/backWhite.png");
const btnBg = require("../../../assets/images/btnBg.png");
const right_arrow = require("../../../assets/images/right_arrow.png");
const unSelectRadio = require("../../../assets/images/unSelectRadio.png");
const selectRadio = require("../../../assets/images/selectRadio.png");
const closeicon = require("../../../assets/images/Dashboard/close.png");


var subThis;
class FindFriend extends ValidationComponent {

    constructor(props) {
        super(props);
        this.search = null,
            this.matchedcontacts = [];
        this.matchedcontactscopy = [];
        this.state = {
            Data: [],
            selectAll: false,
            originalData: [],
            inviteList: [],
            token: store.getState().auth.user.token,
            login_id: store.getState().auth.user.login_id,
            is_close_visible: false,
        };
        this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
        subThis = this
    }

    componentWillMount() {

    }
    componentDidMount() {

        //SplashScreen.hide()

        //this.search.focus()

        setTimeout(async function () {
            if (Platform.OS === 'android') {
                PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                    {
                        title: 'Planmesh',
                        message: 'Planmesh syncs only phone numbers from your address book to Planmesh servers to help you connect with other Planmesh users.',
                        buttonPositive: "OK"
                    }
                ).then((results) => {

                    if (results === PermissionsAndroid.RESULTS.GRANTED) {
                        subThis.loadContacts();
                    }
                })

                // const andoidContactPermission = await PermissionsAndroid.request(
                //     PermissionsAndroid.PERMISSIONS.READ_CONTACTS
                //   );
                //   if (andoidContactPermission === PermissionsAndroid.RESULTS.GRANTED) {
                //     console.log("Contacts Permission granted");
                //     subThis.loadContacts();
                //   } else {
                //     console.log("Contacts permission denied");
                //   }
            } else {
                subThis.loadContacts();
            }
        }, 200);
    }

    loadContacts() {

        Contacts.getAll().then(contacts => {
            //alert('Hiii')
            RNProgressHUB.showSpinIndeterminate()
            let newData = []

            let form = new FormData()
            let str = "";
            contacts.map((contactData) => {

                if (contactData.givenName != null) {
                    const words = contactData.givenName.split(' ')
                    var initial = ""
                    var seactionStr = ""
                    if (words.length >= 1) {
                        initial = words[0].charAt(0)
                        seactionStr = words[0].charAt(0)
                    }
                    if (words.length >= 2) {
                        initial = initial + words[1].charAt(0)
                    }
                    let dict = { key: contactData.rawContactId, select: false, title: ((contactData.givenName) + ' ' + (contactData.familyName != null ? contactData.familyName : '')), initial: initial.toUpperCase(), sectionStr: seactionStr.toUpperCase(), phoneNumbers: contactData.phoneNumbers, is_request_send: 0 }
                    contactData.phoneNumbers ? contactData.phoneNumbers.map((mob1) => {
                        str = str + mob1.number + ',';
                        return mob1.number
                    }) : ''
                    newData.push(dict)
                    //console.log('Call Log',str)

                }
            });
            form.append('login_id', this.state.login_id)
            form.append('mobile_number', str)

            //console.log('GET FRIENDS REQUEST: '+JSON.stringify(form))

            fetch(API_ROOT + 'get_friends',
                {
                    body: form,
                    method: "post"
                }).then(res => res.json()).then(resJson => {
                    //console.log('Contact Friends: ',JSON.stringify(resJson))
                    //alert(JSON.stringify(resJson))
                    RNProgressHUB.dismiss()
                    if (resJson.success) {
                        let invi = resJson.invite_friend_list;

                        let send = resJson.friend_request_list;
                        let invi_list = this.invi_friends(newData, invi)
                        //console.log("inv",invi_list)
                        let send_list = this.send_friends(newData, send)
                        //alert(JSON.stringify(send_list))
                        this.setState({ Data: send_list, originalData: send_list, inviteList: invi_list });

                        RNProgressHUB.dismiss();
                        if (send_list.length == 0) {
                            this.inviteFriends()
                        }

                    }
                    else {
                        RNProgressHUB.dismiss();

                    }
                }).catch(err => {
                    RNProgressHUB.dismiss()
                });

        })


        /* Contacts.getAll((err, contacts) => {
             console.log(contacts)
             console.log(err)
             if (err === 'denied') {
                 console.warn('Permission to access contacts was denied');
                 
             } else {
                 let newData = []
                 
                 let form = new FormData()
                 let str= "";
                 contacts.map((contactData) => {
                   
                     if(contactData.givenName!=null){
                         const words = contactData.givenName.split(' ')
                         var initial = ""
                         var seactionStr = ""
                         if (words.length >= 1) {
                             initial = words[0].charAt(0)
                             seactionStr = words[0].charAt(0)
                         }
                         if (words.length >= 2) {
                             initial = initial + words[1].charAt(0)
                         }
                         let dict = { key: contactData.rawContactId, select: false, title: ((contactData.givenName) +' '+ (contactData.familyName!=null?contactData.familyName:'')), initial: initial.toUpperCase(), sectionStr: seactionStr.toUpperCase(),phoneNumbers:contactData.phoneNumbers, is_request_send:0}
                         contactData.phoneNumbers?contactData.phoneNumbers.map((mob1)=>{
                             str = str + mob1.number + ',';
                             return mob1.number
                         }):''
                         newData.push(dict)
                         //console.log('Call Log',str)
 
                     }
                 });
                 form.append('login_id',this.state.login_id)
                 form.append('mobile_number',str)
 
                 //console.log('GET FRIENDS REQUEST: '+JSON.stringify(form))
 
                 fetch(API_ROOT+'get_friends',
                     {
                         body: form,
                         method: "post"
                     }).then(res=>res.json()).then(resJson=>{
                             //console.log('Contact Friends: ',JSON.stringify(resJson))
                             //alert(JSON.stringify(resJson))
 
                             if(resJson.success){
                                 let invi = resJson.invite_friend_list;
                                
                                 let send = resJson.friend_request_list;
                                 let invi_list = this.invi_friends(newData,invi)
                                 //console.log("inv",invi_list)
                                 let send_list = this.send_friends(newData,send)
 
                                 this.setState({ Data: send_list, originalData: send_list,inviteList:invi_list });
 
                                 //RNProgressHUB.dismiss();
                                 if(send_list.length==0){
                                     this.inviteFriends()
                                 }
                                   
                             }
                             else{
                                 
                                 
                             }
                     }).catch(err=>{
                         
                     });
                 
                 
             }
         })*/
    }
    invi_friends = (newData, invi) => {

        //console.log('Friends Invite: '+invi)

        // let data =  newData.filter(fruit => fruit.phoneNumbers.some(m=>invi.includes(m.number)))
        // return data

        var data = []
        for (item of newData) {
            var number = ''
            if (item.phoneNumbers != undefined && item.phoneNumbers != null && item.phoneNumbers.length > 0 && item.phoneNumbers && item.phoneNumbers[0].number) {
                number = item.phoneNumbers[0].number
            }

            for (invtcnct of invi) {
                if (invtcnct.number == number) {
                    item.is_invited = invtcnct.is_invited
                    data.push(item)
                }
            }
        }

        return data
    }

    send_friends = (newData, send) => {
        //console.log('Friends Requests List: '+JSON.stringify(send))
        var match = []
        newData.map((contact) => {
            //console.log('CONTACT FRIENDS :'+contact.phoneNumbers[0].number)
            send.map((friend) => {

                if (contact.phoneNumbers != null && contact.phoneNumbers.length > 0) {
                    if (contact.phoneNumbers[0].number == friend.mobile_number) {
                        this.matchedcontacts.push(friend)
                        this.matchedcontactscopy.push(friend)

                        if (friend.is_request_send == 1) {
                            contact.is_request_send = 1
                        }
                        contact.login_id = friend.login_id
                        match.push(contact)
                    }
                }

            })
        })

        return match

        //return newData.filter(fruit => fruit.phoneNumbers.some(m=>send.includes(m.number)))

    }
    FlatListItemSeparator = () => <View style={styles.line} />;


    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={[styles.headerAndroidnav, { height: 44 }]}>
                    <StatusBar barStyle="light-content" backgroundColor="#41199B" />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
                        <TouchableOpacity onPress={() => {
                            navigation.goBack()
                        }}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity></View>
                    <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                        <Text style={styles.headerTitle} >Find Friends</Text>
                    </View>
                    {/* <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', right: Width(4) }}>
              <TouchableOpacity onPress={() => NavigationService.reset('dashboard')} >
              <Text style={{ color: 'white', opacity: 0.5 }}>Skip</Text>
                </TouchableOpacity>
              </View> */}

                </Header>

            )
        }
    }

    inviteFriends = () => {
        let selectedArray = this.state.inviteList
        this.props.navigation.push('sendFriendRequest', {
            is_skip_visible: false,
            selectedArray: selectedArray.sort((a, b) => a.sectionStr > b.sectionStr)

        })
    }


    updateFriendsList(invited) {
        var temparr = []
        this.state.Data.map((facet) => {
            if (invited.includes(facet)) {
                var newfacet = facet
                newfacet.is_request_send = 1
                temparr.push(newfacet)
            }
            else {
                temparr.push(facet)
            }
        })
        this.setState({ Data: temparr })
    }

    onRequestSend = () => {
        let mbl = ''
        let otherID = ''
        let selectedArray = []
        this.state.originalData.map((facet) => {
            if (facet.select == true) {
                selectedArray.push(facet)
                otherID = otherID + facet.login_id + ',';
                // facet.phoneNumbers?facet.phoneNumbers.map((mob1)=>{
                //     mbl = mbl + mob1.number + ',';
                //     return mob1.number
                // }):''

            }
        });

        //let data = {token:this.state.token,login_id:this.state.login_id,mobile_number:mbl} 
        let data = { token: this.state.token, login_id: this.state.login_id, other_id: otherID }
        //alert(JSON.stringify(data))
        //console.log(store.getState().auth.user.token)
        //console.log(store.getState().auth.user)
        console.log('Selecred Array0', selectedArray)
        let array = selectedArray.map(item => item.title)
        const MixpanelData = {
            '$email': store.getState().auth.user.email_address,
            'Friends Selected': JSON.stringify(array),
            'Friend Requests Sent': array.length,
            'During Sign Up': 'False'
        }
        console.log('Mixpanel Data..', MixpanelData)
        snedFriendRequest(data).then(res=>{
            if(res.success){
                //Toast.show(res.text)
                //this.loadContacts()
                this.mixpanel.identify(store.getState().auth.user.email_address)
                this.mixpanel.track('Send Friend Requests', MixpanelData);
                this.updateFriendsList(selectedArray)
                //NavigationService.reset('dashboard')
            }
            else{
                Toast.show(res.text)
            }
        }).catch(err=>{
            // alert(JSON.stringify(err))
        })
        // var selectedArray = []
        // this.state.originalData.map((facet) => {
        //     if (facet.select == true) {
        //         selectedArray.push(facet)
        //     }
        // });
        // console.log(selectedArray)
        // this.props.navigation.push('sendFriendRequest', {
        //     selectedArray: selectedArray.sort((a, b) => a.sectionStr > b.sectionStr)
        // })
    }

    loadTableCells(item, index) {
        return (this.cellVideoList(item, index));
    }
    onSelectList(facets, index) {
        let dataArray = this.state.Data;
        var facet = dataArray[index]
        //console.log("old facte", facet)

        if (facet.is_request_send == 0) {
            if (facet.select == true) {
                facet.select = false
            } else {
                facet.select = true
            }
            console.log("New facte", facet)
            this.forceUpdate()
            this.checkIfAllSelected()
        }
        //this.setState({data: facet})
    }
    onSelectAll(value) {
        this.state.Data.map((facet) => {
            facet.select = value.value
            this.setState({ data: facet })
        });
        if (value.value == true) {
            this.setState({ selectAll: true })
        } else {
            this.setState({ selectAll: false })
        }
    }

    checkIfAllSelected() {
        var allselected = true
        this.state.Data.map((facet) => {
            if (facet.select == false || facet.select == null) {
                allselected = false
            }
        });

        if (allselected == true) {
            this.setState({ selectAll: true })
        }
        else {
            this.setState({ selectAll: false })
        }

    }

    cellVideoList = (facet, rowID) => {
        //sectionID = 0;
        console.log('Current Facet: ' + JSON.stringify(facet))
        return (
            <TouchableOpacity key={rowID} onPress={() => { this.onSelectList(facet, rowID) }}>
                <View style={{ flexDirection: 'row', height: 55, flex: 1, borderBottomColor: "#3A4759" }}>
                    <View style={{ paddingLeft: 15, alignItems: 'flex-start', justifyContent: 'center' }}>
                        <View style={{ width: 38, height: 38, borderRadius: 19, backgroundColor: "#D6D6D6", justifyContent: 'center' }}>
                            <Text style={[{ textAlign: 'center', fontFamily: 15, fontFamily: "Raleway-ExtraBold", letterSpacing: 0.02 }, checkFontWeight("800")]}>{facet.initial}</Text>
                        </View>
                    </View>
                    <View style={{ paddingLeft: 15, alignItems: 'flex-start', justifyContent: 'center', flex: 1 }}>
                        <Text style={[{ color: "#363169", fontSize: 16, fontFamily: "Roboto-Medium", letterSpacing: 0.02 }, checkFontWeight("500")]}>{this.matchedcontactscopy[rowID].name}</Text>
                        {/* <Text style={[{ color: "#363169", fontFamily: 16, fontFamily: "Roboto-Medium", letterSpacing: 0.02 }, checkFontWeight("500")]}>{facet.phoneNumbers[0].number}</Text> */}

                    </View>
                    <View style={{ alignItems: 'flex-end', justifyContent: 'center', marginRight: 17 }}>

                        {
                            facet.is_request_send == 1 ?
                                <Text style={{ color: colors.dargrey, fontSize: FontSize(13), fontWeight: '400', fontFamily: "Roboto-Regular" }}>Request sent</Text>
                                :
                                <Image source={facet.select == false ? unSelectRadio : selectRadio} style={{ width: 25, height: 25 }}></Image>
                        }

                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    handleChange = (searchKey) => {
        //console.log(searchKey.searchKey)

        this.setState({ is_close_visible: true })
        var tempData = []
        this.matchedcontactscopy = []
        this.state.originalData.map((contactData) => {
            if (contactData.title.includes(searchKey.searchKey)) {
                tempData.push(contactData)
                //this.matchedcontactscopy.push(contactData)
                this.matchedcontacts.map((frnd) => {
                    if (contactData.phoneNumbers[0].number == frnd.mobile_number) {
                        this.matchedcontactscopy.push(frnd)
                    }
                })
            }
        });
        this.setState({ Data: tempData })
        console.log('Searched contacts: ' + JSON.stringify(tempData))
    }

    onClearTextPressed() {
        this.search.clear()
        this.setState({ is_close_visible: false, Data: this.state.originalData })
        setTimeout(() => {
            this.search.blur()

        }, 100)
    }

    render() {
        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: "#F2F4F7" }} >
                <SafeAreaView />
                <TouchableOpacity onPress={() => this.inviteFriends()} style={{ backgroundColor: "#FF00BA", height: 50, paddingLeft: 20, flexDirection: 'row' }}>
                    <Text style={{ color: "#FFF", fontFamily: "Roboto-Regular", fontSize: 17, letterSpacing: 0.005, alignItems: 'flex-start', alignSelf: 'center', flex: 1 }}>Invite friends to PlanMesh</Text>
                    <Image source={right_arrow} style={{ width: 23, height: 16, alignItems: 'flex-end', alignSelf: 'center', marginRight: 20 }}></Image>
                </TouchableOpacity>
                <View style={{ height: 68, backgroundColor: "#fff", paddingLeft: 15, paddingRight: 15, justifyContent: 'center' }}>
                    <View style={{ backgroundColor: colors.lightgrey, height: 40, borderRadius: 20, justifyContent: 'center', flexDirection: 'row' }}>
                        <Icon name="ios-search" style={{ marginLeft: 45, color: "#B4B7BA", marginTop: 5 }} />
                        <TextInput onChangeText={(searchKey) => this.handleChange({ searchKey })} placeholderTextColor="#8F8F90" placeholder="Search" style={{ fontFamily: "Roboto-Regular", fontSize: 15, letterSpacing: 0.005, color: "#8F8F90", marginRight: 50, width: '83%', marginLeft: 5 }} ref={search => this.search = search} />
                        {
                            this.state.is_close_visible == true ?
                                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', height: 25, width: 25, position: 'absolute', right: 10, top: 7 }} onPress={() => this.onClearTextPressed()}>
                                    <Image source={closeicon} resizeMode='stretch' style={{ height: 17, width: 17 }} />
                                </TouchableOpacity>
                                :
                                null
                        }
                    </View>
                </View>
                <View style={{ height: Height(0.2), backgroundColor: '#EFEFF4' }} />
                <View style={{ backgroundColor: "#FFFFFF", height: 50, paddingLeft: 20, flexDirection: 'row' }}>
                    <Text style={[{ color: "#3a4759", fontFamily: "Roboto-Bold", fontSize: 16, letterSpacing: 0.005, alignItems: 'flex-start', alignSelf: 'center', flex: 1 }, checkFontWeight("700")]}>Select All</Text>
                    {
                        Platform.OS == 'ios' ?
                            <Switch ios_backgroundColor={'#E4E4E5'} trackColor={{ true: "#4bd964" }} value={this.state.selectAll} onValueChange={(value) => this.onSelectAll({ value })} style={{ alignItems: 'flex-end', alignSelf: 'center', marginRight: 20 }} />
                            :
                            <Switch thumbTintColor={'#ffffff'} trackColor={{ true: "#4bd964", false: '#ccc' }} value={this.state.selectAll} onValueChange={(value) => this.onSelectAll({ value })} style={{ alignItems: 'flex-end', alignSelf: 'center', marginRight: 20 }} />
                    }
                </View>
                <ScrollView keyboardDismissMode='on-drag'>
                    <FlatList
                        style={{}}
                        data={this.state.Data}
                        renderItem={
                            ({ item, index }) => this.loadTableCells(item, index)
                        }
                        keyExtractor={(item) => item.key}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                    />

                </ScrollView>
                <TouchableOpacity style={{ width: '100%' }} onPress={() => { this.onRequestSend() }}>
                    <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Send Friend Request</Text>
                    </ImageBackground>
                </TouchableOpacity>
                <SafeAreaView />
            </Container>
        );
    }
}

export default FindFriend;
