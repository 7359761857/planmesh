const React = require("react-native");
const { Dimensions,Platform } = React;

global.DEVICE_HEIGHT = Dimensions.get("window").height;
global.DEVICE_WIDTH = Dimensions.get("window").width;

global.font_SFProDisplay_Bold = "SFProDisplay-Bold";
global.font_SFProDisplay_Light = "SFProDisplay-Light";
global.font_SFProDisplay_Medium = "SFProDisplay-Medium";
global.font_SFProDisplay_Regular = "SFProDisplay-Regular";
global.font_SFProDisplay_Black = "SFProDisplay-Black";
global.font_SFProDisplay_SemiBold = "SFProDisplay-SemiBold";
global.font_SFProDisplay_Thin = "SFProDisplay-Thin";
global.font_MyriadPro_Regular = "MyriadPro-Regular";
global.font_MyriadPro_Bold = "MyriadPro-Bold";
global.font_MyriadPro_BoldSemiExt = "MyriadPro-BoldSemiExt";
global.font_MyriadPro_BoldSemiExt = "MyriadPro-BoldSemiExt";

//api urls
global.baseURL = "http://3.17.135.13:8080/api/"

global.userRegister = baseURL+ "userRegister";
global.userlogin = baseURL+ "userlogin";
global.verifyEmail = baseURL+ "verifyEmail";
global.resendVerifyEmail = baseURL+ "resendVerifyEmail";


global.home = baseURL+ "home";
global.userlogout = baseURL+ "userlogout";
global.userforgotpassword = baseURL+ "userforgotpassword";


global.pages = baseURL+ "pages";
global.checkfb = baseURL+ "checkfb";
global.editUserProfile = baseURL+ "editUserProfile";
global.changepassword = baseURL+ "changepassword";
global.userinfo = baseURL+ "userinfo";
global.checkCode = baseURL+ "checkCode";
global.inviteFrined = baseURL+ "inviteFrined";


//constant variable
global.userid = 0;
global.token = '';
global.deviceToken = '';
global.is_premium = '0';

global.checkFontWeight = function checkFontWeight(value){
    if(Platform.OS === 'android'){
       return {}
    }else{
       return {fontWeight: value}
    }
};



