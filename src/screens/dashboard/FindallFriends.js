import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, Keyboard, TouchableOpacity, StatusBar, Text, Platform } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import { Container, View, Button, Header, Left, Item, Icon, Right, Content, Input } from "native-base";
import { SearchBar } from 'react-native-elements';

import styles from "./style";
import ValidationComponent from 'react-native-form-validator';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
import { TouchableHighlight } from "react-native-gesture-handler";
const backWhite = require("../../../assets/images/backWhite.png");
const flowers = require("../../../assets/images/Dashboard/avtar.png");
import { API_ROOT } from "../../config/constant";
import RNProgressHUB from 'react-native-progresshub';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { store } from "../../redux/store";

import Toast from 'react-native-simple-toast';
const btnBg = require("../../../assets/images/btnBg.png");

const search_img = require("../../../assets/images/Dashboard/search.png");

const close = require("../../../assets/images/Dashboard/close.png");
import MixpanelManager from '../../../Analytics'
class FindallFriends extends ValidationComponent {

    constructor(props) {
        super(props);
        this.arrayholder = [];
        this.is_searchinng = false,
            this.search = null,
            this.did_focus = false,
            this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            if (this.state.text == '') {
                this.getRecommendedUsersAPICall()
            }
            else {
                this.is_searchinng = true
                this.searchUsersAPICall(this.state.text)
            }
        });
        this.state = {
            token: store.getState().auth.user.token,
            login_id: store.getState().auth.user.login_id,
            Friends: [],
            text: '',
            empty: true,
            searchresult: []
        };
    }
    componentDidMount() {
        this._navListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBarStyle('dark-content');
            Platform.OS == 'android' && StatusBar.setBackgroundColor('#fff');

        });

        this.getRecommendedUsersAPICall()

    }

    onPressViewProfile(item) {
        const MixpanelData = {
            'Search Text': this.state.text
        }
        this.mixpanel.identify(store.getState().auth.user.email_address);
        this.mixpanel.track('Search', MixpanelData);
        AsyncStorage.setItem('other_id', item.login_id)
        this.props.navigation.navigate('ohter_user_profile')
    }

    onPressUserName(item) {
        const MixpanelData = {
            'Search Text': this.state.text
        }
        this.mixpanel.identify(store.getState().auth.user.email_address);
        this.mixpanel.track('Search', MixpanelData);
        AsyncStorage.setItem('other_id', item.user_id)
        this.props.navigation.navigate('ohter_user_profile')
    }

    onPressAddFriend(item) {
        console.log('Item =>', item)
        //alert(JSON.stringify(item))

        if (item.is_friend == 2) {
            if (this.is_searchinng == true) {
                //this.cancelFriendRequestAPICall(item.login_id)
            }
            else {
                //this.cancelFriendRequestAPICall(item.user_id)
            }
        }
        else {


            const MixpanelData = {
                'Search Text': this.state.text
            }
            this.mixpanel.identify(store.getState().auth.user.email_address);
            this.mixpanel.track('Search', MixpanelData);
            //console.log('onPressAddFriend')
            this.addFriendAPICall(item.mobile_number, item.login_id, item.name)
        }
    }

    getRecommendedUsersAPICall() {
        //RNProgressHUB.showSpinIndeterminate();
        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        fetch(API_ROOT + 'people_may_you_know', {
            method: 'post',
            body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {

                    this.setState({ Friends: responseData.data })
                    this.arrayholder = responseData.data;
                    //RNProgressHUB.dismiss();


                } else {
                    this.setState({ empty: false })
                    //RNProgressHUB.dismiss();

                }
            })
            .catch((error) => {
                //console.log('People You May Know Error'+error);   
                //RNProgressHUB.dismiss();
                //alert('Oops your connection seems off, Check your connection and try again')
            })
    }


    cancelFriendRequestAPICall(other_id) {
        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('other_id', other_id)

        fetch(API_ROOT + 'friend_request/cancel', {
            method: 'post', body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {

                    if (this.is_searchinng == true) {
                        this.searchUsersAPICall(this.state.text)
                    }
                    else {
                        this.getRecommendedUsersAPICall()
                    }

                } else {
                    alert(responseData.text)
                }
            })
            .catch((error) => {
                // alert('Oops your connection seems off, Check your connection and try again') 
            })
    }

    addFriendAPICall(mobile, other_id, name) {
        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        //data.append('mobile_number', mobile)
        data.append('other_id', other_id)

        fetch(API_ROOT + 'friend_request', {
            method: 'post', body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {
                    let array = []
                    array.push(name)
                    const MixpanelData = {
                        '$email': store.getState().auth.user.email_address,
                        'Friends Selected': JSON.stringify(array),
                        'Friend Requests Sent': array.length,
                        'During Sign Up': 'False'
                    }
                    this.mixpanel.identify(store.getState().auth.user.email_address)
                    this.mixpanel.track('Send Friend Requests', MixpanelData);
                    console.log('Mixpanel data..', MixpanelData)
                    if (this.is_searchinng == true) {
                        this.searchUsersAPICall(this.state.text)
                    }
                    else {
                        this.getRecommendedUsersAPICall()
                    }


                } else {
                    alert(responseData.text)
                }
            })
            .catch((error) => {
                // alert('Oops your connection seems off, Check your connection and try again') 
            })
    }

    searchUsersAPICall(text) {
        var data = new FormData()
        data.append('token', this.state.token)
        data.append('login_id', this.state.login_id)
        data.append('txt_filter', text)

        //console.log('Search Request: '+JSON.stringify(data))

        fetch(API_ROOT + 'user/list', {
            method: 'post', body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData.success) {
                    //console.log('Search Request:'+JSON.stringify(responseData.data))
                    //console.log('Searching: '+this.is_searchinng+' Empty: '+this.state.empty)
                    this.setState({ searchresult: responseData.data })
                } else {
                    //this.is_searchinng = false
                    this.setState({ empty: true, searchresult: [] })
                    //alert(responseData.text)
                }
            })
            .catch((error) => {
                // alert('Oops your connection seems off, Check your connection and try again') 
            })
    }

    componentWillUnmount() {
        this._navListener.remove();
    }

    clearText(text) {
        this.is_searchinng = false
        this.did_focus = false
        this.getRecommendedUsersAPICall()
        this.setState({ text: '', searchresult: [] })
        this.search.clear()
        setTimeout(() => {
            this.search.blur()
        }, 100)
    }

    onFocusDid() {
        this.did_focus = true
        this.is_searchinng = true
        this.forceUpdate()
    }

    didEndEditing() {
        if (this.state.text == '') {
            this.did_focus = false
            this.is_searchinng = false
            this.forceUpdate()
        }
    }

    SearchFilterFunction(text) {

        this.setState({ text: text })
        //alert(this.state.text)

        if (text != '') {
            //alert(this.state.text)
            this.is_searchinng = true
            this.searchUsersAPICall(text)
        }
        else {
            this.is_searchinng = false
            this.getRecommendedUsersAPICall()
        }

        if (this.state.text.length == 1) {
            this.is_searchinng = false
            this.getRecommendedUsersAPICall()
        }

        // if(text == '')
        // {
        //     this.is_searchinng = false
        //     this.getRecommendedUsersAPICall()
        // }
        // //passing the inserted text in textinput
        // const newData = this.arrayholder.filter((item) => {
        //     //applying filter for the inserted text in search bar
        //     const itemData = item.name ? item.name.toUpperCase() : ''.toUpperCase();
        //     const textData = text.toUpperCase();
        //     return itemData.indexOf(textData) > -1;
        // });
        // this.setState({
        //     //setting the filtered newData on datasource
        //     //After setting the data it will automatically re-render the view
        //     Friends: newData,
        //     text: text,
        // });
    }

    render() {

        StatusBar.setBarStyle('dark-content', true);

        return (
            <Container style={{ flex: 1, backgroundColor: colors.white }}>
                <Toastt ref="toast"></Toastt>
                <BackgroundImage >

                    <SafeAreaView style={[{ flex: 0, backgroundColor: '#fff' }]} />
                    <StatusBar barStyle="dark-content" backgroundColor="#ecf0f1" />
                    {/* <View style={{ backgroundColor: '#fff', width: Width(100), height: Height(5) }}> */}
                    <SearchBar
                        containerStyle={{ backgroundColor: "#fff", borderBottomColor: 'transparent', borderTopColor: 'transparent', fontFamily: fonts.Roboto_Regular }}
                        inputContainerStyle={{ borderBottomColor: 'transparent', borderTopColor: 'transparent', backgroundColor: '#EEEEEE', width: Width(90), marginLeft: Width(2.5), alignItems: 'center', fontFamily: fonts.Roboto_Regular }}
                        placeholderTextColor='#8F8F90'
                        inputStyle={{ fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}
                        placeholder="Search for people"
                        returnKeyType="search"
                        round={true}
                        onFocus={() => this.onFocusDid()}
                        onChangeText={text => this.SearchFilterFunction(text)}
                        onEndEditing={() => this.didEndEditing()}
                        value={this.state.text}
                        ref={search => this.search = search}
                        searchIcon={() => <Image source={search_img} style={{ tintColor: '#B4B7BA', marginLeft: Width(1) }} />}
                        clearIcon={() => <TouchableOpacity onPress={() => this.clearText('')}><Ionicons name='md-close-circle' color='#B4B7BA' size={22} /></TouchableOpacity>}
                        cancelIcon={() => <TouchableOpacity onPress={() => this.clearText('')}><Ionicons name='md-close-circle' color='#B4B7BA' size={22} /></TouchableOpacity>}
                    />
                    {/* </View> */}

                    {/* <View style={{ height: Height(2) }}></View> */}

                    {

                        this.is_searchinng == true ?

                            <ScrollView onScroll={() => { Keyboard.dismiss() }} keyboardShouldPersistTaps={'handled'} style={{ marginTop: 10 }}>

                                <View style={{ backgroundColor: '#fff', width: Width(98) }}>

                                    {this.state.searchresult.length == 0 ? <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ marginTop: Height(30), fontSize: 16, fontFamily: fonts.Raleway_Medium, color: colors.fontDarkGrey }}></Text>
                                    </View> :
                                        <View style={{ marginBottom: Height(5) }}>
                                            {this.state.searchresult.map(item => {
                                                return (
                                                    <View style={{}}>
                                                        <View style={{ height: Height(5), flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white, marginBottom: '2%' }}>
                                                            <View style={{ width: Width(15), alignItems: 'center', marginLeft: 7 }}>

                                                                {item.is_friend == 1 ?

                                                                    <TouchableHighlight style={{ height: Height(4.5), width: Height(4.5), borderRadius: Height(4.5), overflow: 'hidden' }} onPress={() => this.onPressViewProfile(item)}>
                                                                        <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.photo }} />
                                                                    </TouchableHighlight>
                                                                    :
                                                                    <TouchableHighlight style={{ height: Height(4.5), width: Height(4.5), borderRadius: Height(4.5), overflow: 'hidden' }} onPress={() => this.onPressViewProfile(item)}>
                                                                        <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.photo }} />
                                                                    </TouchableHighlight>
                                                                }

                                                            </View>
                                                            <View style={{ width: Width(15), justifyContent: 'center', flex: 4.5 }}>
                                                                {
                                                                    item.is_friend == 1 ?
                                                                        <Text style={{ fontSize: FontSize(16), color: colors.whatFontColor, fontFamily: fonts.Roboto_Medium }} onPress={() => this.onPressViewProfile(item)}>{item.name}</Text>
                                                                        :
                                                                        <Text style={{ fontSize: FontSize(16), color: colors.whatFontColor, fontFamily: fonts.Roboto_Medium }} onPress={() => this.onPressViewProfile(item)}>{item.name}</Text>
                                                                }

                                                            </View>
                                                            {
                                                                item.is_friend == 1 ?
                                                                    // <TouchableOpacity style={{ borderColor: colors.blue, borderWidth: 0.7, height: Height(3.8), width: Width(22), borderRadius: Height(2), marginRight: Width(4), alignItems: 'center', justifyContent: 'center' }} onPress = {()=> this.onPressViewProfile(item)}>
                                                                    // <Text style={{ color: colors.blue, fontSize: FontSize(12), fontFamily: fonts.Roboto_Regular }}>View Profile</Text>
                                                                    // </TouchableOpacity>
                                                                    null
                                                                    :
                                                                    <TouchableOpacity style={item.is_friend == 2 ? { borderColor: '#7c7c7e', backgroundColor: '#7c7c7e', borderWidth: 0.7, height: Height(3.8), width: Width(22), borderRadius: Height(2), marginRight: Width(3.8), alignItems: 'center', justifyContent: 'center' } : { borderColor: colors.blue, borderWidth: 0.7, height: Height(3.8), width: Width(22), borderRadius: Height(2), marginRight: Width(4), alignItems: 'center', justifyContent: 'center' }} onPress={() => this.onPressAddFriend(item)}>
                                                                        {item.is_friend == 2 ? <Text style={{ color: colors.white, fontSize: FontSize(12), fontFamily: fonts.Roboto_Regular }}>Pending</Text> :
                                                                            <Text style={{ color: colors.blue, fontSize: FontSize(12), fontFamily: fonts.Roboto_Regular }}>Add Friend</Text>
                                                                        }
                                                                    </TouchableOpacity>

                                                            }

                                                            {/* <TouchableOpacity style={item.is_friend_request_sent == 1 ? { borderColor: colors.blue,backgroundColor:colors.blue,borderWidth: 0.7, height: Height(3.8), width: Width(22), borderRadius: Height(2), marginRight: Width(4), alignItems: 'center', justifyContent: 'center' } : { borderColor: colors.blue, borderWidth: 0.7, height: Height(3.8), width: Width(22), borderRadius: Height(2), marginRight: Width(4), alignItems: 'center', justifyContent: 'center' }} onPress = {()=> this.onPressAddFriend(item)}>
                                                        {item.is_friend_request_sent == 1 ? <Text style={{ color: colors.white, fontSize: FontSize(12), fontFamily: fonts.Roboto_Regular }}>Cancel request</Text>:
                                                        <Text style={{ color: colors.blue, fontSize: FontSize(12), fontFamily: fonts.Roboto_Regular }}>Add Friend</Text>
                                                        }
                                                    </TouchableOpacity> */}
                                                        </View>
                                                        <View style={{ height: Height(.5) }} />

                                                    </View>
                                                );
                                            })}
                                        </View>}

                                </View>

                            </ScrollView>

                            :
                            this.did_focus == true ? null
                                :

                                <ScrollView>

                                    {this.state.Friends.length == 0 ? null :
                                        <Text style={{
                                            marginLeft: Width(5.5),
                                            marginBottom: Height(2),
                                            marginTop: 18,
                                            // marginVertical: Height(2), 
                                            color: '#363169',
                                            fontFamily: fonts.Roboto_Bold,
                                            fontSize: FontSize(14)
                                        }}>People You May Know</Text>
                                    }

                                    <View style={{ backgroundColor: '#fff', width: Width(98) }}>

                                        {this.state.Friends.length == 0 ? <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                            {/* <Text style={{ marginTop: Height(30), fontSize: 16, fontFamily: fonts.Raleway_Medium, color: colors.fontDarkGrey }}>No Friends!</Text> */}
                                        </View> :
                                            <View style={{ marginBottom: Height(5) }}>
                                                {this.state.Friends.map(item => {
                                                    return (
                                                        <View style={{}}>
                                                            <View style={{ height: Height(5), flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white, marginBottom: '2%' }}>
                                                                <View style={{ width: Width(15), alignItems: 'center', marginLeft: 7 }}>
                                                                    <TouchableHighlight style={{ height: Height(4.5), width: Height(4.5), borderRadius: Height(4.5), overflow: 'hidden' }} onPress={() => this.onPressUserName(item)}>
                                                                        <Image style={{ height: '100%', width: '100%' }} source={{ uri: item.photo }} />
                                                                    </TouchableHighlight>
                                                                </View>
                                                                <View style={{ width: Width(15), justifyContent: 'center', flex: 4.5 }}>
                                                                    <Text style={{ fontSize: FontSize(16), color: colors.whatFontColor, fontFamily: fonts.Roboto_Medium }} onPress={() => this.onPressUserName(item)}>{item.name}</Text>
                                                                </View>
                                                                <TouchableOpacity style={item.is_friend == 2 ? { borderColor: '#7c7c7e', backgroundColor: '#7c7c7e', borderWidth: 0.7, height: Height(3.8), width: Width(22), borderRadius: Height(2), marginRight: Width(3.8), alignItems: 'center', justifyContent: 'center' } : { borderColor: colors.blue, borderWidth: 0.7, height: Height(3.8), width: Width(22), borderRadius: Height(2), marginRight: Width(3.8), alignItems: 'center', justifyContent: 'center' }} onPress={() => this.onPressAddFriend(item)}>
                                                                    {item.is_friend == 2 ? <Text style={{ color: colors.white, fontSize: FontSize(12), fontFamily: fonts.Roboto_Regular }}>Pending</Text> :
                                                                        <Text style={{ color: colors.blue, fontSize: FontSize(12), fontFamily: fonts.Roboto_Regular }}>Add Friend</Text>
                                                                    }
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={{ height: Height(.5) }} />

                                                        </View>
                                                    );
                                                })}
                                            </View>}

                                    </View>

                                </ScrollView>


                    }

                    {/* {this.state.Friends == '' ?
                        <TouchableOpacity style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }} onPress={() => this.props.navigation.push('FindFriends_Setting')} >
                            <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Find your friends</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                        : null} */}

                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default FindallFriends;