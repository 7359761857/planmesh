import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, ScrollView, ImageBackground, TouchableOpacity, StatusBar, TouchableHighlight, Text, Platform, TextInput } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Button, Header, Left, Right, Content, Item, Body } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FloatingLabel from 'react-native-floating-labels';
import { Height, Width, FontSize, colors } from "../../config/dimensions";
import { fonts } from "../../config/constant";
const backWhite = require("../../../assets/images/backWhite.png");
const btnBgGrey = require("../../../assets/images/btnBg.png");
const flowers = require("../../../assets/images/Dashboard/flowers.png");
const cup = require("../../../assets/images/Dashboard/full_cup.png");
const Location_new = require("../../../assets/images/Dashboard/full_Location.png");
const time = require("../../../assets/images/Dashboard/full_time.png");
const unSelectRadio = require("../../../assets/images/Dashboard/unSelectRadio.png");
const SelectRadio = require("../../../assets/images/selectRadio.png");
import moment from 'moment';
import { API_ROOT } from "../../config/constant";
import { store } from "../../redux/store";


const current_location = require("../../../assets/images/current_location.png");


class EditPrivacy extends ValidationComponent {
    _didFocusSubscription
    constructor(props) {
        super(props);
        this.state = {
            token: store.getState().auth.user.token,
            login_id: store.getState().auth.user.login_id,
            plan_id:this.props.navigation.state.params.plan_id,
            name: 'Leaticia',
            value: 'friend',
            friendName: 'Friends',
            share_with: 1,
            shareList: [{
                name: 'Friends',
                desc: 'Your friends on Planmesh',
                key: 'friend',
                id: 1,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },
            {
                name: 'Friends of friends',
                desc: 'Your friends and the friends of your friends on Planmesh',
                key: 'fof',
                id: 2,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },
            {
                name: 'Only me',
                desc: 'Only me',
                key: 'me',
                id: 0,
                flowers: require("../../../assets/images/Dashboard/flowers.png")
            },

            ]
        },
        this._navListener = props.navigation.addListener('didFocus', payload => {
                this.setPrivacy()
        }
        )

    }

    setPrivacy()
    {
            let curprivacy = this.props.navigation.getParam('privacy')

            //alert(curprivacy)

            if(curprivacy == 'Friends')
            {
                    this.setState({value:'friend', friendName:'Friends', share_with:1})
            }
            else if (curprivacy == 'Only me')
            {
                    this.setState({value:'me', friendName:'Only me', share_with:0})
            }
            else{
                    this.setState({value:'fof', friendName:'Friends of friends', share_with:2})
            }

    }

    addFriends=()=>{
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('plan_id', this.state.plan_id)
    data.append('login_id', this.state.login_id)
    data.append('share_with', this.state.share_with)

    fetch(API_ROOT + 'plan/privacy/edit', {
      method: 'post', body: data
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
            this.props.navigation.goBack()
            //this.props.navigation.navigate('dashboard', { refreshing: true })
        } else {
          alert(responseData.text)
        }
      })
      .catch((error) => { 
          //alert('Oops your connection seems off, Check your connection and try again') 
        })
    }

    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={[styles.headerAndroidnav,{height:44}]}>
                    <StatusBar barStyle="light-content" backgroundColor="#41199B" />
                    <View style={{ flexDirection: 'row', bottom: Height(2.5), position: 'absolute', left: Width(5) }}>
                        <TouchableOpacity onPress={() => {
                            navigation.goBack()
                        }}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity></View>
                    <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                        <Text style={styles.headerTitle} >Edit Privacy</Text>
                    </View>

                </Header>
            )
        }
    }


    goBack = () => {
        this.props.navigation.goBack()
    }
    render() {
        const { value } = this.state;
        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: '#EFEFF4' }} >
                <Toastt ref="toast"></Toastt>
                <BackgroundImage >
                    <SafeAreaView />
                    <View style={{ height: Height(1), backgroundColor: colors.white }}></View>

                    <Content>
                        <View style={{ height: Height(3), backgroundColor: colors.white }} />
                        <View style={{ backgroundColor: colors.white }}>
                            <Text style={{ backgroundColor: colors.white, marginLeft: Width(3.5), fontSize: FontSize(16), color: colors.dargrey, fontFamily: fonts.Raleway_Bold }}>
                                Who can see your post?
                            </Text>
                            <Text style={{ backgroundColor: colors.white, marginLeft: Width(3.5), marginTop: 2, fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Raleway_Regular }}>
                                Your post will show up in the feed and on your profile.
                            </Text>
                        </View>
                        <View style={{ height: Height(2), backgroundColor: colors.white }} />

                        <View style={{ backgroundColor: colors.white }}>
                            {this.state.shareList.map(item => {
                                return (
                                    <View >
                                        <TouchableOpacity onPress={() => {
                                            this.setState({
                                                share_with: item.id,
                                                value: item.key,
                                                friendName: item.name
                                            });

                                        }} style={{ margin: '1%', flexDirection: 'row', alignItems: 'center', backgroundColor: colors.white }}>
                                            <View style={{ marginLeft: Width(3), width: Width(20), justifyContent: 'center', flex: 4.5 }}>
                                                <Text style={{ fontSize: FontSize(16), color: colors.dargrey, fontFamily: fonts.Raleway_Bold }}>{item.name}</Text>
                                                <Text style={{ marginTop: 2, fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Raleway_Regular }}>{item.desc}</Text>

                                            </View>
                                            <View style={{ alignItems: 'flex-end', right: Width(3), flex: 2 }}>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this.setState({
                                                            share_with: item.id,
                                                            value: item.key,
                                                            friendName: item.name
                                                        });

                                                    }} style={{
                                                        height: 20,
                                                        width: 20,
                                                        borderRadius: 100,
                                                        borderWidth: 1,
                                                        borderColor: colors.dargrey,
                                                        alignItems: 'center',
                                                        justifyContent: 'center',
                                                    }} >
                                                    {value === item.key && <View>
                                                        <Image source={SelectRadio} style={{}} />
                                                    </View>}
                                                </TouchableOpacity>
                                            </View>
                                        </TouchableOpacity>
                                        <View style={{ height: Height(1) }} />

                                    </View>

                                );
                            })}

                            <View style={{ height: Height(35), backgroundColor: colors.white }} />


                        </View>



                    </Content>
                    <TouchableOpacity style={{ width: '103%', marginBottom: Height(2), alignSelf: 'center' }} onPress={this.addFriends} >
                        <ImageBackground source={btnBgGrey} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Done</Text>
                        </ImageBackground>
                    </TouchableOpacity>
                    <SafeAreaView />
                </BackgroundImage>
            </Container>
        );
    }
}

export default EditPrivacy;