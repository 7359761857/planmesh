import React from 'react';
import { Text, Image, View } from 'react-native';
import { connect } from 'react-redux';
import { store } from "../../redux/store";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { FontSize} from "../../config/dimensions";

const ic_redcircle = require("../../../assets/images/Dashboard/ic_red_circle.png");

class HomeIcon extends React.Component {
  constructor(props) {
    // anything you need in the constructor
    super(props)
  }

  render() {
    const { notifications, tintColor } = this.props;

   // below is an example notification icon absolutely positioned 
    return (
        
        <View>
            <MaterialIcons name='home' size={25} color={tintColor} />
            {
                notifications > 0 ?
                <View style= {{backgroundColor:'red', position:'absolute',height:17,width:17,top:-9,right:-9, borderRadius:8.5, justifyContent:'center'}}>
                    <Image source = {ic_redcircle}></Image>
                </View>
                :
                null
            }
        </View>    
    );
}

}
const mapStateToProps = state => ({ notifications: store.getState().auth.new_post_count});
export default connect(mapStateToProps, null)(HomeIcon);