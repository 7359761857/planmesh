const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import { isIphoneX } from '../Default/is-iphone-x'
const HEADER_SIZE = isIphoneX() ? 84 : 100;
const HEADER_PADDING_SIZE = isIphoneX() ? 40 : 16;
import { Height, Width, FontSize, colors } from './../../config/dimensions'
import { fonts } from "../../config/constant";

export default {

  headerAndroidnav: {
    backgroundColor: '#41199B',
    shadowOpacity: 0,
    borderBottomWidth: 0,
    ...Platform.select({
      ios: {
        height: HEADER_SIZE,
        paddingTop: HEADER_PADDING_SIZE,
      },
      android: {
        
      },
    }),
  },
  headerTitle: {
    letterSpacing: 0.02,
    fontSize: 20,
    fontFamily: "Raleway-Medium",
    color: 'white',
    ...Platform.select({
      ios: {
        fontWeight: '500',
      },
      android: {
      },
    })
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },

  headerPlaneColor: {
    justifyContent: 'center',
    fontSize: FontSize(14),
    marginLeft: 3,
    color: '#8F8F90',
    fontFamily: fonts.Roboto_Regular
  },
  textInputIos: {
    height: 18, width: 14,
    marginTop: '2%',
    marginRight: '2%'
  },
  textInputAndroid: {
    marginTop: '2%',
    marginRight: '2%'
  },
  item: {
    paddingLeft: 20,
    paddingVertical: 20,
    backgroundColor: 'wheat'
  },
  separator: {
    height: 1,
    backgroundColor: 'gray'
  },
  header: {
    flex: 1,
    backgroundColor: 'green',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row'
  },
  backButton: {
    height: 94,
    width: 94,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontWeight: 'bold',
    fontSize:FontSize(14),
    color: 'white'
  },
 
  

};
