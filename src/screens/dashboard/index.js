import AsyncStorage from '@react-native-community/async-storage'
import Geolocation from '@react-native-community/geolocation'
import moment from 'moment'
import 'moment/locale/en-au' // without this line it didn't work
import { View } from 'native-base'
import React from 'react'
import {
  Alert,
  Animated,
  AppState,
  Dimensions,
  Easing,
  FlatList,
  Image,
  ImageBackground,
  PermissionsAndroid,
  Platform,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  Share,
  StatusBar,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
} from 'react-native'
import RNCalendarEvents from 'react-native-calendar-events'
import Contacts from 'react-native-contacts'
import ValidationComponent from 'react-native-form-validator'
import Modal from 'react-native-modal'
import { check, PERMISSIONS, RESULTS } from 'react-native-permissions'
import SeeMore from 'react-native-see-more-inline'
import Toast from 'react-native-simple-toast'
import SendSMS from 'react-native-sms'
//import ActionButton from 'react-native-action-button';
import SplashScreen from 'react-native-splash-screen'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Entypo from 'react-native-vector-icons/Entypo'
import MixpanelManager from '../../../Analytics'
import { API_ROOT, fonts } from '../../config/constant'
import { LOGOUT, NEWPOST, NOTIFICATION } from '../../redux/actions/types'
import { store } from '../../redux/store'
import NavigationService from '../../services/NavigationService'
//import Geocoder from 'react-native-geocoder';
import {
  colors,
  FontSize,
  Height,
  isIphone,
  isIphoneX,
  Width,
} from './../../config/dimensions'
import HomeIcon from './HomeIcon'

var dateFormat = require('dateformat')

//import {showMessage, hideMessage} from "react-native-flash-message";

const backWhite = require('../../../assets/images/backWhite.png')
const appIcon = require('../../../assets/images/Dashboard/Icon.png')
const black_heart = require('../../../assets/images/Dashboard/black_heart.png')
const going1 = require('../../../assets/images/Dashboard/going1.png')

moment.locale('en-au')

const avtar = require('../../../assets/images/Dashboard/avtar.png')
const notificationBell = require('../../../assets/images/Dashboard/notificationBell.png')
const profile_icon = require('../../../assets/images/Dashboard/profile_icon.png')
const MdAdd = require('../../../assets/images/Dashboard/plus_btn.png')
const menu0 = require('../../../assets/images/Dashboard/who_new.png')
const cup = require('../../../assets/images/Dashboard/home_cup_post.png')
const Location_new = require('../../../assets/images/Dashboard/Group_3454.png')
const checked = require('../../../assets/images/Dashboard/checked.png')
const intersted = require('../../../assets/images/Dashboard/Favorite_uncolr.png')
const circle_cancel = require('../../../assets/images/Dashboard/circle_cancel.png')
const circle_favorite = require('../../../assets/images/Dashboard/circle_favorite.png')
const circle_tick = require('../../../assets/images/Dashboard/Tick_mark.png')
const plus_icon = require('../../../assets/images/Dashboard/plus_icon.png')
const cross_icon = require('../../../assets/images/Dashboard/cross_icon.png')

const going = require('../../../assets/images/Dashboard/Tick_uncolor.png')
const who = require('../../../assets/images/Dashboard/who.png')
const menu1 = require('../../../assets/images/Dashboard/menu1.png')
const menu2 = require('../../../assets/images/Dashboard/menu2.png')
const menu3 = require('../../../assets/images/Dashboard/menu3.png')
const chai_cup = require('../../../assets/images/Dashboard/chai_cup.png')
const location = require('../../../assets/images/Dashboard/location_home.png')
const calendar = require('../../../assets/images/Dashboard/calendar.png')
const new_user = require('../../../assets/images/Dashboard/new_user.png')
const bg = require('../../../assets/images/Dashboard/bg.png')
const share = require('../../../assets/images/Dashboard/share.png')
const Delete = require('../../../assets/images/Dashboard/Delete.png')
const Edit = require('../../../assets/images/Dashboard/Edit.png')
const btnBg = require('../../../assets/images/Login/inviteButton.png')
const ic_chat = require('../../../assets/images/Dashboard/chat_icon.png')
const ic_who = require('../../../assets/images/Dashboard/ic_who.png')
const ic_what = require('../../../assets/images/Dashboard/ic_what.png')
const ic_when = require('../../../assets/images/Dashboard/ic_when.png')
const ic_where = require('../../../assets/images/Dashboard/ic_where.png')
const ic_favourite_activity = require('../../../assets/images/Dashboard/ic_favorite_activity.png')
const ic_fitness_center = require('../../../assets/images/Dashboard/ic_fitness_center.png')
const ic_houseparty = require('../../../assets/images/Dashboard/ic_houseparty.png')
const ic_local_bar = require('../../../assets/images/Dashboard/ic_local_bar.png')
const ic_local_movies = require('../../../assets/images/Dashboard/ic_local_movies.png')
const ic_phone = require('../../../assets/images/Dashboard/ic_phone.png')
const ic_videocam = require('../../../assets/images/Dashboard/ic_videocam.png')
const ic_videogame = require('../../../assets/images/Dashboard/ic_videogame.png')
const ic_zoom = require('../../../assets/images/Dashboard/ic_zoom.png')
const default_placeholder = require('../../../assets/images/Dashboard/default_placeholder.png')

const { height, width } = Dimensions.get('window')
//const NAVBAR_HEIGHT = Platform.OS == 'android' ? Height(12.5) : Height(12.5);
const NAVBAR_HEIGHT = Platform.OS == 'ios' ? (height >= 812 ? 85 : 64) : 64
const STATUS_BAR_HEIGHT = Platform.select({ ios: Height(5), android: 0 })

const AnimatedListView = Animated.createAnimatedComponent(FlatList)
class Dashboard extends ValidationComponent {
  _didFocusSubscription
  offset = 0
  _didBlurSubscription

  constructor(props) {
    super(props)
    const scrollAnim = new Animated.Value(0)
    const offsetAnim = new Animated.Value(0)
    this.mixpanel = MixpanelManager.sharedInstance.mixpanel
    this.arrayholder = []
    this.scroll = null
    this.flatlist_offset = 0
    this.randomnumber = 0
    this.is_post_loaded = false
    this.eventtitle = ''
    this.currentitemindex = -1
    this.mypostcount = 0
    this.offsetInHours = 0
    this.active = true
    ;(this.getcount_called = false),
      //this.arronlineactivity = ["Zoom Call","Houseparty","Video Chat","Phone Call","Virtual Happy Hour","Game night","Movie night","Virtual Date night","Fitness session"]
      (this.arronlineactivity = [
        { title: 'Video Chat', icon: ic_videocam },
        { title: 'Phone Call', icon: ic_phone },
        { title: 'Virtual Happy Hour', icon: ic_local_bar },
        { title: 'Game night', icon: ic_videogame },
        { title: 'Movie night', icon: ic_local_movies },
        { title: 'Virtual Date night', icon: ic_favourite_activity },
        { title: 'Fitness session', icon: ic_fitness_center },
        { title: 'Zoom Call', icon: ic_zoom },
        { title: 'Houseparty', icon: ic_houseparty },
      ])
    this.state = {
      menuImage: MdAdd,
      isOpen: false,
      isModalVisible: false,
      isMyModalVisible: false,
      isMyDeleteModalVisible: false,
      offset: 0,
      limit: 10,
      refreshing: false,
      token: store.getState().auth.user.token,
      login_id: store.getState().auth.user.login_id,
      name: store.getState().auth.user.name,
      photo: store.getState().auth.user.photo,
      post_name: '',
      other_id: '',
      plan_id: '',
      city: '',
      text: '',
      overlay: false,
      hide: false,
      status: true,
      is_like: false,
      is_new_post_available: false,
      timer: null,
      planList: [],
      empty: false,
      scrollAnim,
      offsetAnim,
      temp: false,
      //appState:AppState.currentState,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          scrollAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: 'clamp',
          }),
          offsetAnim,
        ),
        0,
        NAVBAR_HEIGHT - STATUS_BAR_HEIGHT,
      ),
      Contacts: [],
      Peoples: [],
      currentitem: '',
      is_muted: false,
    }
    subThis = this
    // this._didFocusSubscription = props.navigation.addListener('didFocus', payload => {
    // }
    // )

    this._didBlurSubscription = props.navigation.addListener(
      'didBlur',
      (payload) => {
        this.active = false
        this.resetListeners()
      },
    )

    // this._navListener = this.props.navigation.addListener('didFocus', () => {

    // });
    this.focusListener = this.props.navigation.addListener('didFocus', () => {
      //this.checkContactPermission()
      this.clearPlanDetails()
      this.active = true

      store.dispatch({ type: NEWPOST, data: 0 })

      global.planID = ''
      global.isEventDetails = false

      this.getMyPostList()

      StatusBar.setBarStyle('light-content')
      Platform.OS == 'android' && StatusBar.setBackgroundColor('#41199B')

      this.setUpTimer()

      AsyncStorage.getItem('overlay').then((value) => {
        if (value != null && value == 'true') {
          //this.setState({ overlay: true, temp: true,isOpen:true })
          this.openModel()
        } else {
          this.setState({ overlay: false, temp: false, isOpen: false })
        }
      })
      this.setState({
        photo: store.getState().auth.user.photo,
        name: store.getState().auth.user.name,
      })

      if (global.appIsinBackground == false) {
        this.onRefresh()
      }

      this.getPeople()

      if (global.freshlaunch) {
        setTimeout(this.onNotificationClick, 1500)
      }
    })
  }
  _clampedScrollValue = 0
  _offsetValue = 0
  _scrollValue = 0

  componentWillMount() {
    this.animatedValue = new Animated.Value(0)
    this.loadContacts()
  }

  componentWillReceiveProps(props) {
    // if (props.navigation.state.params != null) {
    //   if (props.navigation.state.params.refreshing == true) {
    //     this.onRefresh()
    //   }
    // }
  }

  clearPlanDetails() {
    AsyncStorage.removeItem('receiver_id')
    AsyncStorage.removeItem('Activity')
    AsyncStorage.removeItem('Location')
    AsyncStorage.removeItem('CurrentDate')
    AsyncStorage.removeItem('start')
    AsyncStorage.removeItem('end')
    AsyncStorage.removeItem('who_desc')

    AsyncStorage.removeItem('What_Activity')
    AsyncStorage.removeItem('what_Location')
    AsyncStorage.removeItem('what_CurrentDate')
    AsyncStorage.removeItem('what_friends_name')
    AsyncStorage.removeItem('what_friends')
    AsyncStorage.removeItem('what_start')
    AsyncStorage.removeItem('what_end')
    AsyncStorage.removeItem('what_desc')
    AsyncStorage.removeItem('what_FullAddress')

    AsyncStorage.removeItem('when_Activity')
    AsyncStorage.removeItem('when_Location')
    AsyncStorage.removeItem('when_CurrentDate')
    AsyncStorage.removeItem('when_friends')
    AsyncStorage.removeItem('when_friends_name')
    AsyncStorage.removeItem('when_start')
    AsyncStorage.removeItem('when_end')
    AsyncStorage.removeItem('when_desc')
    AsyncStorage.removeItem('when_FullAddress')

    AsyncStorage.removeItem('where_Activity')
    AsyncStorage.removeItem('where_Location')
    AsyncStorage.removeItem('where_CurrentDate')
    AsyncStorage.removeItem('where_friends')
    AsyncStorage.removeItem('where_friends_name')
    AsyncStorage.removeItem('where_start')
    AsyncStorage.removeItem('where_end')
    AsyncStorage.removeItem('where_desc')
    AsyncStorage.removeItem('where_FullAddress')
  }

  onNotificationClick() {
    //alert('Called...')

    if (global.data == null || global.data == undefined) {
      global.freshlaunch = false
      return
    }

    const data = global.data

    const plan_data = JSON.parse(global.data.plan_data)

    //alert(JSON.stringify(plan_data))

    //return

    if (data.notification_type == 'is_invite') {
      //alert(plan_data.plan_type)

      if (plan_data.plan_type == '1') {
        //alert('1')
        var name = plan_data.name.split(' ')
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: "Let's meet up!",
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
        //this.props.navigation.navigate('EventDetail',{item:plan_data, plan_id:plan_data.plan_id, other_id:plan_data.login_id, plan_type:plan_data.plan_type,title_header:"Let's meet up!",Final_date:'',eventby:plan_data.name, notificaitonid:data.notification_id})
      } else if (plan_data.plan_type == '2') {
        //alert('2')
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: plan_data.activity,
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      } else if (plan_data.plan_type == '3') {
        //alert('3')
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: 'Wanna go to ' + plan_data.line_1 + '?',
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      } else if (plan_data.plan_type == '4') {
        //alert('4')
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: "I'm free, wanna hangout?",
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      }

      //alert('hii')
    } else if (data.notification_type == 'is_intrested_my_plan') {
      // NavigationService.navigate('Friend_Status',{tabIndex:1, plan_id:plan_data.plan_id,notificaitonid:data.notification_id})

      if (plan_data.plan_type == '1') {
        //alert('1')
        var name = plan_data.friend_name.split(' ')
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: "Let's meet up!",
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      } else if (plan_data.plan_type == '2') {
        //alert('2')
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: plan_data.activity,
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      } else if (plan_data.plan_type == '3') {
        //alert('3')
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: 'Wanna go to ' + plan_data.line_1 + '?',
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      } else if (plan_data.plan_type == '4') {
        //alert('4')
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: "I'm free, wanna hangout?",
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      }
    } else if (data.notification_type == 'is_goin_my_plan') {
      //NavigationService.navigate('Friend_Status',{tabIndex:0, plan_id:plan_data.plan_id, notificaitonid:data.notification_id})

      if (plan_data.plan_type == '1') {
        //alert('1')
        var name = plan_data.friend_name.split(' ')
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: "Let's meet up!",
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      } else if (plan_data.plan_type == '2') {
        //alert('2')
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: plan_data.activity,
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      } else if (plan_data.plan_type == '3') {
        //alert('3')
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: 'Wanna go to ' + plan_data.line_1 + '?',
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      } else if (plan_data.plan_type == '4') {
        //alert('4')
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: "I'm free, wanna hangout?",
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      }
    } else if (data.notification_type == 'is_intrested_other_going') {
      NavigationService.navigate('Friend_Status', {
        tabIndex: 0,
        plan_id: plan_data.plan_id,
        notificaitonid: data.notification_id,
        Title_New: plan_data.activity,
        plan_type: plan_data.plan_type,
      })
    } else if (
      data.notification_type == 'is_suggestion_my_plan' ||
      data.notification_type == 'is_intrested_plan_suggestion' ||
      data.notification_type == 'is_set_final_suggestion' ||
      data.notification_type == 'is_upvote_my_plan' ||
      data.notification_type == 'is_intrested_other_upvote'
    ) {
      if (data.s_type == 'activity') {
        NavigationService.navigate('SuggestActivity', {
          other_id: plan_data.login_id,
          plan_id: plan_data.plan_id,
          is_direct: false,
          notificaitonid: data.notification_id,
        })
      } else if (data.s_type == 'datetime') {
        NavigationService.navigate('SuggestDateTime', {
          other_id: plan_data.login_id,
          plan_id: plan_data.plan_id,
          is_direct: false,
          notificaitonid: data.notification_id,
        })
      } else if (data.s_type == 'location') {
        NavigationService.navigate('SuggestLocation', {
          other_id: plan_data.login_id,
          plan_id: plan_data.plan_id,
          is_direct: false,
          notificaitonid: data.notification_id,
        })
      }
    } else if (data.notification_type == 'is_intrested_plan_chat') {
      if (plan_data.plan_type == 1) {
        var name = plan_data.friend_name
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: "Let's meet up!",
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      } else if (plan_data.plan_type == 2) {
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: plan_data.activity,
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      } else if (plan_data.plan_type == 3) {
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: 'Wanna go to ' + plan_data.line_1 + '?',
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      } else if (plan_data.plan_type == 4) {
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: "I'm free, wanna hangout?",
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      }
    } else if (
      data.notification_type == 'is_chat_my_plan' ||
      data.notification_type == 'is_invite_my_plan' ||
      data.notification_type == 'is_invite_their_plan'
    ) {
      if (plan_data.plan_type == 1) {
        var name = plan_data.friend_name
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: "Let's meet up!",
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      } else if (plan_data.plan_type == 2) {
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: plan_data.activity,
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      } else if (plan_data.plan_type == 3) {
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: 'Wanna go to ' + plan_data.line_1 + '?',
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      } else if (plan_data.plan_type == 4) {
        NavigationService.navigate('EventDetail', {
          item: plan_data,
          plan_id: plan_data.plan_id,
          other_id: plan_data.login_id,
          plan_type: plan_data.plan_type,
          title_header: "I'm free, wanna hangout?",
          Final_date: '',
          eventby: plan_data.name,
          notificaitonid: data.notification_id,
        })
      }
    } else if (data.notification_type == 'Friend Request') {
      // this.setState({index:1, notificaitonid:item.notification_id})
      // this.readNotification()
      // this.getFriendRequests()

      NavigationService.navigate('notification', {
        index: 1,
        notificaitonid: data.notification_id,
      })
    } else if (data.notification_type == 'Friend Request Accepted') {
      //alert(JSON.stringify(data))

      AsyncStorage.setItem('other_id', plan_data.sender_id)
      NavigationService.navigate('ohter_user_profile')
    } else if (data.notification_type == 'New Friend Register') {
      //alert(JSON.stringify(data))
      //AsyncStorage.setItem('other_id', plan_data.user_id)
      //NavigationService.navigate('ohter_user_profile')
    }
    global.plan_data = null
    global.freshlaunch = false
  }

  static navigationOptions = ({ navigation }) => {
    theNavigation = navigation
    return {
      header: null,
      tabBarIcon: ({ tintColor }) => <HomeIcon tintColor={tintColor} />,
    }
  }

  resetListeners() {
    clearInterval(this.state.timer)
    this.state.scrollAnim.removeAllListeners()
    this.state.offsetAnim.removeAllListeners()
    //this._navListener.remove();
  }

  componentDidMount() {
    // let mixpanel = MixpanelManager.sharedInstance.mixpanel;
    // mixpanel.identify(store.getState().auth.user.email_address);
    // mixpanel.getPeople().set("Last Launch App", moment().format('MMMM Do YYYY, h:mm:ss a'));
    // mixpanel.track('Launched App');

    SplashScreen.hide()

    if (Platform.OS == 'ios') {
      //RNCalendarEvents.authorizeEventStore()
      RNCalendarEvents.requestPermissions((readOnly = false))
    } else {
      // this library crashes on android when allowing calender permission
      // RNCalendarEvents.authorizeEventStore()
      this.requestCalendarPermission()
    }

    //alert('Global: '+global.noti_count)
    //alert('NotiCalled '+this.getcount_called)

    AppState.addEventListener('change', this.handleAppStateChange)

    this.randomnumber = Math.floor(Math.random() * 100) + 1

    //this.getPeople()
    //this.getPostList()

    if (global.isfromlogin == true) {
      this.getUserNotiCount()
    }

    //this.getCity() // temp

    Geolocation.getCurrentPosition(
      (position) => {
        this.fetchCurrentAddress(
          position.coords.latitude,
          position.coords.longitude,
        )
      },
      (error) => {
        this.setState({ isenableGps: true })
        this.setState({ error: error.message })
      },
      { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
    )

    this.state.scrollAnim.addListener(({ value }) => {
      const diff = value - this._scrollValue
      this._scrollValue = value
      this._clampedScrollValue = Math.min(
        Math.max(this._clampedScrollValue + diff, 0),
        NAVBAR_HEIGHT - STATUS_BAR_HEIGHT,
      )
    })
    this.state.offsetAnim.addListener(({ value }) => {
      this._offsetValue = value
    })
  }
  componentWillUnmount() {
    //clearInterval(this.state.timer);
    // this.state.scrollAnim.removeAllListeners();
    // this.state.offsetAnim.removeAllListeners();
    // this._navListener.remove();
  }

  checkForCalendarPermissionAndroid() {
    try {
      PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.WRITE_CALENDAR,
        PermissionsAndroid.PERMISSIONS.READ_CALENDAR,
      )
        .then((granted) => {
          if (!granted) {
            this.requestCalendarPermission()
          }
        })
        .catch((error) => {
          console.log('error while checking calendar permission!')
        })
    } catch (error) {
      console.log('error while checking calendar permission!')
    }
  }

  requestCalendarPermission() {
    PermissionsAndroid.requestMultiple([
      PermissionsAndroid.PERMISSIONS.READ_CALENDAR,
      PermissionsAndroid.PERMISSIONS.WRITE_CALENDAR,
    ]).then((result) => {
      if (
        result['android.permission.READ_CALENDAR'] &&
        result['android.permission.WRITE_CALENDAR'] === 'granted'
      ) {
        console.log('calendar permission granted...')
      } else {
        Toast.show('Please allow Calender permissions...')
      }
    })
  }

  handleAppStateChange = (nextAppState) => {
    console.log('the app state: ' + nextAppState)

    if (nextAppState === 'inactive' || nextAppState === 'background') {
      //console.log('the app state: '+nextAppState);
      global.appIsinBackground = true
      global.getcount_called = false
      var date = moment
        .utc()
        .subtract(5, 'seconds')
        .format('YYYY-MM-DD HH:mm:ss')
      AsyncStorage.setItem('notidate', date)
      console.log('BGDATE: ' + date)
      this.updateNotiCount()

      //this.getNotiCountAPICall(date)
      //this.getcount_called = false
    } else if (nextAppState === 'active') {
      global.appIsinBackground = false
      AsyncStorage.getItem('notidate').then((value) => {
        if (value != null) {
          if (Platform.OS == 'android') {
            if (this.getcount_called == false) {
              this.getcount_called = true
              this.getNotiCountAPICall(value)
            }
          } else {
            //this.updateNotiCount()
            this.getNotiCountAPICall(value)
          }
        }
      })
    }
  }

  _onScrollEndDrag = () => {
    this._scrollEndTimer = setTimeout(this._onMomentumScrollEnd, 250)
  }

  _onMomentumScrollBegin = () => {
    clearTimeout(this._scrollEndTimer)
  }

  _onMomentumScrollEnd = () => {
    const toValue =
      this._scrollValue > NAVBAR_HEIGHT &&
      this._clampedScrollValue > (NAVBAR_HEIGHT - STATUS_BAR_HEIGHT) / 2
        ? this._offsetValue + NAVBAR_HEIGHT
        : this._offsetValue - NAVBAR_HEIGHT

    Animated.timing(this.state.offsetAnim, {
      toValue,
      duration: 350,
      useNativeDriver: true,
    }).start()
  }

  fetchCurrentAddress = async (lat, long) => {
    let httpMethod = 'GET'
    let strURL =
      'https://maps.googleapis.com/maps/api/geocode/json?latlng=' +
      lat +
      ',' +
      long +
      '&sensor=true&key=AIzaSyA6JsG7CPXtIfYJJF3UFdz8z2eBDqohKA8'
    let headers = {}
    let params = {}

    APICall.callGoogleWebService(
      httpMethod,
      strURL,
      headers,
      params,
      (resJSON) => {
        //console.log('Google Response: '+JSON.stringify(resJSON))
        if (resJSON.results.length >= 1) {
          resJSON.results[0].address_components.map((locationInfo) => {
            if (locationInfo.types.indexOf('locality') != -1) {
              this.setState({ city: locationInfo.long_name })
              // this.setState({isenableGps:false,currentLocation:locationInfo.long_name},()=>this.validtion())
            }
            if (
              locationInfo.types.indexOf('administrative_area_level_1') != -1
            ) {
              this.setState({
                city: this.state.city + ', ' + locationInfo.short_name,
              })
              // this.setState({isenableGps:false,currentLocation:this.state.currentLocation+', '+locationInfo.long_name},()=>this.validtion())
            }
            this.getCity()
          })
        }
      },
    )
  }

  getCity() {
    //alert('hiiiii')
    var date = new Date()
    this.offsetInHours = date.getTimezoneOffset()

    console.log('Time Offset: ' + this.offsetInHours * -1)

    var data = new FormData()
    data.append('city', this.state.city)
    data.append('login_id', this.state.login_id)
    data.append('timezone', this.offsetInHours * -1)

    fetch(API_ROOT + 'change_location', {
      method: 'post',
      body: data,
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
        } else {
        }
      })
      .catch((error) => {
        //alert('Oops your connection seems off, Check your connection and try again')
      })
  }
  getPeople = () => {
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    //this.setState({empty:true})

    console.log('GetPeople: ' + JSON.stringify(data))

    fetch(API_ROOT + 'people_may_you_know', {
      method: 'post',
      body: data,
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          //console.log('GetPeopleResponse: '+JSON.stringify(responseData))

          let arrPeoples = responseData.data

          if (arrPeoples.length >= 20) {
            arrPeoples.length = 20
          }

          this.setState({ Peoples: arrPeoples })
        } else {
          //console.log('GetPeopleResponse: '+JSON.stringify(responseData))
          //this.setState({empty:false})
        }
      })
      .catch((error) => {
        console.warn(error)
        //alert('Oops your connection seems off, Check your connection and try again')
        //this.setState({empty:false})
      })
  }

  getUserNotiCount() {
    //RNProgressHUB.showSpinIndeterminate()
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)

    //alert('NotiCount Request: '+ JSON.stringify(data))

    fetch(API_ROOT + 'get_noti_count', {
      method: 'post',
      body: data,
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          global.isfromlogin = false

          //alert('User Noti Counts: '+JSON.stringify(responseData))

          //alert('Global Counts: '+global.noti_count + ' New Count: '+responseData.noti_count)

          if (
            global.noti_count == null ||
            global.noti_count == 0 ||
            global.noti_count == NaN
          ) {
            global.noti_count = responseData.noti_count
          } else {
            global.noti_count = global.noti_count + responseData.noti_count
          }

          store.dispatch({ type: NOTIFICATION, data: global.noti_count })

          //RNProgressHUB.dismiss();
        } else {
          global.isfromlogin = false
          this.setState({ status: responseData.success })
          //RNProgressHUB.dismiss();
        }
      })
      .catch((error) => {
        //RNProgressHUB.dismiss();
        global.isfromlogin = false
        console.warn(error)
        //alert('Oops your connection seems off, Check your connection and try again')
      })
  }

  getNotiCountAPICall(dt) {
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('datetime', dt)

    //alert('NotiCount Request: '+ JSON.stringify(data))
    //RNProgressHUB.showSpinIndeterminate()
    fetch(API_ROOT + 'notification/unread_count', {
      method: 'post',
      body: data,
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          //alert('Noti Counts: '+JSON.stringify(responseData))

          //alert('Global Counts: '+global.noti_count + ' New Count: '+responseData.noti_count)

          if (
            global.noti_count == null ||
            global.noti_count == 0 ||
            global.noti_count == NaN
          ) {
            global.noti_count = responseData.noti_count
          } else {
            global.noti_count = global.noti_count + responseData.noti_count
          }

          store.dispatch({ type: NOTIFICATION, data: global.noti_count })

          //RNProgressHUB.dismiss();
        } else {
          this.setState({ status: responseData.success })
          //RNProgressHUB.dismiss();
        }
      })
      .catch((error) => {
        //RNProgressHUB.dismiss();
        //alert('Oops your connection seems off, Check your connection and try again')
        console.warn(error)
      })
  }

  updateNotiCount() {
    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', parseInt(this.state.login_id))
    data.append('noti_count', global.noti_count)

    //alert('NotiCount Request: '+ JSON.stringify(data))

    fetch(API_ROOT + 'add_noti_count', {
      method: 'post',
      body: data,
    })
      .then((response) => response.json())
      .then((responseData) => {
        //alert(JSON.stringify(responseData))
        if (responseData.success) {
          console.log('noti count updated')
        } else {
        }
      })
      .catch((error) => {
        //alert(error)
        //alert('Oops your connection seems off, Check your connection and try again')
      })
  }

  checkContactPermission() {
    check(PERMISSIONS.ANDROID.READ_CONTACTS)
      .then((result) => {
        switch (result) {
          case RESULTS.UNAVAILABLE:
            //alert('This feature is not available (on this device / in this context)');
            break
          case RESULTS.DENIED:
            //alert('The permission has not been requested / is denied but requestable');
            break
          case RESULTS.LIMITED:
            //alert('The permission is limited: some actions are possible');
            break
          case RESULTS.GRANTED:
            //alert('The permission is granted');
            break
          case RESULTS.BLOCKED:
            //alert('The permission is denied and not requestable anymore');
            break
        }
      })
      .catch((error) => {
        // …
      })
  }

  loadContacts() {
    subThis = this
    //alert('Hiiii')
    setTimeout(async function () {
      if (Platform.OS === 'android') {
        PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
          {
            title: 'Planmesh',
            message:
              'Planmesh syncs only phone numbers from your address book to Planmesh servers to help you connect with other Planmesh users.',
            buttonPositive: 'OK',
          },
        ).then((results) => {
          if (results === PermissionsAndroid.RESULTS.GRANTED) {
            subThis.fetchContacts()
          }
        })

        // const andoidContactPermission = await PermissionsAndroid.request(
        //   PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
        //   {
        //     'title': 'Contacts',
        //     'message': 'This app would like to view your contacts.'
        //   }
        // );
        // if (andoidContactPermission === PermissionsAndroid.RESULTS.GRANTED) {
        //   console.log("Contacts Permission granted");
        //   subThis.fetchContacts();
        // } else {
        //   console.log("Contacts permission denied");
        // }
      } else {
        subThis.fetchContacts()
      }
    }, 200)
  }

  fetchContacts() {
    Contacts.getAll().then((contacts) => {
      //alert(JSON.stringify(contacts))

      let newData = []
      contacts.map((contactData) => {
        if (contactData.givenName != null) {
          const words = contactData.givenName.split(' ')
          var initial = ''
          var seactionStr = ''
          if (words.length >= 1) {
            initial = words[0].charAt(0)
            seactionStr = words[0].charAt(0)
          }
          if (words.length >= 2) {
            initial = initial + words[1].charAt(0)
          }

          let dict = {
            key: contactData.rawContactId,
            select: false,
            title:
              contactData.givenName +
              ' ' +
              (contactData.familyName != null ? contactData.familyName : ''),
            initial: initial.toUpperCase(),
            sectionStr: seactionStr.toUpperCase(),
            phoneNumbers: contactData.phoneNumbers,
          }

          if (
            contactData.phoneNumbers != null &&
            contactData.phoneNumbers.length > 0
          ) {
            if (initial != '') {
              newData.push(dict)
            }
          }
        }
      })

      this.setState({ Contacts: newData })
      this.getPostList()
    })

    //alert('called!!')
    //console.log('fetchContacts')

    //RNProgressHUB.showSpinIndeterminate();
    /*
      Contacts.getAll((err, contacts) => {
          if (err === 'denied') {
              console.log('Permission to access contacts was denied');
              //RNProgressHUB.dismiss();
              this.getPostList()
          } else {
              //RNProgressHUB.dismiss();
              let newData = []          
              contacts.map((contactData) => {
    
                if (contactData.givenName != null) {
                    const words = contactData.givenName.split(' ')
                    var initial = ""
                    var seactionStr = ""
                    if (words.length >= 1) {
                        initial = words[0].charAt(0)
                        seactionStr = words[0].charAt(0)
                    }
                    if (words.length >= 2) {
                        initial = initial + words[1].charAt(0)
                    }
    
                    let dict = { key: contactData.rawContactId, select: false, title: ((contactData.givenName) + ' ' + (contactData.familyName != null ? contactData.familyName : '')), initial: initial.toUpperCase(), sectionStr: seactionStr.toUpperCase(), phoneNumbers: contactData.phoneNumbers }
                    
                    if(contactData.phoneNumbers != null && contactData.phoneNumbers.length > 0)
                    {  
                        if(initial != '')
                        {
                          newData.push(dict)
                        }
                        
                    }
                }
            });
              this.setState({Contacts:newData})
              this.getPostList()
          }
      })*/
  }

  getPostList = () => {
    console.log('getPostListCalled')

    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('limit', this.state.limit)
    data.append('offset', this.state.offset)
    data.append('type', '')
    //this.setState({ refreshing: false })
    //console.log('Request: '+JSON.stringify(data))
    //console.log('Current offset: '+ this.offset)

    fetch(API_ROOT + 'plan/list', {
      method: 'post',
      body: data,
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          this.is_post_loaded = true
          //console.log('POST LIST: '+responseData)
          AsyncStorage.setItem('overlay', 'false')
          this.setState({
            planList: responseData.data,
            refreshing: false,
            is_new_post_available: false,
          })
          this.arrayholder = responseData.data
        } else {
          //console.log('POST LIST ELSE: '+responseData)
          AsyncStorage.setItem('overlay', 'false')
          this.is_post_loaded = true
          this.setState({ planList: [], refreshing: false })
          if (responseData.text === 'Invalid token key') {
            alert(responseData.text)
            this.props.navigation.navigate('Login')
          }
          //this.setState({ refreshing: false })
        }
      })
      .catch((error) => {
        //console.log('POST LIST ERR: '+error)
        //alert('Oops your connection seems off, Check your connection and try again')
        this.setState({ refreshing: false })
      })
  }

  cancelFriendRequestAPICall(item, index) {
    var arrpeopls = this.state.Peoples
    var curitem = arrpeopls[index]
    curitem.is_friend = 0
    arrpeopls.splice(index, 1, curitem)

    this.setState({ Peoples: arrpeopls }, () => {
      var data = new FormData()
      data.append('token', this.state.token)
      data.append('login_id', this.state.login_id)
      data.append('other_id', item.user_id)

      fetch(API_ROOT + 'friend_request/cancel', {
        method: 'post',
        body: data,
      })
        .then((response) => response.json())
        .then((responseData) => {
          if (responseData.success) {
            //this.getPeople()
            //this.getPostList()
          } else {
            //alert(responseData.text)
          }
        })
        .catch((error) => {
          //alert('Oops your connection seems off, Check your connection and try again')
        })
    })
  }

  setUpTimer() {
    let timer = setInterval(() => this.checkForNewPost(), 1000 * 10)
    this.setState({ timer })
  }

  checkForNewPost() {
    //this.setState({is_new_post_available:true})

    console.log('checking....')

    if (
      this.state.planList.length > 0 &&
      this.state.is_new_post_available == false
    ) {
      var plandata = this.state.planList[0]
      var data = new FormData()
      data.append('token', this.state.token)
      data.append('login_id', this.state.login_id)
      data.append('plan_id', plandata.plan_id)

      console.log('New Post Request: ' + JSON.stringify(data))

      fetch(API_ROOT + 'new_plan_added/status', {
        method: 'post',
        body: data,
      })
        .then((response) => response.json())
        .then((responseData) => {
          if (responseData.success) {
            console.log('New Post Response: ' + JSON.stringify(responseData))

            if (this.active == false) {
              store.dispatch({ type: NEWPOST, data: 1 })
            }

            this.setState({ is_new_post_available: true })
          } else {
            this.setState({ is_new_post_available: false })
            //alert(responseData.text)
          }
        })
        .catch((error) => {
          this.setState({ is_new_post_available: false })
          //alert('Oops your connection seems off, Check your connection and try again')
          //alert(error)
        })
    }
  }

  addFriendAPICall(item, index) {
    console.log('item..', item)
    var arrpeopls = this.state.Peoples
    var curitem = arrpeopls[index]
    curitem.is_friend = 2
    arrpeopls.splice(index, 1, curitem)

    this.setState({ Peoples: arrpeopls }, () => {
      var data = new FormData()
      data.append('token', this.state.token)
      data.append('login_id', this.state.login_id)
      data.append('mobile_number', item.mobile_number)

      fetch(API_ROOT + 'friend_request', {
        method: 'post',
        body: data,
      })
        .then((response) => response.json())
        .then((responseData) => {
          if (responseData.success) {
            let array = []
            array.push(item.name)
            const MixpanelData = {
              $email: store.getState().auth.user.email_address,
              'Friends Selected': JSON.stringify(array),
              'Friend Requests Sent': array.length,
              'During Sign Up': 'False',
            }
            this.mixpanel.identify(store.getState().auth.user.email_address)
            this.mixpanel.track('Send Friend Requests', MixpanelData)
            console.log('Mixpanel data..', MixpanelData)
            //this.getPeople()
            //this.getPostList()
          } else {
            //alert(responseData.text)
          }
        })
        .catch((error) => {
          //alert('Oops your connection seems off, Check your connection and try again')
        })
    })
  }

  openModel = () => {
    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 300,
      easing: Easing.in(Easing.bezier(0, 0, 0, 1)),
      useNativeDriver: true,
    }).start()
    //this.setState({isOpen:!this.state.isOpen,temp:!this.state.temp})
    this.setState({ isOpen: true, temp: true, overlay: true })
  }
  colseModel = () => {
    Animated.timing(this.animatedValue, {
      toValue: 0,
      duration: 300,
      easing: Easing.in(Easing.bezier(0, 0, 0, 1)),
      useNativeDriver: true,
    }).start()
    this.setState({ isOpen: false, temp: false, overlay: false })
  }
  onLogoutClick = () => {
    store.dispatch({ type: LOGOUT })
    NavigationService.reset('Home')
  }

  handleFlatScroll = (event) => {
    //console.log('handleFlatScroll'+event.nativeEvent.contentOffset.y)
    this.flatlist_offset = event.nativeEvent.contentOffset.y
  }

  onEndReached = () => {
    //console.log('onEndReached: '+this.offset)

    if (this.flatlist_offset > height - 50) {
      //      console.log('onEndReached')
      this.offset = this.offset + this.state.limit
      let data = new FormData()
      data.append('token', this.state.token)
      data.append('login_id', this.state.login_id)
      data.append('limit', this.state.limit)
      data.append('offset', this.offset)
      data.append('type', '')
      fetch(API_ROOT + 'plan/list', {
        method: 'post',
        body: data,
      })
        .then((response) => response.json())

        .then((responseData) => {
          if (responseData.success) {
            console.log('hello', responseData.success)

            let oldData = this.state.planList
            let newData = oldData.concat(responseData.data)
            this.setState({ planList: newData, refreshing: false })
            this.arrayholder = responseData.data
            //console.log('MoreData: '+JSON.stringify(this.state.planList))
          } else {
            this.setState({ status: false })
          }
        })
        .catch((err) => {
          //alert('Oops your connection seems off, Check your connection and try again')
        })
    }

    /*
    
      */
  }

  addToCalendar = () => {
    this.setState(
      {
        isModalVisible: false,
        isMyDeleteModalVisible: false,
        isMyModalVisible: false,
      },
      () => {
        //let startdt = moment.utc(this.state.item.start_date).format('YYYY-MM-DDTHH:mm:ss.000Z')
        //let edndt = moment.utc(this.state.item.end_date).format('YYYY-MM-DDTHH:mm:ss.000Z')
        let startdt = moment
          .utc(this.state.currentitem.start_date)
          .toISOString()
        let edndt = moment.utc(this.state.currentitem.end_date).toISOString()
        RNCalendarEvents.saveEvent(this.eventtitle, {
          startDate: startdt,
          endDate: edndt,
          location:
            this.state.currentitem.line_1 +
            '\n\n' +
            this.state.currentitem.line_2,
          allDay: this.state.currentitem.is_allday == 1 ? true : false,
        })
      },
    )
    setTimeout(() => {
      Toast.show('Added to your calendar.')
    }, 1000)
  }

  deleteplan = () => {
    var plandata = this.state.planList
    plandata.splice(this.currentitemindex, 1)
    this.setState(
      {
        planList: plandata,
        isMyModalVisible: false,
        isMyDeleteModalVisible: false,
      },

      () => {
        var data = new FormData()
        data.append('token', this.state.token)
        data.append('plan_id', this.state.plan_id)
        data.append('login_id', this.state.login_id)

        fetch(API_ROOT + 'delete/plan', {
          method: 'post',
          body: data,
        })
          .then((response) => response.json())
          .then((responseData) => {
            if (responseData.success) {
              const MixpanleData = {
                $email: store.getState().auth.user.email_address,
                'Post ID': this.state.plan_id,
              }
              this.mixpanel.identify(store.getState().auth.user.email_address)
              this.mixpanel.track('Post Cancel', MixpanleData)
              //this.setState({ isMyModalVisible: false, isMyDeleteModalVisible: false })
              //this.onRefresh()
            } else {
              alert(responseData.text)
            }
          })
          .catch((error) => {
            //alert('Oops your connection seems off, Check your connection and try again')
          })
      },
    )
  }
  gotoUnFriend = () => {
    this.setState({ isModalVisible: false })

    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('other_id', this.state.other_id)
    fetch(API_ROOT + 'friend_unfriend', {
      method: 'post',
      body: data,
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          this.onRefresh()
        } else {
          alert(responseData.text)
        }
      })
      .catch((error) => {
        //alert('Oops your connection seems off, Check your connection and try again')
      })
  }
  gotoBlock = () => {
    this.setState({ isModalVisible: false })

    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('other_id', this.state.other_id)
    data.append('is_block', 1)

    fetch(API_ROOT + 'block/user', {
      method: 'post',
      body: data,
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          this.onRefresh()
        } else {
          alert(responseData.text)
        }
      })
      .catch((error) => {
        //alert('Oops your connection seems off, Check your connection and try again')
      })
  }

  gotoHide = () => {
    var plandata = this.state.planList
    plandata.splice(this.currentitemindex, 1)
    this.setState({ planList: plandata, isModalVisible: false }, () => {
      var data = new FormData()
      data.append('token', this.state.token)
      data.append('plan_id', this.state.plan_id)
      data.append('login_id', this.state.login_id)
      data.append('is_hide', 1)

      fetch(API_ROOT + 'hide/plan', {
        method: 'post',
        body: data,
      })
        .then((response) => response.json())
        .then((responseData) => {
          if (responseData.success) {
            // this.setState({ hide: !this.state.hide })
            //this.onRefresh()
          } else {
            alert(responseData.text)
          }
        })
        .catch((error) => {
          //alert('Oops your connection seems off, Check your connection and try again')
        })
    })
  }
  gotoreportUser = () => {
    this.setState({ isModalVisible: false })
    this.props.navigation.navigate('report', { plan_id: this.state.plan_id })
    // var data = new FormData()
    // data.append('token', this.state.token)
    // data.append('plan_id', this.state.plan_id)
    // data.append('login_id', this.state.login_id)
    // data.append('is_interested', 1)
    // fetch(API_ROOT + 'plan/report', {
    //   method: 'post', body: data
    // })
    //   .then((response) => response.json())
    //   .then((responseData) => {
    //     if (responseData.success) {
    //       alert(responseData.text)

    //     } else {
    //       alert(responseData.text)
    //     }
    //   })
    //   .catch((error) => { alert(error) })
  }

  onMuteAction(status) {
    var arrplans = this.state.planList
    var curitem = arrplans[this.currentitemindex]
    if (curitem.is_mute == 1) {
      curitem.is_mute = 0
    } else {
      curitem.is_mute = 1
    }

    arrplans.splice(this.currentitemindex, 1, curitem)

    this.setState({ planList: arrplans, isModalVisible: true }, () => {
      console.log('Mute Status: ' + status)

      var data = new FormData()
      data.append('token', this.state.token)
      data.append('plan_id', this.state.plan_id)
      data.append('login_id', this.state.login_id)
      data.append('status', status == true ? 1 : 0)

      fetch(API_ROOT + 'plan/mute_unmute', {
        method: 'post',
        body: data,
      })
        .then((response) => response.json())
        .then((responseData) => {
          if (responseData.success) {
            // this.setState({ hide: !this.state.hide })
            //this.onRefresh()
          } else {
            //alert(responseData.text)
          }
        })
        .catch((error) => {
          //alert('Oops your connection seems off, Check your connection and try again')
        })
    })
  }

  goToIntersted = (item, index) => {
    var arrplans = this.state.planList
    var curitem = arrplans[index]
    curitem.is_interested_count = curitem.is_interested_count + 1
    if (curitem.is_going == 1) {
      curitem.is_going_count = curitem.is_going_count - 1
    }
    curitem.is_interested = 1
    curitem.is_going = 0
    arrplans.splice(index, 1, curitem)

    this.setState({ planList: arrplans }, () => {
      var data = new FormData()
      data.append('token', this.state.token)
      data.append('plan_id', item.plan_id)
      data.append('login_id', this.state.login_id)
      data.append('is_interested', 1)
      fetch(API_ROOT + 'plan/interested_response', {
        method: 'post',
        body: data,
      })
        .then((response) => response.json())
        .then((responseData) => {
          if (responseData.success) {
            //this.onRefresh()
            const MixpanelData = {
              $email: store.getState().auth.user.email_address,
              'Post ID': item.plan_id,
              Interested: 'True',
            }
            this.mixpanel.identify(store.getState().auth.user.email_address)
            this.mixpanel.track('Interested', MixpanelData)
          } else {
            //alert(responseData.text)
          }
        })
        .catch((error) => {
          //alert('Oops your connection seems off, Check your connection and try again')
        })
    })
  }

  goToNoIntersted = (item, index) => {
    var arrplans = this.state.planList
    var curitem = arrplans[index]
    curitem.is_interested = 0
    curitem.is_interested_count > 0
      ? (curitem.is_interested_count = curitem.is_interested_count - 1)
      : 0
    arrplans.splice(index, 1, curitem)

    this.setState({ planList: arrplans }, () => {
      var data = new FormData()
      data.append('token', this.state.token)
      data.append('plan_id', item.plan_id)
      data.append('login_id', this.state.login_id)
      data.append('is_interested', 2)
      fetch(API_ROOT + 'plan/interested_response', {
        method: 'post',
        body: data,
      })
        .then((response) => response.json())
        .then((responseData) => {
          if (responseData.success) {
            //this.onRefresh()
            const MixpanelData = {
              $email: store.getState().auth.user.email_address,
              'Post ID': item.plan_id,
              Interested: 'False',
            }
            this.mixpanel.identify(store.getState().auth.user.email_address)
            this.mixpanel.track('Interested', MixpanelData)
          } else {
            //alert(responseData.text)
          }
        })
        .catch((error) => {
          //alert('Oops your connection seems off, Check your connection and try again')
        })
    })
  }
  goToGoing = (item, index) => {
    var arrplans = this.state.planList
    var curitem = arrplans[index]
    if (curitem.is_interested == 1) {
      curitem.is_interested_count = curitem.is_interested_count - 1
    }
    curitem.is_going_count = curitem.is_going_count + 1
    curitem.is_interested = 0
    curitem.is_going = 1

    arrplans.splice(index, 1, curitem)
    this.setState({ planList: arrplans }, () => {
      var data = new FormData()
      data.append('token', this.state.token)
      data.append('plan_id', item.plan_id)
      data.append('login_id', this.state.login_id)
      data.append('is_going', 1)

      //alert(JSON.stringify(data))
      //return
      fetch(API_ROOT + 'plan/going_response', {
        method: 'post',
        body: data,
      })
        .then((response) => response.json())
        .then((responseData) => {
          if (responseData.success) {
            //this.onRefresh()
            const MixpanelData = {
              $email: store.getState().auth.user.email_address,
              'Post ID': item.plan_id,
              Going: 'True',
            }
            this.mixpanel.identify(store.getState().auth.user.email_address)
            this.mixpanel.track('Going', MixpanelData)
          } else {
            //alert(responseData.text)
          }
        })
        .catch((error) => {
          //alert('Oops your connection seems off, Check your connection and try again')
        })
    })
  }

  goToNoGoing = (item, index) => {
    var arrplans = this.state.planList
    var curitem = arrplans[index]
    curitem.is_going_count > 0
      ? (curitem.is_going_count = curitem.is_going_count - 1)
      : 0
    curitem.is_going = 0
    arrplans.splice(index, 1, curitem)
    this.setState({ planList: arrplans }, () => {
      var data = new FormData()
      data.append('token', this.state.token)
      data.append('plan_id', item.plan_id)
      data.append('login_id', this.state.login_id)
      data.append('is_going', 2)

      // console.log('user...', store.getState().auth.user)
      console.log('')
      fetch(API_ROOT + 'plan/going_response', {
        method: 'post',
        body: data,
      })
        .then((response) => response.json())
        .then((responseData) => {
          if (responseData.success) {
            //this.onRefresh()
            const MixpanelData = {
              $email: store.getState().auth.user.email_address,
              'Post ID': item.plan_id,
              Going: 'False',
            }
            this.mixpanel.identify(store.getState().auth.user.email_address)
            this.mixpanel.track('Going', MixpanelData)
          } else {
            //alert(responseData.text)
          }
        })
        .catch((error) => {
          //alert('Oops your connection seems off, Check your connection and try again')
        })
    })
  }

  async goToShare(item) {
    // Share.open({ url: 'Hey,join Planmesh app to share your pics',message:});
    const MixpanelData = {
      $email: store.getState().auth.user.email_address,
      'Post ID': item.plan_id,
    }
    this.mixpanel.identify(store.getState().auth.user.email_address)
    this.mixpanel.track('Share', MixpanelData)

    if (item.plan_type == 1) {
      var name = item.friend_name.split(' ')
      //message = 'Hey, '+"i'm inviting you to "+ name[0]+", let's meet up!" + '\n'+ "Let me know on Planmesh if you're in" + '\n\n' +'https://planmeshapp.com/plan_details/'+item.plan_id
      message =
        'Hey, ' +
        "i'm inviting you to " +
        ", Let's meet up!" +
        '\n' +
        "Let me know on Planmesh if you're in" +
        '\n\n' +
        'https://planmeshapp.com/plan_details/' +
        item.plan_id
    } else if (item.plan_type == 2) {
      message =
        'Hey, ' +
        "i'm inviting you to " +
        item.activity +
        " Let me know on Planmesh if you're in" +
        '\n\n' +
        'https://planmeshapp.com/plan_details/' +
        item.plan_id
    } else if (item.plan_type == 3) {
      message =
        'Hey, ' +
        "i'm inviting you to " +
        'Wanna go to ' +
        item.line_1 +
        '?' +
        " Let me know on Planmesh if you're in" +
        '\n\n' +
        'https://planmeshapp.com/plan_details/' +
        item.plan_id
    } else if (item.plan_type == 4) {
      message =
        'Hey, ' +
        "i'm inviting you to " +
        "I'm free, wanna hangout?" +
        " Let me know on Planmesh if you're in" +
        '\n\n' +
        'https://planmeshapp.com/plan_details/' +
        item.plan_id
    }

    try {
      const result = await Share.share({
        message: message,
      })

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message)
    }
  }

  inviteContacts(contactnumber) {
    //alert('hiiii')

    if (contactnumber != '' && contactnumber != null) {
      const MixpanelData = {
        'Invites Sent': 1,
      }
      this.mixpanel.identify(store.getState().auth.user.email_address)
      this.mixpanel.people.increment('Total Invites Sent', 1)
      this.mixpanel.track('Invite Friends', MixpanelData)
      SendSMS.send(
        {
          //body: `Hey! I'm inviting you to Planmesh. Use my referral code E6C0AJ to register and own a piece of Planmesh. Download Now!`+this.state.referral_url,
          body:
            'Hey! Check out Planmesh. Your space to make plans with the people you care about. Get it free at \n iOS: https://apple.co/3r9BwWO \n Android: http://bit.ly/2Xx7zT7',
          recipients: [contactnumber],
          successTypes: ['sent', 'queued'],
          allowAndroidSendWithoutReadPermission: true,
        },
        (completed, cancelled, error) => {
          if (completed) {
            Toast.show('Invitation send successfully')
            //NavigationService.reset('dashboard')
          } else if (cancelled) {
            //Toast.show("Invite send successfully")
          } else {
            //alert(error)
          }
        },
      )
    }
  }

  modelVisible = (item, index) => {
    //alert(JSON.stringify(item))

    this.currentitemindex = index

    var name = item.friend_name.split(' ')

    if (item.plan_type == 1) {
      //this.eventtitle = name[0]+", let's meet up!"
      this.eventtitle = "Let's meet up!"
    } else if (item.plan_type == 2) {
      this.eventtitle = item.activity
    } else if (item.plan_type == 3) {
      this.eventtitle = 'Wanna go to ' + item.line_1 + '?'
    } else if (item.plan_type == 4) {
      this.eventtitle = "I'm free, wanna hangout?"
    }

    if (item.login_id == this.state.login_id) {
      if (item.plan_type == 1) {
        this.setState({
          isMyDeleteModalVisible: true,
          post_name: item.name,
          plan_id: item.plan_id,
          other_id: item.login_id,
          currentitem: item,
          is_muted: item.is_mute == 1 ? true : false,
        })
      } else {
        this.setState({
          isMyModalVisible: true,
          post_name: item.name,
          plan_id: item.plan_id,
          other_id: item.login_id,
          currentitem: item,
          is_muted: item.is_mute == 1 ? true : false,
        })
      }
    } else {
      this.setState({
        isModalVisible: true,
        post_name: item.name,
        plan_id: item.plan_id,
        other_id: item.login_id,
        currentitem: item,
        is_muted: item.is_mute == 1 ? true : false,
      })
    }
  }

  gotoEditEvent(item) {
    this.setState({ currentitem: item }, () => {
      if (item.plan_type == 1) {
        if (item.is_invited_list.length > 0) {
          var arrfriends = item.is_invited_list
          var nameIDs = []
          var count = arrfriends.length - 1
          var name = ''

          if (arrfriends.length > 0) {
            name = arrfriends[0].receiver_name
            arrfriends.map((item) => {
              nameIDs.push(item.receiver_id)
            })

            //AsyncStorage.setItem('receiver_id',nameIDs[0])
            AsyncStorage.setItem('receiver_id', String(nameIDs))
            AsyncStorage.setItem('Activity', item.activity)
            AsyncStorage.setItem('Location', item.line_1)
            AsyncStorage.setItem('who_desc', item.description)
            this.setDateValue(item.start_date, item.end_date)

            var invitedusername = ''

            if (arrfriends.length > 1) {
              invitedusername =
                name + ' and ' + (arrfriends.length - 1) + ' More'
            } else {
              invitedusername = name
            }
            this.props.navigation.navigate('Who2', {
              friendName: invitedusername,
              is_edit_plan: true,
              item: item,
              invitedList: item.is_invited_list,
            })
          }
        }
      } else if (item.plan_type == 2) {
        var arrfriends = item.is_invited_list
        var nameIDs = []
        var count = arrfriends.length - 1
        var name = ''
        var contacts = item.contacts

        if (arrfriends.length > 0) {
          name = arrfriends[0].receiver_name
          arrfriends.map((item) => {
            nameIDs.push(item.receiver_id)
          })
        }
        AsyncStorage.setItem('what_friends', JSON.stringify(nameIDs))
        AsyncStorage.setItem('what_contacts', JSON.stringify(contacts))

        if (arrfriends.length == 0) {
          AsyncStorage.setItem('what_friends_name', '')
        } else if (arrfriends.length == 1) {
          AsyncStorage.setItem('what_friends_name', JSON.stringify(name))
        } else {
          AsyncStorage.setItem(
            'what_friends_name',
            JSON.stringify(name + ' and ' + count + ' more'),
          )
        }

        /*  if(arrfriends.length == 0)
            {
              if(contacts.length == 0)
              {
                AsyncStorage.setItem('what_friends_name', '')
              }
              else
              {
                var cnt = contacts.length
                if(contacts.length == 1)
                {
                  AsyncStorage.setItem('what_friends_name', JSON.stringify(contacts))
                }
                else
                {
                  AsyncStorage.setItem('what_friends_name', JSON.stringify(contacts[0] + ' and ' + (cnt - 1) + ' more'))
                }
              }
              
            }
            else if (arrfriends.length == 1) {
                if(contacts.length > 0)
                {
                  var cnt = contacts.length
                  AsyncStorage.setItem('what_friends_name', JSON.stringify(name + ' and ' + cnt + ' more'))
                }
                else
                {
                  AsyncStorage.setItem('what_friends_name', JSON.stringify(name))
                }
            } else {
                if(contacts.length > 0)
                {
                  var cnt = contacts.length
                  count = count + cnt
                  AsyncStorage.setItem('what_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
                }
                else
                {
                  AsyncStorage.setItem('what_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
                }
            }*/

        AsyncStorage.setItem('What_Activity', item.activity)
        AsyncStorage.setItem('what_Location', item.line_1) //what_FullAddress
        AsyncStorage.setItem('what_FullAddress', item.line_2)
        AsyncStorage.setItem('what_desc', item.description)
        this.setDateValueForWhat(item.start_date, item.end_date)
        this.props.navigation.navigate('Frinds', {
          is_edit_plan: true,
          item: item,
        })
      } else if (item.plan_type == 3) {
        var arrfriends = item.is_invited_list
        var nameIDs = []
        var count = arrfriends.length - 1
        var name = ''
        var contacts = item.contacts

        if (arrfriends.length > 0) {
          name = arrfriends[0].receiver_name
          arrfriends.map((item) => {
            nameIDs.push(item.receiver_id)
          })
        }
        AsyncStorage.setItem('where_friends', JSON.stringify(nameIDs))
        AsyncStorage.setItem('where_contacts', JSON.stringify(contacts))

        if (arrfriends.length == 0) {
          AsyncStorage.setItem('where_friends_name', '')
        } else if (arrfriends.length == 1) {
          AsyncStorage.setItem('where_friends_name', JSON.stringify(name))
        } else {
          AsyncStorage.setItem(
            'where_friends_name',
            JSON.stringify(name + ' and ' + count + ' more'),
          )
        }

        /* if(arrfriends.length == 0)
           {
             //AsyncStorage.setItem('where_friends_name', '')
             if(contacts.length == 0)
             {
               AsyncStorage.setItem('where_friends_name', '')
             }
             else
             {
               var cnt = contacts.length
               if(contacts.length == 1)
               {
                 AsyncStorage.setItem('where_friends_name', JSON.stringify(contacts))
               }
               else
               {
                 AsyncStorage.setItem('where_friends_name', JSON.stringify(contacts[0] + ' and ' + (cnt - 1) + ' more'))
               }
             }
           }
           else if (arrfriends.length == 1) {
               //AsyncStorage.setItem('where_friends_name', JSON.stringify(name))
               if(contacts.length > 0)
               {
                 var cnt = contacts.length
                 AsyncStorage.setItem('where_friends_name', JSON.stringify(name + ' and ' + cnt + ' more'))
               }
               else
               {
                 AsyncStorage.setItem('where_friends_name', JSON.stringify(name))
               }
   
           } else {
              // AsyncStorage.setItem('where_friends_name', JSON.stringify(name + ' and ' + count + ' more'))

              if(contacts.length > 0)
               {
                 var cnt = contacts.length
                 count = count + cnt
                 AsyncStorage.setItem('where_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
               }
               else
               {
                 AsyncStorage.setItem('where_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
               }
   
           }*/

        AsyncStorage.setItem('where_Location', item.line_1)
        AsyncStorage.setItem('where_FullAddress', item.line_2)
        AsyncStorage.setItem('where_Activity', item.activity)
        AsyncStorage.setItem('where_desc', item.description)
        this.setDateValueForWhere(item.start_date, item.end_date)
        this.props.navigation.navigate('where2', {
          is_edit_plan: true,
          item: item,
        })
      } else if (item.plan_type == 4) {
        var arrfriends = item.is_invited_list
        var nameIDs = []

        var count = arrfriends.length - 1
        var name = ''
        var contacts = item.contacts

        if (arrfriends.length > 0) {
          name = arrfriends[0].receiver_name
          arrfriends.map((item) => {
            nameIDs.push(item.receiver_id)
          })
        }

        //console.log('WhenActivity: '+)

        AsyncStorage.setItem('when_friends', JSON.stringify(nameIDs))
        AsyncStorage.setItem('when_contacts', JSON.stringify(contacts))

        if (arrfriends.length == 0) {
          AsyncStorage.setItem('when_friends_name', '')
        } else if (arrfriends.length == 1) {
          AsyncStorage.setItem('when_friends_name', JSON.stringify(name))
        } else {
          AsyncStorage.setItem(
            'when_friends_name',
            JSON.stringify(name + ' and ' + count + ' more'),
          )
        }

        /* if(arrfriends.length == 0)
           {
             //AsyncStorage.setItem('when_friends_name', '')
             if(contacts.length == 0)
             {
               AsyncStorage.setItem('when_friends_name', '')
             }
             else
             {
               var cnt = contacts.length
               if(contacts.length == 1)
               {
                 AsyncStorage.setItem('when_friends_name', JSON.stringify(contacts))
               }
               else
               {
                 AsyncStorage.setItem('when_friends_name', JSON.stringify(contacts[0] + ' and ' + (cnt - 1) + ' more'))
               }
             }
           }
            else if (arrfriends.length == 1) {
               //AsyncStorage.setItem('when_friends_name', JSON.stringify(name))
               if(contacts.length > 0)
               {
                 var cnt = contacts.length
                 AsyncStorage.setItem('when_friends_name', JSON.stringify(name + ' and ' + cnt + ' more'))
               }
               else
               {
                 AsyncStorage.setItem('when_friends_name', JSON.stringify(name))
               }
           } else {
               //AsyncStorage.setItem('when_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
               if(contacts.length > 0)
               {
                 var cnt = contacts.length
                 count = count + cnt
                 AsyncStorage.setItem('when_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
               }
               else
               {
                 AsyncStorage.setItem('when_friends_name', JSON.stringify(name + ' and ' + count + ' more'))
               }
           }*/

        AsyncStorage.setItem('when_Location', item.line_1)
        AsyncStorage.setItem('when_FullAddress', item.line_2)
        AsyncStorage.setItem('when_Activity', item.activity)
        AsyncStorage.setItem('when_desc', item.description)
        this.setDateValueForWhen(item.start_date, item.end_date)
        this.props.navigation.navigate('when2', {
          is_edit_plan: true,
          item: item,
        })
      }
    })
  }

  gotoEventDetail() {
    this.gotoEditEvent(this.state.currentitem)
    this.setState({ isMyModalVisible: false, isMyDeleteModalVisible: false })

    //alert(JSON.stringify(this.state.currentitem))

    // if(this.state.currentitem.plan_type == 1)
    // {
    //     AsyncStorage.setItem('receiver_id',this.state.currentitem.receiver_id)
    //     AsyncStorage.setItem('Activity',this.state.currentitem.activity)
    //     AsyncStorage.setItem('Location',this.state.currentitem.line_1)
    //     this.setDateValue(this.state.currentitem.start_date,this.state.currentitem.end_date)
    //     var invitedusername = this.state.currentitem.friend_name
    //     this.props.navigation.navigate('Who2',{friendName:invitedusername,is_edit_plan:true, item:this.state.currentitem})
    // }
    // else if(this.state.currentitem.plan_type == 2)
    // {

    //   var arrfriends = this.state.currentitem.is_invited_list
    //   var nameIDs = []
    //   var count = arrfriends.length - 1
    //   var name = ''

    //   if(arrfriends.length > 0)
    //   {
    //     name = arrfriends[0].receiver_name
    //     arrfriends.map((item) => {

    //       nameIDs.push(item.receiver_id)

    //     });

    //   }
    //     AsyncStorage.setItem('what_friends', JSON.stringify(nameIDs))

    //     if(arrfriends.length == 0)
    //     {
    //       AsyncStorage.setItem('what_friends_name', '')
    //     }
    //     else if (arrfriends.length == 1) {
    //         AsyncStorage.setItem('what_friends_name', JSON.stringify(name))

    //     } else {
    //         AsyncStorage.setItem('what_friends_name', JSON.stringify(name + ' and ' + count + ' more'))

    //     }

    //   AsyncStorage.setItem('What_Activity',this.state.currentitem.activity)
    //   AsyncStorage.setItem('what_Location',this.state.currentitem.line_1)//what_FullAddress
    //   AsyncStorage.setItem('what_FullAddress',this.state.currentitem.line_2)
    //   this.setDateValueForWhat(this.state.currentitem.start_date,this.state.currentitem.end_date)
    //   this.props.navigation.navigate('Frinds',{is_edit_plan:true, item:this.state.currentitem})
    // }
    // else if(this.state.currentitem.plan_type == 3)
    // {

    //   var arrfriends = this.state.currentitem.is_invited_list
    //   var nameIDs = []
    //   var count = arrfriends.length - 1
    //   var name = ''

    //   if(arrfriends.length > 0)
    //   {
    //     name = arrfriends[0].receiver_name
    //     arrfriends.map((item) => {

    //       nameIDs.push(item.receiver_id)

    //     });

    //   }
    //     AsyncStorage.setItem('where_friends', JSON.stringify(nameIDs))

    //     if(arrfriends.length == 0)
    //     {
    //       AsyncStorage.setItem('where_friends_name', '')
    //     }
    //     else if (arrfriends.length == 1) {
    //         AsyncStorage.setItem('where_friends_name', JSON.stringify(name))

    //     } else {
    //         AsyncStorage.setItem('where_friends_name', JSON.stringify(name + ' and ' + count + ' more'))

    //     }

    //     AsyncStorage.setItem('where_Location',this.state.currentitem.line_1)
    //     AsyncStorage.setItem('where_FullAddress',this.state.currentitem.line_2)
    //     this.setDateValueForWhere(this.state.currentitem.start_date,this.state.currentitem.end_date)
    //     this.props.navigation.navigate('where2', {is_edit_plan:true, item:this.state.currentitem})
    // }
    // else if(this.state.currentitem.plan_type == 4)
    // {

    //   var arrfriends = this.state.currentitem.is_invited_list
    //   var nameIDs = []

    //   var count = arrfriends.length - 1
    //   var name = ''

    //   if(arrfriends.length > 0)
    //   {
    //     name = arrfriends[0].receiver_name
    //     arrfriends.map((item) => {

    //       nameIDs.push(item.receiver_id)

    //     });

    //   }
    //     AsyncStorage.setItem('when_friends', JSON.stringify(nameIDs))

    //     if(arrfriends.length == 0)
    //     {
    //       AsyncStorage.setItem('when_friends_name', '')
    //     }
    //      else if (arrfriends.length == 1) {
    //         AsyncStorage.setItem('when_friends_name', JSON.stringify(name))
    //     } else {
    //         AsyncStorage.setItem('when_friends_name', JSON.stringify(name + ' and ' + count + ' more'))

    //     }

    //   this.setDateValueForWhen(this.state.currentitem.start_date,this.state.currentitem.end_date)
    //   this.props.navigation.navigate('when2',{is_edit_plan:true, item:this.state.currentitem})
    // }
  }
  setDateValue(sdate, edate) {
    var currentDate = moment().format('ll')
    let tomorrow = moment().add(1, 'days').format('ll')
    var date = moment(edate).format('ll')

    if (sdate != '') {
      var gmtStartDateTime = moment.utc(sdate)
      var startdate = gmtStartDateTime.local()

      var gmtEndDateTime = moment.utc(edate)
      var enddate = gmtEndDateTime.local()

      // if (this.state.currentitem.is_now) {
      //   if (date == currentDate) {
      //     AsyncStorage.setItem('CurrentDate', 'Now - ' + moment(enddate).format('LT'))
      //   } else {
      //     if (date == tomorrow) {
      //       AsyncStorage.setItem('CurrentDate', 'Now - Tomorrow · ' + moment(enddate).format('LT'))

      //     } else {
      //       AsyncStorage.setItem('CurrentDate', 'Now - ' + moment(date).format('ddd, MMM D') + ' · ' + moment(enddate).format('LT'))
      //     }
      //   }
      //   AsyncStorage.setItem('start', currentDate)
      //   AsyncStorage.setItem('end', moment(enddate).format('lll'))
      //   AsyncStorage.setItem('is_now', '1')
      //   AsyncStorage.setItem('is_allday', '0')
      // }
      // else
      if (this.state.currentitem.is_allday == 1) {
        if (
          currentDate == moment(startdate).format('ll') &&
          currentDate == moment(enddate).format('ll')
        ) {
          AsyncStorage.setItem('CurrentDate', 'Today · All Day')
        } else {
          if (
            currentDate == moment(startdate).format('ll') &&
            tomorrow == moment(enddate).format('ll')
          ) {
            AsyncStorage.setItem('CurrentDate', 'Today - Tomorrow · All Day')
          } else {
            if (
              tomorrow == moment(startdate).format('ll') &&
              tomorrow == moment(enddate).format('ll')
            ) {
              AsyncStorage.setItem('CurrentDate', 'Tomorrow · All Day')
            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                AsyncStorage.setItem(
                  'CurrentDate',
                  'Tomorrow - ' +
                    moment(enddate).format('ddd, MMM D') +
                    ' · All Day',
                )
              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  AsyncStorage.setItem(
                    'CurrentDate',
                    'Today - ' +
                      moment(enddate).format('ddd, MMM D') +
                      ' · All Day',
                  )
                } else {
                  if (
                    moment(startdate).format('ll') ==
                    moment(enddate).format('ll')
                  ) {
                    AsyncStorage.setItem(
                      'CurrentDate',
                      'All Day - ' + moment(enddate).format('ddd, MMM D'),
                    )
                  } else {
                    AsyncStorage.setItem(
                      'CurrentDate',
                      moment(startdate).format('ddd, MMM D') +
                        ' - ' +
                        moment(enddate).format('ddd, MMM D') +
                        ' · ALL DAY',
                    )
                  }
                }
              }
            }
          }
        }
        AsyncStorage.setItem('start', moment(startdate).format('ll'))
        AsyncStorage.setItem('end', moment(enddate).format('ll'))
        AsyncStorage.setItem('is_now', '0')
        AsyncStorage.setItem('is_allday', '1')
      } else {
        if (
          currentDate == moment(startdate).format('ll') &&
          currentDate == moment(enddate).format('ll')
        ) {
          AsyncStorage.setItem(
            'CurrentDate',
            'Today \u00B7 ' +
              moment(startdate).format('LT') +
              ' - ' +
              moment(enddate).format('LT'),
          )
        } else {
          if (
            tomorrow == moment(startdate).format('ll') &&
            tomorrow == moment(enddate).format('ll')
          ) {
            AsyncStorage.setItem(
              'CurrentDate',
              'Tomorrow \u00B7 ' +
                moment(startdate).format('LT') +
                ' - ' +
                moment(enddate).format('LT'),
            )
          } else {
            if (
              currentDate == moment(startdate).format('ll') &&
              tomorrow == moment(enddate).format('ll')
            ) {
              AsyncStorage.setItem(
                'CurrentDate',
                'Today \u00B7 ' +
                  moment(startdate).format('LT') +
                  ' - Tomorrow \u00B7 ' +
                  moment(enddate).format('LT'),
              )
            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                AsyncStorage.setItem(
                  'CurrentDate',
                  ' Tomorrow - ' +
                    moment(enddate).format('ddd, MMM D \u00B7 LT'),
                )
              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  if (currentDate == moment(enddate).format('ll')) {
                    AsyncStorage.setItem('CurrentDate', 'Tomorrow · All Day')
                  } else {
                    AsyncStorage.setItem(
                      'CurrentDate',
                      'Today - ' +
                        moment(enddate).format('ddd, MMM D \u00B7 LT'),
                    )
                  }
                } else {
                  AsyncStorage.setItem(
                    'CurrentDate',
                    moment(startdate).format('ddd, MMM D \u00B7 LT') +
                      ' to ' +
                      moment(enddate).format('ddd, MMM D \u00B7 LT'),
                  )
                }
              }
            }
          }
        }
        AsyncStorage.setItem('start', moment(startdate).format('lll'))
        AsyncStorage.setItem('end', moment(enddate).format('lll'))
        AsyncStorage.setItem('is_now', '0')
        AsyncStorage.setItem('is_allday', '0')
      }
    } else {
      AsyncStorage.setItem('start', '')
      AsyncStorage.setItem('end', '')
      AsyncStorage.setItem('CurrentDate', '')
    }
  }

  setDateValueForWhat(sdate, edate) {
    var currentDate = moment().format('ll')
    let tomorrow = moment().add(1, 'days').format('ll')
    var date = moment(edate).format('ll')

    if (sdate != '') {
      var gmtStartDateTime = moment.utc(sdate)
      var startdate = gmtStartDateTime.local()

      var gmtEndDateTime = moment.utc(edate)
      var enddate = gmtEndDateTime.local()

      //     if (this.state.currentitem.is_now) {
      //       if (date == currentDate) {
      //         AsyncStorage.setItem('what_CurrentDate', 'Now - ' + moment(enddate).format('LT'))
      //       } else {
      //         if (date == tomorrow) {
      //           AsyncStorage.setItem('what_CurrentDate', 'Now - Tomorrow · ' + moment(enddate).format('LT'))

      //         } else {
      //           AsyncStorage.setItem('what_CurrentDate', 'Now - ' + moment(date).format('ddd, MMM D') + ' · ' + moment(enddate).format('LT'))

      //         }

      //       }

      //       AsyncStorage.setItem('what_start', currentDate)
      //       AsyncStorage.setItem('what_end', moment(enddate).format('lll'))
      //       AsyncStorage.setItem('what_is_now', '1')
      //       AsyncStorage.setItem('what_is_allday', '0')
      // }
      // else
      if (this.state.currentitem.is_allday == 1) {
        if (
          currentDate == moment(startdate).format('ll') &&
          currentDate == moment(enddate).format('ll')
        ) {
          AsyncStorage.setItem('what_CurrentDate', 'Today · All Day')
        } else {
          if (
            currentDate == moment(startdate).format('ll') &&
            tomorrow == moment(enddate).format('ll')
          ) {
            AsyncStorage.setItem(
              'what_CurrentDate',
              'Today - Tomorrow · All Day',
            )
          } else {
            if (
              tomorrow == moment(startdate).format('ll') &&
              tomorrow == moment(enddate).format('ll')
            ) {
              AsyncStorage.setItem('what_CurrentDate', 'Tomorrow · All Day')
            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                AsyncStorage.setItem(
                  'what_CurrentDate',
                  'Tomorrow - ' +
                    moment(enddate).format('ddd, MMM D') +
                    ' · All Day',
                )
              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  AsyncStorage.setItem(
                    'what_CurrentDate',
                    'Today - ' +
                      moment(enddate).format('ddd, MMM D') +
                      ' · All Day',
                  )
                } else {
                  if (
                    moment(startdate).format('ll') ==
                    moment(enddate).format('ll')
                  ) {
                    AsyncStorage.setItem(
                      'what_CurrentDate',
                      'All Day - ' + moment(enddate).format('ddd, MMM D'),
                    )
                  } else {
                    AsyncStorage.setItem(
                      'what_CurrentDate',
                      moment(startdate).format('ddd, MMM D') +
                        ' - ' +
                        moment(enddate).format('ddd, MMM D') +
                        ' · ALL DAY',
                    )
                  }
                }
              }
            }
          }
        }
        AsyncStorage.setItem('what_start', moment(startdate).format('ll'))
        AsyncStorage.setItem('what_end', moment(enddate).format('ll'))
        AsyncStorage.setItem('what_is_now', '0')
        AsyncStorage.setItem('what_is_allday', '1')
      } else {
        ////////////////////////

        if (
          currentDate == moment(startdate).format('ll') &&
          currentDate == moment(enddate).format('ll')
        ) {
          AsyncStorage.setItem(
            'what_CurrentDate',
            'Today \u00B7 ' +
              moment(startdate).format('LT') +
              ' - ' +
              moment(enddate).format('LT'),
          )
        } else {
          if (
            tomorrow == moment(startdate).format('ll') &&
            tomorrow == moment(enddate).format('ll')
          ) {
            AsyncStorage.setItem(
              'what_CurrentDate',
              'Tomorrow \u00B7 ' +
                moment(startdate).format('LT') +
                ' - ' +
                moment(enddate).format('LT'),
            )
          } else {
            if (
              currentDate == moment(startdate).format('ll') &&
              tomorrow == moment(enddate).format('ll')
            ) {
              AsyncStorage.setItem(
                'what_CurrentDate',
                'Today \u00B7 ' +
                  moment(startdate).format('LT') +
                  ' - Tomorrow \u00B7 ' +
                  moment(enddate).format('LT'),
              )
            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                AsyncStorage.setItem(
                  'what_CurrentDate',
                  ' Tomorrow  ' +
                    moment(enddate).format('ddd, MMM D \u00B7 LT'),
                )
              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  if (currentDate == moment(enddate).format('ll')) {
                    AsyncStorage.setItem('what_CurrentDate', 'Today · All Day')
                  } else {
                    AsyncStorage.setItem(
                      'what_CurrentDate',
                      'Today - ' +
                        moment(enddate).format('ddd, MMM D \u00B7 LT'),
                    )
                  }
                } else {
                  AsyncStorage.setItem(
                    'what_CurrentDate',
                    moment(startdate).format('ddd, MMM D \u00B7 LT') +
                      ' to ' +
                      moment(enddate).format('ddd, MMM D \u00B7 LT'),
                  )
                }
              }
            }
          }
        }
        AsyncStorage.setItem('what_start', moment(startdate).format('lll'))
        AsyncStorage.setItem('what_end', moment(enddate).format('lll'))
        AsyncStorage.setItem('what_is_now', '0')
        AsyncStorage.setItem('what_is_allday', '0')
      }
    } else {
      AsyncStorage.setItem('what_start', '')
      AsyncStorage.setItem('what_end', '')
      AsyncStorage.setItem('what_CurrentDate', '')
    }
  }

  setDateValueForWhere(sdate, edate) {
    var currentDate = moment().format('ll')
    let tomorrow = moment().add(1, 'days').format('ll')
    var date = moment(edate).format('ll')

    if (sdate != '') {
      var gmtStartDateTime = moment.utc(sdate)
      var startdate = gmtStartDateTime.local()

      var gmtEndDateTime = moment.utc(edate)
      var enddate = gmtEndDateTime.local()

      //alert('CurrentDate: '+currentDate+' StartDate: '+startdate+' EndDate :'+enddate)

      // if (this.state.currentitem.is_now) {
      //   if (date == currentDate) {
      //     AsyncStorage.setItem('where_CurrentDate', 'Now - ' + moment(enddate).format('LT'))
      //   } else {
      //     if (date == tomorrow) {
      //       AsyncStorage.setItem('where_CurrentDate', 'Now - Tomorrow · ' + moment(enddate).format('LT'))

      //     } else {
      //       AsyncStorage.setItem('where_CurrentDate', 'Now - ' + moment(date).format('ddd, MMM D') + ' · ' + moment(enddate).format('LT'))

      //     }

      //   }

      //   AsyncStorage.setItem('where_start', currentDate)
      //   AsyncStorage.setItem('where_end', moment(enddate).format('lll'))
      //   AsyncStorage.setItem('where_is_now', '1')
      //   AsyncStorage.setItem('where_is_allday', '0')

      // }
      // else

      //console.log('CurrentDate: '+currentDate+' StartDate: '+startdate+' EndDate :'+enddate)

      if (this.state.currentitem.is_allday == 1) {
        if (
          currentDate == moment(startdate).format('ll') &&
          currentDate == moment(enddate).format('ll')
        ) {
          AsyncStorage.setItem('where_CurrentDate', 'Today · All Day')
        } else {
          if (
            currentDate == moment(startdate).format('ll') &&
            tomorrow == moment(enddate).format('ll')
          ) {
            AsyncStorage.setItem(
              'where_CurrentDate',
              'Today - Tomorrow · All Day',
            )
          } else {
            if (
              tomorrow == moment(startdate).format('ll') &&
              tomorrow == moment(enddate).format('ll')
            ) {
              AsyncStorage.setItem('where_CurrentDate', 'Tomorrow · All Day')
            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                AsyncStorage.setItem(
                  'where_CurrentDate',
                  'Tomorrow - ' +
                    moment(enddate).format('ddd, MMM D') +
                    ' · All Day',
                )
              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  AsyncStorage.setItem(
                    'where_CurrentDate',
                    'Today - ' +
                      moment(enddate).format('ddd, MMM D') +
                      ' · All Day',
                  )
                } else {
                  if (
                    moment(startdate).format('ll') ==
                    moment(enddate).format('ll')
                  ) {
                    AsyncStorage.setItem(
                      'where_CurrentDate',
                      'All Day - ' + moment(enddate).format('ddd, MMM D'),
                    )
                  } else {
                    AsyncStorage.setItem(
                      'where_CurrentDate',
                      moment(startdate).format('ddd, MMM D') +
                        ' - ' +
                        moment(enddate).format('ddd, MMM D') +
                        ' · All Day',
                    )
                  }
                }
              }
            }
          }
        }
        AsyncStorage.setItem('where_start', moment(startdate).format('ll'))
        AsyncStorage.setItem('where_end', moment(startdate).format('ll'))
        AsyncStorage.setItem('where_is_now', '0')
        AsyncStorage.setItem('where_is_allday', '1')
      } else {
        /////////////

        if (
          currentDate == moment(startdate).format('ll') &&
          currentDate == moment(enddate).format('ll')
        ) {
          AsyncStorage.setItem(
            'where_CurrentDate',
            'Today \u00B7 ' +
              moment(startdate).format('LT') +
              ' - ' +
              moment(enddate).format('LT'),
          )
        } else {
          if (
            tomorrow == moment(startdate).format('ll') &&
            tomorrow == moment(enddate).format('ll')
          ) {
            AsyncStorage.setItem(
              'where_CurrentDate',
              'Tomorrow \u00B7 ' +
                moment(startdate).format('LT') +
                ' - ' +
                moment(enddate).format('LT'),
            )
          } else {
            if (
              currentDate == moment(startdate).format('ll') &&
              tomorrow == moment(enddate).format('ll')
            ) {
              AsyncStorage.setItem(
                'where_CurrentDate',
                'Today \u00B7 ' +
                  moment(startdate).format('LT') +
                  ' - Tomorrow \u00B7 ' +
                  moment(enddate).format('LT'),
              )
            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                AsyncStorage.setItem(
                  'where_CurrentDate',
                  ' Tomorrow - ' +
                    moment(enddate).format('ddd, MMM D \u00B7 LT'),
                )
              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  AsyncStorage.setItem(
                    'where_CurrentDate',
                    ' Today - ' +
                      moment(enddate).format('ddd, MMM D \u00B7 LT'),
                  )
                } else {
                  AsyncStorage.setItem(
                    'where_CurrentDate',
                    moment(startdate).format('ddd, MMM D \u00B7 LT') +
                      ' to ' +
                      moment(enddate).format('ddd, MMM D \u00B7 LT'),
                  )
                }
              }
            }
          }
        }
        AsyncStorage.setItem('where_start', moment(startdate).format('lll'))
        AsyncStorage.setItem('where_end', moment(enddate).format('lll'))
        AsyncStorage.setItem('where_is_now', '0')
        AsyncStorage.setItem('where_is_allday', '0')
      }
    } else {
      AsyncStorage.setItem('where_start', '')
      AsyncStorage.setItem('where_end', '')
      AsyncStorage.setItem('where_CurrentDate', '')
    }
  }

  setDateValueForWhen(sdate, edate) {
    var currentDate = moment().format('ll')
    let tomorrow = moment().add(1, 'days').format('ll')
    var date = moment(edate).format('ll')

    //alert(sdate)

    if (sdate != '') {
      var gmtStartDateTime = moment.utc(sdate)
      var startdate = gmtStartDateTime.local()

      var gmtEndDateTime = moment.utc(edate)
      var enddate = gmtEndDateTime.local()

      // if (this.state.currentitem.is_now) {
      //   if (date == currentDate) {
      //     AsyncStorage.setItem('when_CurrentDate', 'Now - ' + moment(enddate).format('LT'))
      //   } else {
      //     if (date == tomorrow) {
      //       AsyncStorage.setItem('when_CurrentDate', 'Now - Tomorrow · ' + moment(enddate).format('LT'))

      //     } else {
      //       AsyncStorage.setItem('when_CurrentDate', 'Now - ' + moment(this.state.date).format('ddd, MMM D') + ' · ' + moment(enddate).format('LT'))

      //     }

      //   }

      //   AsyncStorage.setItem('when_start', currentDate)
      //   AsyncStorage.setItem('when_end', moment(enddate).format('lll'))
      //   AsyncStorage.setItem('when_is_now', '1')
      //   AsyncStorage.setItem('when_is_allday', '0')
      // }
      // else

      //alert(JSON.stringify(this.state.currentitem))

      if (this.state.currentitem.is_allday == 1) {
        // alert('CurrentDate: '+currentDate+' StartDate: '+moment(startdate).format('ll')+' EndDate :'+moment(enddate).format('ll'))

        if (
          currentDate == moment(startdate).format('ll') &&
          currentDate == moment(enddate).format('ll')
        ) {
          AsyncStorage.setItem('when_CurrentDate', 'Today · All Day')
        } else {
          if (
            currentDate == moment(startdate).format('ll') &&
            tomorrow == moment(enddate).format('ll')
          ) {
            AsyncStorage.setItem(
              'when_CurrentDate',
              'Today - Tomorrow · All Day',
            )
          } else {
            if (
              tomorrow == moment(startdate).format('ll') &&
              tomorrow == moment(enddate).format('ll')
            ) {
              AsyncStorage.setItem('when_CurrentDate', 'Tomorrow · All Day')
            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                AsyncStorage.setItem(
                  'when_CurrentDate',
                  'Tomorrow - ' +
                    moment(enddate).format('ddd, MMM D') +
                    ' · All Day',
                )
              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  AsyncStorage.setItem(
                    'when_CurrentDate',
                    'Today - ' +
                      moment(enddate).format('ddd, MMM D') +
                      ' · All Day',
                  )
                } else {
                  if (
                    moment(startdate).format('ll') ==
                    moment(enddate).format('ll')
                  ) {
                    AsyncStorage.setItem(
                      'when_CurrentDate',
                      'All Day - ' + moment(enddate).format('ddd, MMM D'),
                    )
                  } else {
                    AsyncStorage.setItem(
                      'when_CurrentDate',
                      moment(startdate).format('ddd, MMM D') +
                        ' - ' +
                        moment(enddate).format('ddd, MMM D') +
                        ' · All Day',
                    )
                  }
                }
              }
            }
          }
        }
        AsyncStorage.setItem('when_start', moment(startdate).format('ll'))
        AsyncStorage.setItem('when_end', moment(enddate).format('ll'))
        AsyncStorage.setItem('when_is_now', '0')
        AsyncStorage.setItem('when_is_allday', '1')
      } else {
        ////////////////////////////////////////////////

        if (
          currentDate == moment(startdate).format('ll') &&
          currentDate == moment(enddate).format('ll')
        ) {
          AsyncStorage.setItem(
            'when_CurrentDate',
            'Today \u00B7 ' +
              moment(startdate).format('LT') +
              ' - ' +
              moment(enddate).format('LT'),
          )
        } else {
          if (
            tomorrow == moment(startdate).format('ll') &&
            tomorrow == moment(enddate).format('ll')
          ) {
            AsyncStorage.setItem(
              'when_CurrentDate',
              'Tomorrow \u00B7 ' +
                moment(startdate).format('LT') +
                ' - ' +
                moment(enddate).format('LT'),
            )
          } else {
            if (
              currentDate == moment(startdate).format('ll') &&
              tomorrow == moment(enddate).format('ll')
            ) {
              AsyncStorage.setItem(
                'when_CurrentDate',
                'Today \u00B7 ' +
                  moment(startdate).format('LT') +
                  ' - Tomorrow \u00B7 ' +
                  moment(enddate).format('LT'),
              )
            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                AsyncStorage.setItem(
                  'when_CurrentDate',
                  ' Tomorrow - ' +
                    moment(enddate).format('ddd, MMM D \u00B7 LT'),
                )
              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  AsyncStorage.setItem(
                    'when_CurrentDate',
                    ' Today - ' +
                      moment(enddate).format('ddd, MMM D \u00B7 LT'),
                  )
                } else {
                  AsyncStorage.setItem(
                    'when_CurrentDate',
                    moment(startdate).format('ddd, MMM D \u00B7 LT') +
                      ' - ' +
                      moment(enddate).format('ddd, MMM D \u00B7 LT'),
                  )
                }
              }
            }
          }
        }
        AsyncStorage.setItem('when_start', moment(startdate).format('lll'))
        AsyncStorage.setItem('when_end', moment(enddate).format('lll'))
        AsyncStorage.setItem('when_is_now', '0')
        AsyncStorage.setItem('when_is_allday', '0')
      }
    } else {
      AsyncStorage.setItem('when_start', '')
      AsyncStorage.setItem('when_end', '')
      AsyncStorage.setItem('when_CurrentDate', '')
    }
  }

  onPressUserName(item) {
    AsyncStorage.setItem('other_id', item.user_id)
    this.props.navigation.navigate('ohter_user_profile')
  }

  _keyExtractor = (item, index) => item.plan_id

  _renderItem = ({ item, index }) => {
    var login_name = this.state.name.split(' ')

    let Final_date = ''
    let dateTime = ''
    var currentDate = moment().format('ll')
    var tomorrow = moment().add(1, 'days').format('ll')

    var name = item.friend_name.split(' ')

    var start = item.start_date.split(' ')
    var end = item.end_date.split(' ')

    var gmtDateTime = moment.utc(item.created_date)
    var date1 = gmtDateTime.local()

    if (moment(date1).format('ll') == moment().format('ll')) {
      dateTime = date1.fromNow()
    } else {
      dateTime = date1.calendar()
    }
    var activity = ''
    var str = String(item.activity)
    if (str.includes('Wanna')) {
      activity = str.substr(6).slice(0, -1)
    } else {
      activity = str
    }
    if (activity.includes('?')) {
      activity = activity.slice(0, -1)
    }

    /*if(item.start_date != '')
    {
          var gmtStartDateTime = moment.utc(item.start_date)
          var localstartdate = gmtStartDateTime.local();

          var gmtEndDateTime = moment.utc(item.end_date)
          var localenddate = gmtEndDateTime.local();

      if (item.is_now == '1') {
        if (currentDate == moment(localstartdate).format('ll') && currentDate == moment(localenddate).format('ll')) {
          Final_date = 'Now - ' + moment(localenddate).format('LT')
        } else {
          if (currentDate == moment(localstartdate).format('ll') && tomorrow == moment(localenddate).format('ll')) {
            Final_date = 'Now - Tomorrow · ' + moment(localenddate).format('LT')
          } else {
            Final_date = 'Now - ' + moment(localenddate).format('ddd, MMM D \u00B7 LT')
          }
  
        }
      } else {


        if (currentDate == moment(localstartdate).format('ll') && currentDate == moment(localenddate).format('ll')) {

          if (start[1] == '00:00:00' && end[1] == '00:00:00') {
            Final_date = 'Today · All Day'
          } else {

            if(item.is_allday == 1)
            {
               Final_date = 'Today · All Day'
            }
            else
            {
              Final_date = 'Today · ' + moment(localstartdate).format('LT')
            }
          }
        } else {
          if (start[1] == '00:00:00' && end[1] == '00:00:00') {
            if (tomorrow == moment(localstartdate).format('ll') && tomorrow == moment(localenddate).format('ll')) {
              Final_date = 'Tomorrow · All Day'
  
            } else {
              if (tomorrow == moment(localstartdate).format('ll')) {
                Final_date = 'Tomorrow · All Day'
  
              } else {
                if (currentDate == moment(localstartdate).format('ll')) {
                  Final_date = 'Today' + ' ·All Day '
  
                } else {
                  
                  Final_date = moment(localstartdate).format('ddd, MMM D') + ' · All Day '
                }
  
              }
  
            }
          } else {
            if (tomorrow == moment(localstartdate).format('ll') && tomorrow == moment(localenddate).format('ll')) {

              if(item.is_allday == 1)
              {
                Final_date = 'Tomorrow · All Day'
              }
              else
              {
                Final_date = 'Tomorrow · ' + moment(localstartdate).format('LT')
              }

              
            } else {
              if (tomorrow == moment(localstartdate).format('ll')) {
                Final_date = 'Tomorrow · ' + moment(localstartdate).format('LT')
  
              } else {
                if (currentDate == moment(localstartdate).format('ll')) {
                  Final_date = 'Today · ' + moment(localstartdate).format('LT')
  
                } else {
                  
                  if(item.is_allday == 1)
                  {
                    Final_date = moment(localstartdate).format('ddd, MMM D') + ' · All Day '
                  }
                  else
                  {
                    Final_date = moment(localstartdate).format('ddd, MMM D \u00B7 LT')
                  }

                  
                }
  
              }
  
            }
  
          }
  
        }
  
      }
    }*/

    if (item.start_date != '') {
      var gmtStartDateTime = moment.utc(item.start_date)
      var startdate = gmtStartDateTime.local()

      var gmtEndDateTime = moment.utc(item.end_date)
      var enddate = gmtEndDateTime.local()

      //     if (this.state.currentitem.is_now) {
      //       if (date == currentDate) {
      //         AsyncStorage.setItem('what_CurrentDate', 'Now - ' + moment(enddate).format('LT'))
      //       } else {
      //         if (date == tomorrow) {
      //           AsyncStorage.setItem('what_CurrentDate', 'Now - Tomorrow · ' + moment(enddate).format('LT'))

      //         } else {
      //           AsyncStorage.setItem('what_CurrentDate', 'Now - ' + moment(date).format('ddd, MMM D') + ' · ' + moment(enddate).format('LT'))

      //         }

      //       }

      //       AsyncStorage.setItem('what_start', currentDate)
      //       AsyncStorage.setItem('what_end', moment(enddate).format('lll'))
      //       AsyncStorage.setItem('what_is_now', '1')
      //       AsyncStorage.setItem('what_is_allday', '0')
      // }
      // else

      if (item.is_allday == 1) {
        if (
          currentDate == moment(startdate).format('ll') &&
          currentDate == moment(enddate).format('ll')
        ) {
          //AsyncStorage.setItem('what_CurrentDate', 'Today · All Day')
          Final_date = 'Today · All Day'
        } else {
          if (
            currentDate == moment(startdate).format('ll') &&
            tomorrow == moment(enddate).format('ll')
          ) {
            //AsyncStorage.setItem('what_CurrentDate', 'Today - Tomorrow · All Day')
            Final_date = 'Today - Tomorrow · All Day'
          } else {
            if (
              tomorrow == moment(startdate).format('ll') &&
              tomorrow == moment(enddate).format('ll')
            ) {
              //AsyncStorage.setItem('what_CurrentDate', 'Tomorrow · All Day')
              Final_date = 'Tomorrow · All Day'
            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                //AsyncStorage.setItem('what_CurrentDate', 'Tomorrow - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')
                Final_date =
                  'Tomorrow - ' +
                  moment(enddate).format('ddd, MMM D') +
                  ' · All Day'
              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  //AsyncStorage.setItem('what_CurrentDate', 'Today - ' + moment(enddate).format('ddd, MMM D') + ' · All Day')
                  Final_date =
                    'Today - ' +
                    moment(enddate).format('ddd, MMM D') +
                    ' · All Day'
                } else {
                  if (
                    moment(startdate).format('ll') ==
                    moment(enddate).format('ll')
                  ) {
                    //AsyncStorage.setItem('what_CurrentDate', 'All Day - ' + moment(enddate).format('ddd, MMM D'))
                    Final_date =
                      'All Day - ' + moment(enddate).format('ddd, MMM D')
                  } else {
                    //AsyncStorage.setItem('what_CurrentDate', moment(startdate).format('ddd, MMM D') + ' - ' + moment(enddate).format('ddd, MMM D') + ' · ALL DAY')
                    Final_date =
                      moment(startdate).format('ddd, MMM D') +
                      ' - ' +
                      moment(enddate).format('ddd, MMM D') +
                      ' · All Day'
                  }
                }
              }
            }
          }
        }
      } else {
        ////////////////////////

        if (
          currentDate == moment(startdate).format('ll') &&
          currentDate == moment(enddate).format('ll')
        ) {
          //AsyncStorage.setItem('what_CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))
          Final_date =
            'Today \u00B7 ' +
            moment(startdate).format('LT') +
            ' - ' +
            moment(enddate).format('LT')
        } else {
          if (
            tomorrow == moment(startdate).format('ll') &&
            tomorrow == moment(enddate).format('ll')
          ) {
            //AsyncStorage.setItem('what_CurrentDate', 'Tomorrow \u00B7 ' + moment(startdate).format('LT') + ' - ' + moment(enddate).format('LT'))
            Final_date =
              'Tomorrow \u00B7 ' +
              moment(startdate).format('LT') +
              ' - ' +
              moment(enddate).format('LT')
          } else {
            if (
              currentDate == moment(startdate).format('ll') &&
              tomorrow == moment(enddate).format('ll')
            ) {
              //AsyncStorage.setItem('what_CurrentDate', 'Today \u00B7 ' + moment(startdate).format('LT') + ' - Tomorrow \u00B7 ' + moment(enddate).format('LT'))
              Final_date =
                'Today \u00B7 ' +
                moment(startdate).format('LT') +
                ' - Tomorrow \u00B7 ' +
                moment(enddate).format('LT')
            } else {
              if (tomorrow == moment(startdate).format('ll')) {
                //AsyncStorage.setItem('what_CurrentDate', ' Tomorrow  ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
                Final_date =
                  ' Tomorrow  ' + moment(enddate).format('ddd, MMM D \u00B7 LT')
              } else {
                if (currentDate == moment(startdate).format('ll')) {
                  if (currentDate == moment(enddate).format('ll')) {
                    //AsyncStorage.setItem('what_CurrentDate', 'Today · All Day')
                    Final_date = 'Today · All Day'
                  } else {
                    //AsyncStorage.setItem('what_CurrentDate', 'Today - ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
                    Final_date =
                      'Today - ' +
                      moment(enddate).format('ddd, MMM D \u00B7 LT')
                  }
                } else {
                  //AsyncStorage.setItem('what_CurrentDate', moment(startdate).format('ddd, MMM D \u00B7 LT') + ' to ' + moment(enddate).format('ddd, MMM D \u00B7 LT'))
                  Final_date =
                    moment(startdate).format('ddd, MMM D \u00B7 LT') +
                    ' to ' +
                    moment(enddate).format('ddd, MMM D \u00B7 LT')
                }
              }
            }
          }
        }
      }
    }

    return (
      <View>
        {index == 0 ? (
          <View>
            <Animated.View style={{ height: Height(7) }}>
              <View
                style={{
                  flexDirection: 'row',
                  backgroundColor: colors.white,
                  height: Height(9.3),
                }}
              >
                {/* <TouchableOpacity onPress={this.getPostList} style={{ flexDirection: 'row', top: Height(0.7), justifyContent: 'center', left: Width(35), alignItems: 'center', position: 'absolute', height: Height(3.5), width: Width(28), borderRadius: Height(2), backgroundColor: colors.blue }}>
                <Text style={{ color: colors.white, fontSize: FontSize(13), fontFamily: fonts.Raleway_Bold }}>New Posts</Text>
                <AntDesign style={{ marginLeft: 3 }} name="arrowup" color={colors.white} size={15} />
              </TouchableOpacity> */}
                <View
                  style={{ justifyContent: 'center', marginLeft: Width(4.5) }}
                >
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('myprofile')}
                    underlayColor="transparent"
                    style={{
                      height: Height(5),
                      width: Height(5),
                      borderRadius: Height(7),
                      overflow: 'hidden',
                    }}
                  >
                    {this.state.photo == '' ? (
                      <Image
                        source={avtar}
                        style={{ height: '100%', width: '100%' }}
                      />
                    ) : (
                      <Image
                        style={{ height: '100%', width: '100%' }}
                        source={{ uri: this.state.photo }}
                      />
                    )}
                  </TouchableOpacity>
                </View>

                <View style={{ justifyContent: 'center' }}>
                  <Text
                    // onPress={() => this.props.navigation.navigate('myprofile')}

                    style={{
                      fontFamily: fonts.Raleway_Regular,
                      fontSize: FontSize(18),
                      color: colors.fontDarkGrey,
                      marginLeft: Width(3.5),
                    }}
                  >
                    {login_name[0]}, want to make a plan?
                  </Text>
                </View>
              </View>
            </Animated.View>
            <View
              style={{
                marginTop: Height(2),
                height: 1,
                backgroundColor: '#F7F7F7',
              }}
            ></View>
            <Animated.View
              style={{
                padding: 8,
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'space-around',
                backgroundColor: 'white',
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('Who1', {
                    is_edit_plan: false,
                  })
                }}
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingHorizontal: 10,
                }}
              >
                <Image source={who} style={{ margin: '3%' }} />
                <Text style={styles.headerPlaneColor}>Who</Text>
              </TouchableOpacity>

              <View
                style={{
                  height: Height(2.5),
                  width: 1,
                  backgroundColor: '#F7F7F7',
                  mjustifyContent: 'center',
                  alignItems: 'center',
                }}
              ></View>

              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('what1', {
                    is_edit_plan: false,
                  })
                }}
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingHorizontal: 10,
                }}
              >
                <Image source={cup} style={{ margin: '3%' }} />
                <Text style={styles.headerPlaneColor}>What</Text>
              </TouchableOpacity>

              <View
                style={{
                  height: Height(2.5),
                  width: 1,
                  backgroundColor: '#F7F7F7',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              ></View>

              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('where1', {
                    is_edit_plan: false,
                  })
                }}
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingHorizontal: 10,
                }}
              >
                <Image
                  source={Location_new}
                  style={[
                    isIphone() ? styles.textInputIos : styles.textInputAndroid,
                  ]}
                />
                <Text style={styles.headerPlaneColor}>Where</Text>
              </TouchableOpacity>

              <View
                style={{
                  height: Height(2.5),
                  width: 1,
                  backgroundColor: colors.lightgrey,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              ></View>

              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('when1', {
                    is_edit_plan: false,
                  })
                }}
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingHorizontal: 10,
                }}
              >
                <Image source={checked} style={{ margin: '3%' }} />
                <Text style={styles.headerPlaneColor}>When</Text>
              </TouchableOpacity>
            </Animated.View>

            {
              <View
                style={{
                  height: Height(25),
                  backgroundColor: '#fff',
                  marginTop: Height(0.7),
                }}
              >
                <Text
                  style={{
                    marginTop: Height(2),
                    marginLeft: Width(4.5),
                    color: colors.appColor,
                    fontFamily: fonts.Raleway_Bold,
                    fontSize: FontSize(18),
                  }}
                >
                  Virtual Activities
                </Text>
                <View
                  style={{
                    marginTop: '2%',
                    marginLeft: Width(0),
                    marginRight: Width(1),
                    flexDirection: 'row',
                  }}
                >
                  <ScrollView
                    showsHorizontalScrollIndicator={false}
                    horizontal
                    contentContainerStyle={{ paddingHorizontal: Width(1) }}
                  >
                    {this.arronlineactivity.map((item) => {
                      return (
                        <TouchableOpacity
                          style={{
                            backgroundColor: '#F6F5FB',
                            height:
                              Platform.OS == 'android'
                                ? Height(16)
                                : Height(15.3),
                            width: Width(33.5),
                            borderRadius: Height(2),
                            alignItems: 'center',
                            marginTop: Height(2),
                            marginHorizontal: Width(1),
                            justifyContent: 'center',
                          }}
                          activeOpacity={0.9}
                          onPress={() => this.onPressOnlineActivity(item)}
                        >
                          <View
                            style={{
                              height: Height(5),
                              width: Height(5),
                              marginTop: Height(1.5),
                              overflow: 'hidden',
                              alignItems: 'center',
                              justifyContent: 'center',
                            }}
                          >
                            <Image
                              style={{ height: '100%', width: '100%' }}
                              resizeMode="contain"
                              source={item.icon}
                            />
                          </View>
                          <Text
                            style={{
                              marginTop: Height(3),
                              fontSize: FontSize(13),
                              color: '#3A4759',
                              fontFamily: fonts.Roboto_Medium,
                            }}
                            numberOfLines={1}
                          >
                            {item.title}
                          </Text>
                        </TouchableOpacity>
                      )
                    })}
                  </ScrollView>
                </View>
              </View>
            }
          </View>
        ) : null}

        {item.plan_type == 1 && item.is_hide == 0 ? (
          <View
            style={{ marginTop: Height(0.7), backgroundColor: colors.white }}
          >
            {/* <TouchableOpacity onPress={() => {
           this.props.navigation.navigate('EventDetail', { item: item , plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header:name[0]+", let's meet up!" , Final_date : Final_date, eventby:item.name})
             }} style={{ height: Height(9), flexDirection: 'row', alignItems: 'center', backgroundColor: 'white' }}> */}
            <TouchableOpacity
              style={{
                height: Height(9),
                flexDirection: 'row',
                alignItems: 'center',
                backgroundColor: 'white',
              }}
              activeOpacity={0.9}
            >
              <View style={{ alignItems: 'center', marginLeft: Width(4.5) }}>
                <TouchableOpacity
                  onPress={
                    item.login_id == this.state.login_id
                      ? () => this.props.navigation.navigate('myprofile')
                      : () => {
                          AsyncStorage.setItem('other_id', item.login_id)
                          this.props.navigation.navigate('ohter_user_profile')
                        }
                  }
                  underlayColor="transparent"
                  style={{
                    height: Height(5),
                    width: Height(5),
                    borderRadius: Height(7),
                    overflow: 'hidden',
                  }}
                >
                  {item.photo == '' ? (
                    <Image
                      source={avtar}
                      style={{ height: '100%', width: '100%' }}
                    />
                  ) : (
                    <Image
                      style={{ height: '100%', width: '100%' }}
                      source={{ uri: item.photo }}
                    />
                  )}
                </TouchableOpacity>
              </View>
              <View style={{ justifyContent: 'center', marginLeft: Width(3) }}>
                <Text
                  onPress={
                    item.login_id == this.state.login_id
                      ? () => this.props.navigation.navigate('myprofile')
                      : () => {
                          AsyncStorage.setItem('other_id', item.login_id)
                          this.props.navigation.navigate('ohter_user_profile')
                        }
                  }
                  style={{
                    fontSize: FontSize(18),
                    color: colors.dargrey,
                    fontFamily: fonts.Raleway_Bold,
                  }}
                >
                  {item.login_id == this.state.login_id ? 'Me' : item.name}
                </Text>
                <Text
                  style={{
                    fontSize: FontSize(12),
                    color: colors.fontDarkGrey,
                    fontFamily: fonts.Roboto_Regular,
                  }}
                >
                  {dateTime}
                  {item.share_with == '' ? null : '\u00B7 '}
                  {item.share_with}
                </Text>
              </View>
              <View
                style={{
                  alignItems: 'flex-end',
                  alignSelf: 'center',
                  marginRight: Width(4),
                  flex: 4,
                }}
              >
                <TouchableOpacity
                  onPress={() => this.modelVisible(item, index)}
                  underlayColor="transparent"
                  style={{
                    justifyContent: 'center',
                    height: Height(6),
                    width: Width(6),
                  }}
                >
                  <Entypo
                    size={Width(6)}
                    name="dots-three-horizontal"
                    color="#000"
                  />
                </TouchableOpacity>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.9}
              onPress={() => {
                //this.props.navigation.navigate('EventDetail', { item: item , plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type , title_header:name[0]+", let's meet up!", Final_date : Final_date, eventby:item.name})
                this.props.navigation.navigate('EventDetail', {
                  item: item,
                  plan_id: item.plan_id,
                  other_id: item.login_id,
                  plan_type: item.plan_type,
                  title_header: "Let's meet up!",
                  Final_date: Final_date,
                  eventby: item.name,
                  invitedList: item.is_invited_list,
                })
              }}
            >
              <ImageBackground
                source={
                  item.plan_image == '' ||
                  item.plan_image == null ||
                  item.plan_image == undefined
                    ? default_placeholder
                    : { uri: item.plan_image }
                }
                style={{
                  height: 265,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                {/* <Text style={{ fontSize: FontSize(20), color: colors.dargrey, fontFamily: fonts.Raleway_Semibold, marginHorizontal:48, lineHeight:27,textAlign:'center' }}>{name[0]}, let's meet up!</Text> */}
                {/* <Text style={{ fontSize: FontSize(20), color: colors.dargrey, fontFamily: fonts.Raleway_Semibold, marginHorizontal:48, lineHeight:27,textAlign:'center' }}>Let's meet up!</Text>
              {item.description == '' ? null : <Text style={{ textAlign: 'center', fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, width: '85%', marginTop: 2, alignSelf: 'center' }}>{item.description}</Text>} */}
              </ImageBackground>
            </TouchableOpacity>

            <View style={{ paddingHorizontal: Width(4.5) }}>
              <Text
                style={{
                  fontSize: FontSize(19),
                  color: colors.dargrey,
                  fontFamily: fonts.Raleway_Bold,
                  lineHeight: 27,
                  textAlign: 'left',
                  marginTop: 10,
                }}
              >
                Let's meet up!
              </Text>
              {/* {item.description == '' ? null : <Text style={{ textAlign: 'left', fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, marginTop: 4,marginHorizontal:Width(4.5)}} numberOfLines={2}>{item.description}</Text>} */}
              {item.description == '' ? null : (
                <SeeMore
                  style={{
                    textAlign: 'left',
                    fontSize: FontSize(13),
                    color: colors.dargrey,
                    fontFamily: fonts.Roboto_Regular,
                    marginTop: 4,
                  }}
                  numberOfLines={2}
                >
                  {item.description}
                </SeeMore>
              )}
            </View>

            <View
              style={{
                backgroundColor: '#FFF',
                alignItems: 'center',
                flexDirection: 'row',
              }}
            >
              <View style={{ flex: 1, marginLeft: Width(4.5) }}>
                <TouchableOpacity
                  style={{ flexDirection: 'row', marginTop: Height(2) }}
                >
                  <Image
                    resizeMode="contain"
                    style={{ alignSelf: 'center' }}
                    source={chai_cup}
                  />
                  {item.activity == '' ? (
                    <Text
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: '#358AF6',
                        marginRight: Width(5),
                      }}
                      onPress={
                        item.total_activity_suggestions == 0
                          ? () =>
                              this.props.navigation.navigate('Activity', {
                                plan_id: item.plan_id,
                                other_id: item.login_id,
                                is_direct: true,
                                title_header: "Let's meet up!",
                              })
                          : () =>
                              this.props.navigation.navigate(
                                'SuggestActivity',
                                {
                                  other_id: item.login_id,
                                  plan_id: item.plan_id,
                                  is_direct: false,
                                  title_header: "Let's meet up!",
                                },
                              )
                      }
                    >
                      {item.total_activity_suggestions == 0
                        ? 'Suggest an activity'
                        : 'View activity suggestions'}
                    </Text>
                  ) : (
                    <Text
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: colors.dargrey,
                        marginRight: Width(5),
                      }}
                    >
                      {item.activity}
                    </Text>
                  )}
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    marginTop: Height(2),
                    marginBottom: Height(2),
                  }}
                >
                  <Image
                    resizeMode="contain"
                    style={{ alignSelf: 'center' }}
                    source={calendar}
                  />
                  {item.start_date == '' ? (
                    <Text
                      onPress={
                        item.total_datetime_suggestions == 0
                          ? () =>
                              this.props.navigation.navigate('DateTime', {
                                plan_id: item.plan_id,
                                other_id: item.login_id,
                                is_direct: true,
                              })
                          : () =>
                              this.props.navigation.navigate(
                                'SuggestDateTime',
                                {
                                  other_id: item.login_id,
                                  plan_id: item.plan_id,
                                  is_direct: false,
                                },
                              )
                      }
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: '#358AF6',
                        marginRight: Width(5),
                      }}
                    >
                      {item.total_datetime_suggestions == 0
                        ? 'Suggest a date & time'
                        : 'View date & time suggestions'}
                    </Text>
                  ) : (
                    <Text
                      style={{
                        width: Width(60),
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: colors.dargrey,
                        marginRight: Width(5),
                      }}
                    >
                      {Final_date}
                    </Text>
                  )}
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ flexDirection: 'row', marginBottom: Height(2) }}
                >
                  <Image
                    resizeMode="contain"
                    style={{
                      alignSelf: 'center',
                      tintColor: colors.whatFontColor,
                    }}
                    source={location}
                  />
                  {item.line_1 == '' ? (
                    <Text
                      onPress={
                        item.total_location_suggestions == 0
                          ? () =>
                              this.props.navigation.navigate('Location', {
                                plan_id: item.plan_id,
                                other_id: item.login_id,
                                is_direct: true,
                              })
                          : () =>
                              this.props.navigation.navigate(
                                'SuggestLocation',
                                {
                                  other_id: item.login_id,
                                  plan_id: item.plan_id,
                                  is_direct: false,
                                },
                              )
                      }
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: '#358AF6',
                        marginRight: Width(5),
                      }}
                    >
                      {item.total_location_suggestions == 0
                        ? 'Suggest a location'
                        : 'View location suggestions'}
                    </Text>
                  ) : (
                    <Text
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: colors.dargrey,
                        marginRight: Width(5),
                      }}
                    >
                      {item.line_1}
                    </Text>
                  )}
                </TouchableOpacity>

                <View
                  style={{
                    height: Height(0.15),
                    backgroundColor: '#EFEFF4',
                    marginRight: Width(4),
                  }}
                />

                {item.is_going_count != 0 ||
                item.is_interested_count != 0 ||
                item.is_invited_count != 0 ? (
                  <View
                    style={{ flexDirection: 'row', marginBottom: Height(1.5) }}
                  >
                    {/* {item.is_going_count != 0 || item.is_interested_count != 0 || item.is_invited_count != 0 ?
                  <Image resizeMode="contain" style={{ marginTop: Height(2), alignSelf: 'center', tintColor: colors.whatFontColor }} source={new_user} /> : null} */}
                    {item.is_going_count != 0 ? (
                      <Text
                        onPress={() => {
                          this.props.navigation.navigate('Friend_Status', {
                            plan_id: item.plan_id,
                            tabIndex: 0,
                            Title_New: item.activity,
                            plan_type: item.plan_type,
                          })
                        }}
                        style={{
                          marginTop: Height(1.5),
                          paddingLeft: Width(0),
                          fontFamily: fonts.Roboto_Regular,
                          fontSize: FontSize(12),
                          color: colors.dargrey,
                        }}
                      >
                        {item.is_going_count + ' going'}
                      </Text>
                    ) : null}
                    {item.is_interested_count != 0 ? (
                      <Text
                        onPress={() => {
                          this.props.navigation.navigate('Friend_Status', {
                            plan_id: item.plan_id,
                            tabIndex: 1,
                            Title_New: item.activity,
                            plan_type: item.plan_type,
                          })
                        }}
                        style={
                          item.is_going_count == 0
                            ? {
                                marginTop: Height(1.5),
                                paddingLeft: Width(0),
                                fontFamily: fonts.Roboto_Regular,
                                fontSize: FontSize(12),
                                color: colors.dargrey,
                              }
                            : {
                                marginTop: Height(1.5),
                                paddingLeft: Width(1),
                                fontFamily: fonts.Roboto_Regular,
                                fontSize: FontSize(12),
                                color: colors.dargrey,
                              }
                        }
                      >
                        {item.is_going_count != 0 ? '\u00B7 ' : null}
                        {item.is_interested_count + ' interested'}
                      </Text>
                    ) : null}
                    {item.is_invited_count != 0 ? (
                      <Text
                        onPress={() => {
                          this.props.navigation.navigate('Friend_Status', {
                            plan_id: item.plan_id,
                            tabIndex: 2,
                            Title_New: item.activity,
                            plan_type: item.plan_type,
                          })
                        }}
                        style={
                          item.is_going_count == 0 &&
                          item.is_interested_count == 0
                            ? {
                                marginTop: Height(1.5),
                                paddingLeft: Width(0),
                                fontFamily: fonts.Roboto_Regular,
                                fontSize: FontSize(12),
                                color: colors.dargrey,
                              }
                            : {
                                marginTop: Height(1.5),
                                paddingLeft: Width(1),
                                fontFamily: fonts.Roboto_Regular,
                                fontSize: FontSize(12),
                                color: colors.dargrey,
                              }
                        }
                      >
                        {item.is_interested_count != 0 ||
                        item.is_going_count != 0
                          ? '\u00B7 '
                          : null}
                        {item.is_invited_count + ' invited'}
                      </Text>
                    ) : null}
                    <TouchableOpacity
                      style={{
                        marginTop: Height(1.5),
                        position: 'absolute',
                        right: Width(4),
                        width: 35,
                        height: 35,
                      }}
                      onPress={() => {
                        //this.props.navigation.navigate('EventDetail', { item: item , plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type , title_header:name[0]+", let's meet up!", Final_date : Final_date, eventby:item.name})
                        this.props.navigation.navigate('EventDetail', {
                          item: item,
                          plan_id: item.plan_id,
                          other_id: item.login_id,
                          plan_type: item.plan_type,
                          title_header: "Let's meet up!",
                          Final_date: Final_date,
                          eventby: item.name,
                          invitedList: item.is_invited_list,
                        })
                      }}
                    >
                      <Image resizeMode="contain" source={ic_chat} />
                    </TouchableOpacity>
                  </View>
                ) : (
                  <View>
                    {item.login_id != this.state.login_id ? (
                      <Text
                        style={{
                          marginTop: Height(1.5),
                          marginBottom: Height(1.5),
                          paddingLeft: Width(0),
                          fontFamily: fonts.Roboto_Regular,
                          fontSize: FontSize(12),
                          color: colors.dargrey,
                        }}
                      >
                        Be the first guest
                      </Text>
                    ) : (
                      <Text
                        style={{
                          marginTop: Height(1.5),
                          marginBottom: Height(1.5),
                          paddingLeft: Width(0),
                          fontFamily: fonts.Roboto_Regular,
                          fontSize: FontSize(12),
                          color: colors.dargrey,
                        }}
                      >
                        No guests yet
                      </Text>
                    )}
                    <TouchableOpacity
                      style={{
                        marginTop: Height(1.5),
                        marginBottom: Height(1.5),
                        position: 'absolute',
                        right: Width(4),
                        width: 35,
                        height: 35,
                      }}
                      onPress={() => {
                        //this.props.navigation.navigate('EventDetail', { item: item , plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type , title_header:name[0]+", let's meet up!", Final_date : Final_date, eventby:item.name})
                        this.props.navigation.navigate('EventDetail', {
                          item: item,
                          plan_id: item.plan_id,
                          other_id: item.login_id,
                          plan_type: item.plan_type,
                          title_header: "Let's meet up!",
                          Final_date: Final_date,
                          eventby: item.name,
                          invitedList: item.is_invited_list,
                        })
                      }}
                    >
                      <Image resizeMode="contain" source={ic_chat} />
                    </TouchableOpacity>
                  </View>
                )}
                {item.login_id != this.state.login_id ? (
                  <View>
                    <View
                      style={{
                        height: Height(0.15),
                        backgroundColor: '#EFEFF4',
                        marginRight: Width(4),
                      }}
                    />
                    <View
                      style={{
                        flexDirection: 'row',
                        alignSelf: 'center',
                        justifyContent: 'center',
                        marginBottom: Height(1),
                        marginTop: Height(2),
                      }}
                    >
                      {/* {(item.is_going == 0 && item.is_interested == 0 && (item.activity != '' && item.line_1 != '' && item.start_date != ''))? */}
                      {item.activity != '' || item.line_1 != '' ? (
                        <View
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                        >
                          {item.is_going == 1 ? (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                onPress={() => this.goToNoGoing(item, index)}
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                              >
                                <Image source={going1} />
                                <Text
                                  style={{
                                    color: colors.pink,
                                    fontFamily: fonts.Roboto_Regular,
                                    marginLeft: Width(1),
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Going
                                </Text>
                              </TouchableOpacity>
                            </View>
                          ) : (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                                onPress={() => this.goToGoing(item, index)}
                              >
                                <Image source={going} />
                                <Text
                                  style={{
                                    fontFamily: fonts.Raleway_Regular,
                                    color: colors.dargrey,
                                    marginLeft: Width(1),
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Going
                                </Text>
                              </TouchableOpacity>
                            </View>
                          )}
                          <View style={{ width: Width(10) }} />

                          {item.is_interested == 1 ? (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                onPress={() =>
                                  this.goToNoIntersted(item, index)
                                }
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                              >
                                <Image source={circle_favorite} />
                                <Text
                                  style={{
                                    marginLeft: Width(1),
                                    color: colors.pink,
                                    fontFamily: fonts.Roboto_Regular,
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Interested
                                </Text>
                              </TouchableOpacity>
                            </View>
                          ) : (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                                onPress={() => this.goToIntersted(item, index)}
                              >
                                <Image source={black_heart} />
                                <Text
                                  style={{
                                    fontFamily: fonts.Raleway_Regular,
                                    color: colors.dargrey,
                                    marginLeft: Width(1),
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Interested
                                </Text>
                              </TouchableOpacity>
                            </View>
                          )}
                          <View style={{ width: Width(10) }} />
                          <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                              onPress={() => this.goToShare(item)}
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                              }}
                            >
                              <Image source={share} />
                              <Text
                                style={{
                                  color: colors.dargrey,
                                  fontFamily: fonts.Raleway_Regular,
                                  marginLeft: Width(1),
                                  fontSize: FontSize(16),
                                }}
                              >
                                Share
                              </Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                      ) : (
                        <View
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                        >
                          {item.is_interested == 1 ? (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                onPress={() =>
                                  this.goToNoIntersted(item, index)
                                }
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                              >
                                <Image source={circle_favorite} />
                                <Text
                                  style={{
                                    marginLeft: Width(1),
                                    color: colors.pink,
                                    fontFamily: fonts.Roboto_Regular,
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Interested
                                </Text>
                              </TouchableOpacity>
                            </View>
                          ) : (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                                onPress={() => this.goToIntersted(item, index)}
                              >
                                <Image source={black_heart} />
                                <Text
                                  style={{
                                    fontFamily: fonts.Raleway_Regular,
                                    color: colors.dargrey,
                                    marginLeft: Width(1),
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Interested
                                </Text>
                              </TouchableOpacity>
                            </View>
                          )}
                          <View style={{ width: Width(10) }} />
                          <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                              onPress={() => this.goToShare(item)}
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                              }}
                            >
                              <Image source={share} />
                              <Text
                                style={{
                                  color: colors.dargrey,
                                  fontFamily: fonts.Raleway_Regular,
                                  marginLeft: Width(1),
                                  fontSize: FontSize(16),
                                }}
                              >
                                Share
                              </Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                      )}
                    </View>
                  </View>
                ) : null}

                {item.login_id == this.state.login_id ? (
                  <View style={{ backgroundColor: 'white' }}>
                    <View
                      style={{
                        height: Height(0.15),
                        backgroundColor: '#EFEFF4',
                        marginRight: Width(4),
                      }}
                    />
                    <View
                      style={{
                        flexDirection: 'row',
                        alignSelf: 'center',
                        justifyContent: 'center',
                        marginBottom: Height(1),
                        marginTop: Height(2),
                      }}
                    >
                      <View
                        style={{
                          flexDirection: 'row',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}
                      >
                        <TouchableOpacity
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                          onPress={() => this.gotoEditEvent(item)}
                        >
                          <Image source={Edit} />
                          <Text
                            style={{
                              fontFamily: fonts.Raleway_Regular,
                              color: colors.dargrey,
                              marginLeft: Width(1.5),
                              fontSize: FontSize(16),
                            }}
                          >
                            Edit
                          </Text>
                        </TouchableOpacity>
                        <View style={{ width: Width(8) }} />
                        <TouchableOpacity
                          onPress={() => {
                            this.currentitemindex = index
                            this.setState({ plan_id: item.plan_id })
                            if (item.plan_type == 1) {
                              //this.eventtitle = name[0]+", let's meet up!"
                              this.eventtitle = "Let's meet up!"
                            } else if (item.plan_type == 2) {
                              this.eventtitle = item.activity
                            } else if (item.plan_type == 3) {
                              this.eventtitle =
                                'Wanna go to ' + item.line_1 + '?'
                            } else if (item.plan_type == 4) {
                              this.eventtitle = "I'm free, wanna hangout?"
                            }
                            Alert.alert(
                              'Cancel Event',
                              'Guests will be notified that this event was canceled',
                              [
                                {
                                  text: 'Cancel',
                                  onPress: () => console.log('Cancel Pressed'),
                                  style: 'cancel',
                                },
                                { text: 'OK', onPress: this.deleteplan },
                              ],
                              { cancelable: false },
                            )
                          }}
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                        >
                          <Image source={Delete} />
                          <Text
                            style={{
                              fontFamily: fonts.Raleway_Regular,
                              color: colors.dargrey,
                              marginLeft: Width(1.5),
                              fontSize: FontSize(16),
                            }}
                          >
                            Cancel
                          </Text>
                        </TouchableOpacity>
                        <View style={{ width: Width(8) }} />
                        <TouchableOpacity
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                          onPress={() => this.goToShare(item)}
                        >
                          <Image source={share} />
                          <Text
                            style={{
                              fontFamily: fonts.Raleway_Regular,
                              color: colors.dargrey,
                              marginLeft: Width(1.5),
                              fontSize: FontSize(16),
                            }}
                          >
                            Share
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                ) : null}
              </View>
            </View>
            <View style={{ height: Height(1) }} />
          </View>
        ) : null}

        {item.plan_type == 1 &&
        this.state.Peoples.length !== 0 &&
        index == 0 &&
        this.randomnumber % 2 == 0 ? (
          <View
            style={{
              height: Height(29),
              backgroundColor: '#F6F5FB',
              marginTop: Height(0.7),
            }}
          >
            <Text
              style={{
                marginTop: Height(2),
                marginLeft: Width(4.5),
                color: colors.appColor,
                fontFamily: fonts.Raleway_Bold,
                fontSize: FontSize(18),
              }}
            >
              People You May Know
            </Text>
            <View
              style={{
                marginTop: '2%',
                marginLeft: Width(0),
                marginRight: Width(1),
                flexDirection: 'row',
              }}
            >
              <ScrollView
                showsHorizontalScrollIndicator={false}
                horizontal
                contentContainerStyle={{ paddingHorizontal: Width(1) }}
              >
                {this.state.Peoples.map((item, index) => {
                  return (
                    <View
                      style={{
                        backgroundColor: colors.white,
                        height:
                          Platform.OS == 'android' ? Height(20) : Height(19.3),
                        width: Width(32.5),
                        borderRadius: Height(2),
                        alignItems: 'center',
                        marginTop: Height(2),
                        marginHorizontal: Width(1),
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => this.onPressUserName(item)}
                      >
                        <View
                          style={{
                            height: Height(7),
                            width: Height(7),
                            borderRadius: Height(7),
                            marginTop: Height(2),
                            overflow: 'hidden',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}
                        >
                          <Image
                            style={{ height: '100%', width: '100%' }}
                            resizeMode="stretch"
                            source={{ uri: item.photo }}
                          />
                        </View>
                      </TouchableOpacity>
                      <Text
                        style={{
                          marginTop: Height(1.5),
                          fontSize: FontSize(13),
                          color: colors.dargrey,
                          fontFamily: fonts.Roboto_Medium,
                          marginHorizontal: 5,
                        }}
                        numberOfLines={1}
                        onPress={() => this.onPressUserName(item)}
                      >
                        {item.name}
                      </Text>

                      {item.is_friend == 2 ? (
                        // <TouchableOpacity style={styles.inviteBox1} onPress = { () => this.cancelFriendRequestAPICall(item,index)}>
                        <TouchableOpacity style={styles.inviteBox1}>
                          <Text
                            style={[
                              {
                                fontSize: FontSize(13),
                                fontFamily: fonts.Raleway_Bold,
                                color: '#ffffff',
                                textAlign: 'center',
                              },
                              checkFontWeight('700'),
                            ]}
                          >
                            Pending
                          </Text>
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity
                          style={styles.inviteBox}
                          onPress={() => this.addFriendAPICall(item, index)}
                        >
                          <Text
                            style={[
                              {
                                fontSize: FontSize(13),
                                fontFamily: fonts.Raleway_Bold,
                                color: '#ffffff',
                                textAlign: 'center',
                              },
                              checkFontWeight('700'),
                            ]}
                          >
                            Add Friend
                          </Text>
                        </TouchableOpacity>
                      )}
                    </View>
                  )
                })}
              </ScrollView>
            </View>
          </View>
        ) : null}

        {item.plan_type == 1 &&
        this.state.Contacts.length > 0 &&
        this.state.Contacts != null &&
        index == 0 &&
        this.randomnumber % 2 != 0 ? (
          <View
            style={{
              height: Height(29),
              backgroundColor: '#F6F5FB',
              marginTop: Height(0.7),
            }}
          >
            <Text
              style={{
                marginTop: Height(2),
                marginLeft: Width(4.5),
                color: colors.appColor,
                fontFamily: fonts.Raleway_Bold,
                fontSize: FontSize(18),
              }}
            >
              Your Contacts
            </Text>
            <View
              style={{
                marginTop: '2%',
                marginLeft: Width(0),
                marginRight: Width(1),
                flexDirection: 'row',
              }}
            >
              <ScrollView
                showsHorizontalScrollIndicator={false}
                horizontal
                contentContainerStyle={{ paddingHorizontal: Width(1) }}
              >
                {this.state.Contacts.map((item) => {
                  return (
                    <View
                      style={{
                        backgroundColor: colors.white,
                        height:
                          Platform.OS == 'android' ? Height(20) : Height(19.3),
                        width: Width(33),
                        borderRadius: Height(2),
                        alignItems: 'center',
                        marginTop: Height(2),
                        marginHorizontal: Width(1),
                        justifyContent: 'center',
                      }}
                    >
                      <View
                        style={{
                          width: Height(7),
                          height: Height(7),
                          borderRadius: Height(7),
                          backgroundColor: '#D6D6D6',
                          justifyContent: 'center',
                        }}
                      >
                        <Text
                          style={[
                            {
                              textAlign: 'center',
                              fontFamily: 15,
                              fontFamily: 'Raleway-ExtraBold',
                              letterSpacing: 0.02,
                            },
                            checkFontWeight('800'),
                          ]}
                        >
                          {item.initial}
                        </Text>
                      </View>
                      <Text
                        style={{
                          marginTop: Height(1.5),
                          fontSize: FontSize(13),
                          color: colors.dargrey,
                          fontFamily: fonts.Roboto_Medium,
                          marginHorizontal: 5,
                        }}
                        numberOfLines={1}
                      >
                        {item.title}
                      </Text>
                      {/* <Text style={{ marginTop: Height(0.5), fontSize: FontSize(13), color: colors.dargrey, fontFamily: fonts.Roboto_Regular }}>{item.phoneNumbers && item.phoneNumbers[0].number}</Text> */}
                      <TouchableOpacity
                        style={styles.inviteBox}
                        onPress={() =>
                          this.inviteContacts(
                            item.phoneNumbers && item.phoneNumbers[0].number,
                          )
                        }
                      >
                        <Text
                          style={[
                            {
                              fontSize: FontSize(13),
                              fontFamily: fonts.Raleway_Bold,
                              color: '#ffffff',
                              textAlign: 'center',
                            },
                            checkFontWeight('700'),
                          ]}
                        >
                          Invite
                        </Text>
                      </TouchableOpacity>
                    </View>
                  )
                })}
              </ScrollView>
            </View>
          </View>
        ) : null}

        {item.plan_type == 2 && item.is_hide == 0 ? (
          <View
            style={{ marginTop: Height(0.7), backgroundColor: colors.white }}
          >
            {/* <TouchableOpacity onPress={() => {
            this.props.navigation.navigate('EventDetail', { item: item , plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header:item.activity , Final_date : Final_date, eventby:item.name})
             }} style={{ height: Height(9), flexDirection: 'row', alignItems: 'center', backgroundColor: 'white' }}> */}

            <TouchableOpacity
              style={{
                height: Height(9),
                flexDirection: 'row',
                alignItems: 'center',
                backgroundColor: 'white',
              }}
              activeOpacity={0.9}
            >
              <View style={{ alignItems: 'center', marginLeft: Width(4.5) }}>
                <TouchableOpacity
                  onPress={
                    item.login_id == this.state.login_id
                      ? () => this.props.navigation.navigate('myprofile')
                      : () => {
                          AsyncStorage.setItem('other_id', item.login_id)
                          this.props.navigation.navigate('ohter_user_profile')
                        }
                  }
                  underlayColor="transparent"
                  style={{
                    height: Height(5),
                    width: Height(5),
                    borderRadius: Height(7),
                    overflow: 'hidden',
                  }}
                >
                  {item.photo == '' ? (
                    <Image
                      source={avtar}
                      style={{ height: '100%', width: '100%' }}
                    />
                  ) : (
                    <Image
                      style={{ height: '100%', width: '100%' }}
                      source={{ uri: item.photo }}
                    />
                  )}
                </TouchableOpacity>
              </View>
              <View style={{ justifyContent: 'center', marginLeft: Width(3) }}>
                <Text
                  onPress={
                    item.login_id == this.state.login_id
                      ? () => this.props.navigation.navigate('myprofile')
                      : () => {
                          AsyncStorage.setItem('other_id', item.login_id)
                          this.props.navigation.navigate('ohter_user_profile')
                        }
                  }
                  style={{
                    fontSize: FontSize(18),
                    color: colors.dargrey,
                    fontFamily: fonts.Raleway_Bold,
                  }}
                >
                  {item.login_id == this.state.login_id ? 'Me' : item.name}
                </Text>
                <Text
                  style={{ fontSize: FontSize(12), color: colors.fontDarkGrey }}
                >
                  {dateTime}
                  {item.share_with == '' ? null : '\u00B7 '}
                  {item.share_with}
                </Text>
              </View>
              <View
                style={{
                  alignItems: 'flex-end',
                  marginRight: Width(4),
                  flex: 4,
                }}
              >
                <TouchableOpacity
                  onPress={() => this.modelVisible(item, index)}
                  underlayColor="transparent"
                  style={{
                    justifyContent: 'center',
                    height: Height(6),
                    width: Width(6),
                  }}
                >
                  <Entypo
                    size={Width(6)}
                    name="dots-three-horizontal"
                    color="#000"
                  />
                </TouchableOpacity>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.9}
              onPress={() => {
                this.props.navigation.navigate('EventDetail', {
                  item: item,
                  plan_id: item.plan_id,
                  other_id: item.login_id,
                  plan_type: item.plan_type,
                  title_header: item.activity,
                  Final_date: Final_date,
                  eventby: item.name,
                })
              }}
            >
              <ImageBackground
                source={
                  item.plan_image == '' ||
                  item.plan_image == null ||
                  item.plan_image == undefined
                    ? default_placeholder
                    : { uri: item.plan_image }
                }
                style={{
                  height: 265,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                {/* <Text style={{ fontSize: FontSize(20), color: colors.dargrey, fontFamily: fonts.Raleway_Semibold, marginHorizontal:48, lineHeight:27,textAlign:'center'}}>{item.activity} </Text>
              {item.description == '' ? null : <Text style={{ textAlign: 'center', fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, width: '85%', marginTop: 2, alignSelf: 'center' }}>{item.description}</Text>} */}
              </ImageBackground>
            </TouchableOpacity>
            <View>
              <View style={{ paddingHorizontal: Width(4.5) }}>
                <Text
                  style={{
                    fontSize: FontSize(19),
                    color: colors.dargrey,
                    fontFamily: fonts.Raleway_Bold,
                    lineHeight: 27,
                    textAlign: 'left',
                    marginTop: 10,
                  }}
                >
                  {item.activity}
                </Text>
                {/* {item.description == '' ? null : <Text style={{ textAlign: 'left', fontSize: FontSize(13), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, marginTop: 4,marginHorizontal:Width(4.5)}}>{item.description}</Text>} */}
                {item.description == '' ? null : (
                  <SeeMore
                    style={{
                      textAlign: 'left',
                      fontSize: FontSize(13),
                      color: colors.dargrey,
                      fontFamily: fonts.Roboto_Regular,
                      marginTop: 4,
                    }}
                    numberOfLines={2}
                  >
                    {item.description}
                  </SeeMore>
                )}
              </View>
            </View>
            <View
              style={{
                backgroundColor: '#FFF',
                alignItems: 'center',
                flexDirection: 'row',
              }}
            >
              <View style={{ flex: 1, marginLeft: Width(4.5) }}>
                <TouchableOpacity
                  style={{ flexDirection: 'row', marginTop: Height(2) }}
                >
                  <Image
                    resizeMode="contain"
                    style={{
                      alignSelf: 'center',
                      tintColor: colors.whatFontColor,
                    }}
                    source={chai_cup}
                  />
                  {item.activity == '' ? (
                    <Text
                      onPress={
                        item.total_activity_suggestions == 0
                          ? () =>
                              this.props.navigation.navigate('Activity', {
                                plan_id: item.plan_id,
                                is_direct: true,
                                title_header: item.activity,
                              })
                          : () =>
                              this.props.navigation.navigate(
                                'SuggestActivity',
                                {
                                  other_id: item.login_id,
                                  plan_id: item.plan_id,
                                  is_direct: false,
                                  title_header: item.activity,
                                },
                              )
                      }
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: '#358AF6',
                      }}
                    ></Text>
                  ) : (
                    <Text
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: colors.dargrey,
                      }}
                    >
                      {activity}
                    </Text>
                  )}
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    marginTop: Height(2),
                    marginBottom: Height(2),
                  }}
                >
                  <Image
                    resizeMode="contain"
                    style={{ alignSelf: 'center' }}
                    source={calendar}
                  />
                  {item.start_date == '' ? (
                    <Text
                      onPress={
                        item.total_datetime_suggestions == 0
                          ? () =>
                              this.props.navigation.navigate('DateTime', {
                                plan_id: item.plan_id,
                                is_direct: true,
                              })
                          : () =>
                              this.props.navigation.navigate(
                                'SuggestDateTime',
                                {
                                  other_id: item.login_id,
                                  plan_id: item.plan_id,
                                  is_direct: false,
                                },
                              )
                      }
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: '#358AF6',
                      }}
                    >
                      {item.total_datetime_suggestions == 0
                        ? 'Suggest a date & time'
                        : 'View date & time suggestions'}
                    </Text>
                  ) : (
                    <Text
                      style={{
                        width: Width(60),
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: colors.dargrey,
                      }}
                    >
                      {Final_date}
                    </Text>
                  )}
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ flexDirection: 'row', marginBottom: Height(2) }}
                >
                  <Image
                    resizeMode="contain"
                    style={{
                      alignSelf: 'center',
                      tintColor: colors.whatFontColor,
                    }}
                    source={location}
                  />
                  {item.line_1 == '' ? (
                    <Text
                      onPress={
                        item.total_location_suggestions == 0
                          ? () =>
                              this.props.navigation.navigate('Location', {
                                plan_id: item.plan_id,
                                is_direct: true,
                              })
                          : () =>
                              this.props.navigation.navigate(
                                'SuggestLocation',
                                {
                                  other_id: item.login_id,
                                  plan_id: item.plan_id,
                                  is_direct: false,
                                },
                              )
                      }
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: '#358AF6',
                      }}
                    >
                      {item.total_location_suggestions == 0
                        ? 'Suggest a location'
                        : 'View location suggestions'}
                    </Text>
                  ) : (
                    <Text
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: colors.dargrey,
                      }}
                    >
                      {item.line_1}
                    </Text>
                  )}
                </TouchableOpacity>

                <View
                  style={{
                    height: Height(0.15),
                    backgroundColor: '#EFEFF4',
                    marginRight: Width(4),
                  }}
                />

                {item.is_going_count != 0 ||
                item.is_interested_count != 0 ||
                item.is_invited_count != 0 ? (
                  <View
                    style={{ flexDirection: 'row', marginBottom: Height(1.5) }}
                  >
                    {/* {item.is_going_count != 0 || item.is_interested_count != 0 || item.is_invited_count != 0 ?
                  <Image resizeMode="contain" style={{ marginTop: Height(2), alignSelf: 'center', tintColor: colors.whatFontColor }} source={new_user} /> : null} */}
                    {item.is_going_count != 0 ? (
                      <Text
                        onPress={() => {
                          this.props.navigation.navigate('Friend_Status', {
                            plan_id: item.plan_id,
                            tabIndex: 0,
                            Title_New: item.activity,
                            plan_type: item.plan_type,
                          })
                        }}
                        style={{
                          marginTop: Height(1.5),
                          paddingLeft: Width(0),
                          fontFamily: fonts.Roboto_Regular,
                          fontSize: FontSize(12),
                          color: colors.dargrey,
                        }}
                      >
                        {item.is_going_count + ' going'}
                      </Text>
                    ) : null}
                    {item.is_interested_count != 0 ? (
                      <Text
                        onPress={() => {
                          this.props.navigation.navigate('Friend_Status', {
                            plan_id: item.plan_id,
                            tabIndex: 1,
                            Title_New: item.activity,
                            plan_type: item.plan_type,
                          })
                        }}
                        style={
                          item.is_going_count == 0
                            ? {
                                marginTop: Height(1.5),
                                paddingLeft: Width(0),
                                fontFamily: fonts.Roboto_Regular,
                                fontSize: FontSize(12),
                                color: colors.dargrey,
                              }
                            : {
                                marginTop: Height(1.5),
                                paddingLeft: Width(1),
                                fontFamily: fonts.Roboto_Regular,
                                fontSize: FontSize(12),
                                color: colors.dargrey,
                              }
                        }
                      >
                        {item.is_going_count != 0 ? '\u00B7 ' : null}
                        {item.is_interested_count + ' interested'}
                      </Text>
                    ) : null}
                    {item.is_invited_count != 0 ? (
                      <Text
                        onPress={() => {
                          this.props.navigation.navigate('Friend_Status', {
                            plan_id: item.plan_id,
                            tabIndex: 2,
                            Title_New: item.activity,
                            plan_type: item.plan_type,
                          })
                        }}
                        style={
                          item.is_going_count == 0 &&
                          item.is_interested_count == 0
                            ? {
                                marginTop: Height(1.5),
                                paddingLeft: Width(0),
                                fontFamily: fonts.Roboto_Regular,
                                fontSize: FontSize(12),
                                color: colors.dargrey,
                              }
                            : {
                                marginTop: Height(1.5),
                                paddingLeft: Width(1),
                                fontFamily: fonts.Roboto_Regular,
                                fontSize: FontSize(12),
                                color: colors.dargrey,
                              }
                        }
                      >
                        {item.is_interested_count != 0 ||
                        item.is_going_count != 0
                          ? '\u00B7 '
                          : null}
                        {item.is_invited_count + ' invited'}
                      </Text>
                    ) : null}
                    <TouchableOpacity
                      style={{
                        marginTop: Height(1.5),
                        position: 'absolute',
                        right: Width(4),
                        width: 35,
                        height: 35,
                      }}
                      onPress={() => {
                        this.props.navigation.navigate('EventDetail', {
                          item: item,
                          plan_id: item.plan_id,
                          other_id: item.login_id,
                          plan_type: item.plan_type,
                          title_header: item.activity,
                          Final_date: Final_date,
                          eventby: item.name,
                        })
                      }}
                    >
                      <Image resizeMode="contain" source={ic_chat} />
                    </TouchableOpacity>
                  </View>
                ) : (
                  <View>
                    {item.login_id != this.state.login_id ? (
                      <Text
                        style={{
                          marginTop: Height(1.5),
                          marginBottom: Height(1.5),
                          paddingLeft: Width(0),
                          fontFamily: fonts.Roboto_Regular,
                          fontSize: FontSize(12),
                          color: colors.dargrey,
                        }}
                      >
                        Be the first guest
                      </Text>
                    ) : (
                      <Text
                        style={{
                          marginTop: Height(1.5),
                          marginBottom: Height(1.5),
                          paddingLeft: Width(0),
                          fontFamily: fonts.Roboto_Regular,
                          fontSize: FontSize(12),
                          color: colors.dargrey,
                        }}
                      >
                        No guests yet
                      </Text>
                    )}

                    <TouchableOpacity
                      style={{
                        marginTop: Height(1.5),
                        marginBottom: Height(1.5),
                        position: 'absolute',
                        right: Width(4),
                        width: 35,
                        height: 35,
                      }}
                      onPress={() => {
                        this.props.navigation.navigate('EventDetail', {
                          item: item,
                          plan_id: item.plan_id,
                          other_id: item.login_id,
                          plan_type: item.plan_type,
                          title_header: item.activity,
                          Final_date: Final_date,
                          eventby: item.name,
                        })
                      }}
                    >
                      <Image resizeMode="contain" source={ic_chat} />
                    </TouchableOpacity>
                  </View>
                )}

                {item.login_id != this.state.login_id ? (
                  <View>
                    <View
                      style={{
                        height: Height(0.15),
                        backgroundColor: '#EFEFF4',
                        marginRight: Width(4),
                      }}
                    />

                    <View
                      style={{
                        flexDirection: 'row',
                        alignSelf: 'center',
                        justifyContent: 'center',
                        marginBottom: Height(1),
                        marginTop: Height(2),
                      }}
                    >
                      {/* {(item.is_going == 0 && item.is_interested == 1 && (item.activity != '' && item.line_1 != '' && item.start_date != ''))? <View style={{ width: Width(8) }} /> : null} */}
                      {/* {(item.is_going == 1 && item.is_interested == 0 && (item.activity != '' && item.line_1 != '' && item.start_date != '')) ? */}

                      {item.activity != '' || item.line_1 != '' ? (
                        <View style={{ flexDirection: 'row' }}>
                          {item.is_going == 1 ? (
                            <TouchableOpacity
                              onPress={() => this.goToNoGoing(item, index)}
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                              }}
                            >
                              <Image source={going1} />
                              <Text
                                style={{
                                  color: colors.pink,
                                  fontFamily: fonts.Roboto_Regular,
                                  marginLeft: Width(1),
                                  fontSize: FontSize(16),
                                }}
                              >
                                Going
                              </Text>
                            </TouchableOpacity>
                          ) : (
                            <TouchableOpacity
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                              }}
                              onPress={() => this.goToGoing(item, index)}
                            >
                              <Image source={going} />
                              <Text
                                style={{
                                  fontFamily: fonts.Raleway_Regular,
                                  color: colors.dargrey,
                                  marginLeft: Width(1),
                                  fontSize: FontSize(16),
                                }}
                              >
                                Going
                              </Text>
                            </TouchableOpacity>
                          )}
                        </View>
                      ) : null}
                      {item.activity != '' || item.line_1 != '' ? (
                        <View style={{ width: Width(11) }} />
                      ) : null}
                      {item.is_interested == 0 ? (
                        <View
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                        >
                          <TouchableOpacity
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                            }}
                            onPress={() => this.goToIntersted(item, index)}
                          >
                            <Image source={black_heart} />
                            <Text
                              style={{
                                fontFamily: fonts.Raleway_Regular,
                                color: colors.dargrey,
                                marginLeft: Width(1),
                                fontSize: FontSize(16),
                              }}
                            >
                              Interested
                            </Text>
                          </TouchableOpacity>
                        </View>
                      ) : (
                        <TouchableOpacity
                          onPress={() => this.goToNoIntersted(item, index)}
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                        >
                          <Image source={circle_favorite} />
                          <Text
                            style={{
                              marginLeft: Width(1),
                              color: colors.pink,
                              fontFamily: fonts.Roboto_Regular,
                              fontSize: FontSize(16),
                            }}
                          >
                            Interested
                          </Text>
                        </TouchableOpacity>
                      )}
                      <View style={{ width: Width(11) }} />
                      <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity
                          onPress={() => this.goToShare(item)}
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                        >
                          <Image source={share} />
                          <Text
                            style={{
                              color: colors.dargrey,
                              fontFamily: fonts.Raleway_Regular,
                              marginLeft: Width(1),
                              fontSize: FontSize(16),
                            }}
                          >
                            Share
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                ) : null}

                {item.login_id == this.state.login_id ? (
                  <View style={{ backgroundColor: 'white' }}>
                    <View
                      style={{
                        height: Height(0.15),
                        backgroundColor: '#EFEFF4',
                        marginRight: Width(4),
                      }}
                    />
                    <View
                      style={{
                        flexDirection: 'row',
                        alignSelf: 'center',
                        justifyContent: 'center',
                        marginBottom: Height(1),
                        marginTop: Height(2),
                      }}
                    >
                      <View
                        style={{
                          flexDirection: 'row',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}
                      >
                        <TouchableOpacity
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                          onPress={() => this.gotoEditEvent(item)}
                        >
                          <Image source={Edit} />
                          <Text
                            style={{
                              fontFamily: fonts.Raleway_Regular,
                              color: colors.dargrey,
                              marginLeft: Width(1.5),
                              fontSize: FontSize(16),
                            }}
                          >
                            Edit
                          </Text>
                        </TouchableOpacity>
                        <View style={{ width: Width(8) }} />
                        <TouchableOpacity
                          onPress={() => {
                            this.currentitemindex = index
                            this.setState({ plan_id: item.plan_id })
                            if (item.plan_type == 1) {
                              //this.eventtitle = name[0]+", let's meet up!"
                              this.eventtitle = "Let's meet up!"
                            } else if (item.plan_type == 2) {
                              this.eventtitle = item.activity
                            } else if (item.plan_type == 3) {
                              this.eventtitle =
                                'Wanna go to ' + item.line_1 + '?'
                            } else if (item.plan_type == 4) {
                              this.eventtitle = "I'm free, wanna hangout?"
                            }
                            Alert.alert(
                              'Cancel Event',
                              'Guests will be notified that this event was canceled',
                              [
                                {
                                  text: 'Cancel',
                                  onPress: () => console.log('Cancel Pressed'),
                                  style: 'cancel',
                                },
                                { text: 'OK', onPress: this.deleteplan },
                              ],
                              { cancelable: false },
                            )
                          }}
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                        >
                          <Image source={Delete} />
                          <Text
                            style={{
                              fontFamily: fonts.Raleway_Regular,
                              color: colors.dargrey,
                              marginLeft: Width(1.5),
                              fontSize: FontSize(16),
                            }}
                          >
                            Cancel
                          </Text>
                        </TouchableOpacity>
                        <View style={{ width: Width(8) }} />
                        <TouchableOpacity
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                          onPress={() => this.goToShare(item)}
                        >
                          <Image source={share} />
                          <Text
                            style={{
                              fontFamily: fonts.Raleway_Regular,
                              color: colors.dargrey,
                              marginLeft: Width(1.5),
                              fontSize: FontSize(16),
                            }}
                          >
                            Share
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                ) : null}
              </View>
            </View>

            <View style={{ height: Height(1) }} />
          </View>
        ) : null}

        {item.plan_type == 2 &&
        item.is_hide == 0 &&
        this.state.Peoples.length !== 0 &&
        index == 0 &&
        this.randomnumber % 2 == 0 ? (
          <View
            style={{
              height: Height(29),
              backgroundColor: '#F6F5FB',
              marginTop: Height(0.7),
            }}
          >
            <Text
              style={{
                marginTop: Height(2),
                marginLeft: Width(4.5),
                color: colors.appColor,
                fontFamily: fonts.Raleway_Bold,
                fontSize: FontSize(18),
              }}
            >
              People You May Know
            </Text>
            <View
              style={{
                marginTop: '2%',
                marginLeft: Width(0),
                marginRight: Width(1),
                flexDirection: 'row',
              }}
            >
              <ScrollView
                showsHorizontalScrollIndicator={false}
                horizontal
                contentContainerStyle={{ paddingHorizontal: Width(1) }}
              >
                {this.state.Peoples.map((item, index) => {
                  return (
                    <View
                      style={{
                        backgroundColor: colors.white,
                        height:
                          Platform.OS == 'android' ? Height(20) : Height(19.3),
                        width: Width(32.5),
                        borderRadius: Height(2),
                        alignItems: 'center',
                        marginTop: Height(2),
                        marginHorizontal: Width(1),
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => this.onPressUserName(item)}
                      >
                        <View
                          style={{
                            height: Height(7),
                            width: Height(7),
                            borderRadius: Height(7),
                            marginTop: Height(2),
                            overflow: 'hidden',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}
                        >
                          <Image
                            style={{ height: '100%', width: '100%' }}
                            resizeMode="stretch"
                            source={{ uri: item.photo }}
                          />
                        </View>
                      </TouchableOpacity>
                      <Text
                        style={{
                          marginTop: Height(1.5),
                          fontSize: FontSize(13),
                          color: colors.dargrey,
                          fontFamily: fonts.Roboto_Medium,
                          marginHorizontal: 5,
                        }}
                        numberOfLines={1}
                        onPress={() => this.onPressUserName(item)}
                      >
                        {item.name}
                      </Text>
                      {item.is_friend == 2 ? (
                        // <TouchableOpacity style={styles.inviteBox1} onPress = { () => this.cancelFriendRequestAPICall(item,index)}>
                        <TouchableOpacity style={styles.inviteBox1}>
                          <Text
                            style={[
                              {
                                fontSize: FontSize(13),
                                fontFamily: fonts.Raleway_Bold,
                                color: '#ffffff',
                                textAlign: 'center',
                              },
                              checkFontWeight('700'),
                            ]}
                          >
                            Pending
                          </Text>
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity
                          style={styles.inviteBox}
                          onPress={() => this.addFriendAPICall(item, index)}
                        >
                          <Text
                            style={[
                              {
                                fontSize: FontSize(13),
                                fontFamily: fonts.Raleway_Bold,
                                color: '#ffffff',
                                textAlign: 'center',
                              },
                              checkFontWeight('700'),
                            ]}
                          >
                            Add Friend
                          </Text>
                        </TouchableOpacity>
                      )}
                    </View>
                  )
                })}
              </ScrollView>
            </View>
          </View>
        ) : null}

        {item.plan_type == 2 &&
        this.state.Contacts.length > 0 &&
        this.state.Contacts != null &&
        index == 0 &&
        this.randomnumber % 2 != 0 ? (
          <View
            style={{
              height: Height(29),
              backgroundColor: '#F6F5FB',
              marginTop: Height(0.7),
            }}
          >
            <Text
              style={{
                marginTop: Height(2),
                marginLeft: Width(4.5),
                color: colors.appColor,
                fontFamily: fonts.Raleway_Bold,
                fontSize: FontSize(18),
              }}
            >
              Your Contacts
            </Text>
            <View
              style={{
                marginTop: '2%',
                marginLeft: Width(0),
                marginRight: Width(1),
                flexDirection: 'row',
              }}
            >
              <ScrollView
                showsHorizontalScrollIndicator={false}
                horizontal
                contentContainerStyle={{ paddingHorizontal: Width(1) }}
              >
                {this.state.Contacts.map((item) => {
                  return (
                    <View
                      style={{
                        backgroundColor: colors.white,
                        height:
                          Platform.OS == 'android' ? Height(20) : Height(19.3),
                        width: Width(33),
                        borderRadius: Height(2),
                        alignItems: 'center',
                        marginTop: Height(2),
                        marginHorizontal: Width(1),
                        justifyContent: 'center',
                      }}
                    >
                      <View
                        style={{
                          width: Height(7),
                          height: Height(7),
                          borderRadius: Height(7),
                          backgroundColor: '#D6D6D6',
                          justifyContent: 'center',
                        }}
                      >
                        <Text
                          style={[
                            {
                              textAlign: 'center',
                              fontFamily: 15,
                              fontFamily: 'Raleway-ExtraBold',
                              letterSpacing: 0.02,
                            },
                            checkFontWeight('800'),
                          ]}
                        >
                          {item.initial}
                        </Text>
                      </View>
                      <Text
                        style={{
                          marginTop: Height(1.5),
                          fontSize: FontSize(13),
                          color: colors.dargrey,
                          fontFamily: fonts.Roboto_Medium,
                          marginHorizontal: 5,
                        }}
                        numberOfLines={1}
                      >
                        {item.title}
                      </Text>
                      {/* <Text style={{ marginTop: Height(0.5), fontSize: FontSize(13), color: colors.dargrey, fontFamily: fonts.Roboto_Regular }}>{item.phoneNumbers && item.phoneNumbers[0].number}</Text> */}
                      <TouchableOpacity
                        style={styles.inviteBox}
                        onPress={() =>
                          this.inviteContacts(
                            item.phoneNumbers && item.phoneNumbers[0].number,
                          )
                        }
                      >
                        <Text
                          style={[
                            {
                              fontSize: FontSize(13),
                              fontFamily: fonts.Raleway_Bold,
                              color: '#ffffff',
                              textAlign: 'center',
                            },
                            checkFontWeight('700'),
                          ]}
                        >
                          Invite
                        </Text>
                      </TouchableOpacity>
                    </View>
                  )
                })}
              </ScrollView>
            </View>
          </View>
        ) : null}

        {item.plan_type == 3 && item.is_hide == 0 ? (
          <View
            style={{ marginTop: Height(0.7), backgroundColor: colors.white }}
          >
            {/* <TouchableOpacity onPress={() => { this.props.navigation.navigate('EventDetail', { item: item , plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type , title_header:"Wanna go to " +item.line_1+"?" , Final_date : Final_date, eventby:item.name}) }} style={{ height: Height(9), flexDirection: 'row', alignItems: 'center', backgroundColor: 'white' }}> */}
            <TouchableOpacity
              style={{
                height: Height(9),
                flexDirection: 'row',
                alignItems: 'center',
                backgroundColor: 'white',
              }}
              activeOpacity={0.9}
            >
              <View style={{ alignItems: 'center', marginLeft: Width(4.5) }}>
                <TouchableOpacity
                  onPress={
                    item.login_id == this.state.login_id
                      ? () => this.props.navigation.navigate('myprofile')
                      : () => {
                          AsyncStorage.setItem('other_id', item.login_id)
                          this.props.navigation.navigate('ohter_user_profile')
                        }
                  }
                  underlayColor="transparent"
                  style={{
                    height: Height(5),
                    width: Height(5),
                    borderRadius: Height(7),
                    overflow: 'hidden',
                  }}
                >
                  {item.photo == '' ? (
                    <Image
                      source={avtar}
                      style={{ height: '100%', width: '100%' }}
                    />
                  ) : (
                    <Image
                      style={{ height: '100%', width: '100%' }}
                      source={{ uri: item.photo }}
                    />
                  )}
                </TouchableOpacity>
              </View>
              <View style={{ justifyContent: 'center', marginLeft: Width(3) }}>
                <Text
                  onPress={
                    item.login_id == this.state.login_id
                      ? () => this.props.navigation.navigate('myprofile')
                      : () => {
                          AsyncStorage.setItem('other_id', item.login_id)
                          this.props.navigation.navigate('ohter_user_profile')
                        }
                  }
                  style={{
                    fontSize: FontSize(18),
                    color: colors.dargrey,
                    fontFamily: fonts.Raleway_Bold,
                  }}
                >
                  {item.login_id == this.state.login_id ? 'Me' : item.name}
                </Text>
                <Text
                  style={{
                    fontSize: FontSize(12),
                    color: colors.fontDarkGrey,
                    fontFamily: fonts.Roboto_Regular,
                  }}
                >
                  {dateTime}
                  {item.share_with == '' ? null : '\u00B7 '}
                  {item.share_with}
                </Text>
              </View>
              <View
                style={{
                  alignItems: 'flex-end',
                  marginRight: Width(4),
                  flex: 4,
                }}
              >
                <TouchableOpacity
                  onPress={() => this.modelVisible(item, index)}
                  underlayColor="transparent"
                  style={{
                    justifyContent: 'center',
                    height: Height(6),
                    width: Width(6),
                  }}
                >
                  <Entypo
                    size={Width(6)}
                    name="dots-three-horizontal"
                    color="#000"
                  />
                </TouchableOpacity>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.9}
              onPress={() => {
                this.props.navigation.navigate('EventDetail', {
                  item: item,
                  plan_id: item.plan_id,
                  other_id: item.login_id,
                  plan_type: item.plan_type,
                  title_header: 'Wanna go to ' + item.line_1 + '?',
                  Final_date: Final_date,
                  eventby: item.name,
                })
              }}
            >
              <ImageBackground
                source={
                  item.plan_image == '' ||
                  item.plan_image == null ||
                  item.plan_image == undefined
                    ? default_placeholder
                    : { uri: item.plan_image }
                }
                style={{
                  height: 265,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                {/* <Text style={{ textAlign: 'center', fontSize: FontSize(20), color: colors.dargrey, fontFamily: fonts.Raleway_Semibold, marginHorizontal:48, lineHeight:27,textAlign:'center' }}>Wanna go to {item.line_1}?</Text>
              {item.description == '' ? null : <Text style={{ textAlign: 'center', fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, width: '85%', marginTop: 2, alignSelf: 'center' }}>{item.description}</Text>} */}
              </ImageBackground>
            </TouchableOpacity>

            <View style={{ paddingHorizontal: Width(4.5) }}>
              <Text
                style={{
                  fontSize: FontSize(19),
                  color: colors.dargrey,
                  fontFamily: fonts.Raleway_Bold,
                  lineHeight: 27,
                  textAlign: 'left',
                  marginTop: 10,
                }}
              >
                Wanna go to {item.line_1}?
              </Text>
              {/* {item.description == '' ? null : <Text style={{ textAlign: 'left', fontSize: FontSize(13), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, marginTop: 4,marginHorizontal:Width(4.5)}}>{item.description}</Text>} */}
              {item.description == '' ? null : (
                <SeeMore
                  style={{
                    textAlign: 'left',
                    fontSize: FontSize(13),
                    color: colors.dargrey,
                    fontFamily: fonts.Roboto_Regular,
                    marginTop: 4,
                  }}
                  numberOfLines={2}
                  seeMoreText="more"
                >
                  {item.description}
                </SeeMore>
              )}
            </View>

            <View
              style={{
                backgroundColor: '#FFF',
                alignItems: 'center',
                flexDirection: 'row',
              }}
            >
              <View style={{ flex: 1, marginLeft: Width(4.5) }}>
                <TouchableOpacity
                  style={{ flexDirection: 'row', marginTop: Height(2) }}
                >
                  <Image
                    resizeMode="contain"
                    style={{
                      alignSelf: 'center',
                      tintColor: colors.whatFontColor,
                    }}
                    source={chai_cup}
                  />
                  {item.activity == '' ? (
                    <Text
                      onPress={
                        item.total_activity_suggestions == 0
                          ? () =>
                              this.props.navigation.navigate('Activity', {
                                plan_id: item.plan_id,
                                other_id: item.login_id,
                                is_direct: true,
                                title_header:
                                  'Wanna go to ' + item.line_1 + '?',
                              })
                          : () =>
                              this.props.navigation.navigate(
                                'SuggestActivity',
                                {
                                  other_id: item.login_id,
                                  plan_id: item.plan_id,
                                  is_direct: false,
                                  title_header:
                                    'Wanna go to ' + item.line_1 + '?',
                                },
                              )
                      }
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: '#358AF6',
                        marginRight: Width(5),
                      }}
                    >
                      {item.total_activity_suggestions == 0
                        ? 'Suggest an activity'
                        : 'View activity suggestions'}
                    </Text>
                  ) : (
                    <Text
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: colors.dargrey,
                        marginRight: Width(5),
                      }}
                    >
                      {item.activity}
                    </Text>
                  )}
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    marginTop: Height(2),
                    marginBottom: Height(2),
                  }}
                >
                  <Image
                    resizeMode="contain"
                    style={{ alignSelf: 'center' }}
                    source={calendar}
                  />
                  {item.start_date == '' ? (
                    <Text
                      onPress={
                        item.total_datetime_suggestions == 0
                          ? () =>
                              this.props.navigation.navigate('DateTime', {
                                plan_id: item.plan_id,
                                other_id: item.login_id,
                                is_direct: true,
                              })
                          : () =>
                              this.props.navigation.navigate(
                                'SuggestDateTime',
                                {
                                  other_id: item.login_id,
                                  plan_id: item.plan_id,
                                  is_direct: false,
                                },
                              )
                      }
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: '#358AF6',
                        marginRight: Width(5),
                      }}
                    >
                      {item.total_datetime_suggestions == 0
                        ? 'Suggest a date & time'
                        : 'View date & time suggestions'}
                    </Text>
                  ) : (
                    <Text
                      style={{
                        width: Width(60),
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: colors.dargrey,
                        marginRight: Width(5),
                      }}
                    >
                      {Final_date}
                    </Text>
                  )}
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ flexDirection: 'row', marginBottom: Height(2) }}
                >
                  <Image
                    resizeMode="contain"
                    style={{
                      alignSelf: 'center',
                      tintColor: colors.whatFontColor,
                    }}
                    source={location}
                  />
                  {item.line_1 == '' ? (
                    <Text
                      onPress={
                        item.total_location_suggestions == 0
                          ? () =>
                              this.props.navigation.navigate('Location', {
                                plan_id: item.plan_id,
                                other_id: item.login_id,
                                is_direct: true,
                              })
                          : () =>
                              this.props.navigation.navigate(
                                'SuggestLocation',
                                {
                                  other_id: item.login_id,
                                  plan_id: item.plan_id,
                                  is_direct: false,
                                },
                              )
                      }
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: '#358AF6',
                        marginRight: Width(5),
                      }}
                    >
                      {item.total_location_suggestions == 0
                        ? 'Suggest a location'
                        : 'View location suggestions'}
                    </Text>
                  ) : (
                    <Text
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: colors.dargrey,
                        marginRight: Width(5),
                      }}
                    >
                      {item.line_1}
                    </Text>
                  )}
                </TouchableOpacity>

                <View
                  style={{
                    height: Height(0.15),
                    backgroundColor: '#EFEFF4',
                    marginRight: Width(4),
                  }}
                />

                {item.is_going_count != 0 ||
                item.is_interested_count != 0 ||
                item.is_invited_count != 0 ? (
                  <View
                    style={{ flexDirection: 'row', marginBottom: Height(1.5) }}
                  >
                    {/* {item.is_going_count != 0 || item.is_interested_count != 0 || item.is_invited_count != 0 ?
                  <Image resizeMode="contain" style={{ marginTop: Height(2), alignSelf: 'center', tintColor: colors.whatFontColor }} source={new_user} /> : null} */}
                    {item.is_going_count != 0 ? (
                      <Text
                        onPress={() => {
                          this.props.navigation.navigate('Friend_Status', {
                            plan_id: item.plan_id,
                            tabIndex: 0,
                            Title_New: item.line_1,
                            plan_type: item.plan_type,
                          })
                        }}
                        style={{
                          marginTop: Height(1.5),
                          paddingLeft: Width(0),
                          fontFamily: fonts.Roboto_Regular,
                          fontSize: FontSize(12),
                          color: colors.dargrey,
                        }}
                      >
                        {item.is_going_count + ' going'}
                      </Text>
                    ) : null}
                    {item.is_interested_count != 0 ? (
                      <Text
                        onPress={() => {
                          this.props.navigation.navigate('Friend_Status', {
                            plan_id: item.plan_id,
                            tabIndex: 1,
                            Title_New: item.line_1,
                            plan_type: item.plan_type,
                          })
                        }}
                        style={
                          item.is_going_count == 0
                            ? {
                                marginTop: Height(1.5),
                                paddingLeft: Width(0),
                                fontFamily: fonts.Roboto_Regular,
                                fontSize: FontSize(12),
                                color: colors.dargrey,
                              }
                            : {
                                marginTop: Height(1.5),
                                paddingLeft: Width(1),
                                fontFamily: fonts.Roboto_Regular,
                                fontSize: FontSize(12),
                                color: colors.dargrey,
                              }
                        }
                      >
                        {item.is_going_count != 0 ? '\u00B7 ' : null}
                        {item.is_interested_count + ' interested'}
                      </Text>
                    ) : null}
                    {item.is_invited_count != 0 ? (
                      <Text
                        onPress={() => {
                          this.props.navigation.navigate('Friend_Status', {
                            plan_id: item.plan_id,
                            tabIndex: 2,
                            Title_New: item.line_1,
                            plan_type: item.plan_type,
                          })
                        }}
                        style={
                          item.is_going_count == 0 &&
                          item.is_interested_count == 0
                            ? {
                                marginTop: Height(1.5),
                                paddingLeft: Width(0),
                                fontFamily: fonts.Roboto_Regular,
                                fontSize: FontSize(12),
                                color: colors.dargrey,
                              }
                            : {
                                marginTop: Height(1.5),
                                paddingLeft: Width(1),
                                fontFamily: fonts.Roboto_Regular,
                                fontSize: FontSize(12),
                                color: colors.dargrey,
                              }
                        }
                      >
                        {item.is_interested_count != 0 ||
                        item.is_going_count != 0
                          ? '\u00B7 '
                          : null}
                        {item.is_invited_count + ' invited'}
                      </Text>
                    ) : null}
                    <TouchableOpacity
                      style={{
                        marginTop: Height(1.5),
                        position: 'absolute',
                        right: Width(4),
                        width: 35,
                        height: 35,
                      }}
                      onPress={() => {
                        this.props.navigation.navigate('EventDetail', {
                          item: item,
                          plan_id: item.plan_id,
                          other_id: item.login_id,
                          plan_type: item.plan_type,
                          title_header: 'Wanna go to ' + item.line_1 + '?',
                          Final_date: Final_date,
                          eventby: item.name,
                        })
                      }}
                    >
                      <Image resizeMode="contain" source={ic_chat} />
                    </TouchableOpacity>
                  </View>
                ) : (
                  <View>
                    {item.login_id != this.state.login_id ? (
                      <Text
                        style={{
                          marginTop: Height(1.5),
                          marginBottom: Height(1.5),
                          paddingLeft: Width(0),
                          fontFamily: fonts.Roboto_Regular,
                          fontSize: FontSize(12),
                          color: colors.dargrey,
                        }}
                      >
                        Be the first guest
                      </Text>
                    ) : (
                      <Text
                        style={{
                          marginTop: Height(1.5),
                          marginBottom: Height(1.5),
                          paddingLeft: Width(0),
                          fontFamily: fonts.Roboto_Regular,
                          fontSize: FontSize(12),
                          color: colors.dargrey,
                        }}
                      >
                        No guests yet
                      </Text>
                    )}
                    <TouchableOpacity
                      style={{
                        marginTop: Height(1.5),
                        marginBottom: Height(1.5),
                        position: 'absolute',
                        right: Width(4),
                        width: 35,
                        height: 35,
                      }}
                      onPress={() => {
                        this.props.navigation.navigate('EventDetail', {
                          item: item,
                          plan_id: item.plan_id,
                          other_id: item.login_id,
                          plan_type: item.plan_type,
                          title_header: 'Wanna go to ' + item.line_1 + '?',
                          Final_date: Final_date,
                          eventby: item.name,
                        })
                      }}
                    >
                      <Image resizeMode="contain" source={ic_chat} />
                    </TouchableOpacity>
                  </View>
                )}

                {item.login_id != this.state.login_id ? (
                  <View>
                    <View
                      style={{
                        height: Height(0.15),
                        backgroundColor: '#EFEFF4',
                        marginRight: Width(4),
                      }}
                    />

                    <View
                      style={{
                        flexDirection: 'row',
                        alignSelf: 'center',
                        justifyContent: 'center',
                        marginBottom: Height(1),
                        marginTop: Height(2),
                      }}
                    >
                      {/* {(item.is_going == 0 && item.is_interested == 0 && (item.activity != '' && item.line_1 != '' && item.start_date != ''))? */}
                      {item.activity != '' || item.line_1 != '' ? (
                        <View
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                        >
                          {item.is_going == 1 ? (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                onPress={() => this.goToNoGoing(item, index)}
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                              >
                                <Image source={going1} />
                                <Text
                                  style={{
                                    color: colors.pink,
                                    fontFamily: fonts.Roboto_Regular,
                                    marginLeft: Width(1),
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Going
                                </Text>
                              </TouchableOpacity>
                            </View>
                          ) : (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                                onPress={() => this.goToGoing(item, index)}
                              >
                                <Image source={going} />
                                <Text
                                  style={{
                                    fontFamily: fonts.Raleway_Regular,
                                    color: colors.dargrey,
                                    marginLeft: Width(1),
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Going
                                </Text>
                              </TouchableOpacity>
                            </View>
                          )}
                          <View style={{ width: Width(10) }} />

                          {item.is_interested == 1 ? (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                onPress={() =>
                                  this.goToNoIntersted(item, index)
                                }
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                              >
                                <Image source={circle_favorite} />
                                <Text
                                  style={{
                                    marginLeft: Width(1),
                                    color: colors.pink,
                                    fontFamily: fonts.Roboto_Regular,
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Interested
                                </Text>
                              </TouchableOpacity>
                            </View>
                          ) : (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                                onPress={() => this.goToIntersted(item, index)}
                              >
                                <Image source={black_heart} />
                                <Text
                                  style={{
                                    fontFamily: fonts.Raleway_Regular,
                                    color: colors.dargrey,
                                    marginLeft: Width(1),
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Interested
                                </Text>
                              </TouchableOpacity>
                            </View>
                          )}
                          <View style={{ width: Width(10) }} />
                          <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                              onPress={() => this.goToShare(item)}
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                              }}
                            >
                              <Image source={share} />
                              <Text
                                style={{
                                  color: colors.dargrey,
                                  fontFamily: fonts.Raleway_Regular,
                                  marginLeft: Width(1),
                                  fontSize: FontSize(16),
                                }}
                              >
                                Share
                              </Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                      ) : (
                        <View
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                        >
                          {item.is_interested == 1 ? (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                onPress={() =>
                                  this.goToNoIntersted(item, index)
                                }
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                              >
                                <Image source={circle_favorite} />
                                <Text
                                  style={{
                                    marginLeft: Width(1),
                                    color: colors.pink,
                                    fontFamily: fonts.Roboto_Regular,
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Interested
                                </Text>
                              </TouchableOpacity>
                            </View>
                          ) : (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                                onPress={() => this.goToIntersted(item, index)}
                              >
                                <Image source={black_heart} />
                                <Text
                                  style={{
                                    fontFamily: fonts.Raleway_Regular,
                                    color: colors.dargrey,
                                    marginLeft: Width(1),
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Interested
                                </Text>
                              </TouchableOpacity>
                            </View>
                          )}
                          <View style={{ width: Width(10) }} />
                          <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                              onPress={() => this.goToShare(item)}
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                              }}
                            >
                              <Image source={share} />
                              <Text
                                style={{
                                  color: colors.dargrey,
                                  fontFamily: fonts.Raleway_Regular,
                                  marginLeft: Width(1),
                                  fontSize: FontSize(16),
                                }}
                              >
                                Share
                              </Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                      )}
                    </View>
                  </View>
                ) : null}

                {item.login_id == this.state.login_id ? (
                  <View style={{ backgroundColor: 'white' }}>
                    <View
                      style={{
                        height: Height(0.15),
                        backgroundColor: '#EFEFF4',
                        marginRight: Width(4),
                      }}
                    />
                    <View
                      style={{
                        flexDirection: 'row',
                        alignSelf: 'center',
                        justifyContent: 'center',
                        marginBottom: Height(1),
                        marginTop: Height(2),
                      }}
                    >
                      <View
                        style={{
                          flexDirection: 'row',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}
                      >
                        <TouchableOpacity
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                          onPress={() => this.gotoEditEvent(item)}
                        >
                          <Image source={Edit} />
                          <Text
                            style={{
                              fontFamily: fonts.Raleway_Regular,
                              color: colors.dargrey,
                              marginLeft: Width(1.5),
                              fontSize: FontSize(16),
                            }}
                          >
                            Edit
                          </Text>
                        </TouchableOpacity>
                        <View style={{ width: Width(8) }} />
                        <TouchableOpacity
                          onPress={() => {
                            this.currentitemindex = index
                            this.setState({ plan_id: item.plan_id })
                            if (item.plan_type == 1) {
                              //this.eventtitle = name[0]+", let's meet up!"
                              this.eventtitle = "Let's meet up!"
                            } else if (item.plan_type == 2) {
                              this.eventtitle = item.activity
                            } else if (item.plan_type == 3) {
                              this.eventtitle =
                                'Wanna go to ' + item.line_1 + '?'
                            } else if (item.plan_type == 4) {
                              this.eventtitle = "I'm free, wanna hangout?"
                            }
                            Alert.alert(
                              'Cancel Event',
                              'Guests will be notified that this event was canceled',
                              [
                                {
                                  text: 'Cancel',
                                  onPress: () => console.log('Cancel Pressed'),
                                  style: 'cancel',
                                },
                                { text: 'OK', onPress: this.deleteplan },
                              ],
                              { cancelable: false },
                            )
                          }}
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                        >
                          <Image source={Delete} />
                          <Text
                            style={{
                              fontFamily: fonts.Raleway_Regular,
                              color: colors.dargrey,
                              marginLeft: Width(1.5),
                              fontSize: FontSize(16),
                            }}
                          >
                            Cancel
                          </Text>
                        </TouchableOpacity>
                        <View style={{ width: Width(8) }} />
                        <TouchableOpacity
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                          onPress={() => this.goToShare(item)}
                        >
                          <Image source={share} />
                          <Text
                            style={{
                              fontFamily: fonts.Raleway_Regular,
                              color: colors.dargrey,
                              marginLeft: Width(1.5),
                              fontSize: FontSize(16),
                            }}
                          >
                            Share
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                ) : null}
              </View>
            </View>
            <View style={{ height: Height(1) }} />
          </View>
        ) : null}

        {item.plan_type == 3 &&
        item.is_hide == 0 &&
        this.state.Peoples.length !== 0 &&
        index == 0 &&
        this.randomnumber % 2 == 0 ? (
          <View
            style={{
              height: Height(29),
              backgroundColor: '#F6F5FB',
              marginTop: Height(0.7),
            }}
          >
            <Text
              style={{
                marginTop: Height(2),
                marginLeft: Width(4.5),
                color: colors.appColor,
                fontFamily: fonts.Raleway_Bold,
                fontSize: FontSize(18),
              }}
            >
              People You May Know
            </Text>
            <View
              style={{
                marginTop: '2%',
                marginLeft: Width(0),
                marginRight: Width(1),
                flexDirection: 'row',
              }}
            >
              <ScrollView
                showsHorizontalScrollIndicator={false}
                horizontal
                contentContainerStyle={{ paddingHorizontal: Width(1) }}
              >
                {this.state.Peoples.map((item, index) => {
                  return (
                    <View
                      style={{
                        backgroundColor: colors.white,
                        height:
                          Platform.OS == 'android' ? Height(20) : Height(19.3),
                        width: Width(32.5),
                        borderRadius: Height(2),
                        alignItems: 'center',
                        marginTop: Height(2),
                        marginHorizontal: Width(1),
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => this.onPressUserName(item)}
                      >
                        <View
                          style={{
                            height: Height(7),
                            width: Height(7),
                            borderRadius: Height(7),
                            marginTop: Height(2),
                            overflow: 'hidden',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}
                        >
                          <Image
                            style={{ height: '100%', width: '100%' }}
                            resizeMode="stretch"
                            source={{ uri: item.photo }}
                          />
                        </View>
                      </TouchableOpacity>
                      <Text
                        style={{
                          marginTop: Height(1.5),
                          fontSize: FontSize(13),
                          color: colors.dargrey,
                          fontFamily: fonts.Roboto_Medium,
                          marginHorizontal: 5,
                        }}
                        numberOfLines={1}
                        onPress={() => this.onPressUserName(item)}
                      >
                        {item.name}
                      </Text>
                      {item.is_friend == 2 ? (
                        // <TouchableOpacity style={styles.inviteBox1} onPress = { () => this.cancelFriendRequestAPICall(item,index)}>
                        <TouchableOpacity style={styles.inviteBox1}>
                          <Text
                            style={[
                              {
                                fontSize: FontSize(13),
                                fontFamily: fonts.Raleway_Bold,
                                color: '#ffffff',
                                textAlign: 'center',
                              },
                              checkFontWeight('700'),
                            ]}
                          >
                            Pending
                          </Text>
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity
                          style={styles.inviteBox}
                          onPress={() => this.addFriendAPICall(item, index)}
                        >
                          <Text
                            style={[
                              {
                                fontSize: FontSize(13),
                                fontFamily: fonts.Raleway_Bold,
                                color: '#ffffff',
                                textAlign: 'center',
                              },
                              checkFontWeight('700'),
                            ]}
                          >
                            Add Friend
                          </Text>
                        </TouchableOpacity>
                      )}
                    </View>
                  )
                })}
              </ScrollView>
            </View>
          </View>
        ) : null}

        {item.plan_type == 3 &&
        this.state.Contacts.length > 0 &&
        this.state.Contacts != null &&
        index == 0 &&
        this.randomnumber % 2 != 0 ? (
          <View
            style={{
              height: Height(29),
              backgroundColor: '#F6F5FB',
              marginTop: Height(0.7),
            }}
          >
            <Text
              style={{
                marginTop: Height(2),
                marginLeft: Width(4.5),
                color: colors.appColor,
                fontFamily: fonts.Raleway_Bold,
                fontSize: FontSize(18),
              }}
            >
              Your Contacts
            </Text>
            <View
              style={{
                marginTop: '2%',
                marginLeft: Width(0),
                marginRight: Width(1),
                flexDirection: 'row',
              }}
            >
              <ScrollView
                showsHorizontalScrollIndicator={false}
                horizontal
                contentContainerStyle={{ paddingHorizontal: Width(1) }}
              >
                {this.state.Contacts.map((item) => {
                  return (
                    <View
                      style={{
                        backgroundColor: colors.white,
                        height:
                          Platform.OS == 'android' ? Height(20) : Height(19.3),
                        width: Width(33),
                        borderRadius: Height(2),
                        alignItems: 'center',
                        marginTop: Height(2),
                        marginHorizontal: Width(1),
                        justifyContent: 'center',
                      }}
                    >
                      <View
                        style={{
                          width: Height(7),
                          height: Height(7),
                          borderRadius: Height(7),
                          backgroundColor: '#D6D6D6',
                          justifyContent: 'center',
                        }}
                      >
                        <Text
                          style={[
                            {
                              textAlign: 'center',
                              fontFamily: 15,
                              fontFamily: 'Raleway-ExtraBold',
                              letterSpacing: 0.02,
                            },
                            checkFontWeight('800'),
                          ]}
                        >
                          {item.initial}
                        </Text>
                      </View>
                      <Text
                        style={{
                          marginTop: Height(1.5),
                          fontSize: FontSize(13),
                          color: colors.dargrey,
                          fontFamily: fonts.Roboto_Medium,
                          marginHorizontal: 5,
                        }}
                        numberOfLines={1}
                      >
                        {item.title}
                      </Text>
                      {/* <Text style={{ marginTop: Height(0.5), fontSize: FontSize(13), color: colors.dargrey, fontFamily: fonts.Roboto_Regular }}>{item.phoneNumbers && item.phoneNumbers[0].number}</Text> */}
                      <TouchableOpacity
                        style={styles.inviteBox}
                        onPress={() =>
                          this.inviteContacts(
                            item.phoneNumbers && item.phoneNumbers[0].number,
                          )
                        }
                      >
                        <Text
                          style={[
                            {
                              fontSize: FontSize(13),
                              fontFamily: fonts.Raleway_Bold,
                              color: '#ffffff',
                              textAlign: 'center',
                            },
                            checkFontWeight('700'),
                          ]}
                        >
                          Invite
                        </Text>
                      </TouchableOpacity>
                    </View>
                  )
                })}
              </ScrollView>
            </View>
          </View>
        ) : null}

        {item.plan_type == 4 && item.is_hide == 0 ? (
          <View
            style={{ marginTop: Height(0.7), backgroundColor: colors.white }}
          >
            {/* <TouchableOpacity onPress={() => { this.props.navigation.navigate('EventDetail', { item: item , plan_id: item.plan_id, other_id: item.login_id, plan_type: item.plan_type, title_header:"I'm free, wanna hangout?" , Final_date : Final_date, eventby:item.name}) }} style={{ height: Height(9), flexDirection: 'row', alignItems: 'center', backgroundColor: 'white' }}> */}
            <TouchableOpacity
              style={{
                height: Height(9),
                flexDirection: 'row',
                alignItems: 'center',
                backgroundColor: 'white',
              }}
              activeOpacity={0.9}
            >
              <View style={{ alignItems: 'center', marginLeft: Width(4.5) }}>
                <TouchableOpacity
                  onPress={
                    item.login_id == this.state.login_id
                      ? () => this.props.navigation.navigate('myprofile')
                      : () => {
                          AsyncStorage.setItem('other_id', item.login_id)
                          this.props.navigation.navigate('ohter_user_profile')
                        }
                  }
                  underlayColor="transparent"
                  style={{
                    height: Height(5),
                    width: Height(5),
                    borderRadius: Height(7),
                    overflow: 'hidden',
                  }}
                >
                  {item.photo == '' ? (
                    <Image
                      source={avtar}
                      style={{ height: '100%', width: '100%' }}
                    />
                  ) : (
                    <Image
                      style={{ height: '100%', width: '100%' }}
                      source={{ uri: item.photo }}
                    />
                  )}
                </TouchableOpacity>
              </View>
              <View style={{ justifyContent: 'center', marginLeft: Width(3) }}>
                <Text
                  onPress={
                    item.login_id == this.state.login_id
                      ? () => this.props.navigation.navigate('myprofile')
                      : () => {
                          AsyncStorage.setItem('other_id', item.login_id)
                          this.props.navigation.navigate('ohter_user_profile')
                        }
                  }
                  style={{
                    fontSize: FontSize(18),
                    color: colors.dargrey,
                    fontFamily: fonts.Raleway_Bold,
                  }}
                >
                  {item.login_id == this.state.login_id ? 'Me' : item.name}
                </Text>
                <Text
                  style={{ fontSize: FontSize(12), color: colors.fontDarkGrey }}
                >
                  {dateTime}
                  {item.share_with == '' ? null : '\u00B7 '}
                  {item.share_with}
                </Text>
              </View>
              <View
                style={{
                  alignItems: 'flex-end',
                  marginRight: Width(4),
                  flex: 4,
                }}
              >
                <TouchableOpacity
                  onPress={() => this.modelVisible(item, index)}
                  underlayColor="transparent"
                  style={{
                    justifyContent: 'center',
                    height: Height(6),
                    width: Width(6),
                  }}
                >
                  <Entypo
                    size={Width(6)}
                    name="dots-three-horizontal"
                    color="#000"
                  />
                </TouchableOpacity>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.9}
              onPress={() => {
                this.props.navigation.navigate('EventDetail', {
                  item: item,
                  plan_id: item.plan_id,
                  other_id: item.login_id,
                  plan_type: item.plan_type,
                  title_header: "I'm free, wanna hangout?",
                  Final_date: Final_date,
                  eventby: item.name,
                })
              }}
            >
              <ImageBackground
                source={
                  item.plan_image == '' ||
                  item.plan_image == null ||
                  item.plan_image == undefined
                    ? default_placeholder
                    : { uri: item.plan_image }
                }
                style={{
                  height: 265,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                {/* <Text style={{ fontSize: FontSize(20), color: colors.dargrey, fontFamily: fonts.Raleway_Semibold, marginHorizontal:48,lineHeight:27,textAlign:'center'}}>I'm free, wanna hangout?</Text>
              {item.description == '' ? null : <Text style={{ textAlign: 'center', fontSize: FontSize(12), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, width: '85%', marginTop: 2, alignSelf: 'center' }}>{item.description}</Text>} */}
              </ImageBackground>
            </TouchableOpacity>
            <View style={{ paddingHorizontal: Width(4.5) }}>
              <Text
                style={{
                  fontSize: FontSize(19),
                  color: colors.dargrey,
                  fontFamily: fonts.Raleway_Bold,
                  lineHeight: 27,
                  textAlign: 'left',
                  marginTop: 10,
                }}
              >
                I'm free, wanna hangout?
              </Text>
              {/* {item.description == '' ? null : <Text style={{ textAlign: 'left', fontSize: FontSize(13), color: colors.dargrey, fontFamily: fonts.Roboto_Regular, marginTop: 4,marginHorizontal:Width(4.5)}}>{item.description}</Text>} */}
              {item.description == '' ? null : (
                <SeeMore
                  style={{
                    textAlign: 'left',
                    fontSize: FontSize(13),
                    color: colors.dargrey,
                    fontFamily: fonts.Roboto_Regular,
                    marginTop: 4,
                  }}
                  numberOfLines={2}
                  seeMoreText="more"
                >
                  {item.description}
                </SeeMore>
              )}
            </View>
            <View
              style={{
                backgroundColor: '#FFF',
                alignItems: 'center',
                flexDirection: 'row',
              }}
            >
              <View style={{ flex: 1, marginLeft: Width(4.5) }}>
                <TouchableOpacity
                  style={{ flexDirection: 'row', marginTop: Height(2) }}
                >
                  <Image
                    resizeMode="contain"
                    style={{ alignSelf: 'center' }}
                    source={chai_cup}
                  />
                  {item.activity == '' ? (
                    <Text
                      onPress={
                        item.total_activity_suggestions == 0
                          ? () =>
                              this.props.navigation.navigate('Activity', {
                                plan_id: item.plan_id,
                                other_id: item.login_id,
                                is_direct: true,
                                title_header: "I'm free, wanna hangout?",
                              })
                          : () =>
                              this.props.navigation.navigate(
                                'SuggestActivity',
                                {
                                  other_id: item.login_id,
                                  plan_id: item.plan_id,
                                  is_direct: false,
                                  title_header: "I'm free, wanna hangout?",
                                },
                              )
                      }
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: '#358AF6',
                        marginRight: Width(5),
                      }}
                    >
                      {item.total_activity_suggestions == 0
                        ? 'Suggest an activity'
                        : 'View activity suggestions'}
                    </Text>
                  ) : (
                    <Text
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: colors.dargrey,
                        marginRight: Width(5),
                      }}
                    >
                      {item.activity}
                    </Text>
                  )}
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    marginTop: Height(2),
                    marginBottom: Height(2),
                  }}
                >
                  <Image
                    resizeMode="contain"
                    style={{ alignSelf: 'center' }}
                    source={calendar}
                  />
                  {item.start_date == '' ? (
                    <Text
                      onPress={
                        item.total_datetime_suggestions == 0
                          ? () =>
                              this.props.navigation.navigate('DateTime', {
                                plan_id: item.plan_id,
                                other_id: item.login_id,
                                is_direct: true,
                              })
                          : () =>
                              this.props.navigation.navigate(
                                'SuggestDateTime',
                                {
                                  other_id: item.login_id,
                                  plan_id: item.plan_id,
                                  is_direct: false,
                                },
                              )
                      }
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: '#358AF6',
                        marginRight: Width(5),
                      }}
                    >
                      {item.total_datetime_suggestions == 0
                        ? 'Suggest a date & time'
                        : 'View date & time suggestions'}
                    </Text>
                  ) : (
                    <Text
                      style={{
                        width: Width(60),
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: colors.dargrey,
                        marginRight: Width(5),
                      }}
                    >
                      {Final_date}
                    </Text>
                  )}
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ flexDirection: 'row', marginBottom: Height(2) }}
                >
                  <Image
                    resizeMode="contain"
                    style={{
                      alignSelf: 'center',
                      tintColor: colors.whatFontColor,
                    }}
                    source={location}
                  />
                  {item.line_1 == '' ? (
                    <Text
                      onPress={
                        item.total_location_suggestions == 0
                          ? () =>
                              this.props.navigation.navigate('Location', {
                                plan_id: item.plan_id,
                                other_id: item.login_id,
                                is_direct: true,
                              })
                          : () =>
                              this.props.navigation.navigate(
                                'SuggestLocation',
                                {
                                  other_id: item.login_id,
                                  plan_id: item.plan_id,
                                  is_direct: false,
                                },
                              )
                      }
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: '#358AF6',
                        marginRight: Width(5),
                      }}
                    >
                      {item.total_location_suggestions == 0
                        ? 'Suggest a location'
                        : 'View location suggestions'}
                    </Text>
                  ) : (
                    <Text
                      style={{
                        paddingLeft: Width(3.3),
                        fontFamily: fonts.Roboto_Regular,
                        fontSize: FontSize(15),
                        color: colors.dargrey,
                        marginRight: Width(5),
                      }}
                    >
                      {item.line_1}
                    </Text>
                  )}
                </TouchableOpacity>

                <View
                  style={{
                    height: Height(0.15),
                    backgroundColor: '#EFEFF4',
                    marginRight: Width(4),
                  }}
                />

                {item.is_going_count != 0 ||
                item.is_interested_count != 0 ||
                item.is_invited_count != 0 ? (
                  <View
                    style={{ flexDirection: 'row', marginBottom: Height(1.5) }}
                  >
                    {/* {item.is_going_count != 0 || item.is_interested_count != 0 || item.is_invited_count != 0 ?
                  <Image resizeMode="contain" style={{ marginTop: Height(2), alignSelf: 'center', tintColor: colors.whatFontColor }} source={new_user} /> : null} */}
                    {item.is_going_count != 0 ? (
                      <Text
                        onPress={() => {
                          this.props.navigation.navigate('Friend_Status', {
                            plan_id: item.plan_id,
                            tabIndex: 0,
                            Title_New: item.activity,
                            plan_type: item.plan_type,
                          })
                        }}
                        style={{
                          marginTop: Height(1.5),
                          paddingLeft: Width(0),
                          fontFamily: fonts.Roboto_Regular,
                          fontSize: FontSize(12),
                          color: colors.dargrey,
                        }}
                      >
                        {item.is_going_count + ' going'}
                      </Text>
                    ) : null}
                    {item.is_interested_count != 0 ? (
                      <Text
                        onPress={() => {
                          this.props.navigation.navigate('Friend_Status', {
                            plan_id: item.plan_id,
                            tabIndex: 1,
                            Title_New: item.activity,
                            plan_type: item.plan_type,
                          })
                        }}
                        style={
                          item.is_going_count == 0
                            ? {
                                marginTop: Height(1.5),
                                paddingLeft: Width(0),
                                fontFamily: fonts.Roboto_Regular,
                                fontSize: FontSize(12),
                                color: colors.dargrey,
                              }
                            : {
                                marginTop: Height(1.5),
                                paddingLeft: Width(1),
                                fontFamily: fonts.Roboto_Regular,
                                fontSize: FontSize(12),
                                color: colors.dargrey,
                              }
                        }
                      >
                        {item.is_going_count != 0 ? '\u00B7 ' : null}
                        {item.is_interested_count + ' interested'}
                      </Text>
                    ) : null}
                    {item.is_invited_count != 0 ? (
                      <Text
                        onPress={() => {
                          this.props.navigation.navigate('Friend_Status', {
                            plan_id: item.plan_id,
                            tabIndex: 2,
                            Title_New: item.activity,
                            plan_type: item.plan_type,
                          })
                        }}
                        style={
                          item.is_going_count == 0 &&
                          item.is_interested_count == 0
                            ? {
                                marginTop: Height(1.5),
                                paddingLeft: Width(0),
                                fontFamily: fonts.Roboto_Regular,
                                fontSize: FontSize(12),
                                color: colors.dargrey,
                              }
                            : {
                                marginTop: Height(1.5),
                                paddingLeft: Width(1),
                                fontFamily: fonts.Roboto_Regular,
                                fontSize: FontSize(12),
                                color: colors.dargrey,
                              }
                        }
                      >
                        {item.is_interested_count != 0 ||
                        item.is_going_count != 0
                          ? '\u00B7 '
                          : null}
                        {item.is_invited_count + ' invited'}
                      </Text>
                    ) : null}
                    <TouchableOpacity
                      style={{
                        marginTop: Height(1.5),
                        position: 'absolute',
                        right: Width(4),
                        width: 35,
                        height: 35,
                      }}
                      onPress={() => {
                        this.props.navigation.navigate('EventDetail', {
                          item: item,
                          plan_id: item.plan_id,
                          other_id: item.login_id,
                          plan_type: item.plan_type,
                          title_header: "I'm free, wanna hangout?",
                          Final_date: Final_date,
                          eventby: item.name,
                        })
                      }}
                    >
                      <Image resizeMode="contain" source={ic_chat} />
                    </TouchableOpacity>
                  </View>
                ) : (
                  <View>
                    {item.login_id != this.state.login_id ? (
                      <Text
                        style={{
                          marginTop: Height(1.5),
                          marginBottom: Height(1.5),
                          paddingLeft: Width(0),
                          fontFamily: fonts.Roboto_Regular,
                          fontSize: FontSize(12),
                          color: colors.dargrey,
                        }}
                      >
                        Be the first guest
                      </Text>
                    ) : (
                      <Text
                        style={{
                          marginTop: Height(1.5),
                          marginBottom: Height(1.5),
                          paddingLeft: Width(0),
                          fontFamily: fonts.Roboto_Regular,
                          fontSize: FontSize(12),
                          color: colors.dargrey,
                        }}
                      >
                        No guests yet
                      </Text>
                    )}
                    <TouchableOpacity
                      style={{
                        marginTop: Height(1.5),
                        marginBottom: Height(1.5),
                        position: 'absolute',
                        right: Width(4),
                        width: 35,
                        height: 35,
                      }}
                      onPress={() => {
                        this.props.navigation.navigate('EventDetail', {
                          item: item,
                          plan_id: item.plan_id,
                          other_id: item.login_id,
                          plan_type: item.plan_type,
                          title_header: "I'm free, wanna hangout?",
                          Final_date: Final_date,
                          eventby: item.name,
                        })
                      }}
                    >
                      <Image resizeMode="contain" source={ic_chat} />
                    </TouchableOpacity>
                  </View>
                )}

                {item.login_id != this.state.login_id ? (
                  <View>
                    <View
                      style={{
                        height: Height(0.15),
                        backgroundColor: '#EFEFF4',
                        marginRight: Width(4),
                      }}
                    />

                    <View
                      style={{
                        flexDirection: 'row',
                        alignSelf: 'center',
                        justifyContent: 'center',
                        marginBottom: Height(1),
                        marginTop: Height(2),
                      }}
                    >
                      {/* {(item.is_going == 0 && item.is_interested == 0 && (item.activity != '' && item.line_1 != '' && item.start_date != ''))? */}
                      {item.activity != '' || item.line_1 != '' ? (
                        <View
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                        >
                          {item.is_going == 1 ? (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                onPress={() => this.goToNoGoing(item, index)}
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                              >
                                <Image source={going1} />
                                <Text
                                  style={{
                                    color: colors.pink,
                                    fontFamily: fonts.Roboto_Regular,
                                    marginLeft: Width(1),
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Going
                                </Text>
                              </TouchableOpacity>
                            </View>
                          ) : (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                                onPress={() => this.goToGoing(item, index)}
                              >
                                <Image source={going} />
                                <Text
                                  style={{
                                    fontFamily: fonts.Raleway_Regular,
                                    color: colors.dargrey,
                                    marginLeft: Width(1),
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Going
                                </Text>
                              </TouchableOpacity>
                            </View>
                          )}
                          <View style={{ width: Width(10) }} />

                          {item.is_interested == 1 ? (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                onPress={() =>
                                  this.goToNoIntersted(item, index)
                                }
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                              >
                                <Image source={circle_favorite} />
                                <Text
                                  style={{
                                    marginLeft: Width(1),
                                    color: colors.pink,
                                    fontFamily: fonts.Roboto_Regular,
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Interested
                                </Text>
                              </TouchableOpacity>
                            </View>
                          ) : (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                                onPress={() => this.goToIntersted(item, index)}
                              >
                                <Image source={black_heart} />
                                <Text
                                  style={{
                                    fontFamily: fonts.Raleway_Regular,
                                    color: colors.dargrey,
                                    marginLeft: Width(1),
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Interested
                                </Text>
                              </TouchableOpacity>
                            </View>
                          )}
                          <View style={{ width: Width(10) }} />
                          <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                              onPress={() => this.goToShare(item)}
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                              }}
                            >
                              <Image source={share} />
                              <Text
                                style={{
                                  color: colors.dargrey,
                                  fontFamily: fonts.Raleway_Regular,
                                  marginLeft: Width(1),
                                  fontSize: FontSize(16),
                                }}
                              >
                                Share
                              </Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                      ) : (
                        <View
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                        >
                          {item.is_interested == 1 ? (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                onPress={() =>
                                  this.goToNoIntersted(item, index)
                                }
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                              >
                                <Image source={circle_favorite} />
                                <Text
                                  style={{
                                    marginLeft: Width(1),
                                    color: colors.pink,
                                    fontFamily: fonts.Roboto_Regular,
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Interested
                                </Text>
                              </TouchableOpacity>
                            </View>
                          ) : (
                            <View style={{ flexDirection: 'row' }}>
                              <TouchableOpacity
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                                onPress={() => this.goToIntersted(item, index)}
                              >
                                <Image source={black_heart} />
                                <Text
                                  style={{
                                    fontFamily: fonts.Raleway_Regular,
                                    color: colors.dargrey,
                                    marginLeft: Width(1),
                                    fontSize: FontSize(16),
                                  }}
                                >
                                  Interested
                                </Text>
                              </TouchableOpacity>
                            </View>
                          )}
                          <View style={{ width: Width(10) }} />
                          <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                              onPress={() => this.goToShare(item)}
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                              }}
                            >
                              <Image source={share} />
                              <Text
                                style={{
                                  color: colors.dargrey,
                                  fontFamily: fonts.Raleway_Regular,
                                  marginLeft: Width(1),
                                  fontSize: FontSize(16),
                                }}
                              >
                                Share
                              </Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                      )}
                    </View>
                  </View>
                ) : null}

                {item.login_id == this.state.login_id ? (
                  <View style={{ backgroundColor: 'white' }}>
                    <View
                      style={{
                        height: Height(0.15),
                        backgroundColor: '#EFEFF4',
                        marginRight: Width(4),
                      }}
                    />
                    <View
                      style={{
                        flexDirection: 'row',
                        alignSelf: 'center',
                        justifyContent: 'center',
                        marginBottom: Height(1),
                        marginTop: Height(2),
                      }}
                    >
                      <View
                        style={{
                          flexDirection: 'row',
                          alignSelf: 'center',
                          justifyContent: 'center',
                        }}
                      >
                        <TouchableOpacity
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                          onPress={() => this.gotoEditEvent(item)}
                        >
                          <Image source={Edit} />
                          <Text
                            style={{
                              fontFamily: fonts.Raleway_Regular,
                              color: colors.dargrey,
                              marginLeft: Width(1.5),
                              fontSize: FontSize(16),
                            }}
                          >
                            Edit
                          </Text>
                        </TouchableOpacity>
                        <View style={{ width: Width(8) }} />
                        <TouchableOpacity
                          onPress={() => {
                            this.currentitemindex = index
                            this.setState({ plan_id: item.plan_id })
                            if (item.plan_type == 1) {
                              //this.eventtitle = name[0]+", let's meet up!"
                              this.eventtitle = "Let's meet up!"
                            } else if (item.plan_type == 2) {
                              this.eventtitle = item.activity
                            } else if (item.plan_type == 3) {
                              this.eventtitle =
                                'Wanna go to ' + item.line_1 + '?'
                            } else if (item.plan_type == 4) {
                              this.eventtitle = "I'm free, wanna hangout?"
                            }
                            Alert.alert(
                              'Cancel Event',
                              'Guests will be notified that this event was canceled',
                              [
                                {
                                  text: 'Cancel',
                                  onPress: () => console.log('Cancel Pressed'),
                                  style: 'cancel',
                                },
                                { text: 'OK', onPress: this.deleteplan },
                              ],
                              { cancelable: false },
                            )
                          }}
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                        >
                          <Image source={Delete} />
                          <Text
                            style={{
                              fontFamily: fonts.Raleway_Regular,
                              color: colors.dargrey,
                              marginLeft: Width(1.5),
                              fontSize: FontSize(16),
                            }}
                          >
                            Cancel
                          </Text>
                        </TouchableOpacity>
                        <View style={{ width: Width(8) }} />
                        <TouchableOpacity
                          style={{ flexDirection: 'row', alignItems: 'center' }}
                          onPress={() => this.goToShare(item)}
                        >
                          <Image source={share} />
                          <Text
                            style={{
                              fontFamily: fonts.Raleway_Regular,
                              color: colors.dargrey,
                              marginLeft: Width(1.5),
                              fontSize: FontSize(16),
                            }}
                          >
                            Share
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                ) : null}
              </View>
            </View>
            <View style={{ height: Height(1) }} />
          </View>
        ) : null}

        {item.plan_type == 4 &&
        item.is_hide == 0 &&
        this.state.Peoples.length !== 0 &&
        index == 0 &&
        this.randomnumber % 2 == 0 ? (
          <View
            style={{
              height: Height(29),
              backgroundColor: '#F6F5FB',
              marginTop: Height(0.7),
            }}
          >
            <Text
              style={{
                marginTop: Height(2),
                marginLeft: Width(4.5),
                color: colors.appColor,
                fontFamily: fonts.Raleway_Bold,
                fontSize: FontSize(18),
              }}
            >
              People You May Know
            </Text>
            <View
              style={{
                marginTop: '2%',
                marginLeft: Width(0),
                marginRight: Width(1),
                flexDirection: 'row',
              }}
            >
              <ScrollView
                showsHorizontalScrollIndicator={false}
                horizontal
                contentContainerStyle={{ paddingHorizontal: Width(1) }}
              >
                {this.state.Peoples.map((item, index) => {
                  return (
                    <View
                      style={{
                        backgroundColor: colors.white,
                        height:
                          Platform.OS == 'android' ? Height(20) : Height(19.3),
                        width: Width(32.5),
                        borderRadius: Height(2),
                        alignItems: 'center',
                        marginTop: Height(2),
                        marginHorizontal: Width(1),
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => this.onPressUserName(item)}
                      >
                        <View
                          style={{
                            height: Height(7),
                            width: Height(7),
                            borderRadius: Height(7),
                            marginTop: Height(2),
                            overflow: 'hidden',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}
                        >
                          <Image
                            style={{ height: '100%', width: '100%' }}
                            resizeMode="stretch"
                            source={{ uri: item.photo }}
                          />
                        </View>
                      </TouchableOpacity>
                      <Text
                        style={{
                          marginTop: Height(1.5),
                          fontSize: FontSize(13),
                          color: colors.dargrey,
                          fontFamily: fonts.Roboto_Medium,
                          marginHorizontal: 5,
                        }}
                        numberOfLines={1}
                        onPress={() => this.onPressUserName(item)}
                      >
                        {item.name}
                      </Text>
                      {item.is_friend == 2 ? (
                        // <TouchableOpacity style={styles.inviteBox1} onPress = { () => this.cancelFriendRequestAPICall(item,index)}>
                        <TouchableOpacity style={styles.inviteBox1}>
                          <Text
                            style={[
                              {
                                fontSize: FontSize(13),
                                fontFamily: fonts.Raleway_Bold,
                                color: '#ffffff',
                                textAlign: 'center',
                              },
                              checkFontWeight('700'),
                            ]}
                          >
                            Pending
                          </Text>
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity
                          style={styles.inviteBox}
                          onPress={() => this.addFriendAPICall(item, index)}
                        >
                          <Text
                            style={[
                              {
                                fontSize: FontSize(13),
                                fontFamily: fonts.Raleway_Bold,
                                color: '#ffffff',
                                textAlign: 'center',
                              },
                              checkFontWeight('700'),
                            ]}
                          >
                            Add Friend
                          </Text>
                        </TouchableOpacity>
                      )}
                    </View>
                  )
                })}
              </ScrollView>
            </View>
          </View>
        ) : null}
        {item.plan_type == 4 &&
        this.state.Contacts.length > 0 &&
        this.state.Contacts != null &&
        index == 0 &&
        this.randomnumber % 2 != 0 ? (
          <View
            style={{
              height: Height(29),
              backgroundColor: '#F6F5FB',
              marginTop: Height(0.7),
            }}
          >
            <Text
              style={{
                marginTop: Height(2),
                marginLeft: Width(4.5),
                color: colors.appColor,
                fontFamily: fonts.Raleway_Bold,
                fontSize: FontSize(18),
              }}
            >
              Your Contacts
            </Text>
            <View
              style={{
                marginTop: '2%',
                marginLeft: Width(0),
                marginRight: Width(1),
                flexDirection: 'row',
              }}
            >
              <ScrollView
                showsHorizontalScrollIndicator={false}
                horizontal
                contentContainerStyle={{ paddingHorizontal: Width(1) }}
              >
                {this.state.Contacts.map((item) => {
                  return (
                    <View
                      style={{
                        backgroundColor: colors.white,
                        height:
                          Platform.OS == 'android' ? Height(20) : Height(19.3),
                        width: Width(33),
                        borderRadius: Height(2),
                        alignItems: 'center',
                        marginTop: Height(2),
                        marginHorizontal: Width(1),
                        justifyContent: 'center',
                      }}
                    >
                      <View
                        style={{
                          width: Height(7),
                          height: Height(7),
                          borderRadius: Height(7),
                          backgroundColor: '#D6D6D6',
                          justifyContent: 'center',
                        }}
                      >
                        <Text
                          style={[
                            {
                              textAlign: 'center',
                              fontFamily: 15,
                              fontFamily: 'Raleway-ExtraBold',
                              letterSpacing: 0.02,
                            },
                            checkFontWeight('800'),
                          ]}
                        >
                          {item.initial}
                        </Text>
                      </View>
                      <Text
                        style={{
                          marginTop: Height(1.5),
                          fontSize: FontSize(13),
                          color: colors.dargrey,
                          fontFamily: fonts.Roboto_Medium,
                          marginHorizontal: 5,
                        }}
                        numberOfLines={1}
                      >
                        {item.title}
                      </Text>
                      {/* <Text style={{ marginTop: Height(0.5), fontSize: FontSize(13), color: colors.dargrey, fontFamily: fonts.Roboto_Regular }}>{item.phoneNumbers && item.phoneNumbers[0].number}</Text> */}
                      <TouchableOpacity
                        style={styles.inviteBox}
                        onPress={() =>
                          this.inviteContacts(
                            item.phoneNumbers && item.phoneNumbers[0].number,
                          )
                        }
                      >
                        <Text
                          style={[
                            {
                              fontSize: FontSize(13),
                              fontFamily: fonts.Raleway_Bold,
                              color: '#ffffff',
                              textAlign: 'center',
                            },
                            checkFontWeight('700'),
                          ]}
                        >
                          Invite
                        </Text>
                      </TouchableOpacity>
                    </View>
                  )
                })}
              </ScrollView>
            </View>
          </View>
        ) : null}
      </View>
    )
  }

  getNewFeeds = () => {
    //this.setState({ refreshing: true,is_new_post_available:false});
    this.onNewPost()
  }

  onRefresh = () => {
    //alert('Hiii')
    //console.log('onRefreshCalled')
    //alert(this.randomnumber)
    //this.setState({ refreshing: false , is_new_post_available:false});
    //this.getPostList()

    if (this.offset <= 0) {
      this.getPostList()
      return
    }

    let data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('limit', this.offset + this.state.limit)
    data.append('offset', 0)
    data.append('type', '')
    fetch(API_ROOT + 'plan/list', {
      method: 'post',
      body: data,
    })
      .then((response) => response.json())

      .then((responseData) => {
        if (responseData.success) {
          //console.log('hello', responseData.success)
          AsyncStorage.setItem('overlay', 'false')
          this.setState({
            planList: responseData.data,
            refreshing: false,
            is_new_post_available: false,
          })
          this.arrayholder = responseData.data
          //console.log('MoreData: '+JSON.stringify(this.state.planList))
        } else {
          AsyncStorage.setItem('overlay', 'false')
          this.setState({ status: false })
        }
      })
      .catch((err) => {
        //alert('Oops your connection seems off, Check your connection and try again')
      })
  }

  onNewPost = () => {
    console.log('onNewPostCalled')

    // this.setState({is_new_post_available:false, refreshing:true},() => {
    //       if(this.scroll != null)
    //       {
    //         this.scroll.getNode().scrollToOffset({ offset: 0, animated: true });
    //       }
    // })

    if (this.scroll != null) {
      this.scroll.getNode().scrollToOffset({ offset: 0, animated: true })
    }

    //this.scroll.scrollTo({x: 0, y: 0, animated: true});

    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('limit', this.state.limit)
    data.append('offset', 0)
    data.append('type', '')
    this.setState({ refreshing: true })
    fetch(API_ROOT + 'plan/list', {
      method: 'post',
      body: data,
    })
      .then((response) => response.json())
      .then((responseData) => {
        if (responseData.success) {
          //console.log('New Post'+responseData)
          this.offset = 0

          this.setState({
            planList: responseData.data,
            refreshing: false,
            is_new_post_available: false,
          })
          //this.scrollView.scrollToEnd( { animated: false } )
          this.arrayholder = responseData.data
        } else {
          //console.log('New Post else'+responseData)
          if (responseData.text === 'Invalid token key') {
            alert(responseData.text)
            this.props.navigation.navigate('Login')
          } else {
            this.setState({ is_new_post_available: false })
          }
          //this.setState({ refreshing: false })
        }
      })
      .catch((error) => {
        //alert('Oops your connection seems off, Check your connection and try again')
        this.setState({ refreshing: false, is_new_post_available: false })
      })
  }

  getMyPostList = () => {
    //RNProgressHUB.showSpinIndeterminate();

    var data = new FormData()
    data.append('token', this.state.token)
    data.append('login_id', this.state.login_id)
    data.append('limit', 10)
    data.append('offset', 0)
    data.append('type', 'my')
    this.setState({ refreshing: false })
    fetch(API_ROOT + 'plan/list', {
      method: 'post',
      body: data,
    })
      .then((response) => response.json())
      .then((responseData) => {
        //RNProgressHUB.dismiss()
        if (responseData.success) {
          var arrposts = responseData.data

          if (arrposts.length > 0) {
            this.mypostcount = 1
          } else {
            this.mypostcount = 0
          }
        } else {
          this.mypostcount = 0
          this.setState({ refreshing: false })
          if (responseData.text == 'Invalid token key') {
            this.props.navigation.navigate('Login')
          }
        }
      })
      .catch((error) => {
        this.mypostcount = 0
        this.setState({ refreshing: false })

        //RNProgressHUB.dismiss()
      })
  }

  onPressOnlineActivity(item) {
    //alert(JSON.stringify(item))
    //["Zoom Call","Houseparty","Video Chat","Phone Call","Virtual Happy Hour","Game night","Movie night","Virtual Date night","Fitness session"]
    var activity = ''

    if (item.title == 'Zoom Call') {
      activity = 'Wanna Have a Zoom Call?'
    } else if (item.title == 'Houseparty') {
      activity = 'Wanna Have a Houseparty?'
    } else if (item.title == 'Video Chat') {
      activity = 'Wanna Video Chat?'
    } else if (item.title == 'Phone Call') {
      activity = 'Wanna Have a Phone Call?'
    } else if (item.title == 'Virtual Happy Hour') {
      activity = 'Wanna Have a Virtual Happy Hour?'
    } else if (item.title == 'Game night') {
      activity = 'Wanna Have a Game Night?'
    } else if (item.title == 'Movie night') {
      activity = 'Wanna Have a Movie Night?'
    } else if (item.title == 'Virtual Date night') {
      activity = 'Wanna Have a Virtual Game Night?'
    } else if (item.title == 'Fitness session') {
      activity = 'Wanna Have a Fitness Session?'
    }

    AsyncStorage.setItem('What_Activity', activity)
    this.props.navigation.navigate('Frinds', {
      is_edit_plan: false,
      is_from_feed: true,
    })
  }

  render() {
    let {
      isModalVisible,
      isMyModalVisible,
      isMyDeleteModalVisible,
    } = this.state
    const interpolateRotation = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '135deg'],
    })
    const animatedStyle = {
      transform: [{ rotate: interpolateRotation }],
    }
    var name = this.state.name.split(' ')
    const { clampedScroll } = this.state

    const navbarTranslate = clampedScroll.interpolate({
      inputRange: [0, NAVBAR_HEIGHT - STATUS_BAR_HEIGHT],
      outputRange: [0, -(NAVBAR_HEIGHT - STATUS_BAR_HEIGHT)],
      extrapolate: 'clamp',
    })
    const navbarOpacity = clampedScroll.interpolate({
      inputRange: [0, NAVBAR_HEIGHT - STATUS_BAR_HEIGHT],
      outputRange: [1, 0],
      extrapolate: 'clamp',
    })

    console.log('Overlay', this.state.overlay + ' ' + this.state.temp)

    return (
      <View style={styles.fill}>
        {this.state.is_new_post_available == true ? (
          <TouchableOpacity
            onPress={this.onNewPost}
            style={{
              flexDirection: 'row',
              top: Height(11.5),
              justifyContent: 'center',
              left: Width(35),
              alignItems: 'center',
              position: 'absolute',
              height: Height(3.5),
              width: Width(28),
              borderRadius: Height(2),
              backgroundColor: colors.blue,
              zIndex: 1,
              shadowOffset: { width: 1, height: 4 },
              shadowColor: '#9E8BF3',
              shadowOpacity: 0.4,
            }}
          >
            <Text
              style={{
                color: colors.white,
                fontSize: FontSize(13),
                fontFamily: fonts.Raleway_Bold,
              }}
            >
              New Posts
            </Text>
            <AntDesign
              style={{ marginLeft: 3 }}
              name="arrowup"
              color={colors.white}
              size={15}
            />
          </TouchableOpacity>
        ) : null}
        <Animated.ScrollView
          // refreshControl={
          //   <RefreshControl refreshing={this.state.refreshing} onRefresh={this.getNewFeeds} />
          // }
          contentContainerStyle={{
            flex: 1,
            marginTop: this.state.planList.length > 0 ? NAVBAR_HEIGHT : 0,
          }}
          scrollEventThrottle={1}
          scrollEnabled={false}
          //          ref={(ref) => { this.scroll = ref; }}
          onMomentumScrollBegin={this._onMomentumScrollBegin}
          onMomentumScrollEnd={this._onMomentumScrollEnd}
          onScrollEndDrag={this._onScrollEndDrag}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollAnim } } }],
            { useNativeDriver: true },
          )}
        >
          {this.state.planList.length == 0 ? (
            <View>
              <Animated.View
                style={{ marginTop: NAVBAR_HEIGHT, height: Height(7) }}
              >
                <View
                  style={{
                    flexDirection: 'row',
                    backgroundColor: colors.white,
                    height: Height(9.3),
                  }}
                >
                  {/* <TouchableOpacity onPress={this.getPostList} style={{ flexDirection: 'row', top: Height(0.7), justifyContent: 'center', left: Width(35), alignItems: 'center', position: 'absolute', height: Height(3.5), width: Width(28), borderRadius: Height(2), backgroundColor: colors.blue }}>
                <Text style={{ color: colors.white, fontSize: FontSize(13), fontFamily: fonts.Raleway_Bold }}>New Posts</Text>
                <AntDesign style={{ marginLeft: 3 }} name="arrowup" color={colors.white} size={15} />
              </TouchableOpacity> */}
                  <View
                    style={{ justifyContent: 'center', marginLeft: Width(4.5) }}
                  >
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate('myprofile')
                      }
                      underlayColor="transparent"
                      style={{
                        height: Height(5),
                        width: Height(5),
                        borderRadius: Height(7),
                        overflow: 'hidden',
                      }}
                    >
                      {this.state.photo == '' ? (
                        <Image
                          source={avtar}
                          style={{ height: '100%', width: '100%' }}
                        />
                      ) : (
                        <Image
                          style={{ height: '100%', width: '100%' }}
                          source={{ uri: this.state.photo }}
                        />
                      )}
                    </TouchableOpacity>
                  </View>

                  <View style={{ justifyContent: 'center' }}>
                    <Text
                      // onPress={() => this.props.navigation.navigate('myprofile')}

                      style={{
                        fontFamily: fonts.Raleway_Regular,
                        fontSize: FontSize(18),
                        color: colors.fontDarkGrey,
                        marginLeft: Width(3.5),
                      }}
                    >
                      {name[0]}, want to make a plan?
                    </Text>
                  </View>
                </View>
              </Animated.View>
              <View
                style={{
                  marginTop: Height(2),
                  height: 1,
                  backgroundColor: '#F7F7F7',
                }}
              ></View>
              <Animated.View
                style={{
                  padding: 8,
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                  alignSelf: 'center',
                  justifyContent: 'space-around',
                  backgroundColor: 'white',
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('Who1', {
                      is_edit_plan: false,
                    })
                  }}
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingHorizontal: 10,
                  }}
                >
                  <Image source={who} style={{ margin: '3%' }} />
                  <Text style={styles.headerPlaneColor}>Who</Text>
                </TouchableOpacity>

                <View
                  style={{
                    height: Height(2.5),
                    width: 1,
                    backgroundColor: '#F7F7F7',
                    mjustifyContent: 'center',
                    alignItems: 'center',
                  }}
                ></View>

                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('what1', {
                      is_edit_plan: false,
                    })
                  }}
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingHorizontal: 10,
                  }}
                >
                  <Image source={cup} style={{ margin: '3%' }} />
                  <Text style={styles.headerPlaneColor}>What</Text>
                </TouchableOpacity>

                <View
                  style={{
                    height: Height(2.5),
                    width: 1,
                    backgroundColor: '#F7F7F7',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                ></View>

                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('where1', {
                      is_edit_plan: false,
                    })
                  }}
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingHorizontal: 10,
                  }}
                >
                  <Image
                    source={Location_new}
                    style={[
                      isIphone()
                        ? styles.textInputIos
                        : styles.textInputAndroid,
                    ]}
                  />
                  <Text style={styles.headerPlaneColor}>Where</Text>
                </TouchableOpacity>

                <View
                  style={{
                    height: Height(2.5),
                    width: 1,
                    backgroundColor: colors.lightgrey,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                ></View>

                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('when1', {
                      is_edit_plan: false,
                    })
                  }}
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingHorizontal: 10,
                  }}
                >
                  <Image source={checked} style={{ margin: '3%' }} />
                  <Text style={styles.headerPlaneColor}>When</Text>
                </TouchableOpacity>
              </Animated.View>
            </View>
          ) : null}

          {this.state.planList.length > 0 ? (
            <AnimatedListView
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this.getNewFeeds}
                />
              }
              contentContainerStyle={styles.contentContainer}
              ref={(ref) => {
                this.scroll = ref
              }}
              data={this.state.planList}
              renderItem={(item, index) => this._renderItem(item, index)}
              onEndReached={this.onEndReached}
              onEndReachedThreshold={0.8}
              onScroll={this.handleFlatScroll}
              extraData={this.state}
              //onEndReachedThreshold={0.8}
            />
          ) : (
            <View>
              {
                <View
                  style={{
                    height: Height(25),
                    backgroundColor: '#fff',
                    marginTop: Height(0.7),
                  }}
                >
                  <Text
                    style={{
                      marginTop: Height(2),
                      marginLeft: Width(4.5),
                      color: colors.appColor,
                      fontFamily: fonts.Raleway_Bold,
                      fontSize: FontSize(18),
                    }}
                  >
                    Virtual Activities
                  </Text>
                  <View
                    style={{
                      marginTop: '2%',
                      marginLeft: Width(0),
                      marginRight: Width(1),
                      flexDirection: 'row',
                    }}
                  >
                    <ScrollView
                      showsHorizontalScrollIndicator={false}
                      horizontal
                      contentContainerStyle={{ paddingHorizontal: Width(1) }}
                    >
                      {this.arronlineactivity.map((item) => {
                        return (
                          <TouchableOpacity
                            style={{
                              backgroundColor: '#F6F5FB',
                              height:
                                Platform.OS == 'android'
                                  ? Height(16)
                                  : Height(15.3),
                              width: Width(33.5),
                              borderRadius: Height(2),
                              alignItems: 'center',
                              marginTop: Height(2),
                              marginHorizontal: Width(1),
                              justifyContent: 'center',
                            }}
                            activeOpacity={0.9}
                            onPress={() => this.onPressOnlineActivity(item)}
                          >
                            <View
                              style={{
                                height: Height(5),
                                width: Height(5),
                                marginTop: Height(1.5),
                                overflow: 'hidden',
                                alignItems: 'center',
                                justifyContent: 'center',
                              }}
                            >
                              <Image
                                style={{ height: '100%', width: '100%' }}
                                resizeMode="contain"
                                source={item.icon}
                              />
                            </View>
                            <Text
                              style={{
                                marginTop: Height(3),
                                fontSize: FontSize(13),
                                color: '#3A4759',
                                fontFamily: fonts.Roboto_Medium,
                              }}
                              numberOfLines={1}
                            >
                              {item.title}
                            </Text>
                          </TouchableOpacity>
                        )
                      })}
                    </ScrollView>
                  </View>
                </View>
              }
              {this.state.Contacts.length > 0 &&
              this.state.Contacts != null &&
              this.is_post_loaded == true ? (
                <View
                  style={{
                    height: Height(29),
                    backgroundColor: '#F6F5FB',
                    marginTop: Height(0.7),
                  }}
                >
                  <Text
                    style={{
                      marginTop: Height(2),
                      marginLeft: Width(4.5),
                      color: colors.appColor,
                      fontFamily: fonts.Raleway_Bold,
                      fontSize: FontSize(18),
                    }}
                  >
                    Your Contacts
                  </Text>
                  <View
                    style={{
                      marginTop: '2%',
                      marginLeft: Width(0),
                      marginRight: Width(1),
                      flexDirection: 'row',
                    }}
                  >
                    <ScrollView
                      showsHorizontalScrollIndicator={false}
                      horizontal
                      contentContainerStyle={{ paddingHorizontal: Width(1) }}
                    >
                      {this.state.Contacts.map((item) => {
                        return (
                          <View
                            style={{
                              backgroundColor: colors.white,
                              height:
                                Platform.OS == 'android'
                                  ? Height(20)
                                  : Height(19.3),
                              width: Width(33),
                              borderRadius: Height(2),
                              alignItems: 'center',
                              marginTop: Height(2),
                              marginHorizontal: Width(1),
                              justifyContent: 'center',
                            }}
                          >
                            <View
                              style={{
                                width: Height(7),
                                height: Height(7),
                                borderRadius: Height(7),
                                backgroundColor: '#D6D6D6',
                                justifyContent: 'center',
                              }}
                            >
                              <Text
                                style={[
                                  {
                                    textAlign: 'center',
                                    fontFamily: 15,
                                    fontFamily: 'Raleway-ExtraBold',
                                    letterSpacing: 0.02,
                                  },
                                  checkFontWeight('800'),
                                ]}
                              >
                                {item.initial}
                              </Text>
                            </View>
                            <Text
                              style={{
                                marginTop: Height(1.5),
                                fontSize: FontSize(13),
                                color: colors.dargrey,
                                fontFamily: fonts.Roboto_Medium,
                                marginHorizontal: 5,
                              }}
                              numberOfLines={1}
                            >
                              {item.title}
                            </Text>
                            {/* <Text style={{ marginTop: Height(0.5), fontSize: FontSize(13), color: colors.dargrey, fontFamily: fonts.Roboto_Regular }}>{item.phoneNumbers && item.phoneNumbers[0].number}</Text> */}
                            <TouchableOpacity
                              style={styles.inviteBox}
                              onPress={() =>
                                this.inviteContacts(
                                  item.phoneNumbers &&
                                    item.phoneNumbers[0].number,
                                )
                              }
                            >
                              <Text
                                style={[
                                  {
                                    fontSize: FontSize(13),
                                    fontFamily: fonts.Raleway_Bold,
                                    color: '#ffffff',
                                    textAlign: 'center',
                                  },
                                  checkFontWeight('700'),
                                ]}
                              >
                                Invite
                              </Text>
                            </TouchableOpacity>
                          </View>
                        )
                      })}
                    </ScrollView>
                  </View>
                </View>
              ) : null}
            </View>
          )}
        </Animated.ScrollView>
        {/* <Animated.View style={[styles.navbar, { transform: [{ translateY: navbarTranslate }] }]}> */}
        <View style={[styles.navbar]}>
          {/* <Animated.Image style={[isIphoneX() ? { marginLeft: Width(4), marginTop: Height(5.4) } : { marginLeft: Width(3), marginTop: Height(4.3) }, { opacity: navbarOpacity }]} source={appIcon} />
          <Animated.Text style={[isIphoneX() ? { fontFamily: fonts.Raleway_Bold, marginLeft: Width(1.5), marginTop: Height(4.4), fontSize: FontSize(23), color: 'white' } : { fontFamily: fonts.Raleway_Bold, marginLeft: Width(3), marginTop: Height(3.3), fontSize: FontSize(23), color: 'white' }, { opacity: navbarOpacity }]}>
            planmesh
        </Animated.Text> */}
          <Image
            style={[
              isIphoneX()
                ? { left: 16, bottom: 16, position: 'absolute' }
                : { left: 16, bottom: 16, position: 'absolute' },
            ]}
            source={appIcon}
          />
          <Text
            style={[
              isIphoneX()
                ? {
                    fontFamily: fonts.Raleway_Bold,
                    left: 50,
                    bottom: 16,
                    fontSize: FontSize(23),
                    color: 'white',
                    position: 'absolute',
                  }
                : {
                    fontFamily: fonts.Raleway_Bold,
                    left: 50,
                    bottom: 16,
                    fontSize: FontSize(23),
                    color: 'white',
                    position: 'absolute',
                  },
            ]}
          >
            planmesh
          </Text>
        </View>
        {/* </Animated.View> */}
        {/* {this.state.temp == true ?
          <ActionButton active={true} spacing={Height(1)} onReset={this.colseModel} onPress={this.openModel} renderIcon={() => <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: Height(6), width: Width(30), backgroundColor: '#FF00BA', borderRadius: Width(6), marginBottom: Height(6), marginRight: Width(6) }}>
            <Animated.View style={[animatedStyle]}>
              <Image source={plus_icon} />
            </Animated.View>

            <Text style={{ paddingLeft: Width(1), color: '#FFF', fontSize: FontSize(16), fontFamily: fonts.Raleway_Bold, justifyContent: 'center' }}>Plan</Text>
          </View>} degrees={0} buttonColor={"transparent"} buttonText={"Plan"} buttonTextStyle={{ alignSelf: 'center' }} source={this.state.menuImage} bgColor="rgba(65,25,155,0.9)" offsetX={Width(7)} offsetY={Height(0)} >

            <ActionButton.Item shadowStyle={{ marginTop: Height(-3) }} buttonColor='transparent' textContainerStyle={{ borderWidth: 0, backgroundColor: 'white' }} spaceBetween={2} hideLabelShadow={true} title="Who do you want to see?" textStyle={[{ color: "#FFFFFF", fontFamily: "Raleway-Bold", fontSize: FontSize(16), marginTop: Height(-3) }, checkFontWeight("700")]}
              onPress={() => { this.props.navigation.navigate('Who1',{is_edit_plan:false}) }}>
              <Image source={menu0} />
            </ActionButton.Item>
            <ActionButton.Item shadowStyle={{ marginTop: Height(-3) }} buttonColor='transparent'
              textContainerStyle={{ borderWidth: 0, backgroundColor: 'transparent' }} spaceBetween={2} hideLabelShadow={true} title="What do you want to do?" textStyle={[{ color: "#FFFFFF", fontFamily: "Raleway-Bold", fontSize: FontSize(16), marginTop: Height(-3) }, checkFontWeight("700")]}
              onPress={() => { this.props.navigation.navigate('what1', {is_edit_plan:false})}}>
              <Image source={menu1} />
            </ActionButton.Item>
            <ActionButton.Item shadowStyle={{ marginTop: Height(-3) }} buttonColor='transparent' textContainerStyle={{ borderWidth: 0, backgroundColor: 'transparent' }} spaceBetween={2} hideLabelShadow={true} title="Where do you wanna go?" textStyle={[{ marginTop: Height(-3), color: "#FFFFFF", fontFamily: "Raleway-Bold", fontSize: FontSize(16) }, checkFontWeight("700")]}
              onPress={() => { this.props.navigation.navigate('where1', {is_edit_plan:false})}}>
              <Image source={menu3} />
            </ActionButton.Item>
            <ActionButton.Item shadowStyle={{ marginTop: Height(-3) }} buttonColor='transparent' textContainerStyle={{ borderWidth: 0, backgroundColor: 'transparent' }} spaceBetween={2} hideLabelShadow={true} title="When are you free?" textStyle={[{ color: "#FFFFFF", fontFamily: "Raleway-Bold", fontSize: FontSize(16), marginTop: Height(-3) }, checkFontWeight("700")]}
              onPress={() => { this.props.navigation.navigate('when1',{is_edit_plan:false} ) }}>
              <Image source={menu2} />
            </ActionButton.Item>

          </ActionButton> : null} */}

        {/* {this.state.temp == false ?
          <ActionButton spacing={Height(1)} onReset={this.colseModel} onPress={this.openModel} renderIcon={() => <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: Height(6), width: Width(30), backgroundColor: '#FF00BA', borderRadius: Width(6), bottom: Height(6), right: Width(6), position:'absolute'}}>
            <Animated.View style={[animatedStyle]}>
              <Image source={plus_icon} />
            </Animated.View>

            <Text style={{ paddingLeft: Width(1), color: '#FFF', fontSize: FontSize(16), fontFamily: fonts.Raleway_Bold, justifyContent: 'center' }}>Plan</Text>
          </View>} degrees={0} buttonColor={"transparent"} buttonText={"Plan"} buttonTextStyle={{ alignSelf: 'center' }} source={this.state.menuImage} bgColor="rgba(65,25,155,0.9)" offsetX={Width(7)} offsetY={Height(0)} >

            <ActionButton.Item shadowStyle={{ marginTop: Height(-3) }} buttonColor='transparent' textContainerStyle={{ borderWidth: 0, backgroundColor: 'white' }} spaceBetween={2} hideLabelShadow={true} title="Who do you want to see?" textStyle={[{ color: "#FFFFFF", fontFamily: "Raleway-Bold", fontSize: FontSize(16), marginTop: Height(-3) }, checkFontWeight("700")]}
              onPress={() => { this.props.navigation.navigate('Who1',{is_edit_plan:false}) }}>
              <Image source={menu0} />
            </ActionButton.Item>
            <ActionButton.Item shadowStyle={{ marginTop: Height(-3) }} buttonColor='transparent'
              textContainerStyle={{ borderWidth: 0, backgroundColor: 'transparent' }} spaceBetween={2} hideLabelShadow={true} title="What do you want to do?" textStyle={[{ color: "#FFFFFF", fontFamily: "Raleway-Bold", fontSize: FontSize(16), marginTop: Height(-3) }, checkFontWeight("700")]}
              onPress={() => { this.props.navigation.navigate('what1', {is_edit_plan:false}) }}>
              <Image source={menu1} />
            </ActionButton.Item>
            <ActionButton.Item shadowStyle={{ marginTop: Height(-3) }} buttonColor='transparent' textContainerStyle={{ borderWidth: 0, backgroundColor: 'transparent' }} spaceBetween={2} hideLabelShadow={true} title="Where do you wanna go?" textStyle={[{ marginTop: Height(-3), color: "#FFFFFF", fontFamily: "Raleway-Bold", fontSize: FontSize(16) }, checkFontWeight("700")]}
              onPress={() => { this.props.navigation.navigate('where1', {is_edit_plan:false}) }}>
              <Image source={menu3} />
            </ActionButton.Item>
            <ActionButton.Item shadowStyle={{ marginTop: Height(-3) }} buttonColor='transparent' textContainerStyle={{ borderWidth: 0, backgroundColor: 'transparent' }} spaceBetween={2} hideLabelShadow={true} title="When are you free?" textStyle={[{ color: "#FFFFFF", fontFamily: "Raleway-Bold", fontSize: FontSize(16), marginTop: Height(-3) }, checkFontWeight("700")]}
              onPress={() => { this.props.navigation.navigate('when1', {is_edit_plan:false}) }}>
              <Image source={menu2} />
            </ActionButton.Item>

          </ActionButton> 
          : null} */}

        {
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              height: Platform.OS == 'android' ? 50 : 56,
              width: Width(30),
              backgroundColor: '#FF00BA',
              borderRadius: Width(6),
              bottom: 30,
              right: 20,
              position: 'absolute',
              zIndex: 2,
              shadowColor: '#9E8BF3',
              shadowOffset: {
                width: 2,
                height: 5,
              },
              shadowOpacity: 0.6,
              shadowRadius: 15,
            }}
          >
            <TouchableOpacity
              activeOpacity={0.9}
              style={{ flexDirection: 'row', alignItems: 'center' }}
              onPress={
                this.state.isOpen
                  ? () => this.colseModel()
                  : () => this.openModel()
              }
            >
              <Animated.View style={[animatedStyle]}>
                <Image source={plus_icon} />
              </Animated.View>
              <Text
                style={{
                  marginLeft: 8,
                  color: '#FFF',
                  fontSize: FontSize(16),
                  fontFamily: fonts.Raleway_Bold,
                  justifyContent: 'center',
                }}
              >
                Plan
              </Text>
            </TouchableOpacity>
          </View>
        }

        {this.state.temp == true ? (
          <Modal
            isVisible={this.state.temp}
            style={{ margin: 0 }}
            animationInTiming={5}
          >
            <View
              style={{
                height: Height(100),
                width: Width(100),
                backgroundColor: 'rgba(65,25,155,0.7)',
              }}
            >
              <TouchableOpacity
                style={{
                  width: Width(100),
                  height: Height(100),
                  alignItems: 'center',
                }}
                onPress={() => this.colseModel()}
                activeOpacity={0.9}
              >
                <Text
                  style={[
                    {
                      marginLeft: 35,
                      marginRight: 35,
                      textAlign: 'center',
                      lineHeight: 40,
                      marginTop: '15%',
                      justifyContent: 'center',
                      alignItems: 'center',
                      color: '#ffffff',
                      fontSize: FontSize(26),
                      fontFamily: 'Raleway-Bold',
                    },
                    checkFontWeight('800'),
                  ]}
                >
                  {this.mypostcount == 0
                    ? 'Let’s get started. Make a plan! Select one of the options below'
                    : 'Make a plan! Select one of the options below'}
                </Text>
                <View
                  style={{
                    width: Width(88),
                    height: Height(10),
                    marginTop: Height(4),
                    backgroundColor: 'white',
                    borderRadius: 15,
                  }}
                >
                  <TouchableOpacity
                    activeOpacity={0.9}
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      width: Width(88),
                      height: Height(10),
                      marginLeft: 20,
                    }}
                    onPress={() => {
                      this.colseModel()
                      this.props.navigation.navigate('Who1', {
                        is_edit_plan: false,
                      })
                    }}
                  >
                    <Image source={ic_who} />
                    <Text
                      style={{
                        fontFamily: 'Raleway-Bold',
                        fontSize: FontSize(20),
                        marginLeft: 15,
                        color: colors.dargrey,
                        marginTop: 3,
                      }}
                    >
                      Who do you want to see?
                    </Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    width: Width(88),
                    height: Height(10),
                    marginTop: Height(4),
                    backgroundColor: 'white',
                    borderRadius: 15,
                    flexDirection: 'row',
                  }}
                >
                  <TouchableOpacity
                    activeOpacity={0.9}
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      width: Width(88),
                      height: Height(10),
                      marginLeft: 20,
                    }}
                    onPress={() => {
                      this.colseModel()
                      this.props.navigation.navigate('what1', {
                        is_edit_plan: false,
                      })
                    }}
                  >
                    <Image source={ic_what} />
                    <Text
                      style={{
                        fontFamily: 'Raleway-Bold',
                        fontSize: FontSize(20),
                        marginLeft: 15,
                        color: colors.dargrey,
                        marginTop: 3,
                      }}
                    >
                      What do you want to do?
                    </Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    width: Width(88),
                    height: Height(10),
                    marginTop: Height(4),
                    backgroundColor: 'white',
                    borderRadius: 15,
                    flexDirection: 'row',
                  }}
                >
                  <TouchableOpacity
                    activeOpacity={0.9}
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      height: Height(10),
                      width: Width(88),
                      marginLeft: 20,
                    }}
                    onPress={() => {
                      this.colseModel()
                      this.props.navigation.navigate('when1', {
                        is_edit_plan: false,
                      })
                    }}
                  >
                    <Image source={ic_when} />
                    <Text
                      style={{
                        fontFamily: 'Raleway-Bold',
                        fontSize: FontSize(20),
                        marginLeft: 15,
                        color: colors.dargrey,
                      }}
                    >
                      When are you free?
                    </Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    width: Width(88),
                    height: Height(10),
                    marginTop: Height(4),
                    backgroundColor: 'white',
                    borderRadius: 15,
                    flexDirection: 'row',
                  }}
                >
                  <TouchableOpacity
                    activeOpacity={0.9}
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      width: Width(88),
                      height: Height(10),
                      marginLeft: 20,
                    }}
                    onPress={() => {
                      this.colseModel()
                      this.props.navigation.navigate('where1', {
                        is_edit_plan: false,
                      })
                    }}
                  >
                    <Image source={ic_where} />
                    <Text
                      style={{
                        fontFamily: 'Raleway-Bold',
                        fontSize: FontSize(20),
                        marginLeft: 15,
                        color: colors.dargrey,
                      }}
                    >
                      Where do you wanna go?
                    </Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: Platform.OS == 'android' ? 50 : 56,
                    width: Width(30),
                    backgroundColor: '#FF00BA',
                    borderRadius: Width(6),
                    bottom: Platform.OS == 'ios' ? 113 : 90,
                    right: 20,
                    position: 'absolute',
                    zIndex: 2,
                    shadowColor: '#9E8BF3',
                    shadowOffset: {
                      width: 2,
                      height: 5,
                    },
                    shadowOpacity: 0.6,
                    shadowRadius: 15,
                  }}
                >
                  <TouchableOpacity
                    activeOpacity={0.9}
                    style={{ flexDirection: 'row', alignItems: 'center' }}
                    onPress={
                      this.state.isOpen
                        ? () => this.colseModel()
                        : () => this.openModel()
                    }
                  >
                    <Animated.View style={[animatedStyle]}>
                      <Image source={plus_icon} />
                    </Animated.View>
                    <Text
                      style={{
                        marginLeft: 8,
                        color: '#FFF',
                        fontSize: FontSize(16),
                        fontFamily: fonts.Raleway_Bold,
                        justifyContent: 'center',
                      }}
                    >
                      Plan
                    </Text>
                  </TouchableOpacity>
                </View>
              </TouchableOpacity>
            </View>
          </Modal>
        ) : null}

        {/* {
          this.state.isOpen == true?
            <Text style={[{ marginLeft: 35, marginRight: 35, textAlign: 'center',lineHeight:40,top: '10%', position: "absolute", justifyContent: 'center', alignItems: 'center', color: '#ffffff', fontSize: FontSize(26), fontFamily: "Raleway-Bold" }, checkFontWeight("800")]}>
              Let’s get started. Plan something! Select one of the options below
              </Text>
            : null
        } */}

        <SafeAreaView />

        <Modal
          isVisible={isModalVisible}
          style={{ margin: 0 }}
          onBackdropPress={() => this.setState({ isModalVisible: false })}
          onBackButtonPress={() => this.setState({ isModalVisible: false })}
        >
          {/* <View style={{ height: this.state.currentitem.is_friend == 1 ? Height(37) : Height(27.5), width: '100%', position: 'absolute', bottom: 0, backgroundColor: '#FFF' }}> */}
          <View
            style={{
              width: '100%',
              position: 'absolute',
              bottom: 0,
              backgroundColor: '#FFF',
            }}
          >
            {this.state.currentitem.start_date != '' ? (
              <View style={{ justifyContent: 'center', height: Height(7) }}>
                <Text
                  onPress={this.addToCalendar}
                  style={{
                    marginLeft: Width(3),
                    fontFamily: fonts.Roboto_Regular,
                    fontSize: FontSize(18),
                    color: colors.dargrey,
                  }}
                >
                  {'Add to calendar'}
                </Text>
              </View>
            ) : null}
            {this.state.currentitem.start_date != '' ? (
              <View
                style={{ borderBottomColor: '#ccc', borderBottomWidth: 1 }}
              />
            ) : null}

            <View style={{ justifyContent: 'center', height: Height(7) }}>
              <Text
                onPress={this.gotoHide}
                style={{
                  marginLeft: Width(3),
                  fontFamily: fonts.Roboto_Regular,
                  fontSize: FontSize(18),
                  color: colors.dargrey,
                }}
              >
                {'Hide from feed'}
              </Text>
            </View>

            {this.state.currentitem.is_friend == 1 ? (
              <View
                style={{ borderBottomColor: '#ccc', borderBottomWidth: 1 }}
              />
            ) : null}
            {this.state.currentitem.is_friend == 1 ? (
              <View style={{ justifyContent: 'center', height: Height(7) }}>
                <Text
                  onPress={this.gotoUnFriend}
                  style={{
                    marginLeft: Width(3),
                    fontFamily: fonts.Roboto_Regular,
                    fontSize: FontSize(18),
                    color: colors.dargrey,
                  }}
                >
                  Unfriend {this.state.post_name}
                </Text>
              </View>
            ) : null}
            <View style={{ borderBottomColor: '#ccc', borderBottomWidth: 1 }} />
            <View style={{ justifyContent: 'center', height: Height(7) }}>
              <Text
                onPress={this.gotoBlock}
                style={{
                  marginLeft: Width(3),
                  fontFamily: fonts.Roboto_Regular,
                  fontSize: FontSize(18),
                  color: colors.dargrey,
                }}
              >
                Block {this.state.post_name}
              </Text>
            </View>
            <View style={{ borderBottomColor: '#ccc', borderBottomWidth: 1 }} />
            <View style={{ justifyContent: 'center', height: Height(7) }}>
              <Text
                onPress={this.gotoreportUser}
                style={{
                  marginLeft: Width(3),
                  fontFamily: fonts.Roboto_Regular,
                  fontSize: FontSize(18),
                  color: colors.dargrey,
                }}
              >
                Report Post
              </Text>
            </View>
            <View style={{ borderBottomColor: '#ccc', borderBottomWidth: 1 }} />
            <View style={{ height: Height(7), flexDirection: 'row' }}>
              <Text
                style={{
                  marginLeft: Width(3),
                  fontFamily: fonts.Roboto_Regular,
                  fontSize: FontSize(18),
                  color: colors.dargrey,
                  width: Width(30),
                  marginTop: 10,
                }}
              >
                Mute
              </Text>
              {Platform.OS == 'ios' ? (
                <Switch
                  style={{ position: 'absolute', right: 20, top: 10 }}
                  trackColor={{ true: '#4BD964' }}
                  ios_backgroundColor={'#E4E4E5'}
                  value={this.state.is_muted}
                  onValueChange={(is_muted) =>
                    this.setState({ is_muted }, () =>
                      this.onMuteAction(this.state.is_muted),
                    )
                  }
                />
              ) : (
                <Switch
                  style={{ position: 'absolute', right: 20, top: 10 }}
                  trackColor={{ true: '#4BD964', false: '#ccc' }}
                  thumbTintColor="#ffffff"
                  value={this.state.is_muted}
                  onValueChange={(is_muted) =>
                    this.setState({ is_muted }, () =>
                      this.onMuteAction(this.state.is_muted),
                    )
                  }
                />
              )}
            </View>
          </View>
        </Modal>
        <Modal
          isVisible={isMyDeleteModalVisible}
          style={{ margin: 0 }}
          onBackdropPress={() =>
            this.setState({ isMyDeleteModalVisible: false })
          }
          onBackButtonPress={() =>
            this.setState({ isMyDeleteModalVisible: false })
          }
        >
          <View
            style={{
              height: Height(15),
              width: '100%',
              position: 'absolute',
              bottom: 0,
              backgroundColor: '#FFF',
            }}
          >
            <View style={{ justifyContent: 'center', height: Height(7) }}>
              <Text
                onPress={() => this.gotoEventDetail()}
                style={{
                  marginLeft: Width(3),
                  fontFamily: fonts.Roboto_Regular,
                  fontSize: FontSize(18),
                  color: colors.dargrey,
                }}
              >
                Edit Plan
              </Text>
            </View>
            <View style={{ borderBottomColor: '#ccc', borderBottomWidth: 1 }} />
            <View style={{ justifyContent: 'center', height: Height(7) }}>
              <Text
                onPress={() => {
                  Alert.alert(
                    'Cancel Event',
                    'Guests will be notified that this event was canceled',
                    [
                      {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                      },
                      { text: 'OK', onPress: this.deleteplan },
                    ],
                    { cancelable: false },
                  )
                }}
                style={{
                  marginLeft: Width(3),
                  fontFamily: fonts.Roboto_Regular,
                  fontSize: FontSize(18),
                  color: colors.dargrey,
                }}
              >
                Cancel
              </Text>
            </View>
          </View>
        </Modal>
        <Modal
          isVisible={isMyModalVisible}
          style={{ margin: 0 }}
          onBackdropPress={() => this.setState({ isMyModalVisible: false })}
          onBackButtonPress={() => this.setState({ isMyModalVisible: false })}
        >
          {/* <View style={{ height: Height(21.5), width: '100%', position: 'absolute', bottom: 0, backgroundColor: '#FFF' }}> */}
          <View
            style={{
              width: '100%',
              position: 'absolute',
              bottom: 0,
              backgroundColor: '#FFF',
            }}
          >
            {this.state.currentitem.start_date != '' ? (
              <View style={{ justifyContent: 'center', height: Height(7) }}>
                <Text
                  onPress={this.addToCalendar}
                  style={{
                    marginLeft: Width(3),
                    fontFamily: fonts.Roboto_Regular,
                    fontSize: FontSize(18),
                    color: colors.dargrey,
                  }}
                >
                  {'Add to calendar'}
                </Text>
              </View>
            ) : null}
            {this.state.currentitem.start_date != '' ? (
              <View
                style={{ borderBottomColor: '#ccc', borderBottomWidth: 1 }}
              />
            ) : null}
            <View style={{ justifyContent: 'center', height: Height(7) }}>
              <Text
                onPress={() => this.gotoEventDetail()}
                style={{
                  marginLeft: Width(3),
                  fontFamily: fonts.Roboto_Regular,
                  fontSize: FontSize(18),
                  color: colors.dargrey,
                }}
              >
                Edit Plan
              </Text>
            </View>
            <View style={{ borderBottomColor: '#ccc', borderBottomWidth: 1 }} />
            <View style={{ justifyContent: 'center', height: Height(7) }}>
              <Text
                onPress={() => {
                  this.setState({ isMyModalVisible: false })
                  this.props.navigation.navigate('EditPrivacy', {
                    plan_id: this.state.plan_id,
                    privacy: this.state.currentitem.share_with,
                  })
                }}
                style={{
                  marginLeft: Width(3),
                  fontFamily: fonts.Roboto_Regular,
                  fontSize: FontSize(18),
                  color: colors.dargrey,
                }}
              >
                Edit Privacy
              </Text>
            </View>
            <View style={{ borderBottomColor: '#ccc', borderBottomWidth: 1 }} />
            <View style={{ justifyContent: 'center', height: Height(7) }}>
              <Text
                onPress={() => {
                  Alert.alert(
                    'Cancel Event',
                    'Guests will be notified that this event was canceled',
                    [
                      {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                      },
                      { text: 'OK', onPress: this.deleteplan },
                    ],
                    { cancelable: false },
                  )
                }}
                style={{
                  marginLeft: Width(3),
                  fontFamily: fonts.Roboto_Regular,
                  fontSize: FontSize(18),
                  color: colors.dargrey,
                }}
              >
                Cancel
              </Text>
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  fill: {
    flex: 1,
    backgroundColor: '#EEEEEE',
  },
  navbar: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: colors.appColor,
    flexDirection: 'row',
    alignItems: 'center',
    height: NAVBAR_HEIGHT,
  },

  contentContainer: {
    //marginTop: NAVBAR_HEIGHT,
  },
  title: {
    marginTop: Height(1),
    color: '#fff',
    fontSize: FontSize(30),
    marginLeft: Width(3),
  },
  row: {
    height: 300,
    width: null,
    marginBottom: 1,
    padding: 16,
    backgroundColor: 'transparent',
  },
  rowText: {
    color: 'white',
    fontSize: 18,
  },
  textInputIos: {
    height: 18,
    width: 14,
    marginTop: '2%',
    marginRight: '2%',
  },
  textInputAndroid: {
    marginTop: '2%',
    marginRight: '2%',
  },
  headerPlaneColor: {
    justifyContent: 'center',
    fontSize: FontSize(12),
    marginLeft: 3,
    color: '#8F8F90',
    fontFamily: fonts.Roboto_Regular,
  },
  inviteBox: {
    marginTop: Height(2),
    height: Height(3.5),
    width: Width(26),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: Height(2),
    backgroundColor: colors.pink,
  },
  inviteBox1: {
    marginTop: Height(2),
    height: Height(3.5),
    width: Width(26),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: Height(2),
    backgroundColor: '#7c7c7e',
  },
})

export default Dashboard
