import React, { Component } from "react";
import { SafeAreaView, Dimensions, Image, Platform, Keyboard, ImageBackground, TouchableOpacity, StatusBar, FlatList, ScrollView } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';

import { Container, View, Text, Button, Header, Left, Right, Content, Item, Input, Icon, Switch, List, ListItem, Body, } from "native-base";
import styles from "./style";
import * as Pref from '../../Pref/Pref';
import ValidationComponent from 'react-native-form-validator';
import FloatingLabel from 'react-native-floating-labels';
import Toast from 'react-native-simple-toast';

let deviceWidth = Dimensions.get('window').width
let deviceHeight = Dimensions.get('window').height
import { Height, FontSize, Width, colors } from "../../config/dimensions";
import SendSMS from 'react-native-sms'
import NavigationService from "../../services/NavigationService";
import { store } from "../../redux/store";
import { TextInput } from "react-native-gesture-handler";
//import { parsePhoneNumberFromString } from 'libphonenumber-js'
import { SearchBar } from 'react-native-elements';
import { fonts } from "../../config/constant";
import Ionicons from 'react-native-vector-icons/Ionicons';
import MixpanelManager from '../../../Analytics'

const backWhite = require("../../../assets/images/backWhite.png");
const btnBg = require("../../../assets/images/btnBg.png");
const right_arrow = require("../../../assets/images/right_arrow.png");
const unSelectRadio = require("../../../assets/images/unSelectRadio.png");
const selectRadio = require("../../../assets/images/selectRadio.png");
const closeicon = require("../../../assets/images/Dashboard/close.png");
const search_img = require("../../../assets/images/Dashboard/search.png");
class MyListItem extends React.PureComponent {
    _onPress = () => {
        this.props.onPressItem(this.props.id);
    };

    formatPhoneNumber(str) {
        let cleaned = ('' + str).replace(/\D/g, '');

        let match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);

        if (match) {
            let intlCode = (match[1] ? '+1 ' : '')
            return [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('')
        }
        return null;
    }

    render() {
        console.log(this.props.item)
        let item = this.props.item
        const textColor = this.props.selected ? true : false;

        var number = ''
        if (item.phoneNumbers && item.phoneNumbers[0].number) {
            number = this.formatPhoneNumber(item.phoneNumbers[0].number)

            if (number == null) {
                number = item.phoneNumbers && item.phoneNumbers[0].number
            }

        }

        //     return 
        //      item.header==true?<ListItem itemDivider style={{ marginLeft: 0, backgroundColor: '#DCCFFA' }}>
        //      <Left style={{ alignItems: 'flex-start' }}>
        //          <Text style={[{ color: "#41199b", textAlign: 'left', fontFamily: 16, fontFamily: "Raleway-ExtraBold", letterSpacing: 0.02 }, checkFontWeight("800")]}>{item.initial}</Text>
        //      </Left>
        //      <Body />
        //      <Right />
        //  </ListItem>:
        return <TouchableOpacity key={this.props.id} onPress={() => this._onPress()}>
            <View style={{ flexDirection: 'row', height: 55, flex: 1, borderBottomColor: "#3A4759", borderBottomWidth: 0.0 }}>
                <View style={{ paddingLeft: 15, alignItems: 'flex-start', justifyContent: 'center' }}>
                    <View style={{ width: 38, height: 38, borderRadius: 19, backgroundColor: "#D6D6D6", justifyContent: 'center' }}>
                        <Text style={[{ textAlign: 'center', fontFamily: 15, fontFamily: "Raleway-ExtraBold", letterSpacing: 0.02 }, checkFontWeight("800")]}>{item.initial}</Text>
                    </View>
                </View>
                <View style={{ paddingLeft: 15, alignItems: 'flex-start', justifyContent: 'center', flex: 1 }}>
                    <Text style={[{ color: "#363169", fontSize: 16, fontFamily: "Roboto-Medium", letterSpacing: 0.02 }]}>{item.title}</Text>
                    <Text style={[{ color: colors.fontDarkGrey, fontSize: 13, fontFamily: "Roboto-Light" }]}>{number}</Text>
                </View>
                <View style={{ alignItems: 'flex-end', justifyContent: 'center', marginRight: 17 }}>
                    <Image source={textColor == false ? unSelectRadio : selectRadio} style={{ width: 25, height: 25 }}></Image>
                </View>
            </View>
        </TouchableOpacity>
    }
}

class sendFriendRequest extends ValidationComponent {

    constructor(props) {
        super(props);
        this.search = null,
            issearchingcontact = false,
            this.contactsearch = null,
            this.state = {
                Data: [{ header: true, initial: "A", id: 0 },
                { select: false, title: "Ahdieh Ashrafi 1Fall11", initial: "AA" },
                { select: false, title: "Ahdieh Ashrafi 1Fall11", initial: "AA" },
                { select: true, title: "Ahdieh Ashrafi 1Fall11", initial: "AA" },
                { select: false, title: "Ahdieh Ashrafi 1Fall11", initial: "AA" },
                { select: false, title: "Ahdieh Ashrafi 1Fall11", initial: "AA" },
                { select: false, title: "Ahdieh Ashrafi 1Fall11", initial: "AA" },
                { select: false, title: "Ahdieh Ashrafi 1Fall11", initial: "AA" }],
                selectAll: false,
                referral_url: store.getState().auth.user.referral_url,
                selected: (new Map(): Map<string, boolean>),
                is_skip_visible: this.props.navigation.state.params.is_skip_visible,
                is_close_visible: false,
                text: '',
                searchcontactdata: [],
                selectedcontacts: [],
                invitedcontacts: [],
                selectAllContact: false,
                contacttext: '',
            };
        this.mixpanel = MixpanelManager.sharedInstance.mixpanel;
    }

    // static navigationOptions = ({ navigation }) => {
    //     theNavigation = navigation;

    //     return {
    //         header: (
    //             <Header style={styles.headerAndroidnav}>
    //                 <StatusBar barStyle="light-content" backgroundColor="#41199B" />
    //                 <Left style={{ alignItems: 'flex-start', justifyContent: 'flex-start', flex: 0.2, }} >
    //                     <Button transparent onPress={() => {
    //                         navigation.goBack()
    //                     }} style={{ width: Width(9), height: Height(3),marginLeft:Width(2) }} >
    //                         <Image source={backWhite} style={{ tintColor: "white" }} />
    //                     </Button>
    //                 </Left>
    //                 <View style={{ alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', flex: 0.6 }}>
    //                     <Text style={[styles.headerTitle]} >Invite Friends</Text>
    //                 </View>
    //                 {
    //                     navigation.state.params.is_skip_visible == true ?
    //                     <Right style={{ alignItems: 'flex-end', justifyContent: 'flex-end', flex: 0.25 }}>
    //                         <Button transparent style={{ alignSelf: 'baseline' }} onPress={() => { {
    //                                 AsyncStorage.setItem('overlay','true')
    //                                 NavigationService.reset('TabNavigator')}
    //                             }}><Text style={{ color: 'white', opacity:0.5 }}>Skip</Text>
    //                         </Button>
    //                     </Right>
    //                     : 
    //                     <Right style={{ alignItems: 'flex-end', justifyContent: 'flex-end', flex: 0.25 }}>
    //                         <Button transparent style={{ alignSelf: 'baseline' }}><Text style={{ color: 'white', opacity:0.5 }}>    </Text>
    //                         </Button>
    //                     </Right>
    //                 }

    //             </Header>
    //         )
    //     }
    // }


    static navigationOptions = ({ navigation }) => {
        theNavigation = navigation;
        return {
            header: (
                <Header style={[styles.headerAndroidnav, { height: 44 }]}>
                    <StatusBar barStyle="light-content" backgroundColor="#41199B" />
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', left: Width(4) }}>
                        <TouchableOpacity onPress={() => {
                            navigation.goBack()
                        }}>
                            <Image source={backWhite} style={{ tintColor: "white" }} />
                        </TouchableOpacity></View>
                    <View style={{ bottom: Height(2), position: 'absolute', alignItems: 'center' }}>
                        <Text style={styles.headerTitle} >Invite Friends</Text>
                    </View>
                    <View style={{ flexDirection: 'row', bottom: Height(2), position: 'absolute', right: Width(4) }}>

                        {
                            navigation.state.params.is_skip_visible == true ?
                                <TouchableOpacity onPress={() => {
                                    {
                                        let mixpanel = MixpanelManager.sharedInstance.mixpanel;
                                        const MixpanelData = {
                                            'from': 'invite',
                                            '$email': store.getState().auth.user.email_address
                                        }
                                        mixpanel.track('Sign Up Complete', MixpanelData);
                                        AsyncStorage.setItem('overlay', 'true')
                                        NavigationService.reset('TabNavigator')
                                    }
                                    // this.mixpanel.timeEvent('signup_complete');    
                                }}>
                                    <Text style={{ color: 'white', fontSize: FontSize(15) }}>Done</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity>
                                    <Text style={{ color: 'white', opacity: 0.5 }}>    </Text>
                                </TouchableOpacity>
                        }
                    </View>

                </Header>

            )
        }
    }


    componentDidMount() {
        //alert('l')
        //this.contactsearch.focus()

        var array = theNavigation.getParam('selectedArray')
        //alert(JSON.stringify(array))
        var convertedArray = this.convertArrayToSectionArray(array)
        //console.log(convertedArray)
        //alert(JSON.stringify(convertedArray))
        this.setState({
            Data: convertedArray,
            originalData: array
        })
        //this.wscall(theNavigation.getParam('selectedId'))
    }

    convertArrayToSectionArray(array) {
        var sectionArray = []
        var finalData = []
        let i = 0;
        array.map((item, index) => {

            item["id"] = i;
            i++;
            finalData.push(item)

            // if (sectionArray.indexOf(item.sectionStr) == -1) {
            //     sectionArray.push(item.sectionStr)
            //     finalData.push({ header: false, initial: item.sectionStr, title: item.sectionStr })
            //     data = array.filter(function (items) {
            //         return items.title.substring(0, 1) == item.sectionStr;
            //     }).map(function (itemss) {
            //         return itemss;
            //     });
            //     data.map((item) => {
            //         item["id"] = i;
            //         i++;
            //         finalData.push(item)
            //     })
            // }
        });
        return finalData.sort((a, b) => a.title > b.title)
    }


    _onPressItem = (id) => {
        // updater functions are preferred for transactional updates
        this.setState((state) => {
            // copy the map rather than modifying state.
            const selected = new Map(state.selected);
            selected.set(id, !selected.get(id)); // toggle
            return { selected };
        });
    };

    _renderItem(item, index) {
        // <MyListItem
        //     id={item.id}
        //     onPressItem={this._onPressItem}
        //     selected={!!this.state.selected.get(item.id)}
        //     title={item.title}
        //     item={item}
        // />

        return (this.contactCellList(item, index));
    }

    formatPhoneNumber(str) {
        let cleaned = ('' + str).replace(/\D/g, '');

        let match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);

        if (match) {
            let intlCode = (match[1] ? '+1 ' : '')
            return [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('')
        }
        return null;
    }

    contactCellList = (item, index) => {

        var number = ''
        if (item.phoneNumbers && item.phoneNumbers[0].number) {
            number = this.formatPhoneNumber(item.phoneNumbers[0].number)

            if (number == null) {
                number = item.phoneNumbers && item.phoneNumbers[0].number
            }

        }

        return (
            <TouchableOpacity onPress={() => { this.onSelectContact(item, index) }}>
                <View style={{ flexDirection: 'row', height: 55, flex: 1, }}>
                    <View style={{ paddingLeft: 15, alignItems: 'flex-start', justifyContent: 'center' }}>
                        <View style={{ width: 38, height: 38, borderRadius: 19, backgroundColor: "#D6D6D6", justifyContent: 'center' }}>
                            <Text style={[{ textAlign: 'center', fontFamily: 15, fontFamily: "Raleway-ExtraBold", letterSpacing: 0.02 }, checkFontWeight("800")]}>{item.initial}</Text>
                        </View>
                    </View>
                    <View style={{ paddingLeft: 15, alignItems: 'flex-start', justifyContent: 'center', flex: 1 }}>
                        <Text style={[{ color: "#363169", fontFamily: 16, fontFamily: "Roboto-Bold", letterSpacing: 0.02 }]}>{item.title}</Text>
                        <Text style={[{ color: "#363169", fontFamily: 16, fontFamily: "Roboto-Medium", letterSpacing: 0.02 }, checkFontWeight("300")]}>{number}</Text>

                    </View>

                    {
                        this.state.invitedcontacts.length > 0 ?
                            <View style={{ alignItems: 'flex-end', justifyContent: 'center', marginRight: 17 }}>
                                {/* <Image source={item.select == false ? unSelectRadio : selectRadio} style={{ width: 25, height: 25 }}></Image> */}
                                {item.select == true ?
                                    <Text style={{ color: colors.dargrey, fontSize: FontSize(13), fontWeight: '400' }}>Invited</Text>
                                    :
                                    <Image source={item.select == false ? unSelectRadio : selectRadio} style={{ width: 25, height: 25 }}></Image>
                                }
                            </View>
                            :
                            <View style={{ alignItems: 'flex-end', justifyContent: 'center', marginRight: 17 }}>
                                <Image source={item.select == false ? unSelectRadio : selectRadio} style={{ width: 25, height: 25 }}></Image>
                            </View>
                    }

                    {/* <View style={{ alignItems: 'flex-end', justifyContent: 'center', marginRight: 17 }}>
                        <Image source={item.select == false ? unSelectRadio : selectRadio} style={{ width: 25, height: 25 }}></Image>
                    </View> */}
                </View>
            </TouchableOpacity>
        )
    }

    onSelectContact(item, index) {
        console.log('onSelectContact')
        if (this.issearchingcontact == true) {
            var dataArray = this.state.searchcontactdata;
            var facet = dataArray[index]
            if (facet.select == true) {
                facet.select = false
                var list = this.state.selectedcontacts
                list.pop(facet.phoneNumbers[0].number)
                dataArray.splice(index, 1, facet)
                this.setState({ selectedcontacts: list, searchcontactdata: dataArray })
            } else {
                facet.select = true
                var list = this.state.selectedcontacts
                list.push(facet.phoneNumbers[0].number)
                dataArray.splice(index, 1, facet)
                this.setState({ selectedcontacts: list, searchcontactdata: dataArray })
            }
        }
        else {
            var dataArray = this.state.Data;
            var facet = dataArray[index]
            if (facet.select == true) {
                facet.select = false
                var list = this.state.selectedcontacts
                list.pop(facet.phoneNumbers[0].number)
                dataArray.splice(index, 1, facet)
                this.setState({ selectedcontacts: list, Data: dataArray })
            } else {
                facet.select = true
                var list = this.state.selectedcontacts
                list.push(facet.phoneNumbers[0].number)
                dataArray.splice(index, 1, facet)
                this.setState({ selectedcontacts: list, Data: dataArray })
            }
        }
        //this.forceUpdate()
        //this.checkIfAllContactsSelected()
    }

    checkIfAllContactsSelected() {
        var allselected = true
        this.state.Data.map((facet) => {
            if (facet.select == false || facet.select == null) {
                allselected = false
            }
        });

        if (allselected == true) {
            this.setState({ selectAllContact: true })
        }
        else {
            this.setState({ selectAllContact: false })
        }
    }

    onSelectAllContacts(value) {
        this.setState({ selectedcontacts: [] })

        if (this.issearchingcontact == true) {
            var list = this.state.selectedcontacts

            this.state.searchcontactdata.map((facet) => {
                facet.select = value.value

                if (facet.select == true) {
                    list.push(facet.phoneNumbers[0].number)
                }
                else {
                    list.pop(facet.phoneNumbers[0].number)
                }
                // this.setState({ data: facet })
            });

            if (value.value == true) {
                this.setState({ selectAllContact: true, selectedcontacts: list })
            } else {
                this.setState({ selectAllContact: false, selectedcontacts: list })
            }
        }
        else {
            var list = this.state.selectedcontacts

            this.state.Data.map((facet) => {
                facet.select = value.value

                if (facet.select == true) {
                    list.push(facet.phoneNumbers[0].number)
                }
                else {
                    list.pop(facet.phoneNumbers[0].number)
                }
                //this.setState({ data: facet })
            });

            if (value.value == true) {
                this.setState({ selectAllContact: true, selectedcontacts: list })
            } else {
                this.setState({ selectAllContact: false, selectedcontacts: list })
            }
        }
    }


    onSelectList(facet, index) {
        let data = this.state.Data
        if (facet.select == true) {
            //facet.select = false
            data[index].select = false
        } else {
            // facet.select = true
            data[index].select = true
        }
        this.setState({ Data: data })
    }
    // onSelectAll(value) {
    //     const selected = new Map(this.state.selected);
    //     this.state.Data.map((facet) => {
    //         if(value.value == true){
    //             selected.set(facet.id, true);
    //         }
    //         else{
    //             selected.set(facet.id, false);
    //         }

    //     });

    //     if (value.value == true) {
    //         this.setState({ selectAll: true,selected })
    //     } else {
    //         this.setState({ selectAll: false,selected })
    //     }
    // }

    onSelectAll(value) {

        this.setState({ selectedcontacts: [] })

        if (this.issearchingcontact == true) {
            var list = this.state.selectedcontacts

            this.state.searchcontactdata.map((facet) => {
                facet.select = value.value

                if (facet.select == true) {
                    list.push(facet.phoneNumbers[0].number)
                }
                else {
                    list.pop(facet.phoneNumbers[0].number)
                }
                //this.setState({ data: facet })
            });

            if (value.value == true) {
                this.setState({ selectAllContact: true, selectedcontacts: list })
            } else {
                this.setState({ selectAllContact: false, selectedcontacts: list })
            }
        }
        else {
            var list = this.state.selectedcontacts

            this.state.Data.map((facet) => {
                facet.select = value.value

                if (facet.select == true) {
                    list.push(facet.phoneNumbers[0].number)
                }
                else {
                    list.pop(facet.phoneNumbers[0].number)
                }
                //this.setState({ data: facet })
            });

            if (value.value == true) {
                this.setState({ selectAllContact: true, selectedcontacts: list })
            } else {
                this.setState({ selectAllContact: false, selectedcontacts: list })
            }
        }


    }

    handleChange = (searchKey) => {
        //console.log(searchKey.searchKey)

        this.setState({ is_close_visible: true })
        var tempData = []
        this.state.originalData.map((contactData) => {

            if (contactData.title.toLowerCase().includes(searchKey.searchKey.toLowerCase())) {
                tempData.push(contactData)
            }
        });
        console.log(tempData)
        var convertedArray = this.convertArrayToSectionArray(tempData)

        this.setState({
            Data: convertedArray
        })
    }

    SearchContactFunction(text) {
        this.setState({ contacttext: text })

        if (this.state.Data.length > 0) {
            if (text != '') {
                this.issearchingcontact = true
                var filteredarray = []
                this.state.Data.map((facet) => {
                    if (facet.title.toLowerCase().includes(text.toLowerCase())) {
                        filteredarray.push(facet)
                    }
                });
                this.setState({ searchcontactdata: filteredarray })
                //this.forceUpdate()
            }
            else {
                this.issearchingcontact = false
                //this.forceUpdate()
            }

            if (this.state.contacttext.length == 1) {
                this.issearchingcontact = false
                //this.forceUpdate()
            }
        }
    }

    onClearContactText = () => {

        this.issearchingcontact = false
        this.setState({ contacttext: '' })
        this.contactsearch.clear()
        setTimeout(() => {
            this.contactsearch.blur()

        }, 100)
    }

    onClearTextPressed() {
        this.search.clear()
        this.setState({ is_close_visible: false, Data: this.state.originalData })
    }

    // SearchFilterFunction(text)
    // {
    //     this.setState({is_close_visible:true,text:text})

    //     if(text == '')
    //     {
    //         this.setState({is_close_visible:false})
    //     }

    // }


    onInviteClick = () => {
        if (this.state.selectedcontacts.length > 0) {
            const MixpanelData = {
                'Invites Sent': this.state.selectedcontacts.length
            }
            this.mixpanel.identify(store.getState().auth.user.email_address)
            this.mixpanel.people.increment("Total Invites Sent", 1);
            this.mixpanel.track('Invite Friends', MixpanelData)
            SendSMS.send({
                //body: `Hey! I'm inviting you to Planmesh. Use my referral code E6C0AJ to register and own a piece of Planmesh. Download Now!`+this.state.referral_url,
                body: 'Hey! Check out Planmesh. Your space to make plans with the people you care about. Get it free at \n iOS: https://apple.co/3r9BwWO \n Android: http://bit.ly/2Xx7zT7',
                recipients: this.state.selectedcontacts,
                successTypes: ['sent', 'queued'],
                allowAndroidSendWithoutReadPermission: true
            }, (completed, cancelled, error) => {
                if (completed) {
                    //Toast.show("Invitation send successfully")
                    //NavigationService.reset('dashboard')
                    this.setState({ invitedcontacts: this.state.selectedcontacts })
                }
                else if (cancelled) {
                    //Toast.show("Invite send successfully")

                }
                else {
                    //alert(error)
                }
            });
        }

        // var selectedArray = []
        // //alert(JSON.stringify(this.state.selected))
        // this.state.originalData.map((contactData) => {
        //    if (!!this.state.selected.get(contactData.id)) {
        //         selectedArray.push(contactData)
        //     }
        // })
        // if(selectedArray.length >= 1){
        //     let mobile=[]
        //     selectedArray.map(mob=>{
        //         mob.phoneNumbers?mob.phoneNumbers.map((mob1)=>{
        //             mobile.push(mob1.number)
        //             return mob1.number
        //         }):''
        //         return mob
        //     })
        //     //alert(JSON.stringify(mobile))
        //     //this.wscall(selectedArray);
        //     SendSMS.send({
        //         //body: `Hey! I'm inviting you to Planmesh. Use my referral code E6C0AJ to register and own a piece of Planmesh. Download Now!`+this.state.referral_url,
        //         body:'Check out Planmesh, your space to make plans with the people I care about. Get it free at',
        //         recipients: mobile,
        //         successTypes: ['sent', 'queued'],
        //         allowAndroidSendWithoutReadPermission: true
        //     }, (completed, cancelled, error) => {
        //         if(completed){
        //             Toast.show("Invite send successfully")
        //             //NavigationService.reset('dashboard')
        //         }
        //         else if(cancelled){
        //             //Toast.show("Invite send successfully")

        //         }
        //         else{
        //             alert(error)
        //         }
        //         console.log('SMS Callback: completed: ' + completed + ' cancelled: ' + cancelled + 'error: ' + error);

        //     });
        // }else{
        //     this.refs.toast.show("Select at least 1 contact person.");
        // }
    }

    wscall = async (selectedArray) => {

        let httpMethod = 'POST'
        let strURL = inviteFrined
        let headers = {
            'Content-Type': 'application/json',
            'userid': userid,
        }
        let params = {
            'invitation': selectedArray,
        }

        APICall.callWebService(httpMethod, strURL, headers, params, (resJSON) => {
            console.log('Login Screen -> ');
            console.log(resJSON)
            if (resJSON.status == 200) {
                this.refs.toast.show(resJSON.message);
                //this.props.navigation.push('dashboard')
                return
            } else {
                this.refs.toast.show(resJSON.message);
            }
        })
    }

    _onMomentumScrollBegin = () => {
        Keyboard.dismiss()
    };

    render() {
        return (
            <Container flex-direction={"row"} style={{ flex: 1, backgroundColor: "#fff" }} >
                <Toastt ref="toast"></Toastt>
                <SafeAreaView />
                {/* <View style={{ height: 68, backgroundColor: "#fff", paddingLeft: 15, paddingRight: 15, justifyContent: 'center' }}>
                    <Item style={{ backgroundColor: colors.lightgrey, height: 40, borderRadius: 22, justifyContent: 'center' }}>
                        <Icon name="ios-search" style={{ marginLeft: 45, color: "#B4B7BA",marginTop:5 }} />
                        <TextInput onChangeText={(searchKey) => this.handleChange({ searchKey })} placeholderTextColor="#b4b7ba" placeholder="Search" style={{ fontFamily: "Roboto-Regular", fontSize: 15, letterSpacing: 0.005, color: "#b4b7ba", marginRight:45, width:'85%'}} ref={search => this.search = search}/>
                        {
                            this.state.is_close_visible == true ?
                            <TouchableOpacity style ={{justifyContent:'center',alignItems:'center',height:25,width:25, position:'absolute',right:10}} onPress={ () => this.onClearTextPressed()}>
                                <Image source={closeicon} resizeMode = 'stretch' style = {{height:17,width:17}}/>
                            </TouchableOpacity>
                            :
                            null
                        }
                    </Item>
                </View> */}

                {/* <View style={{ backgroundColor: '#fff', width: Width(100), height: Height(5) }}> */}
                <SearchBar
                    containerStyle={{ backgroundColor: "#fff", borderBottomColor: 'transparent', borderTopColor: 'transparent', fontFamily: fonts.Roboto_Regular }}
                    inputContainerStyle={{ borderBottomColor: 'transparent', borderTopColor: 'transparent', backgroundColor: '#EEEEEE', width: Width(90), marginLeft: Width(2.5), alignItems: 'center', fontFamily: fonts.Roboto_Regular }}
                    placeholderTextColor='#8F8F90'
                    inputStyle={{ fontFamily: fonts.Roboto_Regular, fontSize: FontSize(16) }}
                    placeholder="Search "
                    round={true}
                    ref={contactsearch => this.contactsearch = contactsearch}
                    onChangeText={text => this.SearchContactFunction(text)}
                    value={this.state.contacttext}
                    searchIcon={() => <Image source={search_img} style={{ tintColor: '#B4B7BA', marginLeft: Width(1) }} />}
                    clearIcon={() => <TouchableOpacity
                        // style={{width:80,backgroundColor:'red',
                        //                                 height:50,
                        //                                 alignItems:'flex-end'
                        //                                 }} 
                        onPress={() => this.onClearContactText()}><Ionicons onPress={() => this.onClearContactText()} name='md-close-circle' color='#B4B7BA' size={22} /></TouchableOpacity>}
                />
                {/* </View> */}

                {/* <View style={{ height: Height(0.2), backgroundColor: '#EFEFF4',marginTop: Height(2)}}/> */}
                <View style={{ backgroundColor: "#FFFFFF", height: 50, paddingLeft: 20, flexDirection: 'row', marginTop: 5 }}>
                    <Text style={[{ color: "#3a4759", fontFamily: "Roboto-Bold", fontSize: 16, letterSpacing: 0.005, alignItems: 'flex-start', alignSelf: 'center', flex: 1 }, checkFontWeight("700")]}>Select All</Text>
                    {
                        Platform.OS == 'ios' ?
                            <Switch ios_backgroundColor={'#E4E4E5'} trackColor={{ true: "#4BD964" }} value={this.state.selectAllContact} onValueChange={(value) => this.onSelectAll({ value })} style={{ alignItems: 'flex-end', alignSelf: 'center', marginRight: 20 }} />
                            :
                            <Switch thumbTintColor={'#ffffff'} trackColor={{ true: "#4BD964", false: '#ccc' }} value={this.state.selectAllContact} onValueChange={(value) => this.onSelectAll({ value })} style={{ alignItems: 'flex-end', alignSelf: 'center', marginRight: 20 }} />
                    }
                </View>
                <ScrollView keyboardDismissMode='on-drag'>

                    {
                        this.issearchingcontact == true ?

                            <FlatList
                                style={{}}
                                data={this.state.searchcontactdata}
                                extraData={this.state}
                                renderItem={
                                    //this._renderItem
                                    ({ item, index }) => this._renderItem(item, index)
                                }
                                onEndReachedThreshold={50}
                                maxToRenderPerBatch={10}
                                windowSize={10}
                                onScrollBeginDrag={this._onMomentumScrollBegin}
                                keyExtractor={(item, index) => index.toString()}

                            // stickyHeaderIndices={this.state.stickyHeaderIndices}
                            />
                            :

                            <FlatList
                                style={{}}
                                data={this.state.Data}
                                extraData={this.state}
                                renderItem={
                                    //this._renderItem
                                    ({ item, index }) => this._renderItem(item, index)
                                }
                                onEndReachedThreshold={50}
                                maxToRenderPerBatch={10}
                                windowSize={10}
                                onScrollBeginDrag={this._onMomentumScrollBegin}
                                keyExtractor={(item, index) => index.toString()}

                            // stickyHeaderIndices={this.state.stickyHeaderIndices}
                            />

                    }


                    {/* {this.state.Data.map((item,index)=>{
                        return item.header==true?null:<TouchableOpacity key={index} onPress={() => { this.onSelectList(item,index) }}>
                        <View style={{ flexDirection: 'row', height: 70, flex: 1, borderBottomColor: "#3A4759", borderBottomWidth: 0.5, }}>
                            <View style={{ paddingLeft: 15, alignItems: 'flex-start', justifyContent: 'center' }}>
                                <View style={{ width: 44, height: 44, borderRadius: 22, backgroundColor: "#D6D6D6", justifyContent: 'center' }}>
                                    <Text style={[{ textAlign: 'center', fontFamily: 15, fontFamily: "Raleway-ExtraBold", letterSpacing: 0.02 }, checkFontWeight("800")]}>{item.initial}</Text>
                                </View>
                            </View>
                            <View style={{ paddingLeft: 15, alignItems: 'flex-start', justifyContent: 'center', flex: 1 }}>
                                <Text style={[{ color: "#363169", fontFamily: 16, fontFamily: "Roboto-Medium", letterSpacing: 0.02 }, checkFontWeight("500")]}>{item.title}</Text>
                            </View>
                            <View style={{ alignItems: 'flex-end', justifyContent: 'center', marginRight: 17 }}>
                                <Image source={item.select == false ? unSelectRadio : selectRadio} style={{ width: 25, height: 25 }}></Image>
                            </View>
                        </View>
                    </TouchableOpacity>
                    })} */}

                </ScrollView>
                <TouchableOpacity style={{ width: '100%' }} onPress={() => this.onInviteClick()}>
                    <ImageBackground source={btnBg} style={{ width: '100%', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={[{ marginBottom: 10, fontSize: 19, fontFamily: "Raleway-Bold", color: "#ffffff", textAlign: 'center' }, checkFontWeight("700")]}>Invite to Planmesh</Text>
                    </ImageBackground>
                </TouchableOpacity>
                <SafeAreaView />
            </Container>
        );
    }
}

export default sendFriendRequest;
