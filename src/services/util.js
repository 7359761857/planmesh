import { geo_code_key } from "../config/constant";
import { store } from "../redux/store";
import Geolocation from '@react-native-community/geolocation';

export const getCurrentCityName = async () =>{
    Geolocation.getCurrentPosition(
       async(position)  => {
            let self = this
          //alert(global.token)
          let lat = position.coords.latitude;
          let lng = position.coords.longitude;
          let url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lng+'&key='+geo_code_key
          //alert(url)
         return await  fetch(url).then(res=>res.json()).then(response=>{
            //alert(JSON.stringify(response))
            let adrss = response.results[0].address_components
            adrss.filter((adrs,index)=>{
                //alert(adrs.long_name)
                
                if(adrs.types.includes('locality')){
                    self.city = adrs.long_name
          
                }
    
                return adrs
              })
             // alert(self.city)
            //store.dispatch({type:UPDATE_SELECTED_CITY1,data:self.city})
            return self.city
          }).catch(err=>{
      
          })     
})
}


export const  capitalize_Words=(str)=>
{
 return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}