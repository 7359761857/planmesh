let monthNames = {"1":"Jan", "2":"Feb", "3":"Mar","4":"Apr", "5":"May", "6":"Jun", "7":"Jul","8":"Aug", "9":"Sep","01":"Jan", "02":"Feb", "03":"Mar","04":"Apr", "05":"May", "06":"Jun", "07":"Jul","08":"Aug", "09":"Sep", "10":"Oct","11":"Nov", "12":"Dec"};

export const dateformat=(date)=>{
    let d = date.split('-')
    let year = d[0]
    let month = d[1]
    let day = d[2]
    // return day +' '+ monthNames[month]+', '+year
    return  day+" "+ monthNames[month]  +" " + year;
}
export const dateformat1 = (date) => {
    let d = date.split('-')
    let year = d[0]
    let month = d[1]
    let day = d[2]
    // return day +' '+ monthNames[month]+', '+year
    return day + '-' + month + '-' +year
}