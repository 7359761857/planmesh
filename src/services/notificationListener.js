import RNFirebase from 'react-native-firebase';
import { Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import NavigationService from "../services/NavigationService";

export const subscribeToNotification = () => {
    // const self = this;

    // RNFirebase.messaging()
    //   .requestPermission()
    //   .then(async () => {
    //     const fcmToken = await RNFirebase.messaging().getToken();
    //     if (fcmToken) {
    //       console.log('fcmToken', fcmToken);
    //       //alert(fcmToken)
    //       if (Platform.OS === 'android') {
    //         global.android_token= fcmToken
    //         global.ios_token= ''
    //         const channel = new RNFirebase.notifications.Android.Channel(
    //           'channelId',
    //           'Channel Name',
    //           RNFirebase.notifications.Android.Importance.Max
    //         ).setDescription('A natural description of the channel');
    //         RNFirebase.notifications().android.createChannel(channel);

    //         self.notificationListener = RNFirebase.notifications().onNotification(notification => {
    //           const notification1 = new RNFirebase.notifications.Notification({
    //             sound: 'default',
    //             show_in_foreground: true
    //           })
    //             .setNotificationId(notification.notificationId)
    //             .setTitle(notification.title)
    //             .setBody(notification.body)
    //             .setData(notification.data)
    //             .android.setChannelId('channelId') // e.g. the id you chose above
    //             .android.setPriority(RNFirebase.notifications.Android.Priority.High);
    //           // alert(notification.notificationId)

    //           RNFirebase.notifications().displayNotification(notification1);
    //         });
    //       } else if (Platform.OS === 'ios') {
    //         global.ios_token= fcmToken
    //         global.android_token= ''
    //         self.notificationListener = RNFirebase.notifications().onNotification(notification => {
    //           const localNotification = new RNFirebase.notifications.Notification(
    //             {
    //               sound: 'default',
    //               show_in_foreground: true
    //             }

    //           )
    //             .setNotificationId(notification.notificationId)
    //             .setTitle(notification.title)
    //             .setBody(notification.body)
    //             .setData(notification.data);
    //           RNFirebase.notifications()
    //             .displayNotification(localNotification)
    //             .catch(err => alert(JSON.stringify(err)));
    //         });
    //       }
    //       self.notificationDisplayedListener = RNFirebase.notifications().onNotificationDisplayed(notification => {
    //         // Process your notification as required
    //         // alert(JSON.stringify(notification.data))
    //         // ANDROID: Remote notifications do not contain
    //         // the channel ID. You will have to specify this manually if you'd like to re-display the notification.
    //       });
    //       self.notificationOpenedListener = RNFirebase.notifications().onNotificationOpened(notificationOpen => {
    //         // Get the action triggered by the notification being opened
    //         //alert(notificationOpen.notification)
    //         //console.log("notificationOpen.notification",notificationOpen.notification.data)
    //         RNFirebase.notifications().removeDeliveredNotification(notificationOpen.notification.notificationId);
    //         // alert(JSON.stringify(notificationOpen.notification.data));
    //         const action = notificationOpen.action;
    //         // Get information about the notification that was opened
    //         const notification = notificationOpen.notification;

    //         const data = notification.data
    //         const plan_data = JSON.parse(data.plan_data)
    //         //console.log("plan_data_data",JSON.stringify(data))
    //         //console.log("plan_data",JSON.stringify(plan_data))
    //         //alert(JSON.stringify(plan_data))

    //         if(data.notification_type == 'is_invite')
    //         {

    //           //alert(plan_data.plan_type)

    //             if(plan_data.plan_type == '1')
    //             {
    //                 //alert('1')
    //                 var name = plan_data.name.split(" ")
    //                 NavigationService.navigate('EventDetail',{item:plan_data, plan_id:plan_data.plan_id, other_id:plan_data.login_id, plan_type:plan_data.plan_type,title_header:name[0]+", let's meet up!",Final_date:'',eventby:plan_data.name, notificaitonid:data.notification_id})
    //             }
    //             else if(plan_data.plan_type == '2')
    //             {
    //               //alert('2')
    //                 NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id:plan_data.login_id, plan_type: plan_data.plan_type, title_header:plan_data.activity , Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
    //             }
    //             else if(plan_data.plan_type == '3')
    //             {
    //               //alert('3')
    //                 NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id:plan_data.login_id, plan_type: plan_data.plan_type , title_header:"Wanna go to " +plan_data.line_1+"?", Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
    //             }
    //             else if(plan_data.plan_type == '4')
    //             {
    //               //alert('4')
    //                 NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type , title_header:"I'm free, wanna hangout?", Final_date : '', eventby:plan_data.name, notificaitonid:data.notification_id})
    //             }

    //             //alert('hii')
    //         }
    //         else if(data.notification_type == 'is_intrested_my_plan')
    //         {
    //               NavigationService.navigate('Friend_Status',{tabIndex:1, plan_id:plan_data.plan_id,notificaitonid:data.notification_id,Title_New:plan_data.activity,plan_type:plan_data.plan_type})
    //         }
    //         else if(data.notification_type == 'is_goin_my_plan')
    //         {
    //               NavigationService.navigate('Friend_Status',{tabIndex:0, plan_id:plan_data.plan_id, notificaitonid:data.notification_id,Title_New:plan_data.activity,plan_type:plan_data.plan_type})
    //         }
    //         else if(data.notification_type == 'is_intrested_other_going')
    //         {
    //               NavigationService.navigate('Friend_Status',{tabIndex:0, plan_id:plan_data.plan_id, notificaitonid:data.notification_id,Title_New:plan_data.activity,plan_type:plan_data.plan_type})
    //         }
    //         else if (data.notification_type == 'is_suggestion_my_plan' || data.notification_type == 'is_intrested_plan_suggestion' || data.notification_type == 'is_set_final_suggestion' || data.notification_type == 'is_upvote_my_plan' || data.notification_type == 'is_intrested_other_upvote')
    //         {

    //           if(data.s_type == 'activity')
    //           {
    //             NavigationService.navigate('SuggestActivity', { other_id: plan_data.login_id, plan_id: plan_data.plan_id , is_direct:false, notificaitonid:data.notification_id})
    //           }
    //           else if(data.s_type == 'datetime')
    //           {
    //             NavigationService.navigate('SuggestDateTime', { other_id: plan_data.login_id, plan_id: plan_data.plan_id, is_direct:false, notificaitonid:data.notification_id })
    //           }
    //           else if(data.s_type == 'location')
    //           {
    //             NavigationService.navigate('SuggestLocation', { other_id: plan_data.login_id, plan_id: plan_data.plan_id,is_direct:false, notificaitonid:data.notification_id })
    //           }
              
    //         }
    //         else if(data.notification_type == 'is_intrested_plan_chat')
    //         {
    //           if(plan_data.plan_type == 1)
    //           {
    //               var name = plan_data.receiver_name
    //               NavigationService.navigate('EventDetail',{item:plan_data, plan_id:plan_data.plan_id, other_id:plan_data.login_id, plan_type:plan_data.plan_type,title_header:name[0]+", let's meet up!",Final_date:'',eventby:plan_data.name, notificaitonid:data.notification_id})
    //           }
    //           else if(plan_data.plan_type == 2)
    //           {
    //               NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type, title_header:plan_data.activity , Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
    //           }
    //           else if(plan_data.plan_type == 3)
    //           {
    //               NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type , title_header:"Wanna go to " +plan_data.line_1+"?", Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
    //           }
    //           else if(plan_data.plan_type == 4)
    //           {
    //               NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type , title_header:"I'm free, wanna hangout?", Final_date : '', eventby:plan_data.name, notificaitonid:data.notification_id})
    //           }
    //         }
    //         else if(data.notification_type == 'is_chat_my_plan' || data.notification_type == 'is_invite_my_plan' || data.notification_type == 'is_invite_their_plan')
    //         {

    //           if(plan_data.plan_type == 1)
    //           {
    //               var name = plan_data.receiver_name
    //               NavigationService.navigate('EventDetail',{item:plan_data, plan_id:plan_data.plan_id, other_id:plan_data.login_id, plan_type:plan_data.plan_type,title_header:name[0]+", let's meet up!",Final_date:'',eventby:plan_data.name, notificaitonid:data.notification_id})
    //           }
    //           else if(plan_data.plan_type == 2)
    //           {
    //               NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type, title_header:plan_data.activity , Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
    //           }
    //           else if(plan_data.plan_type == 3)
    //           {
    //               NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type , title_header:"Wanna go to " +plan_data.line_1+"?", Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
    //           }
    //           else if(plan_data.plan_type == 4)
    //           {
    //               NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type , title_header:"I'm free, wanna hangout?", Final_date : '', eventby:plan_data.name, notificaitonid:data.notification_id})
    //           }
    //         }
    //         else if(data.notification_type == 'Friend Request')
    //         {
    //             // this.setState({index:1, notificaitonid:item.notification_id})
    //             // this.readNotification()
    //             // this.getFriendRequests()

    //             NavigationService.navigate('notification', {index:1})
    //         }
    //         else if(data.notification_type == 'Friend Request Accepted')
    //         {

    //           //alert(JSON.stringify(data))

    //           AsyncStorage.setItem('other_id', plan_data.sender_id)
    //           NavigationService.navigate('ohter_user_profile')
    //         }
            
    //       });
    //        //self.notificationDisplayedListener()
    //        //self.notificationListener()
    //        //self.notificationOpenedListener()
    //       RNFirebase.notifications()
    //         .getInitialNotification()
    //         .then(notificationOpen => {
    //           if (notificationOpen) {
    //             //alert(JSON.stringify(notificationOpen.notification.data));
    //             // App was opened by a notification
    //             // Get the action triggered by the notification being opened
    //             ///alert(notificationOpen.notification)
    //         //console.log("notificationOpen.notification",notificationOpen.notification.data)
    //             const action = notificationOpen.action;
    //             // Get information about the notification that was opened
    //             const notification = notificationOpen.notification;

    //             //alert(JSON.stringify(notification.data))
    //         const data = notification.data
    //         const plan_data = JSON.parse(data.plan_data)
            
    //         //alert(JSON.stringify(plan_data))

    //         if(data.notification_type == 'is_invite')
    //         {

    //           //alert(plan_data.plan_type)

    //             if(plan_data.plan_type == '1')
    //             {
    //                 //alert('1')
    //                 var name = plan_data.name.split(" ")
    //                 NavigationService.navigate('EventDetail',{item:plan_data, plan_id:plan_data.plan_id, other_id:plan_data.login_id, plan_type:plan_data.plan_type,title_header:name[0]+", let's meet up!",Final_date:'',eventby:plan_data.name, notificaitonid:data.notification_id})
    //             }
    //             else if(plan_data.plan_type == '2')
    //             {
    //               //alert('2')
    //                 NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id:plan_data.login_id, plan_type: plan_data.plan_type, title_header:plan_data.activity , Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
    //             }
    //             else if(plan_data.plan_type == '3')
    //             {
    //               //alert('3')
    //                 NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id:plan_data.login_id, plan_type: plan_data.plan_type , title_header:"Wanna go to " +plan_data.line_1+"?", Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
    //             }
    //             else if(plan_data.plan_type == '4')
    //             {
    //               //alert('4')
    //                 NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type , title_header:"I'm free, wanna hangout?", Final_date : '', eventby:plan_data.name, notificaitonid:data.notification_id})
    //             }

    //             //alert('hii')
    //         }
    //         else if(data.notification_type == 'is_intrested_my_plan')
    //         {
    //               NavigationService.navigate('Friend_Status',{tabIndex:1, plan_id:plan_data.plan_id,notificaitonid:data.notification_id,Title_New:plan_data.activity,plan_type:plan_data.plan_type})
    //         }
    //         else if(data.notification_type == 'is_goin_my_plan')
    //         {
    //               NavigationService.navigate('Friend_Status',{tabIndex:0, plan_id:plan_data.plan_id, notificaitonid:data.notification_id,Title_New:plan_data.activity,plan_type:plan_data.plan_type})
    //         }
    //         else if(data.notification_type == 'is_intrested_other_going')
    //         {
    //               NavigationService.navigate('Friend_Status',{tabIndex:0, plan_id:plan_data.plan_id, notificaitonid:data.notification_id,Title_New:plan_data.activity,plan_type:plan_data.plan_type})
    //         }
    //         else if (data.notification_type == 'is_suggestion_my_plan' || data.notification_type == 'is_intrested_plan_suggestion' || data.notification_type == 'is_set_final_suggestion' || data.notification_type == 'is_upvote_my_plan' || data.notification_type == 'is_intrested_other_upvote')
    //         {

    //           if(data.s_type == 'activity')
    //           {
    //             NavigationService.navigate('SuggestActivity', { other_id: plan_data.login_id, plan_id: plan_data.plan_id , is_direct:false, notificaitonid:data.notification_id})
    //           }
    //           else if(data.s_type == 'datetime')
    //           {
    //             NavigationService.navigate('SuggestDateTime', { other_id: plan_data.login_id, plan_id: plan_data.plan_id, is_direct:false, notificaitonid:data.notification_id })
    //           }
    //           else if(data.s_type == 'location')
    //           {
    //             NavigationService.navigate('SuggestLocation', { other_id: plan_data.login_id, plan_id: plan_data.plan_id,is_direct:false, notificaitonid:data.notification_id })
    //           }
              
    //         }
    //         else if(data.notification_type == 'is_intrested_plan_chat')
    //         {
    //           if(plan_data.plan_type == 1)
    //           {
    //               var name = plan_data.receiver_name
    //               NavigationService.navigate('EventDetail',{item:plan_data, plan_id:plan_data.plan_id, other_id:plan_data.login_id, plan_type:plan_data.plan_type,title_header:name[0]+", let's meet up!",Final_date:'',eventby:plan_data.name, notificaitonid:data.notification_id})
    //           }
    //           else if(plan_data.plan_type == 2)
    //           {
    //               NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type, title_header:plan_data.activity , Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
    //           }
    //           else if(plan_data.plan_type == 3)
    //           {
    //               NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type , title_header:"Wanna go to " +plan_data.line_1+"?", Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
    //           }
    //           else if(plan_data.plan_type == 4)
    //           {
    //               NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type , title_header:"I'm free, wanna hangout?", Final_date : '', eventby:plan_data.name, notificaitonid:data.notification_id})
    //           }
    //         }
    //         else if(data.notification_type == 'is_chat_my_plan' || data.notification_type == 'is_invite_my_plan' || data.notification_type == 'is_invite_their_plan')
    //         {

    //           if(plan_data.plan_type == 1)
    //           {
    //               var name = plan_data.receiver_name
    //               NavigationService.navigate('EventDetail',{item:plan_data, plan_id:plan_data.plan_id, other_id:plan_data.login_id, plan_type:plan_data.plan_type,title_header:name[0]+", let's meet up!",Final_date:'',eventby:plan_data.name, notificaitonid:data.notification_id})
    //           }
    //           else if(plan_data.plan_type == 2)
    //           {
    //               NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type, title_header:plan_data.activity , Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
    //           }
    //           else if(plan_data.plan_type == 3)
    //           {
    //               NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type , title_header:"Wanna go to " +plan_data.line_1+"?", Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
    //           }
    //           else if(plan_data.plan_type == 4)
    //           {
    //               NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type , title_header:"I'm free, wanna hangout?", Final_date : '', eventby:plan_data.name, notificaitonid:data.notification_id})
    //           }
    //         }
    //         else if(data.notification_type == 'Friend Request')
    //         {
    //             // this.setState({index:1, notificaitonid:item.notification_id})
    //             // this.readNotification()
    //             // this.getFriendRequests()

    //             NavigationService.navigate('notification',{index:1})
    //         }
    //         else if(data.notification_type == 'Friend Request Accepted')
    //         {
    //           //alert(JSON.stringify(data))
    //           AsyncStorage.setItem('other_id', plan_data.sender_id)
    //           NavigationService.navigate('ohter_user_profile')
    //         }

    //       }
    //         });
    //     } else {
    //       // user doesn't have a device token yet
    //     }
    //   })
    //   .catch(error => {
    //     // User has rejected permissions
    //   });
  };