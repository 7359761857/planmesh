import React, { Component } from "react";
import { StyleProvider } from "native-base";
import { Platform, StyleSheet, Text, View, StatusBar } from 'react-native';
import App from "../App";
import getTheme from "../theme/components";
import variables from "../theme/variables/commonColor";

export default class Setup extends Component {
  componentDidMount() {
    StatusBar.setHidden(false);
    StatusBar.setBarStyle("light-content", false);
  }
  render() {
    return (
        <StyleProvider style={getTheme(variables)}>
          <App />
        </StyleProvider>
    );
  }
}
