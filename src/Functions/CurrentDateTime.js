// added by utsav
const CurrentDateTime = () =>
{
   let now = new Date();
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
    var day = days[ now.getDay() == 0  ? 6 : now.getDay()  - 1];
    var todaydate = now.getDate();
    var hours = now.getHours();
  var minutes = now.getMinutes(); 
  var months = monthNames[now.getMonth()];
  var years =  now.getFullYear();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12; 
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
    return ( todaydate +' '+months+' '+ years +', '+ day  +', '+strTime);
}  
export default CurrentDateTime; 