// added by utsav
import firebase from 'firebase';
import CurrentDateTime from './CurrentDateTime';
import moment from 'moment';

const UploadImage = async (plan_id , sentBy , Name , Avtar , uri , filename) =>
{
  const blob = await new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onload = function() {
      resolve(xhr.response);
    };
    xhr.onerror = function(e) {
      reject(new TypeError('Network request failed'));
    };
    xhr.responseType = 'blob';
    xhr.open('GET', uri, true);
    xhr.send(null);
  });
  const ref = firebase
    .storage()
    .ref()
    .child('Images/'+filename); 
  const snapshot = await ref.put(blob);
  //var date = CurrentDateTime();
  var objcurdate = new Date()
  var date = moment.utc(objcurdate).format("YYYY-MM-DD HH:mm:ss")
  //alert(date);
  var ImagePath = await snapshot.ref.getDownloadURL()
  // alert(ImagePath); 
  firebase.database().ref('Groups').child(plan_id+'/Massages').push({
     sentBy , Name , Avtar, date , Image: ImagePath
})  
  return  true; 
     
} // 36897 58057  36897 69066
export default UploadImage;