import React from "react";
import { Root, View } from "native-base";
import { createStackNavigator} from "react-navigation-stack";
import { createBottomTabNavigator} from "react-navigation-tabs";
import {  Platform, AppState, Dimensions } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import RNFirebase from 'react-native-firebase';
import FlashMessage from "react-native-flash-message";
import {showMessage, hideMessage} from "react-native-flash-message";

import constant from "./screens/Constants";
import NavigationService from './services/NavigationService'
import splash from "./screens/home/splash";
import home from "./screens/home/landing";
import login from "./screens/login";
import SocialLogin from "./screens/login/SocialLogin";
import forgotPassword from "./screens/forgotPassword";
import createPassword from "./screens/createPassword";
import ResetPassord from "./screens/ResetPassord";

import signup from "./screens/Signup";
import Birthday from "./screens/Signup/Birthday";  
import SignupStep1 from "./screens/SignupStep1";
import SignupOptions from "./screens/SignupOptions";
import Categories from "./screens/Categories";
import Interest from "./screens/Categories/Interest";
import Places from "./screens/Categories/Places";
import SuggestActivity from "./screens/Main/SuggestActivity";
import Activity from "./screens/Main/Activity";

import SuggestLocation from "./screens/Main/SuggestLocation";
import Location from "./screens/Main/Location";

import SuggestDateTime from "./screens/Main/SuggestDateTime";
import DateTime from "./screens/Main/DateTime";

import findFriends from "./screens/findFriends";
import sendFriendRequest from "./screens/sendFriendRequest";

import dashboard from "./screens/dashboard";
import EditPrivacy from "./screens/dashboard/EditPrivacy";
import notification from './screens/notification';
import Friend_Status from './screens/Friend_Status';
import FindallFriends from './screens/dashboard/FindallFriends';
import EventDetail from './screens/EventDetail';
import InviteFriends from './screens/EventDetail/InviteFriends';

import myprofile from './screens/profile/myprofile';
import editprofile from './screens/profile/editprofile';
import ohter_user_profile from './screens/profile/ohter_user_profile';
import ohter_user_profile1 from './screens/profile/ohter_user_profile1';
import ProfileCategories from './screens/profile/ProfileCategories';
import ProfileInterest from './screens/profile/ProfileInterest';
import ProfilePlaes from './screens/profile/ProfilePlaes';
import ReportPerson from './screens/profile/ReportPerson';
import OtherProfileInterest from './screens/profile/OtherProfileInterest';
import OtherProfilePlaces from './screens/profile/OtherProfilePlaces';

import FriendsfriendList from './screens/profile/FriendsfriendList';
import ProfilePicView from './screens/profile/ProfilePicView';

import where1 from './screens/where/where1';
import where2 from './screens/where/where2';
import where3 from './screens/where/where3';
import where4 from './screens/where/where4';
import where5 from './screens/where/where5';

import when1 from './screens/when/when1';
import when2 from './screens/when/when2';
import when_friends from './screens/when/when_friends';
import when_acitvity from './screens/when/when_acitvity';
import when_location from './screens/when/when_location';



import what1 from './screens/what/what1';
import what2 from './screens/what/what2';
import what5 from './screens/what/what5';

import Frinds from './screens/what/Frinds';
import Who1 from './screens/Who/Who1';
import Who2 from './screens/Who/Who2';
import what3 from './screens/what/what3';

import Acitivity1 from './screens/Who/Acitivity1';
import Location1 from './screens/Who/Location1';
import Time1 from './screens/Who/Time1';

import settings from './screens/settings/setting';
import account from './screens/settings/account';
import change_password from './screens/settings/change_password';
import report from './screens/settings/report';
import Block from './screens/settings/Block';
import Privacy from './screens/settings/Privacy';
import PrivacyPolicy from './screens/settings/PrivacyPolicy';
import TermsOfUse from './screens/settings/TermsOfUse';

import FindFriends_Setting from './screens/settings/FindFriends_Setting';

import reminders from './screens/settings/reminders';
import notifications from './screens/settings/notification';
import friendslist from './screens/settings/friendslist';
import guestlist from './screens/settings/guestlist';
import EventDescription from './screens/EventDetail/EventDescription';


import BackgroundImage from "./screens/Default/BackgroundImage";
import HandleBack from "./screens/Default/HandleBack";

import Toastt from "./screens/Default/Toastt";
import APICall from './WebService/APICall';
import { store } from "./redux/store";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
 import firebase from 'firebase';
import { TouchableHighlight } from "react-native-gesture-handler";
import { NOTIFICATION, FRIENDREQUEST, NOTIFICATION_COUNT } from "./redux/actions/types";
//import moment from "react-native-form-validator/node_modules/moment";
import moment from 'moment';
import { createAppContainer,createSwitchNavigator } from "react-navigation";
import AuthLoadingScreen from './screens/AuthLoadingScreen';

const { height, width } = Dimensions.get('window')

global.BackgroundImage = BackgroundImage
global.HandleBack = HandleBack
global.Toastt = Toastt
global.APICall = APICall
global.noti_count = 0
global.noti_tab_count = 0
global.friend_request_count = 0
global.isfromlogin = false
global.lastnoti_id = ''
global.apple_user_id = ''
global.appIsinBackground = false
global.freshlaunch = false
global.userDetails = {}
global.data = null
global.isEventDetails = false
global.isConnected = true

const TabNavigator = createBottomTabNavigator({
  dashboard: {
    screen: dashboard,
    navigationOptions: () => ({
      // tabBarIcon: ({ tintColor }) => (
      //   <MaterialIcons name='home' size={25} color={tintColor} />
      // ),
      tabBarVisible: true
    })
  }, FindallFriends: {
    screen: FindallFriends,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => (
        <MaterialIcons name='search' size={25} color={tintColor} />

      )
    })
  },
  notification: {
    screen: notification,
    navigationOptions: () => ({
      // tabBarIcon: ({ tintColor }) => (
      //   <MaterialIcons name='notifications-none' size={25} color={tintColor} />
      // )
    })
  },
  myprofile: {
    screen: myprofile,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => (
        <MaterialIcons name='person-outline' size={25} color={tintColor} />
      )
    })
  },
},
  {
    header: null,
    //initialRouteName: "HistoryScreen",
    initialRouteName: "dashboard",
    tabBarOptions: {
      inactiveTintColor: "#000",
      activeTintColor: '#FF00BA',
      showLabel: false,
      style: {
        borderTopWidth: 0,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 12,
        },
        shadowOpacity: 0.30,
        shadowRadius: 16.00,

        elevation: 24,

      },

    },


  }
);
const AppNavigator1 = createStackNavigator(
  {
    splash: {
      screen: splash,
      navigationOptions: () => ({
        header: null,
      })
    },
    Home: {
      screen: home,
      navigationOptions: () => ({
        header: null,
        gesturesEnabled: false,
      })
    },
    Login: {
      screen: login,
      navigationOptions: () => ({
        gesturesEnabled: false,
      })
    },
    SocialLogin: {
      screen: SocialLogin,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    forgotPassword: {
      screen: forgotPassword,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    createPassword: {
      screen: createPassword,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    ResetPassord: {
      screen: ResetPassord,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    SignupOptions: {
      screen: SignupOptions,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    signup: {
      screen: signup,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    Birthday: {
      screen: Birthday,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    SignupStep1: {
      screen: SignupStep1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    Categories: {
      screen: Categories,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    
    Interest: {
      screen: Interest,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    Places: {
      screen: Places,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    SuggestActivity: {
      screen: SuggestActivity,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,
      })
    },
    Activity: {
      screen: Activity,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    SuggestLocation: {
      screen: SuggestLocation,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled:true

      })
    },
    Location: {
      screen: Location,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    SuggestDateTime: {
      screen: SuggestDateTime,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,
      })
    },
    DateTime: {
      screen: DateTime,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    EventDetail: {
      screen: EventDetail,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,
      })
    },
    InviteFriends: {
      screen: InviteFriends,
      navigationOptions: () => ({
        //header:null
        gesturesEnabled: true,
      })
    },
    findFriends: {
      screen: findFriends,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    sendFriendRequest: {
      screen: sendFriendRequest,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    TabNavigator: {
      screen: TabNavigator,
      navigationOptions: () => ({
        header: null,
        gesturesEnabled: false,
      })
    },
    EditPrivacy: {
      screen: EditPrivacy,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
  
    notification :{
      screen: notification,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
   
    editprofile:{
      screen: editprofile,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    ohter_user_profile:{
      screen: ohter_user_profile,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    ohter_user_profile1:{
      screen: ohter_user_profile1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    ProfileCategories:{
      screen: ProfileCategories,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    ProfileInterest:{
      screen: ProfileInterest,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,

      })
    }, 
    OtherProfileInterest:{
      screen: OtherProfileInterest,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,
      })
    },
    OtherProfilePlaces:{
      screen: OtherProfilePlaces,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,
      })
    },
    ProfilePlaes:{
      screen: ProfilePlaes,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    ReportPerson:{
      screen: ReportPerson,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    FriendsfriendList:{
      screen: FriendsfriendList,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    Who1:{
      screen: Who1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    Who2:{
      screen: Who2,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },   
    Acitivity1:{
      screen: Acitivity1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },   
    Location1:{
      screen: Location1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },   
    Time1:{
      screen: Time1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },   
    where1:{
      screen: where1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    where2:{
      screen: where2,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    where3:{
      screen: where3,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    where4:{
      screen: where4,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    where5:{
      screen: where5,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    when1:{
      screen: when1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    when2:{
      screen: when2,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    when_friends:{
      screen: when_friends,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    when_acitvity:{
      screen: when_acitvity,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
   
    when_location:{
      screen: when_location,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
   
    what1:{
      screen: what1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    what2:{
      screen: what2,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    what3:{
      screen: what3,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    what5:{
      screen: what5,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    Frinds:{
      screen: Frinds,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    settings:{
      screen: settings,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    account:{
      screen: account,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    change_password:{
      screen: change_password,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    FindFriends_Setting:{
      screen: FindFriends_Setting,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    notifications:{
      screen: notifications,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    Friend_Status:{
      screen: Friend_Status,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,
      })
    },
    reminders:{
      screen: reminders,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    report:{
      screen: report,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
     Privacy:{
      screen: Privacy,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    PrivacyPolicy:{
      screen: PrivacyPolicy,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    TermsOfUse:{
      screen: TermsOfUse,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
     Block:{
      screen: Block,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    friendslist:{
      screen: friendslist,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    guestlist:{
      screen: guestlist,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    ProfilePicView:{
      screen: ProfilePicView,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,
      })
    },
    EventDescription:{
      screen: EventDescription,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,
      })
    }
  },
  {
    initialRouteName: "splash",
    headerMode: 'screen',
    navigationOptions: {
      gesturesEnabled: false,
    },
    transitionConfig: (sceneProps) => ({
      transitionSpec: {
        duration: (sceneProps.scene.route.routeName == "TabbarWithoutAnimation" || sceneProps.scene.route.routeName == "Home") ? 0 : 260,
      },
    }),

  }
);

const AuthNavigator = createStackNavigator(
  {
    splash: {
      screen: splash,
      navigationOptions: () => ({
        header: null,
      })
    },
    Home: {
      screen: home,
      navigationOptions: () => ({
        header: null,
        gesturesEnabled: false,
      })
    },
    Login: {
      screen: login,
      navigationOptions: () => ({
        gesturesEnabled: false,
      })
    },
    SocialLogin: {
      screen: SocialLogin,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    forgotPassword: {
      screen: forgotPassword,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    createPassword: {
      screen: createPassword,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    ResetPassord: {
      screen: ResetPassord,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    SignupOptions: {
      screen: SignupOptions,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    signup: {
      screen: signup,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    Birthday: {
      screen: Birthday,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    SignupStep1: {
      screen: SignupStep1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    Categories: {
      screen: Categories,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
     Interest: {
      screen: Interest,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    PrivacyPolicy:{
      screen: PrivacyPolicy,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    TermsOfUse:{
      screen: TermsOfUse,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    }
  },
  {
    headerMode: 'screen',
    navigationOptions: {
      gesturesEnabled: false,
    },
  }
);

const AppNavigator = createStackNavigator(
  {
    splash: {
      screen: splash,
      navigationOptions: () => ({
        header: null,
      })
    },
    Home: {
      screen: home,
      navigationOptions: () => ({
        header: null,
        gesturesEnabled: false,
      })
    },
    Login: {
      screen: login,
      navigationOptions: () => ({
        gesturesEnabled: false,
      })
    },
    SocialLogin: {
      screen: SocialLogin,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    forgotPassword: {
      screen: forgotPassword,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    createPassword: {
      screen: createPassword,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    ResetPassord: {
      screen: ResetPassord,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    SignupOptions: {
      screen: SignupOptions,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    signup: {
      screen: signup,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    Birthday: {
      screen: Birthday,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    SignupStep1: {
      screen: SignupStep1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    Categories: {
      screen: Categories,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
     Interest: {
      screen: Interest,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
     Places: {
      screen: Places,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    SuggestActivity: {
      screen: SuggestActivity,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,
      })
    },
    Activity: {
      screen: Activity,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    SuggestLocation: {
      screen: SuggestLocation,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled:true

      })
    },
    Location: {
      screen: Location,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    SuggestDateTime: {
      screen: SuggestDateTime,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,

      })
    },
    DateTime: {
      screen: DateTime,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    EventDetail: {
      screen: EventDetail,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,

      })
    },
    InviteFriends: {
      screen: InviteFriends,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    findFriends: {
      screen: findFriends,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    when_acitvity:{
      screen: when_acitvity,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    sendFriendRequest: {
      screen: sendFriendRequest,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    TabNavigator: {
      screen: TabNavigator,
      navigationOptions: () => ({
        header: null,
        gesturesEnabled: false,
      })
    },
    EditPrivacy: {
      screen: EditPrivacy,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    notification :{
      screen: notification,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
   
    Friend_Status:{
      screen: Friend_Status,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,
      })
    },
   
    editprofile:{
      screen: editprofile,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    ohter_user_profile:{
      screen: ohter_user_profile,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    ohter_user_profile1:{
      screen: ohter_user_profile1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    ProfileCategories:{
      screen: ProfileCategories,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    ProfileInterest:{
      screen: ProfileInterest,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,

      })
    }, 
    OtherProfileInterest:{
      screen: OtherProfileInterest,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,
      })
    },
    OtherProfilePlaces:{
      screen: OtherProfilePlaces,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,
      })
    },
    ProfilePlaes:{
      screen: ProfilePlaes,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    ReportPerson:{
      screen: ReportPerson,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    FriendsfriendList:{
      screen: FriendsfriendList,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    Who1:{
      screen: Who1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    Who2:{
      screen: Who2,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    Acitivity1:{
      screen: Acitivity1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },   
    Location1:{
      screen: Location1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },   
    Time1:{
      screen: Time1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },   
    where1:{
      screen: where1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    where2:{
      screen: where2,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    where3:{
      screen: where3,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    where4:{
      screen: where4,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    where5:{
      screen: where5,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    when1:{
      screen: when1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
   
    when2:{
      screen: when2,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    when_friends:{
      screen: when_friends,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },  
    when_location:{
      screen: when_location,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },     
    what1:{
      screen: what1,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    what2:{
      screen: what2,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    what3:{
      screen: what3,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    what5:{
      screen: what5,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    Frinds:{
      screen: Frinds,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    settings:{
      screen: settings,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    account:{
      screen: account,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    change_password:{
      screen: change_password,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    FindFriends_Setting:{
      screen: FindFriends_Setting,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    notifications:{
      screen: notifications,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    reminders:{
      screen: reminders,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
     report:{
      screen: report,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
     Privacy:{
      screen: Privacy,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    PrivacyPolicy:{
      screen: PrivacyPolicy,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    TermsOfUse:{
      screen: TermsOfUse,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    Block:{
      screen: Block,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    friendslist:{
      screen: friendslist,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    guestlist:{
      screen: guestlist,
      navigationOptions: () => ({
        gesturesEnabled: true,
      })
    },
    ProfilePicView:{
      screen: ProfilePicView,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,
      })
    },
    EventDescription:{
      screen: EventDescription,
      navigationOptions: () => ({
        header:null,
        gesturesEnabled: true,
      })
    }
  },
  {
    initialRouteName: "TabNavigator",
    headerMode: 'screen',
    navigationOptions: {
      gesturesEnabled: false,
    },
    transitionConfig: (sceneProps) => ({
      transitionSpec: {
        duration: (sceneProps.scene.route.routeName == "TabbarWithoutAnimation" || sceneProps.scene.route.routeName == "Home") ? 0 : 260,
      },
    }),

  }
);

const AppSwitchNavigator = createSwitchNavigator({
  //AuthLoading:AuthLoadingScreen,
  Auth:AuthNavigator,
  App:AppNavigator
})


const firebaseConfig = {
  apiKey: "AIzaSyARwBSs2WkmniGOWlyVPaRxWRm-VVyb6Vg",
  authDomain: "planmesh-415e3.firebaseapp.com",
  databaseURL: "https://planmesh-415e3.firebaseio.com",
  projectId: "planmesh-415e3",
  storageBucket: "planmesh-415e3.appspot.com",
  messagingSenderId: "80422853890",
  appId: "1:80422853890:web:2ce10c52f6c8c41d12761b",
  measurementId: "G-V6R8C9XSZS"
};


const AppContainer = createAppContainer(AppNavigator)

const AppContainer1 =createAppContainer(AppNavigator1)

export default class Root1  extends React.Component{
  constructor(props){
    super(props)
    this.state={
      loading:true,
      login:false,
      appState: AppState.currentState,
      notification_received: false
    }
  }
  componentDidMount(){

    //console.log('Root Called')
    global.freshlaunch = true
   // ****** // 
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig); // add by utsav
  }
   // ****** //
    AsyncStorage.setItem('overlay','false')


    const self = this;

    RNFirebase.messaging()
      .requestPermission()
      .then(async () => {
        const fcmToken = await RNFirebase.messaging().getToken();
        if (fcmToken) {
          //console.log('fcmToken', fcmToken);
          //alert(fcmToken)
          if (Platform.OS === 'android') {
            global.android_token = fcmToken
            global.ios_token = ''
            const channel = new RNFirebase.notifications.Android.Channel(
              'channelId',
              'Channel Name',
              RNFirebase.notifications.Android.Importance.Max
            ).setDescription('A natural description of the channel');
            RNFirebase.notifications().android.createChannel(channel);

            self.notificationListener = RNFirebase.notifications().onNotification(notification => {
              const notification1 = new RNFirebase.notifications.Notification({
                sound: 'default',
                show_in_foreground: true
              })
                .setNotificationId(notification.notificationId)
                .setTitle(notification.title)
                .setBody(notification.body)
                .setData(notification.data)
                .android.setChannelId('channelId')
                .android.setPriority(RNFirebase.notifications.Android.Priority.High);

                if(notification.notificationId != global.lastnoti_id)
                {
                  global.lastnoti_id = notification.notificationId

                  if((notification1.data.notification_type != 'is_chat_my_plan') && (notification1.data.notification_type != 'is_intrested_plan_chat'))
                  {
                    global.noti_count = global.noti_count + 1
                    store.dispatch({type:NOTIFICATION,data:global.noti_count})

                    if((notification1.data.notification_type != 'Friend Request') && (notification1.data.notification_type != 'Reminder'))
                    {
                      global.noti_tab_count = global.noti_tab_count + 1
                      store.dispatch({type:NOTIFICATION_COUNT,data:global.noti_tab_count})
                    }

                  }
                  
                  if(notification1.data.notification_type == 'Friend Request')
                  {
                      global.friend_request_count = global.friend_request_count + 1
                      store.dispatch({type:FRIENDREQUEST,data:global.friend_request_count})
                  }
                  
                }
                //alert(JSON.stringify(notification1))
                RNFirebase.notifications().displayNotification(notification1);

            });
          } else if (Platform.OS === 'ios') {

            global.ios_token = fcmToken
            global.android_token = ''

            //alert(global.ios_token)

            self.notificationListener = RNFirebase.notifications().onNotification(notification => {
              const localNotification = new RNFirebase.notifications.Notification(
                {
                  sound: 'default',
                  show_in_foreground: false
                }

              )
                .setNotificationId(notification.notificationId)
                .setTitle(notification.title)
                .setBody(notification.body)
                .setData(notification.data);
            

              //alert('Notification: '+localNotification.data)

            if((localNotification.data.notification_type != 'is_chat_my_plan') && (localNotification.data.notification_type != 'is_intrested_plan_chat'))
            {
              global.noti_count = global.noti_count + 1
              store.dispatch({type:NOTIFICATION,data:global.noti_count})

              if((localNotification.data.notification_type != 'Friend Request') && (localNotification.data.notification_type != 'Reminder'))
              {
                global.noti_tab_count = global.noti_tab_count + 1
                store.dispatch({type:NOTIFICATION_COUNT,data:global.noti_tab_count})
              }
            }

            if(localNotification.data.notification_type == 'Friend Request')
            {
                global.friend_request_count = global.friend_request_count + 1
                store.dispatch({type:FRIENDREQUEST,data:global.friend_request_count})
            }

            //console.log('Notification Count:: '+noticount)

              //store.dispatch({type:NOTIFICATION,data:1})

              if(global.appstate != 'active')
              {
                RNFirebase.notifications()
                .displayNotification(localNotification)
                .catch(err => alert(JSON.stringify(err)));
              }
              else
              {

                const data = localNotification.data
                const plan_data = JSON.parse(data.plan_data)

                //alert('NotiType: '+data.notification_type)

                //console.log('Global PlanID: '+global.planID+' NotiplanID: '+plan_data.plan_id+' PLanType: '+plan_data.plan_type)
                //console.log('ChatData: '+JSON.stringify(plan_data))

                if((String(global.planID) != String(plan_data.plan_id)) || ((data.notification_type != 'is_chat_my_plan') && (data.notification_type != 'is_intrested_plan_chat')))
                {
                  showMessage({
                    message: localNotification.body,
                    type: "info",
                    backgroundColor : "#363169",
                    hideStatusBar: true,
                    onPress: () => {
                        hideMessage()
                        this.onNotificationClick(data)
                    },
                  });
                }
                //this.setState({notification_received:true})
              }
            });
          }
          self.notificationDisplayedListener = RNFirebase.notifications().onNotificationDisplayed(notification => {
            // Process your notification as required
            // alert(JSON.stringify(notification.data))
            // ANDROID: Remote notifications do not contain
            // the channel ID. You will have to specify this manually if you'd like to re-display the notification.
           
          });
          self.notificationOpenedListener = RNFirebase.notifications().onNotificationOpened(notificationOpen => {
            // Get the action triggered by the notification being opened
            RNFirebase.notifications().removeDeliveredNotification(notificationOpen.notification.notificationId);
             //alert(JSON.stringify(notificationOpen.notification.data));
            const action = notificationOpen.action;
            // Get information about the notification that was opened
            const notification = notificationOpen.notification;

            const data = notification.data
            
            // if(global.noti_count > 0)
            // {
            //   global.noti_count = store.getState().auth.notification_count - 1
            // }

            //this.onNotificationClick(data)

            setTimeout( () => {
              this.onNotificationClick(data)
           },500);

          });
           //self.notificationDisplayedListener()
           //self.notificationListener()
           //self.notificationOpenedListener()
          RNFirebase.notifications()
            .getInitialNotification()
            .then(notificationOpen => {
              if (notificationOpen) {
                //alert(JSON.stringify(notificationOpen.notification.data));
                // App was opened by a notification
                // Get the action triggered by the notification being opened
                const action = notificationOpen.action;
                // Get information about the notification that was opened
                const notification = notificationOpen.notification;

                //alert(JSON.stringify(notification.data))
                const data = notification.data
                //this.onNotificationClick(data)
                setTimeout( () => {
                  this.onNotificationClick(data)
               },500);
          }
            });
        } else {
          // user doesn't have a device token yet
        }
      })
      .catch(error => {
        // User has rejected permissions
      });


  }

  onNotificationClick(data)
  {


    if(global.freshlaunch == true)
    {
        global.data = data
        return
    }

    const plan_data = JSON.parse(data.plan_data)

    //alert(JSON.parse(data.plan_data))

    //return

    if(data.notification_type == 'is_invite')
    {

      //alert(plan_data.plan_type)

        if(global.isEventDetails == true && Platform.OS == 'ios')
        {
            return
        }

        if(plan_data.plan_type == '1')
        {
            //alert('1')
            var name = plan_data.name.split(" ")
            NavigationService.navigate('EventDetail',{item:plan_data, plan_id:plan_data.plan_id, other_id:plan_data.login_id, plan_type:plan_data.plan_type,title_header:"Let's meet up!",Final_date:'',eventby:plan_data.name, notificaitonid:data.notification_id})
        }
        else if(plan_data.plan_type == '2')
        {
          //alert('2')
            NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id:plan_data.login_id, plan_type: plan_data.plan_type, title_header:plan_data.activity , Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
        }
        else if(plan_data.plan_type == '3')
        {
          //alert('3')
            NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id:plan_data.login_id, plan_type: plan_data.plan_type , title_header:"Wanna go to " +plan_data.line_1+"?", Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
        }
        else if(plan_data.plan_type == '4')
        {
          //alert('4')
            NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type , title_header:"I'm free, wanna hangout?", Final_date : '', eventby:plan_data.name, notificaitonid:data.notification_id})
        }

        //alert('hii')
    }
    else if(data.notification_type == 'is_intrested_my_plan')
    {
         // NavigationService.navigate('Friend_Status',{tabIndex:1, plan_id:plan_data.plan_id,notificaitonid:data.notification_id})

         if(global.isEventDetails == true && Platform.OS == 'ios')
         {
             return
         }

         if(plan_data.plan_type == '1')
        {
            //alert('1')
            var name = plan_data.friend_name.split(" ")
            NavigationService.navigate('EventDetail',{item:plan_data, plan_id:plan_data.plan_id, other_id:plan_data.login_id, plan_type:plan_data.plan_type,title_header:"Let's meet up!",Final_date:'',eventby:plan_data.name, notificaitonid:data.notification_id})
        }
        else if(plan_data.plan_type == '2')
        {
          //alert('2')
            NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id:plan_data.login_id, plan_type: plan_data.plan_type, title_header:plan_data.activity , Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
        }
        else if(plan_data.plan_type == '3')
        {
          //alert('3')
            NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id:plan_data.login_id, plan_type: plan_data.plan_type , title_header:"Wanna go to " +plan_data.line_1+"?", Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
        }
        else if(plan_data.plan_type == '4')
        {
          //alert('4')
            NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type , title_header:"I'm free, wanna hangout?", Final_date : '', eventby:plan_data.name, notificaitonid:data.notification_id})
        }
    }
    else if(data.notification_type == 'is_goin_my_plan')
    {
          //NavigationService.navigate('Friend_Status',{tabIndex:0, plan_id:plan_data.plan_id, notificaitonid:data.notification_id})

          if(global.isEventDetails == true && Platform.OS == 'ios')
          {
              return
          }

          if(plan_data.plan_type == '1')
        {
            //alert('1')
            var name = plan_data.friend_name.split(" ")
            NavigationService.navigate('EventDetail',{item:plan_data, plan_id:plan_data.plan_id, other_id:plan_data.login_id, plan_type:plan_data.plan_type,title_header:"Let's meet up!",Final_date:'',eventby:plan_data.name, notificaitonid:data.notification_id})
        }
        else if(plan_data.plan_type == '2')
        {
          //alert('2')
            NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id:plan_data.login_id, plan_type: plan_data.plan_type, title_header:plan_data.activity , Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
        }
        else if(plan_data.plan_type == '3')
        {
          //alert('3')
            NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id:plan_data.login_id, plan_type: plan_data.plan_type , title_header:"Wanna go to " +plan_data.line_1+"?", Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
        }
        else if(plan_data.plan_type == '4')
        {
          //alert('4')
            NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type , title_header:"I'm free, wanna hangout?", Final_date : '', eventby:plan_data.name, notificaitonid:data.notification_id})
        }
    }
    else if(data.notification_type == 'is_intrested_other_going')
    {
          NavigationService.navigate('Friend_Status',{tabIndex:0, plan_id:plan_data.plan_id, notificaitonid:data.notification_id,Title_New:plan_data.activity,plan_type:plan_data.plan_type})
    }
    else if (data.notification_type == 'is_suggestion_my_plan' || data.notification_type == 'is_intrested_plan_suggestion' || data.notification_type == 'is_set_final_suggestion' || data.notification_type == 'is_upvote_my_plan' || data.notification_type == 'is_intrested_other_upvote')
    {

      if(data.s_type == 'activity')
      {
        NavigationService.navigate('SuggestActivity', { other_id: plan_data.login_id, plan_id: plan_data.plan_id , is_direct:false, notificaitonid:data.notification_id})
      }
      else if(data.s_type == 'datetime')
      {
        NavigationService.navigate('SuggestDateTime', { other_id: plan_data.login_id, plan_id: plan_data.plan_id, is_direct:false, notificaitonid:data.notification_id })
      }
      else if(data.s_type == 'location')
      {
        NavigationService.navigate('SuggestLocation', { other_id: plan_data.login_id, plan_id: plan_data.plan_id,is_direct:false, notificaitonid:data.notification_id })
      }
      
    }
    else if(data.notification_type == 'is_intrested_plan_chat')
    {

      if(global.isEventDetails == true && Platform.OS == 'ios')
      {
        return
      }

      if(plan_data.plan_type == 1)
      {
          var name = plan_data.friend_name
          NavigationService.navigate('EventDetail',{item:plan_data, plan_id:plan_data.plan_id, other_id:plan_data.login_id, plan_type:plan_data.plan_type,title_header:"Let's meet up!",Final_date:'',eventby:plan_data.name, notificaitonid:data.notification_id})
      }
      else if(plan_data.plan_type == 2)
      {
          NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type, title_header:plan_data.activity , Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
      }
      else if(plan_data.plan_type == 3)
      {
          NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type , title_header:"Wanna go to " +plan_data.line_1+"?", Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
      }
      else if(plan_data.plan_type == 4)
      {
          NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type , title_header:"I'm free, wanna hangout?", Final_date : '', eventby:plan_data.name, notificaitonid:data.notification_id})
      }
    }
    else if(data.notification_type == 'is_chat_my_plan' || data.notification_type == 'is_invite_my_plan' || data.notification_type == 'is_invite_their_plan')
    {

      if(global.isEventDetails == true && Platform.OS == 'ios')
      {
        return
      }

      if(plan_data.plan_type == 1)
      {
          var name = plan_data.friend_name
          NavigationService.navigate('EventDetail',{item:plan_data, plan_id:plan_data.plan_id, other_id:plan_data.login_id, plan_type:plan_data.plan_type,title_header:"Let's meet up!",Final_date:'',eventby:plan_data.name, notificaitonid:data.notification_id})
      }
      else if(plan_data.plan_type == 2)
      {
          NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type, title_header:plan_data.activity , Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
      }
      else if(plan_data.plan_type == 3)
      {
          NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type , title_header:"Wanna go to " +plan_data.line_1+"?", Final_date : '', eventby:plan_data.name,notificaitonid:data.notification_id})
      }
      else if(plan_data.plan_type == 4)
      {
          NavigationService.navigate('EventDetail', { item: plan_data , plan_id: plan_data.plan_id, other_id: plan_data.login_id, plan_type: plan_data.plan_type , title_header:"I'm free, wanna hangout?", Final_date : '', eventby:plan_data.name, notificaitonid:data.notification_id})
      }
    }
    else if(data.notification_type == 'Friend Request')
    {
        // this.setState({index:1, notificaitonid:item.notification_id})
        // this.readNotification()
        // this.getFriendRequests()

        NavigationService.navigate('notification', {index:1,notificaitonid:data.notification_id})
    }
    else if(data.notification_type == 'Friend Request Accepted')
    {

      //alert(JSON.stringify(data))

      AsyncStorage.setItem('other_id', plan_data.sender_id)
      NavigationService.navigate('ohter_user_profile')
    }
    else if(data.notification_type == 'New Friend Register')
    {

      //alert(JSON.stringify(plan_data.user_id))

      //AsyncStorage.setItem('other_id', plan_data.user_id)
      //NavigationService.navigate('ohter_user_profile')
    }
  }

  componentWillMount(){

    AppState.addEventListener('change', this._handleAppStateChange1);

    if(store.getState().auth.isLoggedIn){
      this.setState({login:true,loading:false})
      //global.noti_count = store.getState().auth.user.notification_count
      global.noti_count = store.getState().auth.notification_count
      console.log('App.js after logged in: '+global.noti_count)
      //this.props.navigation.navigate('App')
    }
    else{
      this.setState({login:false,loading:false})
      //this.props.navigation.navigate('Auth')
    }
  }
componentWillUnmount() {
  // console.log('_handleAppStateChange1')
  //   AppState.removeEventListener('change', this._handleAppStateChange1);
  }

  _handleAppStateChange1 = (nextAppState) => {
    global.appstate = nextAppState
    this.setState({ appState: nextAppState });

  };

  render(){

    //alert('called')

    return (
      

     <View style = {{flex:1}}>
       {this.state.loading?null:this.state.login?
        <AppContainer ref={navigatorRef => {
          
          NavigationService.setTopLevelNavigator(navigatorRef);
          
        }}/>
        :
        <AppContainer1 ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}/>
      }

      {Platform.OS == 'ios' ? <FlashMessage position="top" animated={true} autoHide = {true} duration = {5000} style = { height < 812 ? {height:64, width:'100%', justifyContent:'center'} : {} }/> : null }
        </View>

      // <View style = {{flex:1}}>
      //  {this.state.loading?null:this.state.login?
      //   <AppNavigator ref={navigatorRef => {
          
      //     NavigationService.setTopLevelNavigator(navigatorRef);
          
      //   }}/>
      //   :
      //   <AppNavigator1 ref={navigatorRef => {
      //     NavigationService.setTopLevelNavigator(navigatorRef);

      //   }}/>
      // }

      // {Platform.OS == 'ios' ? <FlashMessage position="top" animated={true} autoHide = {true} duration = {5000} style = { height < 812 ? {height:64, width:'100%', justifyContent:'center'} : {} }/> : null }
      //   </View>
    )
  }
}
