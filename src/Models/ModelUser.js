

class ModelUser {
    init = (name, email, status) => {
        this.state = {
            name: name,
            email: email,
            state: status
        };
        return this;
    }
}

export default ModelUser;