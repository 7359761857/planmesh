// export const API_ROOT = "https://planmeshapp.com/v1/";//https://3.213.221.136/index.php/v1/
// export const IMG_PREFIX_URL = "https://planmeshapp.com/v1/";
// export const PROFILE_PIC_PREFIX ="https://planmeshapp.com/v1/";

export const API_ROOT = "https://planmeshapp.com/v3/";//https://3.213.221.136/index.php/v1/
export const IMG_PREFIX_URL = "https://planmeshapp.com/v3/";
export const PROFILE_PIC_PREFIX ="https://planmeshapp.com/v3/";


export const LOGIN_ERROR = "Please enter email and password";
export const REGISTER_ERROR1 = "Please fill up all details";
export const REGISTER_ERROR2 = "Please upload profile pic";
export const REGISTER_ERROR3 = "Please accept terms & conditions";
export const VERIFICATION_CODE_WARNING = "Please enter 6 digit code";
export const geo_code_key = 'AIzaSyB_MuKPO2Jg6JFW_9T7yponY1l4vZGadcI'


export const fonts={
    Raleway_Medium:"Raleway-Medium",
    Raleway_Bold:"Raleway-Bold",
    Raleway_Regular:"Raleway-Regular",
    Raleway_Semibold:"Raleway-SemiBold",
    Roboto_Regular:"Roboto-Regular",
    Roboto_Medium:"Roboto-Medium",
    Roboto_Bold:"Roboto-Bold"
}