/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import Setup from "./src/boot/setup";
import BackgroundImage from './src/screens/Default/BackgroundImage';
import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from 'react-redux'
import { store, persistor } from './src/redux/store';
import { subscribeToNotification } from './src/services/notificationListener';
import NavigationService from './src/services/NavigationService';


export default class App extends Component {
  componentDidMount() {
    StatusBar.setHidden(false);
    StatusBar.setBarStyle("light-content", false);
    //alert('hiiiiii')
    subscribeToNotification();
  }
  componentWillUnmount(){
    subscribeToNotification();
  }
  render() {
    return <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
               <BackgroundImage>
                 <Setup />
              </BackgroundImage>
            </PersistGate>
           </Provider>;
  }
}